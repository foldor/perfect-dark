#!/bin/sh
#Run this from the perfect-dark root directory.
cp tools/m2ctx/ctx_includes.c .
./tools/m2ctx/m2ctx.py ctx_includes.c
mv ctx.c tools/m2ctx/ctx.c
rm ctx_includes.c