#!/bin/sh
#Run this from the perfect-dark root directory.
find src/ -type f -name "*.h" | sed -e 's/.*/#include "\0"/' > tools/m2ctx/ctx_includes.c
tools/m2ctx/m2ctx.py ctx_includes.c