typedef signed char s8;
typedef unsigned char u8;
typedef signed short int s16;
typedef unsigned short int u16;
typedef signed int s32;
typedef unsigned int u32;
typedef signed long long int s64;
typedef unsigned long long int u64;
typedef volatile u8 vu8;
typedef volatile u16 vu16;
typedef volatile u32 vu32;
typedef volatile u64 vu64;
typedef volatile s8 vs8;
typedef volatile s16 vs16;
typedef volatile s32 vs32;
typedef volatile s64 vs64;
typedef float f32;
typedef double f64;
typedef unsigned long size_t;
typedef struct {
   unsigned int cmd:8;
 unsigned int flags:8;
 unsigned int gain:16;
 unsigned int addr;
} Aadpcm;
typedef struct {
   unsigned int cmd:8;
 unsigned int flags:8;
 unsigned int gain:16;
 unsigned int addr;
} Apolef;
typedef struct {
   unsigned int cmd:8;
 unsigned int flags:8;
 unsigned int pad1:16;
 unsigned int addr;
} Aenvelope;
typedef struct {
   unsigned int cmd:8;
 unsigned int pad1:8;
 unsigned int dmem:16;
 unsigned int pad2:16;
 unsigned int count:16;
} Aclearbuff;
typedef struct {
   unsigned int cmd:8;
 unsigned int pad1:8;
 unsigned int pad2:16;
 unsigned int inL:16;
        unsigned int inR:16;
} Ainterleave;
typedef struct {
   unsigned int cmd:8;
 unsigned int pad1:24;
 unsigned int addr;
} Aloadbuff;
typedef struct {
   unsigned int cmd:8;
 unsigned int flags:8;
 unsigned int pad1:16;
 unsigned int addr;
} Aenvmixer;
typedef struct {
   unsigned int cmd:8;
 unsigned int flags:8;
 unsigned int gain:16;
 unsigned int dmemi:16;
 unsigned int dmemo:16;
} Amixer;
typedef struct {
   unsigned int cmd:8;
 unsigned int flags:8;
 unsigned int dmem2:16;
 unsigned int addr;
} Apan;
typedef struct {
   unsigned int cmd:8;
 unsigned int flags:8;
 unsigned int pitch:16;
 unsigned int addr;
} Aresample;
typedef struct {
   unsigned int cmd:8;
 unsigned int flags:8;
 unsigned int pad1:16;
 unsigned int addr;
} Areverb;
typedef struct {
   unsigned int cmd:8;
 unsigned int pad1:24;
 unsigned int addr;
} Asavebuff;
typedef struct {
   unsigned int cmd:8;
 unsigned int pad1:24;
 unsigned int pad2:2;
 unsigned int number:4;
 unsigned int base:24;
} Asegment;
typedef struct {
   unsigned int cmd:8;
 unsigned int flags:8;
 unsigned int dmemin:16;
 unsigned int dmemout:16;
 unsigned int count:16;
} Asetbuff;
typedef struct {
   unsigned int cmd:8;
 unsigned int flags:8;
 unsigned int vol:16;
 unsigned int voltgt:16;
 unsigned int volrate:16;
} Asetvol;
typedef struct {
    unsigned int cmd:8;
    unsigned int pad1:8;
    unsigned int dmemin:16;
    unsigned int dmemout:16;
    unsigned int count:16;
} Admemmove;
typedef struct {
    unsigned int cmd:8;
    unsigned int pad1:8;
    unsigned int count:16;
    unsigned int addr;
} Aloadadpcm;
typedef struct {
    unsigned int cmd:8;
    unsigned int pad1:8;
    unsigned int pad2:16;
    unsigned int addr;
} Asetloop;
typedef struct {
 unsigned int w0;
 unsigned int w1;
} Awords;
typedef union {
 Awords words;
 Aadpcm adpcm;
        Apolef polef;
 Aclearbuff clearbuff;
 Aenvelope envelope;
        Ainterleave interleave;
 Aloadbuff loadbuff;
        Aenvmixer envmixer;
 Aresample resample;
 Areverb reverb;
 Asavebuff savebuff;
 Asegment segment;
 Asetbuff setbuff;
 Asetvol setvol;
        Admemmove dmemmove;
        Aloadadpcm loadadpcm;
        Amixer mixer;
        Asetloop setloop;
        long long int force_union_align;
} Acmd;
typedef short ADPCM_STATE[16];
typedef short POLEF_STATE[4];
typedef short RESAMPLE_STATE[16];
typedef short ENVMIX_STATE[40];
typedef struct {
 short ob[3];
 unsigned short flag;
 short tc[2];
 unsigned char cn[4];
} Vtx_t;
typedef struct {
 short ob[3];
 unsigned short flag;
 short tc[2];
 signed char n[3];
 unsigned char a;
} Vtx_tn;
typedef union {
    Vtx_t v;
    Vtx_tn n;
    long long int force_structure_alignment;
} Vtx;
typedef struct {
  void *SourceImagePointer;
  void *TlutPointer;
  short Stride;
  short SubImageWidth;
  short SubImageHeight;
  char SourceImageType;
  char SourceImageBitSize;
  short SourceImageOffsetS;
  short SourceImageOffsetT;
  char dummy[4];
} uSprite_t;
typedef union {
  uSprite_t s;
  long long int force_structure_allignment[3];
} uSprite;
typedef struct {
 unsigned char flag;
 unsigned char v[3];
} Tri;
typedef long Mtx_t[4][4];
typedef union {
    Mtx_t m;
    long long int force_structure_alignment;
} Mtx;
typedef struct {
 short vscale[4];
 short vtrans[4];
} Vp_t;
typedef union {
    Vp_t vp;
    long long int force_structure_alignment;
} Vp;
typedef struct {
  unsigned char col[3];
  char pad1;
  unsigned char colc[3];
  char pad2;
  signed char dir[3];
  char pad3;
} Light_t;
typedef struct {
  unsigned char col[3];
  char pad1;
  unsigned char colc[3];
  char pad2;
} Ambient_t;
typedef struct {
  int x1,y1,x2,y2;
} Hilite_t;
typedef union {
    Light_t l;
    long long int force_structure_alignment[2];
} Light;
typedef union {
    Ambient_t l;
    long long int force_structure_alignment[1];
} Ambient;
typedef struct {
    Ambient a;
    Light l[7];
} Lightsn;
typedef struct {
    Ambient a;
    Light l[1];
} Lights0;
typedef struct {
    Ambient a;
    Light l[1];
} Lights1;
typedef struct {
    Ambient a;
    Light l[2];
} Lights2;
typedef struct {
    Ambient a;
    Light l[3];
} Lights3;
typedef struct {
    Ambient a;
    Light l[4];
} Lights4;
typedef struct {
    Ambient a;
    Light l[5];
} Lights5;
typedef struct {
    Ambient a;
    Light l[6];
} Lights6;
typedef struct {
    Ambient a;
    Light l[7];
} Lights7;
typedef struct {
    Light l[2];
} LookAt;
typedef union {
    Hilite_t h;
    long int force_structure_alignment[4];
} Hilite;
typedef struct {
 int cmd:8;
 unsigned int par:8;
 unsigned int len:16;
 unsigned int addr;
} Gdma;
typedef struct {
  int cmd:8;
  int pad:24;
  Tri tri;
} Gtri;
typedef struct {
  int cmd:8;
  int pad1:24;
  int pad2:24;
  unsigned char param:8;
} Gpopmtx;
typedef struct {
  int cmd:8;
  int pad0:8;
  int mw_index:8;
  int number:8;
  int pad1:8;
  int base:24;
} Gsegment;
typedef struct {
  int cmd:8;
  int pad0:8;
  int sft:8;
  int len:8;
  unsigned int data:32;
} GsetothermodeL;
typedef struct {
  int cmd:8;
  int pad0:8;
  int sft:8;
  int len:8;
  unsigned int data:32;
} GsetothermodeH;
typedef struct {
  unsigned char cmd;
  unsigned char lodscale;
  unsigned char tile;
  unsigned char on;
  unsigned short s;
  unsigned short t;
} Gtexture;
typedef struct {
  int cmd:8;
  int pad:24;
  Tri line;
} Gline3D;
typedef struct {
  int cmd:8;
  int pad1:24;
  short int pad2;
  short int scale;
} Gperspnorm;
typedef struct {
                int cmd:8;
                unsigned int fmt:3;
                unsigned int siz:2;
                unsigned int pad:7;
                unsigned int wd:12;
                unsigned int dram;
} Gsetimg;
typedef struct {
  int cmd:8;
  unsigned int muxs0:24;
  unsigned int muxs1:32;
} Gsetcombine;
typedef struct {
  int cmd:8;
  unsigned char pad;
  unsigned char prim_min_level;
  unsigned char prim_level;
  unsigned long color;
} Gsetcolor;
typedef struct {
  int cmd:8;
  int x0:10;
  int x0frac:2;
  int y0:10;
  int y0frac:2;
  unsigned int pad:8;
  int x1:10;
  int x1frac:2;
  int y1:10;
  int y1frac:2;
} Gfillrect;
typedef struct {
  int cmd:8;
  unsigned int fmt:3;
  unsigned int siz:2;
  unsigned int pad0:1;
  unsigned int line:9;
  unsigned int tmem:9;
  unsigned int pad1:5;
  unsigned int tile:3;
  unsigned int palette:4;
  unsigned int ct:1;
  unsigned int mt:1;
  unsigned int maskt:4;
  unsigned int shiftt:4;
  unsigned int cs:1;
  unsigned int ms:1;
  unsigned int masks:4;
  unsigned int shifts:4;
} Gsettile;
typedef struct {
  int cmd:8;
  unsigned int sl:12;
  unsigned int tl:12;
  int pad:5;
  unsigned int tile:3;
  unsigned int sh:12;
  unsigned int th:12;
} Gloadtile;
typedef Gloadtile Gloadblock;
typedef Gloadtile Gsettilesize;
typedef Gloadtile Gloadtlut;
typedef struct {
  unsigned int cmd:8;
  unsigned int xl:12;
  unsigned int yl:12;
  unsigned int pad1:5;
  unsigned int tile:3;
  unsigned int xh:12;
  unsigned int yh:12;
  unsigned int s:16;
  unsigned int t:16;
  unsigned int dsdx:16;
  unsigned int dtdy:16;
} Gtexrect;
typedef struct {
    unsigned long w0;
    unsigned long w1;
    unsigned long w2;
    unsigned long w3;
} TexRect;
typedef struct {
 unsigned int w0;
 unsigned int w1;
} Gwords;
typedef union {
 Gwords words;
 Gdma dma;
 Gtri tri;
 Gline3D line;
 Gpopmtx popmtx;
 Gsegment segment;
 GsetothermodeH setothermodeH;
 GsetothermodeL setothermodeL;
 Gtexture texture;
 Gperspnorm perspnorm;
 Gsetimg setimg;
 Gsetcombine setcombine;
 Gsetcolor setcolor;
 Gfillrect fillrect;
 Gsettile settile;
 Gloadtile loadtile;
 Gsettilesize settilesize;
 Gloadtlut loadtlut;
        long long int force_structure_alignment;
} Gfx;
typedef struct
{
    short ob[3];
    unsigned short flag;
    short tc[2];
    unsigned char cn[4];
} Vtx_t;
typedef struct
{
    short ob[3];
    unsigned short flag;
    short tc[2];
    signed char n[3];
    unsigned char a;
} Vtx_tn;
typedef union
{
    Vtx_t v;
    Vtx_tn n;
    long long int force_structure_alignment;
} Vtx;
typedef struct
{
    short vscale[4];
    short vtrans[4];
} Vp_t;
typedef union
{
    Vp_t vp;
    long long int force_structure_alignment;
} Vp;
typedef long int Mtx_t[4][4];
typedef union
{
    Mtx_t m;
    long long int force_structure_alignment;
} Mtx;
typedef struct
{
    unsigned int w0;
    unsigned int w1;
} Gwords;
typedef union
{
    Gwords words;
    long long int force_structure_alignment;
} Gfx;
typedef struct {
    unsigned char col[3];
    char pad1;
    unsigned char colc[3];
    char pad2;
    signed char dir[3];
    char pad3;
} Light_t;
typedef struct {
    unsigned char col[3];
    char pad1;
    unsigned char colc[3];
    char pad2;
} Ambient_t;
typedef union {
    Ambient_t l;
    long long int force_structure_alignment[1];
} Ambient;
typedef union {
    Light_t l;
    long long int force_structure_alignment[2];
} Light;
typedef struct {
    Ambient a;
    Light l[4];
} Lights4;
typedef struct {
    Light l[2];
} LookAt;
typedef struct {
    int x1, y1, x2, y2;
} Hilite_t;
typedef union {
    Hilite_t h;
    long int force_alignmnet[4];
} Hilite;
typedef struct {
  u16 imageX;
  u16 imageW;
  s16 frameX;
  u16 frameW;
  u16 imageY;
  u16 imageH;
  s16 frameY;
  u16 frameH;
  u64 *imagePtr;
  u16 imageLoad;
  u8 imageFmt;
  u8 imageSiz;
  u16 imagePal;
  u16 imageFlip;
  u16 tmemW;
  u16 tmemH;
  u16 tmemLoadSH;
  u16 tmemLoadTH;
  u16 tmemSizeW;
  u16 tmemSize;
} uObjBg_t;
typedef struct {
  u16 imageX;
  u16 imageW;
  s16 frameX;
  u16 frameW;
  u16 imageY;
  u16 imageH;
  s16 frameY;
  u16 frameH;
  u64 *imagePtr;
  u16 imageLoad;
  u8 imageFmt;
  u8 imageSiz;
  u16 imagePal;
  u16 imageFlip;
  u16 scaleW;
  u16 scaleH;
  s32 imageYorig;
  u8 padding[4];
} uObjScaleBg_t;
typedef union {
  uObjBg_t b;
  uObjScaleBg_t s;
  long long int force_structure_alignment;
} uObjBg;
typedef struct {
  s16 objX;
  u16 scaleW;
  u16 imageW;
  u16 paddingX;
  s16 objY;
  u16 scaleH;
  u16 imageH;
  u16 paddingY;
  u16 imageStride;
  u16 imageAdrs;
  u8 imageFmt;
  u8 imageSiz;
  u8 imagePal;
  u8 imageFlags;
} uObjSprite_t;
typedef union {
  uObjSprite_t s;
  long long int force_structure_alignment;
} uObjSprite;
typedef struct {
  s32 A, B, C, D;
  s16 X, Y;
  u16 BaseScaleX;
  u16 BaseScaleY;
} uObjMtx_t;
typedef union {
  uObjMtx_t m;
  long long int force_structure_alignment;
} uObjMtx;
typedef struct {
  s16 X, Y;
  u16 BaseScaleX;
  u16 BaseScaleY;
} uObjSubMtx_t;
typedef union {
  uObjSubMtx_t m;
  long long int force_structure_alignment;
} uObjSubMtx;
typedef struct {
  u32 type;
  u64 *image;
  u16 tmem;
  u16 tsize;
  u16 tline;
  u16 sid;
  u32 flag;
  u32 mask;
} uObjTxtrBlock_t;
typedef struct {
  u32 type;
  u64 *image;
  u16 tmem;
  u16 twidth;
  u16 theight;
  u16 sid;
  u32 flag;
  u32 mask;
} uObjTxtrTile_t;
typedef struct {
  u32 type;
  u64 *image;
  u16 phead;
  u16 pnum;
  u16 zero;
  u16 sid;
  u32 flag;
  u32 mask;
} uObjTxtrTLUT_t;
typedef union {
  uObjTxtrBlock_t block;
  uObjTxtrTile_t tile;
  uObjTxtrTLUT_t tlut;
  long long int force_structure_alignment;
} uObjTxtr;
typedef struct {
  uObjTxtr txtr;
  uObjSprite sprite;
} uObjTxSprite;
extern u64 gspS2DEX_fifoTextStart[], gspS2DEX_fifoTextEnd[];
extern u64 gspS2DEX_fifoDataStart[], gspS2DEX_fifoDataEnd[];
extern u64 gspS2DEX_fifo_dTextStart[], gspS2DEX_fifo_dTextEnd[];
extern u64 gspS2DEX_fifo_dDataStart[], gspS2DEX_fifo_dDataEnd[];
extern u64 gspS2DEX2_fifoTextStart[], gspS2DEX2_fifoTextEnd[];
extern u64 gspS2DEX2_fifoDataStart[], gspS2DEX2_fifoDataEnd[];
extern u64 gspS2DEX2_xbusTextStart[], gspS2DEX2_xbusTextEnd[];
extern u64 gspS2DEX2_xbusDataStart[], gspS2DEX2_xbusDataEnd[];
extern void guS2DInitBg(uObjBg *);
  extern void guS2DEmuSetScissor(u32, u32, u32, u32, u8);
  extern void guS2DEmuBgRect1Cyc(Gfx **, uObjBg *);
void guPerspectiveF(float mf[4][4], u16 *perspNorm, float fovy, float aspect,
                    float near, float far, float scale);
void guPerspective(Mtx *m, u16 *perspNorm, float fovy, float aspect, float near,
                   float far, float scale);
void guOrtho(Mtx *m, float left, float right, float bottom, float top,
             float near, float far, float scale);
void guTranslate(Mtx *m, float x, float y, float z);
void guRotate(Mtx *m, float a, float x, float y, float z);
void guScale(Mtx *m, float x, float y, float z);
void guMtxF2L(float mf[4][4], Mtx *m);
void guMtxIdent(Mtx *m);
void guMtxIdentF(float mf[4][4]);
void guMtxL2F(float mf[4][4], Mtx *m);
void guNormalize(float *, float *, float *);
void guAlignF(float mf[4][4], float a, float x, float y, float z);
void guRotateF(float mf[4][4], float a, float x, float y, float z);
void guLookAt(Mtx *m, float xEye, float yEye, float zEye,
  float xAt, float yAt, float zAt,
  float xUp, float yUp, float zUp);
signed short coss(unsigned short x);
signed short sins(unsigned short x);
void guLookAtReflect (Mtx *m, LookAt *l, float xEye, float yEye, float zEye,
                    float xAt, float yAt, float zAt,
                    float xUp, float yUp, float zUp);
typedef s32 ALMicroTime;
typedef u8 ALPan;
typedef struct ALLink_s {
    struct ALLink_s *next;
    struct ALLink_s *prev;
} ALLink;
void alUnlink(ALLink *element);
void alLink(ALLink *element, ALLink *after);
typedef s32 (*ALDMAproc)(s32 addr, s32 len, void *state);
typedef ALDMAproc (*ALDMANew)(void *state);
void alCopy(void *src, void *dest, s32 len);
typedef struct {
    u8 *base;
    u8 *cur;
    s32 len;
    s32 count;
} ALHeap;
void alHeapInit(ALHeap *hp, u8 *base, s32 len);
void *alHeapDBAlloc(u8 *file, s32 line, ALHeap *hp, s32 num, s32 size);
s32 alHeapCheck(ALHeap *hp);
typedef u8 ALFxId;
typedef void *ALFxRef;
enum {AL_ADPCM_WAVE = 0,
         AL_RAW16_WAVE};
typedef struct {
    s32 order;
    s32 npredictors;
    s16 book[1];
} ALADPCMBook;
typedef struct {
    u32 start;
    u32 end;
    u32 count;
    ADPCM_STATE state;
} ALADPCMloop;
typedef struct {
    u32 start;
    u32 end;
    u32 count;
} ALRawLoop;
typedef struct {
    ALMicroTime attackTime;
    ALMicroTime decayTime;
    ALMicroTime releaseTime;
    u8 attackVolume;
    u8 decayVolume;
} ALEnvelope;
typedef struct {
    u8 velocityMin;
    u8 velocityMax;
    u8 keyMin;
    u8 keyMax;
    u8 keyBase;
    s8 detune;
} ALKeyMap;
typedef struct {
    ALADPCMloop *loop;
    ALADPCMBook *book;
} ALADPCMWaveInfo;
typedef struct {
    ALRawLoop *loop;
} ALRAWWaveInfo;
typedef struct ALWaveTable_s {
    u8 *base;
    s32 len;
    u8 type;
    u8 flags;
    union {
        ALADPCMWaveInfo adpcmWave;
        ALRAWWaveInfo rawWave;
    } waveInfo;
} ALWaveTable;
typedef struct ALSound_s {
    ALEnvelope *envelope;
    ALKeyMap *keyMap;
    ALWaveTable *wavetable;
    ALPan samplePan;
    u8 sampleVolume;
    u8 flags;
} ALSound;
typedef struct {
    u8 volume;
    ALPan pan;
    u8 priority;
    u8 flags;
    u8 tremType;
    u8 tremRate;
    u8 tremDepth;
    u8 tremDelay;
    u8 vibType;
    u8 vibRate;
    u8 vibDepth;
    u8 vibDelay;
    s16 bendRange;
    s16 soundCount;
    ALSound *soundArray[1];
} ALInstrument;
typedef struct ALBank_s {
    s16 instCount;
    u8 flags;
    u8 pad;
    s32 sampleRate;
    ALInstrument *percussion;
    ALInstrument *instArray[1];
} ALBank;
typedef struct {
    s16 revision;
    s16 bankCount;
    ALBank *bankArray[1];
} ALBankFile;
void alBnkfNew(ALBankFile *f, u8 *table);
typedef struct {
    u8 *offset;
    s32 len;
} ALSeqData;
typedef struct {
    s16 revision;
    s16 seqCount;
    ALSeqData seqArray[1];
} ALSeqFile;
void alSeqFileNew(ALSeqFile *f, u8 *base);
typedef ALMicroTime (*ALVoiceHandler)(void *);
typedef struct {
    s32 maxVVoices;
    s32 maxPVoices;
    s32 maxUpdates;
    s32 maxFXbusses;
    void *dmaproc;
    ALHeap *heap;
    s32 outputRate;
    ALFxId fxTypes[4];
    s32 *params;
} ALSynConfig;
typedef struct ALPlayer_s {
    struct ALPlayer_s *next;
    void *clientData;
    ALVoiceHandler handler;
    ALMicroTime callTime;
    s32 samplesLeft;
} ALPlayer;
typedef struct ALVoice_s {
    ALLink node;
    struct PVoice_s *pvoice;
    ALWaveTable *table;
    void *clientPrivate;
    s16 state;
    s16 priority;
    s16 fxBus;
    s16 unityPitch;
} ALVoice;
typedef struct ALVoiceConfig_s {
    s16 priority;
    s16 fxBus;
    u8 unityPitch;
} ALVoiceConfig;
typedef struct {
             ALPlayer *head;
             ALLink pFreeList;
             ALLink pAllocList;
             ALLink pLameList;
             s32 paramSamples;
             s32 curSamples;
             ALDMANew dma;
             ALHeap *heap;
             struct ALParam_s *paramList;
             struct ALMainBus_s *mainBus;
             struct ALAuxBus_s *auxBus;
             struct ALFilter_s *outputFilter;
             s32 numPVoices;
             s32 maxAuxBusses;
             s32 outputRate;
             s32 maxOutSamples;
} ALSynth;
void alSynNew(ALSynth *s, ALSynConfig *config);
void alSynDelete(ALSynth *s);
void alSynAddPlayer(ALSynth *s, ALPlayer *client);
void alSynRemovePlayer(ALSynth *s, ALPlayer *client);
s32 alSynAllocVoice(ALSynth *s, ALVoice *v, ALVoiceConfig *vc);
void alSynFreeVoice(ALSynth *s, ALVoice *voice);
void alSynStartVoice(ALSynth *s, ALVoice *voice, ALWaveTable *w);
void alSynStartVoiceParams(ALSynth *s, ALVoice *voice, ALWaveTable *w,
                              f32 pitch, s16 vol, ALPan pan, u8 fxmix,
                              ALMicroTime t);
void alSynStopVoice(ALSynth *s, ALVoice *voice);
void alSynSetVol(ALVoice *v, s16 vol, ALMicroTime delta);
void alSynSetPitch(ALSynth *s, ALVoice *voice, f32 ratio);
void alSynSetPan(ALSynth *s, ALVoice *voice, ALPan pan);
void alSynSetFXMix(ALSynth *s, ALVoice *voice, u8 fxmix);
void n_alSynSetPriority(ALVoice *voice, s16 priority);
s16 alSynGetPriority(ALSynth *s, ALVoice *voice);
ALFxRef *alSynAllocFX(ALSynth *s, s16 bus, ALSynConfig *c, ALHeap *hp);
ALFxRef alSynGetFXRef(ALSynth *s, s16 bus, s16 index);
void alSynFreeFX(ALSynth *s, ALFxRef *fx);
void alSynSetFXParam(ALSynth *s, ALFxRef fx, s16 paramID, void *param);
typedef struct {
    ALSynth drvr;
} ALGlobals;
extern ALGlobals *n_syn;
void alInit(ALGlobals *glob, ALSynConfig *c);
void alClose(ALGlobals *glob);
Acmd *alAudioFrame(Acmd *cmdList, s32 *cmdLen, s16 *outBuf, s32 outLen);
enum ALMsg {
    AL_SEQ_REF_EVT,
    AL_SEQ_MIDI_EVT,
    AL_SEQP_MIDI_EVT,
    AL_TEMPO_EVT,
    AL_SEQ_END_EVT,
    AL_NOTE_END_EVT,
    AL_SEQP_ENV_EVT,
    AL_SEQP_META_EVT,
    AL_SEQP_PROG_EVT,
    AL_SEQP_API_EVT,
    AL_SEQP_VOL_EVT,
    AL_SEQP_LOOP_EVT,
    AL_SEQP_PRIORITY_EVT,
    AL_SEQP_SEQ_EVT,
    AL_SEQP_BANK_EVT,
    AL_SEQP_PLAY_EVT,
    AL_SEQP_STOP_EVT,
    AL_SEQP_STOPPING_EVT,
    AL_TRACK_END,
    AL_CSP_LOOPSTART,
    AL_CSP_LOOPEND,
    AL_CSP_NOTEOFF_EVT,
    AL_TREM_OSC_EVT,
    AL_VIB_OSC_EVT
};
enum AL_MIDIstatus {
    AL_MIDI_ChannelMask = 0x0F,
    AL_MIDI_StatusMask = 0xF0,
    AL_MIDI_ChannelVoice = 0x80,
    AL_MIDI_NoteOff = 0x80,
    AL_MIDI_NoteOn = 0x90,
    AL_MIDI_PolyKeyPressure = 0xA0,
    AL_MIDI_ControlChange = 0xB0,
    AL_MIDI_ChannelModeSelect = 0xB0,
    AL_MIDI_ProgramChange = 0xC0,
    AL_MIDI_ChannelPressure = 0xD0,
    AL_MIDI_PitchBendChange = 0xE0,
    AL_MIDI_SysEx = 0xF0,
    AL_MIDI_SystemCommon = 0xF1,
    AL_MIDI_TimeCodeQuarterFrame = 0xF1,
    AL_MIDI_SongPositionPointer = 0xF2,
    AL_MIDI_SongSelect = 0xF3,
    AL_MIDI_Undefined1 = 0xF4,
    AL_MIDI_Undefined2 = 0xF5,
    AL_MIDI_TuneRequest = 0xF6,
    AL_MIDI_EOX = 0xF7,
    AL_MIDI_SystemRealTime = 0xF8,
    AL_MIDI_TimingClock = 0xF8,
    AL_MIDI_Undefined3 = 0xF9,
    AL_MIDI_Start = 0xFA,
    AL_MIDI_Continue = 0xFB,
    AL_MIDI_Stop = 0xFC,
    AL_MIDI_Undefined4 = 0xFD,
    AL_MIDI_ActiveSensing = 0xFE,
    AL_MIDI_SystemReset = 0xFF,
    AL_MIDI_Meta = 0xFF
};
enum AL_MIDIctrl {
    AL_MIDI_VOLUME_CTRL = 0x07,
    AL_MIDI_PAN_CTRL = 0x0A,
    AL_MIDI_PRIORITY_CTRL = 0x10,
    AL_MIDI_FX_CTRL_0 = 0x14,
    AL_MIDI_FX_CTRL_1 = 0x15,
    AL_MIDI_FX_CTRL_2 = 0x16,
    AL_MIDI_FX_CTRL_3 = 0x17,
    AL_MIDI_FX_CTRL_4 = 0x18,
    AL_MIDI_FX_CTRL_5 = 0x19,
    AL_MIDI_FX_CTRL_6 = 0x1A,
    AL_MIDI_FX_CTRL_7 = 0x1B,
    AL_MIDI_FX_CTRL_8 = 0x1C,
    AL_MIDI_FX_CTRL_9 = 0x1D,
    AL_MIDI_SUSTAIN_CTRL = 0x40,
    AL_MIDI_FX1_CTRL = 0x5B,
    AL_MIDI_FX3_CTRL = 0x5D
};
enum AL_MIDImeta {
    AL_MIDI_META_TEMPO = 0x51,
    AL_MIDI_META_EOT = 0x2f
};
typedef struct {
    u8 *curPtr;
    s32 lastTicks;
    s32 curTicks;
    s16 lastStatus;
} ALSeqMarker;
typedef struct {
    s32 ticks;
    u8 status;
    u8 byte1;
    u8 byte2;
    u32 duration;
} ALMIDIEvent;
typedef struct {
    s32 ticks;
    u8 status;
    u8 type;
    u8 len;
    u8 byte1;
    u8 byte2;
    u8 byte3;
} ALTempoEvent;
typedef struct {
    s32 ticks;
    u8 status;
    u8 type;
    u8 len;
} ALEndEvent;
typedef struct {
    struct ALVoice_s *voice;
} ALNoteEvent;
typedef struct {
    struct ALVoice_s *voice;
    ALMicroTime delta;
    u8 vol;
} ALVolumeEvent;
typedef struct {
    s16 vol;
} ALSeqpVolEvent;
typedef struct {
    ALSeqMarker *start;
    ALSeqMarker *end;
    s32 count;
} ALSeqpLoopEvent;
typedef struct {
    u8 chan;
    u8 priority;
} ALSeqpPriorityEvent;
typedef struct {
    void *seq;
} ALSeqpSeqEvent;
typedef struct {
    ALBank *bank;
} ALSeqpBankEvent;
typedef struct {
    struct ALVoiceState_s *vs;
    void *oscState;
    u8 chan;
} ALOscEvent;
typedef struct {
    s16 type;
    union {
        ALMIDIEvent midi;
        ALTempoEvent tempo;
        ALEndEvent end;
        ALNoteEvent note;
        ALVolumeEvent vol;
        ALSeqpLoopEvent loop;
        ALSeqpVolEvent spvol;
 ALSeqpPriorityEvent sppriority;
 ALSeqpSeqEvent spseq;
 ALSeqpBankEvent spbank;
        ALOscEvent osc;
    } msg;
} ALEvent;
typedef struct {
    ALLink node;
    ALMicroTime delta;
    ALEvent evt;
} ALEventListItem;
typedef struct {
    ALLink freeList;
    ALLink allocList;
    s32 eventCount;
} ALEventQueue;
void n_alEvtqNew(ALEventQueue *evtq, ALEventListItem *items,
                          s32 itemCount);
ALMicroTime n_alEvtqNextEvent(ALEventQueue *evtq, ALEvent *evt);
void alEvtqPostEvent(ALEventQueue *evtq, ALEvent *evt,
                                ALMicroTime delta, s32 arg3);
void alEvtqFlush(ALEventQueue *evtq);
void n_alEvtqFlushType(ALEventQueue *evtq, s16 type);
typedef struct ALVoiceState_s {
    struct ALVoiceState_s *next;
    ALVoice voice;
    ALSound *sound;
    ALMicroTime envEndTime;
    f32 pitch;
    f32 vibrato;
    u8 envGain;
    u8 channel;
    u8 key;
    u8 velocity;
    u8 envPhase;
    u8 phase;
    u8 tremelo;
    u8 flags;
    u32 unk38;
    u32 unk3c;
} ALVoiceState;
typedef struct {
             ALInstrument *instrument;
             s16 bendRange;
             ALFxId fxId;
             ALPan pan;
             u8 priority;
             u8 vol;
             u8 fxmix;
             u8 unk0b;
             u8 sustain;
    u8 unk0d;
    u8 unk0e;
    u8 unk0f;
    u8 unk10;
    u8 unk11;
    u8 unk12;
    u8 unk13;
    f32 pitchBend;
    u32 unk18;
    u32 unk1c;
    u32 unk20;
    u32 unk24;
    u32 unk28;
    u32 unk2c;
    u8 unk30;
    u8 unk31;
    u8 unk32;
} ALChanState;
typedef struct ALSeq_s {
    u8 *base;
    u8 *trackStart;
    u8 *curPtr;
    s32 lastTicks;
    s32 len;
    f32 qnpt;
    s16 division;
    s16 lastStatus;
} ALSeq;
typedef struct {
    u32 trackOffset[16];
    u32 division;
} ALCMidiHdr;
typedef struct ALCSeq_s {
    ALCMidiHdr *base;
    u32 validTracks;
    f32 qnpt;
    u32 lastTicks;
    u32 lastDeltaTicks;
    u32 deltaFlag;
    u8 *curLoc[16];
    u8 *curBUPtr[16];
    u8 curBULen[16];
    u8 lastStatus[16];
    u32 evtDeltaTicks[16];
} ALCSeq;
typedef struct {
    u32 validTracks;
    s32 lastTicks;
    u32 lastDeltaTicks;
    u8 *curLoc[16];
    u8 *curBUPtr[16];
    u8 curBULen[16];
    u8 lastStatus[16];
    u32 evtDeltaTicks[16];
} ALCSeqMarker;
typedef struct {
    s32 maxVoices;
    s32 maxEvents;
    u8 maxChannels;
    u8 debugFlags;
    ALHeap *heap;
    void *initOsc;
    void *updateOsc;
    void *stopOsc;
} ALSeqpConfig;
typedef ALMicroTime (*ALOscInit)(void **oscState,f32 *initVal, u8 oscType,
                                   u8 oscRate, u8 oscDepth, u8 oscDelay);
typedef ALMicroTime (*ALOscUpdate)(void *oscState, f32 *updateVal);
typedef void (*ALOscStop)(void *oscState);
typedef struct {
    ALPlayer node;
    ALSynth *drvr;
    ALSeq *target;
    ALMicroTime curTime;
    ALBank *bank;
    s32 uspt;
    s32 nextDelta;
    s32 state;
    u16 chanMask;
    s16 vol;
    u8 maxChannels;
    u8 debugFlags;
    ALEvent nextEvent;
    ALEventQueue evtq;
    ALMicroTime frameTime;
    ALChanState *chanState;
    ALVoiceState *vAllocHead;
    ALVoiceState *vAllocTail;
    ALVoiceState *vFreeList;
    ALOscInit initOsc;
    ALOscUpdate updateOsc;
    ALOscStop stopOsc;
    ALSeqMarker *loopStart;
    ALSeqMarker *loopEnd;
    s32 loopCount;
} ALSeqPlayer;
typedef struct {
    ALPlayer node;
    ALSynth *drvr;
    ALCSeq *target;
    ALMicroTime curTime;
    ALBank *bank;
    s32 uspt;
    s32 nextDelta;
    s32 state;
    u16 chanMask;
    s16 vol;
    u8 maxChannels;
    u8 debugFlags;
    ALEvent nextEvent;
    ALEventQueue evtq;
    ALMicroTime frameTime;
    ALChanState *chanState;
    ALVoiceState *vAllocHead;
    ALVoiceState *vAllocTail;
    ALVoiceState *vFreeList;
    ALOscInit initOsc;
    ALOscUpdate updateOsc;
    ALOscStop stopOsc;
    f32 unk7c;
    f32 unk80;
    s32 unk84;
    u8 unk88;
    u8 unk89;
} ALCSPlayer;
void alSeqNew(ALSeq *seq, u8 *ptr, s32 len);
void alSeqNextEvent(ALSeq *seq, ALEvent *event);
s32 alSeqGetTicks(ALSeq *seq);
f32 alSeqTicksToSec(ALSeq *seq, s32 ticks, u32 tempo);
u32 alSeqSecToTicks(ALSeq *seq, f32 sec, u32 tempo);
void alSeqNewMarker(ALSeq *seq, ALSeqMarker *m, u32 ticks);
void alSeqSetLoc(ALSeq *seq, ALSeqMarker *marker);
void alSeqGetLoc(ALSeq *seq, ALSeqMarker *marker);
void alCSeqNew(ALCSeq *seq, u8 *ptr);
void alCSeqNextEvent(ALCSeq *seq, ALEvent *evt, s32 arg2);
s32 alCSeqGetTicks(ALCSeq *seq);
f32 alCSeqTicksToSec(ALCSeq *seq, s32 ticks, u32 tempo);
u32 alCSeqSecToTicks(ALCSeq *seq, f32 sec, u32 tempo);
void alCSeqNewMarker(ALCSeq *seq, ALCSeqMarker *m, u32 ticks);
void alCSeqSetLoc(ALCSeq *seq, ALCSeqMarker *marker);
void alCSeqGetLoc(ALCSeq *seq, ALCSeqMarker *marker);
f32 alCents2Ratio(s32 cents);
void alSeqpNew(ALSeqPlayer *seqp, ALSeqpConfig *config);
void alSeqpDelete(ALSeqPlayer *seqp);
void alSeqpSetSeq(ALSeqPlayer *seqp, ALSeq *seq);
ALSeq *alSeqpGetSeq(ALSeqPlayer *seqp);
void alSeqpPlay(ALSeqPlayer *seqp);
void alSeqpStop(ALSeqPlayer *seqp);
s32 alSeqpGetState(ALSeqPlayer *seqp);
void alSeqpSetBank(ALSeqPlayer *seqp, ALBank *b);
void alSeqpSetTempo(ALSeqPlayer *seqp, s32 tempo);
s32 alSeqpGetTempo(ALSeqPlayer *seqp);
s16 alSeqpGetVol(ALSeqPlayer *seqp);
void alSeqpSetVol(ALSeqPlayer *seqp, s16 vol);
void alSeqpLoop(ALSeqPlayer *seqp, ALSeqMarker *start, ALSeqMarker *end, s32 count);
void alSeqpSetChlProgram(ALSeqPlayer *seqp, u8 chan, u8 prog);
s32 alSeqpGetChlProgram(ALSeqPlayer *seqp, u8 chan);
void alSeqpSetChlFXMix(ALSeqPlayer *seqp, u8 chan, u8 fxmix);
u8 alSeqpGetChlFXMix(ALSeqPlayer *seqp, u8 chan);
void alSeqpSetChlVol(ALSeqPlayer *seqp, u8 chan, u8 vol);
u8 alSeqpGetChlVol(ALSeqPlayer *seqp, u8 chan);
void alSeqpSetChlPan(ALSeqPlayer *seqp, u8 chan, ALPan pan);
ALPan alSeqpGetChlPan(ALSeqPlayer *seqp, u8 chan);
void alSeqpSetChlPriority(ALSeqPlayer *seqp, u8 chan, u8 priority);
u8 alSeqpGetChlPriority(ALSeqPlayer *seqp, u8 chan);
void alSeqpSendMidi(ALSeqPlayer *seqp, s32 ticks, u8 status, u8 byte1, u8 byte2);
void alCSPNew(ALCSPlayer *seqp, ALSeqpConfig *config);
void alCSPDelete(ALCSPlayer *seqp);
void alCSPSetSeq(ALCSPlayer *seqp, ALCSeq *seq);
ALCSeq *alCSPGetSeq(ALCSPlayer *seqp);
void alCSPPlay(ALCSPlayer *seqp);
void alCSPStop(ALCSPlayer *seqp);
s32 alCSPGetState(ALCSPlayer *seqp);
void alCSPSetBank(ALCSPlayer *seqp, ALBank *b);
void alCSPSetTempo(ALCSPlayer *seqp, s32 tempo);
s32 alCSPGetTempo(ALCSPlayer *seqp);
s16 alCSPGetVol(ALCSPlayer *seqp);
void alCSPSetVol(ALCSPlayer *seqp, s16 vol);
void alCSPSetChlProgram(ALCSPlayer *seqp, u8 chan, u8 prog);
s32 alCSPGetChlProgram(ALCSPlayer *seqp, u8 chan);
void alCSPSetChlFXMix(ALCSPlayer *seqp, u8 chan, u8 fxmix);
u8 alCSPGetChlFXMix(ALCSPlayer *seqp, u8 chan);
void alCSPSetChlPan(ALCSPlayer *seqp, u8 chan, ALPan pan);
ALPan alCSPGetChlPan(ALCSPlayer *seqp, u8 chan);
void alCSPSetChlVol(ALCSPlayer *seqp, u8 chan, u8 vol);
u8 alCSPGetChlVol(ALCSPlayer *seqp, u8 chan);
void alCSPSetChlPriority(ALCSPlayer *seqp, u8 chan, u8 priority);
u8 alCSPGetChlPriority(ALCSPlayer *seqp, u8 chan);
void alCSPSendMidi(ALCSPlayer *seqp, s32 ticks, u8 status,
                       u8 byte1, u8 byte2);
typedef struct {
    s32 maxSounds;
    s32 maxEvents;
    ALHeap *heap;
} ALSndpConfig;
typedef struct {
    ALPlayer node;
    ALEventQueue evtq;
    ALEvent nextEvent;
    ALSynth *drvr;
    s32 target;
    void *sndState;
    s32 maxSounds;
    ALMicroTime frameTime;
    ALMicroTime nextDelta;
    ALMicroTime curTime;
} ALSndPlayer;
typedef s16 ALSndId;
void alSndpNew(ALSndPlayer *sndp, ALSndpConfig *c);
void alSndpDelete(ALSndPlayer *sndp);
ALSndId alSndpAllocate(ALSndPlayer *sndp, ALSound *sound);
void alSndpDeallocate(ALSndPlayer *sndp, ALSndId id);
void alSndpSetSound(ALSndPlayer *sndp, ALSndId id);
ALSndId alSndpGetSound(ALSndPlayer *sndp);
void alSndpPlay(ALSndPlayer *sndp);
void alSndpPlayAt(ALSndPlayer *sndp, ALMicroTime delta);
void alSndpStop(ALSndPlayer *sndp);
void alSndpSetVol(ALSndPlayer *sndp, s16 vol);
void alSndpSetPitch(ALSndPlayer *sndp, f32 pitch);
void alSndpSetPan(ALSndPlayer *sndp, ALPan pan);
void alSndpSetPriority(ALSndPlayer *sndp, ALSndId id, u8 priority);
void alSndpSetFXMix(ALSndPlayer *sndp, u8 mix);
s32 alSndpGetState(ALSndPlayer *sndp);
void alParseAbiCL(Acmd *cmdList, u32 nbytes);
extern u32 osTvType;
extern u32 osRomBase;
extern u32 osResetType;
extern u32 osMemSize;
extern u8 osAppNmiBuffer[64];
extern u32 osAiGetStatus(void);
extern u32 osAiGetLength(void);
extern s32 osAiSetFrequency(u32);
extern s32 osAiSetNextBuffer(void *, u32);
extern void osInvalDCache(void *, s32);
extern void osInvalICache(void *, s32);
extern void osWritebackDCache(void *, s32);
extern void osWritebackDCacheAll(void);
typedef u32 OSEvent;

typedef s32 OSPri;
typedef s32 OSId;

typedef struct
{
    u32 flag;
    u32 count;
    u64 time;
} __OSThreadprofile_s;
typedef union
{
 struct {
  f32 f_odd2;
  f32 f_odd;
  f32 f_even2;
  f32 f_even;
 } f;
} __OSfp;
typedef struct
{
 u64 at, v0, v1, a0, a1, a2, a3;
 u64 t0, t1, t2, t3, t4, t5, t6, t7;
 u64 s0, s1, s2, s3, s4, s5, s6, s7;
 u64 t8, t9, gp, sp, s8, ra;
 u64 lo, hi;
 u32 sr, pc, cause, badvaddr, rcp;
 u32 fpcsr;
 __OSfp fp0, fp2, fp4, fp6, fp8, fp10, fp12, fp14;
 __OSfp fp16, fp18, fp20, fp22, fp24, fp26, fp28, fp30;
} __OSThreadContext;
typedef struct OSThread_s
{
             struct OSThread_s *next;
             OSPri priority;
             struct OSThread_s **queue;
             struct OSThread_s *tlnext;
             u16 state;
             u16 flags;
             OSId id;
             int fp;
             __OSThreadprofile_s *thprof;
             __OSThreadContext context;
} OSThread;
typedef void * OSMesg;
typedef struct OSMesgQueue_s {
 OSThread *mtqueue;
 OSThread *fullqueue;
 s32 validCount;
 s32 first;
 s32 msgCount;
 OSMesg *msg;
} OSMesgQueue;
typedef struct __OSEventState {
 OSMesgQueue *messageQueue;
 OSMesg message;
} __OSEventState;
extern void osCreateMesgQueue(OSMesgQueue *, OSMesg *, s32);
extern s32 osSendMesg(OSMesgQueue *, OSMesg, s32);
extern s32 osJamMesg(OSMesgQueue *, OSMesg, s32);
extern s32 osRecvMesg(OSMesgQueue *, OSMesg *, s32);
extern void osSetEventMesg(OSEvent, OSMesgQueue *, OSMesg);
typedef struct {
 u16 type;
 u8 status;
 u8 errno;
}OSContStatus;
typedef struct {
 u16 button;
 s8 stick_x;
 s8 stick_y;
 u8 errno;
} OSContPad;
typedef struct {
 void *address;
 u8 databuffer[32];
        u8 addressCrc;
 u8 dataCrc;
 u8 errno;
} OSContRamIo;
typedef struct
{
              u32 ramarray[15];
               u32 pifstatus;
} OSPifRam;
typedef struct
{
              u8 dummy;
              u8 txsize;
              u8 rxsize;
              u8 cmd;
              u16 button;
              s8 stick_x;
              s8 stick_y;
} __OSContReadFormat;
typedef struct
{
              u8 dummy;
              u8 txsize;
              u8 rxsize;
              u8 cmd;
              u8 typeh;
              u8 typel;
              u8 status;
              u8 dummy1;
} __OSContRequestFormat;
typedef struct
{
              u8 txsize;
              u8 rxsize;
              u8 cmd;
              u8 typeh;
              u8 typel;
              u8 status;
} __OSContRequestFormatShort;
typedef struct
{
              u8 dummy;
              u8 txsize;
              u8 rxsize;
              u8 cmd;
              u8 hi;
              u8 lo;
              u8 data[1];
               u8 datacrc;
} __OSContRamReadFormat;
typedef union {
              struct
    {
                  u8 bank;
                  u8 page;
    } inode_t;
              u16 ipage;
} __OSInodeUnit;
typedef struct
{
              u32 game_code;
              u16 company_code;
              __OSInodeUnit start_page;
              u8 status;
              s8 reserved;
              u16 data_sum;
              u8 ext_name[4];
               u8 game_name[16];
} __OSDir;
typedef struct
{
              __OSInodeUnit inode_page[128];
} __OSInode;
typedef struct
{
              u32 repaired;
              u32 random;
              u64 serial_mid;
               u64 serial_low;
               u16 deviceid;
               u8 banks;
               u8 version;
               u16 checksum;
               u16 inverted_checksum;
} __OSPackId;
typedef struct
{
              u8 txsize;
              u8 rxsize;
              u8 cmd;
              u8 address;
              u8 data[1];
} __OSContEepromFormat;
extern s32 osContInit(OSMesgQueue *, u8 *, OSContStatus *);
extern s32 osContReset(OSMesgQueue *, OSContStatus *);
extern s32 osContStartQuery(OSMesgQueue *);
extern s32 osContStartReadData(OSMesgQueue *);
extern s32 osContSetCh(u8);
extern void osContGetQuery(OSContStatus *);
extern void osContGetReadData(OSContPad *);
extern s32 osEepromProbe(OSMesgQueue *);
extern s32 osEepromRead(OSMesgQueue *, u8, u8 *);
extern s32 osEepromWrite(OSMesgQueue *, u8, u8 *);
extern s32 osEepromLongRead(OSMesgQueue *, u8, u8 *, int);
extern s32 osEepromLongWrite(OSMesgQueue *, u8, u8 *, int);
typedef u32 OSIntMask;
typedef u32 OSHWIntr;
extern OSIntMask osGetIntMask(void);
extern OSIntMask osSetIntMask(OSIntMask);
OSThread *__osGetCurrFaultedThread(void);
extern void bcopy(const void *, void *, int);
extern int bcmp(const void *, const void *, int);
extern void bzero(void *, int);
extern int sprintf(char *s, const char *fmt, ...);
extern void osSyncPrintf(const char *fmt, ...);
typedef struct {
    union {
                 long long s64;
        double f64;
    } value;
             char *buff;
             int n0;
             int num_leading_zeros;
             int part2_len;
             int num_mid_zeros;
             int part3_len;
             int num_trailing_zeros;
             int precision;
             int width;
             unsigned int size;
             unsigned int flags;
             char length;
} printf_struct;
typedef char *va_list;
void osInitialize(void);
u32 osGetCount(void);
u32 osVirtualToPhysical(void *);
f32 cosf(f32 arg0);
f32 sinf(f32 arg0);
f32 sqrtf(f32 arg0);
void *memcpy(void *, const void *, size_t);
size_t strlen(const char *s);
char *strchr(const char *s, int c);
typedef struct {
 int status;
 OSMesgQueue *queue;
 int channel;
 u8 id[32];
 u8 label[32];
 int version;
 int dir_size;
 int inode_table;
 int minode_table;
 int dir_table;
 int inode_start_page;
 u8 banks;
 u8 activebank;
} OSPfs;
typedef struct {
 u32 file_size;
   u32 game_code;
   u16 company_code;
   char ext_name[4];
   char game_name[16];
} OSPfsState;
s32 osPfsIsPlug(OSMesgQueue *, u8 *);
s32 osPfsAllocateFile(OSPfs *pfs, u16 company_code, u32 game_code, char *game_name, char *ext_name, s32 num_bytes, s32 *file_no);
s32 osPfsFindFile(OSPfs *pfs, u16 company_code, u32 game_code, char *game_name, char *ext_name, s32 *file_no);
s32 osPfsDeleteFile(OSPfs *pfs, u16 company_code, u32 game_code, char *game_name, char *ext_name);
s32 osPfsFileState(OSPfs *pfs, s32 file_no, OSPfsState *state);
s32 osPfsReadWriteFile(OSPfs* pfs, s32 fileNo, u8 flag, s32 offset, s32 size, u8* data);
s32 osPfsFreeBlocks(OSPfs* pfs, s32 *bytes_not_used);
s32 osMotorAccess(OSPfs *pfs, u32 vibrate);
void osSetUpMempakWrite(s32 channel, OSPifRam* buf);
s32 osMotorProbe(OSMesgQueue *mq, OSPfs *pfs, s32 channel);
typedef struct
{
    u32 errStatus;
    void *dramAddr;
    void *C2Addr;
    u32 sectorSize;
    u32 C1ErrNum;
    u32 C1ErrSector[4];
} __OSBlockInfo;
typedef struct
{
    u32 cmdType;
    u16 transferMode;
    u16 blockNum;
    s32 sectorNum;
    u32 devAddr;
    u32 bmCtlShadow;
    u32 seqCtlShadow;
    __OSBlockInfo block[2];
} __OSTranxInfo;
typedef struct OSPiHandle_s
{
    struct OSPiHandle_s *next;
    u8 type;
    u8 latency;
    u8 pageSize;
    u8 relDuration;
    u8 pulse;
    u8 domain;
    u32 baseAddress;
    u32 speed;
    __OSTranxInfo transferInfo;
} OSPiHandle;
typedef struct
{
    u8 type;
    u32 address;
} OSPiInfo;
typedef struct
{
    u16 type;
    u8 pri;
    u8 status;
    OSMesgQueue *retQueue;
} OSIoMesgHdr;
typedef struct
{
             OSIoMesgHdr hdr;
             void *dramAddr;
             u32 devAddr;
             u32 size;
    OSPiHandle *piHandle;
} OSIoMesg;
typedef struct {
 s32 active;
 OSThread *thread;
 OSMesgQueue *cmdQueue;
 OSMesgQueue *evtQueue;
 OSMesgQueue *acsQueue;
 s32 (*dma)(s32, u32, void *, u32);
 s32 (*edma)(OSPiHandle *, s32, u32, void *, u32);
} OSDevMgr;
s32 osPiStartDma(OSIoMesg *mb, s32 priority, s32 direction,
                 u32 devAddr, void *vAddr, u32 nbytes, OSMesgQueue *mq);
void osCreatePiManager(OSPri pri, OSMesgQueue *cmdQ, OSMesg *cmdBuf,
                       s32 cmdMsgCnt);
OSMesgQueue *osPiGetCmdQueue(void);
s32 osPiWriteIo(u32 devAddr, u32 data);
s32 osPiReadIo(u32 devAddr, u32 *data);
s32 osPiRawStartDma(s32 direction, u32 devAddr, void *dramAddr, u32 size);
s32 osEPiRawStartDma(OSPiHandle *pihandle, s32 direction, u32 devAddr, void *dramAddr, u32 size);
void __osDevMgrMain(void *args);
extern u32 osDpGetStatus(void);
extern void osDpSetStatus(u32);
extern void osDpGetCounters(u32 *);
extern s32 osDpSetNextBuffer(void *, u64);
void osCreateThread(OSThread *thread, OSId id, void (*entry)(void *),
    void *arg, void *sp, OSPri pri);
OSId osGetThreadId(OSThread *thread);
OSPri osGetThreadPri(OSThread *thread);
void osSetThreadPri(OSThread *thread, OSPri pri);
void osStartThread(OSThread *thread);
void osStopThread(OSThread *thread);
typedef struct OSTimer_str
{
    struct OSTimer_str *next;
    struct OSTimer_str *prev;
    u64 interval;
    u64 remaining;
    OSMesgQueue *mq;
    OSMesg *msg;
} OSTimer;
typedef u64 OSTime;
OSTime osGetTime(void);
void osSetTime(OSTime time);
int osSetTimer(OSTimer *t, OSTime value, OSTime interval, OSMesgQueue *mq, OSMesg msg);
typedef u32 OSPageMask;
extern void osMapTLB(s32, OSPageMask, void *, u32, u32, s32);
extern void osMapTLBRdb(void);
extern void osUnmapTLB(s32);
extern void osUnmapTLBAll(void);
extern void osSetTLBASID(s32);
typedef struct
{
    u32 ctrl;
    u32 width;
    u32 burst;
    u32 vSync;
    u32 hSync;
    u32 leap;
    u32 hStart;
    u32 xScale;
    u32 vCurrent;
} OSViCommonRegs;
typedef struct
{
    u32 origin;
    u32 yScale;
    u32 vStart;
    u32 vBurst;
    u32 vIntr;
} OSViFieldRegs;
typedef struct
{
    u8 type;
    OSViCommonRegs comRegs;
    OSViFieldRegs fldRegs[2];
} OSViMode;
typedef struct
{
               u16 unk00;
               u16 retraceCount;
               void* buffer;
               OSViMode *unk08;
               u32 features;
               OSMesgQueue *mq;
               OSMesg *msg;
               u32 unk18;
               u32 unk1c;
               u32 unk20;
               f32 unk24;
               u16 unk28;
               u32 unk2c;
} OSViContext;
typedef struct
{
              f32 factor;
              u16 offset;
              u32 scale;
} __OSViScale;
typedef struct
{
              u16 state;
              u16 retraceCount;
              void *framep;
              OSViMode *modep;
              u32 control;
               OSMesgQueue *msgq;
               OSMesg msg;
               __OSViScale x;
               __OSViScale y;
} __OSViContext;
void osCreateViManager(OSPri pri);
void osViSetMode(OSViMode *mode);
void osViSetEvent(OSMesgQueue *mq, OSMesg msg, u32 retraceCount);
void osViBlack(u8 active);
void osViSetSpecialFeatures(u32 func);
void osViSwapBuffer(void *vaddr);
void osViSetXScale(f32 scale);
void osViSetYScale(f32 scale);
extern OSViMode osViModeTable[];
typedef struct
{
             u32 type;
             u32 flags;
             u64 *ucode_boot;
             u32 ucode_boot_size;
             u64 *ucode;
             u32 ucode_size;
             u64 *ucode_data;
             u32 ucode_data_size;
             u64 *dram_stack;
             u32 dram_stack_size;
             u64 *output_buff;
             u64 *output_buff_size;
             u64 *data_ptr;
             u32 data_size;
             u64 *yield_data_ptr;
             u32 yield_data_size;
} OSTask_t;
typedef union {
    OSTask_t t;
    long long int force_structure_alignment;
} OSTask;
typedef u32 OSYieldResult;
void osSpTaskLoad(OSTask *task);
void osSpTaskStartGo(OSTask *task);
void osSpTaskYield(void);
OSYieldResult osSpTaskYielded(OSTask *task);
extern u64 rspF3DBootStart[], rspF3DBootEnd[];
extern u64 rspF3DStart[], rspF3DEnd[];
extern u64 rspF3DDataStart[], rspF3DDataEnd[];
extern u64 rspAspMainStart[], rspAspMainEnd[];
extern u64 rspAspMainDataStart[], rspAspMainDataEnd[];
typedef struct {
    short type;
    char misc[30];
} OSScMsg;
typedef struct OSScTask_s {
    struct OSScTask_s *next;
    u32 state;
    u32 flags;
    void *framebuffer;
    OSTask list;
    OSMesgQueue *msgQ;
    OSMesg msg;
} OSScTask;
typedef struct SCClient_s {
    struct SCClient_s *next;
    OSMesgQueue *msgQ;
} OSScClient;
typedef struct {
    OSScMsg retraceMsg;
    OSScMsg prenmiMsg;
    OSMesgQueue interruptQ;
    OSMesg intBuf[8];
    OSMesgQueue cmdQ;
    OSMesg cmdMsgBuf[8];
    OSThread *thread;
    OSScClient *clientList;
    OSScTask *audioListHead;
    OSScTask *gfxListHead;
    OSScTask *audioListTail;
    OSScTask *gfxListTail;
    OSScTask *curRSPTask;
    OSScTask *curRDPTask;
    u32 frameCount;
    s32 doAudio;
} OSSched;
void osCreateScheduler(OSSched *s, void *stack, u8 mode, u32 numFields);
void osScAddClient(OSSched *sc, OSScClient *c, OSMesgQueue *msgQ, OSScClient *next);
void osScRemoveClient(OSSched *s, OSScClient *c);
OSMesgQueue *osScGetCmdQ(OSSched *s);
ALVoiceState *__mapVoice(ALSeqPlayer *, u8, u8, u8);
void __unmapVoice(ALSeqPlayer *seqp, ALVoice *voice);
char __n_voiceNeedsNoteKill(ALSeqPlayer *seqp, ALVoice *voice, ALMicroTime killTime);
ALVoiceState *__lookupVoice(ALSeqPlayer *, u8, u8);
ALSound *__lookupSound(ALSeqPlayer *, u8, u8, u8);
ALSound *__lookupSoundQuick(ALSeqPlayer *, u8, u8, u8);
s16 __vsVol(ALVoiceState *voice, ALSeqPlayer *seqp);
ALMicroTime __n_vsDelta(ALVoiceState *voice, ALMicroTime t);
ALPan __vsPan(ALVoiceState *voice, ALSeqPlayer *seqp);
void __n_initFromBank(ALSeqPlayer *seqp, ALBank *b);
void __initChanState(ALSeqPlayer *seqp);
void __n_resetPerfChanState(ALSeqPlayer *seqp, s32 chan);
void __n_setInstChanState(ALSeqPlayer *seqp, ALInstrument *inst, s32 chan);
void __seqpPrintVoices(ALSeqPlayer *);
void __seqpReleaseVoice(ALSeqPlayer *seqp, ALVoice *voice,
                                           ALMicroTime deltaTime);
void __n_seqpStopOsc(ALSeqPlayer *seqp, ALVoiceState *vs);
void __postNextSeqEvent(ALSeqPlayer *seqp);
enum {
    AL_FILTER_FREE_VOICE,
    AL_FILTER_SET_SOURCE,
    AL_FILTER_ADD_SOURCE,
    AL_FILTER_ADD_UPDATE,
    AL_FILTER_RESET,
    AL_FILTER_SET_WAVETABLE,
    AL_FILTER_SET_DRAM,
    AL_FILTER_SET_PITCH,
    AL_FILTER_SET_UNITY_PITCH,
    AL_FILTER_START,
    AL_FILTER_SET_STATE,
    AL_FILTER_SET_VOLUME,
    AL_FILTER_SET_PAN,
    AL_FILTER_START_VOICE_ALT,
    AL_FILTER_START_VOICE,
    AL_FILTER_STOP_VOICE,
    AL_FILTER_SET_FXAMT
};
enum {
    AL_ADPCM,
    AL_RESAMPLE,
    AL_BUFFER,
    AL_SAVE,
    AL_ENVMIX,
    AL_FX,
    AL_AUXBUS,
    AL_MAINBUS
};
typedef struct ALParam_s {
    struct ALParam_s *next;
    s32 delta;
    s16 type;
    union {
        f32 f;
        s32 i;
    } data;
    union {
        f32 f;
        s32 i;
    } moredata;
    union {
        f32 f;
        s32 i;
    } stillmoredata;
    union {
        f32 f;
        s32 i;
    } yetstillmoredata;
} ALParam;
typedef struct {
    struct ALParam_s *next;
    s32 delta;
    s16 type;
    s16 unity;
    f32 pitch;
    s16 volume;
    ALPan pan;
    u8 fxMix;
    s32 samples;
    struct ALWaveTable_s *wave;
} ALStartParamAlt;
typedef struct {
    struct ALParam_s *next;
    s32 delta;
    s16 type;
    s16 unity;
    struct ALWaveTable_s *wave;
} ALStartParam;
typedef struct {
    struct ALParam_s *next;
    s32 delta;
    s16 type;
    struct PVoice_s *pvoice;
} ALFreeParam;
typedef Acmd *(*ALCmdHandler)(void *, s16 *, s32, s32, Acmd *);
typedef s32 (*ALSetParam)(void *, s32, void *);
typedef struct ALFilter_s {
    struct ALFilter_s *source;
    ALCmdHandler handler;
    ALSetParam setParam;
    s16 inp;
    s16 outp;
    s32 type;
} ALFilter;
void alFilterNew(ALFilter *f, ALCmdHandler h, ALSetParam s, s32 type);
typedef struct {
    ALFilter filter;
    ADPCM_STATE *state;
    ADPCM_STATE *lstate;
    ALRawLoop loop;
    struct ALWaveTable_s *table;
    s32 bookSize;
    ALDMAproc dma;
    void *dmaState;
    s32 sample;
    s32 lastsam;
    s32 first;
    s32 memin;
} ALLoadFilter;
void alLoadNew(ALLoadFilter *f, ALDMANew dma, ALHeap *hp);
Acmd *alAdpcmPull(void *f, s16 *outp, s32 byteCount, s32 sampleOffset, Acmd *p);
Acmd *alRaw16Pull(void *f, s16 *outp, s32 byteCount, s32 sampleOffset, Acmd *p);
s32 alLoadParam(void *filter, s32 paramID, void *param);
typedef struct ALResampler_s {
    ALFilter filter;
    RESAMPLE_STATE *state;
    f32 ratio;
    s32 upitch;
    f32 delta;
    s32 first;
    ALParam *ctrlList;
    ALParam *ctrlTail;
    s32 motion;
} ALResampler;
typedef struct {
    s16 fc;
    s16 fgain;
    union {
        s16 fccoef[16];
        s64 force_aligned;
    } fcvec;
    POLEF_STATE *fstate;
    s32 first;
} ALLowPass;
typedef struct {
    u32 input;
    u32 output;
    s16 ffcoef;
    s16 fbcoef;
    s16 gain;
    f32 rsinc;
    f32 rsval;
    s32 rsdelta;
    f32 rsgain;
    ALLowPass *lp;
    ALResampler *rs;
} ALDelay;
typedef s32 (*ALSetFXParam)(void *, s32, void *);
typedef struct {
    struct ALFilter_s filter;
    s16 *base;
    s16 *input;
    u32 length;
    ALDelay *delay;
    u8 section_count;
    ALSetFXParam paramHdl;
} ALFx;
void alFxNew(ALFx *r, ALSynConfig *c, ALHeap *hp);
Acmd *alFxPull(void *f, s16 *outp, s32 out, s32 sampleOffset, Acmd *p);
s32 alFxParam(void *filter, s32 paramID, void *param);
s32 alFxParamHdl(void *filter, s32 paramID, void *param);
typedef struct ALMainBus_s {
    ALFilter filter;
    s32 sourceCount;
    s32 maxSources;
    ALFilter **sources;
} ALMainBus;
void alMainBusNew(ALMainBus *m, void *ptr, s32 len);
Acmd *alMainBusPull(void *f, s16 *outp, s32 outCount, s32 sampleOffset, Acmd *p);
s32 alMainBusParam(void *filter, s32 paramID, void *param);
typedef struct ALAuxBus_s {
    ALFilter filter;
    s32 sourceCount;
    s32 maxSources;
    ALFilter **sources;
    ALFx fx[1];
} ALAuxBus;
void alAuxBusNew(ALAuxBus *m, void *ptr, s32 len);
Acmd *alAuxBusPull(void *f, s16 *outp, s32 outCount, s32 sampleOffset, Acmd *p);
s32 alAuxBusParam(void *filter, s32 paramID, void *param);
void alResampleNew(ALResampler *r, ALHeap *hp);
Acmd *alResamplePull(void *f, s16 *outp, s32 out, s32 sampleOffset, Acmd *p);
s32 alResampleParam(void *f, s32 paramID, void *param);
typedef struct ALSave_s {
    ALFilter filter;
    s32 dramout;
    s32 first;
} ALSave;
void alSaveNew(ALSave *r);
Acmd *alSavePull(void *f, s16 *outp, s32 outCount, s32 sampleOffset, Acmd *p);
s32 alSaveParam(void *f, s32 paramID, void *param);
typedef struct ALEnvMixer_s {
 u8 unk00[0x40];
 ALFilter filter;
 ENVMIX_STATE *state;
 s16 pan;
 s16 volume;
 s16 cvolL;
 s16 cvolR;
 s16 dryamt;
 s16 wetamt;
 u16 lratl;
 s16 lratm;
 s16 ltgt;
 u16 rratl;
 s16 rratm;
 s16 rtgt;
 s32 delta;
 s32 segEnd;
 s32 first;
 ALParam *ctrlList;
 ALParam *ctrlTail;
 s32 motion;
} ALEnvMixer;
void alEnvmixerNew(ALEnvMixer *e, ALHeap *hp);
Acmd *alEnvmixerPull(void *f, s16 *outp, s32 out, s32 sampleOffset, Acmd *p);
s32 alEnvmixerParam(void *filter, s32 paramID, void *param);
typedef struct {
    s32 magic;
    s32 size;
    u8 *file;
    s32 line;
    s32 count;
    s32 pad0;
    s32 pad1;
    s32 pad2;
} HeapInfo;
typedef struct PVoice_s {
    ALLink node;
    struct ALVoice_s *vvoice;
    ALFilter *channelKnob;
    ALLoadFilter decoder;
    ALResampler resampler;
    ALEnvMixer envmixer;
    s32 offset;
} PVoice;
ALParam *__allocParam(void);
void __freeParam(ALParam *param);
void _freePVoice(PVoice *pvoice);
void _collectPVoices(void);
s32 _timeToSamples(s32 micros);
ALMicroTime _samplesToTime(ALSynth *synth, s32 samples);
typedef int __int32_t;
typedef unsigned __uint32_t;
typedef struct {
 int hi32;
 int lo32;
} __int64_t;
typedef struct {
 unsigned int hi32;
 unsigned int lo32;
} __uint64_t;
typedef __int32_t __scint_t;
typedef __uint32_t __scunsigned_t;
enum animnum {
 ANIM_IDLE,
 ANIM_TWO_GUN_HOLD,
 ANIM_0002,
 ANIM_0003,
 ANIM_0004,
 ANIM_0005,
 ANIM_0006,
 ANIM_0007,
 ANIM_KNEEL_TWO_HANDED_GUN,
 ANIM_0009,
 ANIM_000A,
 ANIM_000B,
 ANIM_000C,
 ANIM_000D,
 ANIM_000E,
 ANIM_000F,
 ANIM_0010,
 ANIM_0011,
 ANIM_0012,
 ANIM_0013,
 ANIM_0014,
 ANIM_0015,
 ANIM_0016,
 ANIM_0017,
 ANIM_0018,
 ANIM_0019,
 ANIM_DEATH_001A,
 ANIM_001B,
 ANIM_DEATH_001C,
 ANIM_001D,
 ANIM_001E,
 ANIM_001F,
 ANIM_DEATH_0020,
 ANIM_DEATH_0021,
 ANIM_DEATH_0022,
 ANIM_DEATH_0023,
 ANIM_DEATH_0024,
 ANIM_DEATH_0025,
 ANIM_0026,
 ANIM_0027,
 ANIM_0028,
 ANIM_0029,
 ANIM_RUNNING_TWOHANDGUN,
 ANIM_002B,
 ANIM_002C,
 ANIM_002D,
 ANIM_SURRENDER_002E,
 ANIM_SURRENDER_002F,
 ANIM_0030,
 ANIM_0031,
 ANIM_0032,
 ANIM_0033,
 ANIM_0034,
 ANIM_0035,
 ANIM_0036,
 ANIM_0037,
 ANIM_0038,
 ANIM_DEATH_STOMACH_LONG,
 ANIM_003A,
 ANIM_003B,
 ANIM_003C,
 ANIM_TALKING_003D,
 ANIM_THROWGRENADE_STANDING,
 ANIM_003F,
 ANIM_0040,
 ANIM_0041,
 ANIM_0042,
 ANIM_0043,
 ANIM_0044,
 ANIM_0045,
 ANIM_0046,
 ANIM_0047,
 ANIM_0048,
 ANIM_0049,
 ANIM_004A,
 ANIM_KNEEL_SHOOT_RIGHT_HAND,
 ANIM_004C,
 ANIM_004D,
 ANIM_004E,
 ANIM_004F,
 ANIM_0050,
 ANIM_0051,
 ANIM_0052,
 ANIM_0053,
 ANIM_0054,
 ANIM_0055,
 ANIM_0056,
 ANIM_0057,
 ANIM_0058,
 ANIM_RUNNING_ONEHANDGUN,
 ANIM_005A,
 ANIM_005B,
 ANIM_005C,
 ANIM_005D,
 ANIM_005E,
 ANIM_005F,
 ANIM_0060,
 ANIM_0061,
 ANIM_0062,
 ANIM_0063,
 ANIM_0064,
 ANIM_0065,
 ANIM_0066,
 ANIM_0067,
 ANIM_0068,
 ANIM_0069,
 ANIM_006A,
 ANIM_006B,
 ANIM_006C,
 ANIM_006D,
 ANIM_006E,
 ANIM_006F,
 ANIM_0070,
 ANIM_0071,
 ANIM_0072,
 ANIM_0073,
 ANIM_0074,
 ANIM_0075,
 ANIM_0076,
 ANIM_0077,
 ANIM_0078,
 ANIM_0079,
 ANIM_007A,
 ANIM_007B,
 ANIM_007C,
 ANIM_007D,
 ANIM_007E,
 ANIM_007F,
 ANIM_0080,
 ANIM_0081,
 ANIM_0082,
 ANIM_0083,
 ANIM_0084,
 ANIM_0085,
 ANIM_0086,
 ANIM_0087,
 ANIM_0088,
 ANIM_0089,
 ANIM_008A,
 ANIM_008B,
 ANIM_008C,
 ANIM_008D,
 ANIM_008E,
 ANIM_008F,
 ANIM_0090,
 ANIM_0091,
 ANIM_0092,
 ANIM_0093,
 ANIM_0094,
 ANIM_0095,
 ANIM_0096,
 ANIM_0097,
 ANIM_TALKING_0098,
 ANIM_0099,
 ANIM_YAWN,
 ANIM_SCRATCH_HEAD,
 ANIM_ROLL_HEAD,
 ANIM_GRAB_CROTCH,
 ANIM_GRAB_BUTT,
 ANIM_SNEEZE,
 ANIM_TALKING_00A0,
 ANIM_00A1,
 ANIM_00A2,
 ANIM_TALKING_00A3,
 ANIM_00A4,
 ANIM_00A5,
 ANIM_00A6,
 ANIM_00A7,
 ANIM_00A8,
 ANIM_00A9,
 ANIM_00AA,
 ANIM_00AB,
 ANIM_PUSH_BUTTON,
 ANIM_00AD,
 ANIM_00AE,
 ANIM_STANDING_TYPE_ONE_HAND,
 ANIM_00B0,
 ANIM_00B1,
 ANIM_00B2,
 ANIM_00B3,
 ANIM_00B4,
 ANIM_00B5,
 ANIM_00B6,
 ANIM_00B7,
 ANIM_00B8,
 ANIM_00B9,
 ANIM_00BA,
 ANIM_00BB,
 ANIM_00BC,
 ANIM_00BD,
 ANIM_00BE,
 ANIM_00BF,
 ANIM_00C0,
 ANIM_00C1,
 ANIM_00C2,
 ANIM_00C3,
 ANIM_00C4,
 ANIM_00C5,
 ANIM_00C6,
 ANIM_00C7,
 ANIM_00C8,
 ANIM_00C9,
 ANIM_00CA,
 ANIM_00CB,
 ANIM_00CC,
 ANIM_00CD,
 ANIM_00CE,
 ANIM_00CF,
 ANIM_00D0,
 ANIM_00D1,
 ANIM_00D2,
 ANIM_00D3,
 ANIM_00D4,
 ANIM_00D5,
 ANIM_00D6,
 ANIM_00D7,
 ANIM_00D8,
 ANIM_00D9,
 ANIM_00DA,
 ANIM_00DB,
 ANIM_00DC,
 ANIM_00DD,
 ANIM_00DE,
 ANIM_00DF,
 ANIM_00E0,
 ANIM_00E1,
 ANIM_00E2,
 ANIM_00E3,
 ANIM_00E4,
 ANIM_00E5,
 ANIM_00E6,
 ANIM_00E7,
 ANIM_00E8,
 ANIM_00E9,
 ANIM_00EA,
 ANIM_00EB,
 ANIM_00EC,
 ANIM_00ED,
 ANIM_00EE,
 ANIM_00EF,
 ANIM_00F0,
 ANIM_00F1,
 ANIM_00F2,
 ANIM_00F3,
 ANIM_00F4,
 ANIM_00F5,
 ANIM_00F6,
 ANIM_00F7,
 ANIM_00F8,
 ANIM_00F9,
 ANIM_00FA,
 ANIM_00FB,
 ANIM_00FC,
 ANIM_00FD,
 ANIM_00FE,
 ANIM_00FF,
 ANIM_0100,
 ANIM_0101,
 ANIM_0102,
 ANIM_0103,
 ANIM_0104,
 ANIM_0105,
 ANIM_0106,
 ANIM_0107,
 ANIM_0108,
 ANIM_0109,
 ANIM_010A,
 ANIM_010B,
 ANIM_010C,
 ANIM_010D,
 ANIM_010E,
 ANIM_010F,
 ANIM_0110,
 ANIM_0111,
 ANIM_0112,
 ANIM_0113,
 ANIM_0114,
 ANIM_0115,
 ANIM_0116,
 ANIM_0117,
 ANIM_0118,
 ANIM_0119,
 ANIM_011A,
 ANIM_011B,
 ANIM_011C,
 ANIM_011D,
 ANIM_011E,
 ANIM_011F,
 ANIM_0120,
 ANIM_0121,
 ANIM_0122,
 ANIM_0123,
 ANIM_0124,
 ANIM_0125,
 ANIM_0126,
 ANIM_0127,
 ANIM_0128,
 ANIM_0129,
 ANIM_012A,
 ANIM_012B,
 ANIM_012C,
 ANIM_012D,
 ANIM_012E,
 ANIM_012F,
 ANIM_0130,
 ANIM_0131,
 ANIM_0132,
 ANIM_0133,
 ANIM_0134,
 ANIM_0135,
 ANIM_0136,
 ANIM_0137,
 ANIM_0138,
 ANIM_0139,
 ANIM_013A,
 ANIM_013B,
 ANIM_013C,
 ANIM_013D,
 ANIM_013E,
 ANIM_013F,
 ANIM_0140,
 ANIM_0141,
 ANIM_0142,
 ANIM_0143,
 ANIM_0144,
 ANIM_0145,
 ANIM_0146,
 ANIM_0147,
 ANIM_0148,
 ANIM_0149,
 ANIM_014A,
 ANIM_014B,
 ANIM_014C,
 ANIM_014D,
 ANIM_014E,
 ANIM_014F,
 ANIM_0150,
 ANIM_0151,
 ANIM_0152,
 ANIM_0153,
 ANIM_0154,
 ANIM_0155,
 ANIM_0156,
 ANIM_0157,
 ANIM_0158,
 ANIM_0159,
 ANIM_015A,
 ANIM_015B,
 ANIM_015C,
 ANIM_015D,
 ANIM_015E,
 ANIM_015F,
 ANIM_0160,
 ANIM_0161,
 ANIM_0162,
 ANIM_0163,
 ANIM_0164,
 ANIM_0165,
 ANIM_0166,
 ANIM_0167,
 ANIM_0168,
 ANIM_0169,
 ANIM_016A,
 ANIM_016B,
 ANIM_016C,
 ANIM_016D,
 ANIM_016E,
 ANIM_016F,
 ANIM_0170,
 ANIM_0171,
 ANIM_0172,
 ANIM_0173,
 ANIM_0174,
 ANIM_0175,
 ANIM_0176,
 ANIM_0177,
 ANIM_0178,
 ANIM_0179,
 ANIM_017A,
 ANIM_017B,
 ANIM_017C,
 ANIM_017D,
 ANIM_017E,
 ANIM_017F,
 ANIM_0180,
 ANIM_0181,
 ANIM_0182,
 ANIM_0183,
 ANIM_0184,
 ANIM_0185,
 ANIM_0186,
 ANIM_0187,
 ANIM_0188,
 ANIM_0189,
 ANIM_018A,
 ANIM_018B,
 ANIM_018C,
 ANIM_018D,
 ANIM_018E,
 ANIM_018F,
 ANIM_0190,
 ANIM_0191,
 ANIM_0192,
 ANIM_0193,
 ANIM_0194,
 ANIM_0195,
 ANIM_0196,
 ANIM_0197,
 ANIM_0198,
 ANIM_0199,
 ANIM_019A,
 ANIM_019B,
 ANIM_019C,
 ANIM_019D,
 ANIM_019E,
 ANIM_019F,
 ANIM_01A0,
 ANIM_01A1,
 ANIM_01A2,
 ANIM_01A3,
 ANIM_01A4,
 ANIM_01A5,
 ANIM_01A6,
 ANIM_01A7,
 ANIM_01A8,
 ANIM_01A9,
 ANIM_01AA,
 ANIM_01AB,
 ANIM_01AC,
 ANIM_01AD,
 ANIM_01AE,
 ANIM_01AF,
 ANIM_01B0,
 ANIM_01B1,
 ANIM_01B2,
 ANIM_01B3,
 ANIM_01B4,
 ANIM_01B5,
 ANIM_01B6,
 ANIM_01B7,
 ANIM_01B8,
 ANIM_01B9,
 ANIM_01BA,
 ANIM_01BB,
 ANIM_01BC,
 ANIM_01BD,
 ANIM_01BE,
 ANIM_01BF,
 ANIM_01C0,
 ANIM_01C1,
 ANIM_01C2,
 ANIM_01C3,
 ANIM_01C4,
 ANIM_01C5,
 ANIM_01C6,
 ANIM_01C7,
 ANIM_01C8,
 ANIM_01C9,
 ANIM_01CA,
 ANIM_01CB,
 ANIM_01CC,
 ANIM_01CD,
 ANIM_01CE,
 ANIM_01CF,
 ANIM_01D0,
 ANIM_01D1,
 ANIM_01D2,
 ANIM_01D3,
 ANIM_01D4,
 ANIM_01D5,
 ANIM_01D6,
 ANIM_01D7,
 ANIM_01D8,
 ANIM_01D9,
 ANIM_01DA,
 ANIM_01DB,
 ANIM_01DC,
 ANIM_01DD,
 ANIM_01DE,
 ANIM_01DF,
 ANIM_01E0,
 ANIM_01E1,
 ANIM_01E2,
 ANIM_01E3,
 ANIM_01E4,
 ANIM_01E5,
 ANIM_01E6,
 ANIM_01E7,
 ANIM_01E8,
 ANIM_01E9,
 ANIM_01EA,
 ANIM_01EB,
 ANIM_01EC,
 ANIM_01ED,
 ANIM_01EE,
 ANIM_01EF,
 ANIM_01F0,
 ANIM_01F1,
 ANIM_01F2,
 ANIM_01F3,
 ANIM_01F4,
 ANIM_COWER_01F5,
 ANIM_01F6,
 ANIM_01F7,
 ANIM_01F8,
 ANIM_01F9,
 ANIM_STAND_UP_FROM_SITTING,
 ANIM_SITTING_TYPING,
 ANIM_01FC,
 ANIM_01FD,
 ANIM_01FE,
 ANIM_RELOAD,
 ANIM_0200,
 ANIM_DONT_SHOOT,
 ANIM_SURPRISED_0202,
 ANIM_0203,
 ANIM_OPERATE_0204,
 ANIM_OPERATE_0205,
 ANIM_OPERATE_0206,
 ANIM_SMOKE_CIGARETTE,
 ANIM_0208,
 ANIM_RELOAD_0209,
 ANIM_020A,
 ANIM_020B,
 ANIM_020C,
 ANIM_020D,
 ANIM_020E,
 ANIM_020F,
 ANIM_0210,
 ANIM_0211,
 ANIM_0212,
 ANIM_0213,
 ANIM_0214,
 ANIM_0215,
 ANIM_0216,
 ANIM_0217,
 ANIM_0218,
 ANIM_0219,
 ANIM_021A,
 ANIM_021B,
 ANIM_021C,
 ANIM_021D,
 ANIM_021E,
 ANIM_021F,
 ANIM_0220,
 ANIM_OPERATE_0221,
 ANIM_OPERATE_0222,
 ANIM_OPERATE_0223,
 ANIM_0224,
 ANIM_0225,
 ANIM_WALK_BACKWARDS,
 ANIM_SITTING_DORMANT,
 ANIM_BLINDED,
 ANIM_COWER_0229,
 ANIM_022A,
 ANIM_022B,
 ANIM_022C,
 ANIM_022D,
 ANIM_022E,
 ANIM_022F,
 ANIM_0230,
 ANIM_TALKING_0231,
 ANIM_TALKING_0232,
 ANIM_TALKING_0233,
 ANIM_TALKING_0234,
 ANIM_0235,
 ANIM_0236,
 ANIM_0237,
 ANIM_0238,
 ANIM_0239,
 ANIM_023A,
 ANIM_HEAD_ROLL,
 ANIM_023C,
 ANIM_PICK_UP_GUN,
 ANIM_023E,
 ANIM_BIG_SNEEZE,
 ANIM_0240,
 ANIM_0241,
 ANIM_THROWGRENADE_NOPIN,
 ANIM_0243,
 ANIM_THROWGRENADE_CROUCHING,
 ANIM_DRAW_PISTOL_0245,
 ANIM_0246,
 ANIM_0247,
 ANIM_0248,
 ANIM_0249,
 ANIM_024A,
 ANIM_024B,
 ANIM_024C,
 ANIM_024D,
 ANIM_024E,
 ANIM_024F,
 ANIM_0250,
 ANIM_0251,
 ANIM_0252,
 ANIM_0253,
 ANIM_0254,
 ANIM_0255,
 ANIM_0256,
 ANIM_0257,
 ANIM_0258,
 ANIM_0259,
 ANIM_025A,
 ANIM_LOOK_AROUND_025B,
 ANIM_LOOK_AROUND_025C,
 ANIM_LOOK_AROUND_025D,
 ANIM_LOOK_AROUND_025E,
 ANIM_LOOK_AROUND_FRANTIC,
 ANIM_0260,
 ANIM_SPECIALDIE_FALLBACK,
 ANIM_SPECIALDIE_ROLL1,
 ANIM_SPECIALDIE_ROLL2,
 ANIM_0264,
 ANIM_SITTING_0265,
 ANIM_0266,
 ANIM_SPECIALDIE_ROLL3,
 ANIM_SPECIALDIE_OVERRAILING,
 ANIM_SNIPING_GETDOWN,
 ANIM_SNIPING_ONGROUND,
 ANIM_SNIPING_GETUP,
 ANIM_SNIPING_DIE,
 ANIM_026D,
 ANIM_026E,
 ANIM_026F,
 ANIM_0270,
 ANIM_0271,
 ANIM_0272,
 ANIM_0273,
 ANIM_0274,
 ANIM_0275,
 ANIM_0276,
 ANIM_0277,
 ANIM_0278,
 ANIM_0279,
 ANIM_027A,
 ANIM_027B,
 ANIM_027C,
 ANIM_027D,
 ANIM_027E,
 ANIM_027F,
 ANIM_0280,
 ANIM_0281,
 ANIM_0282,
 ANIM_0283,
 ANIM_0284,
 ANIM_0285,
 ANIM_0286,
 ANIM_0287,
 ANIM_DRAW_PISTOL_0288,
 ANIM_DRAW_PISTOL_0289,
 ANIM_028A,
 ANIM_028B,
 ANIM_028C,
 ANIM_028D,
 ANIM_028E,
 ANIM_028F,
 ANIM_0290,
 ANIM_0291,
 ANIM_0292,
 ANIM_0293,
 ANIM_0294,
 ANIM_0295,
 ANIM_0296,
 ANIM_0297,
 ANIM_0298,
 ANIM_FIX_GUN_JAM_EASY,
 ANIM_FIX_GUN_JAM_HARD,
 ANIM_029B,
 ANIM_029C,
 ANIM_029D,
 ANIM_029E,
 ANIM_029F,
 ANIM_02A0,
 ANIM_02A1,
 ANIM_02A2,
 ANIM_02A3,
 ANIM_02A4,
 ANIM_02A5,
 ANIM_02A6,
 ANIM_02A7,
 ANIM_02A8,
 ANIM_02A9,
 ANIM_02AA,
 ANIM_02AB,
 ANIM_02AC,
 ANIM_02AD,
 ANIM_02AE,
 ANIM_02AF,
 ANIM_02B0,
 ANIM_02B1,
 ANIM_02B2,
 ANIM_02B3,
 ANIM_02B4,
 ANIM_02B5,
 ANIM_02B6,
 ANIM_02B7,
 ANIM_02B8,
 ANIM_02B9,
 ANIM_02BA,
 ANIM_02BB,
 ANIM_02BC,
 ANIM_02BD,
 ANIM_02BE,
 ANIM_02BF,
 ANIM_02C0,
 ANIM_02C1,
 ANIM_02C2,
 ANIM_02C3,
 ANIM_02C4,
 ANIM_02C5,
 ANIM_02C6,
 ANIM_02C7,
 ANIM_02C8,
 ANIM_02C9,
 ANIM_02CA,
 ANIM_02CB,
 ANIM_02CC,
 ANIM_02CD,
 ANIM_02CE,
 ANIM_02CF,
 ANIM_02D0,
 ANIM_02D1,
 ANIM_02D2,
 ANIM_02D3,
 ANIM_02D4,
 ANIM_02D5,
 ANIM_02D6,
 ANIM_02D7,
 ANIM_02D8,
 ANIM_02D9,
 ANIM_02DA,
 ANIM_02DB,
 ANIM_02DC,
 ANIM_02DD,
 ANIM_02DE,
 ANIM_02DF,
 ANIM_02E0,
 ANIM_02E1,
 ANIM_02E2,
 ANIM_02E3,
 ANIM_02E4,
 ANIM_02E5,
 ANIM_02E6,
 ANIM_02E7,
 ANIM_02E8,
 ANIM_02E9,
 ANIM_02EA,
 ANIM_02EB,
 ANIM_02EC,
 ANIM_02ED,
 ANIM_02EE,
 ANIM_02EF,
 ANIM_02F0,
 ANIM_02F1,
 ANIM_02F2,
 ANIM_02F3,
 ANIM_02F4,
 ANIM_02F5,
 ANIM_02F6,
 ANIM_02F7,
 ANIM_02F8,
 ANIM_02F9,
 ANIM_02FA,
 ANIM_02FB,
 ANIM_02FC,
 ANIM_02FD,
 ANIM_02FE,
 ANIM_02FF,
 ANIM_0300,
 ANIM_0301,
 ANIM_0302,
 ANIM_0303,
 ANIM_0304,
 ANIM_0305,
 ANIM_0306,
 ANIM_0307,
 ANIM_0308,
 ANIM_0309,
 ANIM_030A,
 ANIM_030B,
 ANIM_030C,
 ANIM_030D,
 ANIM_030E,
 ANIM_030F,
 ANIM_0310,
 ANIM_0311,
 ANIM_0312,
 ANIM_0313,
 ANIM_0314,
 ANIM_0315,
 ANIM_0316,
 ANIM_0317,
 ANIM_0318,
 ANIM_0319,
 ANIM_031A,
 ANIM_031B,
 ANIM_031C,
 ANIM_031D,
 ANIM_031E,
 ANIM_031F,
 ANIM_0320,
 ANIM_0321,
 ANIM_0322,
 ANIM_0323,
 ANIM_0324,
 ANIM_0325,
 ANIM_0326,
 ANIM_0327,
 ANIM_0328,
 ANIM_0329,
 ANIM_032A,
 ANIM_032B,
 ANIM_032C,
 ANIM_032D,
 ANIM_032E,
 ANIM_032F,
 ANIM_0330,
 ANIM_0331,
 ANIM_0332,
 ANIM_0333,
 ANIM_0334,
 ANIM_0335,
 ANIM_0336,
 ANIM_0337,
 ANIM_0338,
 ANIM_0339,
 ANIM_033A,
 ANIM_033B,
 ANIM_033C,
 ANIM_033D,
 ANIM_033E,
 ANIM_033F,
 ANIM_0340,
 ANIM_0341,
 ANIM_0342,
 ANIM_0343,
 ANIM_0344,
 ANIM_0345,
 ANIM_0346,
 ANIM_0347,
 ANIM_0348,
 ANIM_0349,
 ANIM_034A,
 ANIM_034B,
 ANIM_034C,
 ANIM_034D,
 ANIM_034E,
 ANIM_034F,
 ANIM_0350,
 ANIM_0351,
 ANIM_SKEDAR_COLLAPSE,
 ANIM_0353,
 ANIM_0354,
 ANIM_0355,
 ANIM_0356,
 ANIM_0357,
 ANIM_0358,
 ANIM_0359,
 ANIM_035A,
 ANIM_035B,
 ANIM_035C,
 ANIM_035D,
 ANIM_035E,
 ANIM_035F,
 ANIM_0360,
 ANIM_0361,
 ANIM_0362,
 ANIM_0363,
 ANIM_0364,
 ANIM_0365,
 ANIM_0366,
 ANIM_0367,
 ANIM_0368,
 ANIM_0369,
 ANIM_036A,
 ANIM_036B,
 ANIM_036C,
 ANIM_036D,
 ANIM_036E,
 ANIM_036F,
 ANIM_0370,
 ANIM_0371,
 ANIM_0372,
 ANIM_0373,
 ANIM_0374,
 ANIM_0375,
 ANIM_0376,
 ANIM_0377,
 ANIM_0378,
 ANIM_0379,
 ANIM_037A,
 ANIM_037B,
 ANIM_037C,
 ANIM_037D,
 ANIM_037E,
 ANIM_037F,
 ANIM_0380,
 ANIM_0381,
 ANIM_0382,
 ANIM_0383,
 ANIM_SKEDAR_JUMPSTART,
 ANIM_SKEDAR_JUMPAIR,
 ANIM_0386,
 ANIM_0387,
 ANIM_0388,
 ANIM_0389,
 ANIM_038A,
 ANIM_038B,
 ANIM_038C,
 ANIM_038D,
 ANIM_038E,
 ANIM_038F,
 ANIM_0390,
 ANIM_0391,
 ANIM_0392,
 ANIM_0393,
 ANIM_SKEDAR_RUNNING,
 ANIM_0395,
 ANIM_0396,
 ANIM_0397,
 ANIM_0398,
 ANIM_0399,
 ANIM_039A,
 ANIM_039B,
 ANIM_039C,
 ANIM_039D,
 ANIM_039E,
 ANIM_039F,
 ANIM_03A0,
 ANIM_03A1,
 ANIM_03A2,
 ANIM_03A3,
 ANIM_03A4,
 ANIM_03A5,
 ANIM_03A6,
 ANIM_03A7,
 ANIM_03A8,
 ANIM_03A9,
 ANIM_03AA,
 ANIM_03AB,
 ANIM_03AC,
 ANIM_03AD,
 ANIM_03AE,
 ANIM_03AF,
 ANIM_03B0,
 ANIM_03B1,
 ANIM_03B2,
 ANIM_03B3,
 ANIM_03B4,
 ANIM_03B5,
 ANIM_03B6,
 ANIM_03B7,
 ANIM_03B8,
 ANIM_03B9,
 ANIM_03BA,
 ANIM_03BB,
 ANIM_03BC,
 ANIM_03BD,
 ANIM_03BE,
 ANIM_03BF,
 ANIM_03C0,
 ANIM_03C1,
 ANIM_03C2,
 ANIM_03C3,
 ANIM_03C4,
 ANIM_03C5,
 ANIM_03C6,
 ANIM_03C7,
 ANIM_03C8,
 ANIM_03C9,
 ANIM_03CA,
 ANIM_03CB,
 ANIM_03CC,
 ANIM_03CD,
 ANIM_03CE,
 ANIM_03CF,
 ANIM_03D0,
 ANIM_03D1,
 ANIM_03D2,
 ANIM_03D3,
 ANIM_03D4,
 ANIM_03D5,
 ANIM_03D6,
 ANIM_03D7,
 ANIM_03D8,
 ANIM_03D9,
 ANIM_03DA,
 ANIM_03DB,
 ANIM_03DC,
 ANIM_03DD,
 ANIM_03DE,
 ANIM_03DF,
 ANIM_03E0,
 ANIM_03E1,
 ANIM_03E2,
 ANIM_03E3,
 ANIM_03E4,
 ANIM_03E5,
 ANIM_03E6,
 ANIM_03E7,
 ANIM_03E8,
 ANIM_03E9,
 ANIM_03EA,
 ANIM_03EB,
 ANIM_03EC,
 ANIM_03ED,
 ANIM_03EE,
 ANIM_03EF,
 ANIM_03F0,
 ANIM_03F1,
 ANIM_03F2,
 ANIM_03F3,
 ANIM_03F4,
 ANIM_03F5,
 ANIM_03F6,
 ANIM_03F7,
 ANIM_03F8,
 ANIM_03F9,
 ANIM_03FA,
 ANIM_03FB,
 ANIM_03FC,
 ANIM_03FD,
 ANIM_03FE,
 ANIM_03FF,
 ANIM_0400,
 ANIM_0401,
 ANIM_0402,
 ANIM_0403,
 ANIM_0404,
 ANIM_0405,
 ANIM_0406,
 ANIM_0407,
 ANIM_0408,
 ANIM_0409,
 ANIM_040A,
 ANIM_040B,
 ANIM_040C,
 ANIM_040D,
 ANIM_040E,
 ANIM_040F,
 ANIM_0410,
 ANIM_0411,
 ANIM_0412,
 ANIM_0413,
 ANIM_0414,
 ANIM_0415,
 ANIM_0416,
 ANIM_0417,
 ANIM_0418,
 ANIM_0419,
 ANIM_041A,
 ANIM_041B,
 ANIM_041C,
 ANIM_041D,
 ANIM_041E,
 ANIM_041F,
 ANIM_0420,
 ANIM_0421,
 ANIM_0422,
 ANIM_0423,
 ANIM_0424,
 ANIM_0425,
 ANIM_0426,
 ANIM_0427,
 ANIM_0428,
 ANIM_0429,
 ANIM_042A,
 ANIM_042B,
 ANIM_042C,
 ANIM_042D,
 ANIM_042E,
 ANIM_042F,
 ANIM_0430,
 ANIM_0431,
 ANIM_0432,
 ANIM_0433,
 ANIM_0434,
 ANIM_0435,
 ANIM_0436,
 ANIM_0437,
 ANIM_0438,
 ANIM_0439,
 ANIM_043A,
 ANIM_043B,
 ANIM_043C,
 ANIM_043D,
 ANIM_043E,
 ANIM_043F,
 ANIM_0440,
 ANIM_0441,
 ANIM_0442,
 ANIM_0443,
 ANIM_0444,
 ANIM_0445,
 ANIM_0446,
 ANIM_0447,
 ANIM_0448,
 ANIM_0449,
 ANIM_044A,
 ANIM_044B,
 ANIM_044C,
 ANIM_044D,
 ANIM_044E,
 ANIM_044F,
 ANIM_0450,
 ANIM_0451,
 ANIM_0452,
 ANIM_0453,
 ANIM_0454,
 ANIM_0455,
 ANIM_0456,
 ANIM_0457,
 ANIM_0458,
 ANIM_0459,
 ANIM_045A,
 ANIM_045B,
 ANIM_045C,
 ANIM_045D,
 ANIM_045E,
 ANIM_045F,
 ANIM_0460,
 ANIM_0461,
 ANIM_0462,
 ANIM_0463,
 ANIM_0464,
 ANIM_0465,
 ANIM_0466,
 ANIM_0467,
 ANIM_0468,
 ANIM_0469,
 ANIM_046A,
 ANIM_046B,
 ANIM_046C,
 ANIM_046D,
 ANIM_046E,
 ANIM_046F,
 ANIM_0470,
 ANIM_0471,
 ANIM_0472,
 ANIM_0473,
 ANIM_0474,
 ANIM_0475,
 ANIM_0476,
 ANIM_0477,
 ANIM_0478,
 ANIM_0479,
 ANIM_047A,
 ANIM_047B,
 ANIM_047C,
 ANIM_047D,
 ANIM_047E,
 ANIM_047F,
 ANIM_0480,
 ANIM_0481,
 ANIM_0482,
 ANIM_0483,
 ANIM_0484,
 ANIM_0485,
 ANIM_0486,
 ANIM_0487,
 ANIM_0488,
 ANIM_0489,
 ANIM_048A,
 ANIM_048B,
 ANIM_048C,
 ANIM_048D,
 ANIM_048E,
 ANIM_048F,
 ANIM_0490,
 ANIM_0491,
 ANIM_0492,
 ANIM_0493,
 ANIM_0494,
 ANIM_0495,
 ANIM_0496,
 ANIM_0497,
 ANIM_0498,
 ANIM_0499,
 ANIM_049A,
 ANIM_049B,
 ANIM_049C,
 ANIM_049D,
 ANIM_049E,
 ANIM_049F,
 ANIM_04A0,
 ANIM_04A1,
 ANIM_04A2,
 ANIM_04A3,
 ANIM_04A4,
 ANIM_04A5,
 ANIM_04A6,
 ANIM_04A7,
 ANIM_04A8,
 ANIM_04A9,
 ANIM_04AA,
 ANIM_04AB,
 ANIM_04AC,
 ANIM_04AD,
 ANIM_04AE,
 ANIM_04AF,
 ANIM_04B0,
 ANIM_04B1,
 ANIM_04B2,
 ANIM_04B3,
 ANIM_04B4,
 ANIM_04B5,
 ANIM_04B6,
 ANIM_END
};
enum music {
 MUSIC_NONE,
 MUSIC_TITLE2,
 MUSIC_EXTRACTION,
 MUSIC_PAUSEMENU,
 MUSIC_DEFENSE,
 MUSIC_INVESTIGATION_SFX,
 MUSIC_ESCAPE,
 MUSIC_DEEPSEA,
 MUSIC_DDTOWER_SFX,
 MUSIC_DEFECTION,
 MUSIC_DEATH_SOLO,
 MUSIC_DEFECTION_INTRO_SFX,
 MUSIC_VILLA,
 MUSIC_CI,
 MUSIC_CHICAGO,
 MUSIC_G5,
 MUSIC_DEFECTION_X,
 MUSIC_EXTRACTION_X,
 MUSIC_INVESTIGATION,
 MUSIC_INVESTIGATION_X,
 MUSIC_INFILTRATION,
 MUSIC_DEATH_BETA,
 MUSIC_RESCUE,
 MUSIC_AIRBASE,
 MUSIC_AIRFORCEONE,
 MUSIC_DEATH_MP,
 MUSIC_EXTRACTION_OUTRO_SFX,
 MUSIC_MISSION_UNKNOWN,
 MUSIC_PELAGIC,
 MUSIC_CRASHSITE,
 MUSIC_CRASHSITE_X,
 MUSIC_ATTACKSHIP,
 MUSIC_ATTACKSHIP_X,
 MUSIC_SKEDARRUINS,
 MUSIC_DEFECTION_INTRO,
 MUSIC_DEFECTION_OUTRO,
 MUSIC_DEFENSE_X,
 MUSIC_INVESTIGATION_INTRO,
 MUSIC_INVESTIGATION_OUTRO,
 MUSIC_VILLA_X,
 MUSIC_CHICAGO_X,
 MUSIC_G5_X,
 MUSIC_INFILTRATION_X,
 MUSIC_CHICAGO_OUTRO,
 MUSIC_EXTRACTION_OUTRO,
 MUSIC_EXTRACTION_INTRO,
 MUSIC_G5_INTRO,
 MUSIC_CHICAGO_INTRO,
 MUSIC_VILLA_INTRO1,
 MUSIC_INFILTRATION_INTRO,
 MUSIC_RESCUE_X,
 MUSIC_ESCAPE_X,
 MUSIC_AIRBASE_X,
 MUSIC_AIRFORCEONE_X,
 MUSIC_PELAGIC_X,
 MUSIC_DEEPSEA_X,
 MUSIC_SKEDARRUINS_X,
 MUSIC_AIRBASE_OUTRO_LONG,
 MUSIC_DARK_COMBAT,
 MUSIC_SKEDAR_MYSTERY,
 MUSIC_DEEPSEA_BETA,
 MUSIC_CI_OPERATIVE,
 MUSIC_DATADYNE_ACTION,
 MUSIC_MAIAN_TEARS,
 MUSIC_ALIEN_CONFLICT,
 MUSIC_ESCAPE_INTRO,
 MUSIC_RESCUE_OUTRO,
 MUSIC_VILLA_INTRO2,
 MUSIC_VILLA_INTRO3,
 MUSIC_G5_OUTRO,
 MUSIC_G5_MIDCUTSCENE,
 MUSIC_MISSION_FAILED,
 MUSIC_COMBATSIM_MENU,
 MUSIC_MISSION_SUCCESS,
 MUSIC_CRASHSITE_INTRO,
 MUSIC_AIRBASE_INTRO,
 MUSIC_ATTACKSHIP_INTRO,
 MUSIC_DEEPSEA_MIDCUTSCENE,
 MUSIC_AIRFORCEONE_INTRO,
 MUSIC_ATTACKSHIP_OUTRO,
 MUSIC_ESCAPE_MIDCUTSCENE,
 MUSIC_RESCUE_INTRO,
 MUSIC_DEEPSEA_INTRO,
 MUSIC_INFILTRATION_OUTRO,
 MUSIC_PELAGIC_INTRO,
 MUSIC_ESCAPE_OUTRO_LONG,
 MUSIC_DEFENSE_INTRO,
 MUSIC_CRASHSITE_OUTRO,
 MUSIC_CREDITS,
 MUSIC_MAINMENU,
 MUSIC_DEEPSEA_OUTRO,
 MUSIC_AIRFORCEONE_MIDCUTSCENE,
 MUSIC_PELAGIC_OUTRO,
 MUSIC_AIRFORCEONE_OUTRO,
 MUSIC_SKEDARRUINS_INTRO,
 MUSIC_BETA_NOTE,
 MUSIC_AIRBASE_OUTRO,
 MUSIC_DEFENSE_OUTRO,
 MUSIC_SKEDARRUINS_OUTRO,
 MUSIC_VILLA_OUTRO,
 MUSIC_SKEDARRUINS_KING,
 MUSIC_CI_TRAINING,
 MUSIC_CRASHSITE_WIND,
 MUSIC_COMBATSIM_COMPLETE,
 MUSIC_OCEAN,
 MUSIC_WIND,
 MUSIC_TRAFFIC,
 MUSIC_TITLE1,
 MUSIC_CI_INTRO,
 MUSIC_INFILTRATION_SFX,
 MUSIC_DEEPSEA_SFX,
 MUSIC_AIRFORCEONE_SFX,
 MUSIC_ATTACKSHIP_SFX,
 MUSIC_SKEDAR_WIND,
 MUSIC_ESCAPE_OUTRO_SFX,
 MUSIC_A51_LOUDSPEAKER1,
 MUSIC_A51_LOUDSPEAKER2,
 MUSIC_BETA_MELODY,
 MUSIC_ESCAPE_OUTRO_SHORT,
 MUSIC_END
};
enum sfx {
 SFX_0000,
 SFX_LAUNCH_ROCKET,
 SFX_EQUIP_HORIZONSCANNER,
 SFX_0003,
 SFX_0004,
 SFX_0005,
 SFX_0006,
 SFX_0007,
 SFX_0008,
 SFX_0009,
 SFX_000A,
 SFX_000B,
 SFX_000C,
 SFX_ARGH_FEMALE_000D,
 SFX_ARGH_FEMALE_000E,
 SFX_ARGH_FEMALE_000F,
 SFX_BOTTLE_BREAK,
 SFX_0011,
 SFX_0012,
 SFX_0013,
 SFX_0014,
 SFX_0015,
 SFX_0016,
 SFX_0017,
 SFX_0018,
 SFX_0019,
 SFX_001A,
 SFX_001B,
 SFX_001C,
 SFX_001D,
 SFX_001E,
 SFX_001F,
 SFX_0020,
 SFX_0021,
 SFX_0022,
 SFX_0023,
 SFX_0024,
 SFX_0025,
 SFX_0026,
 SFX_0027,
 SFX_0028,
 SFX_0029,
 SFX_002A,
 SFX_MENU_CANCEL,
 SFX_002C,
 SFX_002D,
 SFX_002E,
 SFX_002F,
 SFX_0030,
 SFX_0031,
 SFX_0032,
 SFX_0033,
 SFX_0034,
 SFX_0035,
 SFX_0036,
 SFX_0037,
 SFX_0038,
 SFX_0039,
 SFX_003A,
 SFX_003B,
 SFX_003C,
 SFX_003D,
 SFX_HUDMSG,
 SFX_003F,
 SFX_0040,
 SFX_0041,
 SFX_0042,
 SFX_0043,
 SFX_0044,
 SFX_0045,
 SFX_0046,
 SFX_0047,
 SFX_0048,
 SFX_0049,
 SFX_004A,
 SFX_004B,
 SFX_004C,
 SFX_004D,
 SFX_004E,
 SFX_004F,
 SFX_0050,
 SFX_0051,
 SFX_REGEN,
 SFX_0053,
 SFX_0054,
 SFX_0055,
 SFX_0056,
 SFX_0057,
 SFX_0058,
 SFX_0059,
 SFX_005A,
 SFX_CLOAK_ON,
 SFX_CLOAK_OFF,
 SFX_005D,
 SFX_005E,
 SFX_005F,
 SFX_0060,
 SFX_0061,
 SFX_0062,
 SFX_0063,
 SFX_SHIELD_DAMAGE,
 SFX_LASER_STREAM,
 SFX_0066,
 SFX_0067,
 SFX_0068,
 SFX_0069,
 SFX_006A,
 SFX_006B,
 SFX_006C,
 SFX_006D,
 SFX_006E,
 SFX_006F,
 SFX_0070,
 SFX_0071,
 SFX_0072,
 SFX_0073,
 SFX_0074,
 SFX_0075,
 SFX_0076,
 SFX_0077,
 SFX_0078,
 SFX_0079,
 SFX_007A,
 SFX_007B,
 SFX_007C,
 SFX_007D,
 SFX_007E,
 SFX_007F,
 SFX_0080,
 SFX_0081,
 SFX_0082,
 SFX_0083,
 SFX_0084,
 SFX_0085,
 SFX_ARGH_MALE_0086,
 SFX_ARGH_MALE_0087,
 SFX_ARGH_MALE_0088,
 SFX_ARGH_MALE_0089,
 SFX_ARGH_MALE_008A,
 SFX_ARGH_MALE_008B,
 SFX_ARGH_MALE_008C,
 SFX_ARGH_MALE_008D,
 SFX_ARGH_MALE_008E,
 SFX_ARGH_MALE_008F,
 SFX_ARGH_MALE_0090,
 SFX_ARGH_MALE_0091,
 SFX_ARGH_MALE_0092,
 SFX_ARGH_MALE_0093,
 SFX_ARGH_MALE_0094,
 SFX_ARGH_MALE_0095,
 SFX_ARGH_MALE_0096,
 SFX_ARGH_MALE_0097,
 SFX_ARGH_MALE_0098,
 SFX_ARGH_MALE_0099,
 SFX_ARGH_MALE_009A,
 SFX_ARGH_MALE_009B,
 SFX_ARGH_MALE_009C,
 SFX_ARGH_MALE_009D,
 SFX_ARGH_MALE_009E,
 SFX_009F,
 SFX_00A0,
 SFX_ALARM_AIRBASE,
 SFX_ALARM_2,
 SFX_ALARM_DEFAULT,
 SFX_00A4,
 SFX_00A5,
 SFX_00A6,
 SFX_00A7,
 SFX_00A8,
 SFX_00A9,
 SFX_00AA,
 SFX_00AB,
 SFX_00AC,
 SFX_00AD,
 SFX_00AE,
 SFX_00AF,
 SFX_00B0,
 SFX_00B1,
 SFX_00B2,
 SFX_00B3,
 SFX_00B4,
 SFX_00B5,
 SFX_00B6,
 SFX_00B7,
 SFX_00B8,
 SFX_00B9,
 SFX_PRESS_SWITCH,
 SFX_00BB,
 SFX_00BC,
 SFX_00BD,
 SFX_00BE,
 SFX_00BF,
 SFX_00C0,
 SFX_00C1,
 SFX_00C2,
 SFX_00C3,
 SFX_00C4,
 SFX_00C5,
 SFX_00C6,
 SFX_00C7,
 SFX_00C8,
 SFX_00C9,
 SFX_00CA,
 SFX_00CB,
 SFX_00CC,
 SFX_00CD,
 SFX_00CE,
 SFX_00CF,
 SFX_00D0,
 SFX_00D1,
 SFX_00D2,
 SFX_00D3,
 SFX_00D4,
 SFX_00D5,
 SFX_00D6,
 SFX_00D7,
 SFX_00D8,
 SFX_00D9,
 SFX_00DA,
 SFX_00DB,
 SFX_00DC,
 SFX_00DD,
 SFX_00DE,
 SFX_00DF,
 SFX_00E0,
 SFX_00E1,
 SFX_00E2,
 SFX_00E3,
 SFX_00E4,
 SFX_PICKUP_KEYCARD,
 SFX_00E6,
 SFX_00E7,
 SFX_PICKUP_GUN,
 SFX_PICKUP_KNIFE,
 SFX_PICKUP_AMMO,
 SFX_PICKUP_MINE,
 SFX_00EC,
 SFX_00ED,
 SFX_00EE,
 SFX_00EF,
 SFX_00F0,
 SFX_00F1,
 SFX_PICKUP_LASER,
 SFX_00F3,
 SFX_00F4,
 SFX_BIKE_TAKEOFF,
 SFX_00F6,
 SFX_00F7,
 SFX_00F8,
 SFX_00F9,
 SFX_00FA,
 SFX_00FB,
 SFX_00FC,
 SFX_00FD,
 SFX_00FE,
 SFX_00FF,
 SFX_0100,
 SFX_0101,
 SFX_0102,
 SFX_0103,
 SFX_0104,
 SFX_0105,
 SFX_0106,
 SFX_0107,
 SFX_0108,
 SFX_0109,
 SFX_010A,
 SFX_010B,
 SFX_010C,
 SFX_010D,
 SFX_010E,
 SFX_010F,
 SFX_0110,
 SFX_0111,
 SFX_0112,
 SFX_0113,
 SFX_0114,
 SFX_0115,
 SFX_0116,
 SFX_0117,
 SFX_0118,
 SFX_0119,
 SFX_011A,
 SFX_011B,
 SFX_011C,
 SFX_011D,
 SFX_011E,
 SFX_011F,
 SFX_0120,
 SFX_0121,
 SFX_0122,
 SFX_0123,
 SFX_0124,
 SFX_0125,
 SFX_0126,
 SFX_0127,
 SFX_0128,
 SFX_0129,
 SFX_012A,
 SFX_012B,
 SFX_012C,
 SFX_012D,
 SFX_012E,
 SFX_012F,
 SFX_0130,
 SFX_0131,
 SFX_0132,
 SFX_0133,
 SFX_0134,
 SFX_0135,
 SFX_0136,
 SFX_0137,
 SFX_0138,
 SFX_0139,
 SFX_013A,
 SFX_013B,
 SFX_013C,
 SFX_013D,
 SFX_013E,
 SFX_013F,
 SFX_0140,
 SFX_0141,
 SFX_0142,
 SFX_0143,
 SFX_0144,
 SFX_0145,
 SFX_0146,
 SFX_0147,
 SFX_0148,
 SFX_0149,
 SFX_014A,
 SFX_014B,
 SFX_014C,
 SFX_014D,
 SFX_014E,
 SFX_014F,
 SFX_0150,
 SFX_0151,
 SFX_0152,
 SFX_0153,
 SFX_0154,
 SFX_0155,
 SFX_0156,
 SFX_0157,
 SFX_0158,
 SFX_0159,
 SFX_015A,
 SFX_015B,
 SFX_015C,
 SFX_015D,
 SFX_015E,
 SFX_015F,
 SFX_0160,
 SFX_0161,
 SFX_0162,
 SFX_0163,
 SFX_0164,
 SFX_0165,
 SFX_0166,
 SFX_0167,
 SFX_0168,
 SFX_0169,
 SFX_016A,
 SFX_016B,
 SFX_016C,
 SFX_016D,
 SFX_016E,
 SFX_016F,
 SFX_0170,
 SFX_0171,
 SFX_0172,
 SFX_0173,
 SFX_0174,
 SFX_0175,
 SFX_0176,
 SFX_0177,
 SFX_0178,
 SFX_0179,
 SFX_017A,
 SFX_017B,
 SFX_017C,
 SFX_017D,
 SFX_017E,
 SFX_017F,
 SFX_0180,
 SFX_0181,
 SFX_0182,
 SFX_0183,
 SFX_0184,
 SFX_0185,
 SFX_0186,
 SFX_0187,
 SFX_0188,
 SFX_0189,
 SFX_018A,
 SFX_018B,
 SFX_018C,
 SFX_018D,
 SFX_018E,
 SFX_018F,
 SFX_0190,
 SFX_0191,
 SFX_0192,
 SFX_0193,
 SFX_0194,
 SFX_0195,
 SFX_0196,
 SFX_0197,
 SFX_0198,
 SFX_0199,
 SFX_019A,
 SFX_019B,
 SFX_019C,
 SFX_019D,
 SFX_019E,
 SFX_019F,
 SFX_01A0,
 SFX_01A1,
 SFX_01A2,
 SFX_01A3,
 SFX_01A4,
 SFX_01A5,
 SFX_01A6,
 SFX_01A7,
 SFX_01A8,
 SFX_01A9,
 SFX_01AA,
 SFX_01AB,
 SFX_01AC,
 SFX_01AD,
 SFX_01AE,
 SFX_01AF,
 SFX_01B0,
 SFX_01B1,
 SFX_01B2,
 SFX_01B3,
 SFX_BIKE_ENGINE,
 SFX_01B5,
 SFX_01B6,
 SFX_01B7,
 SFX_01B8,
 SFX_01B9,
 SFX_01BA,
 SFX_01BB,
 SFX_01BC,
 SFX_EYESPY_RUNNING,
 SFX_01BE,
 SFX_01BF,
 SFX_01C0,
 SFX_01C1,
 SFX_01C2,
 SFX_01C3,
 SFX_01C4,
 SFX_01C5,
 SFX_01C6,
 SFX_01C7,
 SFX_SLAYER_BEEP,
 SFX_01C9,
 SFX_01CA,
 SFX_01CB,
 SFX_01CC,
 SFX_PICKUP_SHIELD,
 SFX_01CE,
 SFX_01CF,
 SFX_01D0,
 SFX_01D1,
 SFX_01D2,
 SFX_01D3,
 SFX_01D4,
 SFX_01D5,
 SFX_01D6,
 SFX_01D7,
 SFX_01D8,
 SFX_01D9,
 SFX_01DA,
 SFX_01DB,
 SFX_01DC,
 SFX_01DD,
 SFX_01DE,
 SFX_01DF,
 SFX_01E0,
 SFX_01E1,
 SFX_01E2,
 SFX_01E3,
 SFX_01E4,
 SFX_01E5,
 SFX_01E6,
 SFX_01E7,
 SFX_01E8,
 SFX_01E9,
 SFX_01EA,
 SFX_01EB,
 SFX_01EC,
 SFX_01ED,
 SFX_01EE,
 SFX_01EF,
 SFX_01F0,
 SFX_01F1,
 SFX_01F2,
 SFX_01F3,
 SFX_01F4,
 SFX_01F5,
 SFX_01F6,
 SFX_01F7,
 SFX_01F8,
 SFX_01F9,
 SFX_01FA,
 SFX_01FB,
 SFX_01FC,
 SFX_01FD,
 SFX_01FE,
 SFX_01FF,
 SFX_0200,
 SFX_0201,
 SFX_0202,
 SFX_0203,
 SFX_0204,
 SFX_0205,
 SFX_0206,
 SFX_0207,
 SFX_0208,
 SFX_0209,
 SFX_020A,
 SFX_020B,
 SFX_020C,
 SFX_020D,
 SFX_020E,
 SFX_020F,
 SFX_0210,
 SFX_0211,
 SFX_0212,
 SFX_0213,
 SFX_0214,
 SFX_0215,
 SFX_0216,
 SFX_0217,
 SFX_0218,
 SFX_0219,
 SFX_021A,
 SFX_021B,
 SFX_021C,
 SFX_021D,
 SFX_021E,
 SFX_021F,
 SFX_0220,
 SFX_0221,
 SFX_0222,
 SFX_0223,
 SFX_0224,
 SFX_0225,
 SFX_0226,
 SFX_0227,
 SFX_0228,
 SFX_0229,
 SFX_022A,
 SFX_022B,
 SFX_022C,
 SFX_022D,
 SFX_022E,
 SFX_022F,
 SFX_0230,
 SFX_0231,
 SFX_0232,
 SFX_0233,
 SFX_0234,
 SFX_0235,
 SFX_0236,
 SFX_0237,
 SFX_0238,
 SFX_0239,
 SFX_023A,
 SFX_023B,
 SFX_023C,
 SFX_DRCAROLL_COME_ON,
 SFX_DRCAROLL_TAKING_YOUR_TIME,
 SFX_DRCAROLL_WHAT,
 SFX_ARGH_DRCAROLL_0240,
 SFX_0241,
 SFX_0242,
 SFX_DRCAROLL_OH_CRIKEY,
 SFX_DRCAROLL_GOODNESS_GRACIOUS,
 SFX_DRCAROLL_DONT_THEY_KNOW,
 SFX_DRCAROLL_STOP_THAT,
 SFX_DRCAROLL_GET_OUT_OF_HERE,
 SFX_DRCAROLL_KNOW_WHAT_YOURE_DOING,
 SFX_DRCAROLL_0249,
 SFX_024A,
 SFX_DRCAROLL_GOING_TO_THE_HELIPAD,
 SFX_ARGH_DRCAROLL_024C,
 SFX_DRCAROLL_SYSTEMS_FAILURE,
 SFX_DRCAROLL_YOU_GO_ON,
 SFX_DRCAROLL_I_CANT_MAKE_IT,
 SFX_ARGH_DRCAROLL_0250,
 SFX_ARGH_DRCAROLL_0251,
 SFX_DRCAROLL_QUITE_ENOUGH,
 SFX_0253,
 SFX_0254,
 SFX_0255,
 SFX_DRCAROLL_IM_DYING,
 SFX_DRCAROLL_GOODBYE,
 SFX_DRCAROLL_YOU_WERE_SUPPOSED,
 SFX_ARGH_DRCAROLL_0259,
 SFX_ARGH_DRCAROLL_025A,
 SFX_M0_WHAT_THE,
 SFX_M0_WHO_THE,
 SFX_025D,
 SFX_M0_MEDIC,
 SFX_M0_OW,
 SFX_M0_YOU_SHOT_ME,
 SFX_M0_IM_HIT,
 SFX_M0_IM_TAKING_FIRE,
 SFX_M0_TAKING_DAMAGE,
 SFX_M0_HEY_YOU,
 SFX_M0_INTRUDER_ALERT,
 SFX_M0_GOT_A_CONTACT,
 SFX_0267,
 SFX_0268,
 SFX_0269,
 SFX_026A,
 SFX_M0_HEAR_THAT,
 SFX_M0_WHATS_THAT_NOISE,
 SFX_M0_HEARD_A_NOISE,
 SFX_M0_ARE_YOU_OKAY,
 SFX_M0_GOT_A_MAN_DOWN,
 SFX_M0_HES_BOUGHT_IT,
 SFX_0271,
 SFX_M0_INTRUDER_ALERT2,
 SFX_M0_WEVE_GOT_TROUBLE,
 SFX_M0_WEVE_GOT_PROBLEMS,
 SFX_M0_I_SEE_HER,
 SFX_0276,
 SFX_M0_THERE_SHE_IS,
 SFX_M0_THERE_MOVEMENT,
 SFX_0279,
 SFX_027A,
 SFX_M0_CLEAR_SHOT,
 SFX_M0_SHES_MINE,
 SFX_M0_OPEN_FIRE,
 SFX_M0_WIPE_HER_OUT,
 SFX_M0_WASTE_HER,
 SFX_M0_GIVE_IT_UP,
 SFX_M0_SURRENDER_NOW,
 SFX_M0_TAKE_THAT,
 SFX_0283,
 SFX_0284,
 SFX_0285,
 SFX_0286,
 SFX_M0_HOW_DID_I_MISS,
 SFX_0288,
 SFX_M0_SHES_A_TRICKY_ONE,
 SFX_M0_COVER_ME,
 SFX_M0_WATCH_MY_BACK,
 SFX_M0_TAKE_COVER,
 SFX_M0_TAKE_COVER_028D,
 SFX_M0_ILL_COVER_YOU,
 SFX_M0_GET_DOWN,
 SFX_M0_GO_TO_PLAN_B,
 SFX_0291,
 SFX_M0_RETREAT,
 SFX_M0_CATCH,
 SFX_M0_EVERYBODY_DOWN,
 SFX_0295,
 SFX_0296,
 SFX_M0_FALL_BACK,
 SFX_M0_EVERYONE_BACK_OFF,
 SFX_M0_WITHDRAW,
 SFX_M0_FLANK_THE_TARGET,
 SFX_M0_LETS_SPLIT_UP,
 SFX_M0_SURROUND_HER,
 SFX_M0_GRENADE,
 SFX_029E,
 SFX_029F,
 SFX_02A0,
 SFX_02A1,
 SFX_M0_SHE_GOT_ME,
 SFX_M0_GRAB_A_BODY_BAG,
 SFX_M0_ONE_FOR_THE_MORGUE,
 SFX_M0_REST_IN_PEACE,
 SFX_02A6,
 SFX_M0_DONT_SHOOT_ME,
 SFX_M0_I_GIVE_UP,
 SFX_M0_YOU_WIN_I_SURRENDER,
 SFX_ARGH_JO_02AA,
 SFX_ARGH_JO_02AB,
 SFX_ARGH_JO_02AC,
 SFX_ARGH_JO_02AD,
 SFX_ARGH_JO_02AE,
 SFX_ARGH_JO_02AF,
 SFX_ARGH_JO_02B0,
 SFX_ARGH_JO_02B1,
 SFX_ARGH_JO_02B2,
 SFX_ARGH_JO_02B3,
 SFX_02B4,
 SFX_02B5,
 SFX_02B6,
 SFX_02B7,
 SFX_02B8,
 SFX_02B9,
 SFX_02BA,
 SFX_02BB,
 SFX_02BC,
 SFX_02BD,
 SFX_02BE,
 SFX_02BF,
 SFX_02C0,
 SFX_02C1,
 SFX_02C2,
 SFX_02C3,
 SFX_02C4,
 SFX_02C5,
 SFX_02C6,
 SFX_02C7,
 SFX_02C8,
 SFX_02C9,
 SFX_02CA,
 SFX_02CB,
 SFX_02CC,
 SFX_02CD,
 SFX_02CE,
 SFX_02CF,
 SFX_02D0,
 SFX_02D1,
 SFX_02D2,
 SFX_02D3,
 SFX_02D4,
 SFX_02D5,
 SFX_02D6,
 SFX_02D7,
 SFX_02D8,
 SFX_02D9,
 SFX_02DA,
 SFX_02DB,
 SFX_02DC,
 SFX_02DD,
 SFX_02DE,
 SFX_02DF,
 SFX_02E0,
 SFX_02E1,
 SFX_02E2,
 SFX_02E3,
 SFX_02E4,
 SFX_02E5,
 SFX_02E6,
 SFX_02E7,
 SFX_02E8,
 SFX_02E9,
 SFX_02EA,
 SFX_02EB,
 SFX_02EC,
 SFX_02ED,
 SFX_02EE,
 SFX_02EF,
 SFX_02F0,
 SFX_02F1,
 SFX_02F2,
 SFX_02F3,
 SFX_02F4,
 SFX_02F5,
 SFX_02F6,
 SFX_02F7,
 SFX_02F8,
 SFX_02F9,
 SFX_02FA,
 SFX_02FB,
 SFX_02FC,
 SFX_02FD,
 SFX_02FE,
 SFX_M1_WHA,
 SFX_0300,
 SFX_M1_IM_HIT_IM_HIT,
 SFX_M1_IM_BLEEDING,
 SFX_0303,
 SFX_M1_HELP_ME_OUT,
 SFX_M1_IM_IN_TROUBLE,
 SFX_M1_COME_HERE,
 SFX_M1_THERES_SOMEONE_HERE,
 SFX_M1_GET_HER,
 SFX_M1_WHOA,
 SFX_030A,
 SFX_M1_IS_THAT_A_BULLET,
 SFX_030C,
 SFX_030D,
 SFX_M1_THAT_SOUNDED_LIKE,
 SFX_M1_GUNFIRE,
 SFX_M1_SOMEONES_SHOOTING,
 SFX_0311,
 SFX_0312,
 SFX_0313,
 SFX_0314,
 SFX_0315,
 SFX_M1_HES_GONE,
 SFX_0317,
 SFX_0318,
 SFX_0319,
 SFX_031A,
 SFX_031B,
 SFX_M1_M2_LOOK_OUT_SHES_COMING,
 SFX_M1_M2_TAKE_COVER,
 SFX_M1_M2_LOOK_OUT_LOOK_OUT,
 SFX_M1_OVER_THERE,
 SFX_M1_HALT,
 SFX_M1_FREEZE,
 SFX_M1_LAST_MISTAKE,
 SFX_M1_WHAT_ARE_YOU_WAITING_FOR,
 SFX_M1_BRING_IT_ON,
 SFX_M1_TAKE_HER_DOWN,
 SFX_M1_EVERYBODY_GET_HER,
 SFX_M1_ATTACK,
 SFX_0328,
 SFX_M1_YEAH_BABY,
 SFX_032A,
 SFX_032B,
 SFX_032C,
 SFX_032D,
 SFX_032E,
 SFX_M1_MY_GUN_ITS_USELESS,
 SFX_0330,
 SFX_M1_STOP_DODGING,
 SFX_M1_SOMEONE_HIT_HER,
 SFX_0333,
 SFX_0334,
 SFX_M1_COVER_ME_NOW,
 SFX_M1_IM_GOING_FOR_COVER,
 SFX_M1_GO_FOR_IT,
 SFX_M1_GO_GO_GO,
 SFX_M1_RUN,
 SFX_M1_SHES_TOO_GOOD_RUN,
 SFX_M1_GET_SOME_BACKUP,
 SFX_M1_EVACUATE_THE_AREA,
 SFX_M1_CATCH_THIS,
 SFX_033E,
 SFX_M1_HERE_KEEP_IT,
 SFX_0340,
 SFX_0341,
 SFX_M1_GRENADE,
 SFX_M1_WITHDRAW,
 SFX_M1_FALL_BACK,
 SFX_M1_EVERYONE_GET_BACK,
 SFX_M1_SURROUND_HER,
 SFX_M1_SPREAD_OUT,
 SFX_M1_SPLIT_UP,
 SFX_M1_PLEASE_DONT,
 SFX_M1_DONT_SHOOT,
 SFX_M1_IM_ONLY_DOING_MY_JOB,
 SFX_034C,
 SFX_M1_WHY_ME,
 SFX_M1_CHOKING,
 SFX_034F,
 SFX_0350,
 SFX_0351,
 SFX_0352,
 SFX_0353,
 SFX_0354,
 SFX_M1_OUTSTANDING,
 SFX_M1_IM_JUST_TOO_GOOD,
 SFX_M1_YEEHAH_GOT_ONE,
 SFX_0358,
 SFX_0359,
 SFX_M1_ANOTHER_ONE_BITES_THE_DUST,
 SFX_M0_M1_LOOK_OUT_LOOK_OUT,
 SFX_M0_M1_ITS_A_GRENADE,
 SFX_M0_M1_CLEAR_THE_AREA,
 SFX_035E,
 SFX_035F,
 SFX_0360,
 SFX_0361,
 SFX_0362,
 SFX_0363,
 SFX_0364,
 SFX_0365,
 SFX_0366,
 SFX_0367,
 SFX_0368,
 SFX_0369,
 SFX_036A,
 SFX_036B,
 SFX_036C,
 SFX_036D,
 SFX_036E,
 SFX_036F,
 SFX_0370,
 SFX_0371,
 SFX_0372,
 SFX_0373,
 SFX_0374,
 SFX_0375,
 SFX_0376,
 SFX_0377,
 SFX_0378,
 SFX_0379,
 SFX_037A,
 SFX_F_HEY,
 SFX_F_HUH,
 SFX_037D,
 SFX_F_IM_WOUNDED,
 SFX_F_HELP_ME_OUT,
 SFX_F_IM_IN_TROUBLE,
 SFX_F_GET_HER,
 SFX_F_HEY_YOU_COME_HERE,
 SFX_0383,
 SFX_F_TARGET_ATTACKING,
 SFX_F_UNDER_FIRE,
 SFX_0386,
 SFX_F_WERE_UNDER_FIRE,
 SFX_0388,
 SFX_F_0389,
 SFX_F_SOMEONES_SHOOTING,
 SFX_038B,
 SFX_038C,
 SFX_F_UNIT_DOWN,
 SFX_038E,
 SFX_038F,
 SFX_0390,
 SFX_0391,
 SFX_0392,
 SFX_0393,
 SFX_0394,
 SFX_0395,
 SFX_F_COME_ON,
 SFX_0397,
 SFX_F_EVERYONE_GET_HER,
 SFX_F_ATTACK,
 SFX_F_DID_THAT_HURT,
 SFX_F_YOU_WANT_SOME_MORE,
 SFX_039C,
 SFX_039D,
 SFX_039E,
 SFX_F_THIS_GUNS_USELESS,
 SFX_03A0,
 SFX_F_STAND_STILL,
 SFX_F_SOMEONE_HIT_HER,
 SFX_03A3,
 SFX_F_COVER_ME,
 SFX_03A5,
 SFX_F_TAKE_COVER,
 SFX_F_GO_FOR_IT,
 SFX_03A8,
 SFX_F_RUN,
 SFX_F_GET_REINFORCEMENTS,
 SFX_F_EVACUATE_THE_AREA,
 SFX_F_RETREAT,
 SFX_F_CATCH_THIS,
 SFX_F_TIME_TO_DIE,
 SFX_03AF,
 SFX_F_WITHDRAW,
 SFX_F_FALL_BACK,
 SFX_03B2,
 SFX_F_SPREAD_OUT,
 SFX_F_SPLIT_UP,
 SFX_F_PLEASE_DONT,
 SFX_F_DONT_SHOOT,
 SFX_F_WHY_ME,
 SFX_F_NOO,
 SFX_03B9,
 SFX_03BA,
 SFX_03BB,
 SFX_F_GET_A_CLEANER,
 SFX_03BD,
 SFX_F_IM_JUST_TOO_GOOD,
 SFX_03BF,
 SFX_F_SUCH_A_WASTE,
 SFX_F_LOOK_OUT,
 SFX_F_ITS_A_GRENADE,
 SFX_03C3,
 SFX_M2_HOW_THE,
 SFX_M2_HEY,
 SFX_M2_STOP,
 SFX_03C7,
 SFX_M2_WHY_YOU,
 SFX_03C9,
 SFX_03CA,
 SFX_03CB,
 SFX_03CC,
 SFX_M2_IM_INJURED,
 SFX_M2_IM_HIT_IM_HIT,
 SFX_03CF,
 SFX_M2_TARGET_SIGHTED,
 SFX_M2_COME_ON_MAN,
 SFX_03D2,
 SFX_M2_THAT_WAS_CLOSE,
 SFX_03D4,
 SFX_M2_AY_CARAMBA,
 SFX_M2_LISTEN_GUNSHOTS,
 SFX_M2_SOMEONES_NEARBY,
 SFX_03D8,
 SFX_M2_BODY_COUNTS_TOO_HIGH,
 SFX_M2_I_NEVER_LIKED_HIM_ANYWAY,
 SFX_M2_THAT_WAS_MY_BEST_FRIEND,
 SFX_03DC,
 SFX_03DD,
 SFX_03DE,
 SFX_03DF,
 SFX_03E0,
 SFX_M2_WATCH_OUT,
 SFX_M2_HELP_ME_OUT,
 SFX_M2_WEVE_GOT_AN_INTRUDER,
 SFX_M2_GET_HER,
 SFX_M2_THERE_ATTACK,
 SFX_M2_HEY_YOU_STOP,
 SFX_M2_COME_ON_MAN2,
 SFX_M2_DIE,
 SFX_M2_TAKE_THIS,
 SFX_M2_MOVE_IN,
 SFX_M2_YOURE_OUT_OF_YOUR_LEAGUE,
 SFX_M2_LET_HER_HAVE_IT,
 SFX_M2_SURRENDER_OR_DIE,
 SFX_M2_I_HAVE_YOU_NOW,
 SFX_M2_YOU_WANT_BEAT_ME,
 SFX_03F0,
 SFX_03F1,
 SFX_03F2,
 SFX_03F3,
 SFX_M2_I_DONT_BELIEVE_IT,
 SFX_03F5,
 SFX_03F6,
 SFX_M2_STOP_MOVING,
 SFX_M2_NO_ESCAPE_FOR_YOU,
 SFX_M2_HELP_ME_OUT_HERE,
 SFX_M2_HEY_DISTRACT_HER,
 SFX_M2_KEEP_HER_OCCUPIED,
 SFX_M2_MOVE_IT_MOVE_IT,
 SFX_M2_GET_TO_COVER_NOW,
 SFX_M2_RUN_FOR_IT,
 SFX_M2_RETREAT,
 SFX_0400,
 SFX_M2_GET_BACK_GET_BACK,
 SFX_0402,
 SFX_M2_FIRE_IN_THE_HOLE,
 SFX_0404,
 SFX_M2_HERES_A_LITTLE_PRESENT_FOR_YA,
 SFX_0406,
 SFX_M2_TRY_THIS_FOR_SIZE,
 SFX_M2_GET_OUT_OF_THE_WAY,
 SFX_M2_FALL_BACK,
 SFX_M2_MOVE_OUT,
 SFX_M2_TEAM_UP_GUYS,
 SFX_M2_COME_ON_AROUND_THE_SIDE,
 SFX_M2_SCATTER,
 SFX_M2_I_DONT_LIKE_THIS_ANY_MORE,
 SFX_M2_DONT_HURT_ME,
 SFX_M2_YOU_WIN_I_GIVE_UP,
 SFX_0411,
 SFX_0412,
 SFX_0413,
 SFX_0414,
 SFX_M2_I_DONT_WANT_TO_DIE,
 SFX_0416,
 SFX_M2_ITS_ALL_OVER_FOR_THIS_ONE,
 SFX_0418,
 SFX_0419,
 SFX_041A,
 SFX_M2_IM_THE_MAN,
 SFX_M2_BOY_THAT_WAS_CLOSE,
 SFX_M2_DID_YOU_SEE_THAT,
 SFX_041E,
 SFX_041F,
 SFX_M2_GET_BACK_QUICK,
 SFX_M2_WERE_GONNA_DIE,
 SFX_0422,
 SFX_0423,
 SFX_0424,
 SFX_0425,
 SFX_0426,
 SFX_0427,
 SFX_0428,
 SFX_0429,
 SFX_042A,
 SFX_DOOR_042B,
 SFX_DOOR_042C,
 SFX_042D,
 SFX_DISGUISE_ON,
 SFX_042F,
 SFX_0430,
 SFX_0431,
 SFX_0432,
 SFX_RELOAD_FARSIGHT,
 SFX_0434,
 SFX_0435,
 SFX_0436,
 SFX_0437,
 SFX_0438,
 SFX_0439,
 SFX_043A,
 SFX_043B,
 SFX_043C,
 SFX_043D,
 SFX_MENU_SUBFOCUS,
 SFX_043F,
 SFX_0440,
 SFX_MENU_FOCUS,
 SFX_0442,
 SFX_0443,
 SFX_CIV_THERES_A_MANIAC,
 SFX_0445,
 SFX_CIV_GREETINGS_CITIZEN,
 SFX_CIV_HOWS_IT_GOING,
 SFX_CIV_GUNS_DONT_SCARE_ME,
 SFX_CIV_KEEP_AWAY_FROM_THIS_CAR,
 SFX_044A,
 SFX_FBI_WE_HAVE_AN_INTRUDER,
 SFX_044C,
 SFX_044D,
 SFX_044E,
 SFX_044F,
 SFX_0450,
 SFX_0451,
 SFX_0452,
 SFX_0453,
 SFX_0454,
 SFX_0455,
 SFX_0456,
 SFX_0457,
 SFX_0458,
 SFX_FBI_CODE_2_SITUATION,
 SFX_FBI_REQUEST_BACKUP_IMMEDIATELY,
 SFX_045B,
 SFX_045C,
 SFX_CIV_TAKE_IT_EASY,
 SFX_CIV_I_DONT_WANT_ANY_TROUBLE,
 SFX_CIV_QUICK_DOWN_THERE,
 SFX_0460,
 SFX_CIV_HEY_SUGAR_WANNA_PARTY,
 SFX_0462,
 SFX_CIV_TAKE_THE_WALLET,
 SFX_0464,
 SFX_CIV_HEY_BABY,
 SFX_CIV_WHISTLE,
 SFX_0467,
 SFX_CIV_GET_ME_OUT_OF_HERE,
 SFX_0469,
 SFX_046A,
 SFX_046B,
 SFX_046C,
 SFX_046D,
 SFX_046E,
 SFX_JO_LANDING_046F,
 SFX_0470,
 SFX_0471,
 SFX_0472,
 SFX_0473,
 SFX_0474,
 SFX_0475,
 SFX_0476,
 SFX_0477,
 SFX_0478,
 SFX_0479,
 SFX_047A,
 SFX_047B,
 SFX_047C,
 SFX_047D,
 SFX_047E,
 SFX_047F,
 SFX_0480,
 SFX_0481,
 SFX_0482,
 SFX_0483,
 SFX_0484,
 SFX_0485,
 SFX_0486,
 SFX_0487,
 SFX_0488,
 SFX_0489,
 SFX_048A,
 SFX_048B,
 SFX_048C,
 SFX_048D,
 SFX_048E,
 SFX_048F,
 SFX_0490,
 SFX_0491,
 SFX_0492,
 SFX_0493,
 SFX_0494,
 SFX_0495,
 SFX_0496,
 SFX_0497,
 SFX_0498,
 SFX_0499,
 SFX_049A,
 SFX_049B,
 SFX_049C,
 SFX_049D,
 SFX_049E,
 SFX_049F,
 SFX_04A0,
 SFX_04A1,
 SFX_04A2,
 SFX_04A3,
 SFX_04A4,
 SFX_04A5,
 SFX_04A6,
 SFX_04A7,
 SFX_04A8,
 SFX_04A9,
 SFX_04AA,
 SFX_04AB,
 SFX_ALARM_INFILTRATION,
 SFX_04AD,
 SFX_04AE,
 SFX_04AF,
 SFX_04B0,
 SFX_04B1,
 SFX_04B2,
 SFX_04B3,
 SFX_04B4,
 SFX_04B5,
 SFX_04B6,
 SFX_04B7,
 SFX_04B8,
 SFX_04B9,
 SFX_04BA,
 SFX_04BB,
 SFX_04BC,
 SFX_04BD,
 SFX_04BE,
 SFX_04BF,
 SFX_04C0,
 SFX_04C1,
 SFX_04C2,
 SFX_04C3,
 SFX_04C4,
 SFX_04C5,
 SFX_M0_MY_GUN,
 SFX_M0_TRIGGER_THE_ALARM,
 SFX_04C8,
 SFX_M0_HELLO_THERE,
 SFX_M0_WHATS_THIS,
 SFX_M0_IM_SURE_I_HEARD_A_NOISE,
 SFX_M0_HEARING_THINGS,
 SFX_04CD,
 SFX_M1_WARN_THE_OTHERS,
 SFX_M1_WHAT_IS_IT,
 SFX_M1_HOW_DID_THAT_GET_HERE,
 SFX_M1_DONT_TOUCH_IT,
 SFX_M1_I_CANT_SEE_ANYBODY,
 SFX_M1_THERES_NO_ONE_HERE,
 SFX_M2_ACTIVATE_THE_ALARM,
 SFX_M2_IS_IT_DANGEROUS,
 SFX_M2_DONT_MOVE,
 SFX_M2_STAY_BACK,
 SFX_M2_I_BET_THIS_IS_ANOTHER_DRILL,
 SFX_M2_ANOTHER_FALSE_ALARM,
 SFX_04DA,
 SFX_04DB,
 SFX_04DC,
 SFX_04DD,
 SFX_04DE,
 SFX_04DF,
 SFX_04E0,
 SFX_04E1,
 SFX_04E2,
 SFX_04E3,
 SFX_04E4,
 SFX_04E5,
 SFX_04E6,
 SFX_04E7,
 SFX_04E8,
 SFX_04E9,
 SFX_04EA,
 SFX_04EB,
 SFX_04EC,
 SFX_04ED,
 SFX_04EE,
 SFX_04EF,
 SFX_04F0,
 SFX_04F1,
 SFX_04F2,
 SFX_04F3,
 SFX_04F4,
 SFX_04F5,
 SFX_04F6,
 SFX_04F7,
 SFX_04F8,
 SFX_04F9,
 SFX_04FA,
 SFX_RELOAD_04FB,
 SFX_04FC,
 SFX_04FD,
 SFX_04FE,
 SFX_CAMSPY_SHUTTER,
 SFX_0500,
 SFX_0501,
 SFX_0502,
 SFX_0503,
 SFX_0504,
 SFX_0505,
 SFX_0506,
 SFX_0507,
 SFX_0508,
 SFX_0509,
 SFX_050A,
 SFX_050B,
 SFX_050C,
 SFX_050D,
 SFX_050E,
 SFX_050F,
 SFX_0510,
 SFX_0511,
 SFX_0512,
 SFX_0513,
 SFX_0514,
 SFX_SECURE_THE_PERIMETER,
 SFX_0516,
 SFX_0517,
 SFX_0518,
 SFX_0519,
 SFX_051A,
 SFX_ELVIS_INTERGALACTIC_PEACE,
 SFX_ELVIS_EAT_HOT_LEAD_WEIRDOS,
 SFX_ELVIS_KISS_MY_ALIEN_BUTT,
 SFX_ELVIS_ILL_KICK_YOUR_ASS,
 SFX_ELVIS_FOR_YOUR_OWN_GOOD,
 SFX_ELVIS_YOU_DARE_SHOOT_AT_ME,
 SFX_ELVIS_DONT_MESS_WITH_THE_MAIAN,
 SFX_ELVIS_IM_BAD,
 SFX_ELVIS_ALL_GOING_WRONG,
 SFX_ELVIS_WATCH_THE_SUIT,
 SFX_ELVIS_HEHE,
 SFX_0526,
 SFX_0527,
 SFX_0528,
 SFX_SKEDAR_ROAR_0529,
 SFX_SKEDAR_ROAR_052A,
 SFX_SKEDAR_ROAR_052B,
 SFX_052C,
 SFX_SKEDAR_ROAR_052D,
 SFX_SKEDAR_ROAR_052E,
 SFX_SKEDAR_ROAR_052F,
 SFX_SKEDAR_ROAR_0530,
 SFX_SKEDAR_ROAR_0531,
 SFX_SKEDAR_ROAR_0532,
 SFX_SKEDAR_ROAR_0533,
 SFX_SKEDAR_ROAR_0534,
 SFX_0535,
 SFX_SKEDAR_ROAR_0536,
 SFX_SKEDAR_ROAR_0537,
 SFX_SKEDAR_ROAR_0538,
 SFX_SKEDAR_ROAR_0539,
 SFX_SKEDAR_ROAR_053A,
 SFX_053B,
 SFX_053C,
 SFX_053D,
 SFX_053E,
 SFX_053F,
 SFX_0540,
 SFX_0541,
 SFX_0542,
 SFX_0543,
 SFX_0544,
 SFX_0545,
 SFX_0546,
 SFX_0547,
 SFX_0548,
 SFX_0549,
 SFX_054A,
 SFX_054B,
 SFX_054C,
 SFX_054D,
 SFX_054E,
 SFX_054F,
 SFX_0550,
 SFX_0551,
 SFX_0552,
 SFX_0553,
 SFX_0554,
 SFX_0555,
 SFX_0556,
 SFX_0557,
 SFX_0558,
 SFX_0559,
 SFX_055A,
 SFX_055B,
 SFX_055C,
 SFX_055D,
 SFX_055E,
 SFX_055F,
 SFX_0560,
 SFX_0561,
 SFX_0562,
 SFX_0563,
 SFX_0564,
 SFX_0565,
 SFX_0566,
 SFX_0567,
 SFX_0568,
 SFX_0569,
 SFX_056A,
 SFX_056B,
 SFX_056C,
 SFX_056D,
 SFX_056E,
 SFX_056F,
 SFX_0570,
 SFX_0571,
 SFX_0572,
 SFX_0573,
 SFX_0574,
 SFX_0575,
 SFX_0576,
 SFX_0577,
 SFX_0578,
 SFX_0579,
 SFX_057A,
 SFX_057B,
 SFX_057C,
 SFX_057D,
 SFX_057E,
 SFX_057F,
 SFX_0580,
 SFX_0581,
 SFX_0582,
 SFX_0583,
 SFX_0584,
 SFX_0585,
 SFX_0586,
 SFX_0587,
 SFX_0588,
 SFX_0589,
 SFX_058A,
 SFX_058B,
 SFX_058C,
 SFX_058D,
 SFX_058E,
 SFX_058F,
 SFX_0590,
 SFX_0591,
 SFX_0592,
 SFX_0593,
 SFX_0594,
 SFX_0595,
 SFX_0596,
 SFX_0597,
 SFX_0598,
 SFX_0599,
 SFX_059A,
 SFX_059B,
 SFX_059C,
 SFX_INFIL_STATIC_SHORT,
 SFX_INFIL_STATIC_MEDIUM,
 SFX_INFIL_STATIC_LONG,
 SFX_05A0,
 SFX_05A1,
 SFX_05A2,
 SFX_05A3,
 SFX_05A4,
 SFX_05A5,
 SFX_05A6,
 SFX_05A7,
 SFX_05A8,
 SFX_05A9,
 SFX_05AA,
 SFX_COUGH_05AB,
 SFX_COUGH_05AC,
 SFX_COUGH_05AD,
 SFX_COUGH_05AE,
 SFX_COUGH_04AF,
 SFX_COUGH_04B0,
 SFX_GURGLE_05B1,
 SFX_GURGLE_05B2,
 SFX_05B3,
 SFX_05B4,
 SFX_05B5,
 SFX_JO_LANDING_05B6,
 SFX_JO_LANDING_05B7,
 SFX_MP_SCOREPOINT,
 SFX_05B9,
 SFX_05BA,
 SFX_MENU_SWIPE,
 SFX_MENU_OPENDIALOG,
 SFX_05BD,
 SFX_SHOULD_HAVE_COME_HERE_GIRL,
 SFX_WERE_TAKING_OVER,
 SFX_05C0,
 SFX_05C1,
 SFX_ALARM_ATTACKSHIP,
 SFX_05C3,
 SFX_05C4,
 SFX_05C5,
 SFX_05C6,
 SFX_05C7,
 SFX_HEARTBEAT,
 SFX_JO_BOOST_ACTIVATE,
 SFX_05CA,
 SFX_05CB,
 SFX_05CC,
 SFX_05CD,
 SFX_05CE,
 SFX_05CF,
 SFX_05D0,
 SFX_05D1,
 SFX_05D2,
 SFX_05D3,
 SFX_FR_ALARM,
 SFX_FR_LIGHTSON,
 SFX_FR_LIGHTSOFF,
 SFX_05D7,
 SFX_WOOD_BREAK,
 SFX_FR_CONVEYER,
 SFX_FR_CONVEYER_STOP,
 SFX_TRAINING_FAIL,
 SFX_TRAINING_COMPLETE,
 SFX_MENU_SELECT,
 SFX_05DE,
 SFX_ARGH_MAIAN_05DF,
 SFX_ARGH_MAIAN_05E0,
 SFX_ARGH_MAIAN_05E1,
 SFX_MAIAN_05E2,
 SFX_MAIAN_05E3,
 SFX_MAIAN_05E4,
 SFX_MAIAN_05E5,
 SFX_MAIAN_05E6,
 SFX_MAIAN_05E7,
 SFX_05E8,
 SFX_05E9,
 SFX_05EA,
 SFX_05EB,
 SFX_05EC,
 SFX_05ED,
 SFX_05EE,
 SFX_05EF,
 SFX_05F0,
 SFX_05F1,
 SFX_05F2,
 SFX_05F3,
 SFX_05F4,
 SFX_05F5,
 SFX_05F6,
 SFX_05F7,
 SFX_05F8,
 SFX_05F9,
 SFX_05FA,
 SFX_05FB,
 SFX_05FC,
 SFX_05FD,
 SFX_05FE,
 SFX_05FF,
 SFX_0600,
 SFX_0601,
 SFX_0602,
 SFX_0603,
 SFX_0604,
 SFX_0605,
 SFX_0606,
 SFX_0607,
 SFX_NOSEDIVE,
 SFX_UFOHUM,
 SFX_ALARM_CHICAGO = 0x6455,
 SFX_8000 = 0x8000,
 SFX_DOOR_8001,
 SFX_DOOR_8002,
 SFX_DOOR_8003,
 SFX_DOOR_8004,
 SFX_DOOR_8005,
 SFX_DOOR_8006,
 SFX_DOOR_8007,
 SFX_DOOR_8008,
 SFX_8009,
 SFX_DOOR_800A,
 SFX_DOOR_800B,
 SFX_DOOR_800C,
 SFX_DOOR_800D,
 SFX_DOOR_800E,
 SFX_DOOR_800F,
 SFX_DOOR_8010,
 SFX_DOOR_8011,
 SFX_DOOR_8012,
 SFX_DOOR_8013,
 SFX_DOOR_8014,
 SFX_DOOR_8015,
 SFX_DOOR_8016,
 SFX_DOOR_8017,
 SFX_DOOR_8018,
 SFX_DOOR_8019,
 SFX_DOOR_801A,
 SFX_DOOR_801B,
 SFX_DOOR_801C,
 SFX_DOOR_801D,
 SFX_DOOR_801E,
 SFX_DOOR_801F,
 SFX_DOOR_8020,
 SFX_DOOR_8021,
 SFX_DOOR_8022,
 SFX_8023,
 SFX_8024,
 SFX_8025,
 SFX_DOOR_8026,
 SFX_DOOR_8027,
 SFX_CARR_HELLO_JOANNA,
 SFX_8029,
 SFX_802A,
 SFX_CIFEM_HI_THERE,
 SFX_GRIMSHAW_WELCOME,
 SFX_GRIMSHAW_HI_THERE,
 SFX_GRIMSHAW_UMM_ERR_HI,
 SFX_HOLO_HI,
 SFX_HANGAR_WHAT_DO_YOU_WANT,
 SFX_FOSTER_STAR_AGENT,
 SFX_CIM_HEY_THERE,
 SFX_CIM_HI,
 SFX_CIM_HOWS_IT_GOING,
 SFX_CIFEM_HELLO,
 SFX_CIFEM_HI_JO,
 SFX_CIFEM_HOWS_IT_GOING,
 SFX_8038,
 SFX_8039,
 SFX_803A,
 SFX_803B,
 SFX_803C,
 SFX_803D,
 SFX_803E,
 SFX_803F,
 SFX_MENU_ERROR,
 SFX_8041,
 SFX_8042,
 SFX_8043,
 SFX_8044,
 SFX_8045,
 SFX_8046,
 SFX_8047,
 SFX_8048,
 SFX_8049,
 SFX_804A,
 SFX_804B,
 SFX_804C,
 SFX_804D,
 SFX_804E,
 SFX_RELOAD_DEFAULT,
 SFX_8050,
 SFX_8051,
 SFX_FIREEMPTY,
 SFX_LAUNCH_ROCKET_8053,
 SFX_8054,
 SFX_FIRE_SHOTGUN,
 SFX_8056,
 SFX_DRUGSPY_FIREDART,
 SFX_8058,
 SFX_8059,
 SFX_805A,
 SFX_805B,
 SFX_805C,
 SFX_805D,
 SFX_805E,
 SFX_805F,
 SFX_8060,
 SFX_8061,
 SFX_8062,
 SFX_8063,
 SFX_8064,
 SFX_MAULER_CHARGE,
 SFX_8066,
 SFX_8067,
 SFX_SLAYER_WHIR,
 SFX_8069,
 SFX_806A,
 SFX_806B,
 SFX_806C,
 SFX_806D,
 SFX_806E,
 SFX_806F,
 SFX_8070,
 SFX_8071,
 SFX_8072,
 SFX_8073,
 SFX_8074,
 SFX_8075,
 SFX_HIT_CHR,
 SFX_HIT_GLASS,
 SFX_GLASS_SHATTER,
 SFX_HIT_METAL_8079,
 SFX_807A,
 SFX_HIT_METAL_807B,
 SFX_HATHIT_807C,
 SFX_HIT_SNOW,
 SFX_HIT_WOOD_807E,
 SFX_HIT_WOOD_807F,
 SFX_HIT_WATER,
 SFX_HIT_MUD_8081,
 SFX_HIT_MUD_8082,
 SFX_HIT_MUD_8083,
 SFX_HIT_DIRT_8084,
 SFX_HIT_DIRT_8085,
 SFX_HIT_TILE,
 SFX_HIT_STONE_8087,
 SFX_HIT_STONE_8088,
 SFX_HIT_METALOBJ_8089,
 SFX_HIT_METALOBJ_808A,
 SFX_808B,
 SFX_EYESPYHIT,
 SFX_THUD_808D,
 SFX_THUD_808E,
 SFX_THUD_808F,
 SFX_THUD_8090,
 SFX_THUD_8091,
 SFX_THUD_8092,
 SFX_THUD_8093,
 SFX_THUD_8094,
 SFX_THUD_8095,
 SFX_THUD_8096,
 SFX_THUD_8097,
 SFX_EXPLOSION_8098,
 SFX_8099,
 SFX_EXPLOSION_809A,
 SFX_809B,
 SFX_809C,
 SFX_809D,
 SFX_809E,
 SFX_809F,
 SFX_80A0,
 SFX_80A1,
 SFX_80A2,
 SFX_80A3,
 SFX_80A4,
 SFX_80A5,
 SFX_80A6,
 SFX_80A7,
 SFX_80A8,
 SFX_THROW,
 SFX_80AA,
 SFX_DETONATE,
 SFX_DOOR_80AC,
 SFX_DOOR_80AD,
 SFX_DOOR_80AE,
 SFX_BIKE_PULSE,
 SFX_80B0,
 SFX_80B1,
 SFX_80B2,
 SFX_80B3,
 SFX_80B4,
 SFX_80B5,
 SFX_80B6,
 SFX_80B7,
 SFX_80B8,
 SFX_80B9,
 SFX_80BA,
 SFX_80BB,
 SFX_80BC,
 SFX_80BD,
 SFX_80BE,
 SFX_80BF,
 SFX_80C0,
 SFX_80C1,
 SFX_80C2,
 SFX_80C3,
 SFX_FOOTSTEP_80C4,
 SFX_FOOTSTEP_80C5,
 SFX_FOOTSTEP_80C6,
 SFX_FOOTSTEP_80C7,
 SFX_FOOTSTEP_80C8,
 SFX_FOOTSTEP_80C9,
 SFX_FOOTSTEP_80CA,
 SFX_FOOTSTEP_80CB,
 SFX_FOOTSTEP_80CC,
 SFX_FOOTSTEP_80CD,
 SFX_FOOTSTEP_80CE,
 SFX_FOOTSTEP_80CF,
 SFX_FOOTSTEP_80D0,
 SFX_FOOTSTEP_80D1,
 SFX_FOOTSTEP_80D2,
 SFX_FOOTSTEP_80D3,
 SFX_FOOTSTEP_80D4,
 SFX_FOOTSTEP_80D5,
 SFX_FOOTSTEP_80D6,
 SFX_FOOTSTEP_80D7,
 SFX_FOOTSTEP_80D8,
 SFX_FOOTSTEP_80D9,
 SFX_FOOTSTEP_80DA,
 SFX_FOOTSTEP_80DB,
 SFX_FOOTSTEP_80DC,
 SFX_FOOTSTEP_80DD,
 SFX_FOOTSTEP_80DE,
 SFX_FOOTSTEP_80DF,
 SFX_FOOTSTEP_80E0,
 SFX_FOOTSTEP_80E1,
 SFX_FOOTSTEP_80E2,
 SFX_FOOTSTEP_80E3,
 SFX_FOOTSTEP_80E4,
 SFX_FOOTSTEP_80E5,
 SFX_FOOTSTEP_80E6,
 SFX_FOOTSTEP_80E7,
 SFX_FOOTSTEP_80E8,
 SFX_FOOTSTEP_80E9,
 SFX_FOOTSTEP_80EA,
 SFX_FOOTSTEP_80EB,
 SFX_FOOTSTEP_80EC,
 SFX_FOOTSTEP_80ED,
 SFX_FOOTSTEP_80EE,
 SFX_FOOTSTEP_80EF,
 SFX_FOOTSTEP_80F0,
 SFX_FOOTSTEP_80F1,
 SFX_FOOTSTEP_80F2,
 SFX_FOOTSTEP_80F3,
 SFX_FOOTSTEP_80F4,
 SFX_FOOTSTEP_80F5,
 SFX_80F6,
 SFX_M0_WHAT_THE_HELL,
 SFX_M0_DAMN_IT_MISSED,
 SFX_M0_GODS_SAKE_SOMEONE_HIT_HER,
 SFX_M0_GET_THE_HELL_OUT_OF_HERE,
 SFX_M0_YOU_BITCH,
 SFX_M0_OH_MY_GOD,
 SFX_80FE,
 SFX_80FF,
 SFX_8100,
 SFX_8101,
 SFX_8102,
 SFX_SCI_WHO_THE_HELL_ARE_YOU,
 SFX_8104,
 SFX_8105,
 SFX_8106,
 SFX_8107,
 SFX_8108,
 SFX_8109,
 SFX_810A,
 SFX_810B,
 SFX_SHIP_HUM,
 SFX_810D,
 SFX_810E,
 SFX_810F,
 SFX_8110,
 SFX_8111,
 SFX_8112,
 SFX_8113,
 SFX_8114,
 SFX_8115,
 SFX_8116,
 SFX_8117,
 SFX_TYPING_8118,
 SFX_8119,
 SFX_811A,
 SFX_811B,
 SFX_811C,
 SFX_811D,
 SFX_811E,
 SFX_811F,
 SFX_8120,
 SFX_8121,
 SFX_8122,
 SFX_8123,
 SFX_M1_HOLY,
 SFX_M1_WHAT_THE_HELL,
 SFX_M1_OH_MY_GOD,
 SFX_M1_OH_GOD_IM_HIT,
 SFX_M1_MY_GOD,
 SFX_M1_NOOO,
 SFX_M1_BLOODY_STUPID_GUN,
 SFX_M1_DAMN_IT,
 SFX_M1_DAMN_SHES_GOOD,
 SFX_M1_COVER_MY_ASS,
 SFX_M1_SCREAM,
 SFX_F_MY_GOD,
 SFX_M2_GEEZ_THAT_HURT,
 SFX_M2_DAMN_IT_IM_TAKING_FIRE,
 SFX_M2_GOD_DAMN_IT,
 SFX_M2_HOLY_MOLY,
 SFX_M2_DAMN_MISSED_AGAIN,
 SFX_M2_DAMN_YOU,
 SFX_M2_HELL_SHES_GOOD,
 SFX_M2_LETS_GET_THE_HELL_OUT_OF_HERE,
 SFX_M2_NOOO,
 SFX_M2_OH_GOD_IM_DYING,
 SFX_M2_GOD_RUN,
 SFX_813E,
 SFX_813F,
 SFX_8140,
 SFX_8141,
 SFX_8142,
 SFX_8143,
 SFX_8144,
 SFX_8145,
 SFX_8146,
 SFX_8147,
 SFX_8148,
 SFX_8149,
 SFX_CIV_OH_MY_GOD,
 SFX_814B,
 SFX_814C,
 SFX_8150,
 SFX_8151,
 SFX_8152,
 SFX_8153,
 SFX_8154,
 SFX_8155,
 SFX_8156,
 SFX_8157,
 SFX_8158,
 SFX_8159,
 SFX_815A,
 SFX_815B,
 SFX_815C,
 SFX_M0_HOWS_THINGS,
 SFX_M0_HEY_THERE,
 SFX_M0_HI_HOW_ARE_YOU,
 SFX_8160,
 SFX_8161,
 SFX_8162,
 SFX_M1_HI_THERE,
 SFX_M1_HOWS_THINGS,
 SFX_M2_HELLO,
 SFX_M2_HEY_WHATS_UP,
 SFX_M0_WHAT_THE_HELL_8167,
 SFX_M1_M2_GEEZ,
 SFX_8169,
 SFX_816A,
 SFX_DOOR_816B,
 SFX_DOOR_816C,
 SFX_DOOR_816D,
 SFX_816E,
 SFX_816F,
 SFX_8170,
 SFX_8171,
 SFX_8172,
 SFX_8173,
 SFX_8174,
 SFX_8175,
 SFX_8176,
 SFX_8177,
 SFX_8178,
 SFX_8179,
 SFX_817A,
 SFX_817B,
 SFX_817C,
 SFX_817D,
 SFX_817E,
 SFX_817F,
 SFX_8180,
 SFX_8181,
 SFX_8182,
 SFX_8183,
 SFX_8184,
 SFX_8185,
 SFX_8186,
 SFX_FOOTSTEP_8187,
 SFX_FOOTSTEP_8188,
 SFX_FOOTSTEP_8189,
 SFX_FOOTSTEP_818A,
 SFX_FOOTSTEP_818B,
 SFX_FOOTSTEP_818C,
 SFX_FOOTSTEP_818D,
 SFX_FOOTSTEP_818E,
 SFX_FOOTSTEP_818F,
 SFX_FOOTSTEP_8190,
 SFX_FOOTSTEP_8191,
 SFX_FOOTSTEP_8192,
 SFX_8193,
 SFX_8194,
 SFX_8195,
 SFX_8196,
 SFX_8197,
 SFX_8198,
 SFX_8199,
 SFX_819A,
 SFX_819B,
 SFX_819C,
 SFX_819D,
 SFX_819E,
 SFX_819F,
 SFX_81A0,
 SFX_81A1,
 SFX_81A2,
 SFX_81A3,
 SFX_81A4,
 SFX_81A5,
 SFX_81A6,
 SFX_81A7,
 SFX_DOOR_81A8,
 SFX_81A9,
 SFX_DOOR_81AA,
 SFX_DOOR_81AB,
 SFX_81AC,
 SFX_DOOR_81AD,
 SFX_DOOR_81AE,
 SFX_DOOR_81AF,
 SFX_DOOR_81B0,
 SFX_DOOR_81B1,
 SFX_DOOR_81B2,
 SFX_DOOR_81B3,
 SFX_DOOR_81B4,
 SFX_DOOR_81B5,
 SFX_DOOR_81B6,
 SFX_DOOR_81B7,
 SFX_DOOR_81B8,
 SFX_81B9,
 SFX_81BA,
 SFX_81BB
};
enum weaponnum {
          WEAPON_NONE,
          WEAPON_UNARMED,
          WEAPON_FALCON2,
          WEAPON_FALCON2_SILENCER,
          WEAPON_FALCON2_SCOPE,
          WEAPON_MAGSEC4,
          WEAPON_MAULER,
          WEAPON_PHOENIX,
          WEAPON_DY357MAGNUM,
          WEAPON_DY357LX,
          WEAPON_CMP150,
          WEAPON_CYCLONE,
          WEAPON_CALLISTO,
          WEAPON_RCP120,
          WEAPON_LAPTOPGUN,
          WEAPON_DRAGON,
          WEAPON_K7AVENGER,
          WEAPON_AR34,
          WEAPON_SUPERDRAGON,
          WEAPON_SHOTGUN,
          WEAPON_REAPER,
          WEAPON_SNIPERRIFLE,
          WEAPON_FARSIGHT,
          WEAPON_DEVASTATOR,
          WEAPON_ROCKETLAUNCHER,
          WEAPON_SLAYER,
          WEAPON_COMBATKNIFE,
          WEAPON_CROSSBOW,
          WEAPON_TRANQUILIZER,
          WEAPON_LASER,
          WEAPON_GRENADE,
          WEAPON_NBOMB,
          WEAPON_TIMEDMINE,
          WEAPON_PROXIMITYMINE,
          WEAPON_REMOTEMINE,
          WEAPON_COMBATBOOST,
          WEAPON_PP9I,
          WEAPON_CC13,
          WEAPON_KL01313,
          WEAPON_KF7SPECIAL,
          WEAPON_ZZT,
          WEAPON_DMC,
          WEAPON_AR53,
          WEAPON_RCP45,
          WEAPON_PSYCHOSISGUN,
          WEAPON_NIGHTVISION,
          WEAPON_EYESPY,
          WEAPON_XRAYSCANNER,
          WEAPON_IRSCANNER,
          WEAPON_CLOAKINGDEVICE,
          WEAPON_HORIZONSCANNER,
          WEAPON_TESTER,
          WEAPON_ROCKETLAUNCHER_34,
          WEAPON_ECMMINE,
          WEAPON_DATAUPLINK,
          WEAPON_RTRACKER,
          WEAPON_PRESIDENTSCANNER,
          WEAPON_DOORDECODER,
          WEAPON_AUTOSURGEON,
          WEAPON_EXPLOSIVES,
          WEAPON_SKEDARBOMB,
          WEAPON_COMMSRIDER,
          WEAPON_TRACERBUG,
          WEAPON_TARGETAMPLIFIER,
          WEAPON_DISGUISE40,
          WEAPON_DISGUISE41,
          WEAPON_FLIGHTPLANS,
          WEAPON_RESEARCHTAPE,
          WEAPON_BACKUPDISK,
          WEAPON_KEYCARD45,
          WEAPON_KEYCARD46,
          WEAPON_KEYCARD47,
          WEAPON_KEYCARD48,
          WEAPON_KEYCARD49,
          WEAPON_KEYCARD4A,
          WEAPON_KEYCARD4B,
          WEAPON_KEYCARD4C,
          WEAPON_SUITCASE,
          WEAPON_SHIELD,
          WEAPON_NECKLACE,
          WEAPON_HAMMER,
          WEAPON_SCREWDRIVER,
          WEAPON_ROCKET,
          WEAPON_HOMINGROCKET,
          WEAPON_GRENADEROUND,
          WEAPON_BOLT,
          WEAPON_BRIEFCASE2,
          WEAPON_SKROCKET,
          WEAPON_59,
          WEAPON_WATCHLASER,
          WEAPON_MPSHIELD,
          WEAPON_DISABLED,
          WEAPON_SUICIDEPILL
};
extern u8 *g_StackStartAddrs[7];
extern u8 *g_StackEndAddrs[7];
extern u8 *g_StackAllocatedPos;
extern u32 var8005ce60;
extern u32 var8005ce64;
extern u32 var8005ce68;
extern u32 var8005ce6c;
extern u32 var8005ce70;
extern s32 var8005ce74;
extern f32 var8005ce78[2];
extern f32 var8005ce80[2];
extern u32 var8005ce88[2];
extern s32 var8005ce90;
extern u32 var8005ce94;
extern u32 var8005ce98;
extern u32 var8005ce9c;
extern OSMesg var8005cea8;
extern u32 var8005cec8;
extern s8 g_Resetting;
extern OSDevMgr __osViDevMgr;
extern u32 var8005cefc;
extern OSDevMgr __osPiDevMgr;
extern OSPiHandle *__osCurrentHandle[2];
extern u32 var8005cf30;
extern OSTime osClockRate;
extern s32 osViClock;
extern u32 _osShutdown;
extern u32 __osGlobalIntMask;
extern u32 var8005cf84;
extern u8 g_LoadType;
extern s32 g_MainIsEndscreen;
extern s32 var8005dd18;
extern u32 var8005dd58;
extern s32 g_SndDisabled;
extern u16 g_SfxVolume;
extern s32 g_SoundMode;
extern s32 var8005ddd4;
extern struct audiorussmapping g_AudioRussMappings[];
extern struct audioconfig g_AudioConfigs[];
extern u32 var8005eedc;
extern f32 var8005ef10[2];
extern u32 var8005ef20;
extern s32 g_NumGlobalAilists;
extern s32 g_NumLvAilists;
extern u32 var8005ef40;
extern u32 var8005ef5c;
extern u32 var8005ef7c;
extern u32 var8005ef90;
extern s32 g_ModelDistanceDisabled;
extern f32 g_ModelDistanceScale;
extern s32 var8005efbc;
extern f32 var8005efc0;
extern s32 (*var8005efc4)(struct model *model, struct modelnode *node);
extern struct gfxvtx *(*g_ModelVtxAllocatorFunc)(s32 numvertices);
extern void *var8005efcc;
extern u32 var8005efd0;
extern s32 var8005efdc;
extern u32 var8005efe0;
extern u32 var8005efec;
extern u32 var8005eff8;
extern u32 var8005f000;
extern u32 var8005f004;
extern s16 g_NumAnimations;
extern struct animheader *g_Anims;
extern u32 var8005f010;
extern u32 var8005f014;
extern u32 var8005f018;
extern u32 var8005f01c;
extern u32 var8005f020;
extern u32 var8005f024;
extern u32 var8005f030;
extern u32 var8005f034;
extern u32 var8005f038;
extern u16 *g_RdpOutBufferEnd;
extern u16 *g_RdpOutBufferStart;
extern struct rdptask *g_RdpCurTask;
extern ALGlobals *n_alGlobals;
extern ALGlobals *n_syn;
extern u32 var8005f120;
extern u32 var8005f124;
extern u32 var8005f128;
extern u32 var8005f12c;
extern u32 var8005f130;
extern u32 var8005f134;
extern u32 var8005f138;
extern u32 var8005f13c;
extern u32 var8005f140;
extern u32 var8005f150;
extern u32 var8005f34c;
extern u32 var8005f4dc;
extern u32 var8005f548;
extern s16 var8005f570[];
extern u32 var8005f66c;
extern u32 var8005f670;
extern u8 *var8005f6f8;
extern u8 *var8005f6fc;
extern u32 var8005f704;
extern OSViMode osViModeTable[];
extern u32 var80060890;
extern OSTimer *__osTimerList;
extern __OSViContext var800608b0[2];
extern __OSViContext *__osViCurr;
extern __OSViContext *__osViNext;
extern u32 __osPiAccessQueueEnabled;
extern u32 __osThreadTail;
extern OSThread *__osRunQueue;
extern OSThread *__osActiveQueue;
extern OSThread *__osRunningThread;
extern OSThread *__osFaultedThread;
extern u32 __osSiAccessQueueEnabled;
extern u32 __osContInitialized;
extern s32 __osPfsLastChannel;
extern s32 g_PfsPrevChannel;
extern u8 g_PfsPrevBank;
extern u32 var80060990;
extern u32 var800609a0;
extern u32 var800609c4;
extern u32 var800609f0;
extern u32 var800611ec;
extern u32 var800611f0;
extern u32 var80061220;
extern OSViMode osViModePalLan1;
extern OSViMode osViModeMpalLan1;
extern OSViMode osViModeNtscLan1;
extern char ldigs[];
extern char udigs[];
extern Gfx var80061360[];
extern Gfx var80061380[];
extern Gfx var800613a0[];
extern f32 var80061630;
extern f32 var80061634;
extern s32 g_NbombsActive;
extern s32 g_WeatherActive;
extern u32 var80062410;
extern s32 var800624a4;
extern u32 var800624b0;
extern s32 g_TitleMode;
extern s32 g_TitleNextMode;
extern u32 g_TitleDelayedTimer;
extern s32 g_TitleDelayedMode;
extern s32 g_TitleTimer;
extern u32 var800624d4;
extern u32 var800624d8;
extern u32 var800624dc;
extern u32 var800624e0;
extern s32 g_TitleNextStage;
extern u32 var800624e8;
extern u32 var800624ec;
extern u32 var800624f0;
extern u32 var800624f4;
extern u8 g_FileState;
extern u8 var80062944;
extern u8 var80062948;
extern u8 var8006294c;
extern u32 var80062950;
extern s32 g_WeatherTickEnabled;
extern void *var80062960;
extern f32 var80062968;
extern s32 var8006296c;
extern s32 g_SelectedAnimNum;
extern u32 var80062974;
extern u32 var80062978;
extern u32 var8006297c;
extern s32 g_NextChrnum;
extern struct chrdata *g_ChrSlots;
extern s32 g_NumChrSlots;
extern struct var80062a8c *var80062a8c;
extern s32 var80062a90;
extern s32 g_NumBondBodies;
extern s32 g_NumMaleGuardHeads;
extern s32 g_NumFemaleGuardHeads;
extern s32 g_NumMaleGuardTeamHeads;
extern s32 g_NumFemaleGuardTeamHeads;
extern s32 var80062b14;
extern s32 var80062b18;
extern s32 g_BondBodies[];
extern s32 g_MaleGuardHeads[];
extern s32 g_MaleGuardTeamHeads[];
extern s32 g_FemaleGuardHeads[];
extern s32 g_FemaleGuardTeamHeads[];
extern s32 var80062c80;
extern s32 g_ActiveMaleHeadsIndex;
extern s32 g_ActiveFemaleHeadsIndex;
extern f32 g_EnemyAccuracyScale;
extern f32 g_DamageReceivedScale;
extern f32 g_DamageDealtScale;
extern f32 var80062cac;
extern s32 var80062cbc;
extern struct animtablerow g_DeathAnimsHumanGun[];
extern struct animtable *g_AnimTablesByRace[];
extern struct attackanimconfig var800656c0[];
extern struct attackanimgroup *g_StandHeavyAttackAnims[][32];
extern struct attackanimconfig var80065be0[];
extern struct attackanimgroup *g_StandLightAttackAnims[][32];
extern struct attackanimconfig var800663d8[];
extern struct attackanimgroup *g_StandDualAttackAnims[][32];
extern struct attackanimgroup *g_KneelHeavyAttackAnims[][32];
extern struct attackanimgroup *g_KneelLightAttackAnims[][32];
extern struct attackanimgroup *g_KneelDualAttackAnims[][32];
extern struct attackanimconfig g_RollAttackAnims[];
extern struct attackanimconfig g_AttackAnimHeavyWalk;
extern struct attackanimconfig g_AttackAnimHeavyRun;
extern struct attackanimconfig g_AttackAnimLightWalk;
extern struct attackanimconfig g_AttackAnimLightRun;
extern struct attackanimconfig g_AttackAnimDualWalk;
extern struct attackanimconfig g_AttackAnimDualRun;
extern u32 g_StageFlags;
extern struct chrdata *g_BgChrs;
extern s16 *g_BgChrnums;
extern s32 g_NumBgChrs;
extern s16 *g_TeamList;
extern s16 *g_SquadronList;
extern struct var80067e6c *var80067fdc[];
extern s16 var80067ff0[];
extern f32 var80069880;
extern u32 g_TintedGlassEnabled;
extern s32 g_AlarmTimer;
extern struct audiohandle *g_AlarmAudioHandle;
extern f32 g_AlarmSpeakerWeight;
extern f32 g_GasReleaseTimer240;
extern s32 g_GasReleasing;
extern struct coord g_GasPos;
extern s32 g_GasLastCough60;
extern f32 g_GasSoundTimer240;
extern struct audiohandle *g_GasAudioHandle;
extern u32 g_CountdownTimerOff;
extern s32 g_CountdownTimerRunning;
extern f32 g_CountdownTimerValue60;
extern u32 var80069910;
extern u32 var80069914;
extern u32 var80069918;
extern struct linkliftdoorobj *g_LiftDoors;
extern struct padlockeddoorobj *g_PadlockedDoors;
extern struct safeitemobj *g_SafeItems;
extern struct linksceneryobj *g_LinkedScenery;
extern struct blockedpathobj *g_BlockedPaths;
extern u32 var80069930;
extern s32 var80069934;
extern f32 g_CameraWaitMultiplier;
extern f32 var8006994c;
extern f32 var80069950;
extern f32 var80069954;
extern f32 var80069958;
extern f32 g_AmmoMultiplier;
extern struct padeffectobj *g_PadEffects;
extern s32 g_LastPadEffectIndex;
extern struct autogunobj *g_ThrownLaptops;
extern struct beam *g_ThrownLaptopBeams;
extern s32 g_MaxThrownLaptops;
extern struct prop *g_Lifts[10];
extern u32 g_TvCmdlist00[];
extern u32 var8006aaa0[];
extern u32 var8006aae4[];
extern struct audiochannel *g_AudioChannels;
extern s8 var8006ae18;
extern s8 var8006ae1c;
extern s8 var8006ae20;
extern s8 var8006ae24;
extern s8 var8006ae28;
extern s32 g_ObjectiveLastIndex;
extern s32 g_ObjectiveChecksDisabled;
extern u16 var8006ae90[];
extern u16 var8006af0c[];
extern u16 var8006af8c[];
extern struct inventory_menupos invmenupos_00010fd0;
extern struct inventory_class invclass_default;
extern struct weapon *g_Weapons[];
extern u32 var800700ac;
extern s32 g_CasingsActive;
extern u32 var800705a0;
extern u32 var800705a4;
extern s32 var800705a8;
extern u32 var800705ac;
extern u32 var800705b0;
extern u32 var800705b4;
extern u32 var800705b8;
extern u32 var800705bc;
extern s32 g_ViMode;
extern s32 g_HiResEnabled;
extern s32 var8007072c;
extern u32 var80070738;
extern u32 var8007073c;
extern struct gecreditsdata *g_CurrentGeCreditsData;
extern u32 var80070744;
extern u32 var80070748;
extern u32 var8007074c;
extern s32 g_PlayersWithControl[];
extern s32 g_PlayerInvincible;
extern s32 g_InCutscene;
extern s16 g_DeathAnimations[];
extern u32 g_NumDeathAnimations;
extern u32 var80071180;
extern u32 var80071184;
extern s32 g_ScissorX1;
extern s32 g_ScissorX2;
extern s32 g_ScissorY1;
extern s32 g_ScissorY2;
extern u32 var800711f0;
extern char g_StringPointer[125];
extern char g_StringPointer2[125];
extern s32 g_MpPlayerNum;
extern u32 var800714d8;
extern u16 g_ControlStyleOptions[];
extern struct menudialog g_PreAndPostMissionBriefingMenuDialog;
extern struct stageoverviewentry g_StageNames[21];
extern struct mission missions[];
extern struct menudialog g_SoloMissionControlStyleMenuDialog;
extern struct menudialog g_CiControlStyleMenuDialog;
extern struct menudialog g_CiControlStylePlayer2MenuDialog;
extern struct menudialog g_ChangeAgentMenuDialog;
extern struct menudialog g_2PMissionOptionsHMenuDialog;
extern struct menudialog g_2PMissionOptionsVMenuDialog;
extern struct menudialog g_FrWeaponsAvailableMenuDialog;
extern struct menudialog g_SoloMissionPauseMenuDialog;
extern struct menudialog g_2PMissionPauseHMenuDialog;
extern struct menudialog g_2PMissionPauseVMenuDialog;
extern struct cutscene g_Cutscenes[];
extern struct menudialog g_CiMenuViaPcMenuDialog;
extern struct menudialog g_CiMenuViaPauseMenuDialog;
extern struct menudialog g_CheatsMenuDialog;
extern struct menudialog g_PakChoosePakMenuDialog;
extern struct menudialog g_FilemgrFileSelect4MbMenuDialog;
extern struct menudialog g_MpQuickGo4MbMenuDialog;
extern struct menudialog g_MpConfirmChallenge4MbMenuDialog;
extern struct menudialog g_MainMenu4MbMenuDialog;
extern struct menudialog g_MpEditSimulant4MbMenuDialog;
extern struct menudialog g_AdvancedSetup4MbMenuDialog;
extern struct filelist *g_FileLists[];
extern s32 var80075bd0[];
extern struct var80075c00 var80075c00[];
extern s32 var80075d60;
extern u32 var80075d64;
extern u32 var80075d68;
extern s32 var80075d78;
extern u8 unregistered_function1[];
extern u8 unregistered_function2[];
extern u8 unregistered_function3[];
extern struct ailist g_GlobalAilists[];
extern struct modeltype g_ModelTypeDropship;
extern struct modeltype g_ModelTypeSkShuttle;
extern struct modeltype g_ModelTypeLift;
extern struct modeltype g_ModelTypeCctv;
extern struct modeltype g_ModelTypeTerminal;
extern struct modeltype g_ModelTypeCiHub;
extern struct modeltype g_ModelType19;
extern struct modeltype g_ModelTypeAutogun;
extern struct modeltype g_ModelType11;
extern struct modeltype g_ModelType13;
extern struct modeltype g_ModelType18;
extern struct modeltype g_ModelType12;
extern struct modeltype g_ModelTypeWindowedDoor;
extern struct modeltype g_ModelType17;
extern struct modeltype g_ModelType0C;
extern struct modeltype g_ModelTypeLogo;
extern struct modeltype g_ModelTypePdLogo;
extern struct modeltype g_ModelTypeHoverbike;
extern struct modeltype g_ModelTypeJumpship;
extern struct modeltype g_ModelTypeChopper;
extern struct modeltype g_ModelTypeRope;
extern struct modeltype g_ModelTypeBanner;
extern struct modeltype g_ModelTypeMaianUfo;
extern struct modeltype g_ModelTypeCableCar;
extern struct modeltype g_ModelTypeSubmarine;
extern struct modeltype g_ModelTypeTarget;
extern struct modeltype g_ModelTypeRareLogo;
extern struct modeltype g_ModelTypeWireFence;
extern struct modeltype g_ModelTypeBB;
extern struct modelstate g_ModelStates[441];
extern u8 propexplosiontypes[];
extern struct modeltype g_ModelTypeBasic;
extern struct modeltype g_ModelTypeChrGun;
extern struct modeltype g_ModelType0B;
extern struct modelfiledata g_PlayerModelFileData;
extern struct modeltype g_ModelType0A;
extern struct modeltype g_ModelType20;
extern struct modeltype g_ModelTypeClassicGun;
extern struct modeltype g_ModelTypeCasing;
extern struct modeltype g_ModelType06;
extern struct modeltype g_ModelTypeUzi;
extern struct modeltype g_ModelTypeJoypad;
extern struct modeltype g_ModelType21;
extern struct modeltype g_ModelTypeLaptopGun;
extern struct modeltype g_ModelTypeK7Avenger;
extern struct modeltype g_ModelTypeFalcon2;
extern struct modeltype g_ModelTypeKnife;
extern struct modeltype g_ModelTypeCmp150;
extern struct modeltype g_ModelTypeDragon;
extern struct modeltype g_ModelTypeSuperDragon;
extern struct modeltype g_ModelTypeRocket;
extern struct modeltype g_ModelType4A;
extern struct modeltype g_ModelTypeShotgun;
extern struct modeltype g_ModelTypeFarsight;
extern struct modeltype g_ModelType4D;
extern struct modeltype g_ModelTypeReaper;
extern struct modeltype g_ModelTypeMauler;
extern struct modeltype g_ModelTypeDevastator;
extern struct modeltype g_ModelTypePistol;
extern struct modeltype g_ModelTypeAr34;
extern struct modeltype g_ModelTypeMagnum;
extern struct modeltype g_ModelTypeSlayerRocket;
extern struct modeltype g_ModelTypeCyclone;
extern struct modeltype g_ModelTypeSniperRifle;
extern struct modeltype g_ModelTypeTranquilizer;
extern struct modeltype g_ModelTypeCrossbow;
extern struct modeltype g_ModelTypeTimedProxyMine;
extern struct modeltype g_ModelTypePhoenix;
extern struct modeltype g_ModelTypeCallisto;
extern struct modeltype g_ModelTypeRcp120;
extern struct modeltype g_ModelTypeHudPiece;
extern struct modeltype g_ModelTypeHand;
extern struct modeltype g_ModelTypeLaser;
extern struct modeltype g_ModelTypeGrenade;
extern struct modeltype g_ModelTypeEcmMine;
extern struct modeltype g_ModelTypeUplink;
extern struct modeltype g_ModelTypeRemoteMine;
extern struct modeltype g_ModelTypeChr;
extern struct modeltype g_ModelTypeSkedar;
extern struct modeltype g_ModelTypeDrCaroll;
extern struct modeltype g_ModelType22;
extern struct modeltype g_ModelTypeRobot;
extern struct headorbody g_HeadsAndBodies[];
extern u32 var8007dae4;
extern f32 var8007db80;
extern f32 var8007db84;
extern u32 var8007db88;
extern u32 var8007db94;
extern u32 var8007dba0;
extern u32 var8007dbb8;
extern u32 var8007dbd0;
extern u32 var8007dbe8;
extern u32 var8007dbf4;
extern u32 var8007dc00;
extern struct var8007e3d0 var8007e3d0[];
extern u32 var8007e4a0;
extern u32 var8007e4a4;
extern f32 g_ExplosionDamageReceivedScale;
extern struct sparktype g_SparkTypes[];
extern s32 g_SparksAreActive;
extern struct weatherdata *g_WeatherData;
extern s32 g_NextShardNum;
extern s32 g_ShardsActive;
extern s32 g_ScaleX;
extern struct font *g_FontNumeric2;
extern struct font2a4 *g_FontNumeric1;
extern struct font *g_FontHandelGothicXs2;
extern struct font2a4 *g_FontHandelGothicXs1;
extern struct font *g_FontHandelGothicSm2;
extern struct font2a4 *g_FontHandelGothicSm1;
extern struct font *g_FontHandelGothicMd2;
extern struct font2a4 *g_FontHandelGothicMd1;
extern struct font *g_FontHandelGothicLg2;
extern struct font2a4 *g_FontHandelGothicLg1;
extern u32 var8007fb9c;
extern s32 g_StageIndex;
extern s16 var8007fc0c;
extern struct var800a4640_00 *var8007fc24;
extern u16 var8007fc3c;
extern s32 g_NumPortalThings;
extern f32 var8007fcb4;
extern struct stagetableentry g_Stages[61];
extern s32 var80082050;
extern u32 g_GfxNumSwaps;
extern s32 g_NumReasonsToEndMpMatch;
extern u32 var800840c4;
extern u32 var800840d8;
extern u32 var800840e0;
extern u32 var800840e4;
extern u32 var800840f0;
extern u32 var800840f4;
extern u32 var800840f8;
extern u32 var800840fc;
extern s32 g_Jpn;
extern struct surfacetype *g_SurfaceTypes[15];
extern void *var800844f0;
extern f32 var800845d4;
extern u32 var800845dc;
extern struct menudialog g_2PMissionInventoryHMenuDialog;
extern struct menudialog g_2PMissionInventoryVMenuDialog;
extern struct menudialog g_MpEndscreenChallengeCheatedMenuDialog;
extern struct menudialog g_MpEndscreenChallengeFailedMenuDialog;
extern struct menudialog g_MpDropOutMenuDialog;
extern struct mparena g_MpArenas[];
extern struct menudialog g_MpWeaponsMenuDialog;
extern struct menudialog g_MpPlayerOptionsMenuDialog;
extern struct menudialog g_MpControlMenuDialog;
extern struct menudialog g_MpPlayerStatsMenuDialog;
extern struct menudialog g_MpPlayerNameMenuDialog;
extern struct menudialog g_MpLoadSettingsMenuDialog;
extern struct menudialog g_MpLoadPresetMenuDialog;
extern struct menudialog g_MpLoadPlayerMenuDialog;
extern struct menudialog g_MpArenaMenuDialog;
extern struct menudialog g_MpLimitsMenuDialog;
extern struct menudialog g_MpHandicapsMenuDialog;
extern struct menudialog g_MpReadyMenuDialog;
extern struct menudialog g_MpSimulantsMenuDialog;
extern struct menudialog g_MpTeamsMenuDialog;
extern struct menudialog g_MpChallengeListOrDetailsMenuDialog;
extern struct menudialog g_MpChallengeListOrDetailsViaAdvChallengeMenuDialog;
extern struct menudialog g_MpAdvancedSetupMenuDialog;
extern struct menudialog g_MpQuickGoMenuDialog;
extern struct menudialog g_MpQuickTeamGameSetupMenuDialog;
extern struct menudialog g_MpQuickTeamMenuDialog;
extern struct menudialog g_CombatSimulatorMenuDialog;
extern struct menudialog g_MpCombatOptionsMenuDialog;
extern struct menudialog g_MpBriefcaseOptionsMenuDialog;
extern struct menudialog g_MpCaptureOptionsMenuDialog;
extern struct menudialog g_MpHillOptionsMenuDialog;
extern struct menudialog g_MpHackerOptionsMenuDialog;
extern struct menudialog g_MpPopacapOptionsMenuDialog;
extern struct mpscenariooverview g_MpScenarioOverviews[];
extern struct menudialog g_MpScenarioMenuDialog;
extern struct menudialog g_MpQuickTeamScenarioMenuDialog;
extern s32 var80087260;
extern u32 var80087264;
extern struct mpweapon g_MpWeapons[0x27];
extern struct mphead g_MpHeads[75];
extern struct mpsimulanttype g_MpSimulantTypes[];
extern struct mpbody g_MpBodies[61];
extern struct mppreset g_MpPresets[14];
extern u32 g_TeamColours[];
extern u16 var80087ce4[];
extern u8 g_NumMpSimulantChrs;
extern struct aibotweaponpreference g_AibotWeaponPreferences[];
extern u32 var80087eb4;
extern struct challenge g_MpChallenges[30];
extern u8 g_FrIsValidWeapon;
extern s32 g_FrWeaponNum;
extern u8 g_ChrBioSlot;
extern u8 g_HangarBioSlot;
extern u8 g_DtSlot;
extern u8 var80088bb4;
extern struct menudialog g_FrWeaponListMenuDialog;
extern struct menudialog g_FrTrainingInfoInGameMenuDialog;
extern struct menudialog g_FrTrainingInfoPreGameMenuDialog;
extern struct menudialog g_FrCompletedMenuDialog;
extern struct menudialog g_FrFailedMenuDialog;
extern struct menudialog g_BioListMenuDialog;
extern struct menudialog g_DtListMenuDialog;
extern struct menudialog g_DtDetailsMenuDialog;
extern struct menudialog g_DtFailedMenuDialog;
extern struct menudialog g_DtCompletedMenuDialog;
extern struct menudialog g_HtListMenuDialog;
extern struct menudialog g_HtDetailsMenuDialog;
extern struct menudialog g_HtFailedMenuDialog;
extern struct menudialog g_HtCompletedMenuDialog;
extern struct menudialog g_HangarListMenuDialog;
enum l_ame {
 L_AME_000 = 0x200,
 L_AME_001,
 L_AME_002,
 L_AME_003,
 L_AME_004,
 L_AME_005,
 L_AME_006,
 L_AME_007,
 L_AME_008,
 L_AME_009,
 L_AME_010,
 L_AME_011,
 L_AME_012,
 L_AME_013,
 L_AME_014,
 L_AME_015,
 L_AME_016,
 L_AME_017,
 L_AME_018,
 L_AME_019,
 L_AME_020,
 L_AME_021,
 L_AME_022,
 L_AME_023,
 L_AME_024,
 L_AME_025,
 L_AME_026,
 L_AME_027,
 L_AME_028,
 L_AME_029,
 L_AME_030,
 L_AME_031,
 L_AME_032,
 L_AME_033,
 L_AME_034,
 L_AME_035,
 L_AME_036,
 L_AME_037,
 L_AME_038,
 L_AME_039,
 L_AME_040,
 L_AME_041,
 L_AME_042,
 L_AME_043,
 L_AME_044,
 L_AME_045,
 L_AME_046,
 L_AME_047,
 L_AME_048,
 L_AME_049,
 L_AME_050,
 L_AME_051,
 L_AME_052,
 L_AME_053,
 L_AME_054,
 L_AME_055,
 L_AME_056,
 L_AME_057,
 L_AME_058,
 L_AME_059,
 L_AME_060,
 L_AME_061,
 L_AME_062,
 L_AME_063,
 L_AME_064,
 L_AME_065,
 L_AME_066,
 L_AME_067,
 L_AME_068,
 L_AME_069,
 L_AME_070,
 L_AME_071,
 L_AME_072,
 L_AME_073,
 L_AME_074,
 L_AME_075,
 L_AME_076,
 L_AME_077,
 L_AME_078,
 L_AME_079,
 L_AME_080,
 L_AME_081,
 L_AME_082,
 L_AME_083,
 L_AME_084,
 L_AME_085,
 L_AME_086,
 L_AME_087,
 L_AME_088,
 L_AME_089,
 L_AME_090,
 L_AME_091,
 L_AME_092,
 L_AME_093,
 L_AME_094,
 L_AME_095,
 L_AME_096,
 L_AME_097,
 L_AME_098,
 L_AME_099,
 L_AME_100,
 L_AME_101,
 L_AME_102,
 L_AME_103,
 L_AME_104,
 L_AME_105,
 L_AME_106,
 L_AME_107,
 L_AME_108,
 L_AME_109,
 L_AME_110,
 L_AME_111,
 L_AME_END
};
enum l_arch {
 L_ARCH_END
};
enum l_ark {
 L_ARK_000 = 0x600,
 L_ARK_001,
 L_ARK_002,
 L_ARK_003,
 L_ARK_004,
 L_ARK_005,
 L_ARK_006,
 L_ARK_007,
 L_ARK_008,
 L_ARK_009,
 L_ARK_010,
 L_ARK_011,
 L_ARK_012,
 L_ARK_013,
 L_ARK_014,
 L_ARK_015,
 L_ARK_016,
 L_ARK_017,
 L_ARK_018,
 L_ARK_019,
 L_ARK_020,
 L_ARK_021,
 L_ARK_022,
 L_ARK_023,
 L_ARK_024,
 L_ARK_025,
 L_ARK_026,
 L_ARK_027,
 L_ARK_028,
 L_ARK_029,
 L_ARK_030,
 L_ARK_031,
 L_ARK_032,
 L_ARK_033,
 L_ARK_034,
 L_ARK_035,
 L_ARK_036,
 L_ARK_037,
 L_ARK_038,
 L_ARK_039,
 L_ARK_040,
 L_ARK_041,
 L_ARK_042,
 L_ARK_043,
 L_ARK_044,
 L_ARK_045,
 L_ARK_046,
 L_ARK_047,
 L_ARK_048,
 L_ARK_049,
 L_ARK_050,
 L_ARK_051,
 L_ARK_052,
 L_ARK_053,
 L_ARK_054,
 L_ARK_055,
 L_ARK_056,
 L_ARK_057,
 L_ARK_058,
 L_ARK_059,
 L_ARK_060,
 L_ARK_061,
 L_ARK_062,
 L_ARK_063,
 L_ARK_064,
 L_ARK_065,
 L_ARK_066,
 L_ARK_067,
 L_ARK_068,
 L_ARK_069,
 L_ARK_070,
 L_ARK_071,
 L_ARK_072,
 L_ARK_073,
 L_ARK_074,
 L_ARK_075,
 L_ARK_END
};
enum l_ash {
 L_ASH_END
};
enum l_azt {
 L_AZT_000 = 0xa00,
 L_AZT_001,
 L_AZT_002,
 L_AZT_003,
 L_AZT_004,
 L_AZT_005,
 L_AZT_006,
 L_AZT_007,
 L_AZT_008,
 L_AZT_009,
 L_AZT_010,
 L_AZT_011,
 L_AZT_012,
 L_AZT_013,
 L_AZT_014,
 L_AZT_015,
 L_AZT_016,
 L_AZT_017,
 L_AZT_018,
 L_AZT_019,
 L_AZT_020,
 L_AZT_021,
 L_AZT_022,
 L_AZT_023,
 L_AZT_024,
 L_AZT_025,
 L_AZT_026,
 L_AZT_027,
 L_AZT_028,
 L_AZT_029,
 L_AZT_030,
 L_AZT_031,
 L_AZT_032,
 L_AZT_033,
 L_AZT_034,
 L_AZT_035,
 L_AZT_036,
 L_AZT_037,
 L_AZT_038,
 L_AZT_039,
 L_AZT_040,
 L_AZT_041,
 L_AZT_042,
 L_AZT_043,
 L_AZT_044,
 L_AZT_045,
 L_AZT_046,
 L_AZT_047,
 L_AZT_END
};
enum l_cat {
 L_CAT_END
};
enum l_cave {
 L_CAVE_000 = 0xe00,
 L_CAVE_001,
 L_CAVE_002,
 L_CAVE_003,
 L_CAVE_004,
 L_CAVE_005,
 L_CAVE_006,
 L_CAVE_007,
 L_CAVE_008,
 L_CAVE_009,
 L_CAVE_010,
 L_CAVE_011,
 L_CAVE_012,
 L_CAVE_013,
 L_CAVE_014,
 L_CAVE_015,
 L_CAVE_016,
 L_CAVE_017,
 L_CAVE_018,
 L_CAVE_019,
 L_CAVE_020,
 L_CAVE_021,
 L_CAVE_022,
 L_CAVE_023,
 L_CAVE_024,
 L_CAVE_025,
 L_CAVE_026,
 L_CAVE_027,
 L_CAVE_028,
 L_CAVE_029,
 L_CAVE_030,
 L_CAVE_031,
 L_CAVE_032,
 L_CAVE_033,
 L_CAVE_034,
 L_CAVE_035,
 L_CAVE_036,
 L_CAVE_037,
 L_CAVE_038,
 L_CAVE_039,
 L_CAVE_040,
 L_CAVE_041,
 L_CAVE_042,
 L_CAVE_043,
 L_CAVE_044,
 L_CAVE_045,
 L_CAVE_046,
 L_CAVE_047,
 L_CAVE_048,
 L_CAVE_049,
 L_CAVE_050,
 L_CAVE_051,
 L_CAVE_052,
 L_CAVE_053,
 L_CAVE_054,
 L_CAVE_055,
 L_CAVE_056,
 L_CAVE_057,
 L_CAVE_058,
 L_CAVE_059,
 L_CAVE_060,
 L_CAVE_061,
 L_CAVE_062,
 L_CAVE_063,
 L_CAVE_064,
 L_CAVE_065,
 L_CAVE_066,
 L_CAVE_067,
 L_CAVE_END
};
enum l_arec {
 L_AREC_END
};
enum l_crad {
 L_CRAD_END
};
enum l_cryp {
 L_CRYP_END
};
enum l_dam {
 L_DAM_000 = 0x1600,
 L_DAM_001,
 L_DAM_002,
 L_DAM_003,
 L_DAM_004,
 L_DAM_005,
 L_DAM_006,
 L_DAM_007,
 L_DAM_008,
 L_DAM_009,
 L_DAM_010,
 L_DAM_011,
 L_DAM_012,
 L_DAM_013,
 L_DAM_014,
 L_DAM_015,
 L_DAM_016,
 L_DAM_017,
 L_DAM_018,
 L_DAM_019,
 L_DAM_020,
 L_DAM_021,
 L_DAM_022,
 L_DAM_023,
 L_DAM_024,
 L_DAM_025,
 L_DAM_026,
 L_DAM_027,
 L_DAM_028,
 L_DAM_029,
 L_DAM_030,
 L_DAM_031,
 L_DAM_032,
 L_DAM_033,
 L_DAM_034,
 L_DAM_035,
 L_DAM_036,
 L_DAM_037,
 L_DAM_038,
 L_DAM_039,
 L_DAM_040,
 L_DAM_041,
 L_DAM_042,
 L_DAM_043,
 L_DAM_044,
 L_DAM_045,
 L_DAM_046,
 L_DAM_047,
 L_DAM_END
};
enum l_depo {
 L_DEPO_000 = 0x1800,
 L_DEPO_001,
 L_DEPO_002,
 L_DEPO_003,
 L_DEPO_004,
 L_DEPO_005,
 L_DEPO_006,
 L_DEPO_007,
 L_DEPO_008,
 L_DEPO_009,
 L_DEPO_010,
 L_DEPO_011,
 L_DEPO_012,
 L_DEPO_013,
 L_DEPO_014,
 L_DEPO_015,
 L_DEPO_016,
 L_DEPO_017,
 L_DEPO_018,
 L_DEPO_019,
 L_DEPO_020,
 L_DEPO_021,
 L_DEPO_022,
 L_DEPO_023,
 L_DEPO_024,
 L_DEPO_025,
 L_DEPO_026,
 L_DEPO_027,
 L_DEPO_028,
 L_DEPO_029,
 L_DEPO_030,
 L_DEPO_031,
 L_DEPO_032,
 L_DEPO_033,
 L_DEPO_034,
 L_DEPO_035,
 L_DEPO_036,
 L_DEPO_037,
 L_DEPO_038,
 L_DEPO_039,
 L_DEPO_040,
 L_DEPO_041,
 L_DEPO_042,
 L_DEPO_043,
 L_DEPO_044,
 L_DEPO_045,
 L_DEPO_046,
 L_DEPO_047,
 L_DEPO_048,
 L_DEPO_049,
 L_DEPO_050,
 L_DEPO_051,
 L_DEPO_052,
 L_DEPO_053,
 L_DEPO_054,
 L_DEPO_055,
 L_DEPO_056,
 L_DEPO_057,
 L_DEPO_058,
 L_DEPO_059,
 L_DEPO_060,
 L_DEPO_061,
 L_DEPO_062,
 L_DEPO_063,
 L_DEPO_064,
 L_DEPO_065,
 L_DEPO_066,
 L_DEPO_067,
 L_DEPO_068,
 L_DEPO_069,
 L_DEPO_070,
 L_DEPO_071,
 L_DEPO_072,
 L_DEPO_073,
 L_DEPO_074,
 L_DEPO_075,
 L_DEPO_076,
 L_DEPO_077,
 L_DEPO_078,
 L_DEPO_079,
 L_DEPO_080,
 L_DEPO_081,
 L_DEPO_082,
 L_DEPO_083,
 L_DEPO_END
};
enum l_dest {
 L_DEST_END
};
enum l_dish {
 L_DISH_000 = 0x1c00,
 L_DISH_001,
 L_DISH_002,
 L_DISH_003,
 L_DISH_004,
 L_DISH_005,
 L_DISH_006,
 L_DISH_007,
 L_DISH_008,
 L_DISH_009,
 L_DISH_010,
 L_DISH_011,
 L_DISH_012,
 L_DISH_013,
 L_DISH_014,
 L_DISH_015,
 L_DISH_016,
 L_DISH_017,
 L_DISH_018,
 L_DISH_019,
 L_DISH_020,
 L_DISH_021,
 L_DISH_022,
 L_DISH_023,
 L_DISH_024,
 L_DISH_025,
 L_DISH_026,
 L_DISH_027,
 L_DISH_028,
 L_DISH_029,
 L_DISH_030,
 L_DISH_031,
 L_DISH_032,
 L_DISH_033,
 L_DISH_034,
 L_DISH_035,
 L_DISH_036,
 L_DISH_037,
 L_DISH_038,
 L_DISH_039,
 L_DISH_040,
 L_DISH_041,
 L_DISH_042,
 L_DISH_043,
 L_DISH_044,
 L_DISH_045,
 L_DISH_046,
 L_DISH_047,
 L_DISH_048,
 L_DISH_049,
 L_DISH_050,
 L_DISH_051,
 L_DISH_052,
 L_DISH_053,
 L_DISH_054,
 L_DISH_055,
 L_DISH_056,
 L_DISH_057,
 L_DISH_058,
 L_DISH_059,
 L_DISH_060,
 L_DISH_061,
 L_DISH_062,
 L_DISH_063,
 L_DISH_064,
 L_DISH_065,
 L_DISH_066,
 L_DISH_067,
 L_DISH_068,
 L_DISH_069,
 L_DISH_070,
 L_DISH_071,
 L_DISH_072,
 L_DISH_073,
 L_DISH_074,
 L_DISH_075,
 L_DISH_076,
 L_DISH_077,
 L_DISH_078,
 L_DISH_079,
 L_DISH_080,
 L_DISH_081,
 L_DISH_082,
 L_DISH_083,
 L_DISH_084,
 L_DISH_085,
 L_DISH_086,
 L_DISH_087,
 L_DISH_088,
 L_DISH_089,
 L_DISH_090,
 L_DISH_091,
 L_DISH_092,
 L_DISH_093,
 L_DISH_094,
 L_DISH_095,
 L_DISH_096,
 L_DISH_097,
 L_DISH_098,
 L_DISH_099,
 L_DISH_100,
 L_DISH_101,
 L_DISH_102,
 L_DISH_103,
 L_DISH_104,
 L_DISH_105,
 L_DISH_106,
 L_DISH_107,
 L_DISH_108,
 L_DISH_109,
 L_DISH_110,
 L_DISH_111,
 L_DISH_112,
 L_DISH_113,
 L_DISH_114,
 L_DISH_115,
 L_DISH_116,
 L_DISH_117,
 L_DISH_118,
 L_DISH_119,
 L_DISH_120,
 L_DISH_121,
 L_DISH_122,
 L_DISH_123,
 L_DISH_124,
 L_DISH_125,
 L_DISH_126,
 L_DISH_127,
 L_DISH_END
};
enum l_ear {
 L_EAR_000 = 0x1e00,
 L_EAR_001,
 L_EAR_002,
 L_EAR_003,
 L_EAR_004,
 L_EAR_005,
 L_EAR_006,
 L_EAR_007,
 L_EAR_008,
 L_EAR_009,
 L_EAR_010,
 L_EAR_011,
 L_EAR_012,
 L_EAR_013,
 L_EAR_014,
 L_EAR_015,
 L_EAR_016,
 L_EAR_017,
 L_EAR_018,
 L_EAR_019,
 L_EAR_020,
 L_EAR_021,
 L_EAR_022,
 L_EAR_023,
 L_EAR_024,
 L_EAR_025,
 L_EAR_026,
 L_EAR_027,
 L_EAR_028,
 L_EAR_029,
 L_EAR_030,
 L_EAR_031,
 L_EAR_032,
 L_EAR_033,
 L_EAR_034,
 L_EAR_035,
 L_EAR_036,
 L_EAR_037,
 L_EAR_038,
 L_EAR_039,
 L_EAR_040,
 L_EAR_041,
 L_EAR_042,
 L_EAR_043,
 L_EAR_044,
 L_EAR_045,
 L_EAR_046,
 L_EAR_047,
 L_EAR_048,
 L_EAR_049,
 L_EAR_050,
 L_EAR_051,
 L_EAR_052,
 L_EAR_053,
 L_EAR_054,
 L_EAR_055,
 L_EAR_056,
 L_EAR_057,
 L_EAR_058,
 L_EAR_059,
 L_EAR_060,
 L_EAR_061,
 L_EAR_062,
 L_EAR_063,
 L_EAR_064,
 L_EAR_065,
 L_EAR_066,
 L_EAR_067,
 L_EAR_068,
 L_EAR_069,
 L_EAR_070,
 L_EAR_071,
 L_EAR_072,
 L_EAR_073,
 L_EAR_074,
 L_EAR_075,
 L_EAR_076,
 L_EAR_077,
 L_EAR_078,
 L_EAR_079,
 L_EAR_080,
 L_EAR_081,
 L_EAR_082,
 L_EAR_083,
 L_EAR_084,
 L_EAR_085,
 L_EAR_086,
 L_EAR_087,
 L_EAR_088,
 L_EAR_089,
 L_EAR_090,
 L_EAR_091,
 L_EAR_092,
 L_EAR_093,
 L_EAR_094,
 L_EAR_095,
 L_EAR_096,
 L_EAR_097,
 L_EAR_098,
 L_EAR_099,
 L_EAR_100,
 L_EAR_101,
 L_EAR_102,
 L_EAR_103,
 L_EAR_104,
 L_EAR_105,
 L_EAR_106,
 L_EAR_107,
 L_EAR_END
};
enum l_eld {
 L_ELD_000 = 0x2000,
 L_ELD_001,
 L_ELD_002,
 L_ELD_003,
 L_ELD_004,
 L_ELD_005,
 L_ELD_006,
 L_ELD_007,
 L_ELD_008,
 L_ELD_009,
 L_ELD_010,
 L_ELD_011,
 L_ELD_012,
 L_ELD_013,
 L_ELD_014,
 L_ELD_015,
 L_ELD_016,
 L_ELD_017,
 L_ELD_018,
 L_ELD_019,
 L_ELD_020,
 L_ELD_021,
 L_ELD_022,
 L_ELD_023,
 L_ELD_024,
 L_ELD_025,
 L_ELD_026,
 L_ELD_027,
 L_ELD_028,
 L_ELD_029,
 L_ELD_030,
 L_ELD_031,
 L_ELD_032,
 L_ELD_033,
 L_ELD_034,
 L_ELD_035,
 L_ELD_036,
 L_ELD_037,
 L_ELD_038,
 L_ELD_039,
 L_ELD_040,
 L_ELD_041,
 L_ELD_042,
 L_ELD_043,
 L_ELD_044,
 L_ELD_045,
 L_ELD_046,
 L_ELD_047,
 L_ELD_048,
 L_ELD_049,
 L_ELD_050,
 L_ELD_051,
 L_ELD_END
};
enum l_imp {
 L_IMP_000 = 0x2200,
 L_IMP_001,
 L_IMP_002,
 L_IMP_003,
 L_IMP_004,
 L_IMP_005,
 L_IMP_006,
 L_IMP_007,
 L_IMP_008,
 L_IMP_009,
 L_IMP_010,
 L_IMP_011,
 L_IMP_012,
 L_IMP_013,
 L_IMP_014,
 L_IMP_015,
 L_IMP_016,
 L_IMP_017,
 L_IMP_018,
 L_IMP_019,
 L_IMP_020,
 L_IMP_021,
 L_IMP_022,
 L_IMP_023,
 L_IMP_024,
 L_IMP_025,
 L_IMP_026,
 L_IMP_027,
 L_IMP_028,
 L_IMP_029,
 L_IMP_030,
 L_IMP_031,
 L_IMP_032,
 L_IMP_033,
 L_IMP_034,
 L_IMP_035,
 L_IMP_036,
 L_IMP_037,
 L_IMP_038,
 L_IMP_039,
 L_IMP_040,
 L_IMP_041,
 L_IMP_042,
 L_IMP_043,
 L_IMP_044,
 L_IMP_045,
 L_IMP_046,
 L_IMP_047,
 L_IMP_048,
 L_IMP_049,
 L_IMP_050,
 L_IMP_051,
 L_IMP_052,
 L_IMP_053,
 L_IMP_054,
 L_IMP_055,
 L_IMP_056,
 L_IMP_057,
 L_IMP_058,
 L_IMP_059,
 L_IMP_060,
 L_IMP_061,
 L_IMP_062,
 L_IMP_063,
 L_IMP_END
};
enum l_jun {
 L_JUN_END
};
enum l_lee {
 L_LEE_000 = 0x2600,
 L_LEE_001,
 L_LEE_002,
 L_LEE_003,
 L_LEE_004,
 L_LEE_005,
 L_LEE_006,
 L_LEE_007,
 L_LEE_008,
 L_LEE_009,
 L_LEE_010,
 L_LEE_011,
 L_LEE_012,
 L_LEE_013,
 L_LEE_014,
 L_LEE_015,
 L_LEE_016,
 L_LEE_017,
 L_LEE_018,
 L_LEE_019,
 L_LEE_020,
 L_LEE_021,
 L_LEE_022,
 L_LEE_023,
 L_LEE_024,
 L_LEE_025,
 L_LEE_026,
 L_LEE_027,
 L_LEE_028,
 L_LEE_029,
 L_LEE_030,
 L_LEE_031,
 L_LEE_032,
 L_LEE_033,
 L_LEE_034,
 L_LEE_035,
 L_LEE_036,
 L_LEE_037,
 L_LEE_038,
 L_LEE_039,
 L_LEE_040,
 L_LEE_041,
 L_LEE_042,
 L_LEE_043,
 L_LEE_044,
 L_LEE_045,
 L_LEE_046,
 L_LEE_047,
 L_LEE_048,
 L_LEE_049,
 L_LEE_050,
 L_LEE_051,
 L_LEE_052,
 L_LEE_053,
 L_LEE_054,
 L_LEE_055,
 L_LEE_END
};
enum l_len {
 L_LEN_END
};
enum l_lip {
 L_LIP_000 = 0x2a00,
 L_LIP_001,
 L_LIP_002,
 L_LIP_003,
 L_LIP_004,
 L_LIP_005,
 L_LIP_006,
 L_LIP_007,
 L_LIP_008,
 L_LIP_009,
 L_LIP_010,
 L_LIP_011,
 L_LIP_012,
 L_LIP_013,
 L_LIP_014,
 L_LIP_015,
 L_LIP_016,
 L_LIP_017,
 L_LIP_018,
 L_LIP_019,
 L_LIP_020,
 L_LIP_021,
 L_LIP_022,
 L_LIP_023,
 L_LIP_024,
 L_LIP_025,
 L_LIP_026,
 L_LIP_027,
 L_LIP_028,
 L_LIP_029,
 L_LIP_030,
 L_LIP_031,
 L_LIP_032,
 L_LIP_033,
 L_LIP_034,
 L_LIP_035,
 L_LIP_036,
 L_LIP_037,
 L_LIP_038,
 L_LIP_039,
 L_LIP_040,
 L_LIP_041,
 L_LIP_042,
 L_LIP_043,
 L_LIP_044,
 L_LIP_045,
 L_LIP_046,
 L_LIP_047,
 L_LIP_048,
 L_LIP_049,
 L_LIP_050,
 L_LIP_051,
 L_LIP_052,
 L_LIP_053,
 L_LIP_054,
 L_LIP_055,
 L_LIP_056,
 L_LIP_057,
 L_LIP_058,
 L_LIP_059,
 L_LIP_060,
 L_LIP_061,
 L_LIP_062,
 L_LIP_063,
 L_LIP_064,
 L_LIP_065,
 L_LIP_066,
 L_LIP_067,
 L_LIP_068,
 L_LIP_069,
 L_LIP_070,
 L_LIP_071,
 L_LIP_072,
 L_LIP_073,
 L_LIP_074,
 L_LIP_075,
 L_LIP_076,
 L_LIP_077,
 L_LIP_078,
 L_LIP_079,
 L_LIP_080,
 L_LIP_081,
 L_LIP_082,
 L_LIP_083,
 L_LIP_084,
 L_LIP_085,
 L_LIP_086,
 L_LIP_087,
 L_LIP_088,
 L_LIP_089,
 L_LIP_090,
 L_LIP_091,
 L_LIP_092,
 L_LIP_093,
 L_LIP_094,
 L_LIP_095,
 L_LIP_096,
 L_LIP_097,
 L_LIP_098,
 L_LIP_099,
 L_LIP_100,
 L_LIP_101,
 L_LIP_102,
 L_LIP_103,
 L_LIP_104,
 L_LIP_105,
 L_LIP_106,
 L_LIP_107,
 L_LIP_END
};
enum l_lue {
 L_LUE_000 = 0x2c00,
 L_LUE_001,
 L_LUE_002,
 L_LUE_003,
 L_LUE_004,
 L_LUE_005,
 L_LUE_006,
 L_LUE_007,
 L_LUE_008,
 L_LUE_009,
 L_LUE_010,
 L_LUE_011,
 L_LUE_012,
 L_LUE_013,
 L_LUE_014,
 L_LUE_015,
 L_LUE_016,
 L_LUE_017,
 L_LUE_018,
 L_LUE_019,
 L_LUE_020,
 L_LUE_021,
 L_LUE_022,
 L_LUE_023,
 L_LUE_024,
 L_LUE_025,
 L_LUE_026,
 L_LUE_027,
 L_LUE_028,
 L_LUE_029,
 L_LUE_030,
 L_LUE_031,
 L_LUE_032,
 L_LUE_033,
 L_LUE_034,
 L_LUE_035,
 L_LUE_036,
 L_LUE_037,
 L_LUE_038,
 L_LUE_039,
 L_LUE_040,
 L_LUE_041,
 L_LUE_042,
 L_LUE_043,
 L_LUE_044,
 L_LUE_045,
 L_LUE_046,
 L_LUE_047,
 L_LUE_048,
 L_LUE_049,
 L_LUE_050,
 L_LUE_051,
 L_LUE_052,
 L_LUE_053,
 L_LUE_054,
 L_LUE_055,
 L_LUE_056,
 L_LUE_057,
 L_LUE_058,
 L_LUE_059,
 L_LUE_060,
 L_LUE_061,
 L_LUE_062,
 L_LUE_063,
 L_LUE_064,
 L_LUE_065,
 L_LUE_066,
 L_LUE_067,
 L_LUE_068,
 L_LUE_069,
 L_LUE_070,
 L_LUE_071,
 L_LUE_072,
 L_LUE_073,
 L_LUE_074,
 L_LUE_075,
 L_LUE_END
};
enum l_oat {
 L_OAT_END
};
enum l_pam {
 L_PAM_000 = 0x3000,
 L_PAM_001,
 L_PAM_002,
 L_PAM_003,
 L_PAM_004,
 L_PAM_005,
 L_PAM_006,
 L_PAM_007,
 L_PAM_008,
 L_PAM_009,
 L_PAM_010,
 L_PAM_011,
 L_PAM_012,
 L_PAM_013,
 L_PAM_014,
 L_PAM_015,
 L_PAM_016,
 L_PAM_017,
 L_PAM_018,
 L_PAM_019,
 L_PAM_020,
 L_PAM_021,
 L_PAM_022,
 L_PAM_023,
 L_PAM_024,
 L_PAM_025,
 L_PAM_026,
 L_PAM_027,
 L_PAM_028,
 L_PAM_029,
 L_PAM_030,
 L_PAM_031,
 L_PAM_032,
 L_PAM_033,
 L_PAM_034,
 L_PAM_035,
 L_PAM_036,
 L_PAM_037,
 L_PAM_038,
 L_PAM_039,
 L_PAM_040,
 L_PAM_041,
 L_PAM_042,
 L_PAM_043,
 L_PAM_044,
 L_PAM_045,
 L_PAM_046,
 L_PAM_047,
 L_PAM_END
};
enum l_pete {
 L_PETE_000 = 0x3200,
 L_PETE_001,
 L_PETE_002,
 L_PETE_003,
 L_PETE_004,
 L_PETE_005,
 L_PETE_006,
 L_PETE_007,
 L_PETE_008,
 L_PETE_009,
 L_PETE_010,
 L_PETE_011,
 L_PETE_012,
 L_PETE_013,
 L_PETE_014,
 L_PETE_015,
 L_PETE_016,
 L_PETE_017,
 L_PETE_018,
 L_PETE_019,
 L_PETE_020,
 L_PETE_021,
 L_PETE_022,
 L_PETE_023,
 L_PETE_024,
 L_PETE_025,
 L_PETE_026,
 L_PETE_027,
 L_PETE_028,
 L_PETE_029,
 L_PETE_030,
 L_PETE_031,
 L_PETE_032,
 L_PETE_033,
 L_PETE_034,
 L_PETE_035,
 L_PETE_036,
 L_PETE_037,
 L_PETE_038,
 L_PETE_039,
 L_PETE_040,
 L_PETE_041,
 L_PETE_042,
 L_PETE_043,
 L_PETE_044,
 L_PETE_045,
 L_PETE_046,
 L_PETE_047,
 L_PETE_048,
 L_PETE_049,
 L_PETE_050,
 L_PETE_051,
 L_PETE_052,
 L_PETE_053,
 L_PETE_054,
 L_PETE_055,
 L_PETE_056,
 L_PETE_057,
 L_PETE_058,
 L_PETE_059,
 L_PETE_060,
 L_PETE_061,
 L_PETE_062,
 L_PETE_063,
 L_PETE_064,
 L_PETE_065,
 L_PETE_066,
 L_PETE_067,
 L_PETE_068,
 L_PETE_069,
 L_PETE_070,
 L_PETE_071,
 L_PETE_072,
 L_PETE_073,
 L_PETE_074,
 L_PETE_075,
 L_PETE_END
};
enum l_ref {
 L_REF_END
};
enum l_rit {
 L_RIT_000 = 0x3600,
 L_RIT_001,
 L_RIT_002,
 L_RIT_003,
 L_RIT_004,
 L_RIT_005,
 L_RIT_006,
 L_RIT_007,
 L_RIT_008,
 L_RIT_009,
 L_RIT_010,
 L_RIT_011,
 L_RIT_012,
 L_RIT_013,
 L_RIT_014,
 L_RIT_015,
 L_RIT_016,
 L_RIT_017,
 L_RIT_018,
 L_RIT_019,
 L_RIT_020,
 L_RIT_021,
 L_RIT_022,
 L_RIT_023,
 L_RIT_024,
 L_RIT_025,
 L_RIT_026,
 L_RIT_027,
 L_RIT_028,
 L_RIT_029,
 L_RIT_030,
 L_RIT_031,
 L_RIT_032,
 L_RIT_033,
 L_RIT_034,
 L_RIT_035,
 L_RIT_036,
 L_RIT_037,
 L_RIT_038,
 L_RIT_039,
 L_RIT_040,
 L_RIT_041,
 L_RIT_042,
 L_RIT_043,
 L_RIT_044,
 L_RIT_045,
 L_RIT_046,
 L_RIT_047,
 L_RIT_048,
 L_RIT_049,
 L_RIT_050,
 L_RIT_051,
 L_RIT_052,
 L_RIT_053,
 L_RIT_054,
 L_RIT_055,
 L_RIT_056,
 L_RIT_057,
 L_RIT_058,
 L_RIT_059,
 L_RIT_060,
 L_RIT_061,
 L_RIT_062,
 L_RIT_063,
 L_RIT_064,
 L_RIT_065,
 L_RIT_066,
 L_RIT_067,
 L_RIT_068,
 L_RIT_069,
 L_RIT_070,
 L_RIT_071,
 L_RIT_072,
 L_RIT_073,
 L_RIT_074,
 L_RIT_075,
 L_RIT_076,
 L_RIT_077,
 L_RIT_078,
 L_RIT_079,
 L_RIT_080,
 L_RIT_081,
 L_RIT_082,
 L_RIT_083,
 L_RIT_084,
 L_RIT_085,
 L_RIT_086,
 L_RIT_087,
 L_RIT_END
};
enum l_run {
 L_RUN_END
};
enum l_sevb {
 L_SEVB_000 = 0x3a00,
 L_SEVB_001,
 L_SEVB_002,
 L_SEVB_003,
 L_SEVB_004,
 L_SEVB_005,
 L_SEVB_006,
 L_SEVB_007,
 L_SEVB_END
};
enum l_sev {
 L_SEV_000 = 0x3c00,
 L_SEV_001,
 L_SEV_002,
 L_SEV_003,
 L_SEV_004,
 L_SEV_005,
 L_SEV_006,
 L_SEV_007,
 L_SEV_008,
 L_SEV_009,
 L_SEV_010,
 L_SEV_011,
 L_SEV_012,
 L_SEV_013,
 L_SEV_014,
 L_SEV_015,
 L_SEV_END
};
enum l_sevx {
 L_SEVX_END
};
enum l_sevxb {
 L_SEVXB_END
};
enum l_sho {
 L_SHO_000 = 0x4200,
 L_SHO_001,
 L_SHO_002,
 L_SHO_003,
 L_SHO_004,
 L_SHO_005,
 L_SHO_006,
 L_SHO_007,
 L_SHO_008,
 L_SHO_009,
 L_SHO_010,
 L_SHO_011,
 L_SHO_012,
 L_SHO_013,
 L_SHO_014,
 L_SHO_015,
 L_SHO_016,
 L_SHO_017,
 L_SHO_018,
 L_SHO_019,
 L_SHO_020,
 L_SHO_021,
 L_SHO_022,
 L_SHO_023,
 L_SHO_024,
 L_SHO_025,
 L_SHO_026,
 L_SHO_027,
 L_SHO_028,
 L_SHO_029,
 L_SHO_030,
 L_SHO_031,
 L_SHO_032,
 L_SHO_033,
 L_SHO_034,
 L_SHO_035,
 L_SHO_036,
 L_SHO_037,
 L_SHO_038,
 L_SHO_039,
 L_SHO_040,
 L_SHO_041,
 L_SHO_042,
 L_SHO_043,
 L_SHO_044,
 L_SHO_045,
 L_SHO_046,
 L_SHO_047,
 L_SHO_048,
 L_SHO_049,
 L_SHO_050,
 L_SHO_051,
 L_SHO_052,
 L_SHO_053,
 L_SHO_054,
 L_SHO_055,
 L_SHO_END
};
enum l_silo {
 L_SILO_END
};
enum l_stat {
 L_STAT_000 = 0x4600,
 L_STAT_001,
 L_STAT_002,
 L_STAT_003,
 L_STAT_004,
 L_STAT_005,
 L_STAT_006,
 L_STAT_007,
 L_STAT_008,
 L_STAT_009,
 L_STAT_010,
 L_STAT_011,
 L_STAT_END
};
enum l_tra {
 L_TRA_000 = 0x4800,
 L_TRA_001,
 L_TRA_002,
 L_TRA_003,
 L_TRA_004,
 L_TRA_005,
 L_TRA_006,
 L_TRA_007,
 L_TRA_008,
 L_TRA_009,
 L_TRA_010,
 L_TRA_011,
 L_TRA_012,
 L_TRA_013,
 L_TRA_014,
 L_TRA_015,
 L_TRA_016,
 L_TRA_017,
 L_TRA_018,
 L_TRA_019,
 L_TRA_020,
 L_TRA_021,
 L_TRA_022,
 L_TRA_023,
 L_TRA_024,
 L_TRA_025,
 L_TRA_026,
 L_TRA_027,
 L_TRA_028,
 L_TRA_029,
 L_TRA_030,
 L_TRA_031,
 L_TRA_032,
 L_TRA_033,
 L_TRA_034,
 L_TRA_035,
 L_TRA_036,
 L_TRA_037,
 L_TRA_038,
 L_TRA_039,
 L_TRA_040,
 L_TRA_041,
 L_TRA_042,
 L_TRA_043,
 L_TRA_044,
 L_TRA_045,
 L_TRA_046,
 L_TRA_047,
 L_TRA_048,
 L_TRA_049,
 L_TRA_050,
 L_TRA_051,
 L_TRA_052,
 L_TRA_053,
 L_TRA_054,
 L_TRA_055,
 L_TRA_056,
 L_TRA_057,
 L_TRA_058,
 L_TRA_059,
 L_TRA_060,
 L_TRA_061,
 L_TRA_062,
 L_TRA_063,
 L_TRA_064,
 L_TRA_065,
 L_TRA_066,
 L_TRA_067,
 L_TRA_068,
 L_TRA_069,
 L_TRA_070,
 L_TRA_071,
 L_TRA_072,
 L_TRA_073,
 L_TRA_074,
 L_TRA_075,
 L_TRA_076,
 L_TRA_077,
 L_TRA_078,
 L_TRA_079,
 L_TRA_080,
 L_TRA_081,
 L_TRA_082,
 L_TRA_083,
 L_TRA_084,
 L_TRA_085,
 L_TRA_086,
 L_TRA_087,
 L_TRA_END
};
enum l_wax {
 L_WAX_000 = 0x4a00,
 L_WAX_001,
 L_WAX_002,
 L_WAX_003,
 L_WAX_004,
 L_WAX_005,
 L_WAX_006,
 L_WAX_007,
 L_WAX_008,
 L_WAX_009,
 L_WAX_010,
 L_WAX_011,
 L_WAX_012,
 L_WAX_013,
 L_WAX_014,
 L_WAX_015,
 L_WAX_016,
 L_WAX_017,
 L_WAX_018,
 L_WAX_019,
 L_WAX_020,
 L_WAX_021,
 L_WAX_022,
 L_WAX_023,
 L_WAX_024,
 L_WAX_025,
 L_WAX_026,
 L_WAX_027,
 L_WAX_END
};
enum l_gun {
 L_GUN_000 = 0x4c00,
 L_GUN_001,
 L_GUN_002,
 L_GUN_003,
 L_GUN_004,
 L_GUN_005,
 L_GUN_006,
 L_GUN_007,
 L_GUN_008,
 L_GUN_009,
 L_GUN_010,
 L_GUN_011,
 L_GUN_012,
 L_GUN_013,
 L_GUN_014,
 L_GUN_015,
 L_GUN_016,
 L_GUN_017,
 L_GUN_018,
 L_GUN_019,
 L_GUN_020,
 L_GUN_021,
 L_GUN_022,
 L_GUN_023,
 L_GUN_024,
 L_GUN_025,
 L_GUN_026,
 L_GUN_027,
 L_GUN_028,
 L_GUN_029,
 L_GUN_030,
 L_GUN_031,
 L_GUN_032,
 L_GUN_033,
 L_GUN_034,
 L_GUN_035,
 L_GUN_036,
 L_GUN_037,
 L_GUN_038,
 L_GUN_039,
 L_GUN_040,
 L_GUN_041,
 L_GUN_042,
 L_GUN_043,
 L_GUN_044,
 L_GUN_045,
 L_GUN_046,
 L_GUN_047,
 L_GUN_048,
 L_GUN_049,
 L_GUN_050,
 L_GUN_051,
 L_GUN_052,
 L_GUN_053,
 L_GUN_054,
 L_GUN_055,
 L_GUN_056,
 L_GUN_057,
 L_GUN_058,
 L_GUN_059,
 L_GUN_060,
 L_GUN_061,
 L_GUN_062,
 L_GUN_063,
 L_GUN_064,
 L_GUN_065,
 L_GUN_066,
 L_GUN_067,
 L_GUN_068,
 L_GUN_069,
 L_GUN_070,
 L_GUN_071,
 L_GUN_072,
 L_GUN_073,
 L_GUN_074,
 L_GUN_075,
 L_GUN_076,
 L_GUN_077,
 L_GUN_078,
 L_GUN_079,
 L_GUN_080,
 L_GUN_081,
 L_GUN_082,
 L_GUN_083,
 L_GUN_084,
 L_GUN_085,
 L_GUN_086,
 L_GUN_087,
 L_GUN_088,
 L_GUN_089,
 L_GUN_090,
 L_GUN_091,
 L_GUN_092,
 L_GUN_093,
 L_GUN_094,
 L_GUN_095,
 L_GUN_096,
 L_GUN_097,
 L_GUN_098,
 L_GUN_099,
 L_GUN_100,
 L_GUN_101,
 L_GUN_102,
 L_GUN_103,
 L_GUN_104,
 L_GUN_105,
 L_GUN_106,
 L_GUN_107,
 L_GUN_108,
 L_GUN_109,
 L_GUN_110,
 L_GUN_111,
 L_GUN_112,
 L_GUN_113,
 L_GUN_114,
 L_GUN_115,
 L_GUN_116,
 L_GUN_117,
 L_GUN_118,
 L_GUN_119,
 L_GUN_120,
 L_GUN_121,
 L_GUN_122,
 L_GUN_123,
 L_GUN_124,
 L_GUN_125,
 L_GUN_126,
 L_GUN_127,
 L_GUN_128,
 L_GUN_129,
 L_GUN_130,
 L_GUN_131,
 L_GUN_132,
 L_GUN_133,
 L_GUN_134,
 L_GUN_135,
 L_GUN_136,
 L_GUN_137,
 L_GUN_138,
 L_GUN_139,
 L_GUN_140,
 L_GUN_141,
 L_GUN_142,
 L_GUN_143,
 L_GUN_144,
 L_GUN_145,
 L_GUN_146,
 L_GUN_147,
 L_GUN_148,
 L_GUN_149,
 L_GUN_150,
 L_GUN_151,
 L_GUN_152,
 L_GUN_153,
 L_GUN_154,
 L_GUN_155,
 L_GUN_156,
 L_GUN_157,
 L_GUN_158,
 L_GUN_159,
 L_GUN_160,
 L_GUN_161,
 L_GUN_162,
 L_GUN_163,
 L_GUN_164,
 L_GUN_165,
 L_GUN_166,
 L_GUN_167,
 L_GUN_168,
 L_GUN_169,
 L_GUN_170,
 L_GUN_171,
 L_GUN_172,
 L_GUN_173,
 L_GUN_174,
 L_GUN_175,
 L_GUN_176,
 L_GUN_177,
 L_GUN_178,
 L_GUN_179,
 L_GUN_180,
 L_GUN_181,
 L_GUN_182,
 L_GUN_183,
 L_GUN_184,
 L_GUN_185,
 L_GUN_186,
 L_GUN_187,
 L_GUN_188,
 L_GUN_189,
 L_GUN_190,
 L_GUN_191,
 L_GUN_192,
 L_GUN_193,
 L_GUN_194,
 L_GUN_195,
 L_GUN_196,
 L_GUN_197,
 L_GUN_198,
 L_GUN_199,
 L_GUN_200,
 L_GUN_201,
 L_GUN_202,
 L_GUN_203,
 L_GUN_204,
 L_GUN_205,
 L_GUN_206,
 L_GUN_207,
 L_GUN_208,
 L_GUN_209,
 L_GUN_210,
 L_GUN_211,
 L_GUN_212,
 L_GUN_213,
 L_GUN_214,
 L_GUN_215,
 L_GUN_216,
 L_GUN_217,
 L_GUN_218,
 L_GUN_219,
 L_GUN_220,
 L_GUN_221,
 L_GUN_222,
 L_GUN_223,
 L_GUN_224,
 L_GUN_225,
 L_GUN_226,
 L_GUN_227,
 L_GUN_228,
 L_GUN_229,
 L_GUN_230,
 L_GUN_231,
 L_GUN_232,
 L_GUN_233,
 L_GUN_234,
 L_GUN_235,
 L_GUN_236,
 L_GUN_237,
 L_GUN_238,
 L_GUN_239,
 L_GUN_240,
 L_GUN_241,
 L_GUN_242,
 L_GUN_243,
 L_GUN_END
};
enum l_title {
 L_TITLE_000 = 0x4e00,
 L_TITLE_001,
 L_TITLE_002,
 L_TITLE_003,
 L_TITLE_004,
 L_TITLE_005,
 L_TITLE_006,
 L_TITLE_007,
 L_TITLE_008,
 L_TITLE_009,
 L_TITLE_010,
 L_TITLE_011,
 L_TITLE_012,
 L_TITLE_013,
 L_TITLE_014,
 L_TITLE_015,
 L_TITLE_016,
 L_TITLE_017,
 L_TITLE_018,
 L_TITLE_019,
 L_TITLE_020,
 L_TITLE_021,
 L_TITLE_022,
 L_TITLE_023,
 L_TITLE_024,
 L_TITLE_025,
 L_TITLE_026,
 L_TITLE_027,
 L_TITLE_028,
 L_TITLE_029,
 L_TITLE_030,
 L_TITLE_031,
 L_TITLE_032,
 L_TITLE_033,
 L_TITLE_034,
 L_TITLE_035,
 L_TITLE_036,
 L_TITLE_037,
 L_TITLE_038,
 L_TITLE_039,
 L_TITLE_040,
 L_TITLE_041,
 L_TITLE_042,
 L_TITLE_043,
 L_TITLE_044,
 L_TITLE_045,
 L_TITLE_046,
 L_TITLE_047,
 L_TITLE_048,
 L_TITLE_049,
 L_TITLE_050,
 L_TITLE_051,
 L_TITLE_052,
 L_TITLE_053,
 L_TITLE_054,
 L_TITLE_055,
 L_TITLE_056,
 L_TITLE_057,
 L_TITLE_058,
 L_TITLE_059,
 L_TITLE_060,
 L_TITLE_061,
 L_TITLE_062,
 L_TITLE_063,
 L_TITLE_064,
 L_TITLE_065,
 L_TITLE_066,
 L_TITLE_067,
 L_TITLE_068,
 L_TITLE_069,
 L_TITLE_070,
 L_TITLE_071,
 L_TITLE_072,
 L_TITLE_073,
 L_TITLE_074,
 L_TITLE_075,
 L_TITLE_076,
 L_TITLE_077,
 L_TITLE_078,
 L_TITLE_079,
 L_TITLE_080,
 L_TITLE_081,
 L_TITLE_082,
 L_TITLE_083,
 L_TITLE_084,
 L_TITLE_085,
 L_TITLE_086,
 L_TITLE_087,
 L_TITLE_088,
 L_TITLE_089,
 L_TITLE_090,
 L_TITLE_091,
 L_TITLE_092,
 L_TITLE_093,
 L_TITLE_094,
 L_TITLE_095,
 L_TITLE_096,
 L_TITLE_097,
 L_TITLE_098,
 L_TITLE_099,
 L_TITLE_100,
 L_TITLE_101,
 L_TITLE_102,
 L_TITLE_103,
 L_TITLE_104,
 L_TITLE_105,
 L_TITLE_106,
 L_TITLE_107,
 L_TITLE_108,
 L_TITLE_109,
 L_TITLE_110,
 L_TITLE_111,
 L_TITLE_112,
 L_TITLE_113,
 L_TITLE_114,
 L_TITLE_115,
 L_TITLE_116,
 L_TITLE_117,
 L_TITLE_118,
 L_TITLE_119,
 L_TITLE_120,
 L_TITLE_121,
 L_TITLE_122,
 L_TITLE_123,
 L_TITLE_124,
 L_TITLE_125,
 L_TITLE_126,
 L_TITLE_127,
 L_TITLE_128,
 L_TITLE_129,
 L_TITLE_130,
 L_TITLE_131,
 L_TITLE_132,
 L_TITLE_133,
 L_TITLE_134,
 L_TITLE_135,
 L_TITLE_136,
 L_TITLE_137,
 L_TITLE_138,
 L_TITLE_139,
 L_TITLE_140,
 L_TITLE_141,
 L_TITLE_142,
 L_TITLE_143,
 L_TITLE_144,
 L_TITLE_145,
 L_TITLE_146,
 L_TITLE_147,
 L_TITLE_148,
 L_TITLE_149,
 L_TITLE_150,
 L_TITLE_151,
 L_TITLE_END
};
enum l_mpmenu {
 L_MPMENU_000 = 0x5000,
 L_MPMENU_001,
 L_MPMENU_002,
 L_MPMENU_003,
 L_MPMENU_004,
 L_MPMENU_005,
 L_MPMENU_006,
 L_MPMENU_007,
 L_MPMENU_008,
 L_MPMENU_009,
 L_MPMENU_010,
 L_MPMENU_011,
 L_MPMENU_012,
 L_MPMENU_013,
 L_MPMENU_014,
 L_MPMENU_015,
 L_MPMENU_016,
 L_MPMENU_017,
 L_MPMENU_018,
 L_MPMENU_019,
 L_MPMENU_020,
 L_MPMENU_021,
 L_MPMENU_022,
 L_MPMENU_023,
 L_MPMENU_024,
 L_MPMENU_025,
 L_MPMENU_026,
 L_MPMENU_027,
 L_MPMENU_028,
 L_MPMENU_029,
 L_MPMENU_030,
 L_MPMENU_031,
 L_MPMENU_032,
 L_MPMENU_033,
 L_MPMENU_034,
 L_MPMENU_035,
 L_MPMENU_036,
 L_MPMENU_037,
 L_MPMENU_038,
 L_MPMENU_039,
 L_MPMENU_040,
 L_MPMENU_041,
 L_MPMENU_042,
 L_MPMENU_043,
 L_MPMENU_044,
 L_MPMENU_045,
 L_MPMENU_046,
 L_MPMENU_047,
 L_MPMENU_048,
 L_MPMENU_049,
 L_MPMENU_050,
 L_MPMENU_051,
 L_MPMENU_052,
 L_MPMENU_053,
 L_MPMENU_054,
 L_MPMENU_055,
 L_MPMENU_056,
 L_MPMENU_057,
 L_MPMENU_058,
 L_MPMENU_059,
 L_MPMENU_060,
 L_MPMENU_061,
 L_MPMENU_062,
 L_MPMENU_063,
 L_MPMENU_064,
 L_MPMENU_065,
 L_MPMENU_066,
 L_MPMENU_067,
 L_MPMENU_068,
 L_MPMENU_069,
 L_MPMENU_070,
 L_MPMENU_071,
 L_MPMENU_072,
 L_MPMENU_073,
 L_MPMENU_074,
 L_MPMENU_075,
 L_MPMENU_076,
 L_MPMENU_077,
 L_MPMENU_078,
 L_MPMENU_079,
 L_MPMENU_080,
 L_MPMENU_081,
 L_MPMENU_082,
 L_MPMENU_083,
 L_MPMENU_084,
 L_MPMENU_085,
 L_MPMENU_086,
 L_MPMENU_087,
 L_MPMENU_088,
 L_MPMENU_089,
 L_MPMENU_090,
 L_MPMENU_091,
 L_MPMENU_092,
 L_MPMENU_093,
 L_MPMENU_094,
 L_MPMENU_095,
 L_MPMENU_096,
 L_MPMENU_097,
 L_MPMENU_098,
 L_MPMENU_099,
 L_MPMENU_100,
 L_MPMENU_101,
 L_MPMENU_102,
 L_MPMENU_103,
 L_MPMENU_104,
 L_MPMENU_105,
 L_MPMENU_106,
 L_MPMENU_107,
 L_MPMENU_108,
 L_MPMENU_109,
 L_MPMENU_110,
 L_MPMENU_111,
 L_MPMENU_112,
 L_MPMENU_113,
 L_MPMENU_114,
 L_MPMENU_115,
 L_MPMENU_116,
 L_MPMENU_117,
 L_MPMENU_118,
 L_MPMENU_119,
 L_MPMENU_120,
 L_MPMENU_121,
 L_MPMENU_122,
 L_MPMENU_123,
 L_MPMENU_124,
 L_MPMENU_125,
 L_MPMENU_126,
 L_MPMENU_127,
 L_MPMENU_128,
 L_MPMENU_129,
 L_MPMENU_130,
 L_MPMENU_131,
 L_MPMENU_132,
 L_MPMENU_133,
 L_MPMENU_134,
 L_MPMENU_135,
 L_MPMENU_136,
 L_MPMENU_137,
 L_MPMENU_138,
 L_MPMENU_139,
 L_MPMENU_140,
 L_MPMENU_141,
 L_MPMENU_142,
 L_MPMENU_143,
 L_MPMENU_144,
 L_MPMENU_145,
 L_MPMENU_146,
 L_MPMENU_147,
 L_MPMENU_148,
 L_MPMENU_149,
 L_MPMENU_150,
 L_MPMENU_151,
 L_MPMENU_152,
 L_MPMENU_153,
 L_MPMENU_154,
 L_MPMENU_155,
 L_MPMENU_156,
 L_MPMENU_157,
 L_MPMENU_158,
 L_MPMENU_159,
 L_MPMENU_160,
 L_MPMENU_161,
 L_MPMENU_162,
 L_MPMENU_163,
 L_MPMENU_164,
 L_MPMENU_165,
 L_MPMENU_166,
 L_MPMENU_167,
 L_MPMENU_168,
 L_MPMENU_169,
 L_MPMENU_170,
 L_MPMENU_171,
 L_MPMENU_172,
 L_MPMENU_173,
 L_MPMENU_174,
 L_MPMENU_175,
 L_MPMENU_176,
 L_MPMENU_177,
 L_MPMENU_178,
 L_MPMENU_179,
 L_MPMENU_180,
 L_MPMENU_181,
 L_MPMENU_182,
 L_MPMENU_183,
 L_MPMENU_184,
 L_MPMENU_185,
 L_MPMENU_186,
 L_MPMENU_187,
 L_MPMENU_188,
 L_MPMENU_189,
 L_MPMENU_190,
 L_MPMENU_191,
 L_MPMENU_192,
 L_MPMENU_193,
 L_MPMENU_194,
 L_MPMENU_195,
 L_MPMENU_196,
 L_MPMENU_197,
 L_MPMENU_198,
 L_MPMENU_199,
 L_MPMENU_200,
 L_MPMENU_201,
 L_MPMENU_202,
 L_MPMENU_203,
 L_MPMENU_204,
 L_MPMENU_205,
 L_MPMENU_206,
 L_MPMENU_207,
 L_MPMENU_208,
 L_MPMENU_209,
 L_MPMENU_210,
 L_MPMENU_211,
 L_MPMENU_212,
 L_MPMENU_213,
 L_MPMENU_214,
 L_MPMENU_215,
 L_MPMENU_216,
 L_MPMENU_217,
 L_MPMENU_218,
 L_MPMENU_219,
 L_MPMENU_220,
 L_MPMENU_221,
 L_MPMENU_222,
 L_MPMENU_223,
 L_MPMENU_224,
 L_MPMENU_225,
 L_MPMENU_226,
 L_MPMENU_227,
 L_MPMENU_228,
 L_MPMENU_229,
 L_MPMENU_230,
 L_MPMENU_231,
 L_MPMENU_232,
 L_MPMENU_233,
 L_MPMENU_234,
 L_MPMENU_235,
 L_MPMENU_236,
 L_MPMENU_237,
 L_MPMENU_238,
 L_MPMENU_239,
 L_MPMENU_240,
 L_MPMENU_241,
 L_MPMENU_242,
 L_MPMENU_243,
 L_MPMENU_244,
 L_MPMENU_245,
 L_MPMENU_246,
 L_MPMENU_247,
 L_MPMENU_248,
 L_MPMENU_249,
 L_MPMENU_250,
 L_MPMENU_251,
 L_MPMENU_252,
 L_MPMENU_253,
 L_MPMENU_254,
 L_MPMENU_255,
 L_MPMENU_256,
 L_MPMENU_257,
 L_MPMENU_258,
 L_MPMENU_259,
 L_MPMENU_260,
 L_MPMENU_261,
 L_MPMENU_262,
 L_MPMENU_263,
 L_MPMENU_264,
 L_MPMENU_265,
 L_MPMENU_266,
 L_MPMENU_267,
 L_MPMENU_268,
 L_MPMENU_269,
 L_MPMENU_270,
 L_MPMENU_271,
 L_MPMENU_272,
 L_MPMENU_273,
 L_MPMENU_274,
 L_MPMENU_275,
 L_MPMENU_276,
 L_MPMENU_277,
 L_MPMENU_278,
 L_MPMENU_279,
 L_MPMENU_280,
 L_MPMENU_281,
 L_MPMENU_282,
 L_MPMENU_283,
 L_MPMENU_284,
 L_MPMENU_285,
 L_MPMENU_286,
 L_MPMENU_287,
 L_MPMENU_288,
 L_MPMENU_289,
 L_MPMENU_290,
 L_MPMENU_291,
 L_MPMENU_292,
 L_MPMENU_293,
 L_MPMENU_294,
 L_MPMENU_295,
 L_MPMENU_296,
 L_MPMENU_297,
 L_MPMENU_298,
 L_MPMENU_299,
 L_MPMENU_300,
 L_MPMENU_301,
 L_MPMENU_302,
 L_MPMENU_303,
 L_MPMENU_304,
 L_MPMENU_305,
 L_MPMENU_306,
 L_MPMENU_307,
 L_MPMENU_308,
 L_MPMENU_309,
 L_MPMENU_310,
 L_MPMENU_311,
 L_MPMENU_312,
 L_MPMENU_313,
 L_MPMENU_314,
 L_MPMENU_315,
 L_MPMENU_316,
 L_MPMENU_317,
 L_MPMENU_318,
 L_MPMENU_319,
 L_MPMENU_320,
 L_MPMENU_321,
 L_MPMENU_322,
 L_MPMENU_323,
 L_MPMENU_324,
 L_MPMENU_325,
 L_MPMENU_326,
 L_MPMENU_327,
 L_MPMENU_328,
 L_MPMENU_329,
 L_MPMENU_330,
 L_MPMENU_331,
 L_MPMENU_332,
 L_MPMENU_333,
 L_MPMENU_334,
 L_MPMENU_335,
 L_MPMENU_336,
 L_MPMENU_337,
 L_MPMENU_338,
 L_MPMENU_339,
 L_MPMENU_340,
 L_MPMENU_341,
 L_MPMENU_342,
 L_MPMENU_343,
 L_MPMENU_344,
 L_MPMENU_345,
 L_MPMENU_346,
 L_MPMENU_347,
 L_MPMENU_348,
 L_MPMENU_349,
 L_MPMENU_350,
 L_MPMENU_351,
 L_MPMENU_352,
 L_MPMENU_353,
 L_MPMENU_354,
 L_MPMENU_355,
 L_MPMENU_356,
 L_MPMENU_357,
 L_MPMENU_358,
 L_MPMENU_359,
 L_MPMENU_360,
 L_MPMENU_361,
 L_MPMENU_362,
 L_MPMENU_363,
 L_MPMENU_364,
 L_MPMENU_365,
 L_MPMENU_366,
 L_MPMENU_367,
 L_MPMENU_368,
 L_MPMENU_369,
 L_MPMENU_370,
 L_MPMENU_371,
 L_MPMENU_372,
 L_MPMENU_373,
 L_MPMENU_374,
 L_MPMENU_375,
 L_MPMENU_376,
 L_MPMENU_377,
 L_MPMENU_378,
 L_MPMENU_379,
 L_MPMENU_380,
 L_MPMENU_381,
 L_MPMENU_382,
 L_MPMENU_383,
 L_MPMENU_384,
 L_MPMENU_385,
 L_MPMENU_386,
 L_MPMENU_387,
 L_MPMENU_388,
 L_MPMENU_389,
 L_MPMENU_390,
 L_MPMENU_391,
 L_MPMENU_392,
 L_MPMENU_393,
 L_MPMENU_394,
 L_MPMENU_395,
 L_MPMENU_396,
 L_MPMENU_397,
 L_MPMENU_398,
 L_MPMENU_399,
 L_MPMENU_400,
 L_MPMENU_401,
 L_MPMENU_402,
 L_MPMENU_403,
 L_MPMENU_404,
 L_MPMENU_405,
 L_MPMENU_406,
 L_MPMENU_407,
 L_MPMENU_408,
 L_MPMENU_409,
 L_MPMENU_410,
 L_MPMENU_411,
 L_MPMENU_412,
 L_MPMENU_413,
 L_MPMENU_414,
 L_MPMENU_415,
 L_MPMENU_416,
 L_MPMENU_417,
 L_MPMENU_418,
 L_MPMENU_419,
 L_MPMENU_420,
 L_MPMENU_421,
 L_MPMENU_422,
 L_MPMENU_423,
 L_MPMENU_424,
 L_MPMENU_425,
 L_MPMENU_426,
 L_MPMENU_427,
 L_MPMENU_428,
 L_MPMENU_429,
 L_MPMENU_430,
 L_MPMENU_431,
 L_MPMENU_432,
 L_MPMENU_433,
 L_MPMENU_434,
 L_MPMENU_435,
 L_MPMENU_436,
 L_MPMENU_437,
 L_MPMENU_438,
 L_MPMENU_439,
 L_MPMENU_440,
 L_MPMENU_441,
 L_MPMENU_442,
 L_MPMENU_443,
 L_MPMENU_444,
 L_MPMENU_445,
 L_MPMENU_446,
 L_MPMENU_447,
 L_MPMENU_448,
 L_MPMENU_449,
 L_MPMENU_450,
 L_MPMENU_451,
 L_MPMENU_452,
 L_MPMENU_453,
 L_MPMENU_454,
 L_MPMENU_455,
 L_MPMENU_456,
 L_MPMENU_457,
 L_MPMENU_458,
 L_MPMENU_459,
 L_MPMENU_460,
 L_MPMENU_461,
 L_MPMENU_462,
 L_MPMENU_463,
 L_MPMENU_464,
 L_MPMENU_465,
 L_MPMENU_466,
 L_MPMENU_467,
 L_MPMENU_468,
 L_MPMENU_469,
 L_MPMENU_470,
 L_MPMENU_471,
 L_MPMENU_472,
 L_MPMENU_473,
 L_MPMENU_474,
 L_MPMENU_475,
 L_MPMENU_476,
 L_MPMENU_477,
 L_MPMENU_478,
 L_MPMENU_479,
 L_MPMENU_480,
 L_MPMENU_481,
 L_MPMENU_482,
 L_MPMENU_483,
 L_MPMENU_484,
 L_MPMENU_485,
 L_MPMENU_486,
 L_MPMENU_487,
 L_MPMENU_488,
 L_MPMENU_489,
 L_MPMENU_490,
 L_MPMENU_491,
 L_MPMENU_492,
 L_MPMENU_493,
 L_MPMENU_494,
 L_MPMENU_495,
 L_MPMENU_END
};
enum l_propobj {
 L_PROPOBJ_000 = 0x5200,
 L_PROPOBJ_001,
 L_PROPOBJ_002,
 L_PROPOBJ_003,
 L_PROPOBJ_004,
 L_PROPOBJ_005,
 L_PROPOBJ_006,
 L_PROPOBJ_007,
 L_PROPOBJ_008,
 L_PROPOBJ_009,
 L_PROPOBJ_010,
 L_PROPOBJ_011,
 L_PROPOBJ_012,
 L_PROPOBJ_013,
 L_PROPOBJ_014,
 L_PROPOBJ_015,
 L_PROPOBJ_016,
 L_PROPOBJ_017,
 L_PROPOBJ_018,
 L_PROPOBJ_019,
 L_PROPOBJ_020,
 L_PROPOBJ_021,
 L_PROPOBJ_022,
 L_PROPOBJ_023,
 L_PROPOBJ_024,
 L_PROPOBJ_025,
 L_PROPOBJ_026,
 L_PROPOBJ_027,
 L_PROPOBJ_028,
 L_PROPOBJ_029,
 L_PROPOBJ_030,
 L_PROPOBJ_031,
 L_PROPOBJ_032,
 L_PROPOBJ_033,
 L_PROPOBJ_034,
 L_PROPOBJ_035,
 L_PROPOBJ_036,
 L_PROPOBJ_037,
 L_PROPOBJ_038,
 L_PROPOBJ_039,
 L_PROPOBJ_040,
 L_PROPOBJ_041,
 L_PROPOBJ_042,
 L_PROPOBJ_043,
 L_PROPOBJ_044,
 L_PROPOBJ_045,
 L_PROPOBJ_046,
 L_PROPOBJ_047,
 L_PROPOBJ_048,
 L_PROPOBJ_049,
 L_PROPOBJ_050,
 L_PROPOBJ_051,
 L_PROPOBJ_END
};
enum l_mpweapons {
 L_MPWEAPONS_000 = 0x5400,
 L_MPWEAPONS_001,
 L_MPWEAPONS_002,
 L_MPWEAPONS_003,
 L_MPWEAPONS_004,
 L_MPWEAPONS_005,
 L_MPWEAPONS_006,
 L_MPWEAPONS_007,
 L_MPWEAPONS_008,
 L_MPWEAPONS_009,
 L_MPWEAPONS_010,
 L_MPWEAPONS_011,
 L_MPWEAPONS_012,
 L_MPWEAPONS_013,
 L_MPWEAPONS_014,
 L_MPWEAPONS_015,
 L_MPWEAPONS_016,
 L_MPWEAPONS_017,
 L_MPWEAPONS_018,
 L_MPWEAPONS_019,
 L_MPWEAPONS_020,
 L_MPWEAPONS_021,
 L_MPWEAPONS_022,
 L_MPWEAPONS_023,
 L_MPWEAPONS_024,
 L_MPWEAPONS_025,
 L_MPWEAPONS_026,
 L_MPWEAPONS_027,
 L_MPWEAPONS_028,
 L_MPWEAPONS_029,
 L_MPWEAPONS_030,
 L_MPWEAPONS_031,
 L_MPWEAPONS_032,
 L_MPWEAPONS_033,
 L_MPWEAPONS_034,
 L_MPWEAPONS_035,
 L_MPWEAPONS_036,
 L_MPWEAPONS_037,
 L_MPWEAPONS_038,
 L_MPWEAPONS_039,
 L_MPWEAPONS_040,
 L_MPWEAPONS_041,
 L_MPWEAPONS_042,
 L_MPWEAPONS_043,
 L_MPWEAPONS_044,
 L_MPWEAPONS_045,
 L_MPWEAPONS_046,
 L_MPWEAPONS_047,
 L_MPWEAPONS_048,
 L_MPWEAPONS_049,
 L_MPWEAPONS_050,
 L_MPWEAPONS_051,
 L_MPWEAPONS_052,
 L_MPWEAPONS_053,
 L_MPWEAPONS_054,
 L_MPWEAPONS_055,
 L_MPWEAPONS_056,
 L_MPWEAPONS_057,
 L_MPWEAPONS_058,
 L_MPWEAPONS_059,
 L_MPWEAPONS_060,
 L_MPWEAPONS_061,
 L_MPWEAPONS_062,
 L_MPWEAPONS_063,
 L_MPWEAPONS_064,
 L_MPWEAPONS_065,
 L_MPWEAPONS_066,
 L_MPWEAPONS_067,
 L_MPWEAPONS_068,
 L_MPWEAPONS_069,
 L_MPWEAPONS_070,
 L_MPWEAPONS_071,
 L_MPWEAPONS_072,
 L_MPWEAPONS_073,
 L_MPWEAPONS_074,
 L_MPWEAPONS_075,
 L_MPWEAPONS_076,
 L_MPWEAPONS_077,
 L_MPWEAPONS_078,
 L_MPWEAPONS_079,
 L_MPWEAPONS_080,
 L_MPWEAPONS_081,
 L_MPWEAPONS_082,
 L_MPWEAPONS_083,
 L_MPWEAPONS_084,
 L_MPWEAPONS_085,
 L_MPWEAPONS_086,
 L_MPWEAPONS_087,
 L_MPWEAPONS_088,
 L_MPWEAPONS_089,
 L_MPWEAPONS_090,
 L_MPWEAPONS_091,
 L_MPWEAPONS_092,
 L_MPWEAPONS_093,
 L_MPWEAPONS_094,
 L_MPWEAPONS_095,
 L_MPWEAPONS_096,
 L_MPWEAPONS_097,
 L_MPWEAPONS_098,
 L_MPWEAPONS_099,
 L_MPWEAPONS_100,
 L_MPWEAPONS_101,
 L_MPWEAPONS_102,
 L_MPWEAPONS_103,
 L_MPWEAPONS_104,
 L_MPWEAPONS_105,
 L_MPWEAPONS_106,
 L_MPWEAPONS_107,
 L_MPWEAPONS_108,
 L_MPWEAPONS_109,
 L_MPWEAPONS_110,
 L_MPWEAPONS_111,
 L_MPWEAPONS_112,
 L_MPWEAPONS_113,
 L_MPWEAPONS_114,
 L_MPWEAPONS_115,
 L_MPWEAPONS_116,
 L_MPWEAPONS_117,
 L_MPWEAPONS_118,
 L_MPWEAPONS_119,
 L_MPWEAPONS_120,
 L_MPWEAPONS_121,
 L_MPWEAPONS_122,
 L_MPWEAPONS_123,
 L_MPWEAPONS_124,
 L_MPWEAPONS_125,
 L_MPWEAPONS_126,
 L_MPWEAPONS_127,
 L_MPWEAPONS_128,
 L_MPWEAPONS_129,
 L_MPWEAPONS_130,
 L_MPWEAPONS_131,
 L_MPWEAPONS_132,
 L_MPWEAPONS_133,
 L_MPWEAPONS_134,
 L_MPWEAPONS_135,
 L_MPWEAPONS_136,
 L_MPWEAPONS_137,
 L_MPWEAPONS_138,
 L_MPWEAPONS_139,
 L_MPWEAPONS_140,
 L_MPWEAPONS_141,
 L_MPWEAPONS_142,
 L_MPWEAPONS_143,
 L_MPWEAPONS_144,
 L_MPWEAPONS_145,
 L_MPWEAPONS_146,
 L_MPWEAPONS_147,
 L_MPWEAPONS_148,
 L_MPWEAPONS_149,
 L_MPWEAPONS_150,
 L_MPWEAPONS_151,
 L_MPWEAPONS_152,
 L_MPWEAPONS_153,
 L_MPWEAPONS_154,
 L_MPWEAPONS_155,
 L_MPWEAPONS_156,
 L_MPWEAPONS_157,
 L_MPWEAPONS_158,
 L_MPWEAPONS_159,
 L_MPWEAPONS_160,
 L_MPWEAPONS_161,
 L_MPWEAPONS_162,
 L_MPWEAPONS_163,
 L_MPWEAPONS_164,
 L_MPWEAPONS_165,
 L_MPWEAPONS_166,
 L_MPWEAPONS_167,
 L_MPWEAPONS_168,
 L_MPWEAPONS_169,
 L_MPWEAPONS_170,
 L_MPWEAPONS_171,
 L_MPWEAPONS_172,
 L_MPWEAPONS_173,
 L_MPWEAPONS_174,
 L_MPWEAPONS_175,
 L_MPWEAPONS_176,
 L_MPWEAPONS_177,
 L_MPWEAPONS_178,
 L_MPWEAPONS_179,
 L_MPWEAPONS_180,
 L_MPWEAPONS_181,
 L_MPWEAPONS_182,
 L_MPWEAPONS_183,
 L_MPWEAPONS_184,
 L_MPWEAPONS_185,
 L_MPWEAPONS_186,
 L_MPWEAPONS_187,
 L_MPWEAPONS_188,
 L_MPWEAPONS_189,
 L_MPWEAPONS_190,
 L_MPWEAPONS_191,
 L_MPWEAPONS_192,
 L_MPWEAPONS_193,
 L_MPWEAPONS_194,
 L_MPWEAPONS_195,
 L_MPWEAPONS_196,
 L_MPWEAPONS_197,
 L_MPWEAPONS_198,
 L_MPWEAPONS_199,
 L_MPWEAPONS_200,
 L_MPWEAPONS_201,
 L_MPWEAPONS_202,
 L_MPWEAPONS_203,
 L_MPWEAPONS_204,
 L_MPWEAPONS_205,
 L_MPWEAPONS_206,
 L_MPWEAPONS_207,
 L_MPWEAPONS_208,
 L_MPWEAPONS_209,
 L_MPWEAPONS_210,
 L_MPWEAPONS_211,
 L_MPWEAPONS_212,
 L_MPWEAPONS_213,
 L_MPWEAPONS_214,
 L_MPWEAPONS_215,
 L_MPWEAPONS_216,
 L_MPWEAPONS_217,
 L_MPWEAPONS_218,
 L_MPWEAPONS_219,
 L_MPWEAPONS_220,
 L_MPWEAPONS_221,
 L_MPWEAPONS_222,
 L_MPWEAPONS_223,
 L_MPWEAPONS_224,
 L_MPWEAPONS_225,
 L_MPWEAPONS_226,
 L_MPWEAPONS_227,
 L_MPWEAPONS_228,
 L_MPWEAPONS_229,
 L_MPWEAPONS_230,
 L_MPWEAPONS_231,
 L_MPWEAPONS_232,
 L_MPWEAPONS_233,
 L_MPWEAPONS_234,
 L_MPWEAPONS_235,
 L_MPWEAPONS_236,
 L_MPWEAPONS_237,
 L_MPWEAPONS_238,
 L_MPWEAPONS_239,
 L_MPWEAPONS_240,
 L_MPWEAPONS_241,
 L_MPWEAPONS_242,
 L_MPWEAPONS_243,
 L_MPWEAPONS_244,
 L_MPWEAPONS_245,
 L_MPWEAPONS_246,
 L_MPWEAPONS_247,
 L_MPWEAPONS_248,
 L_MPWEAPONS_249,
 L_MPWEAPONS_250,
 L_MPWEAPONS_251,
 L_MPWEAPONS_252,
 L_MPWEAPONS_253,
 L_MPWEAPONS_254,
 L_MPWEAPONS_255,
 L_MPWEAPONS_256,
 L_MPWEAPONS_257,
 L_MPWEAPONS_258,
 L_MPWEAPONS_259,
 L_MPWEAPONS_260,
 L_MPWEAPONS_261,
 L_MPWEAPONS_262,
 L_MPWEAPONS_263,
 L_MPWEAPONS_END
};
enum l_options {
 L_OPTIONS_000 = 0x5600,
 L_OPTIONS_001,
 L_OPTIONS_002,
 L_OPTIONS_003,
 L_OPTIONS_004,
 L_OPTIONS_005,
 L_OPTIONS_006,
 L_OPTIONS_007,
 L_OPTIONS_008,
 L_OPTIONS_009,
 L_OPTIONS_010,
 L_OPTIONS_011,
 L_OPTIONS_012,
 L_OPTIONS_013,
 L_OPTIONS_014,
 L_OPTIONS_015,
 L_OPTIONS_016,
 L_OPTIONS_017,
 L_OPTIONS_018,
 L_OPTIONS_019,
 L_OPTIONS_020,
 L_OPTIONS_021,
 L_OPTIONS_022,
 L_OPTIONS_023,
 L_OPTIONS_024,
 L_OPTIONS_025,
 L_OPTIONS_026,
 L_OPTIONS_027,
 L_OPTIONS_028,
 L_OPTIONS_029,
 L_OPTIONS_030,
 L_OPTIONS_031,
 L_OPTIONS_032,
 L_OPTIONS_033,
 L_OPTIONS_034,
 L_OPTIONS_035,
 L_OPTIONS_036,
 L_OPTIONS_037,
 L_OPTIONS_038,
 L_OPTIONS_039,
 L_OPTIONS_040,
 L_OPTIONS_041,
 L_OPTIONS_042,
 L_OPTIONS_043,
 L_OPTIONS_044,
 L_OPTIONS_045,
 L_OPTIONS_046,
 L_OPTIONS_047,
 L_OPTIONS_048,
 L_OPTIONS_049,
 L_OPTIONS_050,
 L_OPTIONS_051,
 L_OPTIONS_052,
 L_OPTIONS_053,
 L_OPTIONS_054,
 L_OPTIONS_055,
 L_OPTIONS_056,
 L_OPTIONS_057,
 L_OPTIONS_058,
 L_OPTIONS_059,
 L_OPTIONS_060,
 L_OPTIONS_061,
 L_OPTIONS_062,
 L_OPTIONS_063,
 L_OPTIONS_064,
 L_OPTIONS_065,
 L_OPTIONS_066,
 L_OPTIONS_067,
 L_OPTIONS_068,
 L_OPTIONS_069,
 L_OPTIONS_070,
 L_OPTIONS_071,
 L_OPTIONS_072,
 L_OPTIONS_073,
 L_OPTIONS_074,
 L_OPTIONS_075,
 L_OPTIONS_076,
 L_OPTIONS_077,
 L_OPTIONS_078,
 L_OPTIONS_079,
 L_OPTIONS_080,
 L_OPTIONS_081,
 L_OPTIONS_082,
 L_OPTIONS_083,
 L_OPTIONS_084,
 L_OPTIONS_085,
 L_OPTIONS_086,
 L_OPTIONS_087,
 L_OPTIONS_088,
 L_OPTIONS_089,
 L_OPTIONS_090,
 L_OPTIONS_091,
 L_OPTIONS_092,
 L_OPTIONS_093,
 L_OPTIONS_094,
 L_OPTIONS_095,
 L_OPTIONS_096,
 L_OPTIONS_097,
 L_OPTIONS_098,
 L_OPTIONS_099,
 L_OPTIONS_100,
 L_OPTIONS_101,
 L_OPTIONS_102,
 L_OPTIONS_103,
 L_OPTIONS_104,
 L_OPTIONS_105,
 L_OPTIONS_106,
 L_OPTIONS_107,
 L_OPTIONS_108,
 L_OPTIONS_109,
 L_OPTIONS_110,
 L_OPTIONS_111,
 L_OPTIONS_112,
 L_OPTIONS_113,
 L_OPTIONS_114,
 L_OPTIONS_115,
 L_OPTIONS_116,
 L_OPTIONS_117,
 L_OPTIONS_118,
 L_OPTIONS_119,
 L_OPTIONS_120,
 L_OPTIONS_121,
 L_OPTIONS_122,
 L_OPTIONS_123,
 L_OPTIONS_124,
 L_OPTIONS_125,
 L_OPTIONS_126,
 L_OPTIONS_127,
 L_OPTIONS_128,
 L_OPTIONS_129,
 L_OPTIONS_130,
 L_OPTIONS_131,
 L_OPTIONS_132,
 L_OPTIONS_133,
 L_OPTIONS_134,
 L_OPTIONS_135,
 L_OPTIONS_136,
 L_OPTIONS_137,
 L_OPTIONS_138,
 L_OPTIONS_139,
 L_OPTIONS_140,
 L_OPTIONS_141,
 L_OPTIONS_142,
 L_OPTIONS_143,
 L_OPTIONS_144,
 L_OPTIONS_145,
 L_OPTIONS_146,
 L_OPTIONS_147,
 L_OPTIONS_148,
 L_OPTIONS_149,
 L_OPTIONS_150,
 L_OPTIONS_151,
 L_OPTIONS_152,
 L_OPTIONS_153,
 L_OPTIONS_154,
 L_OPTIONS_155,
 L_OPTIONS_156,
 L_OPTIONS_157,
 L_OPTIONS_158,
 L_OPTIONS_159,
 L_OPTIONS_160,
 L_OPTIONS_161,
 L_OPTIONS_162,
 L_OPTIONS_163,
 L_OPTIONS_164,
 L_OPTIONS_165,
 L_OPTIONS_166,
 L_OPTIONS_167,
 L_OPTIONS_168,
 L_OPTIONS_169,
 L_OPTIONS_170,
 L_OPTIONS_171,
 L_OPTIONS_172,
 L_OPTIONS_173,
 L_OPTIONS_174,
 L_OPTIONS_175,
 L_OPTIONS_176,
 L_OPTIONS_177,
 L_OPTIONS_178,
 L_OPTIONS_179,
 L_OPTIONS_180,
 L_OPTIONS_181,
 L_OPTIONS_182,
 L_OPTIONS_183,
 L_OPTIONS_184,
 L_OPTIONS_185,
 L_OPTIONS_186,
 L_OPTIONS_187,
 L_OPTIONS_188,
 L_OPTIONS_189,
 L_OPTIONS_190,
 L_OPTIONS_191,
 L_OPTIONS_192,
 L_OPTIONS_193,
 L_OPTIONS_194,
 L_OPTIONS_195,
 L_OPTIONS_196,
 L_OPTIONS_197,
 L_OPTIONS_198,
 L_OPTIONS_199,
 L_OPTIONS_200,
 L_OPTIONS_201,
 L_OPTIONS_202,
 L_OPTIONS_203,
 L_OPTIONS_204,
 L_OPTIONS_205,
 L_OPTIONS_206,
 L_OPTIONS_207,
 L_OPTIONS_208,
 L_OPTIONS_209,
 L_OPTIONS_210,
 L_OPTIONS_211,
 L_OPTIONS_212,
 L_OPTIONS_213,
 L_OPTIONS_214,
 L_OPTIONS_215,
 L_OPTIONS_216,
 L_OPTIONS_217,
 L_OPTIONS_218,
 L_OPTIONS_219,
 L_OPTIONS_220,
 L_OPTIONS_221,
 L_OPTIONS_222,
 L_OPTIONS_223,
 L_OPTIONS_224,
 L_OPTIONS_225,
 L_OPTIONS_226,
 L_OPTIONS_227,
 L_OPTIONS_228,
 L_OPTIONS_229,
 L_OPTIONS_230,
 L_OPTIONS_231,
 L_OPTIONS_232,
 L_OPTIONS_233,
 L_OPTIONS_234,
 L_OPTIONS_235,
 L_OPTIONS_236,
 L_OPTIONS_237,
 L_OPTIONS_238,
 L_OPTIONS_239,
 L_OPTIONS_240,
 L_OPTIONS_241,
 L_OPTIONS_242,
 L_OPTIONS_243,
 L_OPTIONS_244,
 L_OPTIONS_245,
 L_OPTIONS_246,
 L_OPTIONS_247,
 L_OPTIONS_248,
 L_OPTIONS_249,
 L_OPTIONS_250,
 L_OPTIONS_251,
 L_OPTIONS_252,
 L_OPTIONS_253,
 L_OPTIONS_254,
 L_OPTIONS_255,
 L_OPTIONS_256,
 L_OPTIONS_257,
 L_OPTIONS_258,
 L_OPTIONS_259,
 L_OPTIONS_260,
 L_OPTIONS_261,
 L_OPTIONS_262,
 L_OPTIONS_263,
 L_OPTIONS_264,
 L_OPTIONS_265,
 L_OPTIONS_266,
 L_OPTIONS_267,
 L_OPTIONS_268,
 L_OPTIONS_269,
 L_OPTIONS_270,
 L_OPTIONS_271,
 L_OPTIONS_272,
 L_OPTIONS_273,
 L_OPTIONS_274,
 L_OPTIONS_275,
 L_OPTIONS_276,
 L_OPTIONS_277,
 L_OPTIONS_278,
 L_OPTIONS_279,
 L_OPTIONS_280,
 L_OPTIONS_281,
 L_OPTIONS_282,
 L_OPTIONS_283,
 L_OPTIONS_284,
 L_OPTIONS_285,
 L_OPTIONS_286,
 L_OPTIONS_287,
 L_OPTIONS_288,
 L_OPTIONS_289,
 L_OPTIONS_290,
 L_OPTIONS_291,
 L_OPTIONS_292,
 L_OPTIONS_293,
 L_OPTIONS_294,
 L_OPTIONS_295,
 L_OPTIONS_296,
 L_OPTIONS_297,
 L_OPTIONS_298,
 L_OPTIONS_299,
 L_OPTIONS_300,
 L_OPTIONS_301,
 L_OPTIONS_302,
 L_OPTIONS_303,
 L_OPTIONS_304,
 L_OPTIONS_305,
 L_OPTIONS_306,
 L_OPTIONS_307,
 L_OPTIONS_308,
 L_OPTIONS_309,
 L_OPTIONS_310,
 L_OPTIONS_311,
 L_OPTIONS_312,
 L_OPTIONS_313,
 L_OPTIONS_314,
 L_OPTIONS_315,
 L_OPTIONS_316,
 L_OPTIONS_317,
 L_OPTIONS_318,
 L_OPTIONS_319,
 L_OPTIONS_320,
 L_OPTIONS_321,
 L_OPTIONS_322,
 L_OPTIONS_323,
 L_OPTIONS_324,
 L_OPTIONS_325,
 L_OPTIONS_326,
 L_OPTIONS_327,
 L_OPTIONS_328,
 L_OPTIONS_329,
 L_OPTIONS_330,
 L_OPTIONS_331,
 L_OPTIONS_332,
 L_OPTIONS_333,
 L_OPTIONS_334,
 L_OPTIONS_335,
 L_OPTIONS_336,
 L_OPTIONS_337,
 L_OPTIONS_338,
 L_OPTIONS_339,
 L_OPTIONS_340,
 L_OPTIONS_341,
 L_OPTIONS_342,
 L_OPTIONS_343,
 L_OPTIONS_344,
 L_OPTIONS_345,
 L_OPTIONS_346,
 L_OPTIONS_347,
 L_OPTIONS_348,
 L_OPTIONS_349,
 L_OPTIONS_350,
 L_OPTIONS_351,
 L_OPTIONS_352,
 L_OPTIONS_353,
 L_OPTIONS_354,
 L_OPTIONS_355,
 L_OPTIONS_356,
 L_OPTIONS_357,
 L_OPTIONS_358,
 L_OPTIONS_359,
 L_OPTIONS_360,
 L_OPTIONS_361,
 L_OPTIONS_362,
 L_OPTIONS_363,
 L_OPTIONS_364,
 L_OPTIONS_365,
 L_OPTIONS_366,
 L_OPTIONS_367,
 L_OPTIONS_368,
 L_OPTIONS_369,
 L_OPTIONS_370,
 L_OPTIONS_371,
 L_OPTIONS_372,
 L_OPTIONS_373,
 L_OPTIONS_374,
 L_OPTIONS_375,
 L_OPTIONS_376,
 L_OPTIONS_377,
 L_OPTIONS_378,
 L_OPTIONS_379,
 L_OPTIONS_380,
 L_OPTIONS_381,
 L_OPTIONS_382,
 L_OPTIONS_383,
 L_OPTIONS_384,
 L_OPTIONS_385,
 L_OPTIONS_386,
 L_OPTIONS_387,
 L_OPTIONS_388,
 L_OPTIONS_389,
 L_OPTIONS_390,
 L_OPTIONS_391,
 L_OPTIONS_392,
 L_OPTIONS_393,
 L_OPTIONS_394,
 L_OPTIONS_395,
 L_OPTIONS_396,
 L_OPTIONS_397,
 L_OPTIONS_398,
 L_OPTIONS_399,
 L_OPTIONS_400,
 L_OPTIONS_401,
 L_OPTIONS_402,
 L_OPTIONS_403,
 L_OPTIONS_404,
 L_OPTIONS_405,
 L_OPTIONS_406,
 L_OPTIONS_407,
 L_OPTIONS_408,
 L_OPTIONS_409,
 L_OPTIONS_410,
 L_OPTIONS_411,
 L_OPTIONS_412,
 L_OPTIONS_413,
 L_OPTIONS_414,
 L_OPTIONS_415,
 L_OPTIONS_416,
 L_OPTIONS_417,
 L_OPTIONS_418,
 L_OPTIONS_419,
 L_OPTIONS_420,
 L_OPTIONS_421,
 L_OPTIONS_422,
 L_OPTIONS_423,
 L_OPTIONS_424,
 L_OPTIONS_425,
 L_OPTIONS_426,
 L_OPTIONS_427,
 L_OPTIONS_428,
 L_OPTIONS_429,
 L_OPTIONS_430,
 L_OPTIONS_431,
 L_OPTIONS_432,
 L_OPTIONS_433,
 L_OPTIONS_434,
 L_OPTIONS_435,
 L_OPTIONS_436,
 L_OPTIONS_437,
 L_OPTIONS_438,
 L_OPTIONS_439,
 L_OPTIONS_440,
 L_OPTIONS_441,
 L_OPTIONS_442,
 L_OPTIONS_443,
 L_OPTIONS_444,
 L_OPTIONS_445,
 L_OPTIONS_446,
 L_OPTIONS_447,
 L_OPTIONS_448,
 L_OPTIONS_449,
 L_OPTIONS_450,
 L_OPTIONS_451,
 L_OPTIONS_452,
 L_OPTIONS_453,
 L_OPTIONS_454,
 L_OPTIONS_455,
 L_OPTIONS_456,
 L_OPTIONS_457,
 L_OPTIONS_458,
 L_OPTIONS_459,
 L_OPTIONS_460,
 L_OPTIONS_461,
 L_OPTIONS_462,
 L_OPTIONS_463,
 L_OPTIONS_464,
 L_OPTIONS_465,
 L_OPTIONS_466,
 L_OPTIONS_467,
 L_OPTIONS_468,
 L_OPTIONS_469,
 L_OPTIONS_470,
 L_OPTIONS_471,
 L_OPTIONS_472,
 L_OPTIONS_473,
 L_OPTIONS_474,
 L_OPTIONS_475,
 L_OPTIONS_476,
 L_OPTIONS_477,
 L_OPTIONS_478,
 L_OPTIONS_479,
 L_OPTIONS_480,
 L_OPTIONS_481,
 L_OPTIONS_482,
 L_OPTIONS_483,
 L_OPTIONS_484,
 L_OPTIONS_485,
 L_OPTIONS_486,
 L_OPTIONS_487,
 L_OPTIONS_488,
 L_OPTIONS_489,
 L_OPTIONS_490,
 L_OPTIONS_491,
 L_OPTIONS_492,
 L_OPTIONS_493,
 L_OPTIONS_494,
 L_OPTIONS_495,
 L_OPTIONS_END
};
enum l_misc {
 L_MISC_000 = 0x5800,
 L_MISC_001,
 L_MISC_002,
 L_MISC_003,
 L_MISC_004,
 L_MISC_005,
 L_MISC_006,
 L_MISC_007,
 L_MISC_008,
 L_MISC_009,
 L_MISC_010,
 L_MISC_011,
 L_MISC_012,
 L_MISC_013,
 L_MISC_014,
 L_MISC_015,
 L_MISC_016,
 L_MISC_017,
 L_MISC_018,
 L_MISC_019,
 L_MISC_020,
 L_MISC_021,
 L_MISC_022,
 L_MISC_023,
 L_MISC_024,
 L_MISC_025,
 L_MISC_026,
 L_MISC_027,
 L_MISC_028,
 L_MISC_029,
 L_MISC_030,
 L_MISC_031,
 L_MISC_032,
 L_MISC_033,
 L_MISC_034,
 L_MISC_035,
 L_MISC_036,
 L_MISC_037,
 L_MISC_038,
 L_MISC_039,
 L_MISC_040,
 L_MISC_041,
 L_MISC_042,
 L_MISC_043,
 L_MISC_044,
 L_MISC_045,
 L_MISC_046,
 L_MISC_047,
 L_MISC_048,
 L_MISC_049,
 L_MISC_050,
 L_MISC_051,
 L_MISC_052,
 L_MISC_053,
 L_MISC_054,
 L_MISC_055,
 L_MISC_056,
 L_MISC_057,
 L_MISC_058,
 L_MISC_059,
 L_MISC_060,
 L_MISC_061,
 L_MISC_062,
 L_MISC_063,
 L_MISC_064,
 L_MISC_065,
 L_MISC_066,
 L_MISC_067,
 L_MISC_068,
 L_MISC_069,
 L_MISC_070,
 L_MISC_071,
 L_MISC_072,
 L_MISC_073,
 L_MISC_074,
 L_MISC_075,
 L_MISC_076,
 L_MISC_077,
 L_MISC_078,
 L_MISC_079,
 L_MISC_080,
 L_MISC_081,
 L_MISC_082,
 L_MISC_083,
 L_MISC_084,
 L_MISC_085,
 L_MISC_086,
 L_MISC_087,
 L_MISC_088,
 L_MISC_089,
 L_MISC_090,
 L_MISC_091,
 L_MISC_092,
 L_MISC_093,
 L_MISC_094,
 L_MISC_095,
 L_MISC_096,
 L_MISC_097,
 L_MISC_098,
 L_MISC_099,
 L_MISC_100,
 L_MISC_101,
 L_MISC_102,
 L_MISC_103,
 L_MISC_104,
 L_MISC_105,
 L_MISC_106,
 L_MISC_107,
 L_MISC_108,
 L_MISC_109,
 L_MISC_110,
 L_MISC_111,
 L_MISC_112,
 L_MISC_113,
 L_MISC_114,
 L_MISC_115,
 L_MISC_116,
 L_MISC_117,
 L_MISC_118,
 L_MISC_119,
 L_MISC_120,
 L_MISC_121,
 L_MISC_122,
 L_MISC_123,
 L_MISC_124,
 L_MISC_125,
 L_MISC_126,
 L_MISC_127,
 L_MISC_128,
 L_MISC_129,
 L_MISC_130,
 L_MISC_131,
 L_MISC_132,
 L_MISC_133,
 L_MISC_134,
 L_MISC_135,
 L_MISC_136,
 L_MISC_137,
 L_MISC_138,
 L_MISC_139,
 L_MISC_140,
 L_MISC_141,
 L_MISC_142,
 L_MISC_143,
 L_MISC_144,
 L_MISC_145,
 L_MISC_146,
 L_MISC_147,
 L_MISC_148,
 L_MISC_149,
 L_MISC_150,
 L_MISC_151,
 L_MISC_152,
 L_MISC_153,
 L_MISC_154,
 L_MISC_155,
 L_MISC_156,
 L_MISC_157,
 L_MISC_158,
 L_MISC_159,
 L_MISC_160,
 L_MISC_161,
 L_MISC_162,
 L_MISC_163,
 L_MISC_164,
 L_MISC_165,
 L_MISC_166,
 L_MISC_167,
 L_MISC_168,
 L_MISC_169,
 L_MISC_170,
 L_MISC_171,
 L_MISC_172,
 L_MISC_173,
 L_MISC_174,
 L_MISC_175,
 L_MISC_176,
 L_MISC_177,
 L_MISC_178,
 L_MISC_179,
 L_MISC_180,
 L_MISC_181,
 L_MISC_182,
 L_MISC_183,
 L_MISC_184,
 L_MISC_185,
 L_MISC_186,
 L_MISC_187,
 L_MISC_188,
 L_MISC_189,
 L_MISC_190,
 L_MISC_191,
 L_MISC_192,
 L_MISC_193,
 L_MISC_194,
 L_MISC_195,
 L_MISC_196,
 L_MISC_197,
 L_MISC_198,
 L_MISC_199,
 L_MISC_200,
 L_MISC_201,
 L_MISC_202,
 L_MISC_203,
 L_MISC_204,
 L_MISC_205,
 L_MISC_206,
 L_MISC_207,
 L_MISC_208,
 L_MISC_209,
 L_MISC_210,
 L_MISC_211,
 L_MISC_212,
 L_MISC_213,
 L_MISC_214,
 L_MISC_215,
 L_MISC_216,
 L_MISC_217,
 L_MISC_218,
 L_MISC_219,
 L_MISC_220,
 L_MISC_221,
 L_MISC_222,
 L_MISC_223,
 L_MISC_224,
 L_MISC_225,
 L_MISC_226,
 L_MISC_227,
 L_MISC_228,
 L_MISC_229,
 L_MISC_230,
 L_MISC_231,
 L_MISC_232,
 L_MISC_233,
 L_MISC_234,
 L_MISC_235,
 L_MISC_236,
 L_MISC_237,
 L_MISC_238,
 L_MISC_239,
 L_MISC_240,
 L_MISC_241,
 L_MISC_242,
 L_MISC_243,
 L_MISC_244,
 L_MISC_245,
 L_MISC_246,
 L_MISC_247,
 L_MISC_248,
 L_MISC_249,
 L_MISC_250,
 L_MISC_251,
 L_MISC_252,
 L_MISC_253,
 L_MISC_254,
 L_MISC_255,
 L_MISC_256,
 L_MISC_257,
 L_MISC_258,
 L_MISC_259,
 L_MISC_260,
 L_MISC_261,
 L_MISC_262,
 L_MISC_263,
 L_MISC_264,
 L_MISC_265,
 L_MISC_266,
 L_MISC_267,
 L_MISC_268,
 L_MISC_269,
 L_MISC_270,
 L_MISC_271,
 L_MISC_272,
 L_MISC_273,
 L_MISC_274,
 L_MISC_275,
 L_MISC_276,
 L_MISC_277,
 L_MISC_278,
 L_MISC_279,
 L_MISC_280,
 L_MISC_281,
 L_MISC_282,
 L_MISC_283,
 L_MISC_284,
 L_MISC_285,
 L_MISC_286,
 L_MISC_287,
 L_MISC_288,
 L_MISC_289,
 L_MISC_290,
 L_MISC_291,
 L_MISC_292,
 L_MISC_293,
 L_MISC_294,
 L_MISC_295,
 L_MISC_296,
 L_MISC_297,
 L_MISC_298,
 L_MISC_299,
 L_MISC_300,
 L_MISC_301,
 L_MISC_302,
 L_MISC_303,
 L_MISC_304,
 L_MISC_305,
 L_MISC_306,
 L_MISC_307,
 L_MISC_308,
 L_MISC_309,
 L_MISC_310,
 L_MISC_311,
 L_MISC_312,
 L_MISC_313,
 L_MISC_314,
 L_MISC_315,
 L_MISC_316,
 L_MISC_317,
 L_MISC_318,
 L_MISC_319,
 L_MISC_320,
 L_MISC_321,
 L_MISC_322,
 L_MISC_323,
 L_MISC_324,
 L_MISC_325,
 L_MISC_326,
 L_MISC_327,
 L_MISC_328,
 L_MISC_329,
 L_MISC_330,
 L_MISC_331,
 L_MISC_332,
 L_MISC_333,
 L_MISC_334,
 L_MISC_335,
 L_MISC_336,
 L_MISC_337,
 L_MISC_338,
 L_MISC_339,
 L_MISC_340,
 L_MISC_341,
 L_MISC_342,
 L_MISC_343,
 L_MISC_344,
 L_MISC_345,
 L_MISC_346,
 L_MISC_347,
 L_MISC_348,
 L_MISC_349,
 L_MISC_350,
 L_MISC_351,
 L_MISC_352,
 L_MISC_353,
 L_MISC_354,
 L_MISC_355,
 L_MISC_356,
 L_MISC_357,
 L_MISC_358,
 L_MISC_359,
 L_MISC_360,
 L_MISC_361,
 L_MISC_362,
 L_MISC_363,
 L_MISC_364,
 L_MISC_365,
 L_MISC_366,
 L_MISC_367,
 L_MISC_368,
 L_MISC_369,
 L_MISC_370,
 L_MISC_371,
 L_MISC_372,
 L_MISC_373,
 L_MISC_374,
 L_MISC_375,
 L_MISC_376,
 L_MISC_377,
 L_MISC_378,
 L_MISC_379,
 L_MISC_380,
 L_MISC_381,
 L_MISC_382,
 L_MISC_383,
 L_MISC_384,
 L_MISC_385,
 L_MISC_386,
 L_MISC_387,
 L_MISC_388,
 L_MISC_389,
 L_MISC_390,
 L_MISC_391,
 L_MISC_392,
 L_MISC_393,
 L_MISC_394,
 L_MISC_395,
 L_MISC_396,
 L_MISC_397,
 L_MISC_398,
 L_MISC_399,
 L_MISC_400,
 L_MISC_401,
 L_MISC_402,
 L_MISC_403,
 L_MISC_404,
 L_MISC_405,
 L_MISC_406,
 L_MISC_407,
 L_MISC_408,
 L_MISC_409,
 L_MISC_410,
 L_MISC_411,
 L_MISC_412,
 L_MISC_413,
 L_MISC_414,
 L_MISC_415,
 L_MISC_416,
 L_MISC_417,
 L_MISC_418,
 L_MISC_419,
 L_MISC_420,
 L_MISC_421,
 L_MISC_422,
 L_MISC_423,
 L_MISC_424,
 L_MISC_425,
 L_MISC_426,
 L_MISC_427,
 L_MISC_428,
 L_MISC_429,
 L_MISC_430,
 L_MISC_431,
 L_MISC_432,
 L_MISC_433,
 L_MISC_434,
 L_MISC_435,
 L_MISC_436,
 L_MISC_437,
 L_MISC_438,
 L_MISC_439,
 L_MISC_440,
 L_MISC_441,
 L_MISC_442,
 L_MISC_443,
 L_MISC_444,
 L_MISC_445,
 L_MISC_446,
 L_MISC_447,
 L_MISC_448,
 L_MISC_449,
 L_MISC_450,
 L_MISC_451,
 L_MISC_452,
 L_MISC_453,
 L_MISC_454,
 L_MISC_455,
 L_MISC_456,
 L_MISC_457,
 L_MISC_458,
 L_MISC_459,
 L_MISC_460,
 L_MISC_461,
 L_MISC_462,
 L_MISC_463,
 L_MISC_464,
 L_MISC_465,
 L_MISC_466,
 L_MISC_467,
 L_MISC_468,
 L_MISC_469,
 L_MISC_470,
 L_MISC_471,
 L_MISC_472,
 L_MISC_473,
 L_MISC_474,
 L_MISC_475,
 L_MISC_END
};
enum l_uff {
 L_UFF_END
};
enum l_old {
 L_OLD_END
};
enum l_ate {
 L_ATE_000 = 0x5e00,
 L_ATE_001,
 L_ATE_002,
 L_ATE_003,
 L_ATE_004,
 L_ATE_005,
 L_ATE_006,
 L_ATE_007,
 L_ATE_008,
 L_ATE_009,
 L_ATE_010,
 L_ATE_011,
 L_ATE_012,
 L_ATE_013,
 L_ATE_014,
 L_ATE_015,
 L_ATE_END
};
enum l_lam {
 L_LAM_END
};
enum l_mp1 {
 L_MP1_END
};
enum l_mp2 {
 L_MP2_END
};
enum l_mp3 {
 L_MP3_END
};
enum l_mp4 {
 L_MP4_END
};
enum l_mp5 {
 L_MP5_END
};
enum l_mp6 {
 L_MP6_END
};
enum l_mp7 {
 L_MP7_END
};
enum l_mp8 {
 L_MP8_END
};
enum l_mp9 {
 L_MP9_END
};
enum l_mp10 {
 L_MP10_END
};
enum l_mp11 {
 L_MP11_END
};
enum l_mp12 {
 L_MP12_END
};
enum l_mp13 {
 L_MP13_END
};
enum l_mp14 {
 L_MP14_END
};
enum l_mp15 {
 L_MP15_END
};
enum l_mp16 {
 L_MP16_END
};
enum l_mp17 {
 L_MP17_END
};
enum l_mp18 {
 L_MP18_END
};
enum l_mp19 {
 L_MP19_END
};
enum l_mp20 {
 L_MP20_END
};
enum pad_ame {
 PAD_AME_0000,
 PAD_AME_0001,
 PAD_AME_0002,
 PAD_AME_0003,
 PAD_AME_0004,
 PAD_AME_0005,
 PAD_AME_0006,
 PAD_AME_0007,
 PAD_AME_0008,
 PAD_AME_0009,
 PAD_AME_000A,
 PAD_AME_000B,
 PAD_AME_000C,
 PAD_AME_000D,
 PAD_AME_000E,
 PAD_AME_000F,
 PAD_AME_0010,
 PAD_AME_0011,
 PAD_AME_0012,
 PAD_AME_0013,
 PAD_AME_0014,
 PAD_AME_0015,
 PAD_AME_0016,
 PAD_AME_0017,
 PAD_AME_0018,
 PAD_AME_0019,
 PAD_AME_001A,
 PAD_AME_001B,
 PAD_AME_001C,
 PAD_AME_001D,
 PAD_AME_001E,
 PAD_AME_001F,
 PAD_AME_0020,
 PAD_AME_0021,
 PAD_AME_0022,
 PAD_AME_0023,
 PAD_AME_0024,
 PAD_AME_0025,
 PAD_AME_0026,
 PAD_AME_0027,
 PAD_AME_0028,
 PAD_AME_0029,
 PAD_AME_002A,
 PAD_AME_002B,
 PAD_AME_002C,
 PAD_AME_002D,
 PAD_AME_002E,
 PAD_AME_002F,
 PAD_AME_0030,
 PAD_AME_0031,
 PAD_AME_0032,
 PAD_AME_0033,
 PAD_AME_0034,
 PAD_AME_0035,
 PAD_AME_0036,
 PAD_AME_0037,
 PAD_AME_0038,
 PAD_AME_0039,
 PAD_AME_003A,
 PAD_AME_003B,
 PAD_AME_003C,
 PAD_AME_003D,
 PAD_AME_003E,
 PAD_AME_003F,
 PAD_AME_0040,
 PAD_AME_0041,
 PAD_AME_0042,
 PAD_AME_0043,
 PAD_AME_0044,
 PAD_AME_0045,
 PAD_AME_0046,
 PAD_AME_0047,
 PAD_AME_0048,
 PAD_AME_0049,
 PAD_AME_004A,
 PAD_AME_004B,
 PAD_AME_004C,
 PAD_AME_004D,
 PAD_AME_004E,
 PAD_AME_004F,
 PAD_AME_0050,
 PAD_AME_0051,
 PAD_AME_0052,
 PAD_AME_0053,
 PAD_AME_0054,
 PAD_AME_0055,
 PAD_AME_0056,
 PAD_AME_0057,
 PAD_AME_0058,
 PAD_AME_0059,
 PAD_AME_005A,
 PAD_AME_005B,
 PAD_AME_005C,
 PAD_AME_005D,
 PAD_AME_005E,
 PAD_AME_005F,
 PAD_AME_0060,
 PAD_AME_0061,
 PAD_AME_0062,
 PAD_AME_0063,
 PAD_AME_0064,
 PAD_AME_0065,
 PAD_AME_0066,
 PAD_AME_0067,
 PAD_AME_0068,
 PAD_AME_0069,
 PAD_AME_006A,
 PAD_AME_006B,
 PAD_AME_006C,
 PAD_AME_006D,
 PAD_AME_006E,
 PAD_AME_006F,
 PAD_AME_0070,
 PAD_AME_0071,
 PAD_AME_0072,
 PAD_AME_0073,
 PAD_AME_0074,
 PAD_AME_0075,
 PAD_AME_0076,
 PAD_AME_0077,
 PAD_AME_0078,
 PAD_AME_0079,
 PAD_AME_007A,
 PAD_AME_007B,
 PAD_AME_007C,
 PAD_AME_007D,
 PAD_AME_007E,
 PAD_AME_007F,
 PAD_AME_0080,
 PAD_AME_0081,
 PAD_AME_0082,
 PAD_AME_0083,
 PAD_AME_0084,
 PAD_AME_0085,
 PAD_AME_0086,
 PAD_AME_0087,
 PAD_AME_0088,
 PAD_AME_0089,
 PAD_AME_008A,
 PAD_AME_008B,
 PAD_AME_008C,
 PAD_AME_008D,
 PAD_AME_008E,
 PAD_AME_008F,
 PAD_AME_0090,
 PAD_AME_0091,
 PAD_AME_0092,
 PAD_AME_0093,
 PAD_AME_0094,
 PAD_AME_0095,
 PAD_AME_0096,
 PAD_AME_0097,
 PAD_AME_0098,
 PAD_AME_0099,
 PAD_AME_009A,
 PAD_AME_009B,
 PAD_AME_009C,
 PAD_AME_009D,
 PAD_AME_009E,
 PAD_AME_009F,
 PAD_AME_00A0,
 PAD_AME_00A1,
 PAD_AME_00A2,
 PAD_AME_00A3,
 PAD_AME_00A4,
 PAD_AME_00A5,
 PAD_AME_00A6,
 PAD_AME_00A7,
 PAD_AME_00A8,
 PAD_AME_00A9,
 PAD_AME_00AA,
 PAD_AME_00AB,
 PAD_AME_00AC,
 PAD_AME_00AD,
 PAD_AME_00AE,
 PAD_AME_00AF,
 PAD_AME_00B0,
 PAD_AME_00B1,
 PAD_AME_00B2,
 PAD_AME_00B3,
 PAD_AME_00B4,
 PAD_AME_00B5,
 PAD_AME_00B6,
 PAD_AME_00B7,
 PAD_AME_00B8,
 PAD_AME_00B9,
 PAD_AME_00BA,
 PAD_AME_00BB,
 PAD_AME_00BC,
 PAD_AME_00BD,
 PAD_AME_00BE,
 PAD_AME_00BF,
 PAD_AME_00C0,
 PAD_AME_00C1,
 PAD_AME_00C2,
 PAD_AME_00C3,
 PAD_AME_00C4,
 PAD_AME_00C5,
 PAD_AME_00C6,
 PAD_AME_00C7,
 PAD_AME_00C8,
 PAD_AME_00C9,
 PAD_AME_00CA,
 PAD_AME_00CB,
 PAD_AME_00CC,
 PAD_AME_00CD,
 PAD_AME_00CE,
 PAD_AME_00CF,
 PAD_AME_00D0,
 PAD_AME_00D1,
 PAD_AME_00D2,
 PAD_AME_00D3,
 PAD_AME_00D4,
 PAD_AME_00D5,
 PAD_AME_00D6,
 PAD_AME_00D7,
 PAD_AME_00D8,
 PAD_AME_00D9,
 PAD_AME_00DA,
 PAD_AME_00DB,
 PAD_AME_00DC,
 PAD_AME_00DD,
 PAD_AME_00DE,
 PAD_AME_00DF,
 PAD_AME_00E0,
 PAD_AME_00E1,
 PAD_AME_00E2,
 PAD_AME_00E3,
 PAD_AME_00E4,
 PAD_AME_00E5,
 PAD_AME_00E6,
 PAD_AME_00E7,
 PAD_AME_00E8,
 PAD_AME_00E9,
 PAD_AME_00EA,
 PAD_AME_00EB,
 PAD_AME_00EC,
 PAD_AME_00ED,
 PAD_AME_00EE,
 PAD_AME_00EF,
 PAD_AME_00F0,
 PAD_AME_00F1,
 PAD_AME_00F2,
 PAD_AME_00F3,
 PAD_AME_00F4,
 PAD_AME_00F5,
 PAD_AME_00F6,
 PAD_AME_00F7,
 PAD_AME_00F8,
 PAD_AME_00F9,
 PAD_AME_00FA,
 PAD_AME_00FB,
 PAD_AME_00FC,
 PAD_AME_00FD,
 PAD_AME_00FE,
 PAD_AME_00FF,
 PAD_AME_0100,
 PAD_AME_0101,
 PAD_AME_0102,
 PAD_AME_0103,
 PAD_AME_0104,
 PAD_AME_0105,
 PAD_AME_0106,
 PAD_AME_0107,
 PAD_AME_0108,
 PAD_AME_0109,
 PAD_AME_010A,
 PAD_AME_010B,
 PAD_AME_010C,
 PAD_AME_010D,
 PAD_AME_010E,
 PAD_AME_010F,
 PAD_AME_0110,
 PAD_AME_0111,
 PAD_AME_0112,
 PAD_AME_0113,
 PAD_AME_0114,
 PAD_AME_0115,
 PAD_AME_0116,
 PAD_AME_0117,
 PAD_AME_0118,
 PAD_AME_0119,
 PAD_AME_011A,
 PAD_AME_011B,
 PAD_AME_011C,
 PAD_AME_011D,
 PAD_AME_011E,
 PAD_AME_011F,
 PAD_AME_0120,
 PAD_AME_0121,
 PAD_AME_0122,
 PAD_AME_0123,
 PAD_AME_0124,
 PAD_AME_0125,
 PAD_AME_0126,
 PAD_AME_0127,
 PAD_AME_0128,
 PAD_AME_0129,
 PAD_AME_012A,
 PAD_AME_012B,
 PAD_AME_012C,
 PAD_AME_012D,
 PAD_AME_012E,
 PAD_AME_012F,
 PAD_AME_0130,
 PAD_AME_0131,
 PAD_AME_0132,
 PAD_AME_0133,
 PAD_AME_0134,
 PAD_AME_0135,
 PAD_AME_0136,
 PAD_AME_0137,
 PAD_AME_0138,
 PAD_AME_0139,
 PAD_AME_013A,
 PAD_AME_013B,
 PAD_AME_013C,
 PAD_AME_013D,
 PAD_AME_013E,
 PAD_AME_013F,
 PAD_AME_0140,
 PAD_AME_0141,
 PAD_AME_0142,
 PAD_AME_0143,
 PAD_AME_0144,
 PAD_AME_0145,
 PAD_AME_0146,
 PAD_AME_0147,
 PAD_AME_0148,
 PAD_AME_0149,
 PAD_AME_014A,
 PAD_AME_014B,
 PAD_AME_014C,
 PAD_AME_014D,
 PAD_AME_014E,
 PAD_AME_014F,
 PAD_AME_0150,
 PAD_AME_0151,
 PAD_AME_0152,
 PAD_AME_0153,
 PAD_AME_0154,
 PAD_AME_0155,
 PAD_AME_0156,
 PAD_AME_0157,
 PAD_AME_0158,
 PAD_AME_0159,
 PAD_AME_015A,
 PAD_AME_015B,
 PAD_AME_015C,
 PAD_AME_015D,
 PAD_AME_015E,
 PAD_AME_015F,
 PAD_AME_0160,
 PAD_AME_0161,
 PAD_AME_0162,
 PAD_AME_0163,
 PAD_AME_0164,
 PAD_AME_0165,
 PAD_AME_0166,
 PAD_AME_0167,
 PAD_AME_0168,
 PAD_AME_0169,
 PAD_AME_016A,
 PAD_AME_016B,
 PAD_AME_016C,
 PAD_AME_016D,
 PAD_AME_016E,
 PAD_AME_016F,
 PAD_AME_0170,
 PAD_AME_0171,
 PAD_AME_0172,
 PAD_AME_0173,
 PAD_AME_0174,
 PAD_AME_0175,
 PAD_AME_0176,
 PAD_AME_0177,
 PAD_AME_0178,
 PAD_AME_0179,
 PAD_AME_017A,
 PAD_AME_017B,
 PAD_AME_017C,
 PAD_AME_017D,
 PAD_AME_017E,
 PAD_AME_017F,
 PAD_AME_0180,
 PAD_AME_0181,
 PAD_AME_0182,
 PAD_AME_0183,
 PAD_AME_0184,
 PAD_AME_0185,
 PAD_AME_0186,
 PAD_AME_0187,
 PAD_AME_0188,
 PAD_AME_0189,
 PAD_AME_018A,
 PAD_AME_018B,
 PAD_AME_018C,
 PAD_AME_018D,
 PAD_AME_018E,
 PAD_AME_018F,
 PAD_AME_0190,
 PAD_AME_0191,
 PAD_AME_0192,
 PAD_AME_0193,
 PAD_AME_0194,
 PAD_AME_0195,
 PAD_AME_0196,
 PAD_AME_0197,
 PAD_AME_0198,
 PAD_AME_0199,
 PAD_AME_019A,
 PAD_AME_019B,
 PAD_AME_019C,
 PAD_AME_019D,
 PAD_AME_019E,
 PAD_AME_019F,
 PAD_AME_01A0,
 PAD_AME_01A1,
 PAD_AME_01A2,
 PAD_AME_01A3,
 PAD_AME_01A4,
 PAD_AME_01A5,
 PAD_AME_01A6,
 PAD_AME_01A7,
 PAD_AME_01A8,
 PAD_AME_01A9,
 PAD_AME_01AA,
 PAD_AME_01AB,
 PAD_AME_01AC,
 PAD_AME_01AD,
 PAD_AME_01AE,
 PAD_AME_01AF,
 PAD_AME_01B0,
 PAD_AME_01B1,
 PAD_AME_01B2,
 PAD_AME_01B3,
 PAD_AME_01B4,
 PAD_AME_01B5,
 PAD_AME_01B6,
 PAD_AME_01B7,
 PAD_AME_01B8,
 PAD_AME_01B9,
 PAD_AME_01BA,
 PAD_AME_01BB,
 PAD_AME_01BC,
 PAD_AME_01BD,
 PAD_AME_01BE,
 PAD_AME_01BF,
 PAD_AME_01C0,
 PAD_AME_01C1,
 PAD_AME_01C2,
 PAD_AME_01C3,
 PAD_AME_01C4,
 PAD_AME_01C5,
 PAD_AME_01C6,
 PAD_AME_01C7,
 PAD_AME_01C8,
 PAD_AME_01C9,
 PAD_AME_01CA,
 PAD_AME_01CB,
 PAD_AME_01CC,
 PAD_AME_01CD,
 PAD_AME_01CE,
 PAD_AME_01CF,
 PAD_AME_01D0,
 PAD_AME_01D1,
 PAD_AME_01D2,
 PAD_AME_01D3,
 PAD_AME_01D4,
 PAD_AME_01D5,
 PAD_AME_01D6,
 PAD_AME_01D7,
 PAD_AME_01D8,
 PAD_AME_01D9,
 PAD_AME_01DA,
 PAD_AME_01DB,
 PAD_AME_01DC,
 PAD_AME_01DD,
 PAD_AME_01DE,
 PAD_AME_01DF,
 PAD_AME_01E0,
 PAD_AME_01E1,
 PAD_AME_01E2,
 PAD_AME_01E3,
 PAD_AME_01E4,
 PAD_AME_01E5,
 PAD_AME_01E6,
 PAD_AME_01E7,
 PAD_AME_01E8,
 PAD_AME_01E9,
 PAD_AME_01EA,
 PAD_AME_01EB,
 PAD_AME_01EC,
 PAD_AME_01ED,
 PAD_AME_01EE,
 PAD_AME_01EF,
 PAD_AME_01F0,
 PAD_AME_01F1,
 PAD_AME_01F2,
 PAD_AME_01F3,
 PAD_AME_01F4,
 PAD_AME_01F5,
 PAD_AME_01F6,
 PAD_AME_01F7,
 PAD_AME_01F8,
 PAD_AME_01F9,
 PAD_AME_01FA,
 PAD_AME_01FB,
 PAD_AME_01FC,
 PAD_AME_01FD,
 PAD_AME_01FE,
 PAD_AME_01FF,
 PAD_AME_0200,
 PAD_AME_0201,
 PAD_AME_0202,
 PAD_AME_0203,
 PAD_AME_0204,
 PAD_AME_0205,
 PAD_AME_0206,
 PAD_AME_0207,
 PAD_AME_0208,
 PAD_AME_0209,
 PAD_AME_020A,
 PAD_AME_020B,
 PAD_AME_020C,
 PAD_AME_020D,
 PAD_AME_020E,
 PAD_AME_020F,
 PAD_AME_0210,
 PAD_AME_0211,
 PAD_AME_0212,
 PAD_AME_0213,
 PAD_AME_0214,
 PAD_AME_0215,
 PAD_AME_0216,
 PAD_AME_0217,
 PAD_AME_0218,
 PAD_AME_0219,
 PAD_AME_021A,
 PAD_AME_021B,
 PAD_AME_021C,
 PAD_AME_021D,
 PAD_AME_021E,
 PAD_AME_021F,
 PAD_AME_0220,
 PAD_AME_0221,
 PAD_AME_0222,
 PAD_AME_0223,
 PAD_AME_0224,
 PAD_AME_0225,
 PAD_AME_0226,
 PAD_AME_0227,
 PAD_AME_0228,
 PAD_AME_0229,
 PAD_AME_022A,
 PAD_AME_022B,
 PAD_AME_022C,
 PAD_AME_022D,
 PAD_AME_022E,
 PAD_AME_022F,
 PAD_AME_0230,
 PAD_AME_0231,
 PAD_AME_0232,
 PAD_AME_0233,
 PAD_AME_0234,
 PAD_AME_0235,
 PAD_AME_0236,
 PAD_AME_0237,
 PAD_AME_0238,
 PAD_AME_0239,
 PAD_AME_023A,
 PAD_AME_023B,
 PAD_AME_023C,
 PAD_AME_023D,
 PAD_AME_023E,
 PAD_AME_023F,
 PAD_AME_0240,
 PAD_AME_0241,
 PAD_AME_0242,
 PAD_AME_0243,
 PAD_AME_0244,
 PAD_AME_0245,
 PAD_AME_0246,
 PAD_AME_0247,
 PAD_AME_0248,
 PAD_AME_0249,
 PAD_AME_024A,
 PAD_AME_024B,
 PAD_AME_024C,
 PAD_AME_024D,
 PAD_AME_024E,
 PAD_AME_024F,
 PAD_AME_0250,
 PAD_AME_0251,
 PAD_AME_0252,
 PAD_AME_0253,
 PAD_AME_0254,
 PAD_AME_0255,
 PAD_AME_0256,
 PAD_AME_0257,
 PAD_AME_0258,
 PAD_AME_0259,
 PAD_AME_025A,
 PAD_AME_025B,
 PAD_AME_025C,
 PAD_AME_025D,
 PAD_AME_025E,
 PAD_AME_025F,
 PAD_AME_END
};
enum pad_arch {
 PAD_ARCH_END
};
enum pad_ark {
 PAD_ARK_0000,
 PAD_ARK_0001,
 PAD_ARK_0002,
 PAD_ARK_0003,
 PAD_ARK_0004,
 PAD_ARK_0005,
 PAD_ARK_0006,
 PAD_ARK_0007,
 PAD_ARK_0008,
 PAD_ARK_0009,
 PAD_ARK_000A,
 PAD_ARK_000B,
 PAD_ARK_000C,
 PAD_ARK_000D,
 PAD_ARK_000E,
 PAD_ARK_000F,
 PAD_ARK_0010,
 PAD_ARK_0011,
 PAD_ARK_0012,
 PAD_ARK_0013,
 PAD_ARK_0014,
 PAD_ARK_0015,
 PAD_ARK_0016,
 PAD_ARK_0017,
 PAD_ARK_0018,
 PAD_ARK_0019,
 PAD_ARK_001A,
 PAD_ARK_001B,
 PAD_ARK_001C,
 PAD_ARK_001D,
 PAD_ARK_001E,
 PAD_ARK_001F,
 PAD_ARK_0020,
 PAD_ARK_0021,
 PAD_ARK_0022,
 PAD_ARK_0023,
 PAD_ARK_0024,
 PAD_ARK_0025,
 PAD_ARK_0026,
 PAD_ARK_0027,
 PAD_ARK_0028,
 PAD_ARK_0029,
 PAD_ARK_002A,
 PAD_ARK_002B,
 PAD_ARK_002C,
 PAD_ARK_002D,
 PAD_ARK_002E,
 PAD_ARK_002F,
 PAD_ARK_0030,
 PAD_ARK_0031,
 PAD_ARK_0032,
 PAD_ARK_0033,
 PAD_ARK_0034,
 PAD_ARK_0035,
 PAD_ARK_0036,
 PAD_ARK_0037,
 PAD_ARK_0038,
 PAD_ARK_0039,
 PAD_ARK_003A,
 PAD_ARK_003B,
 PAD_ARK_003C,
 PAD_ARK_003D,
 PAD_ARK_003E,
 PAD_ARK_003F,
 PAD_ARK_0040,
 PAD_ARK_0041,
 PAD_ARK_0042,
 PAD_ARK_0043,
 PAD_ARK_0044,
 PAD_ARK_0045,
 PAD_ARK_0046,
 PAD_ARK_0047,
 PAD_ARK_0048,
 PAD_ARK_0049,
 PAD_ARK_004A,
 PAD_ARK_004B,
 PAD_ARK_004C,
 PAD_ARK_004D,
 PAD_ARK_004E,
 PAD_ARK_004F,
 PAD_ARK_0050,
 PAD_ARK_0051,
 PAD_ARK_0052,
 PAD_ARK_0053,
 PAD_ARK_0054,
 PAD_ARK_0055,
 PAD_ARK_0056,
 PAD_ARK_0057,
 PAD_ARK_0058,
 PAD_ARK_0059,
 PAD_ARK_005A,
 PAD_ARK_005B,
 PAD_ARK_005C,
 PAD_ARK_005D,
 PAD_ARK_005E,
 PAD_ARK_005F,
 PAD_ARK_0060,
 PAD_ARK_0061,
 PAD_ARK_0062,
 PAD_ARK_0063,
 PAD_ARK_0064,
 PAD_ARK_0065,
 PAD_ARK_0066,
 PAD_ARK_0067,
 PAD_ARK_0068,
 PAD_ARK_0069,
 PAD_ARK_006A,
 PAD_ARK_006B,
 PAD_ARK_006C,
 PAD_ARK_006D,
 PAD_ARK_006E,
 PAD_ARK_006F,
 PAD_ARK_0070,
 PAD_ARK_0071,
 PAD_ARK_0072,
 PAD_ARK_0073,
 PAD_ARK_0074,
 PAD_ARK_0075,
 PAD_ARK_0076,
 PAD_ARK_0077,
 PAD_ARK_0078,
 PAD_ARK_0079,
 PAD_ARK_007A,
 PAD_ARK_007B,
 PAD_ARK_007C,
 PAD_ARK_007D,
 PAD_ARK_007E,
 PAD_ARK_007F,
 PAD_ARK_0080,
 PAD_ARK_0081,
 PAD_ARK_0082,
 PAD_ARK_0083,
 PAD_ARK_0084,
 PAD_ARK_0085,
 PAD_ARK_0086,
 PAD_ARK_0087,
 PAD_ARK_0088,
 PAD_ARK_0089,
 PAD_ARK_008A,
 PAD_ARK_008B,
 PAD_ARK_008C,
 PAD_ARK_008D,
 PAD_ARK_008E,
 PAD_ARK_008F,
 PAD_ARK_0090,
 PAD_ARK_0091,
 PAD_ARK_0092,
 PAD_ARK_0093,
 PAD_ARK_0094,
 PAD_ARK_0095,
 PAD_ARK_0096,
 PAD_ARK_0097,
 PAD_ARK_0098,
 PAD_ARK_0099,
 PAD_ARK_009A,
 PAD_ARK_009B,
 PAD_ARK_009C,
 PAD_ARK_009D,
 PAD_ARK_009E,
 PAD_ARK_009F,
 PAD_ARK_00A0,
 PAD_ARK_00A1,
 PAD_ARK_00A2,
 PAD_ARK_00A3,
 PAD_ARK_00A4,
 PAD_ARK_00A5,
 PAD_ARK_00A6,
 PAD_ARK_00A7,
 PAD_ARK_00A8,
 PAD_ARK_00A9,
 PAD_ARK_00AA,
 PAD_ARK_00AB,
 PAD_ARK_00AC,
 PAD_ARK_00AD,
 PAD_ARK_00AE,
 PAD_ARK_00AF,
 PAD_ARK_00B0,
 PAD_ARK_00B1,
 PAD_ARK_00B2,
 PAD_ARK_00B3,
 PAD_ARK_00B4,
 PAD_ARK_00B5,
 PAD_ARK_00B6,
 PAD_ARK_00B7,
 PAD_ARK_00B8,
 PAD_ARK_00B9,
 PAD_ARK_00BA,
 PAD_ARK_00BB,
 PAD_ARK_00BC,
 PAD_ARK_00BD,
 PAD_ARK_00BE,
 PAD_ARK_00BF,
 PAD_ARK_00C0,
 PAD_ARK_00C1,
 PAD_ARK_00C2,
 PAD_ARK_00C3,
 PAD_ARK_00C4,
 PAD_ARK_00C5,
 PAD_ARK_00C6,
 PAD_ARK_00C7,
 PAD_ARK_00C8,
 PAD_ARK_00C9,
 PAD_ARK_00CA,
 PAD_ARK_00CB,
 PAD_ARK_00CC,
 PAD_ARK_00CD,
 PAD_ARK_00CE,
 PAD_ARK_00CF,
 PAD_ARK_00D0,
 PAD_ARK_00D1,
 PAD_ARK_00D2,
 PAD_ARK_00D3,
 PAD_ARK_00D4,
 PAD_ARK_00D5,
 PAD_ARK_00D6,
 PAD_ARK_00D7,
 PAD_ARK_00D8,
 PAD_ARK_00D9,
 PAD_ARK_00DA,
 PAD_ARK_00DB,
 PAD_ARK_00DC,
 PAD_ARK_00DD,
 PAD_ARK_00DE,
 PAD_ARK_00DF,
 PAD_ARK_00E0,
 PAD_ARK_00E1,
 PAD_ARK_00E2,
 PAD_ARK_00E3,
 PAD_ARK_00E4,
 PAD_ARK_00E5,
 PAD_ARK_00E6,
 PAD_ARK_00E7,
 PAD_ARK_00E8,
 PAD_ARK_00E9,
 PAD_ARK_00EA,
 PAD_ARK_00EB,
 PAD_ARK_00EC,
 PAD_ARK_00ED,
 PAD_ARK_00EE,
 PAD_ARK_00EF,
 PAD_ARK_00F0,
 PAD_ARK_00F1,
 PAD_ARK_00F2,
 PAD_ARK_00F3,
 PAD_ARK_00F4,
 PAD_ARK_00F5,
 PAD_ARK_00F6,
 PAD_ARK_00F7,
 PAD_ARK_00F8,
 PAD_ARK_00F9,
 PAD_ARK_00FA,
 PAD_ARK_00FB,
 PAD_ARK_00FC,
 PAD_ARK_00FD,
 PAD_ARK_00FE,
 PAD_ARK_00FF,
 PAD_ARK_0100,
 PAD_ARK_0101,
 PAD_ARK_0102,
 PAD_ARK_0103,
 PAD_ARK_0104,
 PAD_ARK_0105,
 PAD_ARK_0106,
 PAD_ARK_0107,
 PAD_ARK_0108,
 PAD_ARK_0109,
 PAD_ARK_010A,
 PAD_ARK_010B,
 PAD_ARK_010C,
 PAD_ARK_010D,
 PAD_ARK_010E,
 PAD_ARK_010F,
 PAD_ARK_0110,
 PAD_ARK_0111,
 PAD_ARK_0112,
 PAD_ARK_0113,
 PAD_ARK_0114,
 PAD_ARK_0115,
 PAD_ARK_0116,
 PAD_ARK_0117,
 PAD_ARK_0118,
 PAD_ARK_0119,
 PAD_ARK_011A,
 PAD_ARK_011B,
 PAD_ARK_011C,
 PAD_ARK_011D,
 PAD_ARK_011E,
 PAD_ARK_011F,
 PAD_ARK_0120,
 PAD_ARK_0121,
 PAD_ARK_0122,
 PAD_ARK_0123,
 PAD_ARK_0124,
 PAD_ARK_0125,
 PAD_ARK_0126,
 PAD_ARK_0127,
 PAD_ARK_0128,
 PAD_ARK_0129,
 PAD_ARK_012A,
 PAD_ARK_012B,
 PAD_ARK_012C,
 PAD_ARK_012D,
 PAD_ARK_012E,
 PAD_ARK_012F,
 PAD_ARK_0130,
 PAD_ARK_0131,
 PAD_ARK_0132,
 PAD_ARK_0133,
 PAD_ARK_0134,
 PAD_ARK_0135,
 PAD_ARK_0136,
 PAD_ARK_0137,
 PAD_ARK_0138,
 PAD_ARK_0139,
 PAD_ARK_013A,
 PAD_ARK_013B,
 PAD_ARK_013C,
 PAD_ARK_013D,
 PAD_ARK_013E,
 PAD_ARK_013F,
 PAD_ARK_0140,
 PAD_ARK_0141,
 PAD_ARK_0142,
 PAD_ARK_0143,
 PAD_ARK_0144,
 PAD_ARK_0145,
 PAD_ARK_0146,
 PAD_ARK_0147,
 PAD_ARK_0148,
 PAD_ARK_0149,
 PAD_ARK_014A,
 PAD_ARK_014B,
 PAD_ARK_014C,
 PAD_ARK_014D,
 PAD_ARK_014E,
 PAD_ARK_014F,
 PAD_ARK_0150,
 PAD_ARK_0151,
 PAD_ARK_0152,
 PAD_ARK_0153,
 PAD_ARK_0154,
 PAD_ARK_0155,
 PAD_ARK_0156,
 PAD_ARK_0157,
 PAD_ARK_0158,
 PAD_ARK_0159,
 PAD_ARK_015A,
 PAD_ARK_015B,
 PAD_ARK_015C,
 PAD_ARK_015D,
 PAD_ARK_015E,
 PAD_ARK_015F,
 PAD_ARK_0160,
 PAD_ARK_0161,
 PAD_ARK_0162,
 PAD_ARK_0163,
 PAD_ARK_0164,
 PAD_ARK_0165,
 PAD_ARK_0166,
 PAD_ARK_0167,
 PAD_ARK_0168,
 PAD_ARK_0169,
 PAD_ARK_016A,
 PAD_ARK_016B,
 PAD_ARK_016C,
 PAD_ARK_016D,
 PAD_ARK_016E,
 PAD_ARK_016F,
 PAD_ARK_0170,
 PAD_ARK_0171,
 PAD_ARK_0172,
 PAD_ARK_0173,
 PAD_ARK_0174,
 PAD_ARK_0175,
 PAD_ARK_0176,
 PAD_ARK_0177,
 PAD_ARK_0178,
 PAD_ARK_0179,
 PAD_ARK_017A,
 PAD_ARK_017B,
 PAD_ARK_017C,
 PAD_ARK_017D,
 PAD_ARK_017E,
 PAD_ARK_017F,
 PAD_ARK_0180,
 PAD_ARK_0181,
 PAD_ARK_0182,
 PAD_ARK_0183,
 PAD_ARK_0184,
 PAD_ARK_0185,
 PAD_ARK_0186,
 PAD_ARK_0187,
 PAD_ARK_0188,
 PAD_ARK_0189,
 PAD_ARK_018A,
 PAD_ARK_018B,
 PAD_ARK_018C,
 PAD_ARK_018D,
 PAD_ARK_018E,
 PAD_ARK_018F,
 PAD_ARK_0190,
 PAD_ARK_0191,
 PAD_ARK_0192,
 PAD_ARK_0193,
 PAD_ARK_0194,
 PAD_ARK_0195,
 PAD_ARK_0196,
 PAD_ARK_0197,
 PAD_ARK_0198,
 PAD_ARK_0199,
 PAD_ARK_019A,
 PAD_ARK_019B,
 PAD_ARK_019C,
 PAD_ARK_019D,
 PAD_ARK_019E,
 PAD_ARK_019F,
 PAD_ARK_01A0,
 PAD_ARK_01A1,
 PAD_ARK_01A2,
 PAD_ARK_01A3,
 PAD_ARK_01A4,
 PAD_ARK_01A5,
 PAD_ARK_01A6,
 PAD_ARK_01A7,
 PAD_ARK_01A8,
 PAD_ARK_01A9,
 PAD_ARK_01AA,
 PAD_ARK_01AB,
 PAD_ARK_01AC,
 PAD_ARK_01AD,
 PAD_ARK_01AE,
 PAD_ARK_01AF,
 PAD_ARK_01B0,
 PAD_ARK_01B1,
 PAD_ARK_01B2,
 PAD_ARK_01B3,
 PAD_ARK_01B4,
 PAD_ARK_01B5,
 PAD_ARK_01B6,
 PAD_ARK_01B7,
 PAD_ARK_01B8,
 PAD_ARK_01B9,
 PAD_ARK_01BA,
 PAD_ARK_01BB,
 PAD_ARK_01BC,
 PAD_ARK_01BD,
 PAD_ARK_01BE,
 PAD_ARK_01BF,
 PAD_ARK_01C0,
 PAD_ARK_01C1,
 PAD_ARK_01C2,
 PAD_ARK_01C3,
 PAD_ARK_01C4,
 PAD_ARK_01C5,
 PAD_ARK_01C6,
 PAD_ARK_01C7,
 PAD_ARK_01C8,
 PAD_ARK_01C9,
 PAD_ARK_01CA,
 PAD_ARK_01CB,
 PAD_ARK_01CC,
 PAD_ARK_01CD,
 PAD_ARK_01CE,
 PAD_ARK_01CF,
 PAD_ARK_01D0,
 PAD_ARK_01D1,
 PAD_ARK_01D2,
 PAD_ARK_01D3,
 PAD_ARK_01D4,
 PAD_ARK_01D5,
 PAD_ARK_01D6,
 PAD_ARK_01D7,
 PAD_ARK_01D8,
 PAD_ARK_01D9,
 PAD_ARK_01DA,
 PAD_ARK_01DB,
 PAD_ARK_01DC,
 PAD_ARK_01DD,
 PAD_ARK_01DE,
 PAD_ARK_01DF,
 PAD_ARK_01E0,
 PAD_ARK_01E1,
 PAD_ARK_01E2,
 PAD_ARK_01E3,
 PAD_ARK_01E4,
 PAD_ARK_01E5,
 PAD_ARK_01E6,
 PAD_ARK_01E7,
 PAD_ARK_01E8,
 PAD_ARK_01E9,
 PAD_ARK_01EA,
 PAD_ARK_01EB,
 PAD_ARK_01EC,
 PAD_ARK_01ED,
 PAD_ARK_01EE,
 PAD_ARK_01EF,
 PAD_ARK_01F0,
 PAD_ARK_01F1,
 PAD_ARK_01F2,
 PAD_ARK_01F3,
 PAD_ARK_01F4,
 PAD_ARK_01F5,
 PAD_ARK_01F6,
 PAD_ARK_01F7,
 PAD_ARK_01F8,
 PAD_ARK_01F9,
 PAD_ARK_01FA,
 PAD_ARK_01FB,
 PAD_ARK_01FC,
 PAD_ARK_01FD,
 PAD_ARK_01FE,
 PAD_ARK_01FF,
 PAD_ARK_0200,
 PAD_ARK_0201,
 PAD_ARK_0202,
 PAD_ARK_0203,
 PAD_ARK_0204,
 PAD_ARK_0205,
 PAD_ARK_0206,
 PAD_ARK_0207,
 PAD_ARK_0208,
 PAD_ARK_0209,
 PAD_ARK_020A,
 PAD_ARK_020B,
 PAD_ARK_020C,
 PAD_ARK_020D,
 PAD_ARK_020E,
 PAD_ARK_020F,
 PAD_ARK_0210,
 PAD_ARK_0211,
 PAD_ARK_0212,
 PAD_ARK_0213,
 PAD_ARK_0214,
 PAD_ARK_0215,
 PAD_ARK_0216,
 PAD_ARK_0217,
 PAD_ARK_0218,
 PAD_ARK_0219,
 PAD_ARK_021A,
 PAD_ARK_021B,
 PAD_ARK_021C,
 PAD_ARK_021D,
 PAD_ARK_021E,
 PAD_ARK_021F,
 PAD_ARK_0220,
 PAD_ARK_0221,
 PAD_ARK_0222,
 PAD_ARK_0223,
 PAD_ARK_0224,
 PAD_ARK_0225,
 PAD_ARK_0226,
 PAD_ARK_0227,
 PAD_ARK_0228,
 PAD_ARK_0229,
 PAD_ARK_022A,
 PAD_ARK_022B,
 PAD_ARK_022C,
 PAD_ARK_022D,
 PAD_ARK_022E,
 PAD_ARK_022F,
 PAD_ARK_0230,
 PAD_ARK_0231,
 PAD_ARK_0232,
 PAD_ARK_0233,
 PAD_ARK_0234,
 PAD_ARK_0235,
 PAD_ARK_0236,
 PAD_ARK_0237,
 PAD_ARK_0238,
 PAD_ARK_0239,
 PAD_ARK_023A,
 PAD_ARK_023B,
 PAD_ARK_023C,
 PAD_ARK_023D,
 PAD_ARK_023E,
 PAD_ARK_023F,
 PAD_ARK_0240,
 PAD_ARK_0241,
 PAD_ARK_0242,
 PAD_ARK_0243,
 PAD_ARK_0244,
 PAD_ARK_0245,
 PAD_ARK_0246,
 PAD_ARK_0247,
 PAD_ARK_0248,
 PAD_ARK_0249,
 PAD_ARK_024A,
 PAD_ARK_024B,
 PAD_ARK_024C,
 PAD_ARK_024D,
 PAD_ARK_024E,
 PAD_ARK_024F,
 PAD_ARK_0250,
 PAD_ARK_0251,
 PAD_ARK_0252,
 PAD_ARK_0253,
 PAD_ARK_0254,
 PAD_ARK_0255,
 PAD_ARK_0256,
 PAD_ARK_0257,
 PAD_ARK_0258,
 PAD_ARK_0259,
 PAD_ARK_025A,
 PAD_ARK_025B,
 PAD_ARK_025C,
 PAD_ARK_025D,
 PAD_ARK_025E,
 PAD_ARK_END
};
enum pad_ash {
 PAD_ASH_END
};
enum pad_azt {
 PAD_AZT_0000,
 PAD_AZT_0001,
 PAD_AZT_0002,
 PAD_AZT_0003,
 PAD_AZT_0004,
 PAD_AZT_0005,
 PAD_AZT_0006,
 PAD_AZT_0007,
 PAD_AZT_0008,
 PAD_AZT_0009,
 PAD_AZT_000A,
 PAD_AZT_000B,
 PAD_AZT_000C,
 PAD_AZT_000D,
 PAD_AZT_000E,
 PAD_AZT_000F,
 PAD_AZT_0010,
 PAD_AZT_0011,
 PAD_AZT_0012,
 PAD_AZT_0013,
 PAD_AZT_0014,
 PAD_AZT_0015,
 PAD_AZT_0016,
 PAD_AZT_0017,
 PAD_AZT_0018,
 PAD_AZT_0019,
 PAD_AZT_001A,
 PAD_AZT_001B,
 PAD_AZT_001C,
 PAD_AZT_001D,
 PAD_AZT_001E,
 PAD_AZT_001F,
 PAD_AZT_0020,
 PAD_AZT_0021,
 PAD_AZT_0022,
 PAD_AZT_0023,
 PAD_AZT_0024,
 PAD_AZT_0025,
 PAD_AZT_0026,
 PAD_AZT_0027,
 PAD_AZT_0028,
 PAD_AZT_0029,
 PAD_AZT_002A,
 PAD_AZT_002B,
 PAD_AZT_002C,
 PAD_AZT_002D,
 PAD_AZT_002E,
 PAD_AZT_002F,
 PAD_AZT_0030,
 PAD_AZT_0031,
 PAD_AZT_0032,
 PAD_AZT_0033,
 PAD_AZT_0034,
 PAD_AZT_0035,
 PAD_AZT_0036,
 PAD_AZT_0037,
 PAD_AZT_0038,
 PAD_AZT_0039,
 PAD_AZT_003A,
 PAD_AZT_003B,
 PAD_AZT_003C,
 PAD_AZT_003D,
 PAD_AZT_003E,
 PAD_AZT_003F,
 PAD_AZT_0040,
 PAD_AZT_0041,
 PAD_AZT_0042,
 PAD_AZT_0043,
 PAD_AZT_0044,
 PAD_AZT_0045,
 PAD_AZT_0046,
 PAD_AZT_0047,
 PAD_AZT_0048,
 PAD_AZT_0049,
 PAD_AZT_004A,
 PAD_AZT_004B,
 PAD_AZT_004C,
 PAD_AZT_004D,
 PAD_AZT_004E,
 PAD_AZT_004F,
 PAD_AZT_0050,
 PAD_AZT_0051,
 PAD_AZT_0052,
 PAD_AZT_0053,
 PAD_AZT_0054,
 PAD_AZT_0055,
 PAD_AZT_0056,
 PAD_AZT_0057,
 PAD_AZT_0058,
 PAD_AZT_0059,
 PAD_AZT_005A,
 PAD_AZT_005B,
 PAD_AZT_005C,
 PAD_AZT_005D,
 PAD_AZT_005E,
 PAD_AZT_005F,
 PAD_AZT_0060,
 PAD_AZT_0061,
 PAD_AZT_0062,
 PAD_AZT_0063,
 PAD_AZT_0064,
 PAD_AZT_0065,
 PAD_AZT_0066,
 PAD_AZT_0067,
 PAD_AZT_0068,
 PAD_AZT_0069,
 PAD_AZT_006A,
 PAD_AZT_006B,
 PAD_AZT_006C,
 PAD_AZT_006D,
 PAD_AZT_006E,
 PAD_AZT_006F,
 PAD_AZT_0070,
 PAD_AZT_0071,
 PAD_AZT_0072,
 PAD_AZT_0073,
 PAD_AZT_0074,
 PAD_AZT_0075,
 PAD_AZT_0076,
 PAD_AZT_0077,
 PAD_AZT_0078,
 PAD_AZT_0079,
 PAD_AZT_007A,
 PAD_AZT_007B,
 PAD_AZT_007C,
 PAD_AZT_007D,
 PAD_AZT_007E,
 PAD_AZT_007F,
 PAD_AZT_0080,
 PAD_AZT_0081,
 PAD_AZT_0082,
 PAD_AZT_0083,
 PAD_AZT_0084,
 PAD_AZT_0085,
 PAD_AZT_0086,
 PAD_AZT_0087,
 PAD_AZT_0088,
 PAD_AZT_0089,
 PAD_AZT_008A,
 PAD_AZT_008B,
 PAD_AZT_008C,
 PAD_AZT_008D,
 PAD_AZT_008E,
 PAD_AZT_008F,
 PAD_AZT_0090,
 PAD_AZT_0091,
 PAD_AZT_0092,
 PAD_AZT_0093,
 PAD_AZT_0094,
 PAD_AZT_0095,
 PAD_AZT_0096,
 PAD_AZT_0097,
 PAD_AZT_0098,
 PAD_AZT_0099,
 PAD_AZT_009A,
 PAD_AZT_009B,
 PAD_AZT_009C,
 PAD_AZT_009D,
 PAD_AZT_009E,
 PAD_AZT_009F,
 PAD_AZT_00A0,
 PAD_AZT_00A1,
 PAD_AZT_00A2,
 PAD_AZT_00A3,
 PAD_AZT_00A4,
 PAD_AZT_00A5,
 PAD_AZT_00A6,
 PAD_AZT_00A7,
 PAD_AZT_00A8,
 PAD_AZT_00A9,
 PAD_AZT_00AA,
 PAD_AZT_00AB,
 PAD_AZT_00AC,
 PAD_AZT_00AD,
 PAD_AZT_00AE,
 PAD_AZT_00AF,
 PAD_AZT_00B0,
 PAD_AZT_00B1,
 PAD_AZT_00B2,
 PAD_AZT_00B3,
 PAD_AZT_00B4,
 PAD_AZT_00B5,
 PAD_AZT_00B6,
 PAD_AZT_00B7,
 PAD_AZT_00B8,
 PAD_AZT_00B9,
 PAD_AZT_00BA,
 PAD_AZT_00BB,
 PAD_AZT_00BC,
 PAD_AZT_00BD,
 PAD_AZT_00BE,
 PAD_AZT_00BF,
 PAD_AZT_00C0,
 PAD_AZT_00C1,
 PAD_AZT_00C2,
 PAD_AZT_00C3,
 PAD_AZT_00C4,
 PAD_AZT_00C5,
 PAD_AZT_00C6,
 PAD_AZT_00C7,
 PAD_AZT_00C8,
 PAD_AZT_00C9,
 PAD_AZT_00CA,
 PAD_AZT_00CB,
 PAD_AZT_00CC,
 PAD_AZT_00CD,
 PAD_AZT_00CE,
 PAD_AZT_00CF,
 PAD_AZT_00D0,
 PAD_AZT_00D1,
 PAD_AZT_00D2,
 PAD_AZT_00D3,
 PAD_AZT_00D4,
 PAD_AZT_00D5,
 PAD_AZT_00D6,
 PAD_AZT_00D7,
 PAD_AZT_00D8,
 PAD_AZT_00D9,
 PAD_AZT_00DA,
 PAD_AZT_00DB,
 PAD_AZT_00DC,
 PAD_AZT_00DD,
 PAD_AZT_00DE,
 PAD_AZT_00DF,
 PAD_AZT_00E0,
 PAD_AZT_00E1,
 PAD_AZT_00E2,
 PAD_AZT_00E3,
 PAD_AZT_00E4,
 PAD_AZT_00E5,
 PAD_AZT_00E6,
 PAD_AZT_00E7,
 PAD_AZT_00E8,
 PAD_AZT_00E9,
 PAD_AZT_00EA,
 PAD_AZT_00EB,
 PAD_AZT_00EC,
 PAD_AZT_00ED,
 PAD_AZT_00EE,
 PAD_AZT_00EF,
 PAD_AZT_00F0,
 PAD_AZT_00F1,
 PAD_AZT_00F2,
 PAD_AZT_00F3,
 PAD_AZT_00F4,
 PAD_AZT_00F5,
 PAD_AZT_00F6,
 PAD_AZT_00F7,
 PAD_AZT_00F8,
 PAD_AZT_00F9,
 PAD_AZT_00FA,
 PAD_AZT_00FB,
 PAD_AZT_00FC,
 PAD_AZT_00FD,
 PAD_AZT_00FE,
 PAD_AZT_00FF,
 PAD_AZT_0100,
 PAD_AZT_0101,
 PAD_AZT_0102,
 PAD_AZT_0103,
 PAD_AZT_0104,
 PAD_AZT_0105,
 PAD_AZT_0106,
 PAD_AZT_0107,
 PAD_AZT_0108,
 PAD_AZT_0109,
 PAD_AZT_010A,
 PAD_AZT_010B,
 PAD_AZT_010C,
 PAD_AZT_010D,
 PAD_AZT_010E,
 PAD_AZT_010F,
 PAD_AZT_0110,
 PAD_AZT_0111,
 PAD_AZT_0112,
 PAD_AZT_0113,
 PAD_AZT_0114,
 PAD_AZT_0115,
 PAD_AZT_0116,
 PAD_AZT_0117,
 PAD_AZT_0118,
 PAD_AZT_0119,
 PAD_AZT_011A,
 PAD_AZT_011B,
 PAD_AZT_011C,
 PAD_AZT_011D,
 PAD_AZT_011E,
 PAD_AZT_011F,
 PAD_AZT_0120,
 PAD_AZT_0121,
 PAD_AZT_0122,
 PAD_AZT_0123,
 PAD_AZT_0124,
 PAD_AZT_0125,
 PAD_AZT_0126,
 PAD_AZT_0127,
 PAD_AZT_0128,
 PAD_AZT_0129,
 PAD_AZT_012A,
 PAD_AZT_012B,
 PAD_AZT_012C,
 PAD_AZT_012D,
 PAD_AZT_012E,
 PAD_AZT_012F,
 PAD_AZT_0130,
 PAD_AZT_0131,
 PAD_AZT_0132,
 PAD_AZT_0133,
 PAD_AZT_0134,
 PAD_AZT_0135,
 PAD_AZT_0136,
 PAD_AZT_0137,
 PAD_AZT_0138,
 PAD_AZT_0139,
 PAD_AZT_013A,
 PAD_AZT_013B,
 PAD_AZT_013C,
 PAD_AZT_013D,
 PAD_AZT_013E,
 PAD_AZT_013F,
 PAD_AZT_0140,
 PAD_AZT_0141,
 PAD_AZT_0142,
 PAD_AZT_0143,
 PAD_AZT_0144,
 PAD_AZT_0145,
 PAD_AZT_0146,
 PAD_AZT_0147,
 PAD_AZT_0148,
 PAD_AZT_0149,
 PAD_AZT_014A,
 PAD_AZT_014B,
 PAD_AZT_014C,
 PAD_AZT_014D,
 PAD_AZT_014E,
 PAD_AZT_014F,
 PAD_AZT_0150,
 PAD_AZT_0151,
 PAD_AZT_0152,
 PAD_AZT_0153,
 PAD_AZT_0154,
 PAD_AZT_0155,
 PAD_AZT_0156,
 PAD_AZT_0157,
 PAD_AZT_0158,
 PAD_AZT_0159,
 PAD_AZT_015A,
 PAD_AZT_015B,
 PAD_AZT_015C,
 PAD_AZT_015D,
 PAD_AZT_015E,
 PAD_AZT_015F,
 PAD_AZT_0160,
 PAD_AZT_0161,
 PAD_AZT_0162,
 PAD_AZT_0163,
 PAD_AZT_0164,
 PAD_AZT_0165,
 PAD_AZT_0166,
 PAD_AZT_0167,
 PAD_AZT_0168,
 PAD_AZT_0169,
 PAD_AZT_016A,
 PAD_AZT_016B,
 PAD_AZT_016C,
 PAD_AZT_016D,
 PAD_AZT_016E,
 PAD_AZT_016F,
 PAD_AZT_0170,
 PAD_AZT_0171,
 PAD_AZT_0172,
 PAD_AZT_0173,
 PAD_AZT_0174,
 PAD_AZT_0175,
 PAD_AZT_0176,
 PAD_AZT_0177,
 PAD_AZT_0178,
 PAD_AZT_0179,
 PAD_AZT_017A,
 PAD_AZT_017B,
 PAD_AZT_017C,
 PAD_AZT_017D,
 PAD_AZT_017E,
 PAD_AZT_017F,
 PAD_AZT_0180,
 PAD_AZT_0181,
 PAD_AZT_0182,
 PAD_AZT_0183,
 PAD_AZT_0184,
 PAD_AZT_0185,
 PAD_AZT_0186,
 PAD_AZT_0187,
 PAD_AZT_0188,
 PAD_AZT_0189,
 PAD_AZT_018A,
 PAD_AZT_018B,
 PAD_AZT_018C,
 PAD_AZT_018D,
 PAD_AZT_018E,
 PAD_AZT_018F,
 PAD_AZT_0190,
 PAD_AZT_0191,
 PAD_AZT_0192,
 PAD_AZT_0193,
 PAD_AZT_0194,
 PAD_AZT_0195,
 PAD_AZT_0196,
 PAD_AZT_0197,
 PAD_AZT_0198,
 PAD_AZT_0199,
 PAD_AZT_019A,
 PAD_AZT_019B,
 PAD_AZT_019C,
 PAD_AZT_019D,
 PAD_AZT_019E,
 PAD_AZT_019F,
 PAD_AZT_01A0,
 PAD_AZT_01A1,
 PAD_AZT_01A2,
 PAD_AZT_01A3,
 PAD_AZT_01A4,
 PAD_AZT_01A5,
 PAD_AZT_01A6,
 PAD_AZT_01A7,
 PAD_AZT_01A8,
 PAD_AZT_01A9,
 PAD_AZT_01AA,
 PAD_AZT_01AB,
 PAD_AZT_01AC,
 PAD_AZT_01AD,
 PAD_AZT_01AE,
 PAD_AZT_01AF,
 PAD_AZT_01B0,
 PAD_AZT_01B1,
 PAD_AZT_01B2,
 PAD_AZT_01B3,
 PAD_AZT_01B4,
 PAD_AZT_01B5,
 PAD_AZT_01B6,
 PAD_AZT_01B7,
 PAD_AZT_01B8,
 PAD_AZT_01B9,
 PAD_AZT_01BA,
 PAD_AZT_01BB,
 PAD_AZT_01BC,
 PAD_AZT_01BD,
 PAD_AZT_01BE,
 PAD_AZT_01BF,
 PAD_AZT_01C0,
 PAD_AZT_01C1,
 PAD_AZT_01C2,
 PAD_AZT_01C3,
 PAD_AZT_01C4,
 PAD_AZT_01C5,
 PAD_AZT_01C6,
 PAD_AZT_01C7,
 PAD_AZT_01C8,
 PAD_AZT_01C9,
 PAD_AZT_01CA,
 PAD_AZT_01CB,
 PAD_AZT_01CC,
 PAD_AZT_01CD,
 PAD_AZT_01CE,
 PAD_AZT_01CF,
 PAD_AZT_01D0,
 PAD_AZT_01D1,
 PAD_AZT_01D2,
 PAD_AZT_01D3,
 PAD_AZT_01D4,
 PAD_AZT_01D5,
 PAD_AZT_01D6,
 PAD_AZT_01D7,
 PAD_AZT_01D8,
 PAD_AZT_01D9,
 PAD_AZT_01DA,
 PAD_AZT_01DB,
 PAD_AZT_01DC,
 PAD_AZT_01DD,
 PAD_AZT_01DE,
 PAD_AZT_END
};
enum pad_cat {
 PAD_CAT_END
};
enum pad_cave {
 PAD_CAVE_0000,
 PAD_CAVE_0001,
 PAD_CAVE_0002,
 PAD_CAVE_0003,
 PAD_CAVE_0004,
 PAD_CAVE_0005,
 PAD_CAVE_0006,
 PAD_CAVE_0007,
 PAD_CAVE_0008,
 PAD_CAVE_0009,
 PAD_CAVE_000A,
 PAD_CAVE_000B,
 PAD_CAVE_000C,
 PAD_CAVE_000D,
 PAD_CAVE_000E,
 PAD_CAVE_000F,
 PAD_CAVE_0010,
 PAD_CAVE_0011,
 PAD_CAVE_0012,
 PAD_CAVE_0013,
 PAD_CAVE_0014,
 PAD_CAVE_0015,
 PAD_CAVE_0016,
 PAD_CAVE_0017,
 PAD_CAVE_0018,
 PAD_CAVE_0019,
 PAD_CAVE_001A,
 PAD_CAVE_001B,
 PAD_CAVE_001C,
 PAD_CAVE_001D,
 PAD_CAVE_001E,
 PAD_CAVE_001F,
 PAD_CAVE_0020,
 PAD_CAVE_0021,
 PAD_CAVE_0022,
 PAD_CAVE_0023,
 PAD_CAVE_0024,
 PAD_CAVE_0025,
 PAD_CAVE_0026,
 PAD_CAVE_0027,
 PAD_CAVE_0028,
 PAD_CAVE_0029,
 PAD_CAVE_002A,
 PAD_CAVE_002B,
 PAD_CAVE_002C,
 PAD_CAVE_002D,
 PAD_CAVE_002E,
 PAD_CAVE_002F,
 PAD_CAVE_0030,
 PAD_CAVE_0031,
 PAD_CAVE_0032,
 PAD_CAVE_0033,
 PAD_CAVE_0034,
 PAD_CAVE_0035,
 PAD_CAVE_0036,
 PAD_CAVE_0037,
 PAD_CAVE_0038,
 PAD_CAVE_0039,
 PAD_CAVE_003A,
 PAD_CAVE_003B,
 PAD_CAVE_003C,
 PAD_CAVE_003D,
 PAD_CAVE_003E,
 PAD_CAVE_003F,
 PAD_CAVE_0040,
 PAD_CAVE_0041,
 PAD_CAVE_0042,
 PAD_CAVE_0043,
 PAD_CAVE_0044,
 PAD_CAVE_0045,
 PAD_CAVE_0046,
 PAD_CAVE_0047,
 PAD_CAVE_0048,
 PAD_CAVE_0049,
 PAD_CAVE_004A,
 PAD_CAVE_004B,
 PAD_CAVE_004C,
 PAD_CAVE_004D,
 PAD_CAVE_004E,
 PAD_CAVE_004F,
 PAD_CAVE_0050,
 PAD_CAVE_0051,
 PAD_CAVE_0052,
 PAD_CAVE_0053,
 PAD_CAVE_0054,
 PAD_CAVE_0055,
 PAD_CAVE_0056,
 PAD_CAVE_0057,
 PAD_CAVE_0058,
 PAD_CAVE_0059,
 PAD_CAVE_005A,
 PAD_CAVE_005B,
 PAD_CAVE_005C,
 PAD_CAVE_005D,
 PAD_CAVE_005E,
 PAD_CAVE_005F,
 PAD_CAVE_0060,
 PAD_CAVE_0061,
 PAD_CAVE_0062,
 PAD_CAVE_0063,
 PAD_CAVE_0064,
 PAD_CAVE_0065,
 PAD_CAVE_0066,
 PAD_CAVE_0067,
 PAD_CAVE_0068,
 PAD_CAVE_0069,
 PAD_CAVE_006A,
 PAD_CAVE_006B,
 PAD_CAVE_006C,
 PAD_CAVE_006D,
 PAD_CAVE_006E,
 PAD_CAVE_006F,
 PAD_CAVE_0070,
 PAD_CAVE_0071,
 PAD_CAVE_0072,
 PAD_CAVE_0073,
 PAD_CAVE_0074,
 PAD_CAVE_0075,
 PAD_CAVE_0076,
 PAD_CAVE_0077,
 PAD_CAVE_0078,
 PAD_CAVE_0079,
 PAD_CAVE_007A,
 PAD_CAVE_007B,
 PAD_CAVE_007C,
 PAD_CAVE_007D,
 PAD_CAVE_007E,
 PAD_CAVE_007F,
 PAD_CAVE_0080,
 PAD_CAVE_0081,
 PAD_CAVE_0082,
 PAD_CAVE_0083,
 PAD_CAVE_0084,
 PAD_CAVE_0085,
 PAD_CAVE_0086,
 PAD_CAVE_0087,
 PAD_CAVE_0088,
 PAD_CAVE_0089,
 PAD_CAVE_008A,
 PAD_CAVE_008B,
 PAD_CAVE_008C,
 PAD_CAVE_008D,
 PAD_CAVE_008E,
 PAD_CAVE_008F,
 PAD_CAVE_0090,
 PAD_CAVE_0091,
 PAD_CAVE_0092,
 PAD_CAVE_0093,
 PAD_CAVE_0094,
 PAD_CAVE_0095,
 PAD_CAVE_0096,
 PAD_CAVE_0097,
 PAD_CAVE_0098,
 PAD_CAVE_0099,
 PAD_CAVE_009A,
 PAD_CAVE_009B,
 PAD_CAVE_009C,
 PAD_CAVE_009D,
 PAD_CAVE_009E,
 PAD_CAVE_009F,
 PAD_CAVE_00A0,
 PAD_CAVE_00A1,
 PAD_CAVE_00A2,
 PAD_CAVE_00A3,
 PAD_CAVE_00A4,
 PAD_CAVE_00A5,
 PAD_CAVE_00A6,
 PAD_CAVE_00A7,
 PAD_CAVE_00A8,
 PAD_CAVE_00A9,
 PAD_CAVE_00AA,
 PAD_CAVE_00AB,
 PAD_CAVE_00AC,
 PAD_CAVE_00AD,
 PAD_CAVE_00AE,
 PAD_CAVE_00AF,
 PAD_CAVE_00B0,
 PAD_CAVE_00B1,
 PAD_CAVE_00B2,
 PAD_CAVE_00B3,
 PAD_CAVE_00B4,
 PAD_CAVE_00B5,
 PAD_CAVE_00B6,
 PAD_CAVE_00B7,
 PAD_CAVE_00B8,
 PAD_CAVE_00B9,
 PAD_CAVE_00BA,
 PAD_CAVE_00BB,
 PAD_CAVE_00BC,
 PAD_CAVE_00BD,
 PAD_CAVE_00BE,
 PAD_CAVE_00BF,
 PAD_CAVE_00C0,
 PAD_CAVE_00C1,
 PAD_CAVE_00C2,
 PAD_CAVE_00C3,
 PAD_CAVE_00C4,
 PAD_CAVE_00C5,
 PAD_CAVE_00C6,
 PAD_CAVE_00C7,
 PAD_CAVE_00C8,
 PAD_CAVE_00C9,
 PAD_CAVE_00CA,
 PAD_CAVE_00CB,
 PAD_CAVE_00CC,
 PAD_CAVE_00CD,
 PAD_CAVE_00CE,
 PAD_CAVE_00CF,
 PAD_CAVE_00D0,
 PAD_CAVE_00D1,
 PAD_CAVE_00D2,
 PAD_CAVE_00D3,
 PAD_CAVE_00D4,
 PAD_CAVE_00D5,
 PAD_CAVE_00D6,
 PAD_CAVE_00D7,
 PAD_CAVE_00D8,
 PAD_CAVE_00D9,
 PAD_CAVE_00DA,
 PAD_CAVE_00DB,
 PAD_CAVE_00DC,
 PAD_CAVE_00DD,
 PAD_CAVE_00DE,
 PAD_CAVE_00DF,
 PAD_CAVE_00E0,
 PAD_CAVE_00E1,
 PAD_CAVE_00E2,
 PAD_CAVE_00E3,
 PAD_CAVE_00E4,
 PAD_CAVE_00E5,
 PAD_CAVE_00E6,
 PAD_CAVE_00E7,
 PAD_CAVE_00E8,
 PAD_CAVE_00E9,
 PAD_CAVE_00EA,
 PAD_CAVE_00EB,
 PAD_CAVE_00EC,
 PAD_CAVE_00ED,
 PAD_CAVE_00EE,
 PAD_CAVE_00EF,
 PAD_CAVE_00F0,
 PAD_CAVE_00F1,
 PAD_CAVE_00F2,
 PAD_CAVE_00F3,
 PAD_CAVE_00F4,
 PAD_CAVE_00F5,
 PAD_CAVE_00F6,
 PAD_CAVE_00F7,
 PAD_CAVE_00F8,
 PAD_CAVE_00F9,
 PAD_CAVE_00FA,
 PAD_CAVE_00FB,
 PAD_CAVE_00FC,
 PAD_CAVE_00FD,
 PAD_CAVE_00FE,
 PAD_CAVE_00FF,
 PAD_CAVE_0100,
 PAD_CAVE_0101,
 PAD_CAVE_0102,
 PAD_CAVE_0103,
 PAD_CAVE_0104,
 PAD_CAVE_0105,
 PAD_CAVE_0106,
 PAD_CAVE_0107,
 PAD_CAVE_0108,
 PAD_CAVE_0109,
 PAD_CAVE_010A,
 PAD_CAVE_010B,
 PAD_CAVE_010C,
 PAD_CAVE_010D,
 PAD_CAVE_010E,
 PAD_CAVE_010F,
 PAD_CAVE_0110,
 PAD_CAVE_0111,
 PAD_CAVE_0112,
 PAD_CAVE_0113,
 PAD_CAVE_0114,
 PAD_CAVE_0115,
 PAD_CAVE_0116,
 PAD_CAVE_0117,
 PAD_CAVE_0118,
 PAD_CAVE_0119,
 PAD_CAVE_011A,
 PAD_CAVE_011B,
 PAD_CAVE_011C,
 PAD_CAVE_011D,
 PAD_CAVE_011E,
 PAD_CAVE_011F,
 PAD_CAVE_0120,
 PAD_CAVE_0121,
 PAD_CAVE_0122,
 PAD_CAVE_0123,
 PAD_CAVE_0124,
 PAD_CAVE_0125,
 PAD_CAVE_0126,
 PAD_CAVE_0127,
 PAD_CAVE_0128,
 PAD_CAVE_0129,
 PAD_CAVE_012A,
 PAD_CAVE_012B,
 PAD_CAVE_012C,
 PAD_CAVE_012D,
 PAD_CAVE_012E,
 PAD_CAVE_012F,
 PAD_CAVE_0130,
 PAD_CAVE_0131,
 PAD_CAVE_0132,
 PAD_CAVE_0133,
 PAD_CAVE_0134,
 PAD_CAVE_0135,
 PAD_CAVE_0136,
 PAD_CAVE_0137,
 PAD_CAVE_0138,
 PAD_CAVE_0139,
 PAD_CAVE_013A,
 PAD_CAVE_013B,
 PAD_CAVE_013C,
 PAD_CAVE_013D,
 PAD_CAVE_013E,
 PAD_CAVE_013F,
 PAD_CAVE_0140,
 PAD_CAVE_0141,
 PAD_CAVE_0142,
 PAD_CAVE_0143,
 PAD_CAVE_0144,
 PAD_CAVE_0145,
 PAD_CAVE_0146,
 PAD_CAVE_0147,
 PAD_CAVE_0148,
 PAD_CAVE_0149,
 PAD_CAVE_014A,
 PAD_CAVE_014B,
 PAD_CAVE_014C,
 PAD_CAVE_014D,
 PAD_CAVE_014E,
 PAD_CAVE_014F,
 PAD_CAVE_0150,
 PAD_CAVE_0151,
 PAD_CAVE_0152,
 PAD_CAVE_0153,
 PAD_CAVE_0154,
 PAD_CAVE_0155,
 PAD_CAVE_0156,
 PAD_CAVE_0157,
 PAD_CAVE_0158,
 PAD_CAVE_0159,
 PAD_CAVE_015A,
 PAD_CAVE_015B,
 PAD_CAVE_015C,
 PAD_CAVE_015D,
 PAD_CAVE_015E,
 PAD_CAVE_015F,
 PAD_CAVE_0160,
 PAD_CAVE_0161,
 PAD_CAVE_0162,
 PAD_CAVE_0163,
 PAD_CAVE_0164,
 PAD_CAVE_0165,
 PAD_CAVE_0166,
 PAD_CAVE_0167,
 PAD_CAVE_0168,
 PAD_CAVE_0169,
 PAD_CAVE_016A,
 PAD_CAVE_016B,
 PAD_CAVE_016C,
 PAD_CAVE_016D,
 PAD_CAVE_016E,
 PAD_CAVE_016F,
 PAD_CAVE_0170,
 PAD_CAVE_0171,
 PAD_CAVE_0172,
 PAD_CAVE_0173,
 PAD_CAVE_0174,
 PAD_CAVE_0175,
 PAD_CAVE_0176,
 PAD_CAVE_0177,
 PAD_CAVE_0178,
 PAD_CAVE_0179,
 PAD_CAVE_017A,
 PAD_CAVE_017B,
 PAD_CAVE_017C,
 PAD_CAVE_017D,
 PAD_CAVE_017E,
 PAD_CAVE_017F,
 PAD_CAVE_0180,
 PAD_CAVE_0181,
 PAD_CAVE_0182,
 PAD_CAVE_0183,
 PAD_CAVE_0184,
 PAD_CAVE_0185,
 PAD_CAVE_0186,
 PAD_CAVE_0187,
 PAD_CAVE_0188,
 PAD_CAVE_0189,
 PAD_CAVE_018A,
 PAD_CAVE_018B,
 PAD_CAVE_018C,
 PAD_CAVE_018D,
 PAD_CAVE_018E,
 PAD_CAVE_018F,
 PAD_CAVE_0190,
 PAD_CAVE_0191,
 PAD_CAVE_0192,
 PAD_CAVE_0193,
 PAD_CAVE_0194,
 PAD_CAVE_0195,
 PAD_CAVE_0196,
 PAD_CAVE_0197,
 PAD_CAVE_0198,
 PAD_CAVE_0199,
 PAD_CAVE_019A,
 PAD_CAVE_019B,
 PAD_CAVE_019C,
 PAD_CAVE_019D,
 PAD_CAVE_019E,
 PAD_CAVE_019F,
 PAD_CAVE_01A0,
 PAD_CAVE_01A1,
 PAD_CAVE_01A2,
 PAD_CAVE_01A3,
 PAD_CAVE_01A4,
 PAD_CAVE_01A5,
 PAD_CAVE_01A6,
 PAD_CAVE_01A7,
 PAD_CAVE_01A8,
 PAD_CAVE_01A9,
 PAD_CAVE_01AA,
 PAD_CAVE_01AB,
 PAD_CAVE_01AC,
 PAD_CAVE_01AD,
 PAD_CAVE_01AE,
 PAD_CAVE_01AF,
 PAD_CAVE_01B0,
 PAD_CAVE_01B1,
 PAD_CAVE_01B2,
 PAD_CAVE_01B3,
 PAD_CAVE_01B4,
 PAD_CAVE_01B5,
 PAD_CAVE_01B6,
 PAD_CAVE_01B7,
 PAD_CAVE_01B8,
 PAD_CAVE_01B9,
 PAD_CAVE_01BA,
 PAD_CAVE_01BB,
 PAD_CAVE_01BC,
 PAD_CAVE_01BD,
 PAD_CAVE_01BE,
 PAD_CAVE_01BF,
 PAD_CAVE_01C0,
 PAD_CAVE_01C1,
 PAD_CAVE_01C2,
 PAD_CAVE_01C3,
 PAD_CAVE_01C4,
 PAD_CAVE_01C5,
 PAD_CAVE_01C6,
 PAD_CAVE_01C7,
 PAD_CAVE_01C8,
 PAD_CAVE_01C9,
 PAD_CAVE_01CA,
 PAD_CAVE_01CB,
 PAD_CAVE_01CC,
 PAD_CAVE_01CD,
 PAD_CAVE_01CE,
 PAD_CAVE_01CF,
 PAD_CAVE_01D0,
 PAD_CAVE_01D1,
 PAD_CAVE_01D2,
 PAD_CAVE_01D3,
 PAD_CAVE_01D4,
 PAD_CAVE_01D5,
 PAD_CAVE_01D6,
 PAD_CAVE_01D7,
 PAD_CAVE_01D8,
 PAD_CAVE_01D9,
 PAD_CAVE_01DA,
 PAD_CAVE_01DB,
 PAD_CAVE_01DC,
 PAD_CAVE_01DD,
 PAD_CAVE_01DE,
 PAD_CAVE_01DF,
 PAD_CAVE_01E0,
 PAD_CAVE_01E1,
 PAD_CAVE_01E2,
 PAD_CAVE_01E3,
 PAD_CAVE_01E4,
 PAD_CAVE_01E5,
 PAD_CAVE_01E6,
 PAD_CAVE_01E7,
 PAD_CAVE_01E8,
 PAD_CAVE_01E9,
 PAD_CAVE_01EA,
 PAD_CAVE_01EB,
 PAD_CAVE_01EC,
 PAD_CAVE_01ED,
 PAD_CAVE_01EE,
 PAD_CAVE_01EF,
 PAD_CAVE_01F0,
 PAD_CAVE_01F1,
 PAD_CAVE_01F2,
 PAD_CAVE_01F3,
 PAD_CAVE_01F4,
 PAD_CAVE_01F5,
 PAD_CAVE_01F6,
 PAD_CAVE_01F7,
 PAD_CAVE_01F8,
 PAD_CAVE_01F9,
 PAD_CAVE_END
};
enum pad_arec {
 PAD_AREC_0000,
 PAD_AREC_0001,
 PAD_AREC_0002,
 PAD_AREC_0003,
 PAD_AREC_0004,
 PAD_AREC_0005,
 PAD_AREC_0006,
 PAD_AREC_0007,
 PAD_AREC_0008,
 PAD_AREC_0009,
 PAD_AREC_000A,
 PAD_AREC_000B,
 PAD_AREC_000C,
 PAD_AREC_000D,
 PAD_AREC_000E,
 PAD_AREC_000F,
 PAD_AREC_0010,
 PAD_AREC_0011,
 PAD_AREC_0012,
 PAD_AREC_0013,
 PAD_AREC_0014,
 PAD_AREC_0015,
 PAD_AREC_0016,
 PAD_AREC_0017,
 PAD_AREC_0018,
 PAD_AREC_0019,
 PAD_AREC_001A,
 PAD_AREC_001B,
 PAD_AREC_001C,
 PAD_AREC_001D,
 PAD_AREC_001E,
 PAD_AREC_001F,
 PAD_AREC_0020,
 PAD_AREC_0021,
 PAD_AREC_0022,
 PAD_AREC_0023,
 PAD_AREC_0024,
 PAD_AREC_0025,
 PAD_AREC_0026,
 PAD_AREC_0027,
 PAD_AREC_0028,
 PAD_AREC_0029,
 PAD_AREC_002A,
 PAD_AREC_002B,
 PAD_AREC_002C,
 PAD_AREC_002D,
 PAD_AREC_002E,
 PAD_AREC_002F,
 PAD_AREC_0030,
 PAD_AREC_0031,
 PAD_AREC_0032,
 PAD_AREC_0033,
 PAD_AREC_0034,
 PAD_AREC_0035,
 PAD_AREC_0036,
 PAD_AREC_0037,
 PAD_AREC_0038,
 PAD_AREC_0039,
 PAD_AREC_003A,
 PAD_AREC_003B,
 PAD_AREC_003C,
 PAD_AREC_003D,
 PAD_AREC_003E,
 PAD_AREC_003F,
 PAD_AREC_0040,
 PAD_AREC_0041,
 PAD_AREC_0042,
 PAD_AREC_0043,
 PAD_AREC_0044,
 PAD_AREC_0045,
 PAD_AREC_0046,
 PAD_AREC_0047,
 PAD_AREC_0048,
 PAD_AREC_0049,
 PAD_AREC_004A,
 PAD_AREC_004B,
 PAD_AREC_004C,
 PAD_AREC_004D,
 PAD_AREC_004E,
 PAD_AREC_004F,
 PAD_AREC_0050,
 PAD_AREC_0051,
 PAD_AREC_0052,
 PAD_AREC_0053,
 PAD_AREC_0054,
 PAD_AREC_0055,
 PAD_AREC_0056,
 PAD_AREC_0057,
 PAD_AREC_0058,
 PAD_AREC_0059,
 PAD_AREC_005A,
 PAD_AREC_005B,
 PAD_AREC_005C,
 PAD_AREC_005D,
 PAD_AREC_005E,
 PAD_AREC_005F,
 PAD_AREC_0060,
 PAD_AREC_0061,
 PAD_AREC_0062,
 PAD_AREC_0063,
 PAD_AREC_0064,
 PAD_AREC_0065,
 PAD_AREC_0066,
 PAD_AREC_0067,
 PAD_AREC_0068,
 PAD_AREC_0069,
 PAD_AREC_006A,
 PAD_AREC_006B,
 PAD_AREC_006C,
 PAD_AREC_006D,
 PAD_AREC_006E,
 PAD_AREC_006F,
 PAD_AREC_0070,
 PAD_AREC_0071,
 PAD_AREC_0072,
 PAD_AREC_0073,
 PAD_AREC_0074,
 PAD_AREC_0075,
 PAD_AREC_0076,
 PAD_AREC_0077,
 PAD_AREC_0078,
 PAD_AREC_0079,
 PAD_AREC_007A,
 PAD_AREC_007B,
 PAD_AREC_007C,
 PAD_AREC_007D,
 PAD_AREC_007E,
 PAD_AREC_007F,
 PAD_AREC_0080,
 PAD_AREC_0081,
 PAD_AREC_0082,
 PAD_AREC_0083,
 PAD_AREC_0084,
 PAD_AREC_0085,
 PAD_AREC_0086,
 PAD_AREC_0087,
 PAD_AREC_0088,
 PAD_AREC_0089,
 PAD_AREC_008A,
 PAD_AREC_008B,
 PAD_AREC_008C,
 PAD_AREC_008D,
 PAD_AREC_008E,
 PAD_AREC_008F,
 PAD_AREC_0090,
 PAD_AREC_0091,
 PAD_AREC_0092,
 PAD_AREC_0093,
 PAD_AREC_0094,
 PAD_AREC_0095,
 PAD_AREC_0096,
 PAD_AREC_0097,
 PAD_AREC_0098,
 PAD_AREC_0099,
 PAD_AREC_009A,
 PAD_AREC_009B,
 PAD_AREC_009C,
 PAD_AREC_009D,
 PAD_AREC_009E,
 PAD_AREC_009F,
 PAD_AREC_00A0,
 PAD_AREC_00A1,
 PAD_AREC_00A2,
 PAD_AREC_00A3,
 PAD_AREC_00A4,
 PAD_AREC_00A5,
 PAD_AREC_00A6,
 PAD_AREC_00A7,
 PAD_AREC_00A8,
 PAD_AREC_00A9,
 PAD_AREC_00AA,
 PAD_AREC_00AB,
 PAD_AREC_00AC,
 PAD_AREC_00AD,
 PAD_AREC_00AE,
 PAD_AREC_00AF,
 PAD_AREC_00B0,
 PAD_AREC_00B1,
 PAD_AREC_00B2,
 PAD_AREC_00B3,
 PAD_AREC_00B4,
 PAD_AREC_00B5,
 PAD_AREC_00B6,
 PAD_AREC_00B7,
 PAD_AREC_00B8,
 PAD_AREC_00B9,
 PAD_AREC_00BA,
 PAD_AREC_00BB,
 PAD_AREC_00BC,
 PAD_AREC_00BD,
 PAD_AREC_00BE,
 PAD_AREC_00BF,
 PAD_AREC_00C0,
 PAD_AREC_00C1,
 PAD_AREC_00C2,
 PAD_AREC_00C3,
 PAD_AREC_00C4,
 PAD_AREC_00C5,
 PAD_AREC_00C6,
 PAD_AREC_00C7,
 PAD_AREC_00C8,
 PAD_AREC_00C9,
 PAD_AREC_00CA,
 PAD_AREC_00CB,
 PAD_AREC_00CC,
 PAD_AREC_00CD,
 PAD_AREC_00CE,
 PAD_AREC_00CF,
 PAD_AREC_00D0,
 PAD_AREC_00D1,
 PAD_AREC_00D2,
 PAD_AREC_00D3,
 PAD_AREC_00D4,
 PAD_AREC_00D5,
 PAD_AREC_00D6,
 PAD_AREC_00D7,
 PAD_AREC_00D8,
 PAD_AREC_00D9,
 PAD_AREC_00DA,
 PAD_AREC_00DB,
 PAD_AREC_00DC,
 PAD_AREC_00DD,
 PAD_AREC_00DE,
 PAD_AREC_00DF,
 PAD_AREC_00E0,
 PAD_AREC_00E1,
 PAD_AREC_00E2,
 PAD_AREC_00E3,
 PAD_AREC_00E4,
 PAD_AREC_00E5,
 PAD_AREC_00E6,
 PAD_AREC_00E7,
 PAD_AREC_00E8,
 PAD_AREC_00E9,
 PAD_AREC_00EA,
 PAD_AREC_00EB,
 PAD_AREC_00EC,
 PAD_AREC_00ED,
 PAD_AREC_00EE,
 PAD_AREC_END
};
enum pad_crad {
 PAD_CRAD_0000,
 PAD_CRAD_0001,
 PAD_CRAD_0002,
 PAD_CRAD_0003,
 PAD_CRAD_0004,
 PAD_CRAD_0005,
 PAD_CRAD_0006,
 PAD_CRAD_0007,
 PAD_CRAD_0008,
 PAD_CRAD_0009,
 PAD_CRAD_000A,
 PAD_CRAD_000B,
 PAD_CRAD_000C,
 PAD_CRAD_000D,
 PAD_CRAD_000E,
 PAD_CRAD_000F,
 PAD_CRAD_0010,
 PAD_CRAD_0011,
 PAD_CRAD_0012,
 PAD_CRAD_0013,
 PAD_CRAD_0014,
 PAD_CRAD_0015,
 PAD_CRAD_0016,
 PAD_CRAD_0017,
 PAD_CRAD_0018,
 PAD_CRAD_0019,
 PAD_CRAD_001A,
 PAD_CRAD_001B,
 PAD_CRAD_001C,
 PAD_CRAD_001D,
 PAD_CRAD_001E,
 PAD_CRAD_001F,
 PAD_CRAD_0020,
 PAD_CRAD_0021,
 PAD_CRAD_0022,
 PAD_CRAD_0023,
 PAD_CRAD_0024,
 PAD_CRAD_0025,
 PAD_CRAD_0026,
 PAD_CRAD_0027,
 PAD_CRAD_0028,
 PAD_CRAD_0029,
 PAD_CRAD_002A,
 PAD_CRAD_002B,
 PAD_CRAD_002C,
 PAD_CRAD_002D,
 PAD_CRAD_002E,
 PAD_CRAD_002F,
 PAD_CRAD_0030,
 PAD_CRAD_0031,
 PAD_CRAD_0032,
 PAD_CRAD_0033,
 PAD_CRAD_0034,
 PAD_CRAD_0035,
 PAD_CRAD_0036,
 PAD_CRAD_0037,
 PAD_CRAD_0038,
 PAD_CRAD_0039,
 PAD_CRAD_003A,
 PAD_CRAD_003B,
 PAD_CRAD_003C,
 PAD_CRAD_003D,
 PAD_CRAD_003E,
 PAD_CRAD_003F,
 PAD_CRAD_0040,
 PAD_CRAD_0041,
 PAD_CRAD_0042,
 PAD_CRAD_0043,
 PAD_CRAD_0044,
 PAD_CRAD_0045,
 PAD_CRAD_0046,
 PAD_CRAD_0047,
 PAD_CRAD_0048,
 PAD_CRAD_0049,
 PAD_CRAD_004A,
 PAD_CRAD_004B,
 PAD_CRAD_004C,
 PAD_CRAD_004D,
 PAD_CRAD_004E,
 PAD_CRAD_004F,
 PAD_CRAD_0050,
 PAD_CRAD_0051,
 PAD_CRAD_0052,
 PAD_CRAD_0053,
 PAD_CRAD_0054,
 PAD_CRAD_0055,
 PAD_CRAD_0056,
 PAD_CRAD_0057,
 PAD_CRAD_0058,
 PAD_CRAD_0059,
 PAD_CRAD_005A,
 PAD_CRAD_005B,
 PAD_CRAD_005C,
 PAD_CRAD_005D,
 PAD_CRAD_005E,
 PAD_CRAD_005F,
 PAD_CRAD_0060,
 PAD_CRAD_0061,
 PAD_CRAD_0062,
 PAD_CRAD_0063,
 PAD_CRAD_0064,
 PAD_CRAD_0065,
 PAD_CRAD_0066,
 PAD_CRAD_0067,
 PAD_CRAD_0068,
 PAD_CRAD_0069,
 PAD_CRAD_006A,
 PAD_CRAD_006B,
 PAD_CRAD_006C,
 PAD_CRAD_006D,
 PAD_CRAD_006E,
 PAD_CRAD_006F,
 PAD_CRAD_0070,
 PAD_CRAD_0071,
 PAD_CRAD_0072,
 PAD_CRAD_0073,
 PAD_CRAD_0074,
 PAD_CRAD_0075,
 PAD_CRAD_0076,
 PAD_CRAD_0077,
 PAD_CRAD_0078,
 PAD_CRAD_0079,
 PAD_CRAD_007A,
 PAD_CRAD_007B,
 PAD_CRAD_007C,
 PAD_CRAD_007D,
 PAD_CRAD_007E,
 PAD_CRAD_007F,
 PAD_CRAD_0080,
 PAD_CRAD_0081,
 PAD_CRAD_0082,
 PAD_CRAD_0083,
 PAD_CRAD_0084,
 PAD_CRAD_0085,
 PAD_CRAD_0086,
 PAD_CRAD_0087,
 PAD_CRAD_0088,
 PAD_CRAD_0089,
 PAD_CRAD_008A,
 PAD_CRAD_008B,
 PAD_CRAD_008C,
 PAD_CRAD_008D,
 PAD_CRAD_008E,
 PAD_CRAD_008F,
 PAD_CRAD_0090,
 PAD_CRAD_0091,
 PAD_CRAD_0092,
 PAD_CRAD_0093,
 PAD_CRAD_0094,
 PAD_CRAD_0095,
 PAD_CRAD_0096,
 PAD_CRAD_0097,
 PAD_CRAD_0098,
 PAD_CRAD_0099,
 PAD_CRAD_009A,
 PAD_CRAD_009B,
 PAD_CRAD_009C,
 PAD_CRAD_009D,
 PAD_CRAD_009E,
 PAD_CRAD_009F,
 PAD_CRAD_00A0,
 PAD_CRAD_00A1,
 PAD_CRAD_00A2,
 PAD_CRAD_00A3,
 PAD_CRAD_00A4,
 PAD_CRAD_00A5,
 PAD_CRAD_00A6,
 PAD_CRAD_00A7,
 PAD_CRAD_00A8,
 PAD_CRAD_00A9,
 PAD_CRAD_00AA,
 PAD_CRAD_00AB,
 PAD_CRAD_00AC,
 PAD_CRAD_00AD,
 PAD_CRAD_00AE,
 PAD_CRAD_00AF,
 PAD_CRAD_00B0,
 PAD_CRAD_00B1,
 PAD_CRAD_00B2,
 PAD_CRAD_00B3,
 PAD_CRAD_00B4,
 PAD_CRAD_00B5,
 PAD_CRAD_00B6,
 PAD_CRAD_00B7,
 PAD_CRAD_00B8,
 PAD_CRAD_00B9,
 PAD_CRAD_00BA,
 PAD_CRAD_00BB,
 PAD_CRAD_00BC,
 PAD_CRAD_00BD,
 PAD_CRAD_00BE,
 PAD_CRAD_00BF,
 PAD_CRAD_00C0,
 PAD_CRAD_00C1,
 PAD_CRAD_00C2,
 PAD_CRAD_00C3,
 PAD_CRAD_00C4,
 PAD_CRAD_00C5,
 PAD_CRAD_00C6,
 PAD_CRAD_00C7,
 PAD_CRAD_00C8,
 PAD_CRAD_00C9,
 PAD_CRAD_00CA,
 PAD_CRAD_00CB,
 PAD_CRAD_00CC,
 PAD_CRAD_00CD,
 PAD_CRAD_00CE,
 PAD_CRAD_00CF,
 PAD_CRAD_00D0,
 PAD_CRAD_00D1,
 PAD_CRAD_00D2,
 PAD_CRAD_00D3,
 PAD_CRAD_00D4,
 PAD_CRAD_00D5,
 PAD_CRAD_00D6,
 PAD_CRAD_00D7,
 PAD_CRAD_00D8,
 PAD_CRAD_00D9,
 PAD_CRAD_00DA,
 PAD_CRAD_00DB,
 PAD_CRAD_00DC,
 PAD_CRAD_00DD,
 PAD_CRAD_00DE,
 PAD_CRAD_00DF,
 PAD_CRAD_00E0,
 PAD_CRAD_00E1,
 PAD_CRAD_00E2,
 PAD_CRAD_00E3,
 PAD_CRAD_00E4,
 PAD_CRAD_00E5,
 PAD_CRAD_00E6,
 PAD_CRAD_00E7,
 PAD_CRAD_00E8,
 PAD_CRAD_00E9,
 PAD_CRAD_00EA,
 PAD_CRAD_00EB,
 PAD_CRAD_00EC,
 PAD_CRAD_00ED,
 PAD_CRAD_00EE,
 PAD_CRAD_00EF,
 PAD_CRAD_00F0,
 PAD_CRAD_00F1,
 PAD_CRAD_00F2,
 PAD_CRAD_00F3,
 PAD_CRAD_00F4,
 PAD_CRAD_00F5,
 PAD_CRAD_00F6,
 PAD_CRAD_00F7,
 PAD_CRAD_00F8,
 PAD_CRAD_00F9,
 PAD_CRAD_00FA,
 PAD_CRAD_00FB,
 PAD_CRAD_00FC,
 PAD_CRAD_00FD,
 PAD_CRAD_00FE,
 PAD_CRAD_00FF,
 PAD_CRAD_0100,
 PAD_CRAD_0101,
 PAD_CRAD_0102,
 PAD_CRAD_0103,
 PAD_CRAD_0104,
 PAD_CRAD_END
};
enum pad_cryp {
 PAD_CRYP_0000,
 PAD_CRYP_0001,
 PAD_CRYP_0002,
 PAD_CRYP_0003,
 PAD_CRYP_0004,
 PAD_CRYP_0005,
 PAD_CRYP_0006,
 PAD_CRYP_0007,
 PAD_CRYP_0008,
 PAD_CRYP_0009,
 PAD_CRYP_000A,
 PAD_CRYP_000B,
 PAD_CRYP_000C,
 PAD_CRYP_000D,
 PAD_CRYP_000E,
 PAD_CRYP_000F,
 PAD_CRYP_0010,
 PAD_CRYP_0011,
 PAD_CRYP_0012,
 PAD_CRYP_0013,
 PAD_CRYP_0014,
 PAD_CRYP_0015,
 PAD_CRYP_0016,
 PAD_CRYP_0017,
 PAD_CRYP_0018,
 PAD_CRYP_0019,
 PAD_CRYP_001A,
 PAD_CRYP_001B,
 PAD_CRYP_001C,
 PAD_CRYP_001D,
 PAD_CRYP_001E,
 PAD_CRYP_001F,
 PAD_CRYP_0020,
 PAD_CRYP_0021,
 PAD_CRYP_0022,
 PAD_CRYP_0023,
 PAD_CRYP_0024,
 PAD_CRYP_0025,
 PAD_CRYP_0026,
 PAD_CRYP_0027,
 PAD_CRYP_0028,
 PAD_CRYP_0029,
 PAD_CRYP_002A,
 PAD_CRYP_002B,
 PAD_CRYP_002C,
 PAD_CRYP_002D,
 PAD_CRYP_002E,
 PAD_CRYP_002F,
 PAD_CRYP_0030,
 PAD_CRYP_0031,
 PAD_CRYP_0032,
 PAD_CRYP_0033,
 PAD_CRYP_0034,
 PAD_CRYP_0035,
 PAD_CRYP_0036,
 PAD_CRYP_0037,
 PAD_CRYP_0038,
 PAD_CRYP_0039,
 PAD_CRYP_003A,
 PAD_CRYP_003B,
 PAD_CRYP_003C,
 PAD_CRYP_003D,
 PAD_CRYP_003E,
 PAD_CRYP_003F,
 PAD_CRYP_0040,
 PAD_CRYP_0041,
 PAD_CRYP_0042,
 PAD_CRYP_0043,
 PAD_CRYP_0044,
 PAD_CRYP_0045,
 PAD_CRYP_0046,
 PAD_CRYP_0047,
 PAD_CRYP_0048,
 PAD_CRYP_0049,
 PAD_CRYP_004A,
 PAD_CRYP_004B,
 PAD_CRYP_004C,
 PAD_CRYP_004D,
 PAD_CRYP_004E,
 PAD_CRYP_004F,
 PAD_CRYP_0050,
 PAD_CRYP_0051,
 PAD_CRYP_0052,
 PAD_CRYP_0053,
 PAD_CRYP_0054,
 PAD_CRYP_0055,
 PAD_CRYP_0056,
 PAD_CRYP_0057,
 PAD_CRYP_0058,
 PAD_CRYP_0059,
 PAD_CRYP_005A,
 PAD_CRYP_005B,
 PAD_CRYP_005C,
 PAD_CRYP_005D,
 PAD_CRYP_005E,
 PAD_CRYP_005F,
 PAD_CRYP_0060,
 PAD_CRYP_0061,
 PAD_CRYP_0062,
 PAD_CRYP_0063,
 PAD_CRYP_0064,
 PAD_CRYP_0065,
 PAD_CRYP_0066,
 PAD_CRYP_0067,
 PAD_CRYP_0068,
 PAD_CRYP_0069,
 PAD_CRYP_006A,
 PAD_CRYP_006B,
 PAD_CRYP_006C,
 PAD_CRYP_006D,
 PAD_CRYP_006E,
 PAD_CRYP_006F,
 PAD_CRYP_0070,
 PAD_CRYP_0071,
 PAD_CRYP_0072,
 PAD_CRYP_0073,
 PAD_CRYP_0074,
 PAD_CRYP_0075,
 PAD_CRYP_0076,
 PAD_CRYP_0077,
 PAD_CRYP_0078,
 PAD_CRYP_0079,
 PAD_CRYP_007A,
 PAD_CRYP_007B,
 PAD_CRYP_007C,
 PAD_CRYP_007D,
 PAD_CRYP_007E,
 PAD_CRYP_007F,
 PAD_CRYP_0080,
 PAD_CRYP_0081,
 PAD_CRYP_0082,
 PAD_CRYP_0083,
 PAD_CRYP_0084,
 PAD_CRYP_0085,
 PAD_CRYP_0086,
 PAD_CRYP_0087,
 PAD_CRYP_0088,
 PAD_CRYP_0089,
 PAD_CRYP_008A,
 PAD_CRYP_008B,
 PAD_CRYP_008C,
 PAD_CRYP_008D,
 PAD_CRYP_008E,
 PAD_CRYP_008F,
 PAD_CRYP_0090,
 PAD_CRYP_0091,
 PAD_CRYP_0092,
 PAD_CRYP_0093,
 PAD_CRYP_0094,
 PAD_CRYP_0095,
 PAD_CRYP_0096,
 PAD_CRYP_0097,
 PAD_CRYP_0098,
 PAD_CRYP_0099,
 PAD_CRYP_009A,
 PAD_CRYP_009B,
 PAD_CRYP_009C,
 PAD_CRYP_009D,
 PAD_CRYP_009E,
 PAD_CRYP_009F,
 PAD_CRYP_00A0,
 PAD_CRYP_00A1,
 PAD_CRYP_00A2,
 PAD_CRYP_00A3,
 PAD_CRYP_00A4,
 PAD_CRYP_00A5,
 PAD_CRYP_00A6,
 PAD_CRYP_00A7,
 PAD_CRYP_00A8,
 PAD_CRYP_00A9,
 PAD_CRYP_00AA,
 PAD_CRYP_00AB,
 PAD_CRYP_00AC,
 PAD_CRYP_00AD,
 PAD_CRYP_00AE,
 PAD_CRYP_00AF,
 PAD_CRYP_00B0,
 PAD_CRYP_00B1,
 PAD_CRYP_00B2,
 PAD_CRYP_00B3,
 PAD_CRYP_00B4,
 PAD_CRYP_00B5,
 PAD_CRYP_00B6,
 PAD_CRYP_00B7,
 PAD_CRYP_00B8,
 PAD_CRYP_00B9,
 PAD_CRYP_00BA,
 PAD_CRYP_00BB,
 PAD_CRYP_00BC,
 PAD_CRYP_00BD,
 PAD_CRYP_00BE,
 PAD_CRYP_00BF,
 PAD_CRYP_00C0,
 PAD_CRYP_00C1,
 PAD_CRYP_00C2,
 PAD_CRYP_00C3,
 PAD_CRYP_00C4,
 PAD_CRYP_00C5,
 PAD_CRYP_00C6,
 PAD_CRYP_00C7,
 PAD_CRYP_00C8,
 PAD_CRYP_00C9,
 PAD_CRYP_00CA,
 PAD_CRYP_00CB,
 PAD_CRYP_00CC,
 PAD_CRYP_00CD,
 PAD_CRYP_00CE,
 PAD_CRYP_00CF,
 PAD_CRYP_00D0,
 PAD_CRYP_END
};
enum pad_dam {
 PAD_DAM_0000,
 PAD_DAM_0001,
 PAD_DAM_0002,
 PAD_DAM_0003,
 PAD_DAM_0004,
 PAD_DAM_0005,
 PAD_DAM_0006,
 PAD_DAM_0007,
 PAD_DAM_0008,
 PAD_DAM_0009,
 PAD_DAM_000A,
 PAD_DAM_000B,
 PAD_DAM_000C,
 PAD_DAM_000D,
 PAD_DAM_000E,
 PAD_DAM_000F,
 PAD_DAM_0010,
 PAD_DAM_0011,
 PAD_DAM_0012,
 PAD_DAM_0013,
 PAD_DAM_0014,
 PAD_DAM_0015,
 PAD_DAM_0016,
 PAD_DAM_0017,
 PAD_DAM_0018,
 PAD_DAM_0019,
 PAD_DAM_001A,
 PAD_DAM_001B,
 PAD_DAM_001C,
 PAD_DAM_001D,
 PAD_DAM_001E,
 PAD_DAM_001F,
 PAD_DAM_0020,
 PAD_DAM_0021,
 PAD_DAM_0022,
 PAD_DAM_0023,
 PAD_DAM_0024,
 PAD_DAM_0025,
 PAD_DAM_0026,
 PAD_DAM_0027,
 PAD_DAM_0028,
 PAD_DAM_0029,
 PAD_DAM_002A,
 PAD_DAM_002B,
 PAD_DAM_002C,
 PAD_DAM_002D,
 PAD_DAM_002E,
 PAD_DAM_002F,
 PAD_DAM_0030,
 PAD_DAM_0031,
 PAD_DAM_0032,
 PAD_DAM_0033,
 PAD_DAM_0034,
 PAD_DAM_0035,
 PAD_DAM_0036,
 PAD_DAM_0037,
 PAD_DAM_0038,
 PAD_DAM_0039,
 PAD_DAM_003A,
 PAD_DAM_003B,
 PAD_DAM_003C,
 PAD_DAM_003D,
 PAD_DAM_003E,
 PAD_DAM_003F,
 PAD_DAM_0040,
 PAD_DAM_0041,
 PAD_DAM_0042,
 PAD_DAM_0043,
 PAD_DAM_0044,
 PAD_DAM_0045,
 PAD_DAM_0046,
 PAD_DAM_0047,
 PAD_DAM_0048,
 PAD_DAM_0049,
 PAD_DAM_004A,
 PAD_DAM_004B,
 PAD_DAM_004C,
 PAD_DAM_004D,
 PAD_DAM_004E,
 PAD_DAM_004F,
 PAD_DAM_0050,
 PAD_DAM_0051,
 PAD_DAM_0052,
 PAD_DAM_0053,
 PAD_DAM_0054,
 PAD_DAM_0055,
 PAD_DAM_0056,
 PAD_DAM_0057,
 PAD_DAM_0058,
 PAD_DAM_0059,
 PAD_DAM_005A,
 PAD_DAM_005B,
 PAD_DAM_005C,
 PAD_DAM_005D,
 PAD_DAM_005E,
 PAD_DAM_005F,
 PAD_DAM_0060,
 PAD_DAM_0061,
 PAD_DAM_0062,
 PAD_DAM_0063,
 PAD_DAM_0064,
 PAD_DAM_0065,
 PAD_DAM_0066,
 PAD_DAM_0067,
 PAD_DAM_0068,
 PAD_DAM_0069,
 PAD_DAM_006A,
 PAD_DAM_006B,
 PAD_DAM_006C,
 PAD_DAM_006D,
 PAD_DAM_006E,
 PAD_DAM_006F,
 PAD_DAM_0070,
 PAD_DAM_0071,
 PAD_DAM_0072,
 PAD_DAM_0073,
 PAD_DAM_0074,
 PAD_DAM_0075,
 PAD_DAM_0076,
 PAD_DAM_0077,
 PAD_DAM_0078,
 PAD_DAM_0079,
 PAD_DAM_007A,
 PAD_DAM_007B,
 PAD_DAM_007C,
 PAD_DAM_007D,
 PAD_DAM_007E,
 PAD_DAM_007F,
 PAD_DAM_0080,
 PAD_DAM_0081,
 PAD_DAM_0082,
 PAD_DAM_0083,
 PAD_DAM_0084,
 PAD_DAM_0085,
 PAD_DAM_0086,
 PAD_DAM_0087,
 PAD_DAM_0088,
 PAD_DAM_0089,
 PAD_DAM_008A,
 PAD_DAM_008B,
 PAD_DAM_008C,
 PAD_DAM_008D,
 PAD_DAM_008E,
 PAD_DAM_008F,
 PAD_DAM_0090,
 PAD_DAM_0091,
 PAD_DAM_0092,
 PAD_DAM_0093,
 PAD_DAM_0094,
 PAD_DAM_0095,
 PAD_DAM_0096,
 PAD_DAM_0097,
 PAD_DAM_0098,
 PAD_DAM_0099,
 PAD_DAM_009A,
 PAD_DAM_009B,
 PAD_DAM_009C,
 PAD_DAM_009D,
 PAD_DAM_009E,
 PAD_DAM_009F,
 PAD_DAM_00A0,
 PAD_DAM_00A1,
 PAD_DAM_00A2,
 PAD_DAM_00A3,
 PAD_DAM_00A4,
 PAD_DAM_00A5,
 PAD_DAM_00A6,
 PAD_DAM_00A7,
 PAD_DAM_00A8,
 PAD_DAM_00A9,
 PAD_DAM_00AA,
 PAD_DAM_00AB,
 PAD_DAM_00AC,
 PAD_DAM_00AD,
 PAD_DAM_00AE,
 PAD_DAM_00AF,
 PAD_DAM_00B0,
 PAD_DAM_00B1,
 PAD_DAM_00B2,
 PAD_DAM_00B3,
 PAD_DAM_00B4,
 PAD_DAM_00B5,
 PAD_DAM_00B6,
 PAD_DAM_00B7,
 PAD_DAM_00B8,
 PAD_DAM_00B9,
 PAD_DAM_00BA,
 PAD_DAM_00BB,
 PAD_DAM_00BC,
 PAD_DAM_00BD,
 PAD_DAM_00BE,
 PAD_DAM_00BF,
 PAD_DAM_00C0,
 PAD_DAM_00C1,
 PAD_DAM_00C2,
 PAD_DAM_00C3,
 PAD_DAM_00C4,
 PAD_DAM_00C5,
 PAD_DAM_00C6,
 PAD_DAM_00C7,
 PAD_DAM_00C8,
 PAD_DAM_00C9,
 PAD_DAM_00CA,
 PAD_DAM_00CB,
 PAD_DAM_00CC,
 PAD_DAM_00CD,
 PAD_DAM_00CE,
 PAD_DAM_00CF,
 PAD_DAM_00D0,
 PAD_DAM_00D1,
 PAD_DAM_00D2,
 PAD_DAM_00D3,
 PAD_DAM_00D4,
 PAD_DAM_00D5,
 PAD_DAM_00D6,
 PAD_DAM_00D7,
 PAD_DAM_00D8,
 PAD_DAM_00D9,
 PAD_DAM_00DA,
 PAD_DAM_00DB,
 PAD_DAM_00DC,
 PAD_DAM_00DD,
 PAD_DAM_00DE,
 PAD_DAM_00DF,
 PAD_DAM_00E0,
 PAD_DAM_00E1,
 PAD_DAM_00E2,
 PAD_DAM_00E3,
 PAD_DAM_00E4,
 PAD_DAM_00E5,
 PAD_DAM_00E6,
 PAD_DAM_00E7,
 PAD_DAM_00E8,
 PAD_DAM_00E9,
 PAD_DAM_00EA,
 PAD_DAM_00EB,
 PAD_DAM_00EC,
 PAD_DAM_00ED,
 PAD_DAM_00EE,
 PAD_DAM_00EF,
 PAD_DAM_00F0,
 PAD_DAM_00F1,
 PAD_DAM_00F2,
 PAD_DAM_00F3,
 PAD_DAM_00F4,
 PAD_DAM_00F5,
 PAD_DAM_00F6,
 PAD_DAM_00F7,
 PAD_DAM_00F8,
 PAD_DAM_00F9,
 PAD_DAM_00FA,
 PAD_DAM_00FB,
 PAD_DAM_00FC,
 PAD_DAM_00FD,
 PAD_DAM_00FE,
 PAD_DAM_00FF,
 PAD_DAM_0100,
 PAD_DAM_0101,
 PAD_DAM_0102,
 PAD_DAM_0103,
 PAD_DAM_0104,
 PAD_DAM_0105,
 PAD_DAM_0106,
 PAD_DAM_0107,
 PAD_DAM_0108,
 PAD_DAM_0109,
 PAD_DAM_010A,
 PAD_DAM_010B,
 PAD_DAM_010C,
 PAD_DAM_010D,
 PAD_DAM_010E,
 PAD_DAM_010F,
 PAD_DAM_0110,
 PAD_DAM_0111,
 PAD_DAM_0112,
 PAD_DAM_0113,
 PAD_DAM_0114,
 PAD_DAM_0115,
 PAD_DAM_0116,
 PAD_DAM_0117,
 PAD_DAM_0118,
 PAD_DAM_0119,
 PAD_DAM_011A,
 PAD_DAM_011B,
 PAD_DAM_011C,
 PAD_DAM_011D,
 PAD_DAM_011E,
 PAD_DAM_011F,
 PAD_DAM_0120,
 PAD_DAM_0121,
 PAD_DAM_0122,
 PAD_DAM_0123,
 PAD_DAM_0124,
 PAD_DAM_0125,
 PAD_DAM_0126,
 PAD_DAM_0127,
 PAD_DAM_0128,
 PAD_DAM_0129,
 PAD_DAM_012A,
 PAD_DAM_012B,
 PAD_DAM_012C,
 PAD_DAM_012D,
 PAD_DAM_012E,
 PAD_DAM_012F,
 PAD_DAM_0130,
 PAD_DAM_0131,
 PAD_DAM_0132,
 PAD_DAM_0133,
 PAD_DAM_0134,
 PAD_DAM_0135,
 PAD_DAM_0136,
 PAD_DAM_0137,
 PAD_DAM_0138,
 PAD_DAM_0139,
 PAD_DAM_013A,
 PAD_DAM_013B,
 PAD_DAM_013C,
 PAD_DAM_013D,
 PAD_DAM_013E,
 PAD_DAM_013F,
 PAD_DAM_0140,
 PAD_DAM_0141,
 PAD_DAM_0142,
 PAD_DAM_0143,
 PAD_DAM_0144,
 PAD_DAM_0145,
 PAD_DAM_0146,
 PAD_DAM_0147,
 PAD_DAM_0148,
 PAD_DAM_0149,
 PAD_DAM_014A,
 PAD_DAM_014B,
 PAD_DAM_014C,
 PAD_DAM_014D,
 PAD_DAM_014E,
 PAD_DAM_014F,
 PAD_DAM_0150,
 PAD_DAM_0151,
 PAD_DAM_0152,
 PAD_DAM_0153,
 PAD_DAM_0154,
 PAD_DAM_0155,
 PAD_DAM_0156,
 PAD_DAM_0157,
 PAD_DAM_0158,
 PAD_DAM_0159,
 PAD_DAM_015A,
 PAD_DAM_015B,
 PAD_DAM_015C,
 PAD_DAM_015D,
 PAD_DAM_015E,
 PAD_DAM_015F,
 PAD_DAM_0160,
 PAD_DAM_0161,
 PAD_DAM_0162,
 PAD_DAM_0163,
 PAD_DAM_0164,
 PAD_DAM_0165,
 PAD_DAM_0166,
 PAD_DAM_0167,
 PAD_DAM_0168,
 PAD_DAM_0169,
 PAD_DAM_016A,
 PAD_DAM_016B,
 PAD_DAM_016C,
 PAD_DAM_016D,
 PAD_DAM_016E,
 PAD_DAM_016F,
 PAD_DAM_0170,
 PAD_DAM_0171,
 PAD_DAM_0172,
 PAD_DAM_0173,
 PAD_DAM_0174,
 PAD_DAM_0175,
 PAD_DAM_0176,
 PAD_DAM_0177,
 PAD_DAM_0178,
 PAD_DAM_0179,
 PAD_DAM_017A,
 PAD_DAM_017B,
 PAD_DAM_017C,
 PAD_DAM_017D,
 PAD_DAM_017E,
 PAD_DAM_017F,
 PAD_DAM_0180,
 PAD_DAM_0181,
 PAD_DAM_0182,
 PAD_DAM_0183,
 PAD_DAM_0184,
 PAD_DAM_0185,
 PAD_DAM_0186,
 PAD_DAM_0187,
 PAD_DAM_0188,
 PAD_DAM_0189,
 PAD_DAM_018A,
 PAD_DAM_018B,
 PAD_DAM_018C,
 PAD_DAM_018D,
 PAD_DAM_018E,
 PAD_DAM_018F,
 PAD_DAM_0190,
 PAD_DAM_0191,
 PAD_DAM_0192,
 PAD_DAM_0193,
 PAD_DAM_0194,
 PAD_DAM_0195,
 PAD_DAM_0196,
 PAD_DAM_0197,
 PAD_DAM_0198,
 PAD_DAM_0199,
 PAD_DAM_019A,
 PAD_DAM_019B,
 PAD_DAM_019C,
 PAD_DAM_019D,
 PAD_DAM_019E,
 PAD_DAM_019F,
 PAD_DAM_01A0,
 PAD_DAM_01A1,
 PAD_DAM_01A2,
 PAD_DAM_01A3,
 PAD_DAM_01A4,
 PAD_DAM_01A5,
 PAD_DAM_01A6,
 PAD_DAM_01A7,
 PAD_DAM_01A8,
 PAD_DAM_01A9,
 PAD_DAM_01AA,
 PAD_DAM_01AB,
 PAD_DAM_01AC,
 PAD_DAM_01AD,
 PAD_DAM_01AE,
 PAD_DAM_01AF,
 PAD_DAM_01B0,
 PAD_DAM_01B1,
 PAD_DAM_01B2,
 PAD_DAM_01B3,
 PAD_DAM_01B4,
 PAD_DAM_01B5,
 PAD_DAM_01B6,
 PAD_DAM_01B7,
 PAD_DAM_01B8,
 PAD_DAM_01B9,
 PAD_DAM_01BA,
 PAD_DAM_01BB,
 PAD_DAM_01BC,
 PAD_DAM_01BD,
 PAD_DAM_01BE,
 PAD_DAM_01BF,
 PAD_DAM_01C0,
 PAD_DAM_01C1,
 PAD_DAM_01C2,
 PAD_DAM_01C3,
 PAD_DAM_01C4,
 PAD_DAM_01C5,
 PAD_DAM_01C6,
 PAD_DAM_01C7,
 PAD_DAM_01C8,
 PAD_DAM_01C9,
 PAD_DAM_01CA,
 PAD_DAM_01CB,
 PAD_DAM_01CC,
 PAD_DAM_01CD,
 PAD_DAM_01CE,
 PAD_DAM_01CF,
 PAD_DAM_01D0,
 PAD_DAM_01D1,
 PAD_DAM_01D2,
 PAD_DAM_01D3,
 PAD_DAM_01D4,
 PAD_DAM_01D5,
 PAD_DAM_01D6,
 PAD_DAM_01D7,
 PAD_DAM_01D8,
 PAD_DAM_01D9,
 PAD_DAM_01DA,
 PAD_DAM_01DB,
 PAD_DAM_01DC,
 PAD_DAM_01DD,
 PAD_DAM_01DE,
 PAD_DAM_01DF,
 PAD_DAM_01E0,
 PAD_DAM_01E1,
 PAD_DAM_01E2,
 PAD_DAM_01E3,
 PAD_DAM_01E4,
 PAD_DAM_01E5,
 PAD_DAM_01E6,
 PAD_DAM_01E7,
 PAD_DAM_01E8,
 PAD_DAM_01E9,
 PAD_DAM_01EA,
 PAD_DAM_01EB,
 PAD_DAM_01EC,
 PAD_DAM_01ED,
 PAD_DAM_01EE,
 PAD_DAM_01EF,
 PAD_DAM_01F0,
 PAD_DAM_01F1,
 PAD_DAM_01F2,
 PAD_DAM_01F3,
 PAD_DAM_01F4,
 PAD_DAM_01F5,
 PAD_DAM_01F6,
 PAD_DAM_01F7,
 PAD_DAM_01F8,
 PAD_DAM_01F9,
 PAD_DAM_01FA,
 PAD_DAM_01FB,
 PAD_DAM_01FC,
 PAD_DAM_01FD,
 PAD_DAM_01FE,
 PAD_DAM_01FF,
 PAD_DAM_0200,
 PAD_DAM_0201,
 PAD_DAM_0202,
 PAD_DAM_0203,
 PAD_DAM_0204,
 PAD_DAM_0205,
 PAD_DAM_0206,
 PAD_DAM_0207,
 PAD_DAM_0208,
 PAD_DAM_0209,
 PAD_DAM_020A,
 PAD_DAM_020B,
 PAD_DAM_020C,
 PAD_DAM_020D,
 PAD_DAM_020E,
 PAD_DAM_020F,
 PAD_DAM_0210,
 PAD_DAM_0211,
 PAD_DAM_0212,
 PAD_DAM_0213,
 PAD_DAM_0214,
 PAD_DAM_0215,
 PAD_DAM_0216,
 PAD_DAM_0217,
 PAD_DAM_0218,
 PAD_DAM_0219,
 PAD_DAM_021A,
 PAD_DAM_021B,
 PAD_DAM_021C,
 PAD_DAM_021D,
 PAD_DAM_021E,
 PAD_DAM_021F,
 PAD_DAM_0220,
 PAD_DAM_0221,
 PAD_DAM_0222,
 PAD_DAM_0223,
 PAD_DAM_0224,
 PAD_DAM_END
};
enum pad_depo {
 PAD_DEPO_0000,
 PAD_DEPO_0001,
 PAD_DEPO_0002,
 PAD_DEPO_0003,
 PAD_DEPO_0004,
 PAD_DEPO_0005,
 PAD_DEPO_0006,
 PAD_DEPO_0007,
 PAD_DEPO_0008,
 PAD_DEPO_0009,
 PAD_DEPO_000A,
 PAD_DEPO_000B,
 PAD_DEPO_000C,
 PAD_DEPO_000D,
 PAD_DEPO_000E,
 PAD_DEPO_000F,
 PAD_DEPO_0010,
 PAD_DEPO_0011,
 PAD_DEPO_0012,
 PAD_DEPO_0013,
 PAD_DEPO_0014,
 PAD_DEPO_0015,
 PAD_DEPO_0016,
 PAD_DEPO_0017,
 PAD_DEPO_0018,
 PAD_DEPO_0019,
 PAD_DEPO_001A,
 PAD_DEPO_001B,
 PAD_DEPO_001C,
 PAD_DEPO_001D,
 PAD_DEPO_001E,
 PAD_DEPO_001F,
 PAD_DEPO_0020,
 PAD_DEPO_0021,
 PAD_DEPO_0022,
 PAD_DEPO_0023,
 PAD_DEPO_0024,
 PAD_DEPO_0025,
 PAD_DEPO_0026,
 PAD_DEPO_0027,
 PAD_DEPO_0028,
 PAD_DEPO_0029,
 PAD_DEPO_002A,
 PAD_DEPO_002B,
 PAD_DEPO_002C,
 PAD_DEPO_002D,
 PAD_DEPO_002E,
 PAD_DEPO_002F,
 PAD_DEPO_0030,
 PAD_DEPO_0031,
 PAD_DEPO_0032,
 PAD_DEPO_0033,
 PAD_DEPO_0034,
 PAD_DEPO_0035,
 PAD_DEPO_0036,
 PAD_DEPO_0037,
 PAD_DEPO_0038,
 PAD_DEPO_0039,
 PAD_DEPO_003A,
 PAD_DEPO_003B,
 PAD_DEPO_003C,
 PAD_DEPO_003D,
 PAD_DEPO_003E,
 PAD_DEPO_003F,
 PAD_DEPO_0040,
 PAD_DEPO_0041,
 PAD_DEPO_0042,
 PAD_DEPO_0043,
 PAD_DEPO_0044,
 PAD_DEPO_0045,
 PAD_DEPO_0046,
 PAD_DEPO_0047,
 PAD_DEPO_0048,
 PAD_DEPO_0049,
 PAD_DEPO_004A,
 PAD_DEPO_004B,
 PAD_DEPO_004C,
 PAD_DEPO_004D,
 PAD_DEPO_004E,
 PAD_DEPO_004F,
 PAD_DEPO_0050,
 PAD_DEPO_0051,
 PAD_DEPO_0052,
 PAD_DEPO_0053,
 PAD_DEPO_0054,
 PAD_DEPO_0055,
 PAD_DEPO_0056,
 PAD_DEPO_0057,
 PAD_DEPO_0058,
 PAD_DEPO_0059,
 PAD_DEPO_005A,
 PAD_DEPO_005B,
 PAD_DEPO_005C,
 PAD_DEPO_005D,
 PAD_DEPO_005E,
 PAD_DEPO_005F,
 PAD_DEPO_0060,
 PAD_DEPO_0061,
 PAD_DEPO_0062,
 PAD_DEPO_0063,
 PAD_DEPO_0064,
 PAD_DEPO_0065,
 PAD_DEPO_0066,
 PAD_DEPO_0067,
 PAD_DEPO_0068,
 PAD_DEPO_0069,
 PAD_DEPO_006A,
 PAD_DEPO_006B,
 PAD_DEPO_006C,
 PAD_DEPO_006D,
 PAD_DEPO_006E,
 PAD_DEPO_006F,
 PAD_DEPO_0070,
 PAD_DEPO_0071,
 PAD_DEPO_0072,
 PAD_DEPO_0073,
 PAD_DEPO_0074,
 PAD_DEPO_0075,
 PAD_DEPO_0076,
 PAD_DEPO_0077,
 PAD_DEPO_0078,
 PAD_DEPO_0079,
 PAD_DEPO_007A,
 PAD_DEPO_007B,
 PAD_DEPO_007C,
 PAD_DEPO_007D,
 PAD_DEPO_007E,
 PAD_DEPO_007F,
 PAD_DEPO_0080,
 PAD_DEPO_0081,
 PAD_DEPO_0082,
 PAD_DEPO_0083,
 PAD_DEPO_0084,
 PAD_DEPO_0085,
 PAD_DEPO_0086,
 PAD_DEPO_0087,
 PAD_DEPO_0088,
 PAD_DEPO_0089,
 PAD_DEPO_008A,
 PAD_DEPO_008B,
 PAD_DEPO_008C,
 PAD_DEPO_008D,
 PAD_DEPO_008E,
 PAD_DEPO_008F,
 PAD_DEPO_0090,
 PAD_DEPO_0091,
 PAD_DEPO_0092,
 PAD_DEPO_0093,
 PAD_DEPO_0094,
 PAD_DEPO_0095,
 PAD_DEPO_0096,
 PAD_DEPO_0097,
 PAD_DEPO_0098,
 PAD_DEPO_0099,
 PAD_DEPO_009A,
 PAD_DEPO_009B,
 PAD_DEPO_009C,
 PAD_DEPO_009D,
 PAD_DEPO_009E,
 PAD_DEPO_009F,
 PAD_DEPO_00A0,
 PAD_DEPO_00A1,
 PAD_DEPO_00A2,
 PAD_DEPO_00A3,
 PAD_DEPO_00A4,
 PAD_DEPO_00A5,
 PAD_DEPO_00A6,
 PAD_DEPO_00A7,
 PAD_DEPO_00A8,
 PAD_DEPO_00A9,
 PAD_DEPO_00AA,
 PAD_DEPO_00AB,
 PAD_DEPO_00AC,
 PAD_DEPO_00AD,
 PAD_DEPO_00AE,
 PAD_DEPO_00AF,
 PAD_DEPO_00B0,
 PAD_DEPO_00B1,
 PAD_DEPO_00B2,
 PAD_DEPO_00B3,
 PAD_DEPO_00B4,
 PAD_DEPO_00B5,
 PAD_DEPO_00B6,
 PAD_DEPO_00B7,
 PAD_DEPO_00B8,
 PAD_DEPO_00B9,
 PAD_DEPO_00BA,
 PAD_DEPO_00BB,
 PAD_DEPO_00BC,
 PAD_DEPO_00BD,
 PAD_DEPO_00BE,
 PAD_DEPO_00BF,
 PAD_DEPO_00C0,
 PAD_DEPO_00C1,
 PAD_DEPO_00C2,
 PAD_DEPO_00C3,
 PAD_DEPO_00C4,
 PAD_DEPO_00C5,
 PAD_DEPO_00C6,
 PAD_DEPO_00C7,
 PAD_DEPO_00C8,
 PAD_DEPO_00C9,
 PAD_DEPO_00CA,
 PAD_DEPO_00CB,
 PAD_DEPO_00CC,
 PAD_DEPO_00CD,
 PAD_DEPO_00CE,
 PAD_DEPO_00CF,
 PAD_DEPO_00D0,
 PAD_DEPO_00D1,
 PAD_DEPO_00D2,
 PAD_DEPO_00D3,
 PAD_DEPO_00D4,
 PAD_DEPO_00D5,
 PAD_DEPO_00D6,
 PAD_DEPO_00D7,
 PAD_DEPO_00D8,
 PAD_DEPO_00D9,
 PAD_DEPO_00DA,
 PAD_DEPO_00DB,
 PAD_DEPO_00DC,
 PAD_DEPO_00DD,
 PAD_DEPO_00DE,
 PAD_DEPO_00DF,
 PAD_DEPO_00E0,
 PAD_DEPO_00E1,
 PAD_DEPO_00E2,
 PAD_DEPO_00E3,
 PAD_DEPO_00E4,
 PAD_DEPO_00E5,
 PAD_DEPO_00E6,
 PAD_DEPO_00E7,
 PAD_DEPO_00E8,
 PAD_DEPO_00E9,
 PAD_DEPO_00EA,
 PAD_DEPO_00EB,
 PAD_DEPO_00EC,
 PAD_DEPO_00ED,
 PAD_DEPO_00EE,
 PAD_DEPO_00EF,
 PAD_DEPO_00F0,
 PAD_DEPO_00F1,
 PAD_DEPO_00F2,
 PAD_DEPO_00F3,
 PAD_DEPO_00F4,
 PAD_DEPO_00F5,
 PAD_DEPO_00F6,
 PAD_DEPO_00F7,
 PAD_DEPO_00F8,
 PAD_DEPO_00F9,
 PAD_DEPO_00FA,
 PAD_DEPO_00FB,
 PAD_DEPO_00FC,
 PAD_DEPO_00FD,
 PAD_DEPO_00FE,
 PAD_DEPO_00FF,
 PAD_DEPO_0100,
 PAD_DEPO_0101,
 PAD_DEPO_0102,
 PAD_DEPO_END
};
enum pad_dest {
 PAD_DEST_END
};
enum pad_dish {
 PAD_DISH_0000,
 PAD_DISH_0001,
 PAD_DISH_0002,
 PAD_DISH_0003,
 PAD_DISH_0004,
 PAD_DISH_0005,
 PAD_DISH_0006,
 PAD_DISH_0007,
 PAD_DISH_0008,
 PAD_DISH_0009,
 PAD_DISH_000A,
 PAD_DISH_000B,
 PAD_DISH_000C,
 PAD_DISH_000D,
 PAD_DISH_000E,
 PAD_DISH_000F,
 PAD_DISH_0010,
 PAD_DISH_0011,
 PAD_DISH_0012,
 PAD_DISH_0013,
 PAD_DISH_0014,
 PAD_DISH_0015,
 PAD_DISH_0016,
 PAD_DISH_0017,
 PAD_DISH_0018,
 PAD_DISH_0019,
 PAD_DISH_001A,
 PAD_DISH_001B,
 PAD_DISH_001C,
 PAD_DISH_001D,
 PAD_DISH_001E,
 PAD_DISH_001F,
 PAD_DISH_0020,
 PAD_DISH_0021,
 PAD_DISH_0022,
 PAD_DISH_0023,
 PAD_DISH_0024,
 PAD_DISH_0025,
 PAD_DISH_0026,
 PAD_DISH_0027,
 PAD_DISH_0028,
 PAD_DISH_0029,
 PAD_DISH_002A,
 PAD_DISH_002B,
 PAD_DISH_002C,
 PAD_DISH_002D,
 PAD_DISH_002E,
 PAD_DISH_002F,
 PAD_DISH_0030,
 PAD_DISH_0031,
 PAD_DISH_0032,
 PAD_DISH_0033,
 PAD_DISH_0034,
 PAD_DISH_0035,
 PAD_DISH_0036,
 PAD_DISH_0037,
 PAD_DISH_0038,
 PAD_DISH_0039,
 PAD_DISH_003A,
 PAD_DISH_003B,
 PAD_DISH_003C,
 PAD_DISH_003D,
 PAD_DISH_003E,
 PAD_DISH_003F,
 PAD_DISH_0040,
 PAD_DISH_0041,
 PAD_DISH_0042,
 PAD_DISH_0043,
 PAD_DISH_0044,
 PAD_DISH_0045,
 PAD_DISH_0046,
 PAD_DISH_0047,
 PAD_DISH_0048,
 PAD_DISH_0049,
 PAD_DISH_004A,
 PAD_DISH_004B,
 PAD_DISH_004C,
 PAD_DISH_004D,
 PAD_DISH_004E,
 PAD_DISH_004F,
 PAD_DISH_0050,
 PAD_DISH_0051,
 PAD_DISH_0052,
 PAD_DISH_0053,
 PAD_DISH_0054,
 PAD_DISH_0055,
 PAD_DISH_0056,
 PAD_DISH_0057,
 PAD_DISH_0058,
 PAD_DISH_0059,
 PAD_DISH_005A,
 PAD_DISH_005B,
 PAD_DISH_005C,
 PAD_DISH_005D,
 PAD_DISH_005E,
 PAD_DISH_005F,
 PAD_DISH_0060,
 PAD_DISH_0061,
 PAD_DISH_0062,
 PAD_DISH_0063,
 PAD_DISH_0064,
 PAD_DISH_0065,
 PAD_DISH_0066,
 PAD_DISH_0067,
 PAD_DISH_0068,
 PAD_DISH_0069,
 PAD_DISH_006A,
 PAD_DISH_006B,
 PAD_DISH_006C,
 PAD_DISH_006D,
 PAD_DISH_006E,
 PAD_DISH_006F,
 PAD_DISH_0070,
 PAD_DISH_0071,
 PAD_DISH_0072,
 PAD_DISH_0073,
 PAD_DISH_0074,
 PAD_DISH_0075,
 PAD_DISH_0076,
 PAD_DISH_0077,
 PAD_DISH_0078,
 PAD_DISH_0079,
 PAD_DISH_007A,
 PAD_DISH_007B,
 PAD_DISH_007C,
 PAD_DISH_007D,
 PAD_DISH_007E,
 PAD_DISH_007F,
 PAD_DISH_0080,
 PAD_DISH_0081,
 PAD_DISH_0082,
 PAD_DISH_0083,
 PAD_DISH_0084,
 PAD_DISH_0085,
 PAD_DISH_0086,
 PAD_DISH_0087,
 PAD_DISH_0088,
 PAD_DISH_0089,
 PAD_DISH_008A,
 PAD_DISH_008B,
 PAD_DISH_008C,
 PAD_DISH_008D,
 PAD_DISH_008E,
 PAD_DISH_008F,
 PAD_DISH_0090,
 PAD_DISH_0091,
 PAD_DISH_0092,
 PAD_DISH_0093,
 PAD_DISH_0094,
 PAD_DISH_0095,
 PAD_DISH_0096,
 PAD_DISH_0097,
 PAD_DISH_0098,
 PAD_DISH_0099,
 PAD_DISH_009A,
 PAD_DISH_009B,
 PAD_DISH_009C,
 PAD_DISH_009D,
 PAD_DISH_009E,
 PAD_DISH_009F,
 PAD_DISH_00A0,
 PAD_DISH_00A1,
 PAD_DISH_00A2,
 PAD_DISH_00A3,
 PAD_DISH_00A4,
 PAD_DISH_00A5,
 PAD_DISH_00A6,
 PAD_DISH_00A7,
 PAD_DISH_00A8,
 PAD_DISH_00A9,
 PAD_DISH_00AA,
 PAD_DISH_00AB,
 PAD_DISH_00AC,
 PAD_DISH_00AD,
 PAD_DISH_00AE,
 PAD_DISH_00AF,
 PAD_DISH_00B0,
 PAD_DISH_00B1,
 PAD_DISH_00B2,
 PAD_DISH_00B3,
 PAD_DISH_00B4,
 PAD_DISH_00B5,
 PAD_DISH_00B6,
 PAD_DISH_00B7,
 PAD_DISH_00B8,
 PAD_DISH_00B9,
 PAD_DISH_00BA,
 PAD_DISH_00BB,
 PAD_DISH_00BC,
 PAD_DISH_00BD,
 PAD_DISH_00BE,
 PAD_DISH_00BF,
 PAD_DISH_00C0,
 PAD_DISH_00C1,
 PAD_DISH_00C2,
 PAD_DISH_00C3,
 PAD_DISH_00C4,
 PAD_DISH_00C5,
 PAD_DISH_00C6,
 PAD_DISH_00C7,
 PAD_DISH_00C8,
 PAD_DISH_00C9,
 PAD_DISH_00CA,
 PAD_DISH_00CB,
 PAD_DISH_00CC,
 PAD_DISH_00CD,
 PAD_DISH_00CE,
 PAD_DISH_00CF,
 PAD_DISH_00D0,
 PAD_DISH_00D1,
 PAD_DISH_00D2,
 PAD_DISH_00D3,
 PAD_DISH_00D4,
 PAD_DISH_00D5,
 PAD_DISH_00D6,
 PAD_DISH_00D7,
 PAD_DISH_00D8,
 PAD_DISH_00D9,
 PAD_DISH_00DA,
 PAD_DISH_00DB,
 PAD_DISH_00DC,
 PAD_DISH_00DD,
 PAD_DISH_00DE,
 PAD_DISH_00DF,
 PAD_DISH_00E0,
 PAD_DISH_00E1,
 PAD_DISH_00E2,
 PAD_DISH_00E3,
 PAD_DISH_00E4,
 PAD_DISH_00E5,
 PAD_DISH_00E6,
 PAD_DISH_00E7,
 PAD_DISH_00E8,
 PAD_DISH_00E9,
 PAD_DISH_00EA,
 PAD_DISH_00EB,
 PAD_DISH_00EC,
 PAD_DISH_00ED,
 PAD_DISH_00EE,
 PAD_DISH_00EF,
 PAD_DISH_00F0,
 PAD_DISH_00F1,
 PAD_DISH_00F2,
 PAD_DISH_00F3,
 PAD_DISH_00F4,
 PAD_DISH_00F5,
 PAD_DISH_00F6,
 PAD_DISH_00F7,
 PAD_DISH_00F8,
 PAD_DISH_00F9,
 PAD_DISH_00FA,
 PAD_DISH_00FB,
 PAD_DISH_00FC,
 PAD_DISH_00FD,
 PAD_DISH_00FE,
 PAD_DISH_00FF,
 PAD_DISH_0100,
 PAD_DISH_0101,
 PAD_DISH_0102,
 PAD_DISH_0103,
 PAD_DISH_0104,
 PAD_DISH_0105,
 PAD_DISH_0106,
 PAD_DISH_0107,
 PAD_DISH_0108,
 PAD_DISH_0109,
 PAD_DISH_010A,
 PAD_DISH_010B,
 PAD_DISH_010C,
 PAD_DISH_010D,
 PAD_DISH_010E,
 PAD_DISH_010F,
 PAD_DISH_0110,
 PAD_DISH_0111,
 PAD_DISH_0112,
 PAD_DISH_0113,
 PAD_DISH_0114,
 PAD_DISH_0115,
 PAD_DISH_0116,
 PAD_DISH_0117,
 PAD_DISH_0118,
 PAD_DISH_0119,
 PAD_DISH_011A,
 PAD_DISH_011B,
 PAD_DISH_011C,
 PAD_DISH_011D,
 PAD_DISH_011E,
 PAD_DISH_011F,
 PAD_DISH_0120,
 PAD_DISH_0121,
 PAD_DISH_0122,
 PAD_DISH_0123,
 PAD_DISH_0124,
 PAD_DISH_0125,
 PAD_DISH_0126,
 PAD_DISH_0127,
 PAD_DISH_0128,
 PAD_DISH_0129,
 PAD_DISH_012A,
 PAD_DISH_012B,
 PAD_DISH_012C,
 PAD_DISH_012D,
 PAD_DISH_012E,
 PAD_DISH_012F,
 PAD_DISH_0130,
 PAD_DISH_0131,
 PAD_DISH_0132,
 PAD_DISH_0133,
 PAD_DISH_0134,
 PAD_DISH_0135,
 PAD_DISH_0136,
 PAD_DISH_0137,
 PAD_DISH_0138,
 PAD_DISH_0139,
 PAD_DISH_013A,
 PAD_DISH_013B,
 PAD_DISH_013C,
 PAD_DISH_013D,
 PAD_DISH_013E,
 PAD_DISH_013F,
 PAD_DISH_0140,
 PAD_DISH_0141,
 PAD_DISH_0142,
 PAD_DISH_0143,
 PAD_DISH_0144,
 PAD_DISH_0145,
 PAD_DISH_0146,
 PAD_DISH_0147,
 PAD_DISH_0148,
 PAD_DISH_0149,
 PAD_DISH_014A,
 PAD_DISH_014B,
 PAD_DISH_014C,
 PAD_DISH_014D,
 PAD_DISH_014E,
 PAD_DISH_014F,
 PAD_DISH_0150,
 PAD_DISH_0151,
 PAD_DISH_0152,
 PAD_DISH_0153,
 PAD_DISH_0154,
 PAD_DISH_0155,
 PAD_DISH_0156,
 PAD_DISH_0157,
 PAD_DISH_0158,
 PAD_DISH_0159,
 PAD_DISH_015A,
 PAD_DISH_015B,
 PAD_DISH_015C,
 PAD_DISH_015D,
 PAD_DISH_015E,
 PAD_DISH_015F,
 PAD_DISH_0160,
 PAD_DISH_0161,
 PAD_DISH_0162,
 PAD_DISH_0163,
 PAD_DISH_0164,
 PAD_DISH_0165,
 PAD_DISH_0166,
 PAD_DISH_0167,
 PAD_DISH_0168,
 PAD_DISH_0169,
 PAD_DISH_016A,
 PAD_DISH_016B,
 PAD_DISH_016C,
 PAD_DISH_016D,
 PAD_DISH_016E,
 PAD_DISH_016F,
 PAD_DISH_0170,
 PAD_DISH_0171,
 PAD_DISH_0172,
 PAD_DISH_0173,
 PAD_DISH_0174,
 PAD_DISH_0175,
 PAD_DISH_0176,
 PAD_DISH_0177,
 PAD_DISH_0178,
 PAD_DISH_0179,
 PAD_DISH_017A,
 PAD_DISH_017B,
 PAD_DISH_017C,
 PAD_DISH_017D,
 PAD_DISH_017E,
 PAD_DISH_017F,
 PAD_DISH_0180,
 PAD_DISH_0181,
 PAD_DISH_0182,
 PAD_DISH_0183,
 PAD_DISH_0184,
 PAD_DISH_0185,
 PAD_DISH_0186,
 PAD_DISH_0187,
 PAD_DISH_0188,
 PAD_DISH_0189,
 PAD_DISH_018A,
 PAD_DISH_018B,
 PAD_DISH_018C,
 PAD_DISH_018D,
 PAD_DISH_018E,
 PAD_DISH_018F,
 PAD_DISH_0190,
 PAD_DISH_0191,
 PAD_DISH_0192,
 PAD_DISH_0193,
 PAD_DISH_0194,
 PAD_DISH_0195,
 PAD_DISH_0196,
 PAD_DISH_0197,
 PAD_DISH_0198,
 PAD_DISH_0199,
 PAD_DISH_019A,
 PAD_DISH_019B,
 PAD_DISH_019C,
 PAD_DISH_019D,
 PAD_DISH_019E,
 PAD_DISH_019F,
 PAD_DISH_01A0,
 PAD_DISH_01A1,
 PAD_DISH_01A2,
 PAD_DISH_01A3,
 PAD_DISH_01A4,
 PAD_DISH_01A5,
 PAD_DISH_01A6,
 PAD_DISH_01A7,
 PAD_DISH_01A8,
 PAD_DISH_01A9,
 PAD_DISH_01AA,
 PAD_DISH_01AB,
 PAD_DISH_01AC,
 PAD_DISH_01AD,
 PAD_DISH_01AE,
 PAD_DISH_01AF,
 PAD_DISH_01B0,
 PAD_DISH_01B1,
 PAD_DISH_01B2,
 PAD_DISH_01B3,
 PAD_DISH_01B4,
 PAD_DISH_01B5,
 PAD_DISH_01B6,
 PAD_DISH_01B7,
 PAD_DISH_01B8,
 PAD_DISH_01B9,
 PAD_DISH_01BA,
 PAD_DISH_01BB,
 PAD_DISH_01BC,
 PAD_DISH_01BD,
 PAD_DISH_01BE,
 PAD_DISH_01BF,
 PAD_DISH_01C0,
 PAD_DISH_01C1,
 PAD_DISH_01C2,
 PAD_DISH_01C3,
 PAD_DISH_01C4,
 PAD_DISH_01C5,
 PAD_DISH_01C6,
 PAD_DISH_01C7,
 PAD_DISH_01C8,
 PAD_DISH_01C9,
 PAD_DISH_01CA,
 PAD_DISH_01CB,
 PAD_DISH_01CC,
 PAD_DISH_01CD,
 PAD_DISH_01CE,
 PAD_DISH_01CF,
 PAD_DISH_01D0,
 PAD_DISH_01D1,
 PAD_DISH_01D2,
 PAD_DISH_01D3,
 PAD_DISH_01D4,
 PAD_DISH_01D5,
 PAD_DISH_01D6,
 PAD_DISH_01D7,
 PAD_DISH_01D8,
 PAD_DISH_01D9,
 PAD_DISH_01DA,
 PAD_DISH_01DB,
 PAD_DISH_01DC,
 PAD_DISH_01DD,
 PAD_DISH_01DE,
 PAD_DISH_01DF,
 PAD_DISH_01E0,
 PAD_DISH_01E1,
 PAD_DISH_01E2,
 PAD_DISH_01E3,
 PAD_DISH_01E4,
 PAD_DISH_01E5,
 PAD_DISH_01E6,
 PAD_DISH_01E7,
 PAD_DISH_01E8,
 PAD_DISH_01E9,
 PAD_DISH_01EA,
 PAD_DISH_01EB,
 PAD_DISH_01EC,
 PAD_DISH_01ED,
 PAD_DISH_01EE,
 PAD_DISH_01EF,
 PAD_DISH_01F0,
 PAD_DISH_01F1,
 PAD_DISH_01F2,
 PAD_DISH_01F3,
 PAD_DISH_01F4,
 PAD_DISH_01F5,
 PAD_DISH_01F6,
 PAD_DISH_01F7,
 PAD_DISH_01F8,
 PAD_DISH_01F9,
 PAD_DISH_01FA,
 PAD_DISH_01FB,
 PAD_DISH_01FC,
 PAD_DISH_01FD,
 PAD_DISH_01FE,
 PAD_DISH_01FF,
 PAD_DISH_0200,
 PAD_DISH_0201,
 PAD_DISH_0202,
 PAD_DISH_0203,
 PAD_DISH_0204,
 PAD_DISH_0205,
 PAD_DISH_0206,
 PAD_DISH_0207,
 PAD_DISH_0208,
 PAD_DISH_0209,
 PAD_DISH_020A,
 PAD_DISH_020B,
 PAD_DISH_020C,
 PAD_DISH_020D,
 PAD_DISH_020E,
 PAD_DISH_020F,
 PAD_DISH_0210,
 PAD_DISH_0211,
 PAD_DISH_0212,
 PAD_DISH_0213,
 PAD_DISH_0214,
 PAD_DISH_0215,
 PAD_DISH_0216,
 PAD_DISH_0217,
 PAD_DISH_0218,
 PAD_DISH_0219,
 PAD_DISH_021A,
 PAD_DISH_021B,
 PAD_DISH_021C,
 PAD_DISH_021D,
 PAD_DISH_021E,
 PAD_DISH_021F,
 PAD_DISH_0220,
 PAD_DISH_0221,
 PAD_DISH_0222,
 PAD_DISH_0223,
 PAD_DISH_0224,
 PAD_DISH_0225,
 PAD_DISH_0226,
 PAD_DISH_0227,
 PAD_DISH_0228,
 PAD_DISH_0229,
 PAD_DISH_022A,
 PAD_DISH_022B,
 PAD_DISH_022C,
 PAD_DISH_022D,
 PAD_DISH_022E,
 PAD_DISH_022F,
 PAD_DISH_0230,
 PAD_DISH_0231,
 PAD_DISH_0232,
 PAD_DISH_0233,
 PAD_DISH_0234,
 PAD_DISH_0235,
 PAD_DISH_0236,
 PAD_DISH_0237,
 PAD_DISH_0238,
 PAD_DISH_0239,
 PAD_DISH_023A,
 PAD_DISH_023B,
 PAD_DISH_023C,
 PAD_DISH_023D,
 PAD_DISH_023E,
 PAD_DISH_023F,
 PAD_DISH_0240,
 PAD_DISH_0241,
 PAD_DISH_0242,
 PAD_DISH_0243,
 PAD_DISH_0244,
 PAD_DISH_0245,
 PAD_DISH_0246,
 PAD_DISH_0247,
 PAD_DISH_0248,
 PAD_DISH_0249,
 PAD_DISH_024A,
 PAD_DISH_024B,
 PAD_DISH_024C,
 PAD_DISH_024D,
 PAD_DISH_024E,
 PAD_DISH_024F,
 PAD_DISH_0250,
 PAD_DISH_0251,
 PAD_DISH_0252,
 PAD_DISH_0253,
 PAD_DISH_0254,
 PAD_DISH_0255,
 PAD_DISH_0256,
 PAD_DISH_0257,
 PAD_DISH_0258,
 PAD_DISH_0259,
 PAD_DISH_025A,
 PAD_DISH_025B,
 PAD_DISH_025C,
 PAD_DISH_025D,
 PAD_DISH_025E,
 PAD_DISH_025F,
 PAD_DISH_0260,
 PAD_DISH_0261,
 PAD_DISH_0262,
 PAD_DISH_0263,
 PAD_DISH_0264,
 PAD_DISH_0265,
 PAD_DISH_0266,
 PAD_DISH_0267,
 PAD_DISH_0268,
 PAD_DISH_0269,
 PAD_DISH_026A,
 PAD_DISH_026B,
 PAD_DISH_026C,
 PAD_DISH_026D,
 PAD_DISH_026E,
 PAD_DISH_026F,
 PAD_DISH_0270,
 PAD_DISH_0271,
 PAD_DISH_0272,
 PAD_DISH_0273,
 PAD_DISH_0274,
 PAD_DISH_0275,
 PAD_DISH_0276,
 PAD_DISH_0277,
 PAD_DISH_0278,
 PAD_DISH_0279,
 PAD_DISH_027A,
 PAD_DISH_027B,
 PAD_DISH_027C,
 PAD_DISH_027D,
 PAD_DISH_027E,
 PAD_DISH_027F,
 PAD_DISH_0280,
 PAD_DISH_0281,
 PAD_DISH_0282,
 PAD_DISH_0283,
 PAD_DISH_0284,
 PAD_DISH_0285,
 PAD_DISH_0286,
 PAD_DISH_0287,
 PAD_DISH_0288,
 PAD_DISH_0289,
 PAD_DISH_END
};
enum pad_ear {
 PAD_EAR_0000,
 PAD_EAR_0001,
 PAD_EAR_0002,
 PAD_EAR_0003,
 PAD_EAR_0004,
 PAD_EAR_0005,
 PAD_EAR_0006,
 PAD_EAR_0007,
 PAD_EAR_0008,
 PAD_EAR_0009,
 PAD_EAR_000A,
 PAD_EAR_000B,
 PAD_EAR_000C,
 PAD_EAR_000D,
 PAD_EAR_000E,
 PAD_EAR_000F,
 PAD_EAR_0010,
 PAD_EAR_0011,
 PAD_EAR_0012,
 PAD_EAR_0013,
 PAD_EAR_0014,
 PAD_EAR_0015,
 PAD_EAR_0016,
 PAD_EAR_0017,
 PAD_EAR_0018,
 PAD_EAR_0019,
 PAD_EAR_001A,
 PAD_EAR_001B,
 PAD_EAR_001C,
 PAD_EAR_001D,
 PAD_EAR_001E,
 PAD_EAR_001F,
 PAD_EAR_0020,
 PAD_EAR_0021,
 PAD_EAR_0022,
 PAD_EAR_0023,
 PAD_EAR_0024,
 PAD_EAR_0025,
 PAD_EAR_0026,
 PAD_EAR_0027,
 PAD_EAR_0028,
 PAD_EAR_0029,
 PAD_EAR_002A,
 PAD_EAR_002B,
 PAD_EAR_002C,
 PAD_EAR_002D,
 PAD_EAR_002E,
 PAD_EAR_002F,
 PAD_EAR_0030,
 PAD_EAR_0031,
 PAD_EAR_0032,
 PAD_EAR_0033,
 PAD_EAR_0034,
 PAD_EAR_0035,
 PAD_EAR_0036,
 PAD_EAR_0037,
 PAD_EAR_0038,
 PAD_EAR_0039,
 PAD_EAR_003A,
 PAD_EAR_003B,
 PAD_EAR_003C,
 PAD_EAR_003D,
 PAD_EAR_003E,
 PAD_EAR_003F,
 PAD_EAR_0040,
 PAD_EAR_0041,
 PAD_EAR_0042,
 PAD_EAR_0043,
 PAD_EAR_0044,
 PAD_EAR_0045,
 PAD_EAR_0046,
 PAD_EAR_0047,
 PAD_EAR_0048,
 PAD_EAR_0049,
 PAD_EAR_004A,
 PAD_EAR_004B,
 PAD_EAR_004C,
 PAD_EAR_004D,
 PAD_EAR_004E,
 PAD_EAR_004F,
 PAD_EAR_0050,
 PAD_EAR_0051,
 PAD_EAR_0052,
 PAD_EAR_0053,
 PAD_EAR_0054,
 PAD_EAR_0055,
 PAD_EAR_0056,
 PAD_EAR_0057,
 PAD_EAR_0058,
 PAD_EAR_0059,
 PAD_EAR_005A,
 PAD_EAR_005B,
 PAD_EAR_005C,
 PAD_EAR_005D,
 PAD_EAR_005E,
 PAD_EAR_005F,
 PAD_EAR_0060,
 PAD_EAR_0061,
 PAD_EAR_0062,
 PAD_EAR_0063,
 PAD_EAR_0064,
 PAD_EAR_0065,
 PAD_EAR_0066,
 PAD_EAR_0067,
 PAD_EAR_0068,
 PAD_EAR_0069,
 PAD_EAR_006A,
 PAD_EAR_006B,
 PAD_EAR_006C,
 PAD_EAR_006D,
 PAD_EAR_006E,
 PAD_EAR_006F,
 PAD_EAR_0070,
 PAD_EAR_0071,
 PAD_EAR_0072,
 PAD_EAR_0073,
 PAD_EAR_0074,
 PAD_EAR_0075,
 PAD_EAR_0076,
 PAD_EAR_0077,
 PAD_EAR_0078,
 PAD_EAR_0079,
 PAD_EAR_007A,
 PAD_EAR_007B,
 PAD_EAR_007C,
 PAD_EAR_007D,
 PAD_EAR_007E,
 PAD_EAR_007F,
 PAD_EAR_0080,
 PAD_EAR_0081,
 PAD_EAR_0082,
 PAD_EAR_0083,
 PAD_EAR_0084,
 PAD_EAR_0085,
 PAD_EAR_0086,
 PAD_EAR_0087,
 PAD_EAR_0088,
 PAD_EAR_0089,
 PAD_EAR_008A,
 PAD_EAR_008B,
 PAD_EAR_008C,
 PAD_EAR_008D,
 PAD_EAR_008E,
 PAD_EAR_008F,
 PAD_EAR_0090,
 PAD_EAR_0091,
 PAD_EAR_0092,
 PAD_EAR_0093,
 PAD_EAR_0094,
 PAD_EAR_0095,
 PAD_EAR_0096,
 PAD_EAR_0097,
 PAD_EAR_0098,
 PAD_EAR_0099,
 PAD_EAR_009A,
 PAD_EAR_009B,
 PAD_EAR_009C,
 PAD_EAR_009D,
 PAD_EAR_009E,
 PAD_EAR_009F,
 PAD_EAR_00A0,
 PAD_EAR_00A1,
 PAD_EAR_00A2,
 PAD_EAR_00A3,
 PAD_EAR_00A4,
 PAD_EAR_00A5,
 PAD_EAR_00A6,
 PAD_EAR_00A7,
 PAD_EAR_00A8,
 PAD_EAR_00A9,
 PAD_EAR_00AA,
 PAD_EAR_00AB,
 PAD_EAR_00AC,
 PAD_EAR_00AD,
 PAD_EAR_00AE,
 PAD_EAR_00AF,
 PAD_EAR_00B0,
 PAD_EAR_00B1,
 PAD_EAR_00B2,
 PAD_EAR_00B3,
 PAD_EAR_00B4,
 PAD_EAR_00B5,
 PAD_EAR_00B6,
 PAD_EAR_00B7,
 PAD_EAR_00B8,
 PAD_EAR_00B9,
 PAD_EAR_00BA,
 PAD_EAR_00BB,
 PAD_EAR_00BC,
 PAD_EAR_00BD,
 PAD_EAR_00BE,
 PAD_EAR_00BF,
 PAD_EAR_00C0,
 PAD_EAR_00C1,
 PAD_EAR_00C2,
 PAD_EAR_00C3,
 PAD_EAR_00C4,
 PAD_EAR_00C5,
 PAD_EAR_00C6,
 PAD_EAR_00C7,
 PAD_EAR_00C8,
 PAD_EAR_00C9,
 PAD_EAR_00CA,
 PAD_EAR_00CB,
 PAD_EAR_00CC,
 PAD_EAR_00CD,
 PAD_EAR_00CE,
 PAD_EAR_00CF,
 PAD_EAR_00D0,
 PAD_EAR_00D1,
 PAD_EAR_00D2,
 PAD_EAR_00D3,
 PAD_EAR_00D4,
 PAD_EAR_00D5,
 PAD_EAR_00D6,
 PAD_EAR_00D7,
 PAD_EAR_00D8,
 PAD_EAR_00D9,
 PAD_EAR_00DA,
 PAD_EAR_00DB,
 PAD_EAR_00DC,
 PAD_EAR_00DD,
 PAD_EAR_00DE,
 PAD_EAR_00DF,
 PAD_EAR_00E0,
 PAD_EAR_00E1,
 PAD_EAR_00E2,
 PAD_EAR_00E3,
 PAD_EAR_00E4,
 PAD_EAR_00E5,
 PAD_EAR_00E6,
 PAD_EAR_00E7,
 PAD_EAR_00E8,
 PAD_EAR_00E9,
 PAD_EAR_00EA,
 PAD_EAR_00EB,
 PAD_EAR_00EC,
 PAD_EAR_00ED,
 PAD_EAR_00EE,
 PAD_EAR_00EF,
 PAD_EAR_00F0,
 PAD_EAR_00F1,
 PAD_EAR_00F2,
 PAD_EAR_00F3,
 PAD_EAR_00F4,
 PAD_EAR_00F5,
 PAD_EAR_00F6,
 PAD_EAR_00F7,
 PAD_EAR_00F8,
 PAD_EAR_00F9,
 PAD_EAR_00FA,
 PAD_EAR_00FB,
 PAD_EAR_00FC,
 PAD_EAR_00FD,
 PAD_EAR_00FE,
 PAD_EAR_00FF,
 PAD_EAR_0100,
 PAD_EAR_0101,
 PAD_EAR_0102,
 PAD_EAR_0103,
 PAD_EAR_0104,
 PAD_EAR_0105,
 PAD_EAR_0106,
 PAD_EAR_0107,
 PAD_EAR_0108,
 PAD_EAR_0109,
 PAD_EAR_010A,
 PAD_EAR_010B,
 PAD_EAR_010C,
 PAD_EAR_010D,
 PAD_EAR_010E,
 PAD_EAR_010F,
 PAD_EAR_0110,
 PAD_EAR_0111,
 PAD_EAR_0112,
 PAD_EAR_0113,
 PAD_EAR_0114,
 PAD_EAR_0115,
 PAD_EAR_0116,
 PAD_EAR_0117,
 PAD_EAR_0118,
 PAD_EAR_0119,
 PAD_EAR_011A,
 PAD_EAR_011B,
 PAD_EAR_011C,
 PAD_EAR_011D,
 PAD_EAR_011E,
 PAD_EAR_011F,
 PAD_EAR_0120,
 PAD_EAR_0121,
 PAD_EAR_0122,
 PAD_EAR_0123,
 PAD_EAR_0124,
 PAD_EAR_0125,
 PAD_EAR_0126,
 PAD_EAR_0127,
 PAD_EAR_0128,
 PAD_EAR_0129,
 PAD_EAR_012A,
 PAD_EAR_012B,
 PAD_EAR_012C,
 PAD_EAR_012D,
 PAD_EAR_012E,
 PAD_EAR_012F,
 PAD_EAR_0130,
 PAD_EAR_0131,
 PAD_EAR_0132,
 PAD_EAR_0133,
 PAD_EAR_0134,
 PAD_EAR_0135,
 PAD_EAR_0136,
 PAD_EAR_0137,
 PAD_EAR_0138,
 PAD_EAR_0139,
 PAD_EAR_013A,
 PAD_EAR_013B,
 PAD_EAR_013C,
 PAD_EAR_013D,
 PAD_EAR_013E,
 PAD_EAR_013F,
 PAD_EAR_0140,
 PAD_EAR_0141,
 PAD_EAR_0142,
 PAD_EAR_0143,
 PAD_EAR_0144,
 PAD_EAR_0145,
 PAD_EAR_0146,
 PAD_EAR_0147,
 PAD_EAR_0148,
 PAD_EAR_0149,
 PAD_EAR_014A,
 PAD_EAR_014B,
 PAD_EAR_014C,
 PAD_EAR_014D,
 PAD_EAR_014E,
 PAD_EAR_014F,
 PAD_EAR_0150,
 PAD_EAR_0151,
 PAD_EAR_0152,
 PAD_EAR_0153,
 PAD_EAR_0154,
 PAD_EAR_0155,
 PAD_EAR_0156,
 PAD_EAR_0157,
 PAD_EAR_0158,
 PAD_EAR_0159,
 PAD_EAR_015A,
 PAD_EAR_015B,
 PAD_EAR_015C,
 PAD_EAR_015D,
 PAD_EAR_015E,
 PAD_EAR_015F,
 PAD_EAR_0160,
 PAD_EAR_0161,
 PAD_EAR_0162,
 PAD_EAR_0163,
 PAD_EAR_0164,
 PAD_EAR_0165,
 PAD_EAR_0166,
 PAD_EAR_0167,
 PAD_EAR_0168,
 PAD_EAR_0169,
 PAD_EAR_016A,
 PAD_EAR_016B,
 PAD_EAR_016C,
 PAD_EAR_016D,
 PAD_EAR_016E,
 PAD_EAR_016F,
 PAD_EAR_0170,
 PAD_EAR_0171,
 PAD_EAR_0172,
 PAD_EAR_0173,
 PAD_EAR_0174,
 PAD_EAR_0175,
 PAD_EAR_0176,
 PAD_EAR_0177,
 PAD_EAR_0178,
 PAD_EAR_0179,
 PAD_EAR_017A,
 PAD_EAR_017B,
 PAD_EAR_017C,
 PAD_EAR_017D,
 PAD_EAR_017E,
 PAD_EAR_017F,
 PAD_EAR_0180,
 PAD_EAR_0181,
 PAD_EAR_0182,
 PAD_EAR_0183,
 PAD_EAR_0184,
 PAD_EAR_0185,
 PAD_EAR_0186,
 PAD_EAR_0187,
 PAD_EAR_0188,
 PAD_EAR_0189,
 PAD_EAR_018A,
 PAD_EAR_018B,
 PAD_EAR_018C,
 PAD_EAR_018D,
 PAD_EAR_018E,
 PAD_EAR_018F,
 PAD_EAR_0190,
 PAD_EAR_0191,
 PAD_EAR_0192,
 PAD_EAR_0193,
 PAD_EAR_0194,
 PAD_EAR_0195,
 PAD_EAR_0196,
 PAD_EAR_0197,
 PAD_EAR_0198,
 PAD_EAR_0199,
 PAD_EAR_019A,
 PAD_EAR_019B,
 PAD_EAR_019C,
 PAD_EAR_019D,
 PAD_EAR_019E,
 PAD_EAR_019F,
 PAD_EAR_01A0,
 PAD_EAR_01A1,
 PAD_EAR_01A2,
 PAD_EAR_01A3,
 PAD_EAR_01A4,
 PAD_EAR_01A5,
 PAD_EAR_01A6,
 PAD_EAR_01A7,
 PAD_EAR_01A8,
 PAD_EAR_01A9,
 PAD_EAR_01AA,
 PAD_EAR_01AB,
 PAD_EAR_01AC,
 PAD_EAR_01AD,
 PAD_EAR_01AE,
 PAD_EAR_01AF,
 PAD_EAR_01B0,
 PAD_EAR_01B1,
 PAD_EAR_01B2,
 PAD_EAR_01B3,
 PAD_EAR_01B4,
 PAD_EAR_01B5,
 PAD_EAR_01B6,
 PAD_EAR_01B7,
 PAD_EAR_01B8,
 PAD_EAR_01B9,
 PAD_EAR_01BA,
 PAD_EAR_01BB,
 PAD_EAR_01BC,
 PAD_EAR_01BD,
 PAD_EAR_01BE,
 PAD_EAR_01BF,
 PAD_EAR_01C0,
 PAD_EAR_01C1,
 PAD_EAR_01C2,
 PAD_EAR_01C3,
 PAD_EAR_01C4,
 PAD_EAR_01C5,
 PAD_EAR_01C6,
 PAD_EAR_01C7,
 PAD_EAR_01C8,
 PAD_EAR_01C9,
 PAD_EAR_01CA,
 PAD_EAR_01CB,
 PAD_EAR_01CC,
 PAD_EAR_01CD,
 PAD_EAR_01CE,
 PAD_EAR_01CF,
 PAD_EAR_01D0,
 PAD_EAR_01D1,
 PAD_EAR_01D2,
 PAD_EAR_01D3,
 PAD_EAR_01D4,
 PAD_EAR_01D5,
 PAD_EAR_01D6,
 PAD_EAR_01D7,
 PAD_EAR_01D8,
 PAD_EAR_01D9,
 PAD_EAR_01DA,
 PAD_EAR_01DB,
 PAD_EAR_01DC,
 PAD_EAR_01DD,
 PAD_EAR_01DE,
 PAD_EAR_01DF,
 PAD_EAR_01E0,
 PAD_EAR_01E1,
 PAD_EAR_01E2,
 PAD_EAR_01E3,
 PAD_EAR_01E4,
 PAD_EAR_01E5,
 PAD_EAR_01E6,
 PAD_EAR_01E7,
 PAD_EAR_01E8,
 PAD_EAR_01E9,
 PAD_EAR_01EA,
 PAD_EAR_01EB,
 PAD_EAR_01EC,
 PAD_EAR_01ED,
 PAD_EAR_01EE,
 PAD_EAR_01EF,
 PAD_EAR_01F0,
 PAD_EAR_01F1,
 PAD_EAR_01F2,
 PAD_EAR_01F3,
 PAD_EAR_01F4,
 PAD_EAR_01F5,
 PAD_EAR_01F6,
 PAD_EAR_01F7,
 PAD_EAR_01F8,
 PAD_EAR_01F9,
 PAD_EAR_01FA,
 PAD_EAR_01FB,
 PAD_EAR_01FC,
 PAD_EAR_01FD,
 PAD_EAR_01FE,
 PAD_EAR_01FF,
 PAD_EAR_0200,
 PAD_EAR_0201,
 PAD_EAR_0202,
 PAD_EAR_0203,
 PAD_EAR_0204,
 PAD_EAR_0205,
 PAD_EAR_0206,
 PAD_EAR_0207,
 PAD_EAR_0208,
 PAD_EAR_0209,
 PAD_EAR_020A,
 PAD_EAR_020B,
 PAD_EAR_020C,
 PAD_EAR_020D,
 PAD_EAR_020E,
 PAD_EAR_020F,
 PAD_EAR_0210,
 PAD_EAR_0211,
 PAD_EAR_0212,
 PAD_EAR_0213,
 PAD_EAR_0214,
 PAD_EAR_0215,
 PAD_EAR_0216,
 PAD_EAR_0217,
 PAD_EAR_0218,
 PAD_EAR_0219,
 PAD_EAR_021A,
 PAD_EAR_021B,
 PAD_EAR_021C,
 PAD_EAR_021D,
 PAD_EAR_021E,
 PAD_EAR_021F,
 PAD_EAR_0220,
 PAD_EAR_0221,
 PAD_EAR_0222,
 PAD_EAR_0223,
 PAD_EAR_0224,
 PAD_EAR_0225,
 PAD_EAR_0226,
 PAD_EAR_0227,
 PAD_EAR_0228,
 PAD_EAR_0229,
 PAD_EAR_022A,
 PAD_EAR_022B,
 PAD_EAR_022C,
 PAD_EAR_022D,
 PAD_EAR_022E,
 PAD_EAR_022F,
 PAD_EAR_0230,
 PAD_EAR_0231,
 PAD_EAR_0232,
 PAD_EAR_0233,
 PAD_EAR_0234,
 PAD_EAR_0235,
 PAD_EAR_0236,
 PAD_EAR_0237,
 PAD_EAR_0238,
 PAD_EAR_0239,
 PAD_EAR_023A,
 PAD_EAR_023B,
 PAD_EAR_023C,
 PAD_EAR_023D,
 PAD_EAR_023E,
 PAD_EAR_023F,
 PAD_EAR_0240,
 PAD_EAR_0241,
 PAD_EAR_0242,
 PAD_EAR_0243,
 PAD_EAR_0244,
 PAD_EAR_0245,
 PAD_EAR_0246,
 PAD_EAR_0247,
 PAD_EAR_0248,
 PAD_EAR_0249,
 PAD_EAR_024A,
 PAD_EAR_024B,
 PAD_EAR_024C,
 PAD_EAR_024D,
 PAD_EAR_024E,
 PAD_EAR_024F,
 PAD_EAR_0250,
 PAD_EAR_0251,
 PAD_EAR_0252,
 PAD_EAR_0253,
 PAD_EAR_0254,
 PAD_EAR_0255,
 PAD_EAR_0256,
 PAD_EAR_0257,
 PAD_EAR_0258,
 PAD_EAR_0259,
 PAD_EAR_025A,
 PAD_EAR_025B,
 PAD_EAR_025C,
 PAD_EAR_025D,
 PAD_EAR_025E,
 PAD_EAR_025F,
 PAD_EAR_0260,
 PAD_EAR_0261,
 PAD_EAR_0262,
 PAD_EAR_0263,
 PAD_EAR_0264,
 PAD_EAR_0265,
 PAD_EAR_0266,
 PAD_EAR_0267,
 PAD_EAR_0268,
 PAD_EAR_0269,
 PAD_EAR_026A,
 PAD_EAR_026B,
 PAD_EAR_026C,
 PAD_EAR_026D,
 PAD_EAR_026E,
 PAD_EAR_026F,
 PAD_EAR_0270,
 PAD_EAR_0271,
 PAD_EAR_0272,
 PAD_EAR_0273,
 PAD_EAR_0274,
 PAD_EAR_0275,
 PAD_EAR_0276,
 PAD_EAR_0277,
 PAD_EAR_0278,
 PAD_EAR_0279,
 PAD_EAR_027A,
 PAD_EAR_027B,
 PAD_EAR_027C,
 PAD_EAR_027D,
 PAD_EAR_027E,
 PAD_EAR_027F,
 PAD_EAR_END
};
enum pad_eld {
 PAD_ELD_0000,
 PAD_ELD_0001,
 PAD_ELD_0002,
 PAD_ELD_0003,
 PAD_ELD_0004,
 PAD_ELD_0005,
 PAD_ELD_0006,
 PAD_ELD_0007,
 PAD_ELD_0008,
 PAD_ELD_0009,
 PAD_ELD_000A,
 PAD_ELD_000B,
 PAD_ELD_000C,
 PAD_ELD_000D,
 PAD_ELD_000E,
 PAD_ELD_000F,
 PAD_ELD_0010,
 PAD_ELD_0011,
 PAD_ELD_0012,
 PAD_ELD_0013,
 PAD_ELD_0014,
 PAD_ELD_0015,
 PAD_ELD_0016,
 PAD_ELD_0017,
 PAD_ELD_0018,
 PAD_ELD_0019,
 PAD_ELD_001A,
 PAD_ELD_001B,
 PAD_ELD_001C,
 PAD_ELD_001D,
 PAD_ELD_001E,
 PAD_ELD_001F,
 PAD_ELD_0020,
 PAD_ELD_0021,
 PAD_ELD_0022,
 PAD_ELD_0023,
 PAD_ELD_0024,
 PAD_ELD_0025,
 PAD_ELD_0026,
 PAD_ELD_0027,
 PAD_ELD_0028,
 PAD_ELD_0029,
 PAD_ELD_002A,
 PAD_ELD_002B,
 PAD_ELD_002C,
 PAD_ELD_002D,
 PAD_ELD_002E,
 PAD_ELD_002F,
 PAD_ELD_0030,
 PAD_ELD_0031,
 PAD_ELD_0032,
 PAD_ELD_0033,
 PAD_ELD_0034,
 PAD_ELD_0035,
 PAD_ELD_0036,
 PAD_ELD_0037,
 PAD_ELD_0038,
 PAD_ELD_0039,
 PAD_ELD_003A,
 PAD_ELD_003B,
 PAD_ELD_003C,
 PAD_ELD_003D,
 PAD_ELD_003E,
 PAD_ELD_003F,
 PAD_ELD_0040,
 PAD_ELD_0041,
 PAD_ELD_0042,
 PAD_ELD_0043,
 PAD_ELD_0044,
 PAD_ELD_0045,
 PAD_ELD_0046,
 PAD_ELD_0047,
 PAD_ELD_0048,
 PAD_ELD_0049,
 PAD_ELD_004A,
 PAD_ELD_004B,
 PAD_ELD_004C,
 PAD_ELD_004D,
 PAD_ELD_004E,
 PAD_ELD_004F,
 PAD_ELD_0050,
 PAD_ELD_0051,
 PAD_ELD_0052,
 PAD_ELD_0053,
 PAD_ELD_0054,
 PAD_ELD_0055,
 PAD_ELD_0056,
 PAD_ELD_0057,
 PAD_ELD_0058,
 PAD_ELD_0059,
 PAD_ELD_005A,
 PAD_ELD_005B,
 PAD_ELD_005C,
 PAD_ELD_005D,
 PAD_ELD_005E,
 PAD_ELD_005F,
 PAD_ELD_0060,
 PAD_ELD_0061,
 PAD_ELD_0062,
 PAD_ELD_0063,
 PAD_ELD_0064,
 PAD_ELD_0065,
 PAD_ELD_0066,
 PAD_ELD_0067,
 PAD_ELD_0068,
 PAD_ELD_0069,
 PAD_ELD_006A,
 PAD_ELD_006B,
 PAD_ELD_006C,
 PAD_ELD_006D,
 PAD_ELD_006E,
 PAD_ELD_006F,
 PAD_ELD_0070,
 PAD_ELD_0071,
 PAD_ELD_0072,
 PAD_ELD_0073,
 PAD_ELD_0074,
 PAD_ELD_0075,
 PAD_ELD_0076,
 PAD_ELD_0077,
 PAD_ELD_0078,
 PAD_ELD_0079,
 PAD_ELD_007A,
 PAD_ELD_007B,
 PAD_ELD_007C,
 PAD_ELD_007D,
 PAD_ELD_007E,
 PAD_ELD_007F,
 PAD_ELD_0080,
 PAD_ELD_0081,
 PAD_ELD_0082,
 PAD_ELD_0083,
 PAD_ELD_0084,
 PAD_ELD_0085,
 PAD_ELD_0086,
 PAD_ELD_0087,
 PAD_ELD_0088,
 PAD_ELD_0089,
 PAD_ELD_008A,
 PAD_ELD_008B,
 PAD_ELD_008C,
 PAD_ELD_008D,
 PAD_ELD_008E,
 PAD_ELD_008F,
 PAD_ELD_0090,
 PAD_ELD_0091,
 PAD_ELD_0092,
 PAD_ELD_0093,
 PAD_ELD_0094,
 PAD_ELD_0095,
 PAD_ELD_0096,
 PAD_ELD_0097,
 PAD_ELD_0098,
 PAD_ELD_0099,
 PAD_ELD_009A,
 PAD_ELD_009B,
 PAD_ELD_009C,
 PAD_ELD_009D,
 PAD_ELD_009E,
 PAD_ELD_009F,
 PAD_ELD_00A0,
 PAD_ELD_00A1,
 PAD_ELD_00A2,
 PAD_ELD_00A3,
 PAD_ELD_00A4,
 PAD_ELD_00A5,
 PAD_ELD_00A6,
 PAD_ELD_00A7,
 PAD_ELD_00A8,
 PAD_ELD_00A9,
 PAD_ELD_00AA,
 PAD_ELD_00AB,
 PAD_ELD_00AC,
 PAD_ELD_00AD,
 PAD_ELD_00AE,
 PAD_ELD_00AF,
 PAD_ELD_00B0,
 PAD_ELD_00B1,
 PAD_ELD_00B2,
 PAD_ELD_00B3,
 PAD_ELD_00B4,
 PAD_ELD_00B5,
 PAD_ELD_00B6,
 PAD_ELD_00B7,
 PAD_ELD_00B8,
 PAD_ELD_00B9,
 PAD_ELD_00BA,
 PAD_ELD_00BB,
 PAD_ELD_00BC,
 PAD_ELD_00BD,
 PAD_ELD_00BE,
 PAD_ELD_00BF,
 PAD_ELD_00C0,
 PAD_ELD_00C1,
 PAD_ELD_00C2,
 PAD_ELD_00C3,
 PAD_ELD_00C4,
 PAD_ELD_00C5,
 PAD_ELD_00C6,
 PAD_ELD_00C7,
 PAD_ELD_00C8,
 PAD_ELD_00C9,
 PAD_ELD_00CA,
 PAD_ELD_00CB,
 PAD_ELD_00CC,
 PAD_ELD_00CD,
 PAD_ELD_00CE,
 PAD_ELD_00CF,
 PAD_ELD_00D0,
 PAD_ELD_00D1,
 PAD_ELD_00D2,
 PAD_ELD_00D3,
 PAD_ELD_00D4,
 PAD_ELD_00D5,
 PAD_ELD_00D6,
 PAD_ELD_00D7,
 PAD_ELD_00D8,
 PAD_ELD_00D9,
 PAD_ELD_00DA,
 PAD_ELD_00DB,
 PAD_ELD_00DC,
 PAD_ELD_00DD,
 PAD_ELD_00DE,
 PAD_ELD_00DF,
 PAD_ELD_00E0,
 PAD_ELD_00E1,
 PAD_ELD_00E2,
 PAD_ELD_00E3,
 PAD_ELD_00E4,
 PAD_ELD_00E5,
 PAD_ELD_00E6,
 PAD_ELD_00E7,
 PAD_ELD_00E8,
 PAD_ELD_00E9,
 PAD_ELD_00EA,
 PAD_ELD_00EB,
 PAD_ELD_00EC,
 PAD_ELD_00ED,
 PAD_ELD_00EE,
 PAD_ELD_00EF,
 PAD_ELD_00F0,
 PAD_ELD_00F1,
 PAD_ELD_00F2,
 PAD_ELD_00F3,
 PAD_ELD_00F4,
 PAD_ELD_00F5,
 PAD_ELD_00F6,
 PAD_ELD_00F7,
 PAD_ELD_00F8,
 PAD_ELD_00F9,
 PAD_ELD_00FA,
 PAD_ELD_00FB,
 PAD_ELD_00FC,
 PAD_ELD_00FD,
 PAD_ELD_00FE,
 PAD_ELD_00FF,
 PAD_ELD_0100,
 PAD_ELD_0101,
 PAD_ELD_0102,
 PAD_ELD_0103,
 PAD_ELD_0104,
 PAD_ELD_0105,
 PAD_ELD_0106,
 PAD_ELD_0107,
 PAD_ELD_0108,
 PAD_ELD_0109,
 PAD_ELD_010A,
 PAD_ELD_010B,
 PAD_ELD_010C,
 PAD_ELD_010D,
 PAD_ELD_010E,
 PAD_ELD_010F,
 PAD_ELD_0110,
 PAD_ELD_0111,
 PAD_ELD_0112,
 PAD_ELD_0113,
 PAD_ELD_0114,
 PAD_ELD_0115,
 PAD_ELD_0116,
 PAD_ELD_0117,
 PAD_ELD_0118,
 PAD_ELD_0119,
 PAD_ELD_011A,
 PAD_ELD_011B,
 PAD_ELD_011C,
 PAD_ELD_011D,
 PAD_ELD_011E,
 PAD_ELD_011F,
 PAD_ELD_0120,
 PAD_ELD_0121,
 PAD_ELD_0122,
 PAD_ELD_0123,
 PAD_ELD_0124,
 PAD_ELD_0125,
 PAD_ELD_0126,
 PAD_ELD_0127,
 PAD_ELD_0128,
 PAD_ELD_0129,
 PAD_ELD_012A,
 PAD_ELD_012B,
 PAD_ELD_012C,
 PAD_ELD_012D,
 PAD_ELD_012E,
 PAD_ELD_012F,
 PAD_ELD_0130,
 PAD_ELD_0131,
 PAD_ELD_0132,
 PAD_ELD_0133,
 PAD_ELD_0134,
 PAD_ELD_0135,
 PAD_ELD_0136,
 PAD_ELD_0137,
 PAD_ELD_0138,
 PAD_ELD_0139,
 PAD_ELD_013A,
 PAD_ELD_013B,
 PAD_ELD_013C,
 PAD_ELD_013D,
 PAD_ELD_013E,
 PAD_ELD_013F,
 PAD_ELD_0140,
 PAD_ELD_0141,
 PAD_ELD_0142,
 PAD_ELD_0143,
 PAD_ELD_0144,
 PAD_ELD_0145,
 PAD_ELD_0146,
 PAD_ELD_0147,
 PAD_ELD_0148,
 PAD_ELD_0149,
 PAD_ELD_014A,
 PAD_ELD_014B,
 PAD_ELD_014C,
 PAD_ELD_014D,
 PAD_ELD_014E,
 PAD_ELD_014F,
 PAD_ELD_0150,
 PAD_ELD_0151,
 PAD_ELD_0152,
 PAD_ELD_0153,
 PAD_ELD_0154,
 PAD_ELD_0155,
 PAD_ELD_0156,
 PAD_ELD_0157,
 PAD_ELD_0158,
 PAD_ELD_0159,
 PAD_ELD_015A,
 PAD_ELD_015B,
 PAD_ELD_015C,
 PAD_ELD_015D,
 PAD_ELD_015E,
 PAD_ELD_015F,
 PAD_ELD_0160,
 PAD_ELD_0161,
 PAD_ELD_0162,
 PAD_ELD_0163,
 PAD_ELD_0164,
 PAD_ELD_0165,
 PAD_ELD_0166,
 PAD_ELD_0167,
 PAD_ELD_0168,
 PAD_ELD_0169,
 PAD_ELD_016A,
 PAD_ELD_016B,
 PAD_ELD_016C,
 PAD_ELD_016D,
 PAD_ELD_016E,
 PAD_ELD_016F,
 PAD_ELD_0170,
 PAD_ELD_0171,
 PAD_ELD_0172,
 PAD_ELD_0173,
 PAD_ELD_0174,
 PAD_ELD_0175,
 PAD_ELD_0176,
 PAD_ELD_0177,
 PAD_ELD_0178,
 PAD_ELD_0179,
 PAD_ELD_017A,
 PAD_ELD_017B,
 PAD_ELD_017C,
 PAD_ELD_017D,
 PAD_ELD_017E,
 PAD_ELD_017F,
 PAD_ELD_0180,
 PAD_ELD_0181,
 PAD_ELD_0182,
 PAD_ELD_0183,
 PAD_ELD_0184,
 PAD_ELD_0185,
 PAD_ELD_0186,
 PAD_ELD_0187,
 PAD_ELD_0188,
 PAD_ELD_0189,
 PAD_ELD_018A,
 PAD_ELD_018B,
 PAD_ELD_018C,
 PAD_ELD_018D,
 PAD_ELD_018E,
 PAD_ELD_018F,
 PAD_ELD_0190,
 PAD_ELD_0191,
 PAD_ELD_0192,
 PAD_ELD_0193,
 PAD_ELD_0194,
 PAD_ELD_0195,
 PAD_ELD_0196,
 PAD_ELD_0197,
 PAD_ELD_0198,
 PAD_ELD_0199,
 PAD_ELD_019A,
 PAD_ELD_019B,
 PAD_ELD_019C,
 PAD_ELD_019D,
 PAD_ELD_019E,
 PAD_ELD_019F,
 PAD_ELD_01A0,
 PAD_ELD_01A1,
 PAD_ELD_01A2,
 PAD_ELD_01A3,
 PAD_ELD_01A4,
 PAD_ELD_01A5,
 PAD_ELD_01A6,
 PAD_ELD_01A7,
 PAD_ELD_01A8,
 PAD_ELD_01A9,
 PAD_ELD_01AA,
 PAD_ELD_01AB,
 PAD_ELD_01AC,
 PAD_ELD_01AD,
 PAD_ELD_01AE,
 PAD_ELD_01AF,
 PAD_ELD_01B0,
 PAD_ELD_01B1,
 PAD_ELD_01B2,
 PAD_ELD_01B3,
 PAD_ELD_01B4,
 PAD_ELD_01B5,
 PAD_ELD_01B6,
 PAD_ELD_01B7,
 PAD_ELD_01B8,
 PAD_ELD_01B9,
 PAD_ELD_01BA,
 PAD_ELD_01BB,
 PAD_ELD_01BC,
 PAD_ELD_01BD,
 PAD_ELD_01BE,
 PAD_ELD_01BF,
 PAD_ELD_01C0,
 PAD_ELD_01C1,
 PAD_ELD_01C2,
 PAD_ELD_01C3,
 PAD_ELD_01C4,
 PAD_ELD_01C5,
 PAD_ELD_01C6,
 PAD_ELD_01C7,
 PAD_ELD_01C8,
 PAD_ELD_01C9,
 PAD_ELD_01CA,
 PAD_ELD_01CB,
 PAD_ELD_01CC,
 PAD_ELD_01CD,
 PAD_ELD_01CE,
 PAD_ELD_01CF,
 PAD_ELD_01D0,
 PAD_ELD_01D1,
 PAD_ELD_01D2,
 PAD_ELD_01D3,
 PAD_ELD_01D4,
 PAD_ELD_01D5,
 PAD_ELD_01D6,
 PAD_ELD_01D7,
 PAD_ELD_01D8,
 PAD_ELD_01D9,
 PAD_ELD_01DA,
 PAD_ELD_01DB,
 PAD_ELD_01DC,
 PAD_ELD_01DD,
 PAD_ELD_01DE,
 PAD_ELD_01DF,
 PAD_ELD_01E0,
 PAD_ELD_01E1,
 PAD_ELD_01E2,
 PAD_ELD_01E3,
 PAD_ELD_01E4,
 PAD_ELD_01E5,
 PAD_ELD_01E6,
 PAD_ELD_01E7,
 PAD_ELD_01E8,
 PAD_ELD_01E9,
 PAD_ELD_01EA,
 PAD_ELD_01EB,
 PAD_ELD_01EC,
 PAD_ELD_01ED,
 PAD_ELD_01EE,
 PAD_ELD_01EF,
 PAD_ELD_01F0,
 PAD_ELD_01F1,
 PAD_ELD_01F2,
 PAD_ELD_01F3,
 PAD_ELD_01F4,
 PAD_ELD_01F5,
 PAD_ELD_01F6,
 PAD_ELD_01F7,
 PAD_ELD_01F8,
 PAD_ELD_01F9,
 PAD_ELD_01FA,
 PAD_ELD_01FB,
 PAD_ELD_01FC,
 PAD_ELD_01FD,
 PAD_ELD_01FE,
 PAD_ELD_01FF,
 PAD_ELD_0200,
 PAD_ELD_0201,
 PAD_ELD_0202,
 PAD_ELD_0203,
 PAD_ELD_0204,
 PAD_ELD_0205,
 PAD_ELD_0206,
 PAD_ELD_0207,
 PAD_ELD_0208,
 PAD_ELD_0209,
 PAD_ELD_020A,
 PAD_ELD_020B,
 PAD_ELD_020C,
 PAD_ELD_020D,
 PAD_ELD_020E,
 PAD_ELD_020F,
 PAD_ELD_0210,
 PAD_ELD_0211,
 PAD_ELD_0212,
 PAD_ELD_0213,
 PAD_ELD_0214,
 PAD_ELD_0215,
 PAD_ELD_0216,
 PAD_ELD_0217,
 PAD_ELD_0218,
 PAD_ELD_0219,
 PAD_ELD_021A,
 PAD_ELD_021B,
 PAD_ELD_021C,
 PAD_ELD_021D,
 PAD_ELD_021E,
 PAD_ELD_021F,
 PAD_ELD_0220,
 PAD_ELD_0221,
 PAD_ELD_0222,
 PAD_ELD_0223,
 PAD_ELD_0224,
 PAD_ELD_0225,
 PAD_ELD_0226,
 PAD_ELD_0227,
 PAD_ELD_0228,
 PAD_ELD_0229,
 PAD_ELD_022A,
 PAD_ELD_022B,
 PAD_ELD_022C,
 PAD_ELD_022D,
 PAD_ELD_022E,
 PAD_ELD_022F,
 PAD_ELD_0230,
 PAD_ELD_0231,
 PAD_ELD_0232,
 PAD_ELD_0233,
 PAD_ELD_0234,
 PAD_ELD_0235,
 PAD_ELD_0236,
 PAD_ELD_0237,
 PAD_ELD_0238,
 PAD_ELD_0239,
 PAD_ELD_END
};
enum pad_imp {
 PAD_IMP_0000,
 PAD_IMP_0001,
 PAD_IMP_0002,
 PAD_IMP_0003,
 PAD_IMP_0004,
 PAD_IMP_0005,
 PAD_IMP_0006,
 PAD_IMP_0007,
 PAD_IMP_0008,
 PAD_IMP_0009,
 PAD_IMP_000A,
 PAD_IMP_000B,
 PAD_IMP_000C,
 PAD_IMP_000D,
 PAD_IMP_000E,
 PAD_IMP_000F,
 PAD_IMP_0010,
 PAD_IMP_0011,
 PAD_IMP_0012,
 PAD_IMP_0013,
 PAD_IMP_0014,
 PAD_IMP_0015,
 PAD_IMP_0016,
 PAD_IMP_0017,
 PAD_IMP_0018,
 PAD_IMP_0019,
 PAD_IMP_001A,
 PAD_IMP_001B,
 PAD_IMP_001C,
 PAD_IMP_001D,
 PAD_IMP_001E,
 PAD_IMP_001F,
 PAD_IMP_0020,
 PAD_IMP_0021,
 PAD_IMP_0022,
 PAD_IMP_0023,
 PAD_IMP_0024,
 PAD_IMP_0025,
 PAD_IMP_0026,
 PAD_IMP_0027,
 PAD_IMP_0028,
 PAD_IMP_0029,
 PAD_IMP_002A,
 PAD_IMP_002B,
 PAD_IMP_002C,
 PAD_IMP_002D,
 PAD_IMP_002E,
 PAD_IMP_002F,
 PAD_IMP_0030,
 PAD_IMP_0031,
 PAD_IMP_0032,
 PAD_IMP_0033,
 PAD_IMP_0034,
 PAD_IMP_0035,
 PAD_IMP_0036,
 PAD_IMP_0037,
 PAD_IMP_0038,
 PAD_IMP_0039,
 PAD_IMP_003A,
 PAD_IMP_003B,
 PAD_IMP_003C,
 PAD_IMP_003D,
 PAD_IMP_003E,
 PAD_IMP_003F,
 PAD_IMP_0040,
 PAD_IMP_0041,
 PAD_IMP_0042,
 PAD_IMP_0043,
 PAD_IMP_0044,
 PAD_IMP_0045,
 PAD_IMP_0046,
 PAD_IMP_0047,
 PAD_IMP_0048,
 PAD_IMP_0049,
 PAD_IMP_004A,
 PAD_IMP_004B,
 PAD_IMP_004C,
 PAD_IMP_004D,
 PAD_IMP_004E,
 PAD_IMP_004F,
 PAD_IMP_0050,
 PAD_IMP_0051,
 PAD_IMP_0052,
 PAD_IMP_0053,
 PAD_IMP_0054,
 PAD_IMP_0055,
 PAD_IMP_0056,
 PAD_IMP_0057,
 PAD_IMP_0058,
 PAD_IMP_0059,
 PAD_IMP_005A,
 PAD_IMP_005B,
 PAD_IMP_005C,
 PAD_IMP_005D,
 PAD_IMP_005E,
 PAD_IMP_005F,
 PAD_IMP_0060,
 PAD_IMP_0061,
 PAD_IMP_0062,
 PAD_IMP_0063,
 PAD_IMP_0064,
 PAD_IMP_0065,
 PAD_IMP_0066,
 PAD_IMP_0067,
 PAD_IMP_0068,
 PAD_IMP_0069,
 PAD_IMP_006A,
 PAD_IMP_006B,
 PAD_IMP_006C,
 PAD_IMP_006D,
 PAD_IMP_006E,
 PAD_IMP_006F,
 PAD_IMP_0070,
 PAD_IMP_0071,
 PAD_IMP_0072,
 PAD_IMP_0073,
 PAD_IMP_0074,
 PAD_IMP_0075,
 PAD_IMP_0076,
 PAD_IMP_0077,
 PAD_IMP_0078,
 PAD_IMP_0079,
 PAD_IMP_007A,
 PAD_IMP_007B,
 PAD_IMP_007C,
 PAD_IMP_007D,
 PAD_IMP_007E,
 PAD_IMP_007F,
 PAD_IMP_0080,
 PAD_IMP_0081,
 PAD_IMP_0082,
 PAD_IMP_0083,
 PAD_IMP_0084,
 PAD_IMP_0085,
 PAD_IMP_0086,
 PAD_IMP_0087,
 PAD_IMP_0088,
 PAD_IMP_0089,
 PAD_IMP_008A,
 PAD_IMP_008B,
 PAD_IMP_008C,
 PAD_IMP_008D,
 PAD_IMP_008E,
 PAD_IMP_008F,
 PAD_IMP_0090,
 PAD_IMP_0091,
 PAD_IMP_0092,
 PAD_IMP_0093,
 PAD_IMP_0094,
 PAD_IMP_0095,
 PAD_IMP_0096,
 PAD_IMP_0097,
 PAD_IMP_0098,
 PAD_IMP_0099,
 PAD_IMP_009A,
 PAD_IMP_009B,
 PAD_IMP_009C,
 PAD_IMP_009D,
 PAD_IMP_009E,
 PAD_IMP_009F,
 PAD_IMP_00A0,
 PAD_IMP_00A1,
 PAD_IMP_00A2,
 PAD_IMP_00A3,
 PAD_IMP_00A4,
 PAD_IMP_00A5,
 PAD_IMP_00A6,
 PAD_IMP_00A7,
 PAD_IMP_00A8,
 PAD_IMP_00A9,
 PAD_IMP_00AA,
 PAD_IMP_00AB,
 PAD_IMP_00AC,
 PAD_IMP_00AD,
 PAD_IMP_00AE,
 PAD_IMP_00AF,
 PAD_IMP_00B0,
 PAD_IMP_00B1,
 PAD_IMP_00B2,
 PAD_IMP_00B3,
 PAD_IMP_00B4,
 PAD_IMP_00B5,
 PAD_IMP_00B6,
 PAD_IMP_00B7,
 PAD_IMP_00B8,
 PAD_IMP_00B9,
 PAD_IMP_00BA,
 PAD_IMP_00BB,
 PAD_IMP_00BC,
 PAD_IMP_00BD,
 PAD_IMP_00BE,
 PAD_IMP_00BF,
 PAD_IMP_00C0,
 PAD_IMP_00C1,
 PAD_IMP_00C2,
 PAD_IMP_00C3,
 PAD_IMP_00C4,
 PAD_IMP_00C5,
 PAD_IMP_00C6,
 PAD_IMP_00C7,
 PAD_IMP_00C8,
 PAD_IMP_00C9,
 PAD_IMP_00CA,
 PAD_IMP_00CB,
 PAD_IMP_00CC,
 PAD_IMP_00CD,
 PAD_IMP_00CE,
 PAD_IMP_00CF,
 PAD_IMP_00D0,
 PAD_IMP_00D1,
 PAD_IMP_00D2,
 PAD_IMP_00D3,
 PAD_IMP_00D4,
 PAD_IMP_00D5,
 PAD_IMP_00D6,
 PAD_IMP_00D7,
 PAD_IMP_00D8,
 PAD_IMP_00D9,
 PAD_IMP_00DA,
 PAD_IMP_00DB,
 PAD_IMP_00DC,
 PAD_IMP_00DD,
 PAD_IMP_00DE,
 PAD_IMP_00DF,
 PAD_IMP_00E0,
 PAD_IMP_00E1,
 PAD_IMP_00E2,
 PAD_IMP_00E3,
 PAD_IMP_00E4,
 PAD_IMP_00E5,
 PAD_IMP_00E6,
 PAD_IMP_00E7,
 PAD_IMP_00E8,
 PAD_IMP_00E9,
 PAD_IMP_00EA,
 PAD_IMP_00EB,
 PAD_IMP_00EC,
 PAD_IMP_00ED,
 PAD_IMP_00EE,
 PAD_IMP_00EF,
 PAD_IMP_00F0,
 PAD_IMP_00F1,
 PAD_IMP_00F2,
 PAD_IMP_00F3,
 PAD_IMP_00F4,
 PAD_IMP_00F5,
 PAD_IMP_00F6,
 PAD_IMP_00F7,
 PAD_IMP_00F8,
 PAD_IMP_00F9,
 PAD_IMP_00FA,
 PAD_IMP_00FB,
 PAD_IMP_00FC,
 PAD_IMP_00FD,
 PAD_IMP_00FE,
 PAD_IMP_00FF,
 PAD_IMP_0100,
 PAD_IMP_0101,
 PAD_IMP_0102,
 PAD_IMP_0103,
 PAD_IMP_0104,
 PAD_IMP_0105,
 PAD_IMP_0106,
 PAD_IMP_0107,
 PAD_IMP_0108,
 PAD_IMP_0109,
 PAD_IMP_010A,
 PAD_IMP_010B,
 PAD_IMP_010C,
 PAD_IMP_010D,
 PAD_IMP_010E,
 PAD_IMP_010F,
 PAD_IMP_0110,
 PAD_IMP_0111,
 PAD_IMP_0112,
 PAD_IMP_0113,
 PAD_IMP_0114,
 PAD_IMP_0115,
 PAD_IMP_0116,
 PAD_IMP_0117,
 PAD_IMP_0118,
 PAD_IMP_0119,
 PAD_IMP_011A,
 PAD_IMP_011B,
 PAD_IMP_011C,
 PAD_IMP_011D,
 PAD_IMP_011E,
 PAD_IMP_011F,
 PAD_IMP_0120,
 PAD_IMP_0121,
 PAD_IMP_0122,
 PAD_IMP_0123,
 PAD_IMP_0124,
 PAD_IMP_0125,
 PAD_IMP_0126,
 PAD_IMP_0127,
 PAD_IMP_0128,
 PAD_IMP_0129,
 PAD_IMP_012A,
 PAD_IMP_012B,
 PAD_IMP_012C,
 PAD_IMP_012D,
 PAD_IMP_012E,
 PAD_IMP_012F,
 PAD_IMP_0130,
 PAD_IMP_0131,
 PAD_IMP_0132,
 PAD_IMP_0133,
 PAD_IMP_0134,
 PAD_IMP_0135,
 PAD_IMP_0136,
 PAD_IMP_0137,
 PAD_IMP_0138,
 PAD_IMP_0139,
 PAD_IMP_013A,
 PAD_IMP_013B,
 PAD_IMP_013C,
 PAD_IMP_013D,
 PAD_IMP_013E,
 PAD_IMP_013F,
 PAD_IMP_0140,
 PAD_IMP_0141,
 PAD_IMP_0142,
 PAD_IMP_0143,
 PAD_IMP_0144,
 PAD_IMP_0145,
 PAD_IMP_0146,
 PAD_IMP_0147,
 PAD_IMP_0148,
 PAD_IMP_0149,
 PAD_IMP_014A,
 PAD_IMP_014B,
 PAD_IMP_014C,
 PAD_IMP_014D,
 PAD_IMP_014E,
 PAD_IMP_014F,
 PAD_IMP_0150,
 PAD_IMP_0151,
 PAD_IMP_0152,
 PAD_IMP_0153,
 PAD_IMP_0154,
 PAD_IMP_0155,
 PAD_IMP_0156,
 PAD_IMP_0157,
 PAD_IMP_0158,
 PAD_IMP_0159,
 PAD_IMP_015A,
 PAD_IMP_015B,
 PAD_IMP_015C,
 PAD_IMP_015D,
 PAD_IMP_015E,
 PAD_IMP_015F,
 PAD_IMP_0160,
 PAD_IMP_0161,
 PAD_IMP_0162,
 PAD_IMP_0163,
 PAD_IMP_0164,
 PAD_IMP_0165,
 PAD_IMP_0166,
 PAD_IMP_0167,
 PAD_IMP_0168,
 PAD_IMP_0169,
 PAD_IMP_016A,
 PAD_IMP_016B,
 PAD_IMP_016C,
 PAD_IMP_016D,
 PAD_IMP_016E,
 PAD_IMP_016F,
 PAD_IMP_0170,
 PAD_IMP_0171,
 PAD_IMP_0172,
 PAD_IMP_0173,
 PAD_IMP_0174,
 PAD_IMP_0175,
 PAD_IMP_0176,
 PAD_IMP_0177,
 PAD_IMP_0178,
 PAD_IMP_0179,
 PAD_IMP_017A,
 PAD_IMP_017B,
 PAD_IMP_017C,
 PAD_IMP_017D,
 PAD_IMP_017E,
 PAD_IMP_017F,
 PAD_IMP_0180,
 PAD_IMP_0181,
 PAD_IMP_0182,
 PAD_IMP_0183,
 PAD_IMP_0184,
 PAD_IMP_0185,
 PAD_IMP_0186,
 PAD_IMP_0187,
 PAD_IMP_0188,
 PAD_IMP_0189,
 PAD_IMP_018A,
 PAD_IMP_018B,
 PAD_IMP_018C,
 PAD_IMP_018D,
 PAD_IMP_018E,
 PAD_IMP_018F,
 PAD_IMP_0190,
 PAD_IMP_0191,
 PAD_IMP_0192,
 PAD_IMP_0193,
 PAD_IMP_0194,
 PAD_IMP_0195,
 PAD_IMP_0196,
 PAD_IMP_0197,
 PAD_IMP_0198,
 PAD_IMP_0199,
 PAD_IMP_019A,
 PAD_IMP_019B,
 PAD_IMP_019C,
 PAD_IMP_019D,
 PAD_IMP_019E,
 PAD_IMP_019F,
 PAD_IMP_01A0,
 PAD_IMP_01A1,
 PAD_IMP_01A2,
 PAD_IMP_01A3,
 PAD_IMP_01A4,
 PAD_IMP_01A5,
 PAD_IMP_01A6,
 PAD_IMP_01A7,
 PAD_IMP_01A8,
 PAD_IMP_01A9,
 PAD_IMP_01AA,
 PAD_IMP_01AB,
 PAD_IMP_01AC,
 PAD_IMP_01AD,
 PAD_IMP_01AE,
 PAD_IMP_01AF,
 PAD_IMP_01B0,
 PAD_IMP_01B1,
 PAD_IMP_01B2,
 PAD_IMP_01B3,
 PAD_IMP_01B4,
 PAD_IMP_01B5,
 PAD_IMP_01B6,
 PAD_IMP_01B7,
 PAD_IMP_01B8,
 PAD_IMP_01B9,
 PAD_IMP_01BA,
 PAD_IMP_01BB,
 PAD_IMP_01BC,
 PAD_IMP_01BD,
 PAD_IMP_01BE,
 PAD_IMP_01BF,
 PAD_IMP_01C0,
 PAD_IMP_01C1,
 PAD_IMP_01C2,
 PAD_IMP_01C3,
 PAD_IMP_01C4,
 PAD_IMP_01C5,
 PAD_IMP_01C6,
 PAD_IMP_01C7,
 PAD_IMP_01C8,
 PAD_IMP_01C9,
 PAD_IMP_01CA,
 PAD_IMP_01CB,
 PAD_IMP_01CC,
 PAD_IMP_01CD,
 PAD_IMP_01CE,
 PAD_IMP_01CF,
 PAD_IMP_01D0,
 PAD_IMP_01D1,
 PAD_IMP_01D2,
 PAD_IMP_01D3,
 PAD_IMP_01D4,
 PAD_IMP_01D5,
 PAD_IMP_01D6,
 PAD_IMP_01D7,
 PAD_IMP_01D8,
 PAD_IMP_01D9,
 PAD_IMP_01DA,
 PAD_IMP_01DB,
 PAD_IMP_01DC,
 PAD_IMP_01DD,
 PAD_IMP_01DE,
 PAD_IMP_01DF,
 PAD_IMP_01E0,
 PAD_IMP_01E1,
 PAD_IMP_01E2,
 PAD_IMP_01E3,
 PAD_IMP_01E4,
 PAD_IMP_01E5,
 PAD_IMP_01E6,
 PAD_IMP_01E7,
 PAD_IMP_01E8,
 PAD_IMP_01E9,
 PAD_IMP_01EA,
 PAD_IMP_01EB,
 PAD_IMP_01EC,
 PAD_IMP_01ED,
 PAD_IMP_01EE,
 PAD_IMP_01EF,
 PAD_IMP_01F0,
 PAD_IMP_01F1,
 PAD_IMP_01F2,
 PAD_IMP_01F3,
 PAD_IMP_01F4,
 PAD_IMP_01F5,
 PAD_IMP_01F6,
 PAD_IMP_01F7,
 PAD_IMP_01F8,
 PAD_IMP_01F9,
 PAD_IMP_01FA,
 PAD_IMP_01FB,
 PAD_IMP_01FC,
 PAD_IMP_01FD,
 PAD_IMP_01FE,
 PAD_IMP_01FF,
 PAD_IMP_0200,
 PAD_IMP_0201,
 PAD_IMP_0202,
 PAD_IMP_0203,
 PAD_IMP_0204,
 PAD_IMP_0205,
 PAD_IMP_0206,
 PAD_IMP_0207,
 PAD_IMP_0208,
 PAD_IMP_0209,
 PAD_IMP_020A,
 PAD_IMP_020B,
 PAD_IMP_020C,
 PAD_IMP_020D,
 PAD_IMP_020E,
 PAD_IMP_020F,
 PAD_IMP_0210,
 PAD_IMP_0211,
 PAD_IMP_0212,
 PAD_IMP_0213,
 PAD_IMP_0214,
 PAD_IMP_0215,
 PAD_IMP_0216,
 PAD_IMP_0217,
 PAD_IMP_0218,
 PAD_IMP_0219,
 PAD_IMP_021A,
 PAD_IMP_021B,
 PAD_IMP_021C,
 PAD_IMP_021D,
 PAD_IMP_021E,
 PAD_IMP_021F,
 PAD_IMP_0220,
 PAD_IMP_0221,
 PAD_IMP_0222,
 PAD_IMP_0223,
 PAD_IMP_0224,
 PAD_IMP_0225,
 PAD_IMP_0226,
 PAD_IMP_0227,
 PAD_IMP_0228,
 PAD_IMP_0229,
 PAD_IMP_022A,
 PAD_IMP_022B,
 PAD_IMP_022C,
 PAD_IMP_022D,
 PAD_IMP_022E,
 PAD_IMP_022F,
 PAD_IMP_0230,
 PAD_IMP_0231,
 PAD_IMP_0232,
 PAD_IMP_0233,
 PAD_IMP_0234,
 PAD_IMP_0235,
 PAD_IMP_0236,
 PAD_IMP_0237,
 PAD_IMP_0238,
 PAD_IMP_0239,
 PAD_IMP_023A,
 PAD_IMP_023B,
 PAD_IMP_023C,
 PAD_IMP_023D,
 PAD_IMP_023E,
 PAD_IMP_023F,
 PAD_IMP_0240,
 PAD_IMP_0241,
 PAD_IMP_0242,
 PAD_IMP_0243,
 PAD_IMP_0244,
 PAD_IMP_0245,
 PAD_IMP_0246,
 PAD_IMP_0247,
 PAD_IMP_0248,
 PAD_IMP_0249,
 PAD_IMP_024A,
 PAD_IMP_024B,
 PAD_IMP_024C,
 PAD_IMP_024D,
 PAD_IMP_024E,
 PAD_IMP_024F,
 PAD_IMP_0250,
 PAD_IMP_0251,
 PAD_IMP_0252,
 PAD_IMP_0253,
 PAD_IMP_0254,
 PAD_IMP_0255,
 PAD_IMP_0256,
 PAD_IMP_0257,
 PAD_IMP_0258,
 PAD_IMP_0259,
 PAD_IMP_025A,
 PAD_IMP_025B,
 PAD_IMP_025C,
 PAD_IMP_025D,
 PAD_IMP_025E,
 PAD_IMP_025F,
 PAD_IMP_0260,
 PAD_IMP_0261,
 PAD_IMP_0262,
 PAD_IMP_0263,
 PAD_IMP_0264,
 PAD_IMP_0265,
 PAD_IMP_0266,
 PAD_IMP_0267,
 PAD_IMP_0268,
 PAD_IMP_0269,
 PAD_IMP_026A,
 PAD_IMP_026B,
 PAD_IMP_026C,
 PAD_IMP_026D,
 PAD_IMP_026E,
 PAD_IMP_026F,
 PAD_IMP_0270,
 PAD_IMP_0271,
 PAD_IMP_0272,
 PAD_IMP_0273,
 PAD_IMP_0274,
 PAD_IMP_0275,
 PAD_IMP_0276,
 PAD_IMP_0277,
 PAD_IMP_0278,
 PAD_IMP_0279,
 PAD_IMP_027A,
 PAD_IMP_027B,
 PAD_IMP_027C,
 PAD_IMP_027D,
 PAD_IMP_027E,
 PAD_IMP_027F,
 PAD_IMP_0280,
 PAD_IMP_0281,
 PAD_IMP_0282,
 PAD_IMP_0283,
 PAD_IMP_0284,
 PAD_IMP_0285,
 PAD_IMP_0286,
 PAD_IMP_0287,
 PAD_IMP_0288,
 PAD_IMP_0289,
 PAD_IMP_END
};
enum pad_jun {
 PAD_JUN_0000,
 PAD_JUN_0001,
 PAD_JUN_0002,
 PAD_JUN_0003,
 PAD_JUN_0004,
 PAD_JUN_0005,
 PAD_JUN_0006,
 PAD_JUN_0007,
 PAD_JUN_0008,
 PAD_JUN_0009,
 PAD_JUN_000A,
 PAD_JUN_000B,
 PAD_JUN_000C,
 PAD_JUN_000D,
 PAD_JUN_000E,
 PAD_JUN_000F,
 PAD_JUN_0010,
 PAD_JUN_0011,
 PAD_JUN_0012,
 PAD_JUN_0013,
 PAD_JUN_0014,
 PAD_JUN_0015,
 PAD_JUN_0016,
 PAD_JUN_0017,
 PAD_JUN_0018,
 PAD_JUN_0019,
 PAD_JUN_001A,
 PAD_JUN_001B,
 PAD_JUN_001C,
 PAD_JUN_001D,
 PAD_JUN_001E,
 PAD_JUN_001F,
 PAD_JUN_0020,
 PAD_JUN_0021,
 PAD_JUN_0022,
 PAD_JUN_0023,
 PAD_JUN_0024,
 PAD_JUN_0025,
 PAD_JUN_0026,
 PAD_JUN_0027,
 PAD_JUN_0028,
 PAD_JUN_0029,
 PAD_JUN_002A,
 PAD_JUN_002B,
 PAD_JUN_002C,
 PAD_JUN_002D,
 PAD_JUN_002E,
 PAD_JUN_002F,
 PAD_JUN_0030,
 PAD_JUN_0031,
 PAD_JUN_0032,
 PAD_JUN_0033,
 PAD_JUN_0034,
 PAD_JUN_0035,
 PAD_JUN_0036,
 PAD_JUN_0037,
 PAD_JUN_0038,
 PAD_JUN_0039,
 PAD_JUN_003A,
 PAD_JUN_003B,
 PAD_JUN_003C,
 PAD_JUN_003D,
 PAD_JUN_003E,
 PAD_JUN_003F,
 PAD_JUN_0040,
 PAD_JUN_0041,
 PAD_JUN_0042,
 PAD_JUN_0043,
 PAD_JUN_0044,
 PAD_JUN_0045,
 PAD_JUN_0046,
 PAD_JUN_0047,
 PAD_JUN_0048,
 PAD_JUN_0049,
 PAD_JUN_004A,
 PAD_JUN_004B,
 PAD_JUN_004C,
 PAD_JUN_004D,
 PAD_JUN_004E,
 PAD_JUN_004F,
 PAD_JUN_0050,
 PAD_JUN_0051,
 PAD_JUN_0052,
 PAD_JUN_0053,
 PAD_JUN_0054,
 PAD_JUN_0055,
 PAD_JUN_0056,
 PAD_JUN_0057,
 PAD_JUN_0058,
 PAD_JUN_0059,
 PAD_JUN_005A,
 PAD_JUN_005B,
 PAD_JUN_005C,
 PAD_JUN_005D,
 PAD_JUN_005E,
 PAD_JUN_005F,
 PAD_JUN_0060,
 PAD_JUN_0061,
 PAD_JUN_0062,
 PAD_JUN_0063,
 PAD_JUN_0064,
 PAD_JUN_0065,
 PAD_JUN_0066,
 PAD_JUN_0067,
 PAD_JUN_0068,
 PAD_JUN_0069,
 PAD_JUN_006A,
 PAD_JUN_006B,
 PAD_JUN_006C,
 PAD_JUN_006D,
 PAD_JUN_006E,
 PAD_JUN_006F,
 PAD_JUN_0070,
 PAD_JUN_0071,
 PAD_JUN_0072,
 PAD_JUN_0073,
 PAD_JUN_0074,
 PAD_JUN_0075,
 PAD_JUN_0076,
 PAD_JUN_0077,
 PAD_JUN_0078,
 PAD_JUN_0079,
 PAD_JUN_007A,
 PAD_JUN_007B,
 PAD_JUN_007C,
 PAD_JUN_007D,
 PAD_JUN_007E,
 PAD_JUN_007F,
 PAD_JUN_0080,
 PAD_JUN_0081,
 PAD_JUN_0082,
 PAD_JUN_0083,
 PAD_JUN_0084,
 PAD_JUN_0085,
 PAD_JUN_0086,
 PAD_JUN_0087,
 PAD_JUN_0088,
 PAD_JUN_0089,
 PAD_JUN_008A,
 PAD_JUN_008B,
 PAD_JUN_008C,
 PAD_JUN_008D,
 PAD_JUN_008E,
 PAD_JUN_008F,
 PAD_JUN_0090,
 PAD_JUN_0091,
 PAD_JUN_0092,
 PAD_JUN_0093,
 PAD_JUN_0094,
 PAD_JUN_0095,
 PAD_JUN_0096,
 PAD_JUN_0097,
 PAD_JUN_0098,
 PAD_JUN_0099,
 PAD_JUN_009A,
 PAD_JUN_009B,
 PAD_JUN_009C,
 PAD_JUN_009D,
 PAD_JUN_009E,
 PAD_JUN_009F,
 PAD_JUN_00A0,
 PAD_JUN_00A1,
 PAD_JUN_00A2,
 PAD_JUN_00A3,
 PAD_JUN_00A4,
 PAD_JUN_00A5,
 PAD_JUN_00A6,
 PAD_JUN_00A7,
 PAD_JUN_00A8,
 PAD_JUN_00A9,
 PAD_JUN_00AA,
 PAD_JUN_00AB,
 PAD_JUN_00AC,
 PAD_JUN_00AD,
 PAD_JUN_00AE,
 PAD_JUN_00AF,
 PAD_JUN_00B0,
 PAD_JUN_00B1,
 PAD_JUN_00B2,
 PAD_JUN_00B3,
 PAD_JUN_00B4,
 PAD_JUN_00B5,
 PAD_JUN_00B6,
 PAD_JUN_00B7,
 PAD_JUN_00B8,
 PAD_JUN_00B9,
 PAD_JUN_00BA,
 PAD_JUN_00BB,
 PAD_JUN_00BC,
 PAD_JUN_00BD,
 PAD_JUN_00BE,
 PAD_JUN_00BF,
 PAD_JUN_END
};
enum pad_lee {
 PAD_LEE_0000,
 PAD_LEE_0001,
 PAD_LEE_0002,
 PAD_LEE_0003,
 PAD_LEE_0004,
 PAD_LEE_0005,
 PAD_LEE_0006,
 PAD_LEE_0007,
 PAD_LEE_0008,
 PAD_LEE_0009,
 PAD_LEE_000A,
 PAD_LEE_000B,
 PAD_LEE_000C,
 PAD_LEE_000D,
 PAD_LEE_000E,
 PAD_LEE_000F,
 PAD_LEE_0010,
 PAD_LEE_0011,
 PAD_LEE_0012,
 PAD_LEE_0013,
 PAD_LEE_0014,
 PAD_LEE_0015,
 PAD_LEE_0016,
 PAD_LEE_0017,
 PAD_LEE_0018,
 PAD_LEE_0019,
 PAD_LEE_001A,
 PAD_LEE_001B,
 PAD_LEE_001C,
 PAD_LEE_001D,
 PAD_LEE_001E,
 PAD_LEE_001F,
 PAD_LEE_0020,
 PAD_LEE_0021,
 PAD_LEE_0022,
 PAD_LEE_0023,
 PAD_LEE_0024,
 PAD_LEE_0025,
 PAD_LEE_0026,
 PAD_LEE_0027,
 PAD_LEE_0028,
 PAD_LEE_0029,
 PAD_LEE_002A,
 PAD_LEE_002B,
 PAD_LEE_002C,
 PAD_LEE_002D,
 PAD_LEE_002E,
 PAD_LEE_002F,
 PAD_LEE_0030,
 PAD_LEE_0031,
 PAD_LEE_0032,
 PAD_LEE_0033,
 PAD_LEE_0034,
 PAD_LEE_0035,
 PAD_LEE_0036,
 PAD_LEE_0037,
 PAD_LEE_0038,
 PAD_LEE_0039,
 PAD_LEE_003A,
 PAD_LEE_003B,
 PAD_LEE_003C,
 PAD_LEE_003D,
 PAD_LEE_003E,
 PAD_LEE_003F,
 PAD_LEE_0040,
 PAD_LEE_0041,
 PAD_LEE_0042,
 PAD_LEE_0043,
 PAD_LEE_0044,
 PAD_LEE_0045,
 PAD_LEE_0046,
 PAD_LEE_0047,
 PAD_LEE_0048,
 PAD_LEE_0049,
 PAD_LEE_004A,
 PAD_LEE_004B,
 PAD_LEE_004C,
 PAD_LEE_004D,
 PAD_LEE_004E,
 PAD_LEE_004F,
 PAD_LEE_0050,
 PAD_LEE_0051,
 PAD_LEE_0052,
 PAD_LEE_0053,
 PAD_LEE_0054,
 PAD_LEE_0055,
 PAD_LEE_0056,
 PAD_LEE_0057,
 PAD_LEE_0058,
 PAD_LEE_0059,
 PAD_LEE_005A,
 PAD_LEE_005B,
 PAD_LEE_005C,
 PAD_LEE_005D,
 PAD_LEE_005E,
 PAD_LEE_005F,
 PAD_LEE_0060,
 PAD_LEE_0061,
 PAD_LEE_0062,
 PAD_LEE_0063,
 PAD_LEE_0064,
 PAD_LEE_0065,
 PAD_LEE_0066,
 PAD_LEE_0067,
 PAD_LEE_0068,
 PAD_LEE_0069,
 PAD_LEE_006A,
 PAD_LEE_006B,
 PAD_LEE_006C,
 PAD_LEE_006D,
 PAD_LEE_006E,
 PAD_LEE_006F,
 PAD_LEE_0070,
 PAD_LEE_0071,
 PAD_LEE_0072,
 PAD_LEE_0073,
 PAD_LEE_0074,
 PAD_LEE_0075,
 PAD_LEE_0076,
 PAD_LEE_0077,
 PAD_LEE_0078,
 PAD_LEE_0079,
 PAD_LEE_007A,
 PAD_LEE_007B,
 PAD_LEE_007C,
 PAD_LEE_007D,
 PAD_LEE_007E,
 PAD_LEE_007F,
 PAD_LEE_0080,
 PAD_LEE_0081,
 PAD_LEE_0082,
 PAD_LEE_0083,
 PAD_LEE_0084,
 PAD_LEE_0085,
 PAD_LEE_0086,
 PAD_LEE_0087,
 PAD_LEE_0088,
 PAD_LEE_0089,
 PAD_LEE_008A,
 PAD_LEE_008B,
 PAD_LEE_008C,
 PAD_LEE_008D,
 PAD_LEE_008E,
 PAD_LEE_008F,
 PAD_LEE_0090,
 PAD_LEE_0091,
 PAD_LEE_0092,
 PAD_LEE_0093,
 PAD_LEE_0094,
 PAD_LEE_0095,
 PAD_LEE_0096,
 PAD_LEE_0097,
 PAD_LEE_0098,
 PAD_LEE_0099,
 PAD_LEE_009A,
 PAD_LEE_009B,
 PAD_LEE_009C,
 PAD_LEE_009D,
 PAD_LEE_009E,
 PAD_LEE_009F,
 PAD_LEE_00A0,
 PAD_LEE_00A1,
 PAD_LEE_00A2,
 PAD_LEE_00A3,
 PAD_LEE_00A4,
 PAD_LEE_00A5,
 PAD_LEE_00A6,
 PAD_LEE_00A7,
 PAD_LEE_00A8,
 PAD_LEE_00A9,
 PAD_LEE_00AA,
 PAD_LEE_00AB,
 PAD_LEE_00AC,
 PAD_LEE_00AD,
 PAD_LEE_00AE,
 PAD_LEE_00AF,
 PAD_LEE_00B0,
 PAD_LEE_00B1,
 PAD_LEE_00B2,
 PAD_LEE_00B3,
 PAD_LEE_00B4,
 PAD_LEE_00B5,
 PAD_LEE_00B6,
 PAD_LEE_00B7,
 PAD_LEE_00B8,
 PAD_LEE_00B9,
 PAD_LEE_00BA,
 PAD_LEE_00BB,
 PAD_LEE_00BC,
 PAD_LEE_00BD,
 PAD_LEE_00BE,
 PAD_LEE_00BF,
 PAD_LEE_00C0,
 PAD_LEE_00C1,
 PAD_LEE_00C2,
 PAD_LEE_00C3,
 PAD_LEE_00C4,
 PAD_LEE_00C5,
 PAD_LEE_00C6,
 PAD_LEE_00C7,
 PAD_LEE_00C8,
 PAD_LEE_00C9,
 PAD_LEE_00CA,
 PAD_LEE_00CB,
 PAD_LEE_00CC,
 PAD_LEE_00CD,
 PAD_LEE_00CE,
 PAD_LEE_00CF,
 PAD_LEE_00D0,
 PAD_LEE_00D1,
 PAD_LEE_00D2,
 PAD_LEE_00D3,
 PAD_LEE_00D4,
 PAD_LEE_00D5,
 PAD_LEE_00D6,
 PAD_LEE_00D7,
 PAD_LEE_00D8,
 PAD_LEE_00D9,
 PAD_LEE_00DA,
 PAD_LEE_00DB,
 PAD_LEE_00DC,
 PAD_LEE_00DD,
 PAD_LEE_00DE,
 PAD_LEE_00DF,
 PAD_LEE_00E0,
 PAD_LEE_00E1,
 PAD_LEE_00E2,
 PAD_LEE_00E3,
 PAD_LEE_00E4,
 PAD_LEE_00E5,
 PAD_LEE_00E6,
 PAD_LEE_00E7,
 PAD_LEE_00E8,
 PAD_LEE_00E9,
 PAD_LEE_00EA,
 PAD_LEE_00EB,
 PAD_LEE_00EC,
 PAD_LEE_00ED,
 PAD_LEE_00EE,
 PAD_LEE_00EF,
 PAD_LEE_00F0,
 PAD_LEE_00F1,
 PAD_LEE_00F2,
 PAD_LEE_00F3,
 PAD_LEE_00F4,
 PAD_LEE_00F5,
 PAD_LEE_00F6,
 PAD_LEE_00F7,
 PAD_LEE_00F8,
 PAD_LEE_00F9,
 PAD_LEE_00FA,
 PAD_LEE_00FB,
 PAD_LEE_00FC,
 PAD_LEE_00FD,
 PAD_LEE_00FE,
 PAD_LEE_00FF,
 PAD_LEE_0100,
 PAD_LEE_0101,
 PAD_LEE_0102,
 PAD_LEE_0103,
 PAD_LEE_0104,
 PAD_LEE_0105,
 PAD_LEE_0106,
 PAD_LEE_0107,
 PAD_LEE_0108,
 PAD_LEE_0109,
 PAD_LEE_010A,
 PAD_LEE_010B,
 PAD_LEE_010C,
 PAD_LEE_010D,
 PAD_LEE_010E,
 PAD_LEE_010F,
 PAD_LEE_0110,
 PAD_LEE_0111,
 PAD_LEE_0112,
 PAD_LEE_0113,
 PAD_LEE_0114,
 PAD_LEE_0115,
 PAD_LEE_0116,
 PAD_LEE_0117,
 PAD_LEE_0118,
 PAD_LEE_0119,
 PAD_LEE_011A,
 PAD_LEE_011B,
 PAD_LEE_011C,
 PAD_LEE_011D,
 PAD_LEE_011E,
 PAD_LEE_011F,
 PAD_LEE_0120,
 PAD_LEE_0121,
 PAD_LEE_0122,
 PAD_LEE_0123,
 PAD_LEE_0124,
 PAD_LEE_0125,
 PAD_LEE_0126,
 PAD_LEE_0127,
 PAD_LEE_0128,
 PAD_LEE_0129,
 PAD_LEE_012A,
 PAD_LEE_012B,
 PAD_LEE_012C,
 PAD_LEE_012D,
 PAD_LEE_012E,
 PAD_LEE_012F,
 PAD_LEE_0130,
 PAD_LEE_0131,
 PAD_LEE_0132,
 PAD_LEE_0133,
 PAD_LEE_0134,
 PAD_LEE_0135,
 PAD_LEE_0136,
 PAD_LEE_0137,
 PAD_LEE_0138,
 PAD_LEE_0139,
 PAD_LEE_013A,
 PAD_LEE_013B,
 PAD_LEE_013C,
 PAD_LEE_013D,
 PAD_LEE_013E,
 PAD_LEE_013F,
 PAD_LEE_0140,
 PAD_LEE_0141,
 PAD_LEE_0142,
 PAD_LEE_0143,
 PAD_LEE_0144,
 PAD_LEE_0145,
 PAD_LEE_0146,
 PAD_LEE_0147,
 PAD_LEE_0148,
 PAD_LEE_0149,
 PAD_LEE_014A,
 PAD_LEE_014B,
 PAD_LEE_014C,
 PAD_LEE_014D,
 PAD_LEE_014E,
 PAD_LEE_014F,
 PAD_LEE_0150,
 PAD_LEE_0151,
 PAD_LEE_0152,
 PAD_LEE_0153,
 PAD_LEE_0154,
 PAD_LEE_0155,
 PAD_LEE_0156,
 PAD_LEE_0157,
 PAD_LEE_0158,
 PAD_LEE_0159,
 PAD_LEE_015A,
 PAD_LEE_015B,
 PAD_LEE_015C,
 PAD_LEE_015D,
 PAD_LEE_015E,
 PAD_LEE_015F,
 PAD_LEE_0160,
 PAD_LEE_0161,
 PAD_LEE_0162,
 PAD_LEE_0163,
 PAD_LEE_0164,
 PAD_LEE_0165,
 PAD_LEE_0166,
 PAD_LEE_0167,
 PAD_LEE_0168,
 PAD_LEE_0169,
 PAD_LEE_016A,
 PAD_LEE_016B,
 PAD_LEE_016C,
 PAD_LEE_016D,
 PAD_LEE_016E,
 PAD_LEE_016F,
 PAD_LEE_0170,
 PAD_LEE_0171,
 PAD_LEE_0172,
 PAD_LEE_0173,
 PAD_LEE_0174,
 PAD_LEE_0175,
 PAD_LEE_0176,
 PAD_LEE_0177,
 PAD_LEE_0178,
 PAD_LEE_0179,
 PAD_LEE_017A,
 PAD_LEE_017B,
 PAD_LEE_017C,
 PAD_LEE_017D,
 PAD_LEE_017E,
 PAD_LEE_017F,
 PAD_LEE_0180,
 PAD_LEE_0181,
 PAD_LEE_0182,
 PAD_LEE_0183,
 PAD_LEE_0184,
 PAD_LEE_0185,
 PAD_LEE_0186,
 PAD_LEE_0187,
 PAD_LEE_0188,
 PAD_LEE_0189,
 PAD_LEE_018A,
 PAD_LEE_018B,
 PAD_LEE_018C,
 PAD_LEE_018D,
 PAD_LEE_018E,
 PAD_LEE_018F,
 PAD_LEE_0190,
 PAD_LEE_0191,
 PAD_LEE_0192,
 PAD_LEE_0193,
 PAD_LEE_0194,
 PAD_LEE_0195,
 PAD_LEE_0196,
 PAD_LEE_0197,
 PAD_LEE_0198,
 PAD_LEE_0199,
 PAD_LEE_019A,
 PAD_LEE_019B,
 PAD_LEE_019C,
 PAD_LEE_019D,
 PAD_LEE_019E,
 PAD_LEE_019F,
 PAD_LEE_01A0,
 PAD_LEE_01A1,
 PAD_LEE_01A2,
 PAD_LEE_01A3,
 PAD_LEE_01A4,
 PAD_LEE_01A5,
 PAD_LEE_01A6,
 PAD_LEE_01A7,
 PAD_LEE_01A8,
 PAD_LEE_01A9,
 PAD_LEE_01AA,
 PAD_LEE_01AB,
 PAD_LEE_01AC,
 PAD_LEE_01AD,
 PAD_LEE_01AE,
 PAD_LEE_01AF,
 PAD_LEE_01B0,
 PAD_LEE_01B1,
 PAD_LEE_01B2,
 PAD_LEE_01B3,
 PAD_LEE_01B4,
 PAD_LEE_01B5,
 PAD_LEE_01B6,
 PAD_LEE_01B7,
 PAD_LEE_01B8,
 PAD_LEE_01B9,
 PAD_LEE_01BA,
 PAD_LEE_01BB,
 PAD_LEE_01BC,
 PAD_LEE_01BD,
 PAD_LEE_01BE,
 PAD_LEE_01BF,
 PAD_LEE_01C0,
 PAD_LEE_01C1,
 PAD_LEE_01C2,
 PAD_LEE_01C3,
 PAD_LEE_01C4,
 PAD_LEE_01C5,
 PAD_LEE_01C6,
 PAD_LEE_01C7,
 PAD_LEE_01C8,
 PAD_LEE_01C9,
 PAD_LEE_01CA,
 PAD_LEE_01CB,
 PAD_LEE_01CC,
 PAD_LEE_01CD,
 PAD_LEE_01CE,
 PAD_LEE_01CF,
 PAD_LEE_01D0,
 PAD_LEE_01D1,
 PAD_LEE_01D2,
 PAD_LEE_01D3,
 PAD_LEE_01D4,
 PAD_LEE_01D5,
 PAD_LEE_01D6,
 PAD_LEE_01D7,
 PAD_LEE_01D8,
 PAD_LEE_01D9,
 PAD_LEE_01DA,
 PAD_LEE_01DB,
 PAD_LEE_01DC,
 PAD_LEE_01DD,
 PAD_LEE_01DE,
 PAD_LEE_01DF,
 PAD_LEE_01E0,
 PAD_LEE_01E1,
 PAD_LEE_01E2,
 PAD_LEE_01E3,
 PAD_LEE_01E4,
 PAD_LEE_01E5,
 PAD_LEE_01E6,
 PAD_LEE_01E7,
 PAD_LEE_01E8,
 PAD_LEE_01E9,
 PAD_LEE_01EA,
 PAD_LEE_01EB,
 PAD_LEE_01EC,
 PAD_LEE_01ED,
 PAD_LEE_01EE,
 PAD_LEE_01EF,
 PAD_LEE_01F0,
 PAD_LEE_01F1,
 PAD_LEE_01F2,
 PAD_LEE_01F3,
 PAD_LEE_01F4,
 PAD_LEE_01F5,
 PAD_LEE_END
};
enum pad_len {
 PAD_LEN_END
};
enum pad_lip {
 PAD_LIP_0000,
 PAD_LIP_0001,
 PAD_LIP_0002,
 PAD_LIP_0003,
 PAD_LIP_0004,
 PAD_LIP_0005,
 PAD_LIP_0006,
 PAD_LIP_0007,
 PAD_LIP_0008,
 PAD_LIP_0009,
 PAD_LIP_000A,
 PAD_LIP_000B,
 PAD_LIP_000C,
 PAD_LIP_000D,
 PAD_LIP_000E,
 PAD_LIP_000F,
 PAD_LIP_0010,
 PAD_LIP_0011,
 PAD_LIP_0012,
 PAD_LIP_0013,
 PAD_LIP_0014,
 PAD_LIP_0015,
 PAD_LIP_0016,
 PAD_LIP_0017,
 PAD_LIP_0018,
 PAD_LIP_0019,
 PAD_LIP_001A,
 PAD_LIP_001B,
 PAD_LIP_001C,
 PAD_LIP_001D,
 PAD_LIP_001E,
 PAD_LIP_001F,
 PAD_LIP_0020,
 PAD_LIP_0021,
 PAD_LIP_0022,
 PAD_LIP_0023,
 PAD_LIP_0024,
 PAD_LIP_0025,
 PAD_LIP_0026,
 PAD_LIP_0027,
 PAD_LIP_0028,
 PAD_LIP_0029,
 PAD_LIP_002A,
 PAD_LIP_002B,
 PAD_LIP_002C,
 PAD_LIP_002D,
 PAD_LIP_002E,
 PAD_LIP_002F,
 PAD_LIP_0030,
 PAD_LIP_0031,
 PAD_LIP_0032,
 PAD_LIP_0033,
 PAD_LIP_0034,
 PAD_LIP_0035,
 PAD_LIP_0036,
 PAD_LIP_0037,
 PAD_LIP_0038,
 PAD_LIP_0039,
 PAD_LIP_003A,
 PAD_LIP_003B,
 PAD_LIP_003C,
 PAD_LIP_003D,
 PAD_LIP_003E,
 PAD_LIP_003F,
 PAD_LIP_0040,
 PAD_LIP_0041,
 PAD_LIP_0042,
 PAD_LIP_0043,
 PAD_LIP_0044,
 PAD_LIP_0045,
 PAD_LIP_0046,
 PAD_LIP_0047,
 PAD_LIP_0048,
 PAD_LIP_0049,
 PAD_LIP_004A,
 PAD_LIP_004B,
 PAD_LIP_004C,
 PAD_LIP_004D,
 PAD_LIP_004E,
 PAD_LIP_004F,
 PAD_LIP_0050,
 PAD_LIP_0051,
 PAD_LIP_0052,
 PAD_LIP_0053,
 PAD_LIP_0054,
 PAD_LIP_0055,
 PAD_LIP_0056,
 PAD_LIP_0057,
 PAD_LIP_0058,
 PAD_LIP_0059,
 PAD_LIP_005A,
 PAD_LIP_005B,
 PAD_LIP_005C,
 PAD_LIP_005D,
 PAD_LIP_005E,
 PAD_LIP_005F,
 PAD_LIP_0060,
 PAD_LIP_0061,
 PAD_LIP_0062,
 PAD_LIP_0063,
 PAD_LIP_0064,
 PAD_LIP_0065,
 PAD_LIP_0066,
 PAD_LIP_0067,
 PAD_LIP_0068,
 PAD_LIP_0069,
 PAD_LIP_006A,
 PAD_LIP_006B,
 PAD_LIP_006C,
 PAD_LIP_006D,
 PAD_LIP_006E,
 PAD_LIP_006F,
 PAD_LIP_0070,
 PAD_LIP_0071,
 PAD_LIP_0072,
 PAD_LIP_0073,
 PAD_LIP_0074,
 PAD_LIP_0075,
 PAD_LIP_0076,
 PAD_LIP_0077,
 PAD_LIP_0078,
 PAD_LIP_0079,
 PAD_LIP_007A,
 PAD_LIP_007B,
 PAD_LIP_007C,
 PAD_LIP_007D,
 PAD_LIP_007E,
 PAD_LIP_007F,
 PAD_LIP_0080,
 PAD_LIP_0081,
 PAD_LIP_0082,
 PAD_LIP_0083,
 PAD_LIP_0084,
 PAD_LIP_0085,
 PAD_LIP_0086,
 PAD_LIP_0087,
 PAD_LIP_0088,
 PAD_LIP_0089,
 PAD_LIP_008A,
 PAD_LIP_008B,
 PAD_LIP_008C,
 PAD_LIP_008D,
 PAD_LIP_008E,
 PAD_LIP_008F,
 PAD_LIP_0090,
 PAD_LIP_0091,
 PAD_LIP_0092,
 PAD_LIP_0093,
 PAD_LIP_0094,
 PAD_LIP_0095,
 PAD_LIP_0096,
 PAD_LIP_0097,
 PAD_LIP_0098,
 PAD_LIP_0099,
 PAD_LIP_009A,
 PAD_LIP_009B,
 PAD_LIP_009C,
 PAD_LIP_009D,
 PAD_LIP_009E,
 PAD_LIP_009F,
 PAD_LIP_00A0,
 PAD_LIP_00A1,
 PAD_LIP_00A2,
 PAD_LIP_00A3,
 PAD_LIP_00A4,
 PAD_LIP_00A5,
 PAD_LIP_00A6,
 PAD_LIP_00A7,
 PAD_LIP_00A8,
 PAD_LIP_00A9,
 PAD_LIP_00AA,
 PAD_LIP_00AB,
 PAD_LIP_00AC,
 PAD_LIP_00AD,
 PAD_LIP_00AE,
 PAD_LIP_00AF,
 PAD_LIP_00B0,
 PAD_LIP_00B1,
 PAD_LIP_00B2,
 PAD_LIP_00B3,
 PAD_LIP_00B4,
 PAD_LIP_00B5,
 PAD_LIP_00B6,
 PAD_LIP_00B7,
 PAD_LIP_00B8,
 PAD_LIP_00B9,
 PAD_LIP_00BA,
 PAD_LIP_00BB,
 PAD_LIP_00BC,
 PAD_LIP_00BD,
 PAD_LIP_00BE,
 PAD_LIP_00BF,
 PAD_LIP_00C0,
 PAD_LIP_00C1,
 PAD_LIP_00C2,
 PAD_LIP_00C3,
 PAD_LIP_00C4,
 PAD_LIP_00C5,
 PAD_LIP_00C6,
 PAD_LIP_00C7,
 PAD_LIP_00C8,
 PAD_LIP_00C9,
 PAD_LIP_00CA,
 PAD_LIP_00CB,
 PAD_LIP_00CC,
 PAD_LIP_00CD,
 PAD_LIP_00CE,
 PAD_LIP_00CF,
 PAD_LIP_00D0,
 PAD_LIP_00D1,
 PAD_LIP_00D2,
 PAD_LIP_00D3,
 PAD_LIP_00D4,
 PAD_LIP_00D5,
 PAD_LIP_00D6,
 PAD_LIP_00D7,
 PAD_LIP_00D8,
 PAD_LIP_00D9,
 PAD_LIP_00DA,
 PAD_LIP_00DB,
 PAD_LIP_00DC,
 PAD_LIP_00DD,
 PAD_LIP_00DE,
 PAD_LIP_00DF,
 PAD_LIP_00E0,
 PAD_LIP_00E1,
 PAD_LIP_00E2,
 PAD_LIP_00E3,
 PAD_LIP_00E4,
 PAD_LIP_00E5,
 PAD_LIP_00E6,
 PAD_LIP_00E7,
 PAD_LIP_00E8,
 PAD_LIP_00E9,
 PAD_LIP_00EA,
 PAD_LIP_00EB,
 PAD_LIP_00EC,
 PAD_LIP_00ED,
 PAD_LIP_00EE,
 PAD_LIP_00EF,
 PAD_LIP_00F0,
 PAD_LIP_00F1,
 PAD_LIP_00F2,
 PAD_LIP_00F3,
 PAD_LIP_00F4,
 PAD_LIP_00F5,
 PAD_LIP_00F6,
 PAD_LIP_00F7,
 PAD_LIP_00F8,
 PAD_LIP_00F9,
 PAD_LIP_00FA,
 PAD_LIP_00FB,
 PAD_LIP_00FC,
 PAD_LIP_00FD,
 PAD_LIP_00FE,
 PAD_LIP_00FF,
 PAD_LIP_0100,
 PAD_LIP_0101,
 PAD_LIP_0102,
 PAD_LIP_0103,
 PAD_LIP_0104,
 PAD_LIP_0105,
 PAD_LIP_0106,
 PAD_LIP_0107,
 PAD_LIP_0108,
 PAD_LIP_0109,
 PAD_LIP_010A,
 PAD_LIP_010B,
 PAD_LIP_010C,
 PAD_LIP_010D,
 PAD_LIP_010E,
 PAD_LIP_010F,
 PAD_LIP_0110,
 PAD_LIP_0111,
 PAD_LIP_0112,
 PAD_LIP_0113,
 PAD_LIP_0114,
 PAD_LIP_0115,
 PAD_LIP_0116,
 PAD_LIP_0117,
 PAD_LIP_0118,
 PAD_LIP_0119,
 PAD_LIP_011A,
 PAD_LIP_011B,
 PAD_LIP_011C,
 PAD_LIP_011D,
 PAD_LIP_011E,
 PAD_LIP_011F,
 PAD_LIP_0120,
 PAD_LIP_0121,
 PAD_LIP_0122,
 PAD_LIP_0123,
 PAD_LIP_0124,
 PAD_LIP_0125,
 PAD_LIP_0126,
 PAD_LIP_0127,
 PAD_LIP_0128,
 PAD_LIP_0129,
 PAD_LIP_012A,
 PAD_LIP_012B,
 PAD_LIP_012C,
 PAD_LIP_012D,
 PAD_LIP_012E,
 PAD_LIP_012F,
 PAD_LIP_0130,
 PAD_LIP_0131,
 PAD_LIP_0132,
 PAD_LIP_0133,
 PAD_LIP_0134,
 PAD_LIP_0135,
 PAD_LIP_0136,
 PAD_LIP_0137,
 PAD_LIP_0138,
 PAD_LIP_0139,
 PAD_LIP_013A,
 PAD_LIP_013B,
 PAD_LIP_013C,
 PAD_LIP_013D,
 PAD_LIP_013E,
 PAD_LIP_013F,
 PAD_LIP_0140,
 PAD_LIP_0141,
 PAD_LIP_0142,
 PAD_LIP_0143,
 PAD_LIP_0144,
 PAD_LIP_0145,
 PAD_LIP_0146,
 PAD_LIP_0147,
 PAD_LIP_0148,
 PAD_LIP_0149,
 PAD_LIP_014A,
 PAD_LIP_014B,
 PAD_LIP_014C,
 PAD_LIP_014D,
 PAD_LIP_014E,
 PAD_LIP_014F,
 PAD_LIP_0150,
 PAD_LIP_0151,
 PAD_LIP_0152,
 PAD_LIP_0153,
 PAD_LIP_0154,
 PAD_LIP_0155,
 PAD_LIP_0156,
 PAD_LIP_0157,
 PAD_LIP_0158,
 PAD_LIP_0159,
 PAD_LIP_015A,
 PAD_LIP_015B,
 PAD_LIP_015C,
 PAD_LIP_015D,
 PAD_LIP_015E,
 PAD_LIP_015F,
 PAD_LIP_0160,
 PAD_LIP_0161,
 PAD_LIP_0162,
 PAD_LIP_0163,
 PAD_LIP_0164,
 PAD_LIP_0165,
 PAD_LIP_0166,
 PAD_LIP_0167,
 PAD_LIP_0168,
 PAD_LIP_0169,
 PAD_LIP_016A,
 PAD_LIP_016B,
 PAD_LIP_016C,
 PAD_LIP_016D,
 PAD_LIP_016E,
 PAD_LIP_016F,
 PAD_LIP_0170,
 PAD_LIP_0171,
 PAD_LIP_0172,
 PAD_LIP_0173,
 PAD_LIP_0174,
 PAD_LIP_0175,
 PAD_LIP_0176,
 PAD_LIP_0177,
 PAD_LIP_0178,
 PAD_LIP_0179,
 PAD_LIP_017A,
 PAD_LIP_017B,
 PAD_LIP_017C,
 PAD_LIP_017D,
 PAD_LIP_017E,
 PAD_LIP_017F,
 PAD_LIP_0180,
 PAD_LIP_0181,
 PAD_LIP_0182,
 PAD_LIP_0183,
 PAD_LIP_0184,
 PAD_LIP_0185,
 PAD_LIP_0186,
 PAD_LIP_0187,
 PAD_LIP_0188,
 PAD_LIP_0189,
 PAD_LIP_018A,
 PAD_LIP_018B,
 PAD_LIP_018C,
 PAD_LIP_018D,
 PAD_LIP_018E,
 PAD_LIP_018F,
 PAD_LIP_0190,
 PAD_LIP_0191,
 PAD_LIP_0192,
 PAD_LIP_0193,
 PAD_LIP_0194,
 PAD_LIP_0195,
 PAD_LIP_0196,
 PAD_LIP_0197,
 PAD_LIP_0198,
 PAD_LIP_0199,
 PAD_LIP_019A,
 PAD_LIP_019B,
 PAD_LIP_019C,
 PAD_LIP_019D,
 PAD_LIP_019E,
 PAD_LIP_019F,
 PAD_LIP_01A0,
 PAD_LIP_01A1,
 PAD_LIP_01A2,
 PAD_LIP_01A3,
 PAD_LIP_01A4,
 PAD_LIP_01A5,
 PAD_LIP_01A6,
 PAD_LIP_01A7,
 PAD_LIP_01A8,
 PAD_LIP_01A9,
 PAD_LIP_01AA,
 PAD_LIP_01AB,
 PAD_LIP_01AC,
 PAD_LIP_01AD,
 PAD_LIP_01AE,
 PAD_LIP_01AF,
 PAD_LIP_01B0,
 PAD_LIP_01B1,
 PAD_LIP_01B2,
 PAD_LIP_01B3,
 PAD_LIP_01B4,
 PAD_LIP_01B5,
 PAD_LIP_01B6,
 PAD_LIP_01B7,
 PAD_LIP_01B8,
 PAD_LIP_01B9,
 PAD_LIP_01BA,
 PAD_LIP_01BB,
 PAD_LIP_01BC,
 PAD_LIP_01BD,
 PAD_LIP_01BE,
 PAD_LIP_01BF,
 PAD_LIP_01C0,
 PAD_LIP_01C1,
 PAD_LIP_01C2,
 PAD_LIP_01C3,
 PAD_LIP_01C4,
 PAD_LIP_01C5,
 PAD_LIP_01C6,
 PAD_LIP_01C7,
 PAD_LIP_01C8,
 PAD_LIP_01C9,
 PAD_LIP_01CA,
 PAD_LIP_01CB,
 PAD_LIP_01CC,
 PAD_LIP_01CD,
 PAD_LIP_01CE,
 PAD_LIP_01CF,
 PAD_LIP_01D0,
 PAD_LIP_01D1,
 PAD_LIP_01D2,
 PAD_LIP_01D3,
 PAD_LIP_01D4,
 PAD_LIP_01D5,
 PAD_LIP_01D6,
 PAD_LIP_01D7,
 PAD_LIP_01D8,
 PAD_LIP_01D9,
 PAD_LIP_01DA,
 PAD_LIP_01DB,
 PAD_LIP_01DC,
 PAD_LIP_01DD,
 PAD_LIP_01DE,
 PAD_LIP_01DF,
 PAD_LIP_01E0,
 PAD_LIP_01E1,
 PAD_LIP_01E2,
 PAD_LIP_01E3,
 PAD_LIP_01E4,
 PAD_LIP_01E5,
 PAD_LIP_01E6,
 PAD_LIP_01E7,
 PAD_LIP_01E8,
 PAD_LIP_01E9,
 PAD_LIP_01EA,
 PAD_LIP_01EB,
 PAD_LIP_01EC,
 PAD_LIP_01ED,
 PAD_LIP_01EE,
 PAD_LIP_01EF,
 PAD_LIP_01F0,
 PAD_LIP_01F1,
 PAD_LIP_01F2,
 PAD_LIP_01F3,
 PAD_LIP_01F4,
 PAD_LIP_01F5,
 PAD_LIP_01F6,
 PAD_LIP_01F7,
 PAD_LIP_01F8,
 PAD_LIP_01F9,
 PAD_LIP_01FA,
 PAD_LIP_01FB,
 PAD_LIP_01FC,
 PAD_LIP_01FD,
 PAD_LIP_01FE,
 PAD_LIP_01FF,
 PAD_LIP_0200,
 PAD_LIP_0201,
 PAD_LIP_0202,
 PAD_LIP_0203,
 PAD_LIP_0204,
 PAD_LIP_0205,
 PAD_LIP_0206,
 PAD_LIP_0207,
 PAD_LIP_0208,
 PAD_LIP_0209,
 PAD_LIP_020A,
 PAD_LIP_020B,
 PAD_LIP_020C,
 PAD_LIP_020D,
 PAD_LIP_020E,
 PAD_LIP_020F,
 PAD_LIP_0210,
 PAD_LIP_0211,
 PAD_LIP_0212,
 PAD_LIP_0213,
 PAD_LIP_0214,
 PAD_LIP_0215,
 PAD_LIP_0216,
 PAD_LIP_0217,
 PAD_LIP_0218,
 PAD_LIP_0219,
 PAD_LIP_021A,
 PAD_LIP_021B,
 PAD_LIP_021C,
 PAD_LIP_021D,
 PAD_LIP_021E,
 PAD_LIP_021F,
 PAD_LIP_0220,
 PAD_LIP_0221,
 PAD_LIP_0222,
 PAD_LIP_0223,
 PAD_LIP_0224,
 PAD_LIP_0225,
 PAD_LIP_0226,
 PAD_LIP_0227,
 PAD_LIP_0228,
 PAD_LIP_0229,
 PAD_LIP_022A,
 PAD_LIP_022B,
 PAD_LIP_022C,
 PAD_LIP_022D,
 PAD_LIP_022E,
 PAD_LIP_022F,
 PAD_LIP_0230,
 PAD_LIP_0231,
 PAD_LIP_0232,
 PAD_LIP_0233,
 PAD_LIP_0234,
 PAD_LIP_0235,
 PAD_LIP_0236,
 PAD_LIP_0237,
 PAD_LIP_0238,
 PAD_LIP_0239,
 PAD_LIP_023A,
 PAD_LIP_023B,
 PAD_LIP_023C,
 PAD_LIP_023D,
 PAD_LIP_023E,
 PAD_LIP_023F,
 PAD_LIP_0240,
 PAD_LIP_0241,
 PAD_LIP_0242,
 PAD_LIP_0243,
 PAD_LIP_0244,
 PAD_LIP_0245,
 PAD_LIP_0246,
 PAD_LIP_0247,
 PAD_LIP_0248,
 PAD_LIP_0249,
 PAD_LIP_024A,
 PAD_LIP_024B,
 PAD_LIP_024C,
 PAD_LIP_024D,
 PAD_LIP_024E,
 PAD_LIP_024F,
 PAD_LIP_0250,
 PAD_LIP_0251,
 PAD_LIP_0252,
 PAD_LIP_0253,
 PAD_LIP_0254,
 PAD_LIP_0255,
 PAD_LIP_0256,
 PAD_LIP_0257,
 PAD_LIP_0258,
 PAD_LIP_0259,
 PAD_LIP_025A,
 PAD_LIP_025B,
 PAD_LIP_025C,
 PAD_LIP_025D,
 PAD_LIP_025E,
 PAD_LIP_025F,
 PAD_LIP_0260,
 PAD_LIP_0261,
 PAD_LIP_0262,
 PAD_LIP_0263,
 PAD_LIP_0264,
 PAD_LIP_0265,
 PAD_LIP_0266,
 PAD_LIP_0267,
 PAD_LIP_0268,
 PAD_LIP_0269,
 PAD_LIP_026A,
 PAD_LIP_026B,
 PAD_LIP_026C,
 PAD_LIP_026D,
 PAD_LIP_026E,
 PAD_LIP_026F,
 PAD_LIP_0270,
 PAD_LIP_0271,
 PAD_LIP_0272,
 PAD_LIP_0273,
 PAD_LIP_0274,
 PAD_LIP_0275,
 PAD_LIP_0276,
 PAD_LIP_0277,
 PAD_LIP_0278,
 PAD_LIP_0279,
 PAD_LIP_027A,
 PAD_LIP_027B,
 PAD_LIP_027C,
 PAD_LIP_027D,
 PAD_LIP_027E,
 PAD_LIP_027F,
 PAD_LIP_0280,
 PAD_LIP_0281,
 PAD_LIP_0282,
 PAD_LIP_0283,
 PAD_LIP_0284,
 PAD_LIP_0285,
 PAD_LIP_0286,
 PAD_LIP_0287,
 PAD_LIP_0288,
 PAD_LIP_0289,
 PAD_LIP_028A,
 PAD_LIP_028B,
 PAD_LIP_028C,
 PAD_LIP_028D,
 PAD_LIP_028E,
 PAD_LIP_028F,
 PAD_LIP_0290,
 PAD_LIP_0291,
 PAD_LIP_0292,
 PAD_LIP_0293,
 PAD_LIP_0294,
 PAD_LIP_0295,
 PAD_LIP_0296,
 PAD_LIP_0297,
 PAD_LIP_0298,
 PAD_LIP_0299,
 PAD_LIP_029A,
 PAD_LIP_029B,
 PAD_LIP_029C,
 PAD_LIP_029D,
 PAD_LIP_029E,
 PAD_LIP_029F,
 PAD_LIP_02A0,
 PAD_LIP_02A1,
 PAD_LIP_02A2,
 PAD_LIP_02A3,
 PAD_LIP_02A4,
 PAD_LIP_02A5,
 PAD_LIP_02A6,
 PAD_LIP_02A7,
 PAD_LIP_02A8,
 PAD_LIP_02A9,
 PAD_LIP_02AA,
 PAD_LIP_02AB,
 PAD_LIP_02AC,
 PAD_LIP_02AD,
 PAD_LIP_02AE,
 PAD_LIP_02AF,
 PAD_LIP_02B0,
 PAD_LIP_02B1,
 PAD_LIP_02B2,
 PAD_LIP_02B3,
 PAD_LIP_02B4,
 PAD_LIP_02B5,
 PAD_LIP_02B6,
 PAD_LIP_02B7,
 PAD_LIP_02B8,
 PAD_LIP_02B9,
 PAD_LIP_02BA,
 PAD_LIP_02BB,
 PAD_LIP_02BC,
 PAD_LIP_02BD,
 PAD_LIP_02BE,
 PAD_LIP_02BF,
 PAD_LIP_02C0,
 PAD_LIP_02C1,
 PAD_LIP_02C2,
 PAD_LIP_02C3,
 PAD_LIP_02C4,
 PAD_LIP_02C5,
 PAD_LIP_02C6,
 PAD_LIP_02C7,
 PAD_LIP_02C8,
 PAD_LIP_02C9,
 PAD_LIP_02CA,
 PAD_LIP_02CB,
 PAD_LIP_02CC,
 PAD_LIP_02CD,
 PAD_LIP_02CE,
 PAD_LIP_02CF,
 PAD_LIP_02D0,
 PAD_LIP_02D1,
 PAD_LIP_02D2,
 PAD_LIP_02D3,
 PAD_LIP_02D4,
 PAD_LIP_02D5,
 PAD_LIP_02D6,
 PAD_LIP_02D7,
 PAD_LIP_02D8,
 PAD_LIP_02D9,
 PAD_LIP_02DA,
 PAD_LIP_02DB,
 PAD_LIP_02DC,
 PAD_LIP_02DD,
 PAD_LIP_02DE,
 PAD_LIP_02DF,
 PAD_LIP_02E0,
 PAD_LIP_02E1,
 PAD_LIP_02E2,
 PAD_LIP_02E3,
 PAD_LIP_02E4,
 PAD_LIP_02E5,
 PAD_LIP_02E6,
 PAD_LIP_02E7,
 PAD_LIP_02E8,
 PAD_LIP_02E9,
 PAD_LIP_02EA,
 PAD_LIP_02EB,
 PAD_LIP_02EC,
 PAD_LIP_02ED,
 PAD_LIP_02EE,
 PAD_LIP_02EF,
 PAD_LIP_02F0,
 PAD_LIP_02F1,
 PAD_LIP_02F2,
 PAD_LIP_02F3,
 PAD_LIP_02F4,
 PAD_LIP_02F5,
 PAD_LIP_02F6,
 PAD_LIP_02F7,
 PAD_LIP_02F8,
 PAD_LIP_02F9,
 PAD_LIP_02FA,
 PAD_LIP_02FB,
 PAD_LIP_02FC,
 PAD_LIP_02FD,
 PAD_LIP_02FE,
 PAD_LIP_02FF,
 PAD_LIP_0300,
 PAD_LIP_0301,
 PAD_LIP_0302,
 PAD_LIP_0303,
 PAD_LIP_0304,
 PAD_LIP_0305,
 PAD_LIP_0306,
 PAD_LIP_0307,
 PAD_LIP_0308,
 PAD_LIP_0309,
 PAD_LIP_030A,
 PAD_LIP_030B,
 PAD_LIP_030C,
 PAD_LIP_030D,
 PAD_LIP_030E,
 PAD_LIP_030F,
 PAD_LIP_0310,
 PAD_LIP_0311,
 PAD_LIP_0312,
 PAD_LIP_0313,
 PAD_LIP_0314,
 PAD_LIP_0315,
 PAD_LIP_0316,
 PAD_LIP_0317,
 PAD_LIP_0318,
 PAD_LIP_0319,
 PAD_LIP_031A,
 PAD_LIP_031B,
 PAD_LIP_031C,
 PAD_LIP_031D,
 PAD_LIP_031E,
 PAD_LIP_031F,
 PAD_LIP_0320,
 PAD_LIP_0321,
 PAD_LIP_0322,
 PAD_LIP_0323,
 PAD_LIP_0324,
 PAD_LIP_0325,
 PAD_LIP_0326,
 PAD_LIP_0327,
 PAD_LIP_0328,
 PAD_LIP_0329,
 PAD_LIP_032A,
 PAD_LIP_032B,
 PAD_LIP_032C,
 PAD_LIP_032D,
 PAD_LIP_032E,
 PAD_LIP_032F,
 PAD_LIP_0330,
 PAD_LIP_0331,
 PAD_LIP_0332,
 PAD_LIP_0333,
 PAD_LIP_0334,
 PAD_LIP_0335,
 PAD_LIP_0336,
 PAD_LIP_0337,
 PAD_LIP_0338,
 PAD_LIP_0339,
 PAD_LIP_033A,
 PAD_LIP_033B,
 PAD_LIP_033C,
 PAD_LIP_033D,
 PAD_LIP_033E,
 PAD_LIP_033F,
 PAD_LIP_0340,
 PAD_LIP_0341,
 PAD_LIP_0342,
 PAD_LIP_0343,
 PAD_LIP_0344,
 PAD_LIP_0345,
 PAD_LIP_0346,
 PAD_LIP_0347,
 PAD_LIP_0348,
 PAD_LIP_0349,
 PAD_LIP_034A,
 PAD_LIP_034B,
 PAD_LIP_034C,
 PAD_LIP_034D,
 PAD_LIP_034E,
 PAD_LIP_034F,
 PAD_LIP_0350,
 PAD_LIP_0351,
 PAD_LIP_0352,
 PAD_LIP_0353,
 PAD_LIP_0354,
 PAD_LIP_0355,
 PAD_LIP_0356,
 PAD_LIP_0357,
 PAD_LIP_0358,
 PAD_LIP_0359,
 PAD_LIP_035A,
 PAD_LIP_035B,
 PAD_LIP_035C,
 PAD_LIP_035D,
 PAD_LIP_035E,
 PAD_LIP_035F,
 PAD_LIP_0360,
 PAD_LIP_0361,
 PAD_LIP_0362,
 PAD_LIP_0363,
 PAD_LIP_0364,
 PAD_LIP_0365,
 PAD_LIP_0366,
 PAD_LIP_0367,
 PAD_LIP_0368,
 PAD_LIP_0369,
 PAD_LIP_036A,
 PAD_LIP_036B,
 PAD_LIP_036C,
 PAD_LIP_036D,
 PAD_LIP_036E,
 PAD_LIP_036F,
 PAD_LIP_0370,
 PAD_LIP_0371,
 PAD_LIP_0372,
 PAD_LIP_0373,
 PAD_LIP_0374,
 PAD_LIP_0375,
 PAD_LIP_0376,
 PAD_LIP_0377,
 PAD_LIP_0378,
 PAD_LIP_0379,
 PAD_LIP_037A,
 PAD_LIP_037B,
 PAD_LIP_037C,
 PAD_LIP_037D,
 PAD_LIP_037E,
 PAD_LIP_037F,
 PAD_LIP_0380,
 PAD_LIP_0381,
 PAD_LIP_0382,
 PAD_LIP_0383,
 PAD_LIP_0384,
 PAD_LIP_0385,
 PAD_LIP_0386,
 PAD_LIP_0387,
 PAD_LIP_0388,
 PAD_LIP_0389,
 PAD_LIP_038A,
 PAD_LIP_038B,
 PAD_LIP_038C,
 PAD_LIP_038D,
 PAD_LIP_038E,
 PAD_LIP_038F,
 PAD_LIP_0390,
 PAD_LIP_0391,
 PAD_LIP_0392,
 PAD_LIP_0393,
 PAD_LIP_0394,
 PAD_LIP_0395,
 PAD_LIP_0396,
 PAD_LIP_0397,
 PAD_LIP_0398,
 PAD_LIP_0399,
 PAD_LIP_039A,
 PAD_LIP_039B,
 PAD_LIP_039C,
 PAD_LIP_039D,
 PAD_LIP_039E,
 PAD_LIP_039F,
 PAD_LIP_03A0,
 PAD_LIP_03A1,
 PAD_LIP_03A2,
 PAD_LIP_03A3,
 PAD_LIP_03A4,
 PAD_LIP_03A5,
 PAD_LIP_03A6,
 PAD_LIP_03A7,
 PAD_LIP_03A8,
 PAD_LIP_03A9,
 PAD_LIP_03AA,
 PAD_LIP_03AB,
 PAD_LIP_03AC,
 PAD_LIP_03AD,
 PAD_LIP_03AE,
 PAD_LIP_03AF,
 PAD_LIP_03B0,
 PAD_LIP_03B1,
 PAD_LIP_03B2,
 PAD_LIP_03B3,
 PAD_LIP_03B4,
 PAD_LIP_03B5,
 PAD_LIP_03B6,
 PAD_LIP_03B7,
 PAD_LIP_03B8,
 PAD_LIP_03B9,
 PAD_LIP_03BA,
 PAD_LIP_03BB,
 PAD_LIP_03BC,
 PAD_LIP_03BD,
 PAD_LIP_03BE,
 PAD_LIP_03BF,
 PAD_LIP_03C0,
 PAD_LIP_03C1,
 PAD_LIP_03C2,
 PAD_LIP_03C3,
 PAD_LIP_03C4,
 PAD_LIP_03C5,
 PAD_LIP_03C6,
 PAD_LIP_03C7,
 PAD_LIP_03C8,
 PAD_LIP_03C9,
 PAD_LIP_03CA,
 PAD_LIP_03CB,
 PAD_LIP_03CC,
 PAD_LIP_03CD,
 PAD_LIP_03CE,
 PAD_LIP_03CF,
 PAD_LIP_03D0,
 PAD_LIP_03D1,
 PAD_LIP_03D2,
 PAD_LIP_03D3,
 PAD_LIP_03D4,
 PAD_LIP_03D5,
 PAD_LIP_03D6,
 PAD_LIP_03D7,
 PAD_LIP_03D8,
 PAD_LIP_03D9,
 PAD_LIP_03DA,
 PAD_LIP_03DB,
 PAD_LIP_03DC,
 PAD_LIP_03DD,
 PAD_LIP_03DE,
 PAD_LIP_03DF,
 PAD_LIP_03E0,
 PAD_LIP_03E1,
 PAD_LIP_03E2,
 PAD_LIP_03E3,
 PAD_LIP_03E4,
 PAD_LIP_03E5,
 PAD_LIP_03E6,
 PAD_LIP_03E7,
 PAD_LIP_03E8,
 PAD_LIP_03E9,
 PAD_LIP_03EA,
 PAD_LIP_03EB,
 PAD_LIP_03EC,
 PAD_LIP_03ED,
 PAD_LIP_03EE,
 PAD_LIP_03EF,
 PAD_LIP_03F0,
 PAD_LIP_03F1,
 PAD_LIP_03F2,
 PAD_LIP_03F3,
 PAD_LIP_03F4,
 PAD_LIP_03F5,
 PAD_LIP_03F6,
 PAD_LIP_03F7,
 PAD_LIP_03F8,
 PAD_LIP_03F9,
 PAD_LIP_03FA,
 PAD_LIP_03FB,
 PAD_LIP_03FC,
 PAD_LIP_03FD,
 PAD_LIP_03FE,
 PAD_LIP_03FF,
 PAD_LIP_0400,
 PAD_LIP_0401,
 PAD_LIP_0402,
 PAD_LIP_0403,
 PAD_LIP_0404,
 PAD_LIP_0405,
 PAD_LIP_0406,
 PAD_LIP_0407,
 PAD_LIP_0408,
 PAD_LIP_0409,
 PAD_LIP_040A,
 PAD_LIP_040B,
 PAD_LIP_040C,
 PAD_LIP_040D,
 PAD_LIP_040E,
 PAD_LIP_040F,
 PAD_LIP_0410,
 PAD_LIP_0411,
 PAD_LIP_0412,
 PAD_LIP_0413,
 PAD_LIP_0414,
 PAD_LIP_0415,
 PAD_LIP_0416,
 PAD_LIP_0417,
 PAD_LIP_0418,
 PAD_LIP_0419,
 PAD_LIP_041A,
 PAD_LIP_041B,
 PAD_LIP_041C,
 PAD_LIP_041D,
 PAD_LIP_041E,
 PAD_LIP_041F,
 PAD_LIP_0420,
 PAD_LIP_0421,
 PAD_LIP_0422,
 PAD_LIP_0423,
 PAD_LIP_0424,
 PAD_LIP_0425,
 PAD_LIP_0426,
 PAD_LIP_0427,
 PAD_LIP_0428,
 PAD_LIP_0429,
 PAD_LIP_042A,
 PAD_LIP_042B,
 PAD_LIP_042C,
 PAD_LIP_042D,
 PAD_LIP_042E,
 PAD_LIP_042F,
 PAD_LIP_0430,
 PAD_LIP_0431,
 PAD_LIP_0432,
 PAD_LIP_0433,
 PAD_LIP_0434,
 PAD_LIP_0435,
 PAD_LIP_0436,
 PAD_LIP_0437,
 PAD_LIP_0438,
 PAD_LIP_0439,
 PAD_LIP_043A,
 PAD_LIP_043B,
 PAD_LIP_043C,
 PAD_LIP_043D,
 PAD_LIP_043E,
 PAD_LIP_043F,
 PAD_LIP_0440,
 PAD_LIP_0441,
 PAD_LIP_0442,
 PAD_LIP_0443,
 PAD_LIP_0444,
 PAD_LIP_0445,
 PAD_LIP_0446,
 PAD_LIP_0447,
 PAD_LIP_0448,
 PAD_LIP_0449,
 PAD_LIP_044A,
 PAD_LIP_044B,
 PAD_LIP_044C,
 PAD_LIP_044D,
 PAD_LIP_044E,
 PAD_LIP_044F,
 PAD_LIP_0450,
 PAD_LIP_0451,
 PAD_LIP_0452,
 PAD_LIP_0453,
 PAD_LIP_0454,
 PAD_LIP_0455,
 PAD_LIP_0456,
 PAD_LIP_0457,
 PAD_LIP_0458,
 PAD_LIP_0459,
 PAD_LIP_045A,
 PAD_LIP_045B,
 PAD_LIP_045C,
 PAD_LIP_045D,
 PAD_LIP_045E,
 PAD_LIP_045F,
 PAD_LIP_0460,
 PAD_LIP_0461,
 PAD_LIP_0462,
 PAD_LIP_0463,
 PAD_LIP_0464,
 PAD_LIP_0465,
 PAD_LIP_0466,
 PAD_LIP_0467,
 PAD_LIP_0468,
 PAD_LIP_0469,
 PAD_LIP_046A,
 PAD_LIP_046B,
 PAD_LIP_046C,
 PAD_LIP_046D,
 PAD_LIP_046E,
 PAD_LIP_046F,
 PAD_LIP_0470,
 PAD_LIP_0471,
 PAD_LIP_0472,
 PAD_LIP_0473,
 PAD_LIP_0474,
 PAD_LIP_0475,
 PAD_LIP_0476,
 PAD_LIP_0477,
 PAD_LIP_0478,
 PAD_LIP_0479,
 PAD_LIP_047A,
 PAD_LIP_047B,
 PAD_LIP_047C,
 PAD_LIP_047D,
 PAD_LIP_047E,
 PAD_LIP_047F,
 PAD_LIP_0480,
 PAD_LIP_0481,
 PAD_LIP_0482,
 PAD_LIP_0483,
 PAD_LIP_0484,
 PAD_LIP_0485,
 PAD_LIP_0486,
 PAD_LIP_0487,
 PAD_LIP_0488,
 PAD_LIP_0489,
 PAD_LIP_048A,
 PAD_LIP_048B,
 PAD_LIP_048C,
 PAD_LIP_048D,
 PAD_LIP_048E,
 PAD_LIP_048F,
 PAD_LIP_0490,
 PAD_LIP_0491,
 PAD_LIP_0492,
 PAD_LIP_0493,
 PAD_LIP_0494,
 PAD_LIP_0495,
 PAD_LIP_0496,
 PAD_LIP_0497,
 PAD_LIP_0498,
 PAD_LIP_0499,
 PAD_LIP_049A,
 PAD_LIP_049B,
 PAD_LIP_049C,
 PAD_LIP_049D,
 PAD_LIP_049E,
 PAD_LIP_049F,
 PAD_LIP_04A0,
 PAD_LIP_04A1,
 PAD_LIP_04A2,
 PAD_LIP_04A3,
 PAD_LIP_04A4,
 PAD_LIP_04A5,
 PAD_LIP_04A6,
 PAD_LIP_04A7,
 PAD_LIP_04A8,
 PAD_LIP_04A9,
 PAD_LIP_04AA,
 PAD_LIP_04AB,
 PAD_LIP_04AC,
 PAD_LIP_04AD,
 PAD_LIP_04AE,
 PAD_LIP_04AF,
 PAD_LIP_04B0,
 PAD_LIP_04B1,
 PAD_LIP_04B2,
 PAD_LIP_04B3,
 PAD_LIP_04B4,
 PAD_LIP_04B5,
 PAD_LIP_04B6,
 PAD_LIP_04B7,
 PAD_LIP_04B8,
 PAD_LIP_04B9,
 PAD_LIP_04BA,
 PAD_LIP_04BB,
 PAD_LIP_04BC,
 PAD_LIP_04BD,
 PAD_LIP_04BE,
 PAD_LIP_04BF,
 PAD_LIP_04C0,
 PAD_LIP_04C1,
 PAD_LIP_04C2,
 PAD_LIP_04C3,
 PAD_LIP_04C4,
 PAD_LIP_04C5,
 PAD_LIP_04C6,
 PAD_LIP_04C7,
 PAD_LIP_04C8,
 PAD_LIP_04C9,
 PAD_LIP_04CA,
 PAD_LIP_04CB,
 PAD_LIP_04CC,
 PAD_LIP_04CD,
 PAD_LIP_04CE,
 PAD_LIP_04CF,
 PAD_LIP_04D0,
 PAD_LIP_04D1,
 PAD_LIP_04D2,
 PAD_LIP_04D3,
 PAD_LIP_04D4,
 PAD_LIP_04D5,
 PAD_LIP_04D6,
 PAD_LIP_04D7,
 PAD_LIP_04D8,
 PAD_LIP_04D9,
 PAD_LIP_04DA,
 PAD_LIP_04DB,
 PAD_LIP_04DC,
 PAD_LIP_04DD,
 PAD_LIP_04DE,
 PAD_LIP_04DF,
 PAD_LIP_04E0,
 PAD_LIP_04E1,
 PAD_LIP_04E2,
 PAD_LIP_04E3,
 PAD_LIP_04E4,
 PAD_LIP_04E5,
 PAD_LIP_04E6,
 PAD_LIP_04E7,
 PAD_LIP_04E8,
 PAD_LIP_04E9,
 PAD_LIP_04EA,
 PAD_LIP_04EB,
 PAD_LIP_04EC,
 PAD_LIP_04ED,
 PAD_LIP_04EE,
 PAD_LIP_04EF,
 PAD_LIP_04F0,
 PAD_LIP_04F1,
 PAD_LIP_04F2,
 PAD_LIP_04F3,
 PAD_LIP_04F4,
 PAD_LIP_04F5,
 PAD_LIP_04F6,
 PAD_LIP_04F7,
 PAD_LIP_04F8,
 PAD_LIP_04F9,
 PAD_LIP_04FA,
 PAD_LIP_04FB,
 PAD_LIP_04FC,
 PAD_LIP_04FD,
 PAD_LIP_04FE,
 PAD_LIP_04FF,
 PAD_LIP_0500,
 PAD_LIP_0501,
 PAD_LIP_0502,
 PAD_LIP_0503,
 PAD_LIP_0504,
 PAD_LIP_0505,
 PAD_LIP_0506,
 PAD_LIP_0507,
 PAD_LIP_0508,
 PAD_LIP_0509,
 PAD_LIP_050A,
 PAD_LIP_050B,
 PAD_LIP_050C,
 PAD_LIP_050D,
 PAD_LIP_050E,
 PAD_LIP_050F,
 PAD_LIP_0510,
 PAD_LIP_0511,
 PAD_LIP_0512,
 PAD_LIP_0513,
 PAD_LIP_0514,
 PAD_LIP_0515,
 PAD_LIP_0516,
 PAD_LIP_0517,
 PAD_LIP_0518,
 PAD_LIP_0519,
 PAD_LIP_051A,
 PAD_LIP_051B,
 PAD_LIP_051C,
 PAD_LIP_051D,
 PAD_LIP_051E,
 PAD_LIP_051F,
 PAD_LIP_0520,
 PAD_LIP_0521,
 PAD_LIP_0522,
 PAD_LIP_0523,
 PAD_LIP_0524,
 PAD_LIP_0525,
 PAD_LIP_0526,
 PAD_LIP_0527,
 PAD_LIP_0528,
 PAD_LIP_0529,
 PAD_LIP_052A,
 PAD_LIP_052B,
 PAD_LIP_052C,
 PAD_LIP_052D,
 PAD_LIP_052E,
 PAD_LIP_052F,
 PAD_LIP_0530,
 PAD_LIP_0531,
 PAD_LIP_0532,
 PAD_LIP_0533,
 PAD_LIP_0534,
 PAD_LIP_0535,
 PAD_LIP_0536,
 PAD_LIP_0537,
 PAD_LIP_0538,
 PAD_LIP_0539,
 PAD_LIP_053A,
 PAD_LIP_053B,
 PAD_LIP_053C,
 PAD_LIP_053D,
 PAD_LIP_053E,
 PAD_LIP_053F,
 PAD_LIP_0540,
 PAD_LIP_0541,
 PAD_LIP_0542,
 PAD_LIP_0543,
 PAD_LIP_0544,
 PAD_LIP_0545,
 PAD_LIP_0546,
 PAD_LIP_0547,
 PAD_LIP_0548,
 PAD_LIP_0549,
 PAD_LIP_054A,
 PAD_LIP_054B,
 PAD_LIP_054C,
 PAD_LIP_054D,
 PAD_LIP_054E,
 PAD_LIP_054F,
 PAD_LIP_0550,
 PAD_LIP_0551,
 PAD_LIP_0552,
 PAD_LIP_0553,
 PAD_LIP_0554,
 PAD_LIP_0555,
 PAD_LIP_0556,
 PAD_LIP_0557,
 PAD_LIP_0558,
 PAD_LIP_0559,
 PAD_LIP_055A,
 PAD_LIP_055B,
 PAD_LIP_055C,
 PAD_LIP_055D,
 PAD_LIP_055E,
 PAD_LIP_055F,
 PAD_LIP_0560,
 PAD_LIP_0561,
 PAD_LIP_0562,
 PAD_LIP_0563,
 PAD_LIP_0564,
 PAD_LIP_0565,
 PAD_LIP_0566,
 PAD_LIP_0567,
 PAD_LIP_END
};
enum pad_lue {
 PAD_LUE_0000,
 PAD_LUE_0001,
 PAD_LUE_0002,
 PAD_LUE_0003,
 PAD_LUE_0004,
 PAD_LUE_0005,
 PAD_LUE_0006,
 PAD_LUE_0007,
 PAD_LUE_0008,
 PAD_LUE_0009,
 PAD_LUE_000A,
 PAD_LUE_000B,
 PAD_LUE_000C,
 PAD_LUE_000D,
 PAD_LUE_000E,
 PAD_LUE_000F,
 PAD_LUE_0010,
 PAD_LUE_0011,
 PAD_LUE_0012,
 PAD_LUE_0013,
 PAD_LUE_0014,
 PAD_LUE_0015,
 PAD_LUE_0016,
 PAD_LUE_0017,
 PAD_LUE_0018,
 PAD_LUE_0019,
 PAD_LUE_001A,
 PAD_LUE_001B,
 PAD_LUE_001C,
 PAD_LUE_001D,
 PAD_LUE_001E,
 PAD_LUE_001F,
 PAD_LUE_0020,
 PAD_LUE_0021,
 PAD_LUE_0022,
 PAD_LUE_0023,
 PAD_LUE_0024,
 PAD_LUE_0025,
 PAD_LUE_0026,
 PAD_LUE_0027,
 PAD_LUE_0028,
 PAD_LUE_0029,
 PAD_LUE_002A,
 PAD_LUE_002B,
 PAD_LUE_002C,
 PAD_LUE_002D,
 PAD_LUE_002E,
 PAD_LUE_002F,
 PAD_LUE_0030,
 PAD_LUE_0031,
 PAD_LUE_0032,
 PAD_LUE_0033,
 PAD_LUE_0034,
 PAD_LUE_0035,
 PAD_LUE_0036,
 PAD_LUE_0037,
 PAD_LUE_0038,
 PAD_LUE_0039,
 PAD_LUE_003A,
 PAD_LUE_003B,
 PAD_LUE_003C,
 PAD_LUE_003D,
 PAD_LUE_003E,
 PAD_LUE_003F,
 PAD_LUE_0040,
 PAD_LUE_0041,
 PAD_LUE_0042,
 PAD_LUE_0043,
 PAD_LUE_0044,
 PAD_LUE_0045,
 PAD_LUE_0046,
 PAD_LUE_0047,
 PAD_LUE_0048,
 PAD_LUE_0049,
 PAD_LUE_004A,
 PAD_LUE_004B,
 PAD_LUE_004C,
 PAD_LUE_004D,
 PAD_LUE_004E,
 PAD_LUE_004F,
 PAD_LUE_0050,
 PAD_LUE_0051,
 PAD_LUE_0052,
 PAD_LUE_0053,
 PAD_LUE_0054,
 PAD_LUE_0055,
 PAD_LUE_0056,
 PAD_LUE_0057,
 PAD_LUE_0058,
 PAD_LUE_0059,
 PAD_LUE_005A,
 PAD_LUE_005B,
 PAD_LUE_005C,
 PAD_LUE_005D,
 PAD_LUE_005E,
 PAD_LUE_005F,
 PAD_LUE_0060,
 PAD_LUE_0061,
 PAD_LUE_0062,
 PAD_LUE_0063,
 PAD_LUE_0064,
 PAD_LUE_0065,
 PAD_LUE_0066,
 PAD_LUE_0067,
 PAD_LUE_0068,
 PAD_LUE_0069,
 PAD_LUE_006A,
 PAD_LUE_006B,
 PAD_LUE_006C,
 PAD_LUE_006D,
 PAD_LUE_006E,
 PAD_LUE_006F,
 PAD_LUE_0070,
 PAD_LUE_0071,
 PAD_LUE_0072,
 PAD_LUE_0073,
 PAD_LUE_0074,
 PAD_LUE_0075,
 PAD_LUE_0076,
 PAD_LUE_0077,
 PAD_LUE_0078,
 PAD_LUE_0079,
 PAD_LUE_007A,
 PAD_LUE_007B,
 PAD_LUE_007C,
 PAD_LUE_007D,
 PAD_LUE_007E,
 PAD_LUE_007F,
 PAD_LUE_0080,
 PAD_LUE_0081,
 PAD_LUE_0082,
 PAD_LUE_0083,
 PAD_LUE_0084,
 PAD_LUE_0085,
 PAD_LUE_0086,
 PAD_LUE_0087,
 PAD_LUE_0088,
 PAD_LUE_0089,
 PAD_LUE_008A,
 PAD_LUE_008B,
 PAD_LUE_008C,
 PAD_LUE_008D,
 PAD_LUE_008E,
 PAD_LUE_008F,
 PAD_LUE_0090,
 PAD_LUE_0091,
 PAD_LUE_0092,
 PAD_LUE_0093,
 PAD_LUE_0094,
 PAD_LUE_0095,
 PAD_LUE_0096,
 PAD_LUE_0097,
 PAD_LUE_0098,
 PAD_LUE_0099,
 PAD_LUE_009A,
 PAD_LUE_009B,
 PAD_LUE_009C,
 PAD_LUE_009D,
 PAD_LUE_009E,
 PAD_LUE_009F,
 PAD_LUE_00A0,
 PAD_LUE_00A1,
 PAD_LUE_00A2,
 PAD_LUE_00A3,
 PAD_LUE_00A4,
 PAD_LUE_00A5,
 PAD_LUE_00A6,
 PAD_LUE_00A7,
 PAD_LUE_00A8,
 PAD_LUE_00A9,
 PAD_LUE_00AA,
 PAD_LUE_00AB,
 PAD_LUE_00AC,
 PAD_LUE_00AD,
 PAD_LUE_00AE,
 PAD_LUE_00AF,
 PAD_LUE_00B0,
 PAD_LUE_00B1,
 PAD_LUE_00B2,
 PAD_LUE_00B3,
 PAD_LUE_00B4,
 PAD_LUE_00B5,
 PAD_LUE_00B6,
 PAD_LUE_00B7,
 PAD_LUE_00B8,
 PAD_LUE_00B9,
 PAD_LUE_00BA,
 PAD_LUE_00BB,
 PAD_LUE_00BC,
 PAD_LUE_00BD,
 PAD_LUE_00BE,
 PAD_LUE_00BF,
 PAD_LUE_00C0,
 PAD_LUE_00C1,
 PAD_LUE_00C2,
 PAD_LUE_00C3,
 PAD_LUE_00C4,
 PAD_LUE_00C5,
 PAD_LUE_00C6,
 PAD_LUE_00C7,
 PAD_LUE_00C8,
 PAD_LUE_00C9,
 PAD_LUE_00CA,
 PAD_LUE_00CB,
 PAD_LUE_00CC,
 PAD_LUE_00CD,
 PAD_LUE_00CE,
 PAD_LUE_00CF,
 PAD_LUE_00D0,
 PAD_LUE_00D1,
 PAD_LUE_00D2,
 PAD_LUE_00D3,
 PAD_LUE_00D4,
 PAD_LUE_00D5,
 PAD_LUE_00D6,
 PAD_LUE_00D7,
 PAD_LUE_00D8,
 PAD_LUE_00D9,
 PAD_LUE_00DA,
 PAD_LUE_00DB,
 PAD_LUE_00DC,
 PAD_LUE_00DD,
 PAD_LUE_00DE,
 PAD_LUE_00DF,
 PAD_LUE_00E0,
 PAD_LUE_00E1,
 PAD_LUE_00E2,
 PAD_LUE_00E3,
 PAD_LUE_00E4,
 PAD_LUE_00E5,
 PAD_LUE_00E6,
 PAD_LUE_00E7,
 PAD_LUE_00E8,
 PAD_LUE_00E9,
 PAD_LUE_00EA,
 PAD_LUE_00EB,
 PAD_LUE_00EC,
 PAD_LUE_00ED,
 PAD_LUE_00EE,
 PAD_LUE_00EF,
 PAD_LUE_00F0,
 PAD_LUE_00F1,
 PAD_LUE_00F2,
 PAD_LUE_00F3,
 PAD_LUE_00F4,
 PAD_LUE_00F5,
 PAD_LUE_00F6,
 PAD_LUE_00F7,
 PAD_LUE_00F8,
 PAD_LUE_00F9,
 PAD_LUE_00FA,
 PAD_LUE_00FB,
 PAD_LUE_00FC,
 PAD_LUE_00FD,
 PAD_LUE_00FE,
 PAD_LUE_00FF,
 PAD_LUE_0100,
 PAD_LUE_0101,
 PAD_LUE_0102,
 PAD_LUE_0103,
 PAD_LUE_0104,
 PAD_LUE_0105,
 PAD_LUE_0106,
 PAD_LUE_0107,
 PAD_LUE_0108,
 PAD_LUE_0109,
 PAD_LUE_010A,
 PAD_LUE_010B,
 PAD_LUE_010C,
 PAD_LUE_010D,
 PAD_LUE_010E,
 PAD_LUE_010F,
 PAD_LUE_0110,
 PAD_LUE_0111,
 PAD_LUE_0112,
 PAD_LUE_0113,
 PAD_LUE_0114,
 PAD_LUE_0115,
 PAD_LUE_0116,
 PAD_LUE_0117,
 PAD_LUE_0118,
 PAD_LUE_0119,
 PAD_LUE_011A,
 PAD_LUE_011B,
 PAD_LUE_011C,
 PAD_LUE_011D,
 PAD_LUE_011E,
 PAD_LUE_011F,
 PAD_LUE_0120,
 PAD_LUE_0121,
 PAD_LUE_0122,
 PAD_LUE_0123,
 PAD_LUE_0124,
 PAD_LUE_0125,
 PAD_LUE_0126,
 PAD_LUE_0127,
 PAD_LUE_0128,
 PAD_LUE_0129,
 PAD_LUE_012A,
 PAD_LUE_012B,
 PAD_LUE_012C,
 PAD_LUE_012D,
 PAD_LUE_012E,
 PAD_LUE_012F,
 PAD_LUE_0130,
 PAD_LUE_0131,
 PAD_LUE_0132,
 PAD_LUE_0133,
 PAD_LUE_0134,
 PAD_LUE_0135,
 PAD_LUE_0136,
 PAD_LUE_0137,
 PAD_LUE_0138,
 PAD_LUE_0139,
 PAD_LUE_013A,
 PAD_LUE_013B,
 PAD_LUE_013C,
 PAD_LUE_013D,
 PAD_LUE_013E,
 PAD_LUE_013F,
 PAD_LUE_0140,
 PAD_LUE_0141,
 PAD_LUE_0142,
 PAD_LUE_0143,
 PAD_LUE_0144,
 PAD_LUE_0145,
 PAD_LUE_0146,
 PAD_LUE_0147,
 PAD_LUE_0148,
 PAD_LUE_0149,
 PAD_LUE_014A,
 PAD_LUE_014B,
 PAD_LUE_014C,
 PAD_LUE_014D,
 PAD_LUE_014E,
 PAD_LUE_014F,
 PAD_LUE_0150,
 PAD_LUE_0151,
 PAD_LUE_0152,
 PAD_LUE_0153,
 PAD_LUE_0154,
 PAD_LUE_0155,
 PAD_LUE_0156,
 PAD_LUE_0157,
 PAD_LUE_0158,
 PAD_LUE_0159,
 PAD_LUE_015A,
 PAD_LUE_015B,
 PAD_LUE_015C,
 PAD_LUE_015D,
 PAD_LUE_015E,
 PAD_LUE_015F,
 PAD_LUE_0160,
 PAD_LUE_0161,
 PAD_LUE_0162,
 PAD_LUE_0163,
 PAD_LUE_0164,
 PAD_LUE_0165,
 PAD_LUE_0166,
 PAD_LUE_0167,
 PAD_LUE_0168,
 PAD_LUE_0169,
 PAD_LUE_016A,
 PAD_LUE_016B,
 PAD_LUE_016C,
 PAD_LUE_016D,
 PAD_LUE_016E,
 PAD_LUE_016F,
 PAD_LUE_0170,
 PAD_LUE_0171,
 PAD_LUE_0172,
 PAD_LUE_0173,
 PAD_LUE_0174,
 PAD_LUE_0175,
 PAD_LUE_0176,
 PAD_LUE_0177,
 PAD_LUE_0178,
 PAD_LUE_0179,
 PAD_LUE_017A,
 PAD_LUE_017B,
 PAD_LUE_017C,
 PAD_LUE_017D,
 PAD_LUE_017E,
 PAD_LUE_017F,
 PAD_LUE_0180,
 PAD_LUE_0181,
 PAD_LUE_0182,
 PAD_LUE_0183,
 PAD_LUE_0184,
 PAD_LUE_0185,
 PAD_LUE_0186,
 PAD_LUE_0187,
 PAD_LUE_0188,
 PAD_LUE_0189,
 PAD_LUE_018A,
 PAD_LUE_018B,
 PAD_LUE_018C,
 PAD_LUE_018D,
 PAD_LUE_018E,
 PAD_LUE_018F,
 PAD_LUE_0190,
 PAD_LUE_0191,
 PAD_LUE_0192,
 PAD_LUE_0193,
 PAD_LUE_0194,
 PAD_LUE_0195,
 PAD_LUE_0196,
 PAD_LUE_0197,
 PAD_LUE_0198,
 PAD_LUE_0199,
 PAD_LUE_019A,
 PAD_LUE_019B,
 PAD_LUE_019C,
 PAD_LUE_019D,
 PAD_LUE_019E,
 PAD_LUE_019F,
 PAD_LUE_01A0,
 PAD_LUE_01A1,
 PAD_LUE_01A2,
 PAD_LUE_01A3,
 PAD_LUE_01A4,
 PAD_LUE_01A5,
 PAD_LUE_01A6,
 PAD_LUE_01A7,
 PAD_LUE_01A8,
 PAD_LUE_01A9,
 PAD_LUE_01AA,
 PAD_LUE_01AB,
 PAD_LUE_01AC,
 PAD_LUE_01AD,
 PAD_LUE_01AE,
 PAD_LUE_01AF,
 PAD_LUE_01B0,
 PAD_LUE_01B1,
 PAD_LUE_01B2,
 PAD_LUE_01B3,
 PAD_LUE_01B4,
 PAD_LUE_01B5,
 PAD_LUE_01B6,
 PAD_LUE_01B7,
 PAD_LUE_01B8,
 PAD_LUE_01B9,
 PAD_LUE_01BA,
 PAD_LUE_01BB,
 PAD_LUE_01BC,
 PAD_LUE_01BD,
 PAD_LUE_01BE,
 PAD_LUE_01BF,
 PAD_LUE_01C0,
 PAD_LUE_01C1,
 PAD_LUE_01C2,
 PAD_LUE_01C3,
 PAD_LUE_01C4,
 PAD_LUE_01C5,
 PAD_LUE_01C6,
 PAD_LUE_01C7,
 PAD_LUE_01C8,
 PAD_LUE_01C9,
 PAD_LUE_01CA,
 PAD_LUE_01CB,
 PAD_LUE_01CC,
 PAD_LUE_01CD,
 PAD_LUE_01CE,
 PAD_LUE_01CF,
 PAD_LUE_01D0,
 PAD_LUE_01D1,
 PAD_LUE_01D2,
 PAD_LUE_01D3,
 PAD_LUE_01D4,
 PAD_LUE_01D5,
 PAD_LUE_01D6,
 PAD_LUE_01D7,
 PAD_LUE_01D8,
 PAD_LUE_01D9,
 PAD_LUE_01DA,
 PAD_LUE_01DB,
 PAD_LUE_01DC,
 PAD_LUE_01DD,
 PAD_LUE_01DE,
 PAD_LUE_01DF,
 PAD_LUE_01E0,
 PAD_LUE_01E1,
 PAD_LUE_01E2,
 PAD_LUE_01E3,
 PAD_LUE_01E4,
 PAD_LUE_01E5,
 PAD_LUE_01E6,
 PAD_LUE_01E7,
 PAD_LUE_01E8,
 PAD_LUE_01E9,
 PAD_LUE_01EA,
 PAD_LUE_01EB,
 PAD_LUE_01EC,
 PAD_LUE_01ED,
 PAD_LUE_01EE,
 PAD_LUE_01EF,
 PAD_LUE_01F0,
 PAD_LUE_01F1,
 PAD_LUE_01F2,
 PAD_LUE_01F3,
 PAD_LUE_01F4,
 PAD_LUE_01F5,
 PAD_LUE_01F6,
 PAD_LUE_01F7,
 PAD_LUE_01F8,
 PAD_LUE_01F9,
 PAD_LUE_01FA,
 PAD_LUE_01FB,
 PAD_LUE_01FC,
 PAD_LUE_01FD,
 PAD_LUE_01FE,
 PAD_LUE_01FF,
 PAD_LUE_0200,
 PAD_LUE_0201,
 PAD_LUE_0202,
 PAD_LUE_0203,
 PAD_LUE_0204,
 PAD_LUE_0205,
 PAD_LUE_0206,
 PAD_LUE_0207,
 PAD_LUE_0208,
 PAD_LUE_0209,
 PAD_LUE_020A,
 PAD_LUE_020B,
 PAD_LUE_020C,
 PAD_LUE_020D,
 PAD_LUE_020E,
 PAD_LUE_020F,
 PAD_LUE_0210,
 PAD_LUE_0211,
 PAD_LUE_0212,
 PAD_LUE_0213,
 PAD_LUE_0214,
 PAD_LUE_0215,
 PAD_LUE_0216,
 PAD_LUE_0217,
 PAD_LUE_0218,
 PAD_LUE_0219,
 PAD_LUE_021A,
 PAD_LUE_021B,
 PAD_LUE_021C,
 PAD_LUE_021D,
 PAD_LUE_021E,
 PAD_LUE_021F,
 PAD_LUE_0220,
 PAD_LUE_0221,
 PAD_LUE_0222,
 PAD_LUE_0223,
 PAD_LUE_0224,
 PAD_LUE_0225,
 PAD_LUE_0226,
 PAD_LUE_0227,
 PAD_LUE_0228,
 PAD_LUE_0229,
 PAD_LUE_022A,
 PAD_LUE_022B,
 PAD_LUE_022C,
 PAD_LUE_022D,
 PAD_LUE_022E,
 PAD_LUE_022F,
 PAD_LUE_0230,
 PAD_LUE_0231,
 PAD_LUE_0232,
 PAD_LUE_0233,
 PAD_LUE_0234,
 PAD_LUE_0235,
 PAD_LUE_0236,
 PAD_LUE_0237,
 PAD_LUE_0238,
 PAD_LUE_0239,
 PAD_LUE_023A,
 PAD_LUE_023B,
 PAD_LUE_023C,
 PAD_LUE_023D,
 PAD_LUE_023E,
 PAD_LUE_023F,
 PAD_LUE_0240,
 PAD_LUE_0241,
 PAD_LUE_0242,
 PAD_LUE_0243,
 PAD_LUE_0244,
 PAD_LUE_0245,
 PAD_LUE_0246,
 PAD_LUE_0247,
 PAD_LUE_0248,
 PAD_LUE_0249,
 PAD_LUE_024A,
 PAD_LUE_024B,
 PAD_LUE_024C,
 PAD_LUE_024D,
 PAD_LUE_024E,
 PAD_LUE_024F,
 PAD_LUE_0250,
 PAD_LUE_0251,
 PAD_LUE_0252,
 PAD_LUE_0253,
 PAD_LUE_0254,
 PAD_LUE_0255,
 PAD_LUE_0256,
 PAD_LUE_0257,
 PAD_LUE_0258,
 PAD_LUE_0259,
 PAD_LUE_025A,
 PAD_LUE_025B,
 PAD_LUE_025C,
 PAD_LUE_025D,
 PAD_LUE_025E,
 PAD_LUE_025F,
 PAD_LUE_0260,
 PAD_LUE_0261,
 PAD_LUE_0262,
 PAD_LUE_0263,
 PAD_LUE_0264,
 PAD_LUE_0265,
 PAD_LUE_0266,
 PAD_LUE_0267,
 PAD_LUE_0268,
 PAD_LUE_0269,
 PAD_LUE_026A,
 PAD_LUE_026B,
 PAD_LUE_026C,
 PAD_LUE_026D,
 PAD_LUE_026E,
 PAD_LUE_026F,
 PAD_LUE_0270,
 PAD_LUE_0271,
 PAD_LUE_0272,
 PAD_LUE_0273,
 PAD_LUE_0274,
 PAD_LUE_0275,
 PAD_LUE_0276,
 PAD_LUE_0277,
 PAD_LUE_0278,
 PAD_LUE_0279,
 PAD_LUE_027A,
 PAD_LUE_027B,
 PAD_LUE_027C,
 PAD_LUE_027D,
 PAD_LUE_027E,
 PAD_LUE_027F,
 PAD_LUE_0280,
 PAD_LUE_0281,
 PAD_LUE_0282,
 PAD_LUE_0283,
 PAD_LUE_0284,
 PAD_LUE_0285,
 PAD_LUE_0286,
 PAD_LUE_0287,
 PAD_LUE_0288,
 PAD_LUE_0289,
 PAD_LUE_028A,
 PAD_LUE_028B,
 PAD_LUE_028C,
 PAD_LUE_028D,
 PAD_LUE_028E,
 PAD_LUE_028F,
 PAD_LUE_0290,
 PAD_LUE_0291,
 PAD_LUE_0292,
 PAD_LUE_0293,
 PAD_LUE_0294,
 PAD_LUE_0295,
 PAD_LUE_0296,
 PAD_LUE_0297,
 PAD_LUE_0298,
 PAD_LUE_0299,
 PAD_LUE_029A,
 PAD_LUE_029B,
 PAD_LUE_029C,
 PAD_LUE_029D,
 PAD_LUE_029E,
 PAD_LUE_029F,
 PAD_LUE_02A0,
 PAD_LUE_02A1,
 PAD_LUE_02A2,
 PAD_LUE_02A3,
 PAD_LUE_02A4,
 PAD_LUE_02A5,
 PAD_LUE_02A6,
 PAD_LUE_02A7,
 PAD_LUE_02A8,
 PAD_LUE_02A9,
 PAD_LUE_02AA,
 PAD_LUE_02AB,
 PAD_LUE_02AC,
 PAD_LUE_02AD,
 PAD_LUE_02AE,
 PAD_LUE_02AF,
 PAD_LUE_02B0,
 PAD_LUE_02B1,
 PAD_LUE_02B2,
 PAD_LUE_02B3,
 PAD_LUE_02B4,
 PAD_LUE_02B5,
 PAD_LUE_02B6,
 PAD_LUE_02B7,
 PAD_LUE_02B8,
 PAD_LUE_02B9,
 PAD_LUE_02BA,
 PAD_LUE_02BB,
 PAD_LUE_02BC,
 PAD_LUE_02BD,
 PAD_LUE_02BE,
 PAD_LUE_02BF,
 PAD_LUE_02C0,
 PAD_LUE_02C1,
 PAD_LUE_02C2,
 PAD_LUE_02C3,
 PAD_LUE_02C4,
 PAD_LUE_02C5,
 PAD_LUE_02C6,
 PAD_LUE_02C7,
 PAD_LUE_02C8,
 PAD_LUE_02C9,
 PAD_LUE_02CA,
 PAD_LUE_02CB,
 PAD_LUE_02CC,
 PAD_LUE_02CD,
 PAD_LUE_02CE,
 PAD_LUE_02CF,
 PAD_LUE_02D0,
 PAD_LUE_02D1,
 PAD_LUE_02D2,
 PAD_LUE_02D3,
 PAD_LUE_02D4,
 PAD_LUE_02D5,
 PAD_LUE_02D6,
 PAD_LUE_02D7,
 PAD_LUE_02D8,
 PAD_LUE_02D9,
 PAD_LUE_02DA,
 PAD_LUE_02DB,
 PAD_LUE_02DC,
 PAD_LUE_02DD,
 PAD_LUE_02DE,
 PAD_LUE_02DF,
 PAD_LUE_02E0,
 PAD_LUE_02E1,
 PAD_LUE_02E2,
 PAD_LUE_02E3,
 PAD_LUE_02E4,
 PAD_LUE_02E5,
 PAD_LUE_02E6,
 PAD_LUE_02E7,
 PAD_LUE_02E8,
 PAD_LUE_02E9,
 PAD_LUE_02EA,
 PAD_LUE_02EB,
 PAD_LUE_02EC,
 PAD_LUE_02ED,
 PAD_LUE_02EE,
 PAD_LUE_02EF,
 PAD_LUE_02F0,
 PAD_LUE_02F1,
 PAD_LUE_02F2,
 PAD_LUE_02F3,
 PAD_LUE_02F4,
 PAD_LUE_02F5,
 PAD_LUE_02F6,
 PAD_LUE_02F7,
 PAD_LUE_02F8,
 PAD_LUE_02F9,
 PAD_LUE_02FA,
 PAD_LUE_02FB,
 PAD_LUE_02FC,
 PAD_LUE_02FD,
 PAD_LUE_02FE,
 PAD_LUE_02FF,
 PAD_LUE_0300,
 PAD_LUE_0301,
 PAD_LUE_0302,
 PAD_LUE_0303,
 PAD_LUE_0304,
 PAD_LUE_0305,
 PAD_LUE_0306,
 PAD_LUE_0307,
 PAD_LUE_0308,
 PAD_LUE_0309,
 PAD_LUE_030A,
 PAD_LUE_030B,
 PAD_LUE_030C,
 PAD_LUE_030D,
 PAD_LUE_030E,
 PAD_LUE_030F,
 PAD_LUE_0310,
 PAD_LUE_0311,
 PAD_LUE_0312,
 PAD_LUE_0313,
 PAD_LUE_0314,
 PAD_LUE_0315,
 PAD_LUE_0316,
 PAD_LUE_0317,
 PAD_LUE_0318,
 PAD_LUE_0319,
 PAD_LUE_031A,
 PAD_LUE_031B,
 PAD_LUE_031C,
 PAD_LUE_031D,
 PAD_LUE_031E,
 PAD_LUE_031F,
 PAD_LUE_0320,
 PAD_LUE_0321,
 PAD_LUE_0322,
 PAD_LUE_0323,
 PAD_LUE_0324,
 PAD_LUE_0325,
 PAD_LUE_0326,
 PAD_LUE_0327,
 PAD_LUE_0328,
 PAD_LUE_0329,
 PAD_LUE_032A,
 PAD_LUE_032B,
 PAD_LUE_032C,
 PAD_LUE_032D,
 PAD_LUE_032E,
 PAD_LUE_032F,
 PAD_LUE_0330,
 PAD_LUE_0331,
 PAD_LUE_0332,
 PAD_LUE_0333,
 PAD_LUE_0334,
 PAD_LUE_0335,
 PAD_LUE_0336,
 PAD_LUE_0337,
 PAD_LUE_0338,
 PAD_LUE_0339,
 PAD_LUE_033A,
 PAD_LUE_033B,
 PAD_LUE_033C,
 PAD_LUE_033D,
 PAD_LUE_033E,
 PAD_LUE_033F,
 PAD_LUE_0340,
 PAD_LUE_0341,
 PAD_LUE_0342,
 PAD_LUE_0343,
 PAD_LUE_0344,
 PAD_LUE_0345,
 PAD_LUE_0346,
 PAD_LUE_0347,
 PAD_LUE_0348,
 PAD_LUE_0349,
 PAD_LUE_034A,
 PAD_LUE_034B,
 PAD_LUE_034C,
 PAD_LUE_034D,
 PAD_LUE_034E,
 PAD_LUE_034F,
 PAD_LUE_0350,
 PAD_LUE_0351,
 PAD_LUE_0352,
 PAD_LUE_0353,
 PAD_LUE_0354,
 PAD_LUE_0355,
 PAD_LUE_0356,
 PAD_LUE_0357,
 PAD_LUE_0358,
 PAD_LUE_0359,
 PAD_LUE_035A,
 PAD_LUE_035B,
 PAD_LUE_035C,
 PAD_LUE_035D,
 PAD_LUE_035E,
 PAD_LUE_035F,
 PAD_LUE_0360,
 PAD_LUE_0361,
 PAD_LUE_0362,
 PAD_LUE_0363,
 PAD_LUE_0364,
 PAD_LUE_0365,
 PAD_LUE_0366,
 PAD_LUE_0367,
 PAD_LUE_0368,
 PAD_LUE_0369,
 PAD_LUE_036A,
 PAD_LUE_036B,
 PAD_LUE_036C,
 PAD_LUE_036D,
 PAD_LUE_036E,
 PAD_LUE_036F,
 PAD_LUE_0370,
 PAD_LUE_0371,
 PAD_LUE_0372,
 PAD_LUE_0373,
 PAD_LUE_0374,
 PAD_LUE_0375,
 PAD_LUE_0376,
 PAD_LUE_0377,
 PAD_LUE_0378,
 PAD_LUE_0379,
 PAD_LUE_037A,
 PAD_LUE_037B,
 PAD_LUE_037C,
 PAD_LUE_037D,
 PAD_LUE_037E,
 PAD_LUE_037F,
 PAD_LUE_0380,
 PAD_LUE_0381,
 PAD_LUE_0382,
 PAD_LUE_0383,
 PAD_LUE_0384,
 PAD_LUE_0385,
 PAD_LUE_0386,
 PAD_LUE_0387,
 PAD_LUE_0388,
 PAD_LUE_0389,
 PAD_LUE_038A,
 PAD_LUE_038B,
 PAD_LUE_038C,
 PAD_LUE_038D,
 PAD_LUE_038E,
 PAD_LUE_038F,
 PAD_LUE_0390,
 PAD_LUE_0391,
 PAD_LUE_0392,
 PAD_LUE_0393,
 PAD_LUE_0394,
 PAD_LUE_0395,
 PAD_LUE_0396,
 PAD_LUE_0397,
 PAD_LUE_0398,
 PAD_LUE_0399,
 PAD_LUE_039A,
 PAD_LUE_039B,
 PAD_LUE_039C,
 PAD_LUE_039D,
 PAD_LUE_039E,
 PAD_LUE_039F,
 PAD_LUE_03A0,
 PAD_LUE_03A1,
 PAD_LUE_03A2,
 PAD_LUE_03A3,
 PAD_LUE_03A4,
 PAD_LUE_03A5,
 PAD_LUE_03A6,
 PAD_LUE_03A7,
 PAD_LUE_03A8,
 PAD_LUE_03A9,
 PAD_LUE_03AA,
 PAD_LUE_03AB,
 PAD_LUE_03AC,
 PAD_LUE_03AD,
 PAD_LUE_03AE,
 PAD_LUE_03AF,
 PAD_LUE_03B0,
 PAD_LUE_03B1,
 PAD_LUE_03B2,
 PAD_LUE_03B3,
 PAD_LUE_03B4,
 PAD_LUE_03B5,
 PAD_LUE_03B6,
 PAD_LUE_03B7,
 PAD_LUE_03B8,
 PAD_LUE_03B9,
 PAD_LUE_03BA,
 PAD_LUE_03BB,
 PAD_LUE_03BC,
 PAD_LUE_03BD,
 PAD_LUE_03BE,
 PAD_LUE_03BF,
 PAD_LUE_03C0,
 PAD_LUE_03C1,
 PAD_LUE_03C2,
 PAD_LUE_03C3,
 PAD_LUE_03C4,
 PAD_LUE_03C5,
 PAD_LUE_03C6,
 PAD_LUE_03C7,
 PAD_LUE_03C8,
 PAD_LUE_03C9,
 PAD_LUE_03CA,
 PAD_LUE_03CB,
 PAD_LUE_03CC,
 PAD_LUE_03CD,
 PAD_LUE_03CE,
 PAD_LUE_03CF,
 PAD_LUE_03D0,
 PAD_LUE_03D1,
 PAD_LUE_03D2,
 PAD_LUE_03D3,
 PAD_LUE_03D4,
 PAD_LUE_03D5,
 PAD_LUE_03D6,
 PAD_LUE_03D7,
 PAD_LUE_03D8,
 PAD_LUE_03D9,
 PAD_LUE_03DA,
 PAD_LUE_03DB,
 PAD_LUE_03DC,
 PAD_LUE_03DD,
 PAD_LUE_03DE,
 PAD_LUE_03DF,
 PAD_LUE_03E0,
 PAD_LUE_03E1,
 PAD_LUE_03E2,
 PAD_LUE_03E3,
 PAD_LUE_03E4,
 PAD_LUE_03E5,
 PAD_LUE_03E6,
 PAD_LUE_03E7,
 PAD_LUE_03E8,
 PAD_LUE_03E9,
 PAD_LUE_03EA,
 PAD_LUE_03EB,
 PAD_LUE_03EC,
 PAD_LUE_03ED,
 PAD_LUE_03EE,
 PAD_LUE_03EF,
 PAD_LUE_03F0,
 PAD_LUE_03F1,
 PAD_LUE_03F2,
 PAD_LUE_03F3,
 PAD_LUE_03F4,
 PAD_LUE_03F5,
 PAD_LUE_03F6,
 PAD_LUE_03F7,
 PAD_LUE_03F8,
 PAD_LUE_03F9,
 PAD_LUE_03FA,
 PAD_LUE_03FB,
 PAD_LUE_03FC,
 PAD_LUE_03FD,
 PAD_LUE_03FE,
 PAD_LUE_03FF,
 PAD_LUE_0400,
 PAD_LUE_0401,
 PAD_LUE_0402,
 PAD_LUE_0403,
 PAD_LUE_0404,
 PAD_LUE_0405,
 PAD_LUE_0406,
 PAD_LUE_0407,
 PAD_LUE_0408,
 PAD_LUE_0409,
 PAD_LUE_040A,
 PAD_LUE_040B,
 PAD_LUE_040C,
 PAD_LUE_040D,
 PAD_LUE_040E,
 PAD_LUE_040F,
 PAD_LUE_0410,
 PAD_LUE_0411,
 PAD_LUE_0412,
 PAD_LUE_0413,
 PAD_LUE_0414,
 PAD_LUE_0415,
 PAD_LUE_0416,
 PAD_LUE_0417,
 PAD_LUE_0418,
 PAD_LUE_0419,
 PAD_LUE_041A,
 PAD_LUE_041B,
 PAD_LUE_041C,
 PAD_LUE_041D,
 PAD_LUE_041E,
 PAD_LUE_041F,
 PAD_LUE_0420,
 PAD_LUE_0421,
 PAD_LUE_0422,
 PAD_LUE_0423,
 PAD_LUE_0424,
 PAD_LUE_0425,
 PAD_LUE_0426,
 PAD_LUE_0427,
 PAD_LUE_0428,
 PAD_LUE_0429,
 PAD_LUE_042A,
 PAD_LUE_042B,
 PAD_LUE_042C,
 PAD_LUE_042D,
 PAD_LUE_042E,
 PAD_LUE_042F,
 PAD_LUE_0430,
 PAD_LUE_0431,
 PAD_LUE_0432,
 PAD_LUE_0433,
 PAD_LUE_0434,
 PAD_LUE_0435,
 PAD_LUE_0436,
 PAD_LUE_0437,
 PAD_LUE_0438,
 PAD_LUE_0439,
 PAD_LUE_043A,
 PAD_LUE_043B,
 PAD_LUE_043C,
 PAD_LUE_043D,
 PAD_LUE_043E,
 PAD_LUE_043F,
 PAD_LUE_0440,
 PAD_LUE_0441,
 PAD_LUE_0442,
 PAD_LUE_0443,
 PAD_LUE_0444,
 PAD_LUE_0445,
 PAD_LUE_0446,
 PAD_LUE_0447,
 PAD_LUE_0448,
 PAD_LUE_0449,
 PAD_LUE_044A,
 PAD_LUE_044B,
 PAD_LUE_044C,
 PAD_LUE_044D,
 PAD_LUE_044E,
 PAD_LUE_044F,
 PAD_LUE_0450,
 PAD_LUE_0451,
 PAD_LUE_0452,
 PAD_LUE_0453,
 PAD_LUE_0454,
 PAD_LUE_0455,
 PAD_LUE_0456,
 PAD_LUE_0457,
 PAD_LUE_0458,
 PAD_LUE_0459,
 PAD_LUE_045A,
 PAD_LUE_045B,
 PAD_LUE_045C,
 PAD_LUE_045D,
 PAD_LUE_045E,
 PAD_LUE_045F,
 PAD_LUE_0460,
 PAD_LUE_0461,
 PAD_LUE_0462,
 PAD_LUE_0463,
 PAD_LUE_0464,
 PAD_LUE_0465,
 PAD_LUE_0466,
 PAD_LUE_0467,
 PAD_LUE_0468,
 PAD_LUE_0469,
 PAD_LUE_046A,
 PAD_LUE_046B,
 PAD_LUE_046C,
 PAD_LUE_046D,
 PAD_LUE_046E,
 PAD_LUE_046F,
 PAD_LUE_0470,
 PAD_LUE_0471,
 PAD_LUE_0472,
 PAD_LUE_0473,
 PAD_LUE_0474,
 PAD_LUE_0475,
 PAD_LUE_0476,
 PAD_LUE_0477,
 PAD_LUE_0478,
 PAD_LUE_0479,
 PAD_LUE_047A,
 PAD_LUE_047B,
 PAD_LUE_047C,
 PAD_LUE_047D,
 PAD_LUE_047E,
 PAD_LUE_047F,
 PAD_LUE_0480,
 PAD_LUE_0481,
 PAD_LUE_0482,
 PAD_LUE_0483,
 PAD_LUE_0484,
 PAD_LUE_0485,
 PAD_LUE_0486,
 PAD_LUE_0487,
 PAD_LUE_0488,
 PAD_LUE_0489,
 PAD_LUE_048A,
 PAD_LUE_048B,
 PAD_LUE_048C,
 PAD_LUE_048D,
 PAD_LUE_048E,
 PAD_LUE_048F,
 PAD_LUE_0490,
 PAD_LUE_0491,
 PAD_LUE_0492,
 PAD_LUE_0493,
 PAD_LUE_0494,
 PAD_LUE_0495,
 PAD_LUE_0496,
 PAD_LUE_0497,
 PAD_LUE_0498,
 PAD_LUE_0499,
 PAD_LUE_049A,
 PAD_LUE_049B,
 PAD_LUE_049C,
 PAD_LUE_049D,
 PAD_LUE_049E,
 PAD_LUE_049F,
 PAD_LUE_04A0,
 PAD_LUE_04A1,
 PAD_LUE_04A2,
 PAD_LUE_04A3,
 PAD_LUE_04A4,
 PAD_LUE_04A5,
 PAD_LUE_04A6,
 PAD_LUE_04A7,
 PAD_LUE_04A8,
 PAD_LUE_04A9,
 PAD_LUE_04AA,
 PAD_LUE_04AB,
 PAD_LUE_04AC,
 PAD_LUE_04AD,
 PAD_LUE_04AE,
 PAD_LUE_04AF,
 PAD_LUE_04B0,
 PAD_LUE_04B1,
 PAD_LUE_04B2,
 PAD_LUE_04B3,
 PAD_LUE_04B4,
 PAD_LUE_04B5,
 PAD_LUE_04B6,
 PAD_LUE_04B7,
 PAD_LUE_04B8,
 PAD_LUE_04B9,
 PAD_LUE_04BA,
 PAD_LUE_04BB,
 PAD_LUE_04BC,
 PAD_LUE_04BD,
 PAD_LUE_04BE,
 PAD_LUE_04BF,
 PAD_LUE_04C0,
 PAD_LUE_04C1,
 PAD_LUE_04C2,
 PAD_LUE_04C3,
 PAD_LUE_04C4,
 PAD_LUE_04C5,
 PAD_LUE_04C6,
 PAD_LUE_04C7,
 PAD_LUE_04C8,
 PAD_LUE_04C9,
 PAD_LUE_04CA,
 PAD_LUE_04CB,
 PAD_LUE_04CC,
 PAD_LUE_04CD,
 PAD_LUE_04CE,
 PAD_LUE_04CF,
 PAD_LUE_04D0,
 PAD_LUE_04D1,
 PAD_LUE_04D2,
 PAD_LUE_04D3,
 PAD_LUE_04D4,
 PAD_LUE_04D5,
 PAD_LUE_04D6,
 PAD_LUE_04D7,
 PAD_LUE_04D8,
 PAD_LUE_04D9,
 PAD_LUE_04DA,
 PAD_LUE_04DB,
 PAD_LUE_04DC,
 PAD_LUE_04DD,
 PAD_LUE_04DE,
 PAD_LUE_04DF,
 PAD_LUE_04E0,
 PAD_LUE_04E1,
 PAD_LUE_04E2,
 PAD_LUE_04E3,
 PAD_LUE_04E4,
 PAD_LUE_04E5,
 PAD_LUE_04E6,
 PAD_LUE_04E7,
 PAD_LUE_04E8,
 PAD_LUE_04E9,
 PAD_LUE_04EA,
 PAD_LUE_04EB,
 PAD_LUE_04EC,
 PAD_LUE_04ED,
 PAD_LUE_04EE,
 PAD_LUE_04EF,
 PAD_LUE_04F0,
 PAD_LUE_04F1,
 PAD_LUE_04F2,
 PAD_LUE_04F3,
 PAD_LUE_04F4,
 PAD_LUE_04F5,
 PAD_LUE_04F6,
 PAD_LUE_04F7,
 PAD_LUE_04F8,
 PAD_LUE_04F9,
 PAD_LUE_04FA,
 PAD_LUE_04FB,
 PAD_LUE_04FC,
 PAD_LUE_04FD,
 PAD_LUE_04FE,
 PAD_LUE_04FF,
 PAD_LUE_0500,
 PAD_LUE_0501,
 PAD_LUE_0502,
 PAD_LUE_0503,
 PAD_LUE_0504,
 PAD_LUE_0505,
 PAD_LUE_0506,
 PAD_LUE_0507,
 PAD_LUE_0508,
 PAD_LUE_0509,
 PAD_LUE_050A,
 PAD_LUE_050B,
 PAD_LUE_050C,
 PAD_LUE_050D,
 PAD_LUE_050E,
 PAD_LUE_050F,
 PAD_LUE_0510,
 PAD_LUE_0511,
 PAD_LUE_0512,
 PAD_LUE_0513,
 PAD_LUE_0514,
 PAD_LUE_0515,
 PAD_LUE_0516,
 PAD_LUE_0517,
 PAD_LUE_0518,
 PAD_LUE_0519,
 PAD_LUE_051A,
 PAD_LUE_051B,
 PAD_LUE_051C,
 PAD_LUE_051D,
 PAD_LUE_051E,
 PAD_LUE_051F,
 PAD_LUE_0520,
 PAD_LUE_0521,
 PAD_LUE_0522,
 PAD_LUE_0523,
 PAD_LUE_0524,
 PAD_LUE_0525,
 PAD_LUE_0526,
 PAD_LUE_0527,
 PAD_LUE_0528,
 PAD_LUE_0529,
 PAD_LUE_052A,
 PAD_LUE_052B,
 PAD_LUE_052C,
 PAD_LUE_052D,
 PAD_LUE_052E,
 PAD_LUE_052F,
 PAD_LUE_0530,
 PAD_LUE_0531,
 PAD_LUE_0532,
 PAD_LUE_0533,
 PAD_LUE_0534,
 PAD_LUE_0535,
 PAD_LUE_0536,
 PAD_LUE_0537,
 PAD_LUE_0538,
 PAD_LUE_0539,
 PAD_LUE_053A,
 PAD_LUE_053B,
 PAD_LUE_053C,
 PAD_LUE_053D,
 PAD_LUE_053E,
 PAD_LUE_053F,
 PAD_LUE_0540,
 PAD_LUE_0541,
 PAD_LUE_0542,
 PAD_LUE_0543,
 PAD_LUE_0544,
 PAD_LUE_0545,
 PAD_LUE_0546,
 PAD_LUE_0547,
 PAD_LUE_0548,
 PAD_LUE_0549,
 PAD_LUE_054A,
 PAD_LUE_054B,
 PAD_LUE_054C,
 PAD_LUE_054D,
 PAD_LUE_054E,
 PAD_LUE_054F,
 PAD_LUE_0550,
 PAD_LUE_0551,
 PAD_LUE_0552,
 PAD_LUE_0553,
 PAD_LUE_0554,
 PAD_LUE_0555,
 PAD_LUE_0556,
 PAD_LUE_0557,
 PAD_LUE_0558,
 PAD_LUE_0559,
 PAD_LUE_055A,
 PAD_LUE_055B,
 PAD_LUE_055C,
 PAD_LUE_055D,
 PAD_LUE_055E,
 PAD_LUE_055F,
 PAD_LUE_0560,
 PAD_LUE_0561,
 PAD_LUE_0562,
 PAD_LUE_0563,
 PAD_LUE_0564,
 PAD_LUE_0565,
 PAD_LUE_0566,
 PAD_LUE_0567,
 PAD_LUE_END
};
enum pad_oat {
 PAD_OAT_0000,
 PAD_OAT_0001,
 PAD_OAT_0002,
 PAD_OAT_0003,
 PAD_OAT_0004,
 PAD_OAT_0005,
 PAD_OAT_0006,
 PAD_OAT_0007,
 PAD_OAT_0008,
 PAD_OAT_0009,
 PAD_OAT_000A,
 PAD_OAT_000B,
 PAD_OAT_000C,
 PAD_OAT_000D,
 PAD_OAT_000E,
 PAD_OAT_000F,
 PAD_OAT_0010,
 PAD_OAT_0011,
 PAD_OAT_0012,
 PAD_OAT_0013,
 PAD_OAT_0014,
 PAD_OAT_0015,
 PAD_OAT_0016,
 PAD_OAT_0017,
 PAD_OAT_0018,
 PAD_OAT_0019,
 PAD_OAT_001A,
 PAD_OAT_001B,
 PAD_OAT_001C,
 PAD_OAT_001D,
 PAD_OAT_001E,
 PAD_OAT_001F,
 PAD_OAT_0020,
 PAD_OAT_0021,
 PAD_OAT_0022,
 PAD_OAT_0023,
 PAD_OAT_0024,
 PAD_OAT_0025,
 PAD_OAT_0026,
 PAD_OAT_0027,
 PAD_OAT_0028,
 PAD_OAT_0029,
 PAD_OAT_002A,
 PAD_OAT_002B,
 PAD_OAT_002C,
 PAD_OAT_002D,
 PAD_OAT_002E,
 PAD_OAT_002F,
 PAD_OAT_0030,
 PAD_OAT_0031,
 PAD_OAT_0032,
 PAD_OAT_0033,
 PAD_OAT_0034,
 PAD_OAT_0035,
 PAD_OAT_0036,
 PAD_OAT_0037,
 PAD_OAT_0038,
 PAD_OAT_0039,
 PAD_OAT_003A,
 PAD_OAT_003B,
 PAD_OAT_003C,
 PAD_OAT_003D,
 PAD_OAT_003E,
 PAD_OAT_003F,
 PAD_OAT_0040,
 PAD_OAT_0041,
 PAD_OAT_0042,
 PAD_OAT_0043,
 PAD_OAT_0044,
 PAD_OAT_0045,
 PAD_OAT_0046,
 PAD_OAT_0047,
 PAD_OAT_0048,
 PAD_OAT_0049,
 PAD_OAT_004A,
 PAD_OAT_004B,
 PAD_OAT_004C,
 PAD_OAT_004D,
 PAD_OAT_004E,
 PAD_OAT_004F,
 PAD_OAT_0050,
 PAD_OAT_0051,
 PAD_OAT_0052,
 PAD_OAT_0053,
 PAD_OAT_0054,
 PAD_OAT_0055,
 PAD_OAT_0056,
 PAD_OAT_0057,
 PAD_OAT_0058,
 PAD_OAT_0059,
 PAD_OAT_005A,
 PAD_OAT_005B,
 PAD_OAT_005C,
 PAD_OAT_005D,
 PAD_OAT_005E,
 PAD_OAT_005F,
 PAD_OAT_0060,
 PAD_OAT_0061,
 PAD_OAT_0062,
 PAD_OAT_0063,
 PAD_OAT_0064,
 PAD_OAT_0065,
 PAD_OAT_0066,
 PAD_OAT_0067,
 PAD_OAT_0068,
 PAD_OAT_0069,
 PAD_OAT_006A,
 PAD_OAT_006B,
 PAD_OAT_006C,
 PAD_OAT_006D,
 PAD_OAT_006E,
 PAD_OAT_006F,
 PAD_OAT_0070,
 PAD_OAT_0071,
 PAD_OAT_0072,
 PAD_OAT_0073,
 PAD_OAT_0074,
 PAD_OAT_0075,
 PAD_OAT_0076,
 PAD_OAT_0077,
 PAD_OAT_0078,
 PAD_OAT_0079,
 PAD_OAT_007A,
 PAD_OAT_007B,
 PAD_OAT_007C,
 PAD_OAT_007D,
 PAD_OAT_007E,
 PAD_OAT_007F,
 PAD_OAT_0080,
 PAD_OAT_0081,
 PAD_OAT_0082,
 PAD_OAT_0083,
 PAD_OAT_0084,
 PAD_OAT_0085,
 PAD_OAT_0086,
 PAD_OAT_0087,
 PAD_OAT_0088,
 PAD_OAT_0089,
 PAD_OAT_008A,
 PAD_OAT_008B,
 PAD_OAT_008C,
 PAD_OAT_008D,
 PAD_OAT_008E,
 PAD_OAT_008F,
 PAD_OAT_0090,
 PAD_OAT_0091,
 PAD_OAT_0092,
 PAD_OAT_0093,
 PAD_OAT_0094,
 PAD_OAT_0095,
 PAD_OAT_0096,
 PAD_OAT_0097,
 PAD_OAT_0098,
 PAD_OAT_0099,
 PAD_OAT_009A,
 PAD_OAT_009B,
 PAD_OAT_009C,
 PAD_OAT_009D,
 PAD_OAT_009E,
 PAD_OAT_009F,
 PAD_OAT_00A0,
 PAD_OAT_00A1,
 PAD_OAT_00A2,
 PAD_OAT_00A3,
 PAD_OAT_00A4,
 PAD_OAT_00A5,
 PAD_OAT_00A6,
 PAD_OAT_00A7,
 PAD_OAT_00A8,
 PAD_OAT_00A9,
 PAD_OAT_00AA,
 PAD_OAT_00AB,
 PAD_OAT_00AC,
 PAD_OAT_00AD,
 PAD_OAT_00AE,
 PAD_OAT_00AF,
 PAD_OAT_00B0,
 PAD_OAT_00B1,
 PAD_OAT_00B2,
 PAD_OAT_00B3,
 PAD_OAT_00B4,
 PAD_OAT_00B5,
 PAD_OAT_00B6,
 PAD_OAT_00B7,
 PAD_OAT_00B8,
 PAD_OAT_00B9,
 PAD_OAT_00BA,
 PAD_OAT_00BB,
 PAD_OAT_00BC,
 PAD_OAT_00BD,
 PAD_OAT_00BE,
 PAD_OAT_00BF,
 PAD_OAT_00C0,
 PAD_OAT_00C1,
 PAD_OAT_00C2,
 PAD_OAT_00C3,
 PAD_OAT_00C4,
 PAD_OAT_00C5,
 PAD_OAT_00C6,
 PAD_OAT_00C7,
 PAD_OAT_00C8,
 PAD_OAT_00C9,
 PAD_OAT_00CA,
 PAD_OAT_00CB,
 PAD_OAT_00CC,
 PAD_OAT_00CD,
 PAD_OAT_00CE,
 PAD_OAT_00CF,
 PAD_OAT_00D0,
 PAD_OAT_00D1,
 PAD_OAT_00D2,
 PAD_OAT_00D3,
 PAD_OAT_00D4,
 PAD_OAT_00D5,
 PAD_OAT_00D6,
 PAD_OAT_00D7,
 PAD_OAT_00D8,
 PAD_OAT_00D9,
 PAD_OAT_00DA,
 PAD_OAT_00DB,
 PAD_OAT_00DC,
 PAD_OAT_00DD,
 PAD_OAT_00DE,
 PAD_OAT_00DF,
 PAD_OAT_00E0,
 PAD_OAT_00E1,
 PAD_OAT_00E2,
 PAD_OAT_00E3,
 PAD_OAT_00E4,
 PAD_OAT_00E5,
 PAD_OAT_00E6,
 PAD_OAT_00E7,
 PAD_OAT_00E8,
 PAD_OAT_00E9,
 PAD_OAT_00EA,
 PAD_OAT_00EB,
 PAD_OAT_00EC,
 PAD_OAT_00ED,
 PAD_OAT_00EE,
 PAD_OAT_00EF,
 PAD_OAT_00F0,
 PAD_OAT_00F1,
 PAD_OAT_00F2,
 PAD_OAT_00F3,
 PAD_OAT_00F4,
 PAD_OAT_00F5,
 PAD_OAT_00F6,
 PAD_OAT_00F7,
 PAD_OAT_00F8,
 PAD_OAT_00F9,
 PAD_OAT_00FA,
 PAD_OAT_00FB,
 PAD_OAT_00FC,
 PAD_OAT_00FD,
 PAD_OAT_00FE,
 PAD_OAT_00FF,
 PAD_OAT_0100,
 PAD_OAT_0101,
 PAD_OAT_0102,
 PAD_OAT_0103,
 PAD_OAT_0104,
 PAD_OAT_0105,
 PAD_OAT_END
};
enum pad_pam {
 PAD_PAM_0000,
 PAD_PAM_0001,
 PAD_PAM_0002,
 PAD_PAM_0003,
 PAD_PAM_0004,
 PAD_PAM_0005,
 PAD_PAM_0006,
 PAD_PAM_0007,
 PAD_PAM_0008,
 PAD_PAM_0009,
 PAD_PAM_000A,
 PAD_PAM_000B,
 PAD_PAM_000C,
 PAD_PAM_000D,
 PAD_PAM_000E,
 PAD_PAM_000F,
 PAD_PAM_0010,
 PAD_PAM_0011,
 PAD_PAM_0012,
 PAD_PAM_0013,
 PAD_PAM_0014,
 PAD_PAM_0015,
 PAD_PAM_0016,
 PAD_PAM_0017,
 PAD_PAM_0018,
 PAD_PAM_0019,
 PAD_PAM_001A,
 PAD_PAM_001B,
 PAD_PAM_001C,
 PAD_PAM_001D,
 PAD_PAM_001E,
 PAD_PAM_001F,
 PAD_PAM_0020,
 PAD_PAM_0021,
 PAD_PAM_0022,
 PAD_PAM_0023,
 PAD_PAM_0024,
 PAD_PAM_0025,
 PAD_PAM_0026,
 PAD_PAM_0027,
 PAD_PAM_0028,
 PAD_PAM_0029,
 PAD_PAM_002A,
 PAD_PAM_002B,
 PAD_PAM_002C,
 PAD_PAM_002D,
 PAD_PAM_002E,
 PAD_PAM_002F,
 PAD_PAM_0030,
 PAD_PAM_0031,
 PAD_PAM_0032,
 PAD_PAM_0033,
 PAD_PAM_0034,
 PAD_PAM_0035,
 PAD_PAM_0036,
 PAD_PAM_0037,
 PAD_PAM_0038,
 PAD_PAM_0039,
 PAD_PAM_003A,
 PAD_PAM_003B,
 PAD_PAM_003C,
 PAD_PAM_003D,
 PAD_PAM_003E,
 PAD_PAM_003F,
 PAD_PAM_0040,
 PAD_PAM_0041,
 PAD_PAM_0042,
 PAD_PAM_0043,
 PAD_PAM_0044,
 PAD_PAM_0045,
 PAD_PAM_0046,
 PAD_PAM_0047,
 PAD_PAM_0048,
 PAD_PAM_0049,
 PAD_PAM_004A,
 PAD_PAM_004B,
 PAD_PAM_004C,
 PAD_PAM_004D,
 PAD_PAM_004E,
 PAD_PAM_004F,
 PAD_PAM_0050,
 PAD_PAM_0051,
 PAD_PAM_0052,
 PAD_PAM_0053,
 PAD_PAM_0054,
 PAD_PAM_0055,
 PAD_PAM_0056,
 PAD_PAM_0057,
 PAD_PAM_0058,
 PAD_PAM_0059,
 PAD_PAM_005A,
 PAD_PAM_005B,
 PAD_PAM_005C,
 PAD_PAM_005D,
 PAD_PAM_005E,
 PAD_PAM_005F,
 PAD_PAM_0060,
 PAD_PAM_0061,
 PAD_PAM_0062,
 PAD_PAM_0063,
 PAD_PAM_0064,
 PAD_PAM_0065,
 PAD_PAM_0066,
 PAD_PAM_0067,
 PAD_PAM_0068,
 PAD_PAM_0069,
 PAD_PAM_006A,
 PAD_PAM_006B,
 PAD_PAM_006C,
 PAD_PAM_006D,
 PAD_PAM_006E,
 PAD_PAM_006F,
 PAD_PAM_0070,
 PAD_PAM_0071,
 PAD_PAM_0072,
 PAD_PAM_0073,
 PAD_PAM_0074,
 PAD_PAM_0075,
 PAD_PAM_0076,
 PAD_PAM_0077,
 PAD_PAM_0078,
 PAD_PAM_0079,
 PAD_PAM_007A,
 PAD_PAM_007B,
 PAD_PAM_007C,
 PAD_PAM_007D,
 PAD_PAM_007E,
 PAD_PAM_007F,
 PAD_PAM_0080,
 PAD_PAM_0081,
 PAD_PAM_0082,
 PAD_PAM_0083,
 PAD_PAM_0084,
 PAD_PAM_0085,
 PAD_PAM_0086,
 PAD_PAM_0087,
 PAD_PAM_0088,
 PAD_PAM_0089,
 PAD_PAM_008A,
 PAD_PAM_008B,
 PAD_PAM_008C,
 PAD_PAM_008D,
 PAD_PAM_008E,
 PAD_PAM_008F,
 PAD_PAM_0090,
 PAD_PAM_0091,
 PAD_PAM_0092,
 PAD_PAM_0093,
 PAD_PAM_0094,
 PAD_PAM_0095,
 PAD_PAM_0096,
 PAD_PAM_0097,
 PAD_PAM_0098,
 PAD_PAM_0099,
 PAD_PAM_009A,
 PAD_PAM_009B,
 PAD_PAM_009C,
 PAD_PAM_009D,
 PAD_PAM_009E,
 PAD_PAM_009F,
 PAD_PAM_00A0,
 PAD_PAM_00A1,
 PAD_PAM_00A2,
 PAD_PAM_00A3,
 PAD_PAM_00A4,
 PAD_PAM_00A5,
 PAD_PAM_00A6,
 PAD_PAM_00A7,
 PAD_PAM_00A8,
 PAD_PAM_00A9,
 PAD_PAM_00AA,
 PAD_PAM_00AB,
 PAD_PAM_00AC,
 PAD_PAM_00AD,
 PAD_PAM_00AE,
 PAD_PAM_00AF,
 PAD_PAM_00B0,
 PAD_PAM_00B1,
 PAD_PAM_00B2,
 PAD_PAM_00B3,
 PAD_PAM_00B4,
 PAD_PAM_00B5,
 PAD_PAM_00B6,
 PAD_PAM_00B7,
 PAD_PAM_00B8,
 PAD_PAM_00B9,
 PAD_PAM_00BA,
 PAD_PAM_00BB,
 PAD_PAM_00BC,
 PAD_PAM_00BD,
 PAD_PAM_00BE,
 PAD_PAM_00BF,
 PAD_PAM_00C0,
 PAD_PAM_00C1,
 PAD_PAM_00C2,
 PAD_PAM_00C3,
 PAD_PAM_00C4,
 PAD_PAM_00C5,
 PAD_PAM_00C6,
 PAD_PAM_00C7,
 PAD_PAM_00C8,
 PAD_PAM_00C9,
 PAD_PAM_00CA,
 PAD_PAM_00CB,
 PAD_PAM_00CC,
 PAD_PAM_00CD,
 PAD_PAM_00CE,
 PAD_PAM_00CF,
 PAD_PAM_00D0,
 PAD_PAM_00D1,
 PAD_PAM_00D2,
 PAD_PAM_00D3,
 PAD_PAM_00D4,
 PAD_PAM_00D5,
 PAD_PAM_00D6,
 PAD_PAM_00D7,
 PAD_PAM_00D8,
 PAD_PAM_00D9,
 PAD_PAM_00DA,
 PAD_PAM_00DB,
 PAD_PAM_00DC,
 PAD_PAM_00DD,
 PAD_PAM_00DE,
 PAD_PAM_00DF,
 PAD_PAM_00E0,
 PAD_PAM_00E1,
 PAD_PAM_00E2,
 PAD_PAM_00E3,
 PAD_PAM_00E4,
 PAD_PAM_00E5,
 PAD_PAM_00E6,
 PAD_PAM_00E7,
 PAD_PAM_00E8,
 PAD_PAM_00E9,
 PAD_PAM_00EA,
 PAD_PAM_00EB,
 PAD_PAM_00EC,
 PAD_PAM_00ED,
 PAD_PAM_00EE,
 PAD_PAM_00EF,
 PAD_PAM_00F0,
 PAD_PAM_00F1,
 PAD_PAM_00F2,
 PAD_PAM_00F3,
 PAD_PAM_00F4,
 PAD_PAM_00F5,
 PAD_PAM_00F6,
 PAD_PAM_00F7,
 PAD_PAM_00F8,
 PAD_PAM_00F9,
 PAD_PAM_00FA,
 PAD_PAM_00FB,
 PAD_PAM_00FC,
 PAD_PAM_00FD,
 PAD_PAM_00FE,
 PAD_PAM_00FF,
 PAD_PAM_0100,
 PAD_PAM_0101,
 PAD_PAM_0102,
 PAD_PAM_0103,
 PAD_PAM_0104,
 PAD_PAM_0105,
 PAD_PAM_0106,
 PAD_PAM_0107,
 PAD_PAM_0108,
 PAD_PAM_0109,
 PAD_PAM_010A,
 PAD_PAM_010B,
 PAD_PAM_010C,
 PAD_PAM_010D,
 PAD_PAM_010E,
 PAD_PAM_010F,
 PAD_PAM_0110,
 PAD_PAM_0111,
 PAD_PAM_0112,
 PAD_PAM_0113,
 PAD_PAM_0114,
 PAD_PAM_0115,
 PAD_PAM_0116,
 PAD_PAM_0117,
 PAD_PAM_0118,
 PAD_PAM_0119,
 PAD_PAM_011A,
 PAD_PAM_011B,
 PAD_PAM_011C,
 PAD_PAM_011D,
 PAD_PAM_011E,
 PAD_PAM_011F,
 PAD_PAM_0120,
 PAD_PAM_0121,
 PAD_PAM_0122,
 PAD_PAM_0123,
 PAD_PAM_0124,
 PAD_PAM_0125,
 PAD_PAM_0126,
 PAD_PAM_0127,
 PAD_PAM_0128,
 PAD_PAM_0129,
 PAD_PAM_012A,
 PAD_PAM_012B,
 PAD_PAM_012C,
 PAD_PAM_012D,
 PAD_PAM_012E,
 PAD_PAM_012F,
 PAD_PAM_0130,
 PAD_PAM_0131,
 PAD_PAM_0132,
 PAD_PAM_0133,
 PAD_PAM_0134,
 PAD_PAM_0135,
 PAD_PAM_0136,
 PAD_PAM_0137,
 PAD_PAM_0138,
 PAD_PAM_0139,
 PAD_PAM_013A,
 PAD_PAM_013B,
 PAD_PAM_013C,
 PAD_PAM_013D,
 PAD_PAM_013E,
 PAD_PAM_013F,
 PAD_PAM_0140,
 PAD_PAM_0141,
 PAD_PAM_0142,
 PAD_PAM_0143,
 PAD_PAM_0144,
 PAD_PAM_0145,
 PAD_PAM_0146,
 PAD_PAM_0147,
 PAD_PAM_0148,
 PAD_PAM_0149,
 PAD_PAM_014A,
 PAD_PAM_014B,
 PAD_PAM_014C,
 PAD_PAM_014D,
 PAD_PAM_014E,
 PAD_PAM_014F,
 PAD_PAM_0150,
 PAD_PAM_0151,
 PAD_PAM_0152,
 PAD_PAM_0153,
 PAD_PAM_0154,
 PAD_PAM_0155,
 PAD_PAM_0156,
 PAD_PAM_0157,
 PAD_PAM_0158,
 PAD_PAM_0159,
 PAD_PAM_015A,
 PAD_PAM_015B,
 PAD_PAM_015C,
 PAD_PAM_015D,
 PAD_PAM_015E,
 PAD_PAM_015F,
 PAD_PAM_0160,
 PAD_PAM_0161,
 PAD_PAM_0162,
 PAD_PAM_0163,
 PAD_PAM_0164,
 PAD_PAM_0165,
 PAD_PAM_0166,
 PAD_PAM_0167,
 PAD_PAM_0168,
 PAD_PAM_0169,
 PAD_PAM_016A,
 PAD_PAM_016B,
 PAD_PAM_016C,
 PAD_PAM_016D,
 PAD_PAM_016E,
 PAD_PAM_016F,
 PAD_PAM_0170,
 PAD_PAM_0171,
 PAD_PAM_0172,
 PAD_PAM_0173,
 PAD_PAM_0174,
 PAD_PAM_0175,
 PAD_PAM_0176,
 PAD_PAM_0177,
 PAD_PAM_0178,
 PAD_PAM_0179,
 PAD_PAM_017A,
 PAD_PAM_017B,
 PAD_PAM_017C,
 PAD_PAM_017D,
 PAD_PAM_017E,
 PAD_PAM_017F,
 PAD_PAM_0180,
 PAD_PAM_0181,
 PAD_PAM_0182,
 PAD_PAM_0183,
 PAD_PAM_0184,
 PAD_PAM_0185,
 PAD_PAM_0186,
 PAD_PAM_0187,
 PAD_PAM_0188,
 PAD_PAM_0189,
 PAD_PAM_018A,
 PAD_PAM_018B,
 PAD_PAM_018C,
 PAD_PAM_018D,
 PAD_PAM_018E,
 PAD_PAM_018F,
 PAD_PAM_0190,
 PAD_PAM_0191,
 PAD_PAM_0192,
 PAD_PAM_0193,
 PAD_PAM_0194,
 PAD_PAM_0195,
 PAD_PAM_0196,
 PAD_PAM_0197,
 PAD_PAM_0198,
 PAD_PAM_0199,
 PAD_PAM_019A,
 PAD_PAM_019B,
 PAD_PAM_019C,
 PAD_PAM_019D,
 PAD_PAM_019E,
 PAD_PAM_019F,
 PAD_PAM_01A0,
 PAD_PAM_01A1,
 PAD_PAM_01A2,
 PAD_PAM_01A3,
 PAD_PAM_01A4,
 PAD_PAM_01A5,
 PAD_PAM_01A6,
 PAD_PAM_01A7,
 PAD_PAM_01A8,
 PAD_PAM_01A9,
 PAD_PAM_01AA,
 PAD_PAM_01AB,
 PAD_PAM_01AC,
 PAD_PAM_01AD,
 PAD_PAM_01AE,
 PAD_PAM_01AF,
 PAD_PAM_01B0,
 PAD_PAM_01B1,
 PAD_PAM_01B2,
 PAD_PAM_01B3,
 PAD_PAM_01B4,
 PAD_PAM_01B5,
 PAD_PAM_01B6,
 PAD_PAM_01B7,
 PAD_PAM_01B8,
 PAD_PAM_01B9,
 PAD_PAM_01BA,
 PAD_PAM_01BB,
 PAD_PAM_01BC,
 PAD_PAM_01BD,
 PAD_PAM_01BE,
 PAD_PAM_01BF,
 PAD_PAM_01C0,
 PAD_PAM_01C1,
 PAD_PAM_01C2,
 PAD_PAM_01C3,
 PAD_PAM_01C4,
 PAD_PAM_01C5,
 PAD_PAM_01C6,
 PAD_PAM_01C7,
 PAD_PAM_01C8,
 PAD_PAM_01C9,
 PAD_PAM_01CA,
 PAD_PAM_01CB,
 PAD_PAM_01CC,
 PAD_PAM_01CD,
 PAD_PAM_01CE,
 PAD_PAM_01CF,
 PAD_PAM_01D0,
 PAD_PAM_01D1,
 PAD_PAM_01D2,
 PAD_PAM_01D3,
 PAD_PAM_01D4,
 PAD_PAM_01D5,
 PAD_PAM_01D6,
 PAD_PAM_01D7,
 PAD_PAM_01D8,
 PAD_PAM_01D9,
 PAD_PAM_01DA,
 PAD_PAM_01DB,
 PAD_PAM_01DC,
 PAD_PAM_01DD,
 PAD_PAM_01DE,
 PAD_PAM_01DF,
 PAD_PAM_01E0,
 PAD_PAM_01E1,
 PAD_PAM_01E2,
 PAD_PAM_01E3,
 PAD_PAM_01E4,
 PAD_PAM_01E5,
 PAD_PAM_01E6,
 PAD_PAM_01E7,
 PAD_PAM_01E8,
 PAD_PAM_01E9,
 PAD_PAM_01EA,
 PAD_PAM_01EB,
 PAD_PAM_01EC,
 PAD_PAM_01ED,
 PAD_PAM_01EE,
 PAD_PAM_01EF,
 PAD_PAM_01F0,
 PAD_PAM_01F1,
 PAD_PAM_01F2,
 PAD_PAM_01F3,
 PAD_PAM_01F4,
 PAD_PAM_01F5,
 PAD_PAM_01F6,
 PAD_PAM_01F7,
 PAD_PAM_01F8,
 PAD_PAM_01F9,
 PAD_PAM_01FA,
 PAD_PAM_01FB,
 PAD_PAM_01FC,
 PAD_PAM_01FD,
 PAD_PAM_01FE,
 PAD_PAM_01FF,
 PAD_PAM_0200,
 PAD_PAM_0201,
 PAD_PAM_0202,
 PAD_PAM_0203,
 PAD_PAM_0204,
 PAD_PAM_0205,
 PAD_PAM_0206,
 PAD_PAM_0207,
 PAD_PAM_0208,
 PAD_PAM_0209,
 PAD_PAM_020A,
 PAD_PAM_020B,
 PAD_PAM_020C,
 PAD_PAM_020D,
 PAD_PAM_020E,
 PAD_PAM_020F,
 PAD_PAM_0210,
 PAD_PAM_0211,
 PAD_PAM_0212,
 PAD_PAM_0213,
 PAD_PAM_0214,
 PAD_PAM_0215,
 PAD_PAM_0216,
 PAD_PAM_0217,
 PAD_PAM_0218,
 PAD_PAM_0219,
 PAD_PAM_021A,
 PAD_PAM_021B,
 PAD_PAM_021C,
 PAD_PAM_021D,
 PAD_PAM_021E,
 PAD_PAM_021F,
 PAD_PAM_0220,
 PAD_PAM_0221,
 PAD_PAM_0222,
 PAD_PAM_0223,
 PAD_PAM_0224,
 PAD_PAM_0225,
 PAD_PAM_0226,
 PAD_PAM_0227,
 PAD_PAM_0228,
 PAD_PAM_0229,
 PAD_PAM_022A,
 PAD_PAM_022B,
 PAD_PAM_022C,
 PAD_PAM_022D,
 PAD_PAM_022E,
 PAD_PAM_022F,
 PAD_PAM_0230,
 PAD_PAM_0231,
 PAD_PAM_0232,
 PAD_PAM_0233,
 PAD_PAM_0234,
 PAD_PAM_0235,
 PAD_PAM_0236,
 PAD_PAM_0237,
 PAD_PAM_0238,
 PAD_PAM_0239,
 PAD_PAM_023A,
 PAD_PAM_023B,
 PAD_PAM_023C,
 PAD_PAM_023D,
 PAD_PAM_023E,
 PAD_PAM_023F,
 PAD_PAM_0240,
 PAD_PAM_0241,
 PAD_PAM_0242,
 PAD_PAM_0243,
 PAD_PAM_0244,
 PAD_PAM_0245,
 PAD_PAM_0246,
 PAD_PAM_0247,
 PAD_PAM_0248,
 PAD_PAM_0249,
 PAD_PAM_024A,
 PAD_PAM_024B,
 PAD_PAM_024C,
 PAD_PAM_024D,
 PAD_PAM_024E,
 PAD_PAM_024F,
 PAD_PAM_0250,
 PAD_PAM_0251,
 PAD_PAM_0252,
 PAD_PAM_0253,
 PAD_PAM_0254,
 PAD_PAM_0255,
 PAD_PAM_0256,
 PAD_PAM_0257,
 PAD_PAM_0258,
 PAD_PAM_0259,
 PAD_PAM_025A,
 PAD_PAM_025B,
 PAD_PAM_025C,
 PAD_PAM_025D,
 PAD_PAM_025E,
 PAD_PAM_025F,
 PAD_PAM_0260,
 PAD_PAM_0261,
 PAD_PAM_0262,
 PAD_PAM_0263,
 PAD_PAM_0264,
 PAD_PAM_0265,
 PAD_PAM_0266,
 PAD_PAM_0267,
 PAD_PAM_0268,
 PAD_PAM_0269,
 PAD_PAM_026A,
 PAD_PAM_026B,
 PAD_PAM_026C,
 PAD_PAM_026D,
 PAD_PAM_026E,
 PAD_PAM_026F,
 PAD_PAM_0270,
 PAD_PAM_0271,
 PAD_PAM_0272,
 PAD_PAM_0273,
 PAD_PAM_0274,
 PAD_PAM_0275,
 PAD_PAM_0276,
 PAD_PAM_0277,
 PAD_PAM_0278,
 PAD_PAM_0279,
 PAD_PAM_027A,
 PAD_PAM_027B,
 PAD_PAM_027C,
 PAD_PAM_027D,
 PAD_PAM_027E,
 PAD_PAM_027F,
 PAD_PAM_0280,
 PAD_PAM_0281,
 PAD_PAM_0282,
 PAD_PAM_0283,
 PAD_PAM_0284,
 PAD_PAM_0285,
 PAD_PAM_0286,
 PAD_PAM_0287,
 PAD_PAM_0288,
 PAD_PAM_0289,
 PAD_PAM_028A,
 PAD_PAM_028B,
 PAD_PAM_028C,
 PAD_PAM_028D,
 PAD_PAM_028E,
 PAD_PAM_028F,
 PAD_PAM_0290,
 PAD_PAM_0291,
 PAD_PAM_0292,
 PAD_PAM_0293,
 PAD_PAM_0294,
 PAD_PAM_0295,
 PAD_PAM_0296,
 PAD_PAM_0297,
 PAD_PAM_0298,
 PAD_PAM_0299,
 PAD_PAM_029A,
 PAD_PAM_029B,
 PAD_PAM_END
};
enum pad_pete {
 PAD_PETE_0000,
 PAD_PETE_0001,
 PAD_PETE_0002,
 PAD_PETE_0003,
 PAD_PETE_0004,
 PAD_PETE_0005,
 PAD_PETE_0006,
 PAD_PETE_0007,
 PAD_PETE_0008,
 PAD_PETE_0009,
 PAD_PETE_000A,
 PAD_PETE_000B,
 PAD_PETE_000C,
 PAD_PETE_000D,
 PAD_PETE_000E,
 PAD_PETE_000F,
 PAD_PETE_0010,
 PAD_PETE_0011,
 PAD_PETE_0012,
 PAD_PETE_0013,
 PAD_PETE_0014,
 PAD_PETE_0015,
 PAD_PETE_0016,
 PAD_PETE_0017,
 PAD_PETE_0018,
 PAD_PETE_0019,
 PAD_PETE_001A,
 PAD_PETE_001B,
 PAD_PETE_001C,
 PAD_PETE_001D,
 PAD_PETE_001E,
 PAD_PETE_001F,
 PAD_PETE_0020,
 PAD_PETE_0021,
 PAD_PETE_0022,
 PAD_PETE_0023,
 PAD_PETE_0024,
 PAD_PETE_0025,
 PAD_PETE_0026,
 PAD_PETE_0027,
 PAD_PETE_0028,
 PAD_PETE_0029,
 PAD_PETE_002A,
 PAD_PETE_002B,
 PAD_PETE_002C,
 PAD_PETE_002D,
 PAD_PETE_002E,
 PAD_PETE_002F,
 PAD_PETE_0030,
 PAD_PETE_0031,
 PAD_PETE_0032,
 PAD_PETE_0033,
 PAD_PETE_0034,
 PAD_PETE_0035,
 PAD_PETE_0036,
 PAD_PETE_0037,
 PAD_PETE_0038,
 PAD_PETE_0039,
 PAD_PETE_003A,
 PAD_PETE_003B,
 PAD_PETE_003C,
 PAD_PETE_003D,
 PAD_PETE_003E,
 PAD_PETE_003F,
 PAD_PETE_0040,
 PAD_PETE_0041,
 PAD_PETE_0042,
 PAD_PETE_0043,
 PAD_PETE_0044,
 PAD_PETE_0045,
 PAD_PETE_0046,
 PAD_PETE_0047,
 PAD_PETE_0048,
 PAD_PETE_0049,
 PAD_PETE_004A,
 PAD_PETE_004B,
 PAD_PETE_004C,
 PAD_PETE_004D,
 PAD_PETE_004E,
 PAD_PETE_004F,
 PAD_PETE_0050,
 PAD_PETE_0051,
 PAD_PETE_0052,
 PAD_PETE_0053,
 PAD_PETE_0054,
 PAD_PETE_0055,
 PAD_PETE_0056,
 PAD_PETE_0057,
 PAD_PETE_0058,
 PAD_PETE_0059,
 PAD_PETE_005A,
 PAD_PETE_005B,
 PAD_PETE_005C,
 PAD_PETE_005D,
 PAD_PETE_005E,
 PAD_PETE_005F,
 PAD_PETE_0060,
 PAD_PETE_0061,
 PAD_PETE_0062,
 PAD_PETE_0063,
 PAD_PETE_0064,
 PAD_PETE_0065,
 PAD_PETE_0066,
 PAD_PETE_0067,
 PAD_PETE_0068,
 PAD_PETE_0069,
 PAD_PETE_006A,
 PAD_PETE_006B,
 PAD_PETE_006C,
 PAD_PETE_006D,
 PAD_PETE_006E,
 PAD_PETE_006F,
 PAD_PETE_0070,
 PAD_PETE_0071,
 PAD_PETE_0072,
 PAD_PETE_0073,
 PAD_PETE_0074,
 PAD_PETE_0075,
 PAD_PETE_0076,
 PAD_PETE_0077,
 PAD_PETE_0078,
 PAD_PETE_0079,
 PAD_PETE_007A,
 PAD_PETE_007B,
 PAD_PETE_007C,
 PAD_PETE_007D,
 PAD_PETE_007E,
 PAD_PETE_007F,
 PAD_PETE_0080,
 PAD_PETE_0081,
 PAD_PETE_0082,
 PAD_PETE_0083,
 PAD_PETE_0084,
 PAD_PETE_0085,
 PAD_PETE_0086,
 PAD_PETE_0087,
 PAD_PETE_0088,
 PAD_PETE_0089,
 PAD_PETE_008A,
 PAD_PETE_008B,
 PAD_PETE_008C,
 PAD_PETE_008D,
 PAD_PETE_008E,
 PAD_PETE_008F,
 PAD_PETE_0090,
 PAD_PETE_0091,
 PAD_PETE_0092,
 PAD_PETE_0093,
 PAD_PETE_0094,
 PAD_PETE_0095,
 PAD_PETE_0096,
 PAD_PETE_0097,
 PAD_PETE_0098,
 PAD_PETE_0099,
 PAD_PETE_009A,
 PAD_PETE_009B,
 PAD_PETE_009C,
 PAD_PETE_009D,
 PAD_PETE_009E,
 PAD_PETE_009F,
 PAD_PETE_00A0,
 PAD_PETE_00A1,
 PAD_PETE_00A2,
 PAD_PETE_00A3,
 PAD_PETE_00A4,
 PAD_PETE_00A5,
 PAD_PETE_00A6,
 PAD_PETE_00A7,
 PAD_PETE_00A8,
 PAD_PETE_00A9,
 PAD_PETE_00AA,
 PAD_PETE_00AB,
 PAD_PETE_00AC,
 PAD_PETE_00AD,
 PAD_PETE_00AE,
 PAD_PETE_00AF,
 PAD_PETE_00B0,
 PAD_PETE_00B1,
 PAD_PETE_00B2,
 PAD_PETE_00B3,
 PAD_PETE_00B4,
 PAD_PETE_00B5,
 PAD_PETE_00B6,
 PAD_PETE_00B7,
 PAD_PETE_00B8,
 PAD_PETE_00B9,
 PAD_PETE_00BA,
 PAD_PETE_00BB,
 PAD_PETE_00BC,
 PAD_PETE_00BD,
 PAD_PETE_00BE,
 PAD_PETE_00BF,
 PAD_PETE_00C0,
 PAD_PETE_00C1,
 PAD_PETE_00C2,
 PAD_PETE_00C3,
 PAD_PETE_00C4,
 PAD_PETE_00C5,
 PAD_PETE_00C6,
 PAD_PETE_00C7,
 PAD_PETE_00C8,
 PAD_PETE_00C9,
 PAD_PETE_00CA,
 PAD_PETE_00CB,
 PAD_PETE_00CC,
 PAD_PETE_00CD,
 PAD_PETE_00CE,
 PAD_PETE_00CF,
 PAD_PETE_00D0,
 PAD_PETE_00D1,
 PAD_PETE_00D2,
 PAD_PETE_00D3,
 PAD_PETE_00D4,
 PAD_PETE_00D5,
 PAD_PETE_00D6,
 PAD_PETE_00D7,
 PAD_PETE_00D8,
 PAD_PETE_00D9,
 PAD_PETE_00DA,
 PAD_PETE_00DB,
 PAD_PETE_00DC,
 PAD_PETE_00DD,
 PAD_PETE_00DE,
 PAD_PETE_00DF,
 PAD_PETE_00E0,
 PAD_PETE_00E1,
 PAD_PETE_00E2,
 PAD_PETE_00E3,
 PAD_PETE_00E4,
 PAD_PETE_00E5,
 PAD_PETE_00E6,
 PAD_PETE_00E7,
 PAD_PETE_00E8,
 PAD_PETE_00E9,
 PAD_PETE_00EA,
 PAD_PETE_00EB,
 PAD_PETE_00EC,
 PAD_PETE_00ED,
 PAD_PETE_00EE,
 PAD_PETE_00EF,
 PAD_PETE_00F0,
 PAD_PETE_00F1,
 PAD_PETE_00F2,
 PAD_PETE_00F3,
 PAD_PETE_00F4,
 PAD_PETE_00F5,
 PAD_PETE_00F6,
 PAD_PETE_00F7,
 PAD_PETE_00F8,
 PAD_PETE_00F9,
 PAD_PETE_00FA,
 PAD_PETE_00FB,
 PAD_PETE_00FC,
 PAD_PETE_00FD,
 PAD_PETE_00FE,
 PAD_PETE_00FF,
 PAD_PETE_0100,
 PAD_PETE_0101,
 PAD_PETE_0102,
 PAD_PETE_0103,
 PAD_PETE_0104,
 PAD_PETE_0105,
 PAD_PETE_0106,
 PAD_PETE_0107,
 PAD_PETE_0108,
 PAD_PETE_0109,
 PAD_PETE_010A,
 PAD_PETE_010B,
 PAD_PETE_010C,
 PAD_PETE_010D,
 PAD_PETE_010E,
 PAD_PETE_010F,
 PAD_PETE_0110,
 PAD_PETE_0111,
 PAD_PETE_0112,
 PAD_PETE_0113,
 PAD_PETE_0114,
 PAD_PETE_0115,
 PAD_PETE_0116,
 PAD_PETE_0117,
 PAD_PETE_0118,
 PAD_PETE_0119,
 PAD_PETE_011A,
 PAD_PETE_011B,
 PAD_PETE_011C,
 PAD_PETE_011D,
 PAD_PETE_011E,
 PAD_PETE_011F,
 PAD_PETE_0120,
 PAD_PETE_0121,
 PAD_PETE_0122,
 PAD_PETE_0123,
 PAD_PETE_0124,
 PAD_PETE_0125,
 PAD_PETE_END
};
enum pad_ref {
 PAD_REF_0000,
 PAD_REF_0001,
 PAD_REF_0002,
 PAD_REF_0003,
 PAD_REF_0004,
 PAD_REF_0005,
 PAD_REF_0006,
 PAD_REF_0007,
 PAD_REF_0008,
 PAD_REF_0009,
 PAD_REF_000A,
 PAD_REF_000B,
 PAD_REF_000C,
 PAD_REF_000D,
 PAD_REF_000E,
 PAD_REF_000F,
 PAD_REF_0010,
 PAD_REF_0011,
 PAD_REF_0012,
 PAD_REF_0013,
 PAD_REF_0014,
 PAD_REF_0015,
 PAD_REF_0016,
 PAD_REF_0017,
 PAD_REF_0018,
 PAD_REF_0019,
 PAD_REF_001A,
 PAD_REF_001B,
 PAD_REF_001C,
 PAD_REF_001D,
 PAD_REF_001E,
 PAD_REF_001F,
 PAD_REF_0020,
 PAD_REF_0021,
 PAD_REF_0022,
 PAD_REF_0023,
 PAD_REF_0024,
 PAD_REF_0025,
 PAD_REF_0026,
 PAD_REF_0027,
 PAD_REF_0028,
 PAD_REF_0029,
 PAD_REF_002A,
 PAD_REF_002B,
 PAD_REF_002C,
 PAD_REF_002D,
 PAD_REF_002E,
 PAD_REF_002F,
 PAD_REF_0030,
 PAD_REF_0031,
 PAD_REF_0032,
 PAD_REF_0033,
 PAD_REF_0034,
 PAD_REF_0035,
 PAD_REF_0036,
 PAD_REF_0037,
 PAD_REF_0038,
 PAD_REF_0039,
 PAD_REF_003A,
 PAD_REF_003B,
 PAD_REF_003C,
 PAD_REF_003D,
 PAD_REF_003E,
 PAD_REF_003F,
 PAD_REF_0040,
 PAD_REF_0041,
 PAD_REF_0042,
 PAD_REF_0043,
 PAD_REF_0044,
 PAD_REF_0045,
 PAD_REF_0046,
 PAD_REF_0047,
 PAD_REF_0048,
 PAD_REF_0049,
 PAD_REF_004A,
 PAD_REF_004B,
 PAD_REF_004C,
 PAD_REF_004D,
 PAD_REF_004E,
 PAD_REF_004F,
 PAD_REF_0050,
 PAD_REF_0051,
 PAD_REF_0052,
 PAD_REF_0053,
 PAD_REF_0054,
 PAD_REF_0055,
 PAD_REF_0056,
 PAD_REF_0057,
 PAD_REF_0058,
 PAD_REF_0059,
 PAD_REF_005A,
 PAD_REF_005B,
 PAD_REF_005C,
 PAD_REF_005D,
 PAD_REF_005E,
 PAD_REF_005F,
 PAD_REF_0060,
 PAD_REF_0061,
 PAD_REF_0062,
 PAD_REF_0063,
 PAD_REF_0064,
 PAD_REF_0065,
 PAD_REF_0066,
 PAD_REF_0067,
 PAD_REF_0068,
 PAD_REF_0069,
 PAD_REF_006A,
 PAD_REF_006B,
 PAD_REF_006C,
 PAD_REF_006D,
 PAD_REF_006E,
 PAD_REF_006F,
 PAD_REF_0070,
 PAD_REF_0071,
 PAD_REF_0072,
 PAD_REF_0073,
 PAD_REF_0074,
 PAD_REF_0075,
 PAD_REF_0076,
 PAD_REF_0077,
 PAD_REF_0078,
 PAD_REF_0079,
 PAD_REF_007A,
 PAD_REF_007B,
 PAD_REF_007C,
 PAD_REF_007D,
 PAD_REF_007E,
 PAD_REF_007F,
 PAD_REF_0080,
 PAD_REF_0081,
 PAD_REF_0082,
 PAD_REF_0083,
 PAD_REF_0084,
 PAD_REF_0085,
 PAD_REF_0086,
 PAD_REF_0087,
 PAD_REF_0088,
 PAD_REF_0089,
 PAD_REF_008A,
 PAD_REF_008B,
 PAD_REF_008C,
 PAD_REF_008D,
 PAD_REF_008E,
 PAD_REF_008F,
 PAD_REF_0090,
 PAD_REF_0091,
 PAD_REF_0092,
 PAD_REF_0093,
 PAD_REF_0094,
 PAD_REF_0095,
 PAD_REF_0096,
 PAD_REF_0097,
 PAD_REF_0098,
 PAD_REF_0099,
 PAD_REF_009A,
 PAD_REF_009B,
 PAD_REF_009C,
 PAD_REF_009D,
 PAD_REF_009E,
 PAD_REF_009F,
 PAD_REF_00A0,
 PAD_REF_00A1,
 PAD_REF_00A2,
 PAD_REF_00A3,
 PAD_REF_00A4,
 PAD_REF_00A5,
 PAD_REF_00A6,
 PAD_REF_00A7,
 PAD_REF_00A8,
 PAD_REF_00A9,
 PAD_REF_00AA,
 PAD_REF_00AB,
 PAD_REF_00AC,
 PAD_REF_00AD,
 PAD_REF_00AE,
 PAD_REF_00AF,
 PAD_REF_00B0,
 PAD_REF_00B1,
 PAD_REF_00B2,
 PAD_REF_00B3,
 PAD_REF_00B4,
 PAD_REF_00B5,
 PAD_REF_00B6,
 PAD_REF_00B7,
 PAD_REF_00B8,
 PAD_REF_00B9,
 PAD_REF_00BA,
 PAD_REF_00BB,
 PAD_REF_00BC,
 PAD_REF_00BD,
 PAD_REF_00BE,
 PAD_REF_00BF,
 PAD_REF_00C0,
 PAD_REF_00C1,
 PAD_REF_00C2,
 PAD_REF_00C3,
 PAD_REF_00C4,
 PAD_REF_00C5,
 PAD_REF_00C6,
 PAD_REF_00C7,
 PAD_REF_00C8,
 PAD_REF_00C9,
 PAD_REF_00CA,
 PAD_REF_00CB,
 PAD_REF_00CC,
 PAD_REF_00CD,
 PAD_REF_00CE,
 PAD_REF_00CF,
 PAD_REF_00D0,
 PAD_REF_00D1,
 PAD_REF_00D2,
 PAD_REF_00D3,
 PAD_REF_00D4,
 PAD_REF_00D5,
 PAD_REF_00D6,
 PAD_REF_00D7,
 PAD_REF_00D8,
 PAD_REF_00D9,
 PAD_REF_00DA,
 PAD_REF_00DB,
 PAD_REF_00DC,
 PAD_REF_00DD,
 PAD_REF_00DE,
 PAD_REF_00DF,
 PAD_REF_00E0,
 PAD_REF_00E1,
 PAD_REF_END
};
enum pad_rit {
 PAD_RIT_0000,
 PAD_RIT_0001,
 PAD_RIT_0002,
 PAD_RIT_0003,
 PAD_RIT_0004,
 PAD_RIT_0005,
 PAD_RIT_0006,
 PAD_RIT_0007,
 PAD_RIT_0008,
 PAD_RIT_0009,
 PAD_RIT_000A,
 PAD_RIT_000B,
 PAD_RIT_000C,
 PAD_RIT_000D,
 PAD_RIT_000E,
 PAD_RIT_000F,
 PAD_RIT_0010,
 PAD_RIT_0011,
 PAD_RIT_0012,
 PAD_RIT_0013,
 PAD_RIT_0014,
 PAD_RIT_0015,
 PAD_RIT_0016,
 PAD_RIT_0017,
 PAD_RIT_0018,
 PAD_RIT_0019,
 PAD_RIT_001A,
 PAD_RIT_001B,
 PAD_RIT_001C,
 PAD_RIT_001D,
 PAD_RIT_001E,
 PAD_RIT_001F,
 PAD_RIT_0020,
 PAD_RIT_0021,
 PAD_RIT_0022,
 PAD_RIT_0023,
 PAD_RIT_0024,
 PAD_RIT_0025,
 PAD_RIT_0026,
 PAD_RIT_0027,
 PAD_RIT_0028,
 PAD_RIT_0029,
 PAD_RIT_002A,
 PAD_RIT_002B,
 PAD_RIT_002C,
 PAD_RIT_002D,
 PAD_RIT_002E,
 PAD_RIT_002F,
 PAD_RIT_0030,
 PAD_RIT_0031,
 PAD_RIT_0032,
 PAD_RIT_0033,
 PAD_RIT_0034,
 PAD_RIT_0035,
 PAD_RIT_0036,
 PAD_RIT_0037,
 PAD_RIT_0038,
 PAD_RIT_0039,
 PAD_RIT_003A,
 PAD_RIT_003B,
 PAD_RIT_003C,
 PAD_RIT_003D,
 PAD_RIT_003E,
 PAD_RIT_003F,
 PAD_RIT_0040,
 PAD_RIT_0041,
 PAD_RIT_0042,
 PAD_RIT_0043,
 PAD_RIT_0044,
 PAD_RIT_0045,
 PAD_RIT_0046,
 PAD_RIT_0047,
 PAD_RIT_0048,
 PAD_RIT_0049,
 PAD_RIT_004A,
 PAD_RIT_004B,
 PAD_RIT_004C,
 PAD_RIT_004D,
 PAD_RIT_004E,
 PAD_RIT_004F,
 PAD_RIT_0050,
 PAD_RIT_0051,
 PAD_RIT_0052,
 PAD_RIT_0053,
 PAD_RIT_0054,
 PAD_RIT_0055,
 PAD_RIT_0056,
 PAD_RIT_0057,
 PAD_RIT_0058,
 PAD_RIT_0059,
 PAD_RIT_005A,
 PAD_RIT_005B,
 PAD_RIT_005C,
 PAD_RIT_005D,
 PAD_RIT_005E,
 PAD_RIT_005F,
 PAD_RIT_0060,
 PAD_RIT_0061,
 PAD_RIT_0062,
 PAD_RIT_0063,
 PAD_RIT_0064,
 PAD_RIT_0065,
 PAD_RIT_0066,
 PAD_RIT_0067,
 PAD_RIT_0068,
 PAD_RIT_0069,
 PAD_RIT_006A,
 PAD_RIT_006B,
 PAD_RIT_006C,
 PAD_RIT_006D,
 PAD_RIT_006E,
 PAD_RIT_006F,
 PAD_RIT_0070,
 PAD_RIT_0071,
 PAD_RIT_0072,
 PAD_RIT_0073,
 PAD_RIT_0074,
 PAD_RIT_0075,
 PAD_RIT_0076,
 PAD_RIT_0077,
 PAD_RIT_0078,
 PAD_RIT_0079,
 PAD_RIT_007A,
 PAD_RIT_007B,
 PAD_RIT_007C,
 PAD_RIT_007D,
 PAD_RIT_007E,
 PAD_RIT_007F,
 PAD_RIT_0080,
 PAD_RIT_0081,
 PAD_RIT_0082,
 PAD_RIT_0083,
 PAD_RIT_0084,
 PAD_RIT_0085,
 PAD_RIT_0086,
 PAD_RIT_0087,
 PAD_RIT_0088,
 PAD_RIT_0089,
 PAD_RIT_008A,
 PAD_RIT_008B,
 PAD_RIT_008C,
 PAD_RIT_008D,
 PAD_RIT_008E,
 PAD_RIT_008F,
 PAD_RIT_0090,
 PAD_RIT_0091,
 PAD_RIT_0092,
 PAD_RIT_0093,
 PAD_RIT_0094,
 PAD_RIT_0095,
 PAD_RIT_0096,
 PAD_RIT_0097,
 PAD_RIT_0098,
 PAD_RIT_0099,
 PAD_RIT_009A,
 PAD_RIT_009B,
 PAD_RIT_009C,
 PAD_RIT_009D,
 PAD_RIT_009E,
 PAD_RIT_009F,
 PAD_RIT_00A0,
 PAD_RIT_00A1,
 PAD_RIT_00A2,
 PAD_RIT_00A3,
 PAD_RIT_00A4,
 PAD_RIT_00A5,
 PAD_RIT_00A6,
 PAD_RIT_00A7,
 PAD_RIT_00A8,
 PAD_RIT_00A9,
 PAD_RIT_00AA,
 PAD_RIT_00AB,
 PAD_RIT_00AC,
 PAD_RIT_00AD,
 PAD_RIT_00AE,
 PAD_RIT_00AF,
 PAD_RIT_00B0,
 PAD_RIT_00B1,
 PAD_RIT_00B2,
 PAD_RIT_00B3,
 PAD_RIT_00B4,
 PAD_RIT_00B5,
 PAD_RIT_00B6,
 PAD_RIT_00B7,
 PAD_RIT_00B8,
 PAD_RIT_00B9,
 PAD_RIT_00BA,
 PAD_RIT_00BB,
 PAD_RIT_00BC,
 PAD_RIT_00BD,
 PAD_RIT_00BE,
 PAD_RIT_00BF,
 PAD_RIT_00C0,
 PAD_RIT_00C1,
 PAD_RIT_00C2,
 PAD_RIT_00C3,
 PAD_RIT_00C4,
 PAD_RIT_00C5,
 PAD_RIT_00C6,
 PAD_RIT_00C7,
 PAD_RIT_00C8,
 PAD_RIT_00C9,
 PAD_RIT_00CA,
 PAD_RIT_00CB,
 PAD_RIT_00CC,
 PAD_RIT_00CD,
 PAD_RIT_00CE,
 PAD_RIT_00CF,
 PAD_RIT_00D0,
 PAD_RIT_00D1,
 PAD_RIT_00D2,
 PAD_RIT_00D3,
 PAD_RIT_00D4,
 PAD_RIT_00D5,
 PAD_RIT_00D6,
 PAD_RIT_00D7,
 PAD_RIT_00D8,
 PAD_RIT_00D9,
 PAD_RIT_00DA,
 PAD_RIT_00DB,
 PAD_RIT_00DC,
 PAD_RIT_00DD,
 PAD_RIT_00DE,
 PAD_RIT_00DF,
 PAD_RIT_00E0,
 PAD_RIT_00E1,
 PAD_RIT_00E2,
 PAD_RIT_00E3,
 PAD_RIT_00E4,
 PAD_RIT_00E5,
 PAD_RIT_00E6,
 PAD_RIT_00E7,
 PAD_RIT_00E8,
 PAD_RIT_00E9,
 PAD_RIT_00EA,
 PAD_RIT_00EB,
 PAD_RIT_00EC,
 PAD_RIT_00ED,
 PAD_RIT_00EE,
 PAD_RIT_00EF,
 PAD_RIT_00F0,
 PAD_RIT_00F1,
 PAD_RIT_00F2,
 PAD_RIT_00F3,
 PAD_RIT_00F4,
 PAD_RIT_00F5,
 PAD_RIT_00F6,
 PAD_RIT_00F7,
 PAD_RIT_00F8,
 PAD_RIT_00F9,
 PAD_RIT_00FA,
 PAD_RIT_00FB,
 PAD_RIT_00FC,
 PAD_RIT_00FD,
 PAD_RIT_00FE,
 PAD_RIT_00FF,
 PAD_RIT_0100,
 PAD_RIT_0101,
 PAD_RIT_0102,
 PAD_RIT_0103,
 PAD_RIT_0104,
 PAD_RIT_0105,
 PAD_RIT_0106,
 PAD_RIT_0107,
 PAD_RIT_0108,
 PAD_RIT_0109,
 PAD_RIT_010A,
 PAD_RIT_010B,
 PAD_RIT_010C,
 PAD_RIT_010D,
 PAD_RIT_010E,
 PAD_RIT_010F,
 PAD_RIT_0110,
 PAD_RIT_0111,
 PAD_RIT_0112,
 PAD_RIT_0113,
 PAD_RIT_0114,
 PAD_RIT_0115,
 PAD_RIT_0116,
 PAD_RIT_0117,
 PAD_RIT_0118,
 PAD_RIT_0119,
 PAD_RIT_011A,
 PAD_RIT_011B,
 PAD_RIT_011C,
 PAD_RIT_011D,
 PAD_RIT_011E,
 PAD_RIT_011F,
 PAD_RIT_0120,
 PAD_RIT_0121,
 PAD_RIT_0122,
 PAD_RIT_0123,
 PAD_RIT_0124,
 PAD_RIT_0125,
 PAD_RIT_0126,
 PAD_RIT_0127,
 PAD_RIT_0128,
 PAD_RIT_0129,
 PAD_RIT_012A,
 PAD_RIT_012B,
 PAD_RIT_012C,
 PAD_RIT_012D,
 PAD_RIT_012E,
 PAD_RIT_012F,
 PAD_RIT_0130,
 PAD_RIT_0131,
 PAD_RIT_0132,
 PAD_RIT_0133,
 PAD_RIT_0134,
 PAD_RIT_0135,
 PAD_RIT_0136,
 PAD_RIT_0137,
 PAD_RIT_0138,
 PAD_RIT_0139,
 PAD_RIT_013A,
 PAD_RIT_013B,
 PAD_RIT_013C,
 PAD_RIT_013D,
 PAD_RIT_013E,
 PAD_RIT_013F,
 PAD_RIT_0140,
 PAD_RIT_0141,
 PAD_RIT_0142,
 PAD_RIT_0143,
 PAD_RIT_0144,
 PAD_RIT_0145,
 PAD_RIT_0146,
 PAD_RIT_0147,
 PAD_RIT_0148,
 PAD_RIT_0149,
 PAD_RIT_014A,
 PAD_RIT_014B,
 PAD_RIT_014C,
 PAD_RIT_014D,
 PAD_RIT_014E,
 PAD_RIT_014F,
 PAD_RIT_0150,
 PAD_RIT_0151,
 PAD_RIT_0152,
 PAD_RIT_0153,
 PAD_RIT_0154,
 PAD_RIT_0155,
 PAD_RIT_0156,
 PAD_RIT_0157,
 PAD_RIT_0158,
 PAD_RIT_0159,
 PAD_RIT_015A,
 PAD_RIT_015B,
 PAD_RIT_015C,
 PAD_RIT_015D,
 PAD_RIT_015E,
 PAD_RIT_015F,
 PAD_RIT_0160,
 PAD_RIT_0161,
 PAD_RIT_0162,
 PAD_RIT_0163,
 PAD_RIT_0164,
 PAD_RIT_0165,
 PAD_RIT_0166,
 PAD_RIT_0167,
 PAD_RIT_0168,
 PAD_RIT_0169,
 PAD_RIT_016A,
 PAD_RIT_016B,
 PAD_RIT_016C,
 PAD_RIT_016D,
 PAD_RIT_016E,
 PAD_RIT_016F,
 PAD_RIT_0170,
 PAD_RIT_0171,
 PAD_RIT_END
};
enum pad_run {
 PAD_RUN_END
};
enum pad_sevb {
 PAD_SEVB_END
};
enum pad_sev {
 PAD_SEV_0000,
 PAD_SEV_0001,
 PAD_SEV_0002,
 PAD_SEV_0003,
 PAD_SEV_0004,
 PAD_SEV_0005,
 PAD_SEV_0006,
 PAD_SEV_0007,
 PAD_SEV_0008,
 PAD_SEV_0009,
 PAD_SEV_000A,
 PAD_SEV_000B,
 PAD_SEV_000C,
 PAD_SEV_000D,
 PAD_SEV_000E,
 PAD_SEV_000F,
 PAD_SEV_0010,
 PAD_SEV_0011,
 PAD_SEV_0012,
 PAD_SEV_0013,
 PAD_SEV_0014,
 PAD_SEV_0015,
 PAD_SEV_0016,
 PAD_SEV_0017,
 PAD_SEV_0018,
 PAD_SEV_0019,
 PAD_SEV_001A,
 PAD_SEV_001B,
 PAD_SEV_001C,
 PAD_SEV_001D,
 PAD_SEV_001E,
 PAD_SEV_001F,
 PAD_SEV_0020,
 PAD_SEV_0021,
 PAD_SEV_0022,
 PAD_SEV_0023,
 PAD_SEV_0024,
 PAD_SEV_0025,
 PAD_SEV_0026,
 PAD_SEV_0027,
 PAD_SEV_0028,
 PAD_SEV_0029,
 PAD_SEV_002A,
 PAD_SEV_002B,
 PAD_SEV_002C,
 PAD_SEV_002D,
 PAD_SEV_002E,
 PAD_SEV_002F,
 PAD_SEV_0030,
 PAD_SEV_0031,
 PAD_SEV_0032,
 PAD_SEV_0033,
 PAD_SEV_0034,
 PAD_SEV_0035,
 PAD_SEV_0036,
 PAD_SEV_0037,
 PAD_SEV_0038,
 PAD_SEV_0039,
 PAD_SEV_003A,
 PAD_SEV_003B,
 PAD_SEV_003C,
 PAD_SEV_003D,
 PAD_SEV_003E,
 PAD_SEV_003F,
 PAD_SEV_0040,
 PAD_SEV_0041,
 PAD_SEV_0042,
 PAD_SEV_0043,
 PAD_SEV_0044,
 PAD_SEV_0045,
 PAD_SEV_0046,
 PAD_SEV_0047,
 PAD_SEV_0048,
 PAD_SEV_0049,
 PAD_SEV_004A,
 PAD_SEV_004B,
 PAD_SEV_004C,
 PAD_SEV_004D,
 PAD_SEV_004E,
 PAD_SEV_004F,
 PAD_SEV_0050,
 PAD_SEV_0051,
 PAD_SEV_0052,
 PAD_SEV_0053,
 PAD_SEV_0054,
 PAD_SEV_0055,
 PAD_SEV_0056,
 PAD_SEV_0057,
 PAD_SEV_0058,
 PAD_SEV_0059,
 PAD_SEV_005A,
 PAD_SEV_005B,
 PAD_SEV_005C,
 PAD_SEV_005D,
 PAD_SEV_005E,
 PAD_SEV_005F,
 PAD_SEV_0060,
 PAD_SEV_0061,
 PAD_SEV_0062,
 PAD_SEV_0063,
 PAD_SEV_0064,
 PAD_SEV_0065,
 PAD_SEV_0066,
 PAD_SEV_0067,
 PAD_SEV_0068,
 PAD_SEV_0069,
 PAD_SEV_006A,
 PAD_SEV_006B,
 PAD_SEV_006C,
 PAD_SEV_006D,
 PAD_SEV_006E,
 PAD_SEV_006F,
 PAD_SEV_0070,
 PAD_SEV_0071,
 PAD_SEV_0072,
 PAD_SEV_0073,
 PAD_SEV_0074,
 PAD_SEV_0075,
 PAD_SEV_0076,
 PAD_SEV_0077,
 PAD_SEV_0078,
 PAD_SEV_0079,
 PAD_SEV_007A,
 PAD_SEV_007B,
 PAD_SEV_007C,
 PAD_SEV_007D,
 PAD_SEV_007E,
 PAD_SEV_007F,
 PAD_SEV_0080,
 PAD_SEV_0081,
 PAD_SEV_0082,
 PAD_SEV_0083,
 PAD_SEV_0084,
 PAD_SEV_0085,
 PAD_SEV_0086,
 PAD_SEV_0087,
 PAD_SEV_0088,
 PAD_SEV_0089,
 PAD_SEV_008A,
 PAD_SEV_008B,
 PAD_SEV_008C,
 PAD_SEV_008D,
 PAD_SEV_008E,
 PAD_SEV_008F,
 PAD_SEV_0090,
 PAD_SEV_0091,
 PAD_SEV_0092,
 PAD_SEV_0093,
 PAD_SEV_0094,
 PAD_SEV_0095,
 PAD_SEV_0096,
 PAD_SEV_0097,
 PAD_SEV_0098,
 PAD_SEV_0099,
 PAD_SEV_009A,
 PAD_SEV_009B,
 PAD_SEV_009C,
 PAD_SEV_009D,
 PAD_SEV_009E,
 PAD_SEV_009F,
 PAD_SEV_00A0,
 PAD_SEV_00A1,
 PAD_SEV_00A2,
 PAD_SEV_00A3,
 PAD_SEV_00A4,
 PAD_SEV_00A5,
 PAD_SEV_00A6,
 PAD_SEV_00A7,
 PAD_SEV_00A8,
 PAD_SEV_00A9,
 PAD_SEV_00AA,
 PAD_SEV_00AB,
 PAD_SEV_00AC,
 PAD_SEV_00AD,
 PAD_SEV_00AE,
 PAD_SEV_00AF,
 PAD_SEV_00B0,
 PAD_SEV_00B1,
 PAD_SEV_00B2,
 PAD_SEV_00B3,
 PAD_SEV_00B4,
 PAD_SEV_00B5,
 PAD_SEV_00B6,
 PAD_SEV_00B7,
 PAD_SEV_00B8,
 PAD_SEV_00B9,
 PAD_SEV_00BA,
 PAD_SEV_00BB,
 PAD_SEV_00BC,
 PAD_SEV_00BD,
 PAD_SEV_00BE,
 PAD_SEV_00BF,
 PAD_SEV_00C0,
 PAD_SEV_00C1,
 PAD_SEV_00C2,
 PAD_SEV_00C3,
 PAD_SEV_00C4,
 PAD_SEV_00C5,
 PAD_SEV_00C6,
 PAD_SEV_00C7,
 PAD_SEV_00C8,
 PAD_SEV_00C9,
 PAD_SEV_00CA,
 PAD_SEV_00CB,
 PAD_SEV_00CC,
 PAD_SEV_00CD,
 PAD_SEV_00CE,
 PAD_SEV_00CF,
 PAD_SEV_00D0,
 PAD_SEV_00D1,
 PAD_SEV_00D2,
 PAD_SEV_00D3,
 PAD_SEV_00D4,
 PAD_SEV_00D5,
 PAD_SEV_00D6,
 PAD_SEV_00D7,
 PAD_SEV_00D8,
 PAD_SEV_00D9,
 PAD_SEV_00DA,
 PAD_SEV_00DB,
 PAD_SEV_00DC,
 PAD_SEV_00DD,
 PAD_SEV_00DE,
 PAD_SEV_00DF,
 PAD_SEV_00E0,
 PAD_SEV_00E1,
 PAD_SEV_00E2,
 PAD_SEV_00E3,
 PAD_SEV_00E4,
 PAD_SEV_00E5,
 PAD_SEV_00E6,
 PAD_SEV_00E7,
 PAD_SEV_00E8,
 PAD_SEV_00E9,
 PAD_SEV_00EA,
 PAD_SEV_00EB,
 PAD_SEV_00EC,
 PAD_SEV_00ED,
 PAD_SEV_00EE,
 PAD_SEV_00EF,
 PAD_SEV_00F0,
 PAD_SEV_00F1,
 PAD_SEV_00F2,
 PAD_SEV_00F3,
 PAD_SEV_00F4,
 PAD_SEV_00F5,
 PAD_SEV_00F6,
 PAD_SEV_00F7,
 PAD_SEV_00F8,
 PAD_SEV_00F9,
 PAD_SEV_00FA,
 PAD_SEV_00FB,
 PAD_SEV_00FC,
 PAD_SEV_00FD,
 PAD_SEV_00FE,
 PAD_SEV_00FF,
 PAD_SEV_0100,
 PAD_SEV_0101,
 PAD_SEV_0102,
 PAD_SEV_0103,
 PAD_SEV_0104,
 PAD_SEV_0105,
 PAD_SEV_0106,
 PAD_SEV_0107,
 PAD_SEV_0108,
 PAD_SEV_0109,
 PAD_SEV_010A,
 PAD_SEV_010B,
 PAD_SEV_010C,
 PAD_SEV_010D,
 PAD_SEV_010E,
 PAD_SEV_010F,
 PAD_SEV_0110,
 PAD_SEV_0111,
 PAD_SEV_0112,
 PAD_SEV_0113,
 PAD_SEV_0114,
 PAD_SEV_0115,
 PAD_SEV_0116,
 PAD_SEV_0117,
 PAD_SEV_0118,
 PAD_SEV_0119,
 PAD_SEV_011A,
 PAD_SEV_011B,
 PAD_SEV_011C,
 PAD_SEV_011D,
 PAD_SEV_011E,
 PAD_SEV_011F,
 PAD_SEV_0120,
 PAD_SEV_0121,
 PAD_SEV_0122,
 PAD_SEV_0123,
 PAD_SEV_0124,
 PAD_SEV_0125,
 PAD_SEV_0126,
 PAD_SEV_0127,
 PAD_SEV_0128,
 PAD_SEV_0129,
 PAD_SEV_012A,
 PAD_SEV_012B,
 PAD_SEV_012C,
 PAD_SEV_012D,
 PAD_SEV_012E,
 PAD_SEV_012F,
 PAD_SEV_0130,
 PAD_SEV_0131,
 PAD_SEV_0132,
 PAD_SEV_0133,
 PAD_SEV_0134,
 PAD_SEV_0135,
 PAD_SEV_0136,
 PAD_SEV_0137,
 PAD_SEV_0138,
 PAD_SEV_0139,
 PAD_SEV_013A,
 PAD_SEV_013B,
 PAD_SEV_013C,
 PAD_SEV_013D,
 PAD_SEV_013E,
 PAD_SEV_013F,
 PAD_SEV_0140,
 PAD_SEV_0141,
 PAD_SEV_0142,
 PAD_SEV_0143,
 PAD_SEV_0144,
 PAD_SEV_0145,
 PAD_SEV_0146,
 PAD_SEV_0147,
 PAD_SEV_0148,
 PAD_SEV_0149,
 PAD_SEV_014A,
 PAD_SEV_014B,
 PAD_SEV_014C,
 PAD_SEV_014D,
 PAD_SEV_014E,
 PAD_SEV_014F,
 PAD_SEV_0150,
 PAD_SEV_0151,
 PAD_SEV_0152,
 PAD_SEV_0153,
 PAD_SEV_0154,
 PAD_SEV_0155,
 PAD_SEV_0156,
 PAD_SEV_0157,
 PAD_SEV_0158,
 PAD_SEV_0159,
 PAD_SEV_015A,
 PAD_SEV_015B,
 PAD_SEV_015C,
 PAD_SEV_015D,
 PAD_SEV_015E,
 PAD_SEV_015F,
 PAD_SEV_0160,
 PAD_SEV_0161,
 PAD_SEV_0162,
 PAD_SEV_0163,
 PAD_SEV_0164,
 PAD_SEV_0165,
 PAD_SEV_0166,
 PAD_SEV_0167,
 PAD_SEV_0168,
 PAD_SEV_0169,
 PAD_SEV_016A,
 PAD_SEV_016B,
 PAD_SEV_016C,
 PAD_SEV_016D,
 PAD_SEV_016E,
 PAD_SEV_016F,
 PAD_SEV_0170,
 PAD_SEV_0171,
 PAD_SEV_0172,
 PAD_SEV_0173,
 PAD_SEV_0174,
 PAD_SEV_0175,
 PAD_SEV_0176,
 PAD_SEV_0177,
 PAD_SEV_0178,
 PAD_SEV_0179,
 PAD_SEV_017A,
 PAD_SEV_017B,
 PAD_SEV_017C,
 PAD_SEV_017D,
 PAD_SEV_017E,
 PAD_SEV_017F,
 PAD_SEV_0180,
 PAD_SEV_0181,
 PAD_SEV_0182,
 PAD_SEV_0183,
 PAD_SEV_0184,
 PAD_SEV_0185,
 PAD_SEV_0186,
 PAD_SEV_0187,
 PAD_SEV_0188,
 PAD_SEV_0189,
 PAD_SEV_018A,
 PAD_SEV_018B,
 PAD_SEV_018C,
 PAD_SEV_018D,
 PAD_SEV_018E,
 PAD_SEV_018F,
 PAD_SEV_0190,
 PAD_SEV_0191,
 PAD_SEV_0192,
 PAD_SEV_0193,
 PAD_SEV_0194,
 PAD_SEV_0195,
 PAD_SEV_0196,
 PAD_SEV_0197,
 PAD_SEV_0198,
 PAD_SEV_0199,
 PAD_SEV_019A,
 PAD_SEV_019B,
 PAD_SEV_019C,
 PAD_SEV_019D,
 PAD_SEV_019E,
 PAD_SEV_019F,
 PAD_SEV_01A0,
 PAD_SEV_01A1,
 PAD_SEV_01A2,
 PAD_SEV_01A3,
 PAD_SEV_01A4,
 PAD_SEV_01A5,
 PAD_SEV_01A6,
 PAD_SEV_01A7,
 PAD_SEV_01A8,
 PAD_SEV_01A9,
 PAD_SEV_01AA,
 PAD_SEV_01AB,
 PAD_SEV_01AC,
 PAD_SEV_01AD,
 PAD_SEV_01AE,
 PAD_SEV_01AF,
 PAD_SEV_01B0,
 PAD_SEV_01B1,
 PAD_SEV_01B2,
 PAD_SEV_01B3,
 PAD_SEV_01B4,
 PAD_SEV_01B5,
 PAD_SEV_01B6,
 PAD_SEV_01B7,
 PAD_SEV_01B8,
 PAD_SEV_01B9,
 PAD_SEV_01BA,
 PAD_SEV_01BB,
 PAD_SEV_01BC,
 PAD_SEV_01BD,
 PAD_SEV_01BE,
 PAD_SEV_01BF,
 PAD_SEV_01C0,
 PAD_SEV_01C1,
 PAD_SEV_01C2,
 PAD_SEV_01C3,
 PAD_SEV_01C4,
 PAD_SEV_01C5,
 PAD_SEV_01C6,
 PAD_SEV_01C7,
 PAD_SEV_01C8,
 PAD_SEV_01C9,
 PAD_SEV_01CA,
 PAD_SEV_01CB,
 PAD_SEV_01CC,
 PAD_SEV_01CD,
 PAD_SEV_01CE,
 PAD_SEV_01CF,
 PAD_SEV_01D0,
 PAD_SEV_01D1,
 PAD_SEV_01D2,
 PAD_SEV_01D3,
 PAD_SEV_01D4,
 PAD_SEV_01D5,
 PAD_SEV_01D6,
 PAD_SEV_01D7,
 PAD_SEV_01D8,
 PAD_SEV_01D9,
 PAD_SEV_01DA,
 PAD_SEV_01DB,
 PAD_SEV_01DC,
 PAD_SEV_01DD,
 PAD_SEV_01DE,
 PAD_SEV_01DF,
 PAD_SEV_01E0,
 PAD_SEV_01E1,
 PAD_SEV_01E2,
 PAD_SEV_01E3,
 PAD_SEV_01E4,
 PAD_SEV_01E5,
 PAD_SEV_01E6,
 PAD_SEV_01E7,
 PAD_SEV_01E8,
 PAD_SEV_01E9,
 PAD_SEV_01EA,
 PAD_SEV_01EB,
 PAD_SEV_01EC,
 PAD_SEV_01ED,
 PAD_SEV_01EE,
 PAD_SEV_01EF,
 PAD_SEV_01F0,
 PAD_SEV_01F1,
 PAD_SEV_01F2,
 PAD_SEV_01F3,
 PAD_SEV_01F4,
 PAD_SEV_01F5,
 PAD_SEV_01F6,
 PAD_SEV_01F7,
 PAD_SEV_01F8,
 PAD_SEV_01F9,
 PAD_SEV_01FA,
 PAD_SEV_01FB,
 PAD_SEV_01FC,
 PAD_SEV_01FD,
 PAD_SEV_01FE,
 PAD_SEV_01FF,
 PAD_SEV_0200,
 PAD_SEV_0201,
 PAD_SEV_0202,
 PAD_SEV_0203,
 PAD_SEV_0204,
 PAD_SEV_0205,
 PAD_SEV_0206,
 PAD_SEV_0207,
 PAD_SEV_0208,
 PAD_SEV_0209,
 PAD_SEV_020A,
 PAD_SEV_020B,
 PAD_SEV_020C,
 PAD_SEV_020D,
 PAD_SEV_020E,
 PAD_SEV_020F,
 PAD_SEV_0210,
 PAD_SEV_0211,
 PAD_SEV_0212,
 PAD_SEV_0213,
 PAD_SEV_0214,
 PAD_SEV_0215,
 PAD_SEV_0216,
 PAD_SEV_0217,
 PAD_SEV_0218,
 PAD_SEV_0219,
 PAD_SEV_021A,
 PAD_SEV_021B,
 PAD_SEV_021C,
 PAD_SEV_021D,
 PAD_SEV_021E,
 PAD_SEV_021F,
 PAD_SEV_0220,
 PAD_SEV_0221,
 PAD_SEV_0222,
 PAD_SEV_0223,
 PAD_SEV_0224,
 PAD_SEV_0225,
 PAD_SEV_0226,
 PAD_SEV_0227,
 PAD_SEV_0228,
 PAD_SEV_0229,
 PAD_SEV_022A,
 PAD_SEV_022B,
 PAD_SEV_022C,
 PAD_SEV_022D,
 PAD_SEV_022E,
 PAD_SEV_022F,
 PAD_SEV_0230,
 PAD_SEV_0231,
 PAD_SEV_0232,
 PAD_SEV_0233,
 PAD_SEV_0234,
 PAD_SEV_0235,
 PAD_SEV_0236,
 PAD_SEV_0237,
 PAD_SEV_0238,
 PAD_SEV_0239,
 PAD_SEV_023A,
 PAD_SEV_023B,
 PAD_SEV_023C,
 PAD_SEV_023D,
 PAD_SEV_023E,
 PAD_SEV_023F,
 PAD_SEV_0240,
 PAD_SEV_0241,
 PAD_SEV_0242,
 PAD_SEV_0243,
 PAD_SEV_0244,
 PAD_SEV_0245,
 PAD_SEV_0246,
 PAD_SEV_0247,
 PAD_SEV_0248,
 PAD_SEV_0249,
 PAD_SEV_024A,
 PAD_SEV_024B,
 PAD_SEV_024C,
 PAD_SEV_024D,
 PAD_SEV_024E,
 PAD_SEV_024F,
 PAD_SEV_0250,
 PAD_SEV_0251,
 PAD_SEV_0252,
 PAD_SEV_0253,
 PAD_SEV_0254,
 PAD_SEV_0255,
 PAD_SEV_0256,
 PAD_SEV_0257,
 PAD_SEV_0258,
 PAD_SEV_0259,
 PAD_SEV_025A,
 PAD_SEV_025B,
 PAD_SEV_025C,
 PAD_SEV_025D,
 PAD_SEV_025E,
 PAD_SEV_025F,
 PAD_SEV_0260,
 PAD_SEV_0261,
 PAD_SEV_0262,
 PAD_SEV_0263,
 PAD_SEV_0264,
 PAD_SEV_0265,
 PAD_SEV_0266,
 PAD_SEV_0267,
 PAD_SEV_0268,
 PAD_SEV_0269,
 PAD_SEV_026A,
 PAD_SEV_026B,
 PAD_SEV_026C,
 PAD_SEV_026D,
 PAD_SEV_026E,
 PAD_SEV_026F,
 PAD_SEV_0270,
 PAD_SEV_0271,
 PAD_SEV_0272,
 PAD_SEV_0273,
 PAD_SEV_0274,
 PAD_SEV_0275,
 PAD_SEV_0276,
 PAD_SEV_0277,
 PAD_SEV_0278,
 PAD_SEV_0279,
 PAD_SEV_027A,
 PAD_SEV_027B,
 PAD_SEV_027C,
 PAD_SEV_027D,
 PAD_SEV_027E,
 PAD_SEV_027F,
 PAD_SEV_0280,
 PAD_SEV_0281,
 PAD_SEV_0282,
 PAD_SEV_0283,
 PAD_SEV_0284,
 PAD_SEV_0285,
 PAD_SEV_0286,
 PAD_SEV_0287,
 PAD_SEV_0288,
 PAD_SEV_0289,
 PAD_SEV_028A,
 PAD_SEV_028B,
 PAD_SEV_028C,
 PAD_SEV_028D,
 PAD_SEV_028E,
 PAD_SEV_028F,
 PAD_SEV_0290,
 PAD_SEV_0291,
 PAD_SEV_0292,
 PAD_SEV_0293,
 PAD_SEV_0294,
 PAD_SEV_0295,
 PAD_SEV_0296,
 PAD_SEV_0297,
 PAD_SEV_0298,
 PAD_SEV_0299,
 PAD_SEV_029A,
 PAD_SEV_029B,
 PAD_SEV_029C,
 PAD_SEV_029D,
 PAD_SEV_029E,
 PAD_SEV_029F,
 PAD_SEV_02A0,
 PAD_SEV_02A1,
 PAD_SEV_02A2,
 PAD_SEV_02A3,
 PAD_SEV_02A4,
 PAD_SEV_02A5,
 PAD_SEV_02A6,
 PAD_SEV_02A7,
 PAD_SEV_02A8,
 PAD_SEV_02A9,
 PAD_SEV_02AA,
 PAD_SEV_02AB,
 PAD_SEV_02AC,
 PAD_SEV_02AD,
 PAD_SEV_02AE,
 PAD_SEV_02AF,
 PAD_SEV_02B0,
 PAD_SEV_02B1,
 PAD_SEV_02B2,
 PAD_SEV_02B3,
 PAD_SEV_02B4,
 PAD_SEV_02B5,
 PAD_SEV_02B6,
 PAD_SEV_02B7,
 PAD_SEV_02B8,
 PAD_SEV_02B9,
 PAD_SEV_02BA,
 PAD_SEV_02BB,
 PAD_SEV_02BC,
 PAD_SEV_02BD,
 PAD_SEV_02BE,
 PAD_SEV_02BF,
 PAD_SEV_02C0,
 PAD_SEV_02C1,
 PAD_SEV_02C2,
 PAD_SEV_02C3,
 PAD_SEV_02C4,
 PAD_SEV_02C5,
 PAD_SEV_02C6,
 PAD_SEV_02C7,
 PAD_SEV_02C8,
 PAD_SEV_02C9,
 PAD_SEV_02CA,
 PAD_SEV_02CB,
 PAD_SEV_02CC,
 PAD_SEV_02CD,
 PAD_SEV_02CE,
 PAD_SEV_02CF,
 PAD_SEV_02D0,
 PAD_SEV_02D1,
 PAD_SEV_02D2,
 PAD_SEV_02D3,
 PAD_SEV_02D4,
 PAD_SEV_02D5,
 PAD_SEV_02D6,
 PAD_SEV_02D7,
 PAD_SEV_02D8,
 PAD_SEV_02D9,
 PAD_SEV_02DA,
 PAD_SEV_02DB,
 PAD_SEV_02DC,
 PAD_SEV_02DD,
 PAD_SEV_02DE,
 PAD_SEV_02DF,
 PAD_SEV_02E0,
 PAD_SEV_02E1,
 PAD_SEV_02E2,
 PAD_SEV_02E3,
 PAD_SEV_02E4,
 PAD_SEV_02E5,
 PAD_SEV_02E6,
 PAD_SEV_02E7,
 PAD_SEV_02E8,
 PAD_SEV_02E9,
 PAD_SEV_02EA,
 PAD_SEV_02EB,
 PAD_SEV_02EC,
 PAD_SEV_02ED,
 PAD_SEV_02EE,
 PAD_SEV_02EF,
 PAD_SEV_02F0,
 PAD_SEV_02F1,
 PAD_SEV_02F2,
 PAD_SEV_02F3,
 PAD_SEV_02F4,
 PAD_SEV_02F5,
 PAD_SEV_02F6,
 PAD_SEV_02F7,
 PAD_SEV_02F8,
 PAD_SEV_02F9,
 PAD_SEV_02FA,
 PAD_SEV_02FB,
 PAD_SEV_02FC,
 PAD_SEV_02FD,
 PAD_SEV_02FE,
 PAD_SEV_02FF,
 PAD_SEV_0300,
 PAD_SEV_0301,
 PAD_SEV_0302,
 PAD_SEV_0303,
 PAD_SEV_0304,
 PAD_SEV_0305,
 PAD_SEV_0306,
 PAD_SEV_0307,
 PAD_SEV_0308,
 PAD_SEV_0309,
 PAD_SEV_030A,
 PAD_SEV_030B,
 PAD_SEV_030C,
 PAD_SEV_030D,
 PAD_SEV_030E,
 PAD_SEV_030F,
 PAD_SEV_0310,
 PAD_SEV_0311,
 PAD_SEV_0312,
 PAD_SEV_0313,
 PAD_SEV_0314,
 PAD_SEV_0315,
 PAD_SEV_0316,
 PAD_SEV_0317,
 PAD_SEV_0318,
 PAD_SEV_0319,
 PAD_SEV_031A,
 PAD_SEV_031B,
 PAD_SEV_031C,
 PAD_SEV_031D,
 PAD_SEV_031E,
 PAD_SEV_031F,
 PAD_SEV_0320,
 PAD_SEV_0321,
 PAD_SEV_0322,
 PAD_SEV_0323,
 PAD_SEV_0324,
 PAD_SEV_0325,
 PAD_SEV_0326,
 PAD_SEV_0327,
 PAD_SEV_0328,
 PAD_SEV_0329,
 PAD_SEV_032A,
 PAD_SEV_032B,
 PAD_SEV_032C,
 PAD_SEV_032D,
 PAD_SEV_032E,
 PAD_SEV_032F,
 PAD_SEV_0330,
 PAD_SEV_0331,
 PAD_SEV_0332,
 PAD_SEV_0333,
 PAD_SEV_0334,
 PAD_SEV_0335,
 PAD_SEV_0336,
 PAD_SEV_0337,
 PAD_SEV_0338,
 PAD_SEV_0339,
 PAD_SEV_033A,
 PAD_SEV_033B,
 PAD_SEV_033C,
 PAD_SEV_033D,
 PAD_SEV_033E,
 PAD_SEV_033F,
 PAD_SEV_0340,
 PAD_SEV_0341,
 PAD_SEV_0342,
 PAD_SEV_0343,
 PAD_SEV_0344,
 PAD_SEV_0345,
 PAD_SEV_0346,
 PAD_SEV_0347,
 PAD_SEV_0348,
 PAD_SEV_0349,
 PAD_SEV_034A,
 PAD_SEV_034B,
 PAD_SEV_034C,
 PAD_SEV_034D,
 PAD_SEV_034E,
 PAD_SEV_034F,
 PAD_SEV_0350,
 PAD_SEV_0351,
 PAD_SEV_0352,
 PAD_SEV_0353,
 PAD_SEV_0354,
 PAD_SEV_0355,
 PAD_SEV_0356,
 PAD_SEV_0357,
 PAD_SEV_0358,
 PAD_SEV_0359,
 PAD_SEV_035A,
 PAD_SEV_035B,
 PAD_SEV_035C,
 PAD_SEV_035D,
 PAD_SEV_035E,
 PAD_SEV_035F,
 PAD_SEV_0360,
 PAD_SEV_0361,
 PAD_SEV_0362,
 PAD_SEV_0363,
 PAD_SEV_0364,
 PAD_SEV_0365,
 PAD_SEV_0366,
 PAD_SEV_0367,
 PAD_SEV_0368,
 PAD_SEV_0369,
 PAD_SEV_036A,
 PAD_SEV_036B,
 PAD_SEV_036C,
 PAD_SEV_036D,
 PAD_SEV_036E,
 PAD_SEV_036F,
 PAD_SEV_0370,
 PAD_SEV_0371,
 PAD_SEV_0372,
 PAD_SEV_0373,
 PAD_SEV_0374,
 PAD_SEV_0375,
 PAD_SEV_0376,
 PAD_SEV_0377,
 PAD_SEV_0378,
 PAD_SEV_0379,
 PAD_SEV_037A,
 PAD_SEV_037B,
 PAD_SEV_037C,
 PAD_SEV_037D,
 PAD_SEV_037E,
 PAD_SEV_037F,
 PAD_SEV_0380,
 PAD_SEV_0381,
 PAD_SEV_0382,
 PAD_SEV_0383,
 PAD_SEV_0384,
 PAD_SEV_0385,
 PAD_SEV_0386,
 PAD_SEV_0387,
 PAD_SEV_0388,
 PAD_SEV_0389,
 PAD_SEV_038A,
 PAD_SEV_038B,
 PAD_SEV_038C,
 PAD_SEV_038D,
 PAD_SEV_038E,
 PAD_SEV_038F,
 PAD_SEV_0390,
 PAD_SEV_0391,
 PAD_SEV_0392,
 PAD_SEV_0393,
 PAD_SEV_0394,
 PAD_SEV_0395,
 PAD_SEV_0396,
 PAD_SEV_0397,
 PAD_SEV_0398,
 PAD_SEV_0399,
 PAD_SEV_039A,
 PAD_SEV_039B,
 PAD_SEV_039C,
 PAD_SEV_039D,
 PAD_SEV_039E,
 PAD_SEV_039F,
 PAD_SEV_03A0,
 PAD_SEV_03A1,
 PAD_SEV_03A2,
 PAD_SEV_03A3,
 PAD_SEV_03A4,
 PAD_SEV_03A5,
 PAD_SEV_03A6,
 PAD_SEV_03A7,
 PAD_SEV_03A8,
 PAD_SEV_03A9,
 PAD_SEV_03AA,
 PAD_SEV_03AB,
 PAD_SEV_03AC,
 PAD_SEV_03AD,
 PAD_SEV_03AE,
 PAD_SEV_03AF,
 PAD_SEV_03B0,
 PAD_SEV_03B1,
 PAD_SEV_03B2,
 PAD_SEV_03B3,
 PAD_SEV_03B4,
 PAD_SEV_03B5,
 PAD_SEV_03B6,
 PAD_SEV_03B7,
 PAD_SEV_03B8,
 PAD_SEV_03B9,
 PAD_SEV_03BA,
 PAD_SEV_03BB,
 PAD_SEV_03BC,
 PAD_SEV_03BD,
 PAD_SEV_03BE,
 PAD_SEV_03BF,
 PAD_SEV_03C0,
 PAD_SEV_03C1,
 PAD_SEV_03C2,
 PAD_SEV_03C3,
 PAD_SEV_03C4,
 PAD_SEV_03C5,
 PAD_SEV_03C6,
 PAD_SEV_03C7,
 PAD_SEV_03C8,
 PAD_SEV_03C9,
 PAD_SEV_03CA,
 PAD_SEV_03CB,
 PAD_SEV_03CC,
 PAD_SEV_03CD,
 PAD_SEV_03CE,
 PAD_SEV_03CF,
 PAD_SEV_03D0,
 PAD_SEV_03D1,
 PAD_SEV_03D2,
 PAD_SEV_03D3,
 PAD_SEV_03D4,
 PAD_SEV_03D5,
 PAD_SEV_03D6,
 PAD_SEV_03D7,
 PAD_SEV_03D8,
 PAD_SEV_03D9,
 PAD_SEV_03DA,
 PAD_SEV_03DB,
 PAD_SEV_03DC,
 PAD_SEV_03DD,
 PAD_SEV_03DE,
 PAD_SEV_03DF,
 PAD_SEV_03E0,
 PAD_SEV_03E1,
 PAD_SEV_03E2,
 PAD_SEV_03E3,
 PAD_SEV_03E4,
 PAD_SEV_03E5,
 PAD_SEV_03E6,
 PAD_SEV_03E7,
 PAD_SEV_03E8,
 PAD_SEV_03E9,
 PAD_SEV_03EA,
 PAD_SEV_03EB,
 PAD_SEV_03EC,
 PAD_SEV_03ED,
 PAD_SEV_03EE,
 PAD_SEV_03EF,
 PAD_SEV_03F0,
 PAD_SEV_03F1,
 PAD_SEV_03F2,
 PAD_SEV_03F3,
 PAD_SEV_03F4,
 PAD_SEV_03F5,
 PAD_SEV_03F6,
 PAD_SEV_03F7,
 PAD_SEV_03F8,
 PAD_SEV_03F9,
 PAD_SEV_03FA,
 PAD_SEV_03FB,
 PAD_SEV_03FC,
 PAD_SEV_03FD,
 PAD_SEV_03FE,
 PAD_SEV_03FF,
 PAD_SEV_0400,
 PAD_SEV_0401,
 PAD_SEV_0402,
 PAD_SEV_0403,
 PAD_SEV_0404,
 PAD_SEV_0405,
 PAD_SEV_0406,
 PAD_SEV_0407,
 PAD_SEV_0408,
 PAD_SEV_0409,
 PAD_SEV_040A,
 PAD_SEV_040B,
 PAD_SEV_040C,
 PAD_SEV_040D,
 PAD_SEV_040E,
 PAD_SEV_040F,
 PAD_SEV_0410,
 PAD_SEV_0411,
 PAD_SEV_0412,
 PAD_SEV_0413,
 PAD_SEV_0414,
 PAD_SEV_0415,
 PAD_SEV_0416,
 PAD_SEV_0417,
 PAD_SEV_0418,
 PAD_SEV_0419,
 PAD_SEV_041A,
 PAD_SEV_041B,
 PAD_SEV_041C,
 PAD_SEV_041D,
 PAD_SEV_041E,
 PAD_SEV_041F,
 PAD_SEV_0420,
 PAD_SEV_0421,
 PAD_SEV_0422,
 PAD_SEV_0423,
 PAD_SEV_0424,
 PAD_SEV_0425,
 PAD_SEV_0426,
 PAD_SEV_0427,
 PAD_SEV_0428,
 PAD_SEV_0429,
 PAD_SEV_042A,
 PAD_SEV_042B,
 PAD_SEV_042C,
 PAD_SEV_042D,
 PAD_SEV_042E,
 PAD_SEV_042F,
 PAD_SEV_0430,
 PAD_SEV_0431,
 PAD_SEV_0432,
 PAD_SEV_0433,
 PAD_SEV_0434,
 PAD_SEV_0435,
 PAD_SEV_0436,
 PAD_SEV_0437,
 PAD_SEV_0438,
 PAD_SEV_0439,
 PAD_SEV_043A,
 PAD_SEV_043B,
 PAD_SEV_043C,
 PAD_SEV_043D,
 PAD_SEV_043E,
 PAD_SEV_043F,
 PAD_SEV_0440,
 PAD_SEV_0441,
 PAD_SEV_0442,
 PAD_SEV_0443,
 PAD_SEV_0444,
 PAD_SEV_0445,
 PAD_SEV_0446,
 PAD_SEV_0447,
 PAD_SEV_0448,
 PAD_SEV_0449,
 PAD_SEV_044A,
 PAD_SEV_044B,
 PAD_SEV_044C,
 PAD_SEV_044D,
 PAD_SEV_044E,
 PAD_SEV_044F,
 PAD_SEV_0450,
 PAD_SEV_0451,
 PAD_SEV_0452,
 PAD_SEV_0453,
 PAD_SEV_0454,
 PAD_SEV_0455,
 PAD_SEV_0456,
 PAD_SEV_0457,
 PAD_SEV_0458,
 PAD_SEV_0459,
 PAD_SEV_045A,
 PAD_SEV_045B,
 PAD_SEV_045C,
 PAD_SEV_045D,
 PAD_SEV_045E,
 PAD_SEV_045F,
 PAD_SEV_0460,
 PAD_SEV_0461,
 PAD_SEV_0462,
 PAD_SEV_0463,
 PAD_SEV_0464,
 PAD_SEV_0465,
 PAD_SEV_0466,
 PAD_SEV_0467,
 PAD_SEV_0468,
 PAD_SEV_0469,
 PAD_SEV_046A,
 PAD_SEV_046B,
 PAD_SEV_046C,
 PAD_SEV_046D,
 PAD_SEV_046E,
 PAD_SEV_046F,
 PAD_SEV_0470,
 PAD_SEV_0471,
 PAD_SEV_0472,
 PAD_SEV_0473,
 PAD_SEV_0474,
 PAD_SEV_0475,
 PAD_SEV_0476,
 PAD_SEV_0477,
 PAD_SEV_0478,
 PAD_SEV_0479,
 PAD_SEV_047A,
 PAD_SEV_047B,
 PAD_SEV_047C,
 PAD_SEV_047D,
 PAD_SEV_047E,
 PAD_SEV_047F,
 PAD_SEV_0480,
 PAD_SEV_0481,
 PAD_SEV_0482,
 PAD_SEV_0483,
 PAD_SEV_0484,
 PAD_SEV_0485,
 PAD_SEV_0486,
 PAD_SEV_0487,
 PAD_SEV_0488,
 PAD_SEV_0489,
 PAD_SEV_048A,
 PAD_SEV_048B,
 PAD_SEV_048C,
 PAD_SEV_048D,
 PAD_SEV_048E,
 PAD_SEV_048F,
 PAD_SEV_0490,
 PAD_SEV_0491,
 PAD_SEV_0492,
 PAD_SEV_0493,
 PAD_SEV_0494,
 PAD_SEV_0495,
 PAD_SEV_0496,
 PAD_SEV_0497,
 PAD_SEV_0498,
 PAD_SEV_0499,
 PAD_SEV_049A,
 PAD_SEV_049B,
 PAD_SEV_049C,
 PAD_SEV_049D,
 PAD_SEV_049E,
 PAD_SEV_049F,
 PAD_SEV_04A0,
 PAD_SEV_04A1,
 PAD_SEV_04A2,
 PAD_SEV_04A3,
 PAD_SEV_04A4,
 PAD_SEV_04A5,
 PAD_SEV_04A6,
 PAD_SEV_04A7,
 PAD_SEV_04A8,
 PAD_SEV_04A9,
 PAD_SEV_04AA,
 PAD_SEV_04AB,
 PAD_SEV_04AC,
 PAD_SEV_04AD,
 PAD_SEV_04AE,
 PAD_SEV_04AF,
 PAD_SEV_04B0,
 PAD_SEV_04B1,
 PAD_SEV_04B2,
 PAD_SEV_04B3,
 PAD_SEV_04B4,
 PAD_SEV_04B5,
 PAD_SEV_04B6,
 PAD_SEV_04B7,
 PAD_SEV_04B8,
 PAD_SEV_04B9,
 PAD_SEV_04BA,
 PAD_SEV_04BB,
 PAD_SEV_04BC,
 PAD_SEV_04BD,
 PAD_SEV_04BE,
 PAD_SEV_04BF,
 PAD_SEV_04C0,
 PAD_SEV_04C1,
 PAD_SEV_04C2,
 PAD_SEV_04C3,
 PAD_SEV_04C4,
 PAD_SEV_04C5,
 PAD_SEV_04C6,
 PAD_SEV_04C7,
 PAD_SEV_04C8,
 PAD_SEV_04C9,
 PAD_SEV_04CA,
 PAD_SEV_04CB,
 PAD_SEV_04CC,
 PAD_SEV_04CD,
 PAD_SEV_04CE,
 PAD_SEV_04CF,
 PAD_SEV_04D0,
 PAD_SEV_04D1,
 PAD_SEV_04D2,
 PAD_SEV_04D3,
 PAD_SEV_04D4,
 PAD_SEV_04D5,
 PAD_SEV_04D6,
 PAD_SEV_04D7,
 PAD_SEV_04D8,
 PAD_SEV_04D9,
 PAD_SEV_04DA,
 PAD_SEV_04DB,
 PAD_SEV_04DC,
 PAD_SEV_04DD,
 PAD_SEV_04DE,
 PAD_SEV_04DF,
 PAD_SEV_04E0,
 PAD_SEV_04E1,
 PAD_SEV_04E2,
 PAD_SEV_04E3,
 PAD_SEV_04E4,
 PAD_SEV_04E5,
 PAD_SEV_04E6,
 PAD_SEV_04E7,
 PAD_SEV_04E8,
 PAD_SEV_04E9,
 PAD_SEV_04EA,
 PAD_SEV_04EB,
 PAD_SEV_04EC,
 PAD_SEV_04ED,
 PAD_SEV_04EE,
 PAD_SEV_04EF,
 PAD_SEV_04F0,
 PAD_SEV_04F1,
 PAD_SEV_04F2,
 PAD_SEV_04F3,
 PAD_SEV_04F4,
 PAD_SEV_04F5,
 PAD_SEV_04F6,
 PAD_SEV_04F7,
 PAD_SEV_04F8,
 PAD_SEV_04F9,
 PAD_SEV_04FA,
 PAD_SEV_04FB,
 PAD_SEV_04FC,
 PAD_SEV_04FD,
 PAD_SEV_04FE,
 PAD_SEV_04FF,
 PAD_SEV_0500,
 PAD_SEV_0501,
 PAD_SEV_0502,
 PAD_SEV_0503,
 PAD_SEV_0504,
 PAD_SEV_0505,
 PAD_SEV_0506,
 PAD_SEV_0507,
 PAD_SEV_0508,
 PAD_SEV_0509,
 PAD_SEV_050A,
 PAD_SEV_050B,
 PAD_SEV_050C,
 PAD_SEV_050D,
 PAD_SEV_050E,
 PAD_SEV_050F,
 PAD_SEV_0510,
 PAD_SEV_0511,
 PAD_SEV_0512,
 PAD_SEV_0513,
 PAD_SEV_0514,
 PAD_SEV_0515,
 PAD_SEV_0516,
 PAD_SEV_0517,
 PAD_SEV_0518,
 PAD_SEV_0519,
 PAD_SEV_051A,
 PAD_SEV_051B,
 PAD_SEV_051C,
 PAD_SEV_051D,
 PAD_SEV_051E,
 PAD_SEV_051F,
 PAD_SEV_0520,
 PAD_SEV_0521,
 PAD_SEV_0522,
 PAD_SEV_0523,
 PAD_SEV_0524,
 PAD_SEV_0525,
 PAD_SEV_0526,
 PAD_SEV_0527,
 PAD_SEV_0528,
 PAD_SEV_0529,
 PAD_SEV_052A,
 PAD_SEV_052B,
 PAD_SEV_052C,
 PAD_SEV_052D,
 PAD_SEV_052E,
 PAD_SEV_052F,
 PAD_SEV_0530,
 PAD_SEV_0531,
 PAD_SEV_0532,
 PAD_SEV_0533,
 PAD_SEV_0534,
 PAD_SEV_0535,
 PAD_SEV_0536,
 PAD_SEV_0537,
 PAD_SEV_0538,
 PAD_SEV_0539,
 PAD_SEV_053A,
 PAD_SEV_053B,
 PAD_SEV_053C,
 PAD_SEV_053D,
 PAD_SEV_053E,
 PAD_SEV_053F,
 PAD_SEV_0540,
 PAD_SEV_0541,
 PAD_SEV_0542,
 PAD_SEV_0543,
 PAD_SEV_0544,
 PAD_SEV_0545,
 PAD_SEV_0546,
 PAD_SEV_0547,
 PAD_SEV_0548,
 PAD_SEV_0549,
 PAD_SEV_054A,
 PAD_SEV_054B,
 PAD_SEV_054C,
 PAD_SEV_054D,
 PAD_SEV_054E,
 PAD_SEV_054F,
 PAD_SEV_0550,
 PAD_SEV_0551,
 PAD_SEV_0552,
 PAD_SEV_0553,
 PAD_SEV_0554,
 PAD_SEV_0555,
 PAD_SEV_0556,
 PAD_SEV_0557,
 PAD_SEV_0558,
 PAD_SEV_0559,
 PAD_SEV_055A,
 PAD_SEV_055B,
 PAD_SEV_055C,
 PAD_SEV_055D,
 PAD_SEV_055E,
 PAD_SEV_055F,
 PAD_SEV_0560,
 PAD_SEV_0561,
 PAD_SEV_0562,
 PAD_SEV_0563,
 PAD_SEV_0564,
 PAD_SEV_0565,
 PAD_SEV_0566,
 PAD_SEV_0567,
 PAD_SEV_END
};
enum pad_sevx {
 PAD_SEVX_END
};
enum pad_sho {
 PAD_SHO_0000,
 PAD_SHO_0001,
 PAD_SHO_0002,
 PAD_SHO_0003,
 PAD_SHO_0004,
 PAD_SHO_0005,
 PAD_SHO_0006,
 PAD_SHO_0007,
 PAD_SHO_0008,
 PAD_SHO_0009,
 PAD_SHO_000A,
 PAD_SHO_000B,
 PAD_SHO_000C,
 PAD_SHO_000D,
 PAD_SHO_000E,
 PAD_SHO_000F,
 PAD_SHO_0010,
 PAD_SHO_0011,
 PAD_SHO_0012,
 PAD_SHO_0013,
 PAD_SHO_0014,
 PAD_SHO_0015,
 PAD_SHO_0016,
 PAD_SHO_0017,
 PAD_SHO_0018,
 PAD_SHO_0019,
 PAD_SHO_001A,
 PAD_SHO_001B,
 PAD_SHO_001C,
 PAD_SHO_001D,
 PAD_SHO_001E,
 PAD_SHO_001F,
 PAD_SHO_0020,
 PAD_SHO_0021,
 PAD_SHO_0022,
 PAD_SHO_0023,
 PAD_SHO_0024,
 PAD_SHO_0025,
 PAD_SHO_0026,
 PAD_SHO_0027,
 PAD_SHO_0028,
 PAD_SHO_0029,
 PAD_SHO_002A,
 PAD_SHO_002B,
 PAD_SHO_002C,
 PAD_SHO_002D,
 PAD_SHO_002E,
 PAD_SHO_002F,
 PAD_SHO_0030,
 PAD_SHO_0031,
 PAD_SHO_0032,
 PAD_SHO_0033,
 PAD_SHO_0034,
 PAD_SHO_0035,
 PAD_SHO_0036,
 PAD_SHO_0037,
 PAD_SHO_0038,
 PAD_SHO_0039,
 PAD_SHO_003A,
 PAD_SHO_003B,
 PAD_SHO_003C,
 PAD_SHO_003D,
 PAD_SHO_003E,
 PAD_SHO_003F,
 PAD_SHO_0040,
 PAD_SHO_0041,
 PAD_SHO_0042,
 PAD_SHO_0043,
 PAD_SHO_0044,
 PAD_SHO_0045,
 PAD_SHO_0046,
 PAD_SHO_0047,
 PAD_SHO_0048,
 PAD_SHO_0049,
 PAD_SHO_004A,
 PAD_SHO_004B,
 PAD_SHO_004C,
 PAD_SHO_004D,
 PAD_SHO_004E,
 PAD_SHO_004F,
 PAD_SHO_0050,
 PAD_SHO_0051,
 PAD_SHO_0052,
 PAD_SHO_0053,
 PAD_SHO_0054,
 PAD_SHO_0055,
 PAD_SHO_0056,
 PAD_SHO_0057,
 PAD_SHO_0058,
 PAD_SHO_0059,
 PAD_SHO_005A,
 PAD_SHO_005B,
 PAD_SHO_005C,
 PAD_SHO_005D,
 PAD_SHO_005E,
 PAD_SHO_005F,
 PAD_SHO_0060,
 PAD_SHO_0061,
 PAD_SHO_0062,
 PAD_SHO_0063,
 PAD_SHO_0064,
 PAD_SHO_0065,
 PAD_SHO_0066,
 PAD_SHO_0067,
 PAD_SHO_0068,
 PAD_SHO_0069,
 PAD_SHO_006A,
 PAD_SHO_006B,
 PAD_SHO_006C,
 PAD_SHO_006D,
 PAD_SHO_006E,
 PAD_SHO_006F,
 PAD_SHO_0070,
 PAD_SHO_0071,
 PAD_SHO_0072,
 PAD_SHO_0073,
 PAD_SHO_0074,
 PAD_SHO_0075,
 PAD_SHO_0076,
 PAD_SHO_0077,
 PAD_SHO_0078,
 PAD_SHO_0079,
 PAD_SHO_007A,
 PAD_SHO_007B,
 PAD_SHO_007C,
 PAD_SHO_007D,
 PAD_SHO_007E,
 PAD_SHO_007F,
 PAD_SHO_0080,
 PAD_SHO_0081,
 PAD_SHO_0082,
 PAD_SHO_0083,
 PAD_SHO_0084,
 PAD_SHO_0085,
 PAD_SHO_0086,
 PAD_SHO_0087,
 PAD_SHO_0088,
 PAD_SHO_0089,
 PAD_SHO_008A,
 PAD_SHO_008B,
 PAD_SHO_008C,
 PAD_SHO_008D,
 PAD_SHO_008E,
 PAD_SHO_008F,
 PAD_SHO_0090,
 PAD_SHO_0091,
 PAD_SHO_0092,
 PAD_SHO_0093,
 PAD_SHO_0094,
 PAD_SHO_0095,
 PAD_SHO_0096,
 PAD_SHO_0097,
 PAD_SHO_0098,
 PAD_SHO_0099,
 PAD_SHO_009A,
 PAD_SHO_009B,
 PAD_SHO_009C,
 PAD_SHO_009D,
 PAD_SHO_009E,
 PAD_SHO_009F,
 PAD_SHO_00A0,
 PAD_SHO_00A1,
 PAD_SHO_00A2,
 PAD_SHO_00A3,
 PAD_SHO_00A4,
 PAD_SHO_00A5,
 PAD_SHO_00A6,
 PAD_SHO_00A7,
 PAD_SHO_00A8,
 PAD_SHO_00A9,
 PAD_SHO_00AA,
 PAD_SHO_00AB,
 PAD_SHO_00AC,
 PAD_SHO_00AD,
 PAD_SHO_00AE,
 PAD_SHO_00AF,
 PAD_SHO_00B0,
 PAD_SHO_00B1,
 PAD_SHO_00B2,
 PAD_SHO_00B3,
 PAD_SHO_00B4,
 PAD_SHO_00B5,
 PAD_SHO_00B6,
 PAD_SHO_00B7,
 PAD_SHO_00B8,
 PAD_SHO_00B9,
 PAD_SHO_00BA,
 PAD_SHO_00BB,
 PAD_SHO_00BC,
 PAD_SHO_00BD,
 PAD_SHO_00BE,
 PAD_SHO_00BF,
 PAD_SHO_00C0,
 PAD_SHO_00C1,
 PAD_SHO_00C2,
 PAD_SHO_00C3,
 PAD_SHO_00C4,
 PAD_SHO_00C5,
 PAD_SHO_00C6,
 PAD_SHO_00C7,
 PAD_SHO_00C8,
 PAD_SHO_00C9,
 PAD_SHO_00CA,
 PAD_SHO_00CB,
 PAD_SHO_00CC,
 PAD_SHO_00CD,
 PAD_SHO_00CE,
 PAD_SHO_00CF,
 PAD_SHO_00D0,
 PAD_SHO_00D1,
 PAD_SHO_00D2,
 PAD_SHO_00D3,
 PAD_SHO_00D4,
 PAD_SHO_00D5,
 PAD_SHO_00D6,
 PAD_SHO_00D7,
 PAD_SHO_00D8,
 PAD_SHO_00D9,
 PAD_SHO_00DA,
 PAD_SHO_00DB,
 PAD_SHO_00DC,
 PAD_SHO_00DD,
 PAD_SHO_00DE,
 PAD_SHO_00DF,
 PAD_SHO_00E0,
 PAD_SHO_00E1,
 PAD_SHO_00E2,
 PAD_SHO_00E3,
 PAD_SHO_00E4,
 PAD_SHO_00E5,
 PAD_SHO_00E6,
 PAD_SHO_00E7,
 PAD_SHO_00E8,
 PAD_SHO_00E9,
 PAD_SHO_00EA,
 PAD_SHO_00EB,
 PAD_SHO_00EC,
 PAD_SHO_00ED,
 PAD_SHO_00EE,
 PAD_SHO_00EF,
 PAD_SHO_00F0,
 PAD_SHO_00F1,
 PAD_SHO_00F2,
 PAD_SHO_00F3,
 PAD_SHO_00F4,
 PAD_SHO_00F5,
 PAD_SHO_00F6,
 PAD_SHO_00F7,
 PAD_SHO_00F8,
 PAD_SHO_00F9,
 PAD_SHO_00FA,
 PAD_SHO_00FB,
 PAD_SHO_00FC,
 PAD_SHO_00FD,
 PAD_SHO_00FE,
 PAD_SHO_00FF,
 PAD_SHO_0100,
 PAD_SHO_0101,
 PAD_SHO_0102,
 PAD_SHO_0103,
 PAD_SHO_0104,
 PAD_SHO_0105,
 PAD_SHO_0106,
 PAD_SHO_0107,
 PAD_SHO_0108,
 PAD_SHO_0109,
 PAD_SHO_010A,
 PAD_SHO_010B,
 PAD_SHO_010C,
 PAD_SHO_010D,
 PAD_SHO_010E,
 PAD_SHO_010F,
 PAD_SHO_0110,
 PAD_SHO_0111,
 PAD_SHO_0112,
 PAD_SHO_0113,
 PAD_SHO_0114,
 PAD_SHO_0115,
 PAD_SHO_0116,
 PAD_SHO_0117,
 PAD_SHO_0118,
 PAD_SHO_0119,
 PAD_SHO_011A,
 PAD_SHO_011B,
 PAD_SHO_011C,
 PAD_SHO_011D,
 PAD_SHO_011E,
 PAD_SHO_011F,
 PAD_SHO_0120,
 PAD_SHO_0121,
 PAD_SHO_0122,
 PAD_SHO_0123,
 PAD_SHO_0124,
 PAD_SHO_0125,
 PAD_SHO_0126,
 PAD_SHO_0127,
 PAD_SHO_0128,
 PAD_SHO_0129,
 PAD_SHO_012A,
 PAD_SHO_012B,
 PAD_SHO_012C,
 PAD_SHO_012D,
 PAD_SHO_012E,
 PAD_SHO_012F,
 PAD_SHO_0130,
 PAD_SHO_0131,
 PAD_SHO_0132,
 PAD_SHO_0133,
 PAD_SHO_0134,
 PAD_SHO_0135,
 PAD_SHO_0136,
 PAD_SHO_0137,
 PAD_SHO_0138,
 PAD_SHO_0139,
 PAD_SHO_013A,
 PAD_SHO_013B,
 PAD_SHO_013C,
 PAD_SHO_013D,
 PAD_SHO_013E,
 PAD_SHO_013F,
 PAD_SHO_0140,
 PAD_SHO_0141,
 PAD_SHO_0142,
 PAD_SHO_0143,
 PAD_SHO_0144,
 PAD_SHO_0145,
 PAD_SHO_0146,
 PAD_SHO_0147,
 PAD_SHO_0148,
 PAD_SHO_0149,
 PAD_SHO_014A,
 PAD_SHO_014B,
 PAD_SHO_014C,
 PAD_SHO_014D,
 PAD_SHO_014E,
 PAD_SHO_014F,
 PAD_SHO_0150,
 PAD_SHO_0151,
 PAD_SHO_END
};
enum pad_silo {
 PAD_SILO_END
};
enum pad_stat {
 PAD_STAT_0000,
 PAD_STAT_0001,
 PAD_STAT_0002,
 PAD_STAT_0003,
 PAD_STAT_0004,
 PAD_STAT_0005,
 PAD_STAT_0006,
 PAD_STAT_0007,
 PAD_STAT_0008,
 PAD_STAT_0009,
 PAD_STAT_000A,
 PAD_STAT_000B,
 PAD_STAT_000C,
 PAD_STAT_000D,
 PAD_STAT_000E,
 PAD_STAT_000F,
 PAD_STAT_0010,
 PAD_STAT_0011,
 PAD_STAT_0012,
 PAD_STAT_0013,
 PAD_STAT_0014,
 PAD_STAT_0015,
 PAD_STAT_0016,
 PAD_STAT_0017,
 PAD_STAT_0018,
 PAD_STAT_0019,
 PAD_STAT_001A,
 PAD_STAT_001B,
 PAD_STAT_001C,
 PAD_STAT_001D,
 PAD_STAT_001E,
 PAD_STAT_001F,
 PAD_STAT_0020,
 PAD_STAT_0021,
 PAD_STAT_0022,
 PAD_STAT_0023,
 PAD_STAT_0024,
 PAD_STAT_0025,
 PAD_STAT_0026,
 PAD_STAT_0027,
 PAD_STAT_0028,
 PAD_STAT_0029,
 PAD_STAT_002A,
 PAD_STAT_002B,
 PAD_STAT_002C,
 PAD_STAT_002D,
 PAD_STAT_002E,
 PAD_STAT_002F,
 PAD_STAT_0030,
 PAD_STAT_0031,
 PAD_STAT_0032,
 PAD_STAT_0033,
 PAD_STAT_0034,
 PAD_STAT_0035,
 PAD_STAT_0036,
 PAD_STAT_0037,
 PAD_STAT_0038,
 PAD_STAT_0039,
 PAD_STAT_003A,
 PAD_STAT_003B,
 PAD_STAT_003C,
 PAD_STAT_003D,
 PAD_STAT_003E,
 PAD_STAT_003F,
 PAD_STAT_0040,
 PAD_STAT_0041,
 PAD_STAT_0042,
 PAD_STAT_0043,
 PAD_STAT_0044,
 PAD_STAT_0045,
 PAD_STAT_0046,
 PAD_STAT_0047,
 PAD_STAT_0048,
 PAD_STAT_0049,
 PAD_STAT_004A,
 PAD_STAT_004B,
 PAD_STAT_004C,
 PAD_STAT_004D,
 PAD_STAT_004E,
 PAD_STAT_004F,
 PAD_STAT_0050,
 PAD_STAT_0051,
 PAD_STAT_0052,
 PAD_STAT_0053,
 PAD_STAT_0054,
 PAD_STAT_0055,
 PAD_STAT_0056,
 PAD_STAT_0057,
 PAD_STAT_0058,
 PAD_STAT_0059,
 PAD_STAT_005A,
 PAD_STAT_005B,
 PAD_STAT_005C,
 PAD_STAT_005D,
 PAD_STAT_005E,
 PAD_STAT_005F,
 PAD_STAT_0060,
 PAD_STAT_0061,
 PAD_STAT_0062,
 PAD_STAT_0063,
 PAD_STAT_0064,
 PAD_STAT_0065,
 PAD_STAT_0066,
 PAD_STAT_0067,
 PAD_STAT_0068,
 PAD_STAT_0069,
 PAD_STAT_006A,
 PAD_STAT_006B,
 PAD_STAT_006C,
 PAD_STAT_006D,
 PAD_STAT_006E,
 PAD_STAT_006F,
 PAD_STAT_0070,
 PAD_STAT_0071,
 PAD_STAT_0072,
 PAD_STAT_0073,
 PAD_STAT_0074,
 PAD_STAT_0075,
 PAD_STAT_0076,
 PAD_STAT_0077,
 PAD_STAT_0078,
 PAD_STAT_0079,
 PAD_STAT_007A,
 PAD_STAT_007B,
 PAD_STAT_007C,
 PAD_STAT_007D,
 PAD_STAT_007E,
 PAD_STAT_007F,
 PAD_STAT_0080,
 PAD_STAT_0081,
 PAD_STAT_0082,
 PAD_STAT_0083,
 PAD_STAT_0084,
 PAD_STAT_0085,
 PAD_STAT_0086,
 PAD_STAT_0087,
 PAD_STAT_0088,
 PAD_STAT_0089,
 PAD_STAT_008A,
 PAD_STAT_008B,
 PAD_STAT_008C,
 PAD_STAT_008D,
 PAD_STAT_008E,
 PAD_STAT_008F,
 PAD_STAT_0090,
 PAD_STAT_0091,
 PAD_STAT_0092,
 PAD_STAT_0093,
 PAD_STAT_0094,
 PAD_STAT_0095,
 PAD_STAT_0096,
 PAD_STAT_0097,
 PAD_STAT_0098,
 PAD_STAT_0099,
 PAD_STAT_009A,
 PAD_STAT_009B,
 PAD_STAT_009C,
 PAD_STAT_009D,
 PAD_STAT_009E,
 PAD_STAT_009F,
 PAD_STAT_00A0,
 PAD_STAT_00A1,
 PAD_STAT_00A2,
 PAD_STAT_00A3,
 PAD_STAT_00A4,
 PAD_STAT_00A5,
 PAD_STAT_00A6,
 PAD_STAT_00A7,
 PAD_STAT_00A8,
 PAD_STAT_00A9,
 PAD_STAT_00AA,
 PAD_STAT_00AB,
 PAD_STAT_00AC,
 PAD_STAT_00AD,
 PAD_STAT_00AE,
 PAD_STAT_00AF,
 PAD_STAT_00B0,
 PAD_STAT_00B1,
 PAD_STAT_00B2,
 PAD_STAT_00B3,
 PAD_STAT_00B4,
 PAD_STAT_00B5,
 PAD_STAT_00B6,
 PAD_STAT_00B7,
 PAD_STAT_00B8,
 PAD_STAT_00B9,
 PAD_STAT_00BA,
 PAD_STAT_00BB,
 PAD_STAT_00BC,
 PAD_STAT_00BD,
 PAD_STAT_00BE,
 PAD_STAT_00BF,
 PAD_STAT_00C0,
 PAD_STAT_00C1,
 PAD_STAT_00C2,
 PAD_STAT_00C3,
 PAD_STAT_00C4,
 PAD_STAT_00C5,
 PAD_STAT_00C6,
 PAD_STAT_00C7,
 PAD_STAT_00C8,
 PAD_STAT_00C9,
 PAD_STAT_00CA,
 PAD_STAT_00CB,
 PAD_STAT_00CC,
 PAD_STAT_00CD,
 PAD_STAT_00CE,
 PAD_STAT_00CF,
 PAD_STAT_00D0,
 PAD_STAT_00D1,
 PAD_STAT_00D2,
 PAD_STAT_00D3,
 PAD_STAT_00D4,
 PAD_STAT_00D5,
 PAD_STAT_00D6,
 PAD_STAT_00D7,
 PAD_STAT_00D8,
 PAD_STAT_00D9,
 PAD_STAT_00DA,
 PAD_STAT_00DB,
 PAD_STAT_00DC,
 PAD_STAT_00DD,
 PAD_STAT_00DE,
 PAD_STAT_00DF,
 PAD_STAT_00E0,
 PAD_STAT_00E1,
 PAD_STAT_00E2,
 PAD_STAT_00E3,
 PAD_STAT_00E4,
 PAD_STAT_00E5,
 PAD_STAT_00E6,
 PAD_STAT_00E7,
 PAD_STAT_00E8,
 PAD_STAT_00E9,
 PAD_STAT_00EA,
 PAD_STAT_00EB,
 PAD_STAT_00EC,
 PAD_STAT_00ED,
 PAD_STAT_00EE,
 PAD_STAT_00EF,
 PAD_STAT_00F0,
 PAD_STAT_00F1,
 PAD_STAT_00F2,
 PAD_STAT_00F3,
 PAD_STAT_00F4,
 PAD_STAT_00F5,
 PAD_STAT_00F6,
 PAD_STAT_00F7,
 PAD_STAT_00F8,
 PAD_STAT_00F9,
 PAD_STAT_00FA,
 PAD_STAT_00FB,
 PAD_STAT_00FC,
 PAD_STAT_00FD,
 PAD_STAT_00FE,
 PAD_STAT_00FF,
 PAD_STAT_0100,
 PAD_STAT_0101,
 PAD_STAT_0102,
 PAD_STAT_0103,
 PAD_STAT_0104,
 PAD_STAT_0105,
 PAD_STAT_0106,
 PAD_STAT_0107,
 PAD_STAT_0108,
 PAD_STAT_0109,
 PAD_STAT_010A,
 PAD_STAT_010B,
 PAD_STAT_010C,
 PAD_STAT_010D,
 PAD_STAT_010E,
 PAD_STAT_010F,
 PAD_STAT_0110,
 PAD_STAT_0111,
 PAD_STAT_0112,
 PAD_STAT_0113,
 PAD_STAT_0114,
 PAD_STAT_0115,
 PAD_STAT_0116,
 PAD_STAT_0117,
 PAD_STAT_0118,
 PAD_STAT_0119,
 PAD_STAT_011A,
 PAD_STAT_011B,
 PAD_STAT_011C,
 PAD_STAT_011D,
 PAD_STAT_011E,
 PAD_STAT_011F,
 PAD_STAT_0120,
 PAD_STAT_0121,
 PAD_STAT_0122,
 PAD_STAT_0123,
 PAD_STAT_0124,
 PAD_STAT_0125,
 PAD_STAT_0126,
 PAD_STAT_0127,
 PAD_STAT_0128,
 PAD_STAT_0129,
 PAD_STAT_012A,
 PAD_STAT_012B,
 PAD_STAT_012C,
 PAD_STAT_012D,
 PAD_STAT_012E,
 PAD_STAT_012F,
 PAD_STAT_0130,
 PAD_STAT_0131,
 PAD_STAT_0132,
 PAD_STAT_0133,
 PAD_STAT_0134,
 PAD_STAT_0135,
 PAD_STAT_0136,
 PAD_STAT_0137,
 PAD_STAT_0138,
 PAD_STAT_0139,
 PAD_STAT_013A,
 PAD_STAT_013B,
 PAD_STAT_013C,
 PAD_STAT_013D,
 PAD_STAT_013E,
 PAD_STAT_013F,
 PAD_STAT_0140,
 PAD_STAT_0141,
 PAD_STAT_0142,
 PAD_STAT_0143,
 PAD_STAT_0144,
 PAD_STAT_0145,
 PAD_STAT_0146,
 PAD_STAT_0147,
 PAD_STAT_0148,
 PAD_STAT_0149,
 PAD_STAT_014A,
 PAD_STAT_014B,
 PAD_STAT_014C,
 PAD_STAT_014D,
 PAD_STAT_014E,
 PAD_STAT_014F,
 PAD_STAT_0150,
 PAD_STAT_0151,
 PAD_STAT_END
};
enum pad_tra {
 PAD_TRA_0000,
 PAD_TRA_0001,
 PAD_TRA_0002,
 PAD_TRA_0003,
 PAD_TRA_0004,
 PAD_TRA_0005,
 PAD_TRA_0006,
 PAD_TRA_0007,
 PAD_TRA_0008,
 PAD_TRA_0009,
 PAD_TRA_000A,
 PAD_TRA_000B,
 PAD_TRA_000C,
 PAD_TRA_000D,
 PAD_TRA_000E,
 PAD_TRA_000F,
 PAD_TRA_0010,
 PAD_TRA_0011,
 PAD_TRA_0012,
 PAD_TRA_0013,
 PAD_TRA_0014,
 PAD_TRA_0015,
 PAD_TRA_0016,
 PAD_TRA_0017,
 PAD_TRA_0018,
 PAD_TRA_0019,
 PAD_TRA_001A,
 PAD_TRA_001B,
 PAD_TRA_001C,
 PAD_TRA_001D,
 PAD_TRA_001E,
 PAD_TRA_001F,
 PAD_TRA_0020,
 PAD_TRA_0021,
 PAD_TRA_0022,
 PAD_TRA_0023,
 PAD_TRA_0024,
 PAD_TRA_0025,
 PAD_TRA_0026,
 PAD_TRA_0027,
 PAD_TRA_0028,
 PAD_TRA_0029,
 PAD_TRA_002A,
 PAD_TRA_002B,
 PAD_TRA_002C,
 PAD_TRA_002D,
 PAD_TRA_002E,
 PAD_TRA_002F,
 PAD_TRA_0030,
 PAD_TRA_0031,
 PAD_TRA_0032,
 PAD_TRA_0033,
 PAD_TRA_0034,
 PAD_TRA_0035,
 PAD_TRA_0036,
 PAD_TRA_0037,
 PAD_TRA_0038,
 PAD_TRA_0039,
 PAD_TRA_003A,
 PAD_TRA_003B,
 PAD_TRA_003C,
 PAD_TRA_003D,
 PAD_TRA_003E,
 PAD_TRA_003F,
 PAD_TRA_0040,
 PAD_TRA_0041,
 PAD_TRA_0042,
 PAD_TRA_0043,
 PAD_TRA_0044,
 PAD_TRA_0045,
 PAD_TRA_0046,
 PAD_TRA_0047,
 PAD_TRA_0048,
 PAD_TRA_0049,
 PAD_TRA_004A,
 PAD_TRA_004B,
 PAD_TRA_004C,
 PAD_TRA_004D,
 PAD_TRA_004E,
 PAD_TRA_004F,
 PAD_TRA_0050,
 PAD_TRA_0051,
 PAD_TRA_0052,
 PAD_TRA_0053,
 PAD_TRA_0054,
 PAD_TRA_0055,
 PAD_TRA_0056,
 PAD_TRA_0057,
 PAD_TRA_0058,
 PAD_TRA_0059,
 PAD_TRA_005A,
 PAD_TRA_005B,
 PAD_TRA_005C,
 PAD_TRA_005D,
 PAD_TRA_005E,
 PAD_TRA_005F,
 PAD_TRA_0060,
 PAD_TRA_0061,
 PAD_TRA_0062,
 PAD_TRA_0063,
 PAD_TRA_0064,
 PAD_TRA_0065,
 PAD_TRA_0066,
 PAD_TRA_0067,
 PAD_TRA_0068,
 PAD_TRA_0069,
 PAD_TRA_006A,
 PAD_TRA_006B,
 PAD_TRA_006C,
 PAD_TRA_006D,
 PAD_TRA_006E,
 PAD_TRA_006F,
 PAD_TRA_0070,
 PAD_TRA_0071,
 PAD_TRA_0072,
 PAD_TRA_0073,
 PAD_TRA_0074,
 PAD_TRA_0075,
 PAD_TRA_0076,
 PAD_TRA_0077,
 PAD_TRA_0078,
 PAD_TRA_0079,
 PAD_TRA_007A,
 PAD_TRA_007B,
 PAD_TRA_007C,
 PAD_TRA_007D,
 PAD_TRA_007E,
 PAD_TRA_007F,
 PAD_TRA_0080,
 PAD_TRA_0081,
 PAD_TRA_0082,
 PAD_TRA_0083,
 PAD_TRA_0084,
 PAD_TRA_0085,
 PAD_TRA_0086,
 PAD_TRA_0087,
 PAD_TRA_0088,
 PAD_TRA_0089,
 PAD_TRA_008A,
 PAD_TRA_008B,
 PAD_TRA_008C,
 PAD_TRA_008D,
 PAD_TRA_008E,
 PAD_TRA_008F,
 PAD_TRA_0090,
 PAD_TRA_0091,
 PAD_TRA_0092,
 PAD_TRA_0093,
 PAD_TRA_0094,
 PAD_TRA_0095,
 PAD_TRA_0096,
 PAD_TRA_0097,
 PAD_TRA_0098,
 PAD_TRA_0099,
 PAD_TRA_009A,
 PAD_TRA_009B,
 PAD_TRA_009C,
 PAD_TRA_009D,
 PAD_TRA_009E,
 PAD_TRA_009F,
 PAD_TRA_00A0,
 PAD_TRA_00A1,
 PAD_TRA_00A2,
 PAD_TRA_00A3,
 PAD_TRA_00A4,
 PAD_TRA_00A5,
 PAD_TRA_00A6,
 PAD_TRA_00A7,
 PAD_TRA_00A8,
 PAD_TRA_00A9,
 PAD_TRA_00AA,
 PAD_TRA_00AB,
 PAD_TRA_00AC,
 PAD_TRA_00AD,
 PAD_TRA_00AE,
 PAD_TRA_00AF,
 PAD_TRA_00B0,
 PAD_TRA_00B1,
 PAD_TRA_00B2,
 PAD_TRA_00B3,
 PAD_TRA_00B4,
 PAD_TRA_00B5,
 PAD_TRA_00B6,
 PAD_TRA_00B7,
 PAD_TRA_00B8,
 PAD_TRA_00B9,
 PAD_TRA_00BA,
 PAD_TRA_00BB,
 PAD_TRA_00BC,
 PAD_TRA_00BD,
 PAD_TRA_00BE,
 PAD_TRA_00BF,
 PAD_TRA_00C0,
 PAD_TRA_00C1,
 PAD_TRA_00C2,
 PAD_TRA_00C3,
 PAD_TRA_00C4,
 PAD_TRA_00C5,
 PAD_TRA_00C6,
 PAD_TRA_00C7,
 PAD_TRA_00C8,
 PAD_TRA_00C9,
 PAD_TRA_00CA,
 PAD_TRA_00CB,
 PAD_TRA_00CC,
 PAD_TRA_00CD,
 PAD_TRA_00CE,
 PAD_TRA_00CF,
 PAD_TRA_00D0,
 PAD_TRA_00D1,
 PAD_TRA_00D2,
 PAD_TRA_00D3,
 PAD_TRA_00D4,
 PAD_TRA_00D5,
 PAD_TRA_00D6,
 PAD_TRA_00D7,
 PAD_TRA_00D8,
 PAD_TRA_00D9,
 PAD_TRA_00DA,
 PAD_TRA_00DB,
 PAD_TRA_00DC,
 PAD_TRA_00DD,
 PAD_TRA_00DE,
 PAD_TRA_00DF,
 PAD_TRA_00E0,
 PAD_TRA_00E1,
 PAD_TRA_00E2,
 PAD_TRA_00E3,
 PAD_TRA_00E4,
 PAD_TRA_00E5,
 PAD_TRA_00E6,
 PAD_TRA_00E7,
 PAD_TRA_00E8,
 PAD_TRA_00E9,
 PAD_TRA_00EA,
 PAD_TRA_00EB,
 PAD_TRA_00EC,
 PAD_TRA_00ED,
 PAD_TRA_00EE,
 PAD_TRA_00EF,
 PAD_TRA_00F0,
 PAD_TRA_00F1,
 PAD_TRA_00F2,
 PAD_TRA_00F3,
 PAD_TRA_00F4,
 PAD_TRA_00F5,
 PAD_TRA_00F6,
 PAD_TRA_00F7,
 PAD_TRA_00F8,
 PAD_TRA_00F9,
 PAD_TRA_00FA,
 PAD_TRA_00FB,
 PAD_TRA_00FC,
 PAD_TRA_00FD,
 PAD_TRA_00FE,
 PAD_TRA_00FF,
 PAD_TRA_0100,
 PAD_TRA_0101,
 PAD_TRA_0102,
 PAD_TRA_0103,
 PAD_TRA_0104,
 PAD_TRA_0105,
 PAD_TRA_0106,
 PAD_TRA_0107,
 PAD_TRA_0108,
 PAD_TRA_0109,
 PAD_TRA_010A,
 PAD_TRA_010B,
 PAD_TRA_010C,
 PAD_TRA_010D,
 PAD_TRA_010E,
 PAD_TRA_010F,
 PAD_TRA_0110,
 PAD_TRA_0111,
 PAD_TRA_0112,
 PAD_TRA_0113,
 PAD_TRA_0114,
 PAD_TRA_0115,
 PAD_TRA_0116,
 PAD_TRA_0117,
 PAD_TRA_0118,
 PAD_TRA_0119,
 PAD_TRA_011A,
 PAD_TRA_011B,
 PAD_TRA_011C,
 PAD_TRA_011D,
 PAD_TRA_011E,
 PAD_TRA_011F,
 PAD_TRA_0120,
 PAD_TRA_0121,
 PAD_TRA_0122,
 PAD_TRA_0123,
 PAD_TRA_0124,
 PAD_TRA_0125,
 PAD_TRA_0126,
 PAD_TRA_0127,
 PAD_TRA_0128,
 PAD_TRA_0129,
 PAD_TRA_012A,
 PAD_TRA_012B,
 PAD_TRA_012C,
 PAD_TRA_012D,
 PAD_TRA_012E,
 PAD_TRA_012F,
 PAD_TRA_0130,
 PAD_TRA_0131,
 PAD_TRA_0132,
 PAD_TRA_0133,
 PAD_TRA_0134,
 PAD_TRA_0135,
 PAD_TRA_0136,
 PAD_TRA_0137,
 PAD_TRA_0138,
 PAD_TRA_0139,
 PAD_TRA_013A,
 PAD_TRA_013B,
 PAD_TRA_013C,
 PAD_TRA_013D,
 PAD_TRA_013E,
 PAD_TRA_013F,
 PAD_TRA_0140,
 PAD_TRA_0141,
 PAD_TRA_0142,
 PAD_TRA_0143,
 PAD_TRA_0144,
 PAD_TRA_0145,
 PAD_TRA_0146,
 PAD_TRA_0147,
 PAD_TRA_0148,
 PAD_TRA_0149,
 PAD_TRA_014A,
 PAD_TRA_014B,
 PAD_TRA_014C,
 PAD_TRA_014D,
 PAD_TRA_014E,
 PAD_TRA_014F,
 PAD_TRA_0150,
 PAD_TRA_0151,
 PAD_TRA_0152,
 PAD_TRA_0153,
 PAD_TRA_0154,
 PAD_TRA_0155,
 PAD_TRA_0156,
 PAD_TRA_0157,
 PAD_TRA_0158,
 PAD_TRA_0159,
 PAD_TRA_015A,
 PAD_TRA_015B,
 PAD_TRA_015C,
 PAD_TRA_015D,
 PAD_TRA_015E,
 PAD_TRA_015F,
 PAD_TRA_0160,
 PAD_TRA_0161,
 PAD_TRA_0162,
 PAD_TRA_0163,
 PAD_TRA_0164,
 PAD_TRA_0165,
 PAD_TRA_0166,
 PAD_TRA_0167,
 PAD_TRA_0168,
 PAD_TRA_0169,
 PAD_TRA_016A,
 PAD_TRA_016B,
 PAD_TRA_016C,
 PAD_TRA_016D,
 PAD_TRA_016E,
 PAD_TRA_016F,
 PAD_TRA_0170,
 PAD_TRA_0171,
 PAD_TRA_0172,
 PAD_TRA_0173,
 PAD_TRA_0174,
 PAD_TRA_0175,
 PAD_TRA_0176,
 PAD_TRA_0177,
 PAD_TRA_0178,
 PAD_TRA_0179,
 PAD_TRA_017A,
 PAD_TRA_017B,
 PAD_TRA_017C,
 PAD_TRA_017D,
 PAD_TRA_017E,
 PAD_TRA_017F,
 PAD_TRA_0180,
 PAD_TRA_0181,
 PAD_TRA_0182,
 PAD_TRA_0183,
 PAD_TRA_0184,
 PAD_TRA_0185,
 PAD_TRA_0186,
 PAD_TRA_0187,
 PAD_TRA_0188,
 PAD_TRA_0189,
 PAD_TRA_018A,
 PAD_TRA_018B,
 PAD_TRA_018C,
 PAD_TRA_018D,
 PAD_TRA_018E,
 PAD_TRA_018F,
 PAD_TRA_0190,
 PAD_TRA_0191,
 PAD_TRA_0192,
 PAD_TRA_0193,
 PAD_TRA_0194,
 PAD_TRA_0195,
 PAD_TRA_0196,
 PAD_TRA_0197,
 PAD_TRA_0198,
 PAD_TRA_0199,
 PAD_TRA_019A,
 PAD_TRA_019B,
 PAD_TRA_019C,
 PAD_TRA_019D,
 PAD_TRA_019E,
 PAD_TRA_019F,
 PAD_TRA_01A0,
 PAD_TRA_01A1,
 PAD_TRA_01A2,
 PAD_TRA_01A3,
 PAD_TRA_01A4,
 PAD_TRA_01A5,
 PAD_TRA_01A6,
 PAD_TRA_01A7,
 PAD_TRA_01A8,
 PAD_TRA_01A9,
 PAD_TRA_01AA,
 PAD_TRA_01AB,
 PAD_TRA_01AC,
 PAD_TRA_01AD,
 PAD_TRA_01AE,
 PAD_TRA_01AF,
 PAD_TRA_01B0,
 PAD_TRA_01B1,
 PAD_TRA_01B2,
 PAD_TRA_01B3,
 PAD_TRA_01B4,
 PAD_TRA_01B5,
 PAD_TRA_01B6,
 PAD_TRA_01B7,
 PAD_TRA_01B8,
 PAD_TRA_01B9,
 PAD_TRA_01BA,
 PAD_TRA_01BB,
 PAD_TRA_01BC,
 PAD_TRA_01BD,
 PAD_TRA_01BE,
 PAD_TRA_01BF,
 PAD_TRA_01C0,
 PAD_TRA_01C1,
 PAD_TRA_01C2,
 PAD_TRA_01C3,
 PAD_TRA_01C4,
 PAD_TRA_01C5,
 PAD_TRA_01C6,
 PAD_TRA_01C7,
 PAD_TRA_01C8,
 PAD_TRA_01C9,
 PAD_TRA_01CA,
 PAD_TRA_01CB,
 PAD_TRA_01CC,
 PAD_TRA_01CD,
 PAD_TRA_01CE,
 PAD_TRA_01CF,
 PAD_TRA_01D0,
 PAD_TRA_01D1,
 PAD_TRA_01D2,
 PAD_TRA_01D3,
 PAD_TRA_01D4,
 PAD_TRA_01D5,
 PAD_TRA_01D6,
 PAD_TRA_01D7,
 PAD_TRA_01D8,
 PAD_TRA_01D9,
 PAD_TRA_01DA,
 PAD_TRA_01DB,
 PAD_TRA_01DC,
 PAD_TRA_01DD,
 PAD_TRA_01DE,
 PAD_TRA_01DF,
 PAD_TRA_01E0,
 PAD_TRA_01E1,
 PAD_TRA_01E2,
 PAD_TRA_01E3,
 PAD_TRA_01E4,
 PAD_TRA_01E5,
 PAD_TRA_01E6,
 PAD_TRA_01E7,
 PAD_TRA_01E8,
 PAD_TRA_01E9,
 PAD_TRA_01EA,
 PAD_TRA_01EB,
 PAD_TRA_01EC,
 PAD_TRA_01ED,
 PAD_TRA_01EE,
 PAD_TRA_01EF,
 PAD_TRA_01F0,
 PAD_TRA_01F1,
 PAD_TRA_01F2,
 PAD_TRA_01F3,
 PAD_TRA_01F4,
 PAD_TRA_01F5,
 PAD_TRA_01F6,
 PAD_TRA_01F7,
 PAD_TRA_01F8,
 PAD_TRA_01F9,
 PAD_TRA_01FA,
 PAD_TRA_01FB,
 PAD_TRA_01FC,
 PAD_TRA_01FD,
 PAD_TRA_01FE,
 PAD_TRA_01FF,
 PAD_TRA_0200,
 PAD_TRA_0201,
 PAD_TRA_0202,
 PAD_TRA_0203,
 PAD_TRA_0204,
 PAD_TRA_0205,
 PAD_TRA_0206,
 PAD_TRA_0207,
 PAD_TRA_0208,
 PAD_TRA_0209,
 PAD_TRA_020A,
 PAD_TRA_020B,
 PAD_TRA_020C,
 PAD_TRA_020D,
 PAD_TRA_020E,
 PAD_TRA_020F,
 PAD_TRA_0210,
 PAD_TRA_0211,
 PAD_TRA_0212,
 PAD_TRA_0213,
 PAD_TRA_0214,
 PAD_TRA_0215,
 PAD_TRA_0216,
 PAD_TRA_0217,
 PAD_TRA_0218,
 PAD_TRA_0219,
 PAD_TRA_021A,
 PAD_TRA_021B,
 PAD_TRA_021C,
 PAD_TRA_021D,
 PAD_TRA_021E,
 PAD_TRA_021F,
 PAD_TRA_0220,
 PAD_TRA_0221,
 PAD_TRA_0222,
 PAD_TRA_0223,
 PAD_TRA_0224,
 PAD_TRA_0225,
 PAD_TRA_0226,
 PAD_TRA_0227,
 PAD_TRA_0228,
 PAD_TRA_0229,
 PAD_TRA_022A,
 PAD_TRA_022B,
 PAD_TRA_022C,
 PAD_TRA_022D,
 PAD_TRA_022E,
 PAD_TRA_022F,
 PAD_TRA_0230,
 PAD_TRA_0231,
 PAD_TRA_0232,
 PAD_TRA_0233,
 PAD_TRA_0234,
 PAD_TRA_0235,
 PAD_TRA_0236,
 PAD_TRA_0237,
 PAD_TRA_0238,
 PAD_TRA_0239,
 PAD_TRA_023A,
 PAD_TRA_023B,
 PAD_TRA_023C,
 PAD_TRA_023D,
 PAD_TRA_023E,
 PAD_TRA_023F,
 PAD_TRA_0240,
 PAD_TRA_0241,
 PAD_TRA_0242,
 PAD_TRA_0243,
 PAD_TRA_0244,
 PAD_TRA_0245,
 PAD_TRA_0246,
 PAD_TRA_0247,
 PAD_TRA_0248,
 PAD_TRA_0249,
 PAD_TRA_024A,
 PAD_TRA_024B,
 PAD_TRA_024C,
 PAD_TRA_024D,
 PAD_TRA_024E,
 PAD_TRA_024F,
 PAD_TRA_0250,
 PAD_TRA_0251,
 PAD_TRA_0252,
 PAD_TRA_0253,
 PAD_TRA_0254,
 PAD_TRA_0255,
 PAD_TRA_0256,
 PAD_TRA_0257,
 PAD_TRA_0258,
 PAD_TRA_0259,
 PAD_TRA_025A,
 PAD_TRA_025B,
 PAD_TRA_025C,
 PAD_TRA_025D,
 PAD_TRA_025E,
 PAD_TRA_025F,
 PAD_TRA_0260,
 PAD_TRA_0261,
 PAD_TRA_0262,
 PAD_TRA_0263,
 PAD_TRA_0264,
 PAD_TRA_0265,
 PAD_TRA_0266,
 PAD_TRA_0267,
 PAD_TRA_0268,
 PAD_TRA_0269,
 PAD_TRA_026A,
 PAD_TRA_026B,
 PAD_TRA_026C,
 PAD_TRA_026D,
 PAD_TRA_026E,
 PAD_TRA_026F,
 PAD_TRA_0270,
 PAD_TRA_0271,
 PAD_TRA_0272,
 PAD_TRA_0273,
 PAD_TRA_0274,
 PAD_TRA_0275,
 PAD_TRA_0276,
 PAD_TRA_0277,
 PAD_TRA_0278,
 PAD_TRA_0279,
 PAD_TRA_027A,
 PAD_TRA_027B,
 PAD_TRA_027C,
 PAD_TRA_027D,
 PAD_TRA_027E,
 PAD_TRA_027F,
 PAD_TRA_0280,
 PAD_TRA_0281,
 PAD_TRA_0282,
 PAD_TRA_0283,
 PAD_TRA_0284,
 PAD_TRA_0285,
 PAD_TRA_0286,
 PAD_TRA_0287,
 PAD_TRA_0288,
 PAD_TRA_0289,
 PAD_TRA_028A,
 PAD_TRA_028B,
 PAD_TRA_028C,
 PAD_TRA_028D,
 PAD_TRA_028E,
 PAD_TRA_028F,
 PAD_TRA_0290,
 PAD_TRA_0291,
 PAD_TRA_0292,
 PAD_TRA_0293,
 PAD_TRA_0294,
 PAD_TRA_0295,
 PAD_TRA_0296,
 PAD_TRA_0297,
 PAD_TRA_0298,
 PAD_TRA_0299,
 PAD_TRA_029A,
 PAD_TRA_029B,
 PAD_TRA_029C,
 PAD_TRA_029D,
 PAD_TRA_029E,
 PAD_TRA_029F,
 PAD_TRA_02A0,
 PAD_TRA_02A1,
 PAD_TRA_02A2,
 PAD_TRA_02A3,
 PAD_TRA_02A4,
 PAD_TRA_02A5,
 PAD_TRA_02A6,
 PAD_TRA_02A7,
 PAD_TRA_02A8,
 PAD_TRA_02A9,
 PAD_TRA_02AA,
 PAD_TRA_02AB,
 PAD_TRA_02AC,
 PAD_TRA_02AD,
 PAD_TRA_02AE,
 PAD_TRA_02AF,
 PAD_TRA_02B0,
 PAD_TRA_02B1,
 PAD_TRA_02B2,
 PAD_TRA_02B3,
 PAD_TRA_02B4,
 PAD_TRA_02B5,
 PAD_TRA_02B6,
 PAD_TRA_02B7,
 PAD_TRA_02B8,
 PAD_TRA_02B9,
 PAD_TRA_02BA,
 PAD_TRA_02BB,
 PAD_TRA_02BC,
 PAD_TRA_02BD,
 PAD_TRA_02BE,
 PAD_TRA_02BF,
 PAD_TRA_02C0,
 PAD_TRA_02C1,
 PAD_TRA_02C2,
 PAD_TRA_02C3,
 PAD_TRA_02C4,
 PAD_TRA_02C5,
 PAD_TRA_02C6,
 PAD_TRA_02C7,
 PAD_TRA_02C8,
 PAD_TRA_02C9,
 PAD_TRA_02CA,
 PAD_TRA_02CB,
 PAD_TRA_02CC,
 PAD_TRA_02CD,
 PAD_TRA_02CE,
 PAD_TRA_02CF,
 PAD_TRA_02D0,
 PAD_TRA_02D1,
 PAD_TRA_02D2,
 PAD_TRA_02D3,
 PAD_TRA_02D4,
 PAD_TRA_02D5,
 PAD_TRA_02D6,
 PAD_TRA_02D7,
 PAD_TRA_02D8,
 PAD_TRA_02D9,
 PAD_TRA_02DA,
 PAD_TRA_02DB,
 PAD_TRA_02DC,
 PAD_TRA_02DD,
 PAD_TRA_02DE,
 PAD_TRA_02DF,
 PAD_TRA_02E0,
 PAD_TRA_02E1,
 PAD_TRA_02E2,
 PAD_TRA_02E3,
 PAD_TRA_02E4,
 PAD_TRA_02E5,
 PAD_TRA_02E6,
 PAD_TRA_02E7,
 PAD_TRA_02E8,
 PAD_TRA_02E9,
 PAD_TRA_02EA,
 PAD_TRA_02EB,
 PAD_TRA_02EC,
 PAD_TRA_02ED,
 PAD_TRA_02EE,
 PAD_TRA_02EF,
 PAD_TRA_02F0,
 PAD_TRA_02F1,
 PAD_TRA_02F2,
 PAD_TRA_02F3,
 PAD_TRA_02F4,
 PAD_TRA_02F5,
 PAD_TRA_02F6,
 PAD_TRA_02F7,
 PAD_TRA_02F8,
 PAD_TRA_02F9,
 PAD_TRA_02FA,
 PAD_TRA_02FB,
 PAD_TRA_02FC,
 PAD_TRA_02FD,
 PAD_TRA_02FE,
 PAD_TRA_02FF,
 PAD_TRA_0300,
 PAD_TRA_0301,
 PAD_TRA_0302,
 PAD_TRA_0303,
 PAD_TRA_0304,
 PAD_TRA_0305,
 PAD_TRA_0306,
 PAD_TRA_0307,
 PAD_TRA_0308,
 PAD_TRA_0309,
 PAD_TRA_030A,
 PAD_TRA_030B,
 PAD_TRA_030C,
 PAD_TRA_030D,
 PAD_TRA_030E,
 PAD_TRA_030F,
 PAD_TRA_0310,
 PAD_TRA_0311,
 PAD_TRA_0312,
 PAD_TRA_0313,
 PAD_TRA_0314,
 PAD_TRA_0315,
 PAD_TRA_0316,
 PAD_TRA_0317,
 PAD_TRA_0318,
 PAD_TRA_0319,
 PAD_TRA_031A,
 PAD_TRA_031B,
 PAD_TRA_031C,
 PAD_TRA_031D,
 PAD_TRA_031E,
 PAD_TRA_031F,
 PAD_TRA_0320,
 PAD_TRA_0321,
 PAD_TRA_0322,
 PAD_TRA_0323,
 PAD_TRA_0324,
 PAD_TRA_0325,
 PAD_TRA_0326,
 PAD_TRA_0327,
 PAD_TRA_0328,
 PAD_TRA_0329,
 PAD_TRA_032A,
 PAD_TRA_032B,
 PAD_TRA_032C,
 PAD_TRA_032D,
 PAD_TRA_032E,
 PAD_TRA_032F,
 PAD_TRA_0330,
 PAD_TRA_0331,
 PAD_TRA_0332,
 PAD_TRA_0333,
 PAD_TRA_0334,
 PAD_TRA_0335,
 PAD_TRA_0336,
 PAD_TRA_0337,
 PAD_TRA_0338,
 PAD_TRA_0339,
 PAD_TRA_033A,
 PAD_TRA_033B,
 PAD_TRA_033C,
 PAD_TRA_033D,
 PAD_TRA_033E,
 PAD_TRA_033F,
 PAD_TRA_0340,
 PAD_TRA_0341,
 PAD_TRA_0342,
 PAD_TRA_0343,
 PAD_TRA_0344,
 PAD_TRA_0345,
 PAD_TRA_0346,
 PAD_TRA_0347,
 PAD_TRA_0348,
 PAD_TRA_0349,
 PAD_TRA_034A,
 PAD_TRA_034B,
 PAD_TRA_034C,
 PAD_TRA_034D,
 PAD_TRA_034E,
 PAD_TRA_034F,
 PAD_TRA_0350,
 PAD_TRA_0351,
 PAD_TRA_0352,
 PAD_TRA_0353,
 PAD_TRA_0354,
 PAD_TRA_0355,
 PAD_TRA_0356,
 PAD_TRA_0357,
 PAD_TRA_0358,
 PAD_TRA_0359,
 PAD_TRA_035A,
 PAD_TRA_035B,
 PAD_TRA_035C,
 PAD_TRA_035D,
 PAD_TRA_035E,
 PAD_TRA_035F,
 PAD_TRA_0360,
 PAD_TRA_0361,
 PAD_TRA_0362,
 PAD_TRA_0363,
 PAD_TRA_0364,
 PAD_TRA_0365,
 PAD_TRA_0366,
 PAD_TRA_0367,
 PAD_TRA_0368,
 PAD_TRA_0369,
 PAD_TRA_036A,
 PAD_TRA_036B,
 PAD_TRA_036C,
 PAD_TRA_036D,
 PAD_TRA_036E,
 PAD_TRA_036F,
 PAD_TRA_0370,
 PAD_TRA_0371,
 PAD_TRA_0372,
 PAD_TRA_0373,
 PAD_TRA_0374,
 PAD_TRA_0375,
 PAD_TRA_0376,
 PAD_TRA_0377,
 PAD_TRA_0378,
 PAD_TRA_0379,
 PAD_TRA_037A,
 PAD_TRA_037B,
 PAD_TRA_037C,
 PAD_TRA_037D,
 PAD_TRA_037E,
 PAD_TRA_037F,
 PAD_TRA_0380,
 PAD_TRA_0381,
 PAD_TRA_0382,
 PAD_TRA_0383,
 PAD_TRA_0384,
 PAD_TRA_0385,
 PAD_TRA_0386,
 PAD_TRA_0387,
 PAD_TRA_0388,
 PAD_TRA_0389,
 PAD_TRA_038A,
 PAD_TRA_038B,
 PAD_TRA_038C,
 PAD_TRA_038D,
 PAD_TRA_038E,
 PAD_TRA_038F,
 PAD_TRA_0390,
 PAD_TRA_0391,
 PAD_TRA_0392,
 PAD_TRA_0393,
 PAD_TRA_0394,
 PAD_TRA_0395,
 PAD_TRA_0396,
 PAD_TRA_0397,
 PAD_TRA_0398,
 PAD_TRA_0399,
 PAD_TRA_039A,
 PAD_TRA_039B,
 PAD_TRA_039C,
 PAD_TRA_039D,
 PAD_TRA_039E,
 PAD_TRA_039F,
 PAD_TRA_03A0,
 PAD_TRA_03A1,
 PAD_TRA_03A2,
 PAD_TRA_03A3,
 PAD_TRA_03A4,
 PAD_TRA_03A5,
 PAD_TRA_03A6,
 PAD_TRA_03A7,
 PAD_TRA_03A8,
 PAD_TRA_03A9,
 PAD_TRA_03AA,
 PAD_TRA_03AB,
 PAD_TRA_03AC,
 PAD_TRA_03AD,
 PAD_TRA_03AE,
 PAD_TRA_03AF,
 PAD_TRA_03B0,
 PAD_TRA_03B1,
 PAD_TRA_03B2,
 PAD_TRA_03B3,
 PAD_TRA_03B4,
 PAD_TRA_03B5,
 PAD_TRA_03B6,
 PAD_TRA_03B7,
 PAD_TRA_03B8,
 PAD_TRA_03B9,
 PAD_TRA_03BA,
 PAD_TRA_03BB,
 PAD_TRA_03BC,
 PAD_TRA_03BD,
 PAD_TRA_03BE,
 PAD_TRA_03BF,
 PAD_TRA_03C0,
 PAD_TRA_03C1,
 PAD_TRA_03C2,
 PAD_TRA_03C3,
 PAD_TRA_03C4,
 PAD_TRA_03C5,
 PAD_TRA_03C6,
 PAD_TRA_03C7,
 PAD_TRA_03C8,
 PAD_TRA_03C9,
 PAD_TRA_03CA,
 PAD_TRA_03CB,
 PAD_TRA_03CC,
 PAD_TRA_03CD,
 PAD_TRA_03CE,
 PAD_TRA_03CF,
 PAD_TRA_03D0,
 PAD_TRA_03D1,
 PAD_TRA_03D2,
 PAD_TRA_03D3,
 PAD_TRA_03D4,
 PAD_TRA_03D5,
 PAD_TRA_03D6,
 PAD_TRA_03D7,
 PAD_TRA_03D8,
 PAD_TRA_03D9,
 PAD_TRA_03DA,
 PAD_TRA_03DB,
 PAD_TRA_03DC,
 PAD_TRA_03DD,
 PAD_TRA_03DE,
 PAD_TRA_03DF,
 PAD_TRA_03E0,
 PAD_TRA_03E1,
 PAD_TRA_03E2,
 PAD_TRA_03E3,
 PAD_TRA_03E4,
 PAD_TRA_03E5,
 PAD_TRA_03E6,
 PAD_TRA_03E7,
 PAD_TRA_03E8,
 PAD_TRA_03E9,
 PAD_TRA_03EA,
 PAD_TRA_03EB,
 PAD_TRA_03EC,
 PAD_TRA_03ED,
 PAD_TRA_03EE,
 PAD_TRA_03EF,
 PAD_TRA_03F0,
 PAD_TRA_03F1,
 PAD_TRA_03F2,
 PAD_TRA_03F3,
 PAD_TRA_03F4,
 PAD_TRA_03F5,
 PAD_TRA_03F6,
 PAD_TRA_03F7,
 PAD_TRA_03F8,
 PAD_TRA_03F9,
 PAD_TRA_03FA,
 PAD_TRA_03FB,
 PAD_TRA_03FC,
 PAD_TRA_03FD,
 PAD_TRA_03FE,
 PAD_TRA_03FF,
 PAD_TRA_0400,
 PAD_TRA_0401,
 PAD_TRA_0402,
 PAD_TRA_0403,
 PAD_TRA_0404,
 PAD_TRA_0405,
 PAD_TRA_0406,
 PAD_TRA_0407,
 PAD_TRA_0408,
 PAD_TRA_0409,
 PAD_TRA_040A,
 PAD_TRA_040B,
 PAD_TRA_040C,
 PAD_TRA_040D,
 PAD_TRA_040E,
 PAD_TRA_040F,
 PAD_TRA_0410,
 PAD_TRA_0411,
 PAD_TRA_0412,
 PAD_TRA_0413,
 PAD_TRA_0414,
 PAD_TRA_0415,
 PAD_TRA_0416,
 PAD_TRA_0417,
 PAD_TRA_0418,
 PAD_TRA_0419,
 PAD_TRA_041A,
 PAD_TRA_041B,
 PAD_TRA_041C,
 PAD_TRA_041D,
 PAD_TRA_041E,
 PAD_TRA_041F,
 PAD_TRA_0420,
 PAD_TRA_0421,
 PAD_TRA_0422,
 PAD_TRA_0423,
 PAD_TRA_0424,
 PAD_TRA_0425,
 PAD_TRA_0426,
 PAD_TRA_0427,
 PAD_TRA_0428,
 PAD_TRA_0429,
 PAD_TRA_042A,
 PAD_TRA_042B,
 PAD_TRA_042C,
 PAD_TRA_042D,
 PAD_TRA_042E,
 PAD_TRA_042F,
 PAD_TRA_0430,
 PAD_TRA_0431,
 PAD_TRA_0432,
 PAD_TRA_0433,
 PAD_TRA_0434,
 PAD_TRA_0435,
 PAD_TRA_0436,
 PAD_TRA_0437,
 PAD_TRA_0438,
 PAD_TRA_0439,
 PAD_TRA_043A,
 PAD_TRA_043B,
 PAD_TRA_043C,
 PAD_TRA_043D,
 PAD_TRA_043E,
 PAD_TRA_043F,
 PAD_TRA_0440,
 PAD_TRA_0441,
 PAD_TRA_0442,
 PAD_TRA_0443,
 PAD_TRA_0444,
 PAD_TRA_0445,
 PAD_TRA_0446,
 PAD_TRA_0447,
 PAD_TRA_0448,
 PAD_TRA_0449,
 PAD_TRA_044A,
 PAD_TRA_044B,
 PAD_TRA_044C,
 PAD_TRA_044D,
 PAD_TRA_044E,
 PAD_TRA_044F,
 PAD_TRA_0450,
 PAD_TRA_0451,
 PAD_TRA_0452,
 PAD_TRA_0453,
 PAD_TRA_0454,
 PAD_TRA_0455,
 PAD_TRA_0456,
 PAD_TRA_0457,
 PAD_TRA_0458,
 PAD_TRA_0459,
 PAD_TRA_045A,
 PAD_TRA_045B,
 PAD_TRA_045C,
 PAD_TRA_045D,
 PAD_TRA_045E,
 PAD_TRA_045F,
 PAD_TRA_0460,
 PAD_TRA_0461,
 PAD_TRA_0462,
 PAD_TRA_0463,
 PAD_TRA_0464,
 PAD_TRA_0465,
 PAD_TRA_0466,
 PAD_TRA_0467,
 PAD_TRA_0468,
 PAD_TRA_0469,
 PAD_TRA_046A,
 PAD_TRA_046B,
 PAD_TRA_046C,
 PAD_TRA_046D,
 PAD_TRA_046E,
 PAD_TRA_046F,
 PAD_TRA_0470,
 PAD_TRA_0471,
 PAD_TRA_0472,
 PAD_TRA_0473,
 PAD_TRA_0474,
 PAD_TRA_0475,
 PAD_TRA_0476,
 PAD_TRA_0477,
 PAD_TRA_0478,
 PAD_TRA_0479,
 PAD_TRA_047A,
 PAD_TRA_047B,
 PAD_TRA_047C,
 PAD_TRA_047D,
 PAD_TRA_047E,
 PAD_TRA_047F,
 PAD_TRA_0480,
 PAD_TRA_0481,
 PAD_TRA_0482,
 PAD_TRA_0483,
 PAD_TRA_0484,
 PAD_TRA_0485,
 PAD_TRA_0486,
 PAD_TRA_0487,
 PAD_TRA_0488,
 PAD_TRA_0489,
 PAD_TRA_048A,
 PAD_TRA_048B,
 PAD_TRA_048C,
 PAD_TRA_048D,
 PAD_TRA_048E,
 PAD_TRA_048F,
 PAD_TRA_0490,
 PAD_TRA_0491,
 PAD_TRA_0492,
 PAD_TRA_0493,
 PAD_TRA_0494,
 PAD_TRA_0495,
 PAD_TRA_0496,
 PAD_TRA_0497,
 PAD_TRA_0498,
 PAD_TRA_0499,
 PAD_TRA_049A,
 PAD_TRA_049B,
 PAD_TRA_049C,
 PAD_TRA_049D,
 PAD_TRA_049E,
 PAD_TRA_049F,
 PAD_TRA_04A0,
 PAD_TRA_04A1,
 PAD_TRA_04A2,
 PAD_TRA_04A3,
 PAD_TRA_04A4,
 PAD_TRA_04A5,
 PAD_TRA_04A6,
 PAD_TRA_04A7,
 PAD_TRA_04A8,
 PAD_TRA_04A9,
 PAD_TRA_04AA,
 PAD_TRA_04AB,
 PAD_TRA_04AC,
 PAD_TRA_04AD,
 PAD_TRA_04AE,
 PAD_TRA_04AF,
 PAD_TRA_04B0,
 PAD_TRA_04B1,
 PAD_TRA_04B2,
 PAD_TRA_04B3,
 PAD_TRA_04B4,
 PAD_TRA_04B5,
 PAD_TRA_04B6,
 PAD_TRA_04B7,
 PAD_TRA_04B8,
 PAD_TRA_04B9,
 PAD_TRA_04BA,
 PAD_TRA_04BB,
 PAD_TRA_04BC,
 PAD_TRA_04BD,
 PAD_TRA_04BE,
 PAD_TRA_04BF,
 PAD_TRA_04C0,
 PAD_TRA_04C1,
 PAD_TRA_04C2,
 PAD_TRA_04C3,
 PAD_TRA_04C4,
 PAD_TRA_04C5,
 PAD_TRA_04C6,
 PAD_TRA_04C7,
 PAD_TRA_04C8,
 PAD_TRA_04C9,
 PAD_TRA_04CA,
 PAD_TRA_04CB,
 PAD_TRA_04CC,
 PAD_TRA_04CD,
 PAD_TRA_04CE,
 PAD_TRA_04CF,
 PAD_TRA_04D0,
 PAD_TRA_04D1,
 PAD_TRA_04D2,
 PAD_TRA_04D3,
 PAD_TRA_04D4,
 PAD_TRA_04D5,
 PAD_TRA_04D6,
 PAD_TRA_04D7,
 PAD_TRA_04D8,
 PAD_TRA_04D9,
 PAD_TRA_04DA,
 PAD_TRA_04DB,
 PAD_TRA_04DC,
 PAD_TRA_04DD,
 PAD_TRA_04DE,
 PAD_TRA_04DF,
 PAD_TRA_04E0,
 PAD_TRA_04E1,
 PAD_TRA_04E2,
 PAD_TRA_04E3,
 PAD_TRA_04E4,
 PAD_TRA_04E5,
 PAD_TRA_04E6,
 PAD_TRA_04E7,
 PAD_TRA_04E8,
 PAD_TRA_04E9,
 PAD_TRA_04EA,
 PAD_TRA_04EB,
 PAD_TRA_04EC,
 PAD_TRA_04ED,
 PAD_TRA_04EE,
 PAD_TRA_04EF,
 PAD_TRA_04F0,
 PAD_TRA_04F1,
 PAD_TRA_04F2,
 PAD_TRA_04F3,
 PAD_TRA_04F4,
 PAD_TRA_04F5,
 PAD_TRA_04F6,
 PAD_TRA_04F7,
 PAD_TRA_04F8,
 PAD_TRA_04F9,
 PAD_TRA_04FA,
 PAD_TRA_04FB,
 PAD_TRA_04FC,
 PAD_TRA_04FD,
 PAD_TRA_04FE,
 PAD_TRA_04FF,
 PAD_TRA_0500,
 PAD_TRA_0501,
 PAD_TRA_0502,
 PAD_TRA_0503,
 PAD_TRA_0504,
 PAD_TRA_0505,
 PAD_TRA_0506,
 PAD_TRA_0507,
 PAD_TRA_0508,
 PAD_TRA_0509,
 PAD_TRA_050A,
 PAD_TRA_050B,
 PAD_TRA_050C,
 PAD_TRA_050D,
 PAD_TRA_050E,
 PAD_TRA_050F,
 PAD_TRA_0510,
 PAD_TRA_0511,
 PAD_TRA_0512,
 PAD_TRA_0513,
 PAD_TRA_0514,
 PAD_TRA_0515,
 PAD_TRA_0516,
 PAD_TRA_0517,
 PAD_TRA_0518,
 PAD_TRA_0519,
 PAD_TRA_051A,
 PAD_TRA_051B,
 PAD_TRA_051C,
 PAD_TRA_051D,
 PAD_TRA_051E,
 PAD_TRA_051F,
 PAD_TRA_0520,
 PAD_TRA_0521,
 PAD_TRA_0522,
 PAD_TRA_0523,
 PAD_TRA_0524,
 PAD_TRA_0525,
 PAD_TRA_0526,
 PAD_TRA_0527,
 PAD_TRA_0528,
 PAD_TRA_0529,
 PAD_TRA_052A,
 PAD_TRA_052B,
 PAD_TRA_052C,
 PAD_TRA_052D,
 PAD_TRA_052E,
 PAD_TRA_052F,
 PAD_TRA_0530,
 PAD_TRA_0531,
 PAD_TRA_0532,
 PAD_TRA_0533,
 PAD_TRA_0534,
 PAD_TRA_0535,
 PAD_TRA_0536,
 PAD_TRA_0537,
 PAD_TRA_0538,
 PAD_TRA_0539,
 PAD_TRA_053A,
 PAD_TRA_053B,
 PAD_TRA_053C,
 PAD_TRA_053D,
 PAD_TRA_053E,
 PAD_TRA_053F,
 PAD_TRA_0540,
 PAD_TRA_0541,
 PAD_TRA_0542,
 PAD_TRA_0543,
 PAD_TRA_0544,
 PAD_TRA_0545,
 PAD_TRA_0546,
 PAD_TRA_0547,
 PAD_TRA_0548,
 PAD_TRA_0549,
 PAD_TRA_054A,
 PAD_TRA_054B,
 PAD_TRA_054C,
 PAD_TRA_054D,
 PAD_TRA_054E,
 PAD_TRA_054F,
 PAD_TRA_0550,
 PAD_TRA_0551,
 PAD_TRA_0552,
 PAD_TRA_0553,
 PAD_TRA_0554,
 PAD_TRA_0555,
 PAD_TRA_0556,
 PAD_TRA_0557,
 PAD_TRA_0558,
 PAD_TRA_0559,
 PAD_TRA_055A,
 PAD_TRA_055B,
 PAD_TRA_055C,
 PAD_TRA_055D,
 PAD_TRA_055E,
 PAD_TRA_055F,
 PAD_TRA_0560,
 PAD_TRA_0561,
 PAD_TRA_0562,
 PAD_TRA_0563,
 PAD_TRA_0564,
 PAD_TRA_0565,
 PAD_TRA_0566,
 PAD_TRA_0567,
 PAD_TRA_END
};
enum pad_wax {
 PAD_WAX_0000,
 PAD_WAX_0001,
 PAD_WAX_0002,
 PAD_WAX_0003,
 PAD_WAX_0004,
 PAD_WAX_0005,
 PAD_WAX_0006,
 PAD_WAX_0007,
 PAD_WAX_0008,
 PAD_WAX_0009,
 PAD_WAX_000A,
 PAD_WAX_000B,
 PAD_WAX_000C,
 PAD_WAX_000D,
 PAD_WAX_000E,
 PAD_WAX_000F,
 PAD_WAX_0010,
 PAD_WAX_0011,
 PAD_WAX_0012,
 PAD_WAX_0013,
 PAD_WAX_0014,
 PAD_WAX_0015,
 PAD_WAX_0016,
 PAD_WAX_0017,
 PAD_WAX_0018,
 PAD_WAX_0019,
 PAD_WAX_001A,
 PAD_WAX_001B,
 PAD_WAX_001C,
 PAD_WAX_001D,
 PAD_WAX_001E,
 PAD_WAX_001F,
 PAD_WAX_0020,
 PAD_WAX_0021,
 PAD_WAX_0022,
 PAD_WAX_0023,
 PAD_WAX_0024,
 PAD_WAX_0025,
 PAD_WAX_0026,
 PAD_WAX_0027,
 PAD_WAX_0028,
 PAD_WAX_0029,
 PAD_WAX_002A,
 PAD_WAX_002B,
 PAD_WAX_002C,
 PAD_WAX_002D,
 PAD_WAX_002E,
 PAD_WAX_002F,
 PAD_WAX_0030,
 PAD_WAX_0031,
 PAD_WAX_0032,
 PAD_WAX_0033,
 PAD_WAX_0034,
 PAD_WAX_0035,
 PAD_WAX_0036,
 PAD_WAX_0037,
 PAD_WAX_0038,
 PAD_WAX_0039,
 PAD_WAX_003A,
 PAD_WAX_003B,
 PAD_WAX_003C,
 PAD_WAX_003D,
 PAD_WAX_003E,
 PAD_WAX_003F,
 PAD_WAX_0040,
 PAD_WAX_0041,
 PAD_WAX_0042,
 PAD_WAX_0043,
 PAD_WAX_0044,
 PAD_WAX_0045,
 PAD_WAX_0046,
 PAD_WAX_0047,
 PAD_WAX_0048,
 PAD_WAX_0049,
 PAD_WAX_004A,
 PAD_WAX_004B,
 PAD_WAX_004C,
 PAD_WAX_004D,
 PAD_WAX_004E,
 PAD_WAX_004F,
 PAD_WAX_0050,
 PAD_WAX_0051,
 PAD_WAX_0052,
 PAD_WAX_0053,
 PAD_WAX_0054,
 PAD_WAX_0055,
 PAD_WAX_0056,
 PAD_WAX_0057,
 PAD_WAX_0058,
 PAD_WAX_0059,
 PAD_WAX_005A,
 PAD_WAX_005B,
 PAD_WAX_005C,
 PAD_WAX_005D,
 PAD_WAX_005E,
 PAD_WAX_005F,
 PAD_WAX_0060,
 PAD_WAX_0061,
 PAD_WAX_0062,
 PAD_WAX_0063,
 PAD_WAX_0064,
 PAD_WAX_0065,
 PAD_WAX_0066,
 PAD_WAX_0067,
 PAD_WAX_0068,
 PAD_WAX_0069,
 PAD_WAX_006A,
 PAD_WAX_006B,
 PAD_WAX_006C,
 PAD_WAX_006D,
 PAD_WAX_006E,
 PAD_WAX_006F,
 PAD_WAX_0070,
 PAD_WAX_0071,
 PAD_WAX_0072,
 PAD_WAX_0073,
 PAD_WAX_0074,
 PAD_WAX_0075,
 PAD_WAX_0076,
 PAD_WAX_0077,
 PAD_WAX_0078,
 PAD_WAX_0079,
 PAD_WAX_007A,
 PAD_WAX_007B,
 PAD_WAX_007C,
 PAD_WAX_007D,
 PAD_WAX_007E,
 PAD_WAX_007F,
 PAD_WAX_0080,
 PAD_WAX_0081,
 PAD_WAX_0082,
 PAD_WAX_0083,
 PAD_WAX_0084,
 PAD_WAX_0085,
 PAD_WAX_0086,
 PAD_WAX_0087,
 PAD_WAX_0088,
 PAD_WAX_0089,
 PAD_WAX_008A,
 PAD_WAX_008B,
 PAD_WAX_008C,
 PAD_WAX_008D,
 PAD_WAX_008E,
 PAD_WAX_008F,
 PAD_WAX_0090,
 PAD_WAX_0091,
 PAD_WAX_0092,
 PAD_WAX_0093,
 PAD_WAX_0094,
 PAD_WAX_0095,
 PAD_WAX_0096,
 PAD_WAX_0097,
 PAD_WAX_0098,
 PAD_WAX_0099,
 PAD_WAX_009A,
 PAD_WAX_009B,
 PAD_WAX_009C,
 PAD_WAX_009D,
 PAD_WAX_009E,
 PAD_WAX_009F,
 PAD_WAX_00A0,
 PAD_WAX_00A1,
 PAD_WAX_00A2,
 PAD_WAX_00A3,
 PAD_WAX_00A4,
 PAD_WAX_00A5,
 PAD_WAX_00A6,
 PAD_WAX_00A7,
 PAD_WAX_00A8,
 PAD_WAX_00A9,
 PAD_WAX_00AA,
 PAD_WAX_00AB,
 PAD_WAX_00AC,
 PAD_WAX_00AD,
 PAD_WAX_00AE,
 PAD_WAX_00AF,
 PAD_WAX_00B0,
 PAD_WAX_00B1,
 PAD_WAX_00B2,
 PAD_WAX_00B3,
 PAD_WAX_00B4,
 PAD_WAX_00B5,
 PAD_WAX_00B6,
 PAD_WAX_00B7,
 PAD_WAX_00B8,
 PAD_WAX_00B9,
 PAD_WAX_00BA,
 PAD_WAX_00BB,
 PAD_WAX_00BC,
 PAD_WAX_00BD,
 PAD_WAX_00BE,
 PAD_WAX_00BF,
 PAD_WAX_00C0,
 PAD_WAX_00C1,
 PAD_WAX_00C2,
 PAD_WAX_00C3,
 PAD_WAX_00C4,
 PAD_WAX_00C5,
 PAD_WAX_00C6,
 PAD_WAX_00C7,
 PAD_WAX_00C8,
 PAD_WAX_00C9,
 PAD_WAX_00CA,
 PAD_WAX_00CB,
 PAD_WAX_00CC,
 PAD_WAX_00CD,
 PAD_WAX_00CE,
 PAD_WAX_00CF,
 PAD_WAX_00D0,
 PAD_WAX_00D1,
 PAD_WAX_00D2,
 PAD_WAX_00D3,
 PAD_WAX_00D4,
 PAD_WAX_00D5,
 PAD_WAX_00D6,
 PAD_WAX_00D7,
 PAD_WAX_00D8,
 PAD_WAX_00D9,
 PAD_WAX_00DA,
 PAD_WAX_00DB,
 PAD_WAX_00DC,
 PAD_WAX_00DD,
 PAD_WAX_00DE,
 PAD_WAX_00DF,
 PAD_WAX_00E0,
 PAD_WAX_00E1,
 PAD_WAX_00E2,
 PAD_WAX_00E3,
 PAD_WAX_00E4,
 PAD_WAX_00E5,
 PAD_WAX_00E6,
 PAD_WAX_00E7,
 PAD_WAX_00E8,
 PAD_WAX_00E9,
 PAD_WAX_00EA,
 PAD_WAX_00EB,
 PAD_WAX_00EC,
 PAD_WAX_00ED,
 PAD_WAX_00EE,
 PAD_WAX_00EF,
 PAD_WAX_00F0,
 PAD_WAX_00F1,
 PAD_WAX_00F2,
 PAD_WAX_00F3,
 PAD_WAX_00F4,
 PAD_WAX_00F5,
 PAD_WAX_00F6,
 PAD_WAX_00F7,
 PAD_WAX_00F8,
 PAD_WAX_00F9,
 PAD_WAX_00FA,
 PAD_WAX_00FB,
 PAD_WAX_00FC,
 PAD_WAX_00FD,
 PAD_WAX_00FE,
 PAD_WAX_00FF,
 PAD_WAX_0100,
 PAD_WAX_0101,
 PAD_WAX_0102,
 PAD_WAX_0103,
 PAD_WAX_0104,
 PAD_WAX_0105,
 PAD_WAX_0106,
 PAD_WAX_0107,
 PAD_WAX_0108,
 PAD_WAX_0109,
 PAD_WAX_010A,
 PAD_WAX_010B,
 PAD_WAX_010C,
 PAD_WAX_010D,
 PAD_WAX_010E,
 PAD_WAX_010F,
 PAD_WAX_0110,
 PAD_WAX_0111,
 PAD_WAX_0112,
 PAD_WAX_0113,
 PAD_WAX_0114,
 PAD_WAX_0115,
 PAD_WAX_0116,
 PAD_WAX_0117,
 PAD_WAX_0118,
 PAD_WAX_0119,
 PAD_WAX_011A,
 PAD_WAX_011B,
 PAD_WAX_011C,
 PAD_WAX_011D,
 PAD_WAX_011E,
 PAD_WAX_011F,
 PAD_WAX_0120,
 PAD_WAX_0121,
 PAD_WAX_0122,
 PAD_WAX_0123,
 PAD_WAX_0124,
 PAD_WAX_0125,
 PAD_WAX_0126,
 PAD_WAX_0127,
 PAD_WAX_0128,
 PAD_WAX_0129,
 PAD_WAX_012A,
 PAD_WAX_012B,
 PAD_WAX_012C,
 PAD_WAX_012D,
 PAD_WAX_012E,
 PAD_WAX_012F,
 PAD_WAX_0130,
 PAD_WAX_0131,
 PAD_WAX_0132,
 PAD_WAX_0133,
 PAD_WAX_0134,
 PAD_WAX_0135,
 PAD_WAX_0136,
 PAD_WAX_0137,
 PAD_WAX_0138,
 PAD_WAX_0139,
 PAD_WAX_013A,
 PAD_WAX_013B,
 PAD_WAX_013C,
 PAD_WAX_013D,
 PAD_WAX_013E,
 PAD_WAX_013F,
 PAD_WAX_0140,
 PAD_WAX_0141,
 PAD_WAX_0142,
 PAD_WAX_0143,
 PAD_WAX_0144,
 PAD_WAX_0145,
 PAD_WAX_0146,
 PAD_WAX_0147,
 PAD_WAX_0148,
 PAD_WAX_0149,
 PAD_WAX_014A,
 PAD_WAX_014B,
 PAD_WAX_014C,
 PAD_WAX_014D,
 PAD_WAX_014E,
 PAD_WAX_014F,
 PAD_WAX_0150,
 PAD_WAX_0151,
 PAD_WAX_0152,
 PAD_WAX_0153,
 PAD_WAX_0154,
 PAD_WAX_0155,
 PAD_WAX_0156,
 PAD_WAX_0157,
 PAD_WAX_0158,
 PAD_WAX_0159,
 PAD_WAX_015A,
 PAD_WAX_015B,
 PAD_WAX_015C,
 PAD_WAX_015D,
 PAD_WAX_015E,
 PAD_WAX_015F,
 PAD_WAX_0160,
 PAD_WAX_0161,
 PAD_WAX_0162,
 PAD_WAX_0163,
 PAD_WAX_0164,
 PAD_WAX_0165,
 PAD_WAX_0166,
 PAD_WAX_0167,
 PAD_WAX_0168,
 PAD_WAX_0169,
 PAD_WAX_016A,
 PAD_WAX_016B,
 PAD_WAX_016C,
 PAD_WAX_016D,
 PAD_WAX_016E,
 PAD_WAX_016F,
 PAD_WAX_0170,
 PAD_WAX_0171,
 PAD_WAX_0172,
 PAD_WAX_0173,
 PAD_WAX_0174,
 PAD_WAX_0175,
 PAD_WAX_0176,
 PAD_WAX_0177,
 PAD_WAX_0178,
 PAD_WAX_0179,
 PAD_WAX_017A,
 PAD_WAX_017B,
 PAD_WAX_017C,
 PAD_WAX_017D,
 PAD_WAX_017E,
 PAD_WAX_017F,
 PAD_WAX_0180,
 PAD_WAX_0181,
 PAD_WAX_0182,
 PAD_WAX_0183,
 PAD_WAX_0184,
 PAD_WAX_0185,
 PAD_WAX_0186,
 PAD_WAX_0187,
 PAD_WAX_0188,
 PAD_WAX_0189,
 PAD_WAX_018A,
 PAD_WAX_018B,
 PAD_WAX_018C,
 PAD_WAX_018D,
 PAD_WAX_018E,
 PAD_WAX_018F,
 PAD_WAX_0190,
 PAD_WAX_0191,
 PAD_WAX_0192,
 PAD_WAX_0193,
 PAD_WAX_0194,
 PAD_WAX_0195,
 PAD_WAX_0196,
 PAD_WAX_0197,
 PAD_WAX_0198,
 PAD_WAX_0199,
 PAD_WAX_019A,
 PAD_WAX_019B,
 PAD_WAX_019C,
 PAD_WAX_019D,
 PAD_WAX_019E,
 PAD_WAX_019F,
 PAD_WAX_01A0,
 PAD_WAX_01A1,
 PAD_WAX_01A2,
 PAD_WAX_01A3,
 PAD_WAX_01A4,
 PAD_WAX_01A5,
 PAD_WAX_01A6,
 PAD_WAX_01A7,
 PAD_WAX_01A8,
 PAD_WAX_01A9,
 PAD_WAX_01AA,
 PAD_WAX_01AB,
 PAD_WAX_01AC,
 PAD_WAX_01AD,
 PAD_WAX_01AE,
 PAD_WAX_01AF,
 PAD_WAX_01B0,
 PAD_WAX_01B1,
 PAD_WAX_01B2,
 PAD_WAX_01B3,
 PAD_WAX_01B4,
 PAD_WAX_01B5,
 PAD_WAX_01B6,
 PAD_WAX_01B7,
 PAD_WAX_01B8,
 PAD_WAX_01B9,
 PAD_WAX_01BA,
 PAD_WAX_01BB,
 PAD_WAX_01BC,
 PAD_WAX_01BD,
 PAD_WAX_01BE,
 PAD_WAX_01BF,
 PAD_WAX_01C0,
 PAD_WAX_01C1,
 PAD_WAX_01C2,
 PAD_WAX_01C3,
 PAD_WAX_01C4,
 PAD_WAX_01C5,
 PAD_WAX_01C6,
 PAD_WAX_01C7,
 PAD_WAX_01C8,
 PAD_WAX_01C9,
 PAD_WAX_01CA,
 PAD_WAX_01CB,
 PAD_WAX_01CC,
 PAD_WAX_01CD,
 PAD_WAX_01CE,
 PAD_WAX_01CF,
 PAD_WAX_01D0,
 PAD_WAX_01D1,
 PAD_WAX_01D2,
 PAD_WAX_01D3,
 PAD_WAX_01D4,
 PAD_WAX_01D5,
 PAD_WAX_01D6,
 PAD_WAX_01D7,
 PAD_WAX_01D8,
 PAD_WAX_01D9,
 PAD_WAX_01DA,
 PAD_WAX_01DB,
 PAD_WAX_01DC,
 PAD_WAX_01DD,
 PAD_WAX_01DE,
 PAD_WAX_01DF,
 PAD_WAX_01E0,
 PAD_WAX_01E1,
 PAD_WAX_01E2,
 PAD_WAX_01E3,
 PAD_WAX_01E4,
 PAD_WAX_01E5,
 PAD_WAX_01E6,
 PAD_WAX_01E7,
 PAD_WAX_01E8,
 PAD_WAX_01E9,
 PAD_WAX_01EA,
 PAD_WAX_01EB,
 PAD_WAX_01EC,
 PAD_WAX_01ED,
 PAD_WAX_01EE,
 PAD_WAX_01EF,
 PAD_WAX_01F0,
 PAD_WAX_01F1,
 PAD_WAX_01F2,
 PAD_WAX_01F3,
 PAD_WAX_01F4,
 PAD_WAX_01F5,
 PAD_WAX_01F6,
 PAD_WAX_01F7,
 PAD_WAX_01F8,
 PAD_WAX_01F9,
 PAD_WAX_01FA,
 PAD_WAX_01FB,
 PAD_WAX_01FC,
 PAD_WAX_01FD,
 PAD_WAX_01FE,
 PAD_WAX_01FF,
 PAD_WAX_0200,
 PAD_WAX_0201,
 PAD_WAX_0202,
 PAD_WAX_0203,
 PAD_WAX_0204,
 PAD_WAX_0205,
 PAD_WAX_0206,
 PAD_WAX_0207,
 PAD_WAX_0208,
 PAD_WAX_0209,
 PAD_WAX_020A,
 PAD_WAX_020B,
 PAD_WAX_020C,
 PAD_WAX_020D,
 PAD_WAX_020E,
 PAD_WAX_020F,
 PAD_WAX_0210,
 PAD_WAX_0211,
 PAD_WAX_0212,
 PAD_WAX_0213,
 PAD_WAX_0214,
 PAD_WAX_0215,
 PAD_WAX_0216,
 PAD_WAX_0217,
 PAD_WAX_0218,
 PAD_WAX_0219,
 PAD_WAX_021A,
 PAD_WAX_021B,
 PAD_WAX_021C,
 PAD_WAX_021D,
 PAD_WAX_021E,
 PAD_WAX_021F,
 PAD_WAX_0220,
 PAD_WAX_0221,
 PAD_WAX_0222,
 PAD_WAX_0223,
 PAD_WAX_0224,
 PAD_WAX_0225,
 PAD_WAX_0226,
 PAD_WAX_0227,
 PAD_WAX_0228,
 PAD_WAX_0229,
 PAD_WAX_022A,
 PAD_WAX_022B,
 PAD_WAX_022C,
 PAD_WAX_022D,
 PAD_WAX_022E,
 PAD_WAX_022F,
 PAD_WAX_0230,
 PAD_WAX_0231,
 PAD_WAX_0232,
 PAD_WAX_0233,
 PAD_WAX_0234,
 PAD_WAX_0235,
 PAD_WAX_0236,
 PAD_WAX_0237,
 PAD_WAX_0238,
 PAD_WAX_0239,
 PAD_WAX_023A,
 PAD_WAX_023B,
 PAD_WAX_023C,
 PAD_WAX_023D,
 PAD_WAX_023E,
 PAD_WAX_023F,
 PAD_WAX_0240,
 PAD_WAX_0241,
 PAD_WAX_0242,
 PAD_WAX_0243,
 PAD_WAX_0244,
 PAD_WAX_0245,
 PAD_WAX_0246,
 PAD_WAX_0247,
 PAD_WAX_0248,
 PAD_WAX_0249,
 PAD_WAX_024A,
 PAD_WAX_024B,
 PAD_WAX_024C,
 PAD_WAX_024D,
 PAD_WAX_024E,
 PAD_WAX_024F,
 PAD_WAX_0250,
 PAD_WAX_0251,
 PAD_WAX_0252,
 PAD_WAX_0253,
 PAD_WAX_0254,
 PAD_WAX_0255,
 PAD_WAX_0256,
 PAD_WAX_0257,
 PAD_WAX_0258,
 PAD_WAX_0259,
 PAD_WAX_025A,
 PAD_WAX_025B,
 PAD_WAX_025C,
 PAD_WAX_025D,
 PAD_WAX_025E,
 PAD_WAX_025F,
 PAD_WAX_END
};
enum pad_uff {
 PAD_UFF_END
};
enum pad_old {
 PAD_OLD_END
};
enum pad_ate {
 PAD_ATE_0000,
 PAD_ATE_0001,
 PAD_ATE_0002,
 PAD_ATE_0003,
 PAD_ATE_0004,
 PAD_ATE_0005,
 PAD_ATE_0006,
 PAD_ATE_0007,
 PAD_ATE_0008,
 PAD_ATE_0009,
 PAD_ATE_000A,
 PAD_ATE_000B,
 PAD_ATE_000C,
 PAD_ATE_000D,
 PAD_ATE_000E,
 PAD_ATE_000F,
 PAD_ATE_0010,
 PAD_ATE_0011,
 PAD_ATE_0012,
 PAD_ATE_0013,
 PAD_ATE_0014,
 PAD_ATE_0015,
 PAD_ATE_0016,
 PAD_ATE_0017,
 PAD_ATE_0018,
 PAD_ATE_0019,
 PAD_ATE_001A,
 PAD_ATE_001B,
 PAD_ATE_001C,
 PAD_ATE_001D,
 PAD_ATE_001E,
 PAD_ATE_001F,
 PAD_ATE_0020,
 PAD_ATE_0021,
 PAD_ATE_0022,
 PAD_ATE_0023,
 PAD_ATE_0024,
 PAD_ATE_0025,
 PAD_ATE_0026,
 PAD_ATE_0027,
 PAD_ATE_0028,
 PAD_ATE_0029,
 PAD_ATE_002A,
 PAD_ATE_002B,
 PAD_ATE_002C,
 PAD_ATE_002D,
 PAD_ATE_002E,
 PAD_ATE_002F,
 PAD_ATE_0030,
 PAD_ATE_0031,
 PAD_ATE_0032,
 PAD_ATE_0033,
 PAD_ATE_0034,
 PAD_ATE_0035,
 PAD_ATE_0036,
 PAD_ATE_0037,
 PAD_ATE_0038,
 PAD_ATE_0039,
 PAD_ATE_003A,
 PAD_ATE_003B,
 PAD_ATE_003C,
 PAD_ATE_003D,
 PAD_ATE_003E,
 PAD_ATE_003F,
 PAD_ATE_0040,
 PAD_ATE_0041,
 PAD_ATE_0042,
 PAD_ATE_0043,
 PAD_ATE_0044,
 PAD_ATE_0045,
 PAD_ATE_0046,
 PAD_ATE_0047,
 PAD_ATE_0048,
 PAD_ATE_0049,
 PAD_ATE_004A,
 PAD_ATE_004B,
 PAD_ATE_004C,
 PAD_ATE_004D,
 PAD_ATE_004E,
 PAD_ATE_004F,
 PAD_ATE_0050,
 PAD_ATE_0051,
 PAD_ATE_0052,
 PAD_ATE_0053,
 PAD_ATE_0054,
 PAD_ATE_0055,
 PAD_ATE_0056,
 PAD_ATE_0057,
 PAD_ATE_0058,
 PAD_ATE_0059,
 PAD_ATE_005A,
 PAD_ATE_005B,
 PAD_ATE_005C,
 PAD_ATE_005D,
 PAD_ATE_005E,
 PAD_ATE_005F,
 PAD_ATE_0060,
 PAD_ATE_0061,
 PAD_ATE_0062,
 PAD_ATE_0063,
 PAD_ATE_0064,
 PAD_ATE_0065,
 PAD_ATE_0066,
 PAD_ATE_0067,
 PAD_ATE_0068,
 PAD_ATE_0069,
 PAD_ATE_006A,
 PAD_ATE_006B,
 PAD_ATE_006C,
 PAD_ATE_006D,
 PAD_ATE_006E,
 PAD_ATE_006F,
 PAD_ATE_0070,
 PAD_ATE_0071,
 PAD_ATE_0072,
 PAD_ATE_0073,
 PAD_ATE_0074,
 PAD_ATE_0075,
 PAD_ATE_0076,
 PAD_ATE_0077,
 PAD_ATE_0078,
 PAD_ATE_0079,
 PAD_ATE_007A,
 PAD_ATE_007B,
 PAD_ATE_007C,
 PAD_ATE_007D,
 PAD_ATE_007E,
 PAD_ATE_007F,
 PAD_ATE_0080,
 PAD_ATE_0081,
 PAD_ATE_0082,
 PAD_ATE_0083,
 PAD_ATE_0084,
 PAD_ATE_0085,
 PAD_ATE_0086,
 PAD_ATE_0087,
 PAD_ATE_0088,
 PAD_ATE_0089,
 PAD_ATE_008A,
 PAD_ATE_008B,
 PAD_ATE_008C,
 PAD_ATE_008D,
 PAD_ATE_008E,
 PAD_ATE_008F,
 PAD_ATE_0090,
 PAD_ATE_0091,
 PAD_ATE_0092,
 PAD_ATE_0093,
 PAD_ATE_0094,
 PAD_ATE_0095,
 PAD_ATE_0096,
 PAD_ATE_0097,
 PAD_ATE_0098,
 PAD_ATE_0099,
 PAD_ATE_009A,
 PAD_ATE_009B,
 PAD_ATE_009C,
 PAD_ATE_009D,
 PAD_ATE_009E,
 PAD_ATE_009F,
 PAD_ATE_00A0,
 PAD_ATE_00A1,
 PAD_ATE_00A2,
 PAD_ATE_00A3,
 PAD_ATE_00A4,
 PAD_ATE_00A5,
 PAD_ATE_00A6,
 PAD_ATE_00A7,
 PAD_ATE_00A8,
 PAD_ATE_00A9,
 PAD_ATE_00AA,
 PAD_ATE_00AB,
 PAD_ATE_00AC,
 PAD_ATE_00AD,
 PAD_ATE_00AE,
 PAD_ATE_00AF,
 PAD_ATE_00B0,
 PAD_ATE_00B1,
 PAD_ATE_00B2,
 PAD_ATE_00B3,
 PAD_ATE_00B4,
 PAD_ATE_00B5,
 PAD_ATE_00B6,
 PAD_ATE_00B7,
 PAD_ATE_00B8,
 PAD_ATE_00B9,
 PAD_ATE_00BA,
 PAD_ATE_00BB,
 PAD_ATE_00BC,
 PAD_ATE_00BD,
 PAD_ATE_00BE,
 PAD_ATE_00BF,
 PAD_ATE_00C0,
 PAD_ATE_00C1,
 PAD_ATE_00C2,
 PAD_ATE_00C3,
 PAD_ATE_00C4,
 PAD_ATE_00C5,
 PAD_ATE_00C6,
 PAD_ATE_00C7,
 PAD_ATE_00C8,
 PAD_ATE_00C9,
 PAD_ATE_00CA,
 PAD_ATE_00CB,
 PAD_ATE_00CC,
 PAD_ATE_00CD,
 PAD_ATE_00CE,
 PAD_ATE_00CF,
 PAD_ATE_00D0,
 PAD_ATE_00D1,
 PAD_ATE_00D2,
 PAD_ATE_00D3,
 PAD_ATE_00D4,
 PAD_ATE_00D5,
 PAD_ATE_00D6,
 PAD_ATE_00D7,
 PAD_ATE_00D8,
 PAD_ATE_00D9,
 PAD_ATE_00DA,
 PAD_ATE_00DB,
 PAD_ATE_00DC,
 PAD_ATE_00DD,
 PAD_ATE_00DE,
 PAD_ATE_00DF,
 PAD_ATE_00E0,
 PAD_ATE_00E1,
 PAD_ATE_00E2,
 PAD_ATE_00E3,
 PAD_ATE_00E4,
 PAD_ATE_00E5,
 PAD_ATE_00E6,
 PAD_ATE_00E7,
 PAD_ATE_00E8,
 PAD_ATE_00E9,
 PAD_ATE_00EA,
 PAD_ATE_00EB,
 PAD_ATE_00EC,
 PAD_ATE_00ED,
 PAD_ATE_00EE,
 PAD_ATE_00EF,
 PAD_ATE_00F0,
 PAD_ATE_00F1,
 PAD_ATE_00F2,
 PAD_ATE_00F3,
 PAD_ATE_00F4,
 PAD_ATE_00F5,
 PAD_ATE_00F6,
 PAD_ATE_00F7,
 PAD_ATE_00F8,
 PAD_ATE_00F9,
 PAD_ATE_00FA,
 PAD_ATE_00FB,
 PAD_ATE_00FC,
 PAD_ATE_00FD,
 PAD_ATE_00FE,
 PAD_ATE_00FF,
 PAD_ATE_0100,
 PAD_ATE_0101,
 PAD_ATE_0102,
 PAD_ATE_0103,
 PAD_ATE_0104,
 PAD_ATE_0105,
 PAD_ATE_0106,
 PAD_ATE_0107,
 PAD_ATE_0108,
 PAD_ATE_0109,
 PAD_ATE_010A,
 PAD_ATE_010B,
 PAD_ATE_010C,
 PAD_ATE_010D,
 PAD_ATE_010E,
 PAD_ATE_010F,
 PAD_ATE_0110,
 PAD_ATE_0111,
 PAD_ATE_0112,
 PAD_ATE_0113,
 PAD_ATE_0114,
 PAD_ATE_0115,
 PAD_ATE_0116,
 PAD_ATE_0117,
 PAD_ATE_0118,
 PAD_ATE_0119,
 PAD_ATE_011A,
 PAD_ATE_011B,
 PAD_ATE_011C,
 PAD_ATE_011D,
 PAD_ATE_011E,
 PAD_ATE_011F,
 PAD_ATE_0120,
 PAD_ATE_0121,
 PAD_ATE_0122,
 PAD_ATE_0123,
 PAD_ATE_0124,
 PAD_ATE_0125,
 PAD_ATE_0126,
 PAD_ATE_0127,
 PAD_ATE_0128,
 PAD_ATE_0129,
 PAD_ATE_012A,
 PAD_ATE_012B,
 PAD_ATE_012C,
 PAD_ATE_012D,
 PAD_ATE_012E,
 PAD_ATE_012F,
 PAD_ATE_0130,
 PAD_ATE_0131,
 PAD_ATE_0132,
 PAD_ATE_0133,
 PAD_ATE_0134,
 PAD_ATE_0135,
 PAD_ATE_0136,
 PAD_ATE_0137,
 PAD_ATE_0138,
 PAD_ATE_0139,
 PAD_ATE_013A,
 PAD_ATE_013B,
 PAD_ATE_013C,
 PAD_ATE_013D,
 PAD_ATE_013E,
 PAD_ATE_013F,
 PAD_ATE_0140,
 PAD_ATE_0141,
 PAD_ATE_0142,
 PAD_ATE_0143,
 PAD_ATE_0144,
 PAD_ATE_0145,
 PAD_ATE_0146,
 PAD_ATE_0147,
 PAD_ATE_0148,
 PAD_ATE_0149,
 PAD_ATE_014A,
 PAD_ATE_014B,
 PAD_ATE_014C,
 PAD_ATE_014D,
 PAD_ATE_014E,
 PAD_ATE_014F,
 PAD_ATE_0150,
 PAD_ATE_0151,
 PAD_ATE_0152,
 PAD_ATE_0153,
 PAD_ATE_0154,
 PAD_ATE_0155,
 PAD_ATE_0156,
 PAD_ATE_0157,
 PAD_ATE_0158,
 PAD_ATE_0159,
 PAD_ATE_015A,
 PAD_ATE_015B,
 PAD_ATE_015C,
 PAD_ATE_015D,
 PAD_ATE_015E,
 PAD_ATE_015F,
 PAD_ATE_0160,
 PAD_ATE_0161,
 PAD_ATE_0162,
 PAD_ATE_0163,
 PAD_ATE_0164,
 PAD_ATE_0165,
 PAD_ATE_0166,
 PAD_ATE_0167,
 PAD_ATE_0168,
 PAD_ATE_0169,
 PAD_ATE_016A,
 PAD_ATE_016B,
 PAD_ATE_016C,
 PAD_ATE_016D,
 PAD_ATE_016E,
 PAD_ATE_016F,
 PAD_ATE_0170,
 PAD_ATE_0171,
 PAD_ATE_0172,
 PAD_ATE_0173,
 PAD_ATE_0174,
 PAD_ATE_0175,
 PAD_ATE_0176,
 PAD_ATE_0177,
 PAD_ATE_0178,
 PAD_ATE_0179,
 PAD_ATE_017A,
 PAD_ATE_017B,
 PAD_ATE_017C,
 PAD_ATE_017D,
 PAD_ATE_017E,
 PAD_ATE_017F,
 PAD_ATE_0180,
 PAD_ATE_0181,
 PAD_ATE_0182,
 PAD_ATE_0183,
 PAD_ATE_0184,
 PAD_ATE_0185,
 PAD_ATE_0186,
 PAD_ATE_0187,
 PAD_ATE_0188,
 PAD_ATE_0189,
 PAD_ATE_018A,
 PAD_ATE_018B,
 PAD_ATE_018C,
 PAD_ATE_018D,
 PAD_ATE_018E,
 PAD_ATE_018F,
 PAD_ATE_0190,
 PAD_ATE_0191,
 PAD_ATE_0192,
 PAD_ATE_0193,
 PAD_ATE_0194,
 PAD_ATE_0195,
 PAD_ATE_0196,
 PAD_ATE_0197,
 PAD_ATE_0198,
 PAD_ATE_0199,
 PAD_ATE_019A,
 PAD_ATE_019B,
 PAD_ATE_019C,
 PAD_ATE_019D,
 PAD_ATE_019E,
 PAD_ATE_019F,
 PAD_ATE_01A0,
 PAD_ATE_01A1,
 PAD_ATE_01A2,
 PAD_ATE_01A3,
 PAD_ATE_01A4,
 PAD_ATE_01A5,
 PAD_ATE_01A6,
 PAD_ATE_01A7,
 PAD_ATE_01A8,
 PAD_ATE_01A9,
 PAD_ATE_01AA,
 PAD_ATE_01AB,
 PAD_ATE_01AC,
 PAD_ATE_01AD,
 PAD_ATE_01AE,
 PAD_ATE_01AF,
 PAD_ATE_01B0,
 PAD_ATE_01B1,
 PAD_ATE_01B2,
 PAD_ATE_01B3,
 PAD_ATE_01B4,
 PAD_ATE_01B5,
 PAD_ATE_01B6,
 PAD_ATE_01B7,
 PAD_ATE_01B8,
 PAD_ATE_01B9,
 PAD_ATE_01BA,
 PAD_ATE_01BB,
 PAD_ATE_01BC,
 PAD_ATE_01BD,
 PAD_ATE_01BE,
 PAD_ATE_01BF,
 PAD_ATE_01C0,
 PAD_ATE_01C1,
 PAD_ATE_01C2,
 PAD_ATE_01C3,
 PAD_ATE_01C4,
 PAD_ATE_01C5,
 PAD_ATE_01C6,
 PAD_ATE_01C7,
 PAD_ATE_01C8,
 PAD_ATE_01C9,
 PAD_ATE_01CA,
 PAD_ATE_01CB,
 PAD_ATE_01CC,
 PAD_ATE_01CD,
 PAD_ATE_01CE,
 PAD_ATE_01CF,
 PAD_ATE_01D0,
 PAD_ATE_01D1,
 PAD_ATE_01D2,
 PAD_ATE_01D3,
 PAD_ATE_01D4,
 PAD_ATE_01D5,
 PAD_ATE_01D6,
 PAD_ATE_01D7,
 PAD_ATE_01D8,
 PAD_ATE_01D9,
 PAD_ATE_01DA,
 PAD_ATE_01DB,
 PAD_ATE_01DC,
 PAD_ATE_01DD,
 PAD_ATE_01DE,
 PAD_ATE_01DF,
 PAD_ATE_01E0,
 PAD_ATE_01E1,
 PAD_ATE_01E2,
 PAD_ATE_01E3,
 PAD_ATE_01E4,
 PAD_ATE_01E5,
 PAD_ATE_01E6,
 PAD_ATE_01E7,
 PAD_ATE_01E8,
 PAD_ATE_01E9,
 PAD_ATE_01EA,
 PAD_ATE_01EB,
 PAD_ATE_01EC,
 PAD_ATE_01ED,
 PAD_ATE_01EE,
 PAD_ATE_01EF,
 PAD_ATE_01F0,
 PAD_ATE_01F1,
 PAD_ATE_01F2,
 PAD_ATE_01F3,
 PAD_ATE_01F4,
 PAD_ATE_01F5,
 PAD_ATE_01F6,
 PAD_ATE_01F7,
 PAD_ATE_01F8,
 PAD_ATE_01F9,
 PAD_ATE_01FA,
 PAD_ATE_01FB,
 PAD_ATE_01FC,
 PAD_ATE_01FD,
 PAD_ATE_01FE,
 PAD_ATE_01FF,
 PAD_ATE_0200,
 PAD_ATE_0201,
 PAD_ATE_0202,
 PAD_ATE_0203,
 PAD_ATE_0204,
 PAD_ATE_0205,
 PAD_ATE_0206,
 PAD_ATE_0207,
 PAD_ATE_0208,
 PAD_ATE_0209,
 PAD_ATE_020A,
 PAD_ATE_020B,
 PAD_ATE_020C,
 PAD_ATE_020D,
 PAD_ATE_020E,
 PAD_ATE_020F,
 PAD_ATE_0210,
 PAD_ATE_0211,
 PAD_ATE_0212,
 PAD_ATE_0213,
 PAD_ATE_0214,
 PAD_ATE_0215,
 PAD_ATE_0216,
 PAD_ATE_0217,
 PAD_ATE_0218,
 PAD_ATE_0219,
 PAD_ATE_021A,
 PAD_ATE_021B,
 PAD_ATE_021C,
 PAD_ATE_021D,
 PAD_ATE_021E,
 PAD_ATE_021F,
 PAD_ATE_0220,
 PAD_ATE_0221,
 PAD_ATE_0222,
 PAD_ATE_0223,
 PAD_ATE_0224,
 PAD_ATE_0225,
 PAD_ATE_0226,
 PAD_ATE_0227,
 PAD_ATE_0228,
 PAD_ATE_0229,
 PAD_ATE_022A,
 PAD_ATE_022B,
 PAD_ATE_022C,
 PAD_ATE_022D,
 PAD_ATE_022E,
 PAD_ATE_022F,
 PAD_ATE_0230,
 PAD_ATE_0231,
 PAD_ATE_0232,
 PAD_ATE_0233,
 PAD_ATE_0234,
 PAD_ATE_0235,
 PAD_ATE_0236,
 PAD_ATE_0237,
 PAD_ATE_0238,
 PAD_ATE_0239,
 PAD_ATE_023A,
 PAD_ATE_023B,
 PAD_ATE_023C,
 PAD_ATE_023D,
 PAD_ATE_023E,
 PAD_ATE_023F,
 PAD_ATE_0240,
 PAD_ATE_0241,
 PAD_ATE_0242,
 PAD_ATE_0243,
 PAD_ATE_0244,
 PAD_ATE_0245,
 PAD_ATE_0246,
 PAD_ATE_0247,
 PAD_ATE_0248,
 PAD_ATE_0249,
 PAD_ATE_024A,
 PAD_ATE_024B,
 PAD_ATE_024C,
 PAD_ATE_024D,
 PAD_ATE_024E,
 PAD_ATE_024F,
 PAD_ATE_0250,
 PAD_ATE_0251,
 PAD_ATE_0252,
 PAD_ATE_0253,
 PAD_ATE_0254,
 PAD_ATE_0255,
 PAD_ATE_0256,
 PAD_ATE_0257,
 PAD_ATE_0258,
 PAD_ATE_0259,
 PAD_ATE_025A,
 PAD_ATE_025B,
 PAD_ATE_025C,
 PAD_ATE_025D,
 PAD_ATE_025E,
 PAD_ATE_025F,
 PAD_ATE_0260,
 PAD_ATE_0261,
 PAD_ATE_0262,
 PAD_ATE_0263,
 PAD_ATE_0264,
 PAD_ATE_0265,
 PAD_ATE_0266,
 PAD_ATE_0267,
 PAD_ATE_0268,
 PAD_ATE_0269,
 PAD_ATE_026A,
 PAD_ATE_026B,
 PAD_ATE_026C,
 PAD_ATE_026D,
 PAD_ATE_026E,
 PAD_ATE_026F,
 PAD_ATE_0270,
 PAD_ATE_0271,
 PAD_ATE_0272,
 PAD_ATE_0273,
 PAD_ATE_0274,
 PAD_ATE_0275,
 PAD_ATE_0276,
 PAD_ATE_0277,
 PAD_ATE_0278,
 PAD_ATE_0279,
 PAD_ATE_027A,
 PAD_ATE_027B,
 PAD_ATE_027C,
 PAD_ATE_027D,
 PAD_ATE_027E,
 PAD_ATE_027F,
 PAD_ATE_0280,
 PAD_ATE_0281,
 PAD_ATE_0282,
 PAD_ATE_0283,
 PAD_ATE_0284,
 PAD_ATE_0285,
 PAD_ATE_0286,
 PAD_ATE_0287,
 PAD_ATE_0288,
 PAD_ATE_0289,
 PAD_ATE_END
};
enum pad_lam {
 PAD_LAM_END
};
enum pad_mp1 {
 PAD_MP1_0000,
 PAD_MP1_0001,
 PAD_MP1_0002,
 PAD_MP1_0003,
 PAD_MP1_0004,
 PAD_MP1_0005,
 PAD_MP1_0006,
 PAD_MP1_0007,
 PAD_MP1_0008,
 PAD_MP1_0009,
 PAD_MP1_000A,
 PAD_MP1_000B,
 PAD_MP1_000C,
 PAD_MP1_000D,
 PAD_MP1_000E,
 PAD_MP1_000F,
 PAD_MP1_0010,
 PAD_MP1_0011,
 PAD_MP1_0012,
 PAD_MP1_0013,
 PAD_MP1_0014,
 PAD_MP1_0015,
 PAD_MP1_0016,
 PAD_MP1_0017,
 PAD_MP1_0018,
 PAD_MP1_0019,
 PAD_MP1_001A,
 PAD_MP1_001B,
 PAD_MP1_001C,
 PAD_MP1_001D,
 PAD_MP1_001E,
 PAD_MP1_001F,
 PAD_MP1_0020,
 PAD_MP1_0021,
 PAD_MP1_0022,
 PAD_MP1_0023,
 PAD_MP1_0024,
 PAD_MP1_0025,
 PAD_MP1_0026,
 PAD_MP1_0027,
 PAD_MP1_0028,
 PAD_MP1_0029,
 PAD_MP1_002A,
 PAD_MP1_002B,
 PAD_MP1_002C,
 PAD_MP1_002D,
 PAD_MP1_002E,
 PAD_MP1_002F,
 PAD_MP1_0030,
 PAD_MP1_0031,
 PAD_MP1_0032,
 PAD_MP1_0033,
 PAD_MP1_0034,
 PAD_MP1_0035,
 PAD_MP1_0036,
 PAD_MP1_0037,
 PAD_MP1_0038,
 PAD_MP1_0039,
 PAD_MP1_003A,
 PAD_MP1_003B,
 PAD_MP1_003C,
 PAD_MP1_003D,
 PAD_MP1_003E,
 PAD_MP1_003F,
 PAD_MP1_0040,
 PAD_MP1_0041,
 PAD_MP1_0042,
 PAD_MP1_0043,
 PAD_MP1_0044,
 PAD_MP1_0045,
 PAD_MP1_0046,
 PAD_MP1_0047,
 PAD_MP1_0048,
 PAD_MP1_0049,
 PAD_MP1_004A,
 PAD_MP1_004B,
 PAD_MP1_004C,
 PAD_MP1_004D,
 PAD_MP1_004E,
 PAD_MP1_004F,
 PAD_MP1_0050,
 PAD_MP1_0051,
 PAD_MP1_0052,
 PAD_MP1_0053,
 PAD_MP1_0054,
 PAD_MP1_0055,
 PAD_MP1_0056,
 PAD_MP1_0057,
 PAD_MP1_0058,
 PAD_MP1_0059,
 PAD_MP1_005A,
 PAD_MP1_005B,
 PAD_MP1_005C,
 PAD_MP1_005D,
 PAD_MP1_005E,
 PAD_MP1_005F,
 PAD_MP1_0060,
 PAD_MP1_0061,
 PAD_MP1_0062,
 PAD_MP1_0063,
 PAD_MP1_0064,
 PAD_MP1_0065,
 PAD_MP1_0066,
 PAD_MP1_0067,
 PAD_MP1_0068,
 PAD_MP1_0069,
 PAD_MP1_006A,
 PAD_MP1_006B,
 PAD_MP1_006C,
 PAD_MP1_006D,
 PAD_MP1_006E,
 PAD_MP1_006F,
 PAD_MP1_0070,
 PAD_MP1_0071,
 PAD_MP1_0072,
 PAD_MP1_0073,
 PAD_MP1_0074,
 PAD_MP1_0075,
 PAD_MP1_0076,
 PAD_MP1_0077,
 PAD_MP1_0078,
 PAD_MP1_0079,
 PAD_MP1_007A,
 PAD_MP1_007B,
 PAD_MP1_007C,
 PAD_MP1_007D,
 PAD_MP1_007E,
 PAD_MP1_007F,
 PAD_MP1_0080,
 PAD_MP1_0081,
 PAD_MP1_0082,
 PAD_MP1_0083,
 PAD_MP1_0084,
 PAD_MP1_0085,
 PAD_MP1_0086,
 PAD_MP1_0087,
 PAD_MP1_0088,
 PAD_MP1_0089,
 PAD_MP1_008A,
 PAD_MP1_008B,
 PAD_MP1_008C,
 PAD_MP1_008D,
 PAD_MP1_008E,
 PAD_MP1_008F,
 PAD_MP1_0090,
 PAD_MP1_0091,
 PAD_MP1_0092,
 PAD_MP1_0093,
 PAD_MP1_0094,
 PAD_MP1_0095,
 PAD_MP1_0096,
 PAD_MP1_0097,
 PAD_MP1_0098,
 PAD_MP1_0099,
 PAD_MP1_009A,
 PAD_MP1_009B,
 PAD_MP1_009C,
 PAD_MP1_009D,
 PAD_MP1_009E,
 PAD_MP1_009F,
 PAD_MP1_00A0,
 PAD_MP1_00A1,
 PAD_MP1_00A2,
 PAD_MP1_00A3,
 PAD_MP1_00A4,
 PAD_MP1_00A5,
 PAD_MP1_00A6,
 PAD_MP1_00A7,
 PAD_MP1_00A8,
 PAD_MP1_00A9,
 PAD_MP1_00AA,
 PAD_MP1_00AB,
 PAD_MP1_00AC,
 PAD_MP1_00AD,
 PAD_MP1_00AE,
 PAD_MP1_00AF,
 PAD_MP1_00B0,
 PAD_MP1_00B1,
 PAD_MP1_00B2,
 PAD_MP1_00B3,
 PAD_MP1_00B4,
 PAD_MP1_00B5,
 PAD_MP1_00B6,
 PAD_MP1_00B7,
 PAD_MP1_00B8,
 PAD_MP1_00B9,
 PAD_MP1_00BA,
 PAD_MP1_00BB,
 PAD_MP1_00BC,
 PAD_MP1_00BD,
 PAD_MP1_00BE,
 PAD_MP1_00BF,
 PAD_MP1_00C0,
 PAD_MP1_00C1,
 PAD_MP1_00C2,
 PAD_MP1_00C3,
 PAD_MP1_00C4,
 PAD_MP1_00C5,
 PAD_MP1_00C6,
 PAD_MP1_00C7,
 PAD_MP1_00C8,
 PAD_MP1_00C9,
 PAD_MP1_00CA,
 PAD_MP1_00CB,
 PAD_MP1_00CC,
 PAD_MP1_00CD,
 PAD_MP1_00CE,
 PAD_MP1_00CF,
 PAD_MP1_00D0,
 PAD_MP1_00D1,
 PAD_MP1_00D2,
 PAD_MP1_00D3,
 PAD_MP1_00D4,
 PAD_MP1_00D5,
 PAD_MP1_00D6,
 PAD_MP1_00D7,
 PAD_MP1_00D8,
 PAD_MP1_00D9,
 PAD_MP1_00DA,
 PAD_MP1_00DB,
 PAD_MP1_00DC,
 PAD_MP1_00DD,
 PAD_MP1_00DE,
 PAD_MP1_00DF,
 PAD_MP1_00E0,
 PAD_MP1_00E1,
 PAD_MP1_00E2,
 PAD_MP1_00E3,
 PAD_MP1_00E4,
 PAD_MP1_00E5,
 PAD_MP1_00E6,
 PAD_MP1_00E7,
 PAD_MP1_00E8,
 PAD_MP1_00E9,
 PAD_MP1_00EA,
 PAD_MP1_00EB,
 PAD_MP1_00EC,
 PAD_MP1_00ED,
 PAD_MP1_00EE,
 PAD_MP1_00EF,
 PAD_MP1_00F0,
 PAD_MP1_00F1,
 PAD_MP1_00F2,
 PAD_MP1_00F3,
 PAD_MP1_00F4,
 PAD_MP1_00F5,
 PAD_MP1_00F6,
 PAD_MP1_END
};
enum pad_mp2 {
 PAD_MP2_END
};
enum pad_mp3 {
 PAD_MP3_0000,
 PAD_MP3_0001,
 PAD_MP3_0002,
 PAD_MP3_0003,
 PAD_MP3_0004,
 PAD_MP3_0005,
 PAD_MP3_0006,
 PAD_MP3_0007,
 PAD_MP3_0008,
 PAD_MP3_0009,
 PAD_MP3_000A,
 PAD_MP3_000B,
 PAD_MP3_000C,
 PAD_MP3_000D,
 PAD_MP3_000E,
 PAD_MP3_000F,
 PAD_MP3_0010,
 PAD_MP3_0011,
 PAD_MP3_0012,
 PAD_MP3_0013,
 PAD_MP3_0014,
 PAD_MP3_0015,
 PAD_MP3_0016,
 PAD_MP3_0017,
 PAD_MP3_0018,
 PAD_MP3_0019,
 PAD_MP3_001A,
 PAD_MP3_001B,
 PAD_MP3_001C,
 PAD_MP3_001D,
 PAD_MP3_001E,
 PAD_MP3_001F,
 PAD_MP3_0020,
 PAD_MP3_0021,
 PAD_MP3_0022,
 PAD_MP3_0023,
 PAD_MP3_0024,
 PAD_MP3_0025,
 PAD_MP3_0026,
 PAD_MP3_0027,
 PAD_MP3_0028,
 PAD_MP3_0029,
 PAD_MP3_002A,
 PAD_MP3_002B,
 PAD_MP3_002C,
 PAD_MP3_002D,
 PAD_MP3_002E,
 PAD_MP3_002F,
 PAD_MP3_0030,
 PAD_MP3_0031,
 PAD_MP3_0032,
 PAD_MP3_0033,
 PAD_MP3_0034,
 PAD_MP3_0035,
 PAD_MP3_0036,
 PAD_MP3_0037,
 PAD_MP3_0038,
 PAD_MP3_0039,
 PAD_MP3_003A,
 PAD_MP3_003B,
 PAD_MP3_003C,
 PAD_MP3_003D,
 PAD_MP3_003E,
 PAD_MP3_003F,
 PAD_MP3_0040,
 PAD_MP3_0041,
 PAD_MP3_0042,
 PAD_MP3_0043,
 PAD_MP3_0044,
 PAD_MP3_0045,
 PAD_MP3_0046,
 PAD_MP3_0047,
 PAD_MP3_0048,
 PAD_MP3_0049,
 PAD_MP3_004A,
 PAD_MP3_004B,
 PAD_MP3_004C,
 PAD_MP3_004D,
 PAD_MP3_004E,
 PAD_MP3_004F,
 PAD_MP3_0050,
 PAD_MP3_0051,
 PAD_MP3_0052,
 PAD_MP3_0053,
 PAD_MP3_0054,
 PAD_MP3_0055,
 PAD_MP3_0056,
 PAD_MP3_0057,
 PAD_MP3_0058,
 PAD_MP3_0059,
 PAD_MP3_005A,
 PAD_MP3_005B,
 PAD_MP3_005C,
 PAD_MP3_005D,
 PAD_MP3_005E,
 PAD_MP3_005F,
 PAD_MP3_0060,
 PAD_MP3_0061,
 PAD_MP3_0062,
 PAD_MP3_0063,
 PAD_MP3_0064,
 PAD_MP3_0065,
 PAD_MP3_0066,
 PAD_MP3_0067,
 PAD_MP3_0068,
 PAD_MP3_0069,
 PAD_MP3_006A,
 PAD_MP3_006B,
 PAD_MP3_006C,
 PAD_MP3_006D,
 PAD_MP3_006E,
 PAD_MP3_006F,
 PAD_MP3_0070,
 PAD_MP3_0071,
 PAD_MP3_0072,
 PAD_MP3_0073,
 PAD_MP3_0074,
 PAD_MP3_0075,
 PAD_MP3_0076,
 PAD_MP3_0077,
 PAD_MP3_0078,
 PAD_MP3_0079,
 PAD_MP3_007A,
 PAD_MP3_007B,
 PAD_MP3_007C,
 PAD_MP3_007D,
 PAD_MP3_007E,
 PAD_MP3_007F,
 PAD_MP3_0080,
 PAD_MP3_0081,
 PAD_MP3_0082,
 PAD_MP3_0083,
 PAD_MP3_0084,
 PAD_MP3_0085,
 PAD_MP3_0086,
 PAD_MP3_0087,
 PAD_MP3_0088,
 PAD_MP3_0089,
 PAD_MP3_008A,
 PAD_MP3_008B,
 PAD_MP3_008C,
 PAD_MP3_008D,
 PAD_MP3_008E,
 PAD_MP3_008F,
 PAD_MP3_0090,
 PAD_MP3_0091,
 PAD_MP3_0092,
 PAD_MP3_0093,
 PAD_MP3_0094,
 PAD_MP3_0095,
 PAD_MP3_0096,
 PAD_MP3_0097,
 PAD_MP3_0098,
 PAD_MP3_0099,
 PAD_MP3_009A,
 PAD_MP3_009B,
 PAD_MP3_009C,
 PAD_MP3_009D,
 PAD_MP3_009E,
 PAD_MP3_009F,
 PAD_MP3_00A0,
 PAD_MP3_00A1,
 PAD_MP3_00A2,
 PAD_MP3_00A3,
 PAD_MP3_00A4,
 PAD_MP3_00A5,
 PAD_MP3_00A6,
 PAD_MP3_00A7,
 PAD_MP3_00A8,
 PAD_MP3_00A9,
 PAD_MP3_00AA,
 PAD_MP3_00AB,
 PAD_MP3_00AC,
 PAD_MP3_00AD,
 PAD_MP3_00AE,
 PAD_MP3_00AF,
 PAD_MP3_00B0,
 PAD_MP3_00B1,
 PAD_MP3_00B2,
 PAD_MP3_00B3,
 PAD_MP3_00B4,
 PAD_MP3_00B5,
 PAD_MP3_00B6,
 PAD_MP3_00B7,
 PAD_MP3_00B8,
 PAD_MP3_00B9,
 PAD_MP3_00BA,
 PAD_MP3_00BB,
 PAD_MP3_00BC,
 PAD_MP3_00BD,
 PAD_MP3_00BE,
 PAD_MP3_00BF,
 PAD_MP3_00C0,
 PAD_MP3_00C1,
 PAD_MP3_00C2,
 PAD_MP3_00C3,
 PAD_MP3_00C4,
 PAD_MP3_00C5,
 PAD_MP3_00C6,
 PAD_MP3_00C7,
 PAD_MP3_00C8,
 PAD_MP3_00C9,
 PAD_MP3_00CA,
 PAD_MP3_00CB,
 PAD_MP3_00CC,
 PAD_MP3_00CD,
 PAD_MP3_00CE,
 PAD_MP3_00CF,
 PAD_MP3_00D0,
 PAD_MP3_00D1,
 PAD_MP3_00D2,
 PAD_MP3_00D3,
 PAD_MP3_00D4,
 PAD_MP3_00D5,
 PAD_MP3_00D6,
 PAD_MP3_00D7,
 PAD_MP3_00D8,
 PAD_MP3_00D9,
 PAD_MP3_00DA,
 PAD_MP3_00DB,
 PAD_MP3_00DC,
 PAD_MP3_00DD,
 PAD_MP3_00DE,
 PAD_MP3_00DF,
 PAD_MP3_00E0,
 PAD_MP3_00E1,
 PAD_MP3_00E2,
 PAD_MP3_00E3,
 PAD_MP3_00E4,
 PAD_MP3_00E5,
 PAD_MP3_00E6,
 PAD_MP3_00E7,
 PAD_MP3_00E8,
 PAD_MP3_00E9,
 PAD_MP3_00EA,
 PAD_MP3_00EB,
 PAD_MP3_00EC,
 PAD_MP3_00ED,
 PAD_MP3_00EE,
 PAD_MP3_00EF,
 PAD_MP3_00F0,
 PAD_MP3_00F1,
 PAD_MP3_00F2,
 PAD_MP3_00F3,
 PAD_MP3_00F4,
 PAD_MP3_00F5,
 PAD_MP3_00F6,
 PAD_MP3_00F7,
 PAD_MP3_00F8,
 PAD_MP3_00F9,
 PAD_MP3_00FA,
 PAD_MP3_00FB,
 PAD_MP3_00FC,
 PAD_MP3_00FD,
 PAD_MP3_00FE,
 PAD_MP3_00FF,
 PAD_MP3_0100,
 PAD_MP3_0101,
 PAD_MP3_0102,
 PAD_MP3_0103,
 PAD_MP3_0104,
 PAD_MP3_0105,
 PAD_MP3_0106,
 PAD_MP3_0107,
 PAD_MP3_0108,
 PAD_MP3_0109,
 PAD_MP3_010A,
 PAD_MP3_010B,
 PAD_MP3_010C,
 PAD_MP3_010D,
 PAD_MP3_010E,
 PAD_MP3_010F,
 PAD_MP3_0110,
 PAD_MP3_0111,
 PAD_MP3_0112,
 PAD_MP3_0113,
 PAD_MP3_0114,
 PAD_MP3_0115,
 PAD_MP3_0116,
 PAD_MP3_0117,
 PAD_MP3_0118,
 PAD_MP3_0119,
 PAD_MP3_011A,
 PAD_MP3_011B,
 PAD_MP3_011C,
 PAD_MP3_END
};
enum pad_mp4 {
 PAD_MP4_0000,
 PAD_MP4_0001,
 PAD_MP4_0002,
 PAD_MP4_0003,
 PAD_MP4_0004,
 PAD_MP4_0005,
 PAD_MP4_0006,
 PAD_MP4_0007,
 PAD_MP4_0008,
 PAD_MP4_0009,
 PAD_MP4_000A,
 PAD_MP4_000B,
 PAD_MP4_000C,
 PAD_MP4_000D,
 PAD_MP4_000E,
 PAD_MP4_000F,
 PAD_MP4_0010,
 PAD_MP4_0011,
 PAD_MP4_0012,
 PAD_MP4_0013,
 PAD_MP4_0014,
 PAD_MP4_0015,
 PAD_MP4_0016,
 PAD_MP4_0017,
 PAD_MP4_0018,
 PAD_MP4_0019,
 PAD_MP4_001A,
 PAD_MP4_001B,
 PAD_MP4_001C,
 PAD_MP4_001D,
 PAD_MP4_001E,
 PAD_MP4_001F,
 PAD_MP4_0020,
 PAD_MP4_0021,
 PAD_MP4_0022,
 PAD_MP4_0023,
 PAD_MP4_0024,
 PAD_MP4_0025,
 PAD_MP4_0026,
 PAD_MP4_0027,
 PAD_MP4_0028,
 PAD_MP4_0029,
 PAD_MP4_002A,
 PAD_MP4_002B,
 PAD_MP4_002C,
 PAD_MP4_002D,
 PAD_MP4_002E,
 PAD_MP4_002F,
 PAD_MP4_0030,
 PAD_MP4_0031,
 PAD_MP4_0032,
 PAD_MP4_0033,
 PAD_MP4_0034,
 PAD_MP4_0035,
 PAD_MP4_0036,
 PAD_MP4_0037,
 PAD_MP4_0038,
 PAD_MP4_0039,
 PAD_MP4_003A,
 PAD_MP4_003B,
 PAD_MP4_003C,
 PAD_MP4_003D,
 PAD_MP4_003E,
 PAD_MP4_003F,
 PAD_MP4_0040,
 PAD_MP4_0041,
 PAD_MP4_0042,
 PAD_MP4_0043,
 PAD_MP4_0044,
 PAD_MP4_0045,
 PAD_MP4_0046,
 PAD_MP4_0047,
 PAD_MP4_0048,
 PAD_MP4_0049,
 PAD_MP4_004A,
 PAD_MP4_004B,
 PAD_MP4_004C,
 PAD_MP4_004D,
 PAD_MP4_004E,
 PAD_MP4_004F,
 PAD_MP4_0050,
 PAD_MP4_0051,
 PAD_MP4_0052,
 PAD_MP4_0053,
 PAD_MP4_0054,
 PAD_MP4_0055,
 PAD_MP4_0056,
 PAD_MP4_0057,
 PAD_MP4_0058,
 PAD_MP4_0059,
 PAD_MP4_005A,
 PAD_MP4_005B,
 PAD_MP4_005C,
 PAD_MP4_005D,
 PAD_MP4_005E,
 PAD_MP4_005F,
 PAD_MP4_0060,
 PAD_MP4_0061,
 PAD_MP4_0062,
 PAD_MP4_0063,
 PAD_MP4_0064,
 PAD_MP4_0065,
 PAD_MP4_0066,
 PAD_MP4_0067,
 PAD_MP4_0068,
 PAD_MP4_0069,
 PAD_MP4_006A,
 PAD_MP4_006B,
 PAD_MP4_006C,
 PAD_MP4_006D,
 PAD_MP4_006E,
 PAD_MP4_006F,
 PAD_MP4_0070,
 PAD_MP4_0071,
 PAD_MP4_0072,
 PAD_MP4_0073,
 PAD_MP4_0074,
 PAD_MP4_0075,
 PAD_MP4_0076,
 PAD_MP4_0077,
 PAD_MP4_0078,
 PAD_MP4_0079,
 PAD_MP4_007A,
 PAD_MP4_007B,
 PAD_MP4_007C,
 PAD_MP4_007D,
 PAD_MP4_007E,
 PAD_MP4_007F,
 PAD_MP4_0080,
 PAD_MP4_0081,
 PAD_MP4_0082,
 PAD_MP4_0083,
 PAD_MP4_0084,
 PAD_MP4_0085,
 PAD_MP4_0086,
 PAD_MP4_0087,
 PAD_MP4_0088,
 PAD_MP4_0089,
 PAD_MP4_008A,
 PAD_MP4_008B,
 PAD_MP4_008C,
 PAD_MP4_008D,
 PAD_MP4_008E,
 PAD_MP4_008F,
 PAD_MP4_0090,
 PAD_MP4_0091,
 PAD_MP4_0092,
 PAD_MP4_0093,
 PAD_MP4_0094,
 PAD_MP4_0095,
 PAD_MP4_0096,
 PAD_MP4_0097,
 PAD_MP4_0098,
 PAD_MP4_0099,
 PAD_MP4_009A,
 PAD_MP4_009B,
 PAD_MP4_009C,
 PAD_MP4_009D,
 PAD_MP4_009E,
 PAD_MP4_009F,
 PAD_MP4_00A0,
 PAD_MP4_00A1,
 PAD_MP4_00A2,
 PAD_MP4_00A3,
 PAD_MP4_00A4,
 PAD_MP4_00A5,
 PAD_MP4_00A6,
 PAD_MP4_00A7,
 PAD_MP4_00A8,
 PAD_MP4_00A9,
 PAD_MP4_00AA,
 PAD_MP4_00AB,
 PAD_MP4_00AC,
 PAD_MP4_00AD,
 PAD_MP4_00AE,
 PAD_MP4_00AF,
 PAD_MP4_00B0,
 PAD_MP4_00B1,
 PAD_MP4_00B2,
 PAD_MP4_00B3,
 PAD_MP4_00B4,
 PAD_MP4_00B5,
 PAD_MP4_00B6,
 PAD_MP4_00B7,
 PAD_MP4_00B8,
 PAD_MP4_00B9,
 PAD_MP4_00BA,
 PAD_MP4_00BB,
 PAD_MP4_00BC,
 PAD_MP4_00BD,
 PAD_MP4_00BE,
 PAD_MP4_00BF,
 PAD_MP4_00C0,
 PAD_MP4_00C1,
 PAD_MP4_00C2,
 PAD_MP4_00C3,
 PAD_MP4_00C4,
 PAD_MP4_00C5,
 PAD_MP4_00C6,
 PAD_MP4_00C7,
 PAD_MP4_00C8,
 PAD_MP4_00C9,
 PAD_MP4_00CA,
 PAD_MP4_00CB,
 PAD_MP4_00CC,
 PAD_MP4_00CD,
 PAD_MP4_00CE,
 PAD_MP4_00CF,
 PAD_MP4_00D0,
 PAD_MP4_00D1,
 PAD_MP4_00D2,
 PAD_MP4_00D3,
 PAD_MP4_00D4,
 PAD_MP4_00D5,
 PAD_MP4_00D6,
 PAD_MP4_00D7,
 PAD_MP4_00D8,
 PAD_MP4_00D9,
 PAD_MP4_00DA,
 PAD_MP4_00DB,
 PAD_MP4_00DC,
 PAD_MP4_00DD,
 PAD_MP4_00DE,
 PAD_MP4_00DF,
 PAD_MP4_00E0,
 PAD_MP4_00E1,
 PAD_MP4_00E2,
 PAD_MP4_00E3,
 PAD_MP4_00E4,
 PAD_MP4_00E5,
 PAD_MP4_00E6,
 PAD_MP4_00E7,
 PAD_MP4_00E8,
 PAD_MP4_00E9,
 PAD_MP4_00EA,
 PAD_MP4_00EB,
 PAD_MP4_00EC,
 PAD_MP4_00ED,
 PAD_MP4_00EE,
 PAD_MP4_00EF,
 PAD_MP4_00F0,
 PAD_MP4_00F1,
 PAD_MP4_00F2,
 PAD_MP4_00F3,
 PAD_MP4_00F4,
 PAD_MP4_00F5,
 PAD_MP4_00F6,
 PAD_MP4_00F7,
 PAD_MP4_00F8,
 PAD_MP4_00F9,
 PAD_MP4_00FA,
 PAD_MP4_00FB,
 PAD_MP4_00FC,
 PAD_MP4_00FD,
 PAD_MP4_00FE,
 PAD_MP4_00FF,
 PAD_MP4_0100,
 PAD_MP4_0101,
 PAD_MP4_0102,
 PAD_MP4_0103,
 PAD_MP4_0104,
 PAD_MP4_0105,
 PAD_MP4_0106,
 PAD_MP4_0107,
 PAD_MP4_0108,
 PAD_MP4_0109,
 PAD_MP4_010A,
 PAD_MP4_010B,
 PAD_MP4_010C,
 PAD_MP4_010D,
 PAD_MP4_010E,
 PAD_MP4_010F,
 PAD_MP4_0110,
 PAD_MP4_0111,
 PAD_MP4_0112,
 PAD_MP4_0113,
 PAD_MP4_0114,
 PAD_MP4_0115,
 PAD_MP4_0116,
 PAD_MP4_0117,
 PAD_MP4_0118,
 PAD_MP4_0119,
 PAD_MP4_011A,
 PAD_MP4_011B,
 PAD_MP4_011C,
 PAD_MP4_011D,
 PAD_MP4_011E,
 PAD_MP4_011F,
 PAD_MP4_0120,
 PAD_MP4_0121,
 PAD_MP4_0122,
 PAD_MP4_0123,
 PAD_MP4_0124,
 PAD_MP4_0125,
 PAD_MP4_0126,
 PAD_MP4_0127,
 PAD_MP4_0128,
 PAD_MP4_0129,
 PAD_MP4_012A,
 PAD_MP4_012B,
 PAD_MP4_012C,
 PAD_MP4_012D,
 PAD_MP4_012E,
 PAD_MP4_012F,
 PAD_MP4_0130,
 PAD_MP4_0131,
 PAD_MP4_0132,
 PAD_MP4_0133,
 PAD_MP4_0134,
 PAD_MP4_0135,
 PAD_MP4_0136,
 PAD_MP4_0137,
 PAD_MP4_0138,
 PAD_MP4_0139,
 PAD_MP4_013A,
 PAD_MP4_013B,
 PAD_MP4_013C,
 PAD_MP4_013D,
 PAD_MP4_013E,
 PAD_MP4_013F,
 PAD_MP4_0140,
 PAD_MP4_0141,
 PAD_MP4_0142,
 PAD_MP4_0143,
 PAD_MP4_0144,
 PAD_MP4_0145,
 PAD_MP4_0146,
 PAD_MP4_0147,
 PAD_MP4_0148,
 PAD_MP4_0149,
 PAD_MP4_014A,
 PAD_MP4_014B,
 PAD_MP4_014C,
 PAD_MP4_014D,
 PAD_MP4_014E,
 PAD_MP4_014F,
 PAD_MP4_0150,
 PAD_MP4_0151,
 PAD_MP4_0152,
 PAD_MP4_0153,
 PAD_MP4_0154,
 PAD_MP4_0155,
 PAD_MP4_0156,
 PAD_MP4_0157,
 PAD_MP4_0158,
 PAD_MP4_0159,
 PAD_MP4_015A,
 PAD_MP4_015B,
 PAD_MP4_015C,
 PAD_MP4_015D,
 PAD_MP4_015E,
 PAD_MP4_015F,
 PAD_MP4_0160,
 PAD_MP4_0161,
 PAD_MP4_0162,
 PAD_MP4_0163,
 PAD_MP4_0164,
 PAD_MP4_0165,
 PAD_MP4_0166,
 PAD_MP4_0167,
 PAD_MP4_0168,
 PAD_MP4_0169,
 PAD_MP4_016A,
 PAD_MP4_016B,
 PAD_MP4_016C,
 PAD_MP4_016D,
 PAD_MP4_016E,
 PAD_MP4_016F,
 PAD_MP4_0170,
 PAD_MP4_0171,
 PAD_MP4_0172,
 PAD_MP4_0173,
 PAD_MP4_0174,
 PAD_MP4_0175,
 PAD_MP4_0176,
 PAD_MP4_0177,
 PAD_MP4_0178,
 PAD_MP4_0179,
 PAD_MP4_017A,
 PAD_MP4_017B,
 PAD_MP4_017C,
 PAD_MP4_017D,
 PAD_MP4_017E,
 PAD_MP4_017F,
 PAD_MP4_0180,
 PAD_MP4_0181,
 PAD_MP4_0182,
 PAD_MP4_0183,
 PAD_MP4_0184,
 PAD_MP4_0185,
 PAD_MP4_0186,
 PAD_MP4_0187,
 PAD_MP4_0188,
 PAD_MP4_0189,
 PAD_MP4_018A,
 PAD_MP4_018B,
 PAD_MP4_018C,
 PAD_MP4_018D,
 PAD_MP4_018E,
 PAD_MP4_018F,
 PAD_MP4_END
};
enum pad_mp5 {
 PAD_MP5_0000,
 PAD_MP5_0001,
 PAD_MP5_0002,
 PAD_MP5_0003,
 PAD_MP5_0004,
 PAD_MP5_0005,
 PAD_MP5_0006,
 PAD_MP5_0007,
 PAD_MP5_0008,
 PAD_MP5_0009,
 PAD_MP5_000A,
 PAD_MP5_000B,
 PAD_MP5_000C,
 PAD_MP5_000D,
 PAD_MP5_000E,
 PAD_MP5_000F,
 PAD_MP5_0010,
 PAD_MP5_0011,
 PAD_MP5_0012,
 PAD_MP5_0013,
 PAD_MP5_0014,
 PAD_MP5_0015,
 PAD_MP5_0016,
 PAD_MP5_0017,
 PAD_MP5_0018,
 PAD_MP5_0019,
 PAD_MP5_001A,
 PAD_MP5_001B,
 PAD_MP5_001C,
 PAD_MP5_001D,
 PAD_MP5_001E,
 PAD_MP5_001F,
 PAD_MP5_0020,
 PAD_MP5_0021,
 PAD_MP5_0022,
 PAD_MP5_0023,
 PAD_MP5_0024,
 PAD_MP5_0025,
 PAD_MP5_0026,
 PAD_MP5_0027,
 PAD_MP5_0028,
 PAD_MP5_0029,
 PAD_MP5_002A,
 PAD_MP5_002B,
 PAD_MP5_002C,
 PAD_MP5_002D,
 PAD_MP5_002E,
 PAD_MP5_002F,
 PAD_MP5_0030,
 PAD_MP5_0031,
 PAD_MP5_0032,
 PAD_MP5_0033,
 PAD_MP5_0034,
 PAD_MP5_0035,
 PAD_MP5_0036,
 PAD_MP5_0037,
 PAD_MP5_0038,
 PAD_MP5_0039,
 PAD_MP5_003A,
 PAD_MP5_003B,
 PAD_MP5_003C,
 PAD_MP5_003D,
 PAD_MP5_003E,
 PAD_MP5_003F,
 PAD_MP5_0040,
 PAD_MP5_0041,
 PAD_MP5_0042,
 PAD_MP5_0043,
 PAD_MP5_0044,
 PAD_MP5_0045,
 PAD_MP5_0046,
 PAD_MP5_0047,
 PAD_MP5_0048,
 PAD_MP5_0049,
 PAD_MP5_004A,
 PAD_MP5_004B,
 PAD_MP5_004C,
 PAD_MP5_004D,
 PAD_MP5_004E,
 PAD_MP5_004F,
 PAD_MP5_0050,
 PAD_MP5_0051,
 PAD_MP5_0052,
 PAD_MP5_0053,
 PAD_MP5_0054,
 PAD_MP5_0055,
 PAD_MP5_0056,
 PAD_MP5_0057,
 PAD_MP5_0058,
 PAD_MP5_0059,
 PAD_MP5_005A,
 PAD_MP5_005B,
 PAD_MP5_005C,
 PAD_MP5_005D,
 PAD_MP5_005E,
 PAD_MP5_005F,
 PAD_MP5_0060,
 PAD_MP5_0061,
 PAD_MP5_0062,
 PAD_MP5_0063,
 PAD_MP5_0064,
 PAD_MP5_0065,
 PAD_MP5_0066,
 PAD_MP5_0067,
 PAD_MP5_0068,
 PAD_MP5_0069,
 PAD_MP5_006A,
 PAD_MP5_006B,
 PAD_MP5_006C,
 PAD_MP5_006D,
 PAD_MP5_006E,
 PAD_MP5_006F,
 PAD_MP5_0070,
 PAD_MP5_0071,
 PAD_MP5_0072,
 PAD_MP5_0073,
 PAD_MP5_0074,
 PAD_MP5_0075,
 PAD_MP5_0076,
 PAD_MP5_0077,
 PAD_MP5_0078,
 PAD_MP5_0079,
 PAD_MP5_007A,
 PAD_MP5_007B,
 PAD_MP5_007C,
 PAD_MP5_007D,
 PAD_MP5_007E,
 PAD_MP5_007F,
 PAD_MP5_0080,
 PAD_MP5_0081,
 PAD_MP5_0082,
 PAD_MP5_0083,
 PAD_MP5_0084,
 PAD_MP5_0085,
 PAD_MP5_0086,
 PAD_MP5_0087,
 PAD_MP5_0088,
 PAD_MP5_0089,
 PAD_MP5_008A,
 PAD_MP5_008B,
 PAD_MP5_008C,
 PAD_MP5_008D,
 PAD_MP5_008E,
 PAD_MP5_008F,
 PAD_MP5_0090,
 PAD_MP5_0091,
 PAD_MP5_0092,
 PAD_MP5_0093,
 PAD_MP5_0094,
 PAD_MP5_0095,
 PAD_MP5_0096,
 PAD_MP5_0097,
 PAD_MP5_0098,
 PAD_MP5_0099,
 PAD_MP5_009A,
 PAD_MP5_009B,
 PAD_MP5_009C,
 PAD_MP5_009D,
 PAD_MP5_009E,
 PAD_MP5_009F,
 PAD_MP5_00A0,
 PAD_MP5_00A1,
 PAD_MP5_00A2,
 PAD_MP5_00A3,
 PAD_MP5_00A4,
 PAD_MP5_00A5,
 PAD_MP5_00A6,
 PAD_MP5_00A7,
 PAD_MP5_00A8,
 PAD_MP5_00A9,
 PAD_MP5_00AA,
 PAD_MP5_00AB,
 PAD_MP5_00AC,
 PAD_MP5_00AD,
 PAD_MP5_00AE,
 PAD_MP5_00AF,
 PAD_MP5_00B0,
 PAD_MP5_00B1,
 PAD_MP5_00B2,
 PAD_MP5_00B3,
 PAD_MP5_00B4,
 PAD_MP5_00B5,
 PAD_MP5_00B6,
 PAD_MP5_00B7,
 PAD_MP5_00B8,
 PAD_MP5_00B9,
 PAD_MP5_00BA,
 PAD_MP5_00BB,
 PAD_MP5_00BC,
 PAD_MP5_00BD,
 PAD_MP5_00BE,
 PAD_MP5_00BF,
 PAD_MP5_00C0,
 PAD_MP5_00C1,
 PAD_MP5_00C2,
 PAD_MP5_00C3,
 PAD_MP5_00C4,
 PAD_MP5_00C5,
 PAD_MP5_00C6,
 PAD_MP5_00C7,
 PAD_MP5_00C8,
 PAD_MP5_00C9,
 PAD_MP5_00CA,
 PAD_MP5_00CB,
 PAD_MP5_00CC,
 PAD_MP5_00CD,
 PAD_MP5_00CE,
 PAD_MP5_00CF,
 PAD_MP5_00D0,
 PAD_MP5_00D1,
 PAD_MP5_00D2,
 PAD_MP5_00D3,
 PAD_MP5_00D4,
 PAD_MP5_00D5,
 PAD_MP5_00D6,
 PAD_MP5_00D7,
 PAD_MP5_00D8,
 PAD_MP5_00D9,
 PAD_MP5_00DA,
 PAD_MP5_00DB,
 PAD_MP5_00DC,
 PAD_MP5_00DD,
 PAD_MP5_00DE,
 PAD_MP5_00DF,
 PAD_MP5_00E0,
 PAD_MP5_00E1,
 PAD_MP5_00E2,
 PAD_MP5_00E3,
 PAD_MP5_00E4,
 PAD_MP5_00E5,
 PAD_MP5_00E6,
 PAD_MP5_00E7,
 PAD_MP5_00E8,
 PAD_MP5_00E9,
 PAD_MP5_00EA,
 PAD_MP5_00EB,
 PAD_MP5_00EC,
 PAD_MP5_00ED,
 PAD_MP5_00EE,
 PAD_MP5_00EF,
 PAD_MP5_00F0,
 PAD_MP5_00F1,
 PAD_MP5_00F2,
 PAD_MP5_00F3,
 PAD_MP5_00F4,
 PAD_MP5_00F5,
 PAD_MP5_00F6,
 PAD_MP5_00F7,
 PAD_MP5_00F8,
 PAD_MP5_00F9,
 PAD_MP5_00FA,
 PAD_MP5_00FB,
 PAD_MP5_00FC,
 PAD_MP5_00FD,
 PAD_MP5_00FE,
 PAD_MP5_00FF,
 PAD_MP5_0100,
 PAD_MP5_0101,
 PAD_MP5_0102,
 PAD_MP5_0103,
 PAD_MP5_0104,
 PAD_MP5_0105,
 PAD_MP5_0106,
 PAD_MP5_0107,
 PAD_MP5_0108,
 PAD_MP5_0109,
 PAD_MP5_010A,
 PAD_MP5_010B,
 PAD_MP5_010C,
 PAD_MP5_010D,
 PAD_MP5_010E,
 PAD_MP5_010F,
 PAD_MP5_0110,
 PAD_MP5_0111,
 PAD_MP5_0112,
 PAD_MP5_0113,
 PAD_MP5_0114,
 PAD_MP5_0115,
 PAD_MP5_0116,
 PAD_MP5_0117,
 PAD_MP5_0118,
 PAD_MP5_0119,
 PAD_MP5_011A,
 PAD_MP5_011B,
 PAD_MP5_011C,
 PAD_MP5_011D,
 PAD_MP5_011E,
 PAD_MP5_011F,
 PAD_MP5_0120,
 PAD_MP5_0121,
 PAD_MP5_0122,
 PAD_MP5_0123,
 PAD_MP5_0124,
 PAD_MP5_0125,
 PAD_MP5_0126,
 PAD_MP5_0127,
 PAD_MP5_0128,
 PAD_MP5_0129,
 PAD_MP5_012A,
 PAD_MP5_012B,
 PAD_MP5_012C,
 PAD_MP5_012D,
 PAD_MP5_012E,
 PAD_MP5_012F,
 PAD_MP5_0130,
 PAD_MP5_0131,
 PAD_MP5_0132,
 PAD_MP5_0133,
 PAD_MP5_END
};
enum pad_mp6 {
 PAD_MP6_END
};
enum pad_mp7 {
 PAD_MP7_END
};
enum pad_mp8 {
 PAD_MP8_END
};
enum pad_mp9 {
 PAD_MP9_0000,
 PAD_MP9_0001,
 PAD_MP9_0002,
 PAD_MP9_0003,
 PAD_MP9_0004,
 PAD_MP9_0005,
 PAD_MP9_0006,
 PAD_MP9_0007,
 PAD_MP9_0008,
 PAD_MP9_0009,
 PAD_MP9_000A,
 PAD_MP9_000B,
 PAD_MP9_000C,
 PAD_MP9_000D,
 PAD_MP9_000E,
 PAD_MP9_000F,
 PAD_MP9_0010,
 PAD_MP9_0011,
 PAD_MP9_0012,
 PAD_MP9_0013,
 PAD_MP9_0014,
 PAD_MP9_0015,
 PAD_MP9_0016,
 PAD_MP9_0017,
 PAD_MP9_0018,
 PAD_MP9_0019,
 PAD_MP9_001A,
 PAD_MP9_001B,
 PAD_MP9_001C,
 PAD_MP9_001D,
 PAD_MP9_001E,
 PAD_MP9_001F,
 PAD_MP9_0020,
 PAD_MP9_0021,
 PAD_MP9_0022,
 PAD_MP9_0023,
 PAD_MP9_0024,
 PAD_MP9_0025,
 PAD_MP9_0026,
 PAD_MP9_0027,
 PAD_MP9_0028,
 PAD_MP9_0029,
 PAD_MP9_002A,
 PAD_MP9_002B,
 PAD_MP9_002C,
 PAD_MP9_002D,
 PAD_MP9_002E,
 PAD_MP9_002F,
 PAD_MP9_0030,
 PAD_MP9_0031,
 PAD_MP9_0032,
 PAD_MP9_0033,
 PAD_MP9_0034,
 PAD_MP9_0035,
 PAD_MP9_0036,
 PAD_MP9_0037,
 PAD_MP9_0038,
 PAD_MP9_0039,
 PAD_MP9_003A,
 PAD_MP9_003B,
 PAD_MP9_003C,
 PAD_MP9_003D,
 PAD_MP9_003E,
 PAD_MP9_003F,
 PAD_MP9_0040,
 PAD_MP9_0041,
 PAD_MP9_0042,
 PAD_MP9_0043,
 PAD_MP9_0044,
 PAD_MP9_0045,
 PAD_MP9_0046,
 PAD_MP9_0047,
 PAD_MP9_0048,
 PAD_MP9_0049,
 PAD_MP9_004A,
 PAD_MP9_004B,
 PAD_MP9_004C,
 PAD_MP9_004D,
 PAD_MP9_004E,
 PAD_MP9_004F,
 PAD_MP9_0050,
 PAD_MP9_0051,
 PAD_MP9_0052,
 PAD_MP9_0053,
 PAD_MP9_0054,
 PAD_MP9_0055,
 PAD_MP9_0056,
 PAD_MP9_0057,
 PAD_MP9_0058,
 PAD_MP9_0059,
 PAD_MP9_005A,
 PAD_MP9_005B,
 PAD_MP9_005C,
 PAD_MP9_005D,
 PAD_MP9_005E,
 PAD_MP9_005F,
 PAD_MP9_0060,
 PAD_MP9_0061,
 PAD_MP9_0062,
 PAD_MP9_0063,
 PAD_MP9_0064,
 PAD_MP9_0065,
 PAD_MP9_0066,
 PAD_MP9_0067,
 PAD_MP9_0068,
 PAD_MP9_0069,
 PAD_MP9_006A,
 PAD_MP9_006B,
 PAD_MP9_006C,
 PAD_MP9_006D,
 PAD_MP9_006E,
 PAD_MP9_006F,
 PAD_MP9_0070,
 PAD_MP9_0071,
 PAD_MP9_0072,
 PAD_MP9_0073,
 PAD_MP9_0074,
 PAD_MP9_0075,
 PAD_MP9_0076,
 PAD_MP9_0077,
 PAD_MP9_0078,
 PAD_MP9_0079,
 PAD_MP9_007A,
 PAD_MP9_007B,
 PAD_MP9_007C,
 PAD_MP9_007D,
 PAD_MP9_007E,
 PAD_MP9_007F,
 PAD_MP9_0080,
 PAD_MP9_0081,
 PAD_MP9_0082,
 PAD_MP9_0083,
 PAD_MP9_0084,
 PAD_MP9_0085,
 PAD_MP9_0086,
 PAD_MP9_0087,
 PAD_MP9_0088,
 PAD_MP9_0089,
 PAD_MP9_008A,
 PAD_MP9_008B,
 PAD_MP9_008C,
 PAD_MP9_008D,
 PAD_MP9_008E,
 PAD_MP9_008F,
 PAD_MP9_0090,
 PAD_MP9_0091,
 PAD_MP9_0092,
 PAD_MP9_0093,
 PAD_MP9_0094,
 PAD_MP9_0095,
 PAD_MP9_0096,
 PAD_MP9_0097,
 PAD_MP9_0098,
 PAD_MP9_0099,
 PAD_MP9_009A,
 PAD_MP9_009B,
 PAD_MP9_009C,
 PAD_MP9_009D,
 PAD_MP9_009E,
 PAD_MP9_009F,
 PAD_MP9_00A0,
 PAD_MP9_00A1,
 PAD_MP9_00A2,
 PAD_MP9_00A3,
 PAD_MP9_00A4,
 PAD_MP9_00A5,
 PAD_MP9_00A6,
 PAD_MP9_00A7,
 PAD_MP9_00A8,
 PAD_MP9_00A9,
 PAD_MP9_00AA,
 PAD_MP9_00AB,
 PAD_MP9_00AC,
 PAD_MP9_00AD,
 PAD_MP9_00AE,
 PAD_MP9_00AF,
 PAD_MP9_00B0,
 PAD_MP9_00B1,
 PAD_MP9_00B2,
 PAD_MP9_00B3,
 PAD_MP9_00B4,
 PAD_MP9_00B5,
 PAD_MP9_00B6,
 PAD_MP9_00B7,
 PAD_MP9_00B8,
 PAD_MP9_00B9,
 PAD_MP9_END
};
enum pad_mp10 {
 PAD_MP10_0000,
 PAD_MP10_0001,
 PAD_MP10_0002,
 PAD_MP10_0003,
 PAD_MP10_0004,
 PAD_MP10_0005,
 PAD_MP10_0006,
 PAD_MP10_0007,
 PAD_MP10_0008,
 PAD_MP10_0009,
 PAD_MP10_000A,
 PAD_MP10_000B,
 PAD_MP10_000C,
 PAD_MP10_000D,
 PAD_MP10_000E,
 PAD_MP10_000F,
 PAD_MP10_0010,
 PAD_MP10_0011,
 PAD_MP10_0012,
 PAD_MP10_0013,
 PAD_MP10_0014,
 PAD_MP10_0015,
 PAD_MP10_0016,
 PAD_MP10_0017,
 PAD_MP10_0018,
 PAD_MP10_0019,
 PAD_MP10_001A,
 PAD_MP10_001B,
 PAD_MP10_001C,
 PAD_MP10_001D,
 PAD_MP10_001E,
 PAD_MP10_001F,
 PAD_MP10_0020,
 PAD_MP10_0021,
 PAD_MP10_0022,
 PAD_MP10_0023,
 PAD_MP10_0024,
 PAD_MP10_0025,
 PAD_MP10_0026,
 PAD_MP10_0027,
 PAD_MP10_0028,
 PAD_MP10_0029,
 PAD_MP10_002A,
 PAD_MP10_002B,
 PAD_MP10_002C,
 PAD_MP10_002D,
 PAD_MP10_002E,
 PAD_MP10_002F,
 PAD_MP10_0030,
 PAD_MP10_0031,
 PAD_MP10_0032,
 PAD_MP10_0033,
 PAD_MP10_0034,
 PAD_MP10_0035,
 PAD_MP10_0036,
 PAD_MP10_0037,
 PAD_MP10_0038,
 PAD_MP10_0039,
 PAD_MP10_003A,
 PAD_MP10_003B,
 PAD_MP10_003C,
 PAD_MP10_003D,
 PAD_MP10_003E,
 PAD_MP10_003F,
 PAD_MP10_0040,
 PAD_MP10_0041,
 PAD_MP10_0042,
 PAD_MP10_0043,
 PAD_MP10_0044,
 PAD_MP10_0045,
 PAD_MP10_0046,
 PAD_MP10_0047,
 PAD_MP10_0048,
 PAD_MP10_0049,
 PAD_MP10_004A,
 PAD_MP10_004B,
 PAD_MP10_004C,
 PAD_MP10_004D,
 PAD_MP10_004E,
 PAD_MP10_004F,
 PAD_MP10_0050,
 PAD_MP10_0051,
 PAD_MP10_0052,
 PAD_MP10_0053,
 PAD_MP10_0054,
 PAD_MP10_0055,
 PAD_MP10_0056,
 PAD_MP10_0057,
 PAD_MP10_0058,
 PAD_MP10_0059,
 PAD_MP10_005A,
 PAD_MP10_005B,
 PAD_MP10_005C,
 PAD_MP10_005D,
 PAD_MP10_005E,
 PAD_MP10_005F,
 PAD_MP10_0060,
 PAD_MP10_0061,
 PAD_MP10_0062,
 PAD_MP10_0063,
 PAD_MP10_0064,
 PAD_MP10_0065,
 PAD_MP10_0066,
 PAD_MP10_0067,
 PAD_MP10_0068,
 PAD_MP10_0069,
 PAD_MP10_006A,
 PAD_MP10_006B,
 PAD_MP10_006C,
 PAD_MP10_006D,
 PAD_MP10_006E,
 PAD_MP10_006F,
 PAD_MP10_0070,
 PAD_MP10_0071,
 PAD_MP10_0072,
 PAD_MP10_0073,
 PAD_MP10_0074,
 PAD_MP10_0075,
 PAD_MP10_0076,
 PAD_MP10_0077,
 PAD_MP10_0078,
 PAD_MP10_0079,
 PAD_MP10_007A,
 PAD_MP10_007B,
 PAD_MP10_007C,
 PAD_MP10_007D,
 PAD_MP10_007E,
 PAD_MP10_007F,
 PAD_MP10_0080,
 PAD_MP10_0081,
 PAD_MP10_0082,
 PAD_MP10_0083,
 PAD_MP10_0084,
 PAD_MP10_0085,
 PAD_MP10_0086,
 PAD_MP10_0087,
 PAD_MP10_0088,
 PAD_MP10_0089,
 PAD_MP10_008A,
 PAD_MP10_008B,
 PAD_MP10_008C,
 PAD_MP10_008D,
 PAD_MP10_008E,
 PAD_MP10_008F,
 PAD_MP10_0090,
 PAD_MP10_0091,
 PAD_MP10_0092,
 PAD_MP10_0093,
 PAD_MP10_0094,
 PAD_MP10_0095,
 PAD_MP10_0096,
 PAD_MP10_0097,
 PAD_MP10_0098,
 PAD_MP10_0099,
 PAD_MP10_009A,
 PAD_MP10_009B,
 PAD_MP10_009C,
 PAD_MP10_009D,
 PAD_MP10_009E,
 PAD_MP10_009F,
 PAD_MP10_00A0,
 PAD_MP10_00A1,
 PAD_MP10_00A2,
 PAD_MP10_00A3,
 PAD_MP10_00A4,
 PAD_MP10_00A5,
 PAD_MP10_00A6,
 PAD_MP10_00A7,
 PAD_MP10_00A8,
 PAD_MP10_00A9,
 PAD_MP10_00AA,
 PAD_MP10_00AB,
 PAD_MP10_00AC,
 PAD_MP10_00AD,
 PAD_MP10_00AE,
 PAD_MP10_00AF,
 PAD_MP10_00B0,
 PAD_MP10_00B1,
 PAD_MP10_00B2,
 PAD_MP10_00B3,
 PAD_MP10_00B4,
 PAD_MP10_00B5,
 PAD_MP10_00B6,
 PAD_MP10_00B7,
 PAD_MP10_00B8,
 PAD_MP10_00B9,
 PAD_MP10_END
};
enum pad_mp11 {
 PAD_MP11_0000,
 PAD_MP11_0001,
 PAD_MP11_0002,
 PAD_MP11_0003,
 PAD_MP11_0004,
 PAD_MP11_0005,
 PAD_MP11_0006,
 PAD_MP11_0007,
 PAD_MP11_0008,
 PAD_MP11_0009,
 PAD_MP11_000A,
 PAD_MP11_000B,
 PAD_MP11_000C,
 PAD_MP11_000D,
 PAD_MP11_000E,
 PAD_MP11_000F,
 PAD_MP11_0010,
 PAD_MP11_0011,
 PAD_MP11_0012,
 PAD_MP11_0013,
 PAD_MP11_0014,
 PAD_MP11_0015,
 PAD_MP11_0016,
 PAD_MP11_0017,
 PAD_MP11_0018,
 PAD_MP11_0019,
 PAD_MP11_001A,
 PAD_MP11_001B,
 PAD_MP11_001C,
 PAD_MP11_001D,
 PAD_MP11_001E,
 PAD_MP11_001F,
 PAD_MP11_0020,
 PAD_MP11_0021,
 PAD_MP11_0022,
 PAD_MP11_0023,
 PAD_MP11_0024,
 PAD_MP11_0025,
 PAD_MP11_0026,
 PAD_MP11_0027,
 PAD_MP11_0028,
 PAD_MP11_0029,
 PAD_MP11_002A,
 PAD_MP11_002B,
 PAD_MP11_002C,
 PAD_MP11_002D,
 PAD_MP11_002E,
 PAD_MP11_002F,
 PAD_MP11_0030,
 PAD_MP11_0031,
 PAD_MP11_0032,
 PAD_MP11_0033,
 PAD_MP11_0034,
 PAD_MP11_0035,
 PAD_MP11_0036,
 PAD_MP11_0037,
 PAD_MP11_0038,
 PAD_MP11_0039,
 PAD_MP11_003A,
 PAD_MP11_003B,
 PAD_MP11_003C,
 PAD_MP11_003D,
 PAD_MP11_003E,
 PAD_MP11_003F,
 PAD_MP11_0040,
 PAD_MP11_0041,
 PAD_MP11_0042,
 PAD_MP11_0043,
 PAD_MP11_0044,
 PAD_MP11_0045,
 PAD_MP11_0046,
 PAD_MP11_0047,
 PAD_MP11_0048,
 PAD_MP11_0049,
 PAD_MP11_004A,
 PAD_MP11_004B,
 PAD_MP11_004C,
 PAD_MP11_004D,
 PAD_MP11_004E,
 PAD_MP11_004F,
 PAD_MP11_0050,
 PAD_MP11_0051,
 PAD_MP11_0052,
 PAD_MP11_0053,
 PAD_MP11_0054,
 PAD_MP11_0055,
 PAD_MP11_0056,
 PAD_MP11_0057,
 PAD_MP11_0058,
 PAD_MP11_0059,
 PAD_MP11_005A,
 PAD_MP11_005B,
 PAD_MP11_005C,
 PAD_MP11_005D,
 PAD_MP11_005E,
 PAD_MP11_005F,
 PAD_MP11_0060,
 PAD_MP11_0061,
 PAD_MP11_0062,
 PAD_MP11_0063,
 PAD_MP11_0064,
 PAD_MP11_0065,
 PAD_MP11_0066,
 PAD_MP11_0067,
 PAD_MP11_0068,
 PAD_MP11_0069,
 PAD_MP11_006A,
 PAD_MP11_006B,
 PAD_MP11_006C,
 PAD_MP11_006D,
 PAD_MP11_006E,
 PAD_MP11_006F,
 PAD_MP11_0070,
 PAD_MP11_0071,
 PAD_MP11_0072,
 PAD_MP11_0073,
 PAD_MP11_0074,
 PAD_MP11_0075,
 PAD_MP11_0076,
 PAD_MP11_0077,
 PAD_MP11_0078,
 PAD_MP11_0079,
 PAD_MP11_007A,
 PAD_MP11_007B,
 PAD_MP11_007C,
 PAD_MP11_007D,
 PAD_MP11_007E,
 PAD_MP11_007F,
 PAD_MP11_0080,
 PAD_MP11_0081,
 PAD_MP11_0082,
 PAD_MP11_0083,
 PAD_MP11_0084,
 PAD_MP11_0085,
 PAD_MP11_0086,
 PAD_MP11_0087,
 PAD_MP11_0088,
 PAD_MP11_0089,
 PAD_MP11_008A,
 PAD_MP11_008B,
 PAD_MP11_008C,
 PAD_MP11_008D,
 PAD_MP11_008E,
 PAD_MP11_008F,
 PAD_MP11_0090,
 PAD_MP11_0091,
 PAD_MP11_0092,
 PAD_MP11_0093,
 PAD_MP11_0094,
 PAD_MP11_0095,
 PAD_MP11_0096,
 PAD_MP11_0097,
 PAD_MP11_0098,
 PAD_MP11_0099,
 PAD_MP11_009A,
 PAD_MP11_009B,
 PAD_MP11_009C,
 PAD_MP11_009D,
 PAD_MP11_009E,
 PAD_MP11_009F,
 PAD_MP11_00A0,
 PAD_MP11_00A1,
 PAD_MP11_00A2,
 PAD_MP11_00A3,
 PAD_MP11_00A4,
 PAD_MP11_00A5,
 PAD_MP11_00A6,
 PAD_MP11_00A7,
 PAD_MP11_00A8,
 PAD_MP11_00A9,
 PAD_MP11_00AA,
 PAD_MP11_00AB,
 PAD_MP11_00AC,
 PAD_MP11_00AD,
 PAD_MP11_00AE,
 PAD_MP11_00AF,
 PAD_MP11_00B0,
 PAD_MP11_00B1,
 PAD_MP11_00B2,
 PAD_MP11_00B3,
 PAD_MP11_00B4,
 PAD_MP11_00B5,
 PAD_MP11_00B6,
 PAD_MP11_00B7,
 PAD_MP11_00B8,
 PAD_MP11_00B9,
 PAD_MP11_00BA,
 PAD_MP11_00BB,
 PAD_MP11_00BC,
 PAD_MP11_00BD,
 PAD_MP11_00BE,
 PAD_MP11_00BF,
 PAD_MP11_00C0,
 PAD_MP11_00C1,
 PAD_MP11_00C2,
 PAD_MP11_00C3,
 PAD_MP11_00C4,
 PAD_MP11_00C5,
 PAD_MP11_00C6,
 PAD_MP11_00C7,
 PAD_MP11_00C8,
 PAD_MP11_00C9,
 PAD_MP11_00CA,
 PAD_MP11_00CB,
 PAD_MP11_00CC,
 PAD_MP11_00CD,
 PAD_MP11_00CE,
 PAD_MP11_00CF,
 PAD_MP11_00D0,
 PAD_MP11_00D1,
 PAD_MP11_00D2,
 PAD_MP11_00D3,
 PAD_MP11_00D4,
 PAD_MP11_00D5,
 PAD_MP11_00D6,
 PAD_MP11_00D7,
 PAD_MP11_00D8,
 PAD_MP11_00D9,
 PAD_MP11_00DA,
 PAD_MP11_00DB,
 PAD_MP11_00DC,
 PAD_MP11_00DD,
 PAD_MP11_00DE,
 PAD_MP11_00DF,
 PAD_MP11_00E0,
 PAD_MP11_00E1,
 PAD_MP11_00E2,
 PAD_MP11_00E3,
 PAD_MP11_00E4,
 PAD_MP11_00E5,
 PAD_MP11_00E6,
 PAD_MP11_00E7,
 PAD_MP11_00E8,
 PAD_MP11_00E9,
 PAD_MP11_00EA,
 PAD_MP11_00EB,
 PAD_MP11_00EC,
 PAD_MP11_00ED,
 PAD_MP11_00EE,
 PAD_MP11_00EF,
 PAD_MP11_00F0,
 PAD_MP11_00F1,
 PAD_MP11_00F2,
 PAD_MP11_END
};
enum pad_mp12 {
 PAD_MP12_0000,
 PAD_MP12_0001,
 PAD_MP12_0002,
 PAD_MP12_0003,
 PAD_MP12_0004,
 PAD_MP12_0005,
 PAD_MP12_0006,
 PAD_MP12_0007,
 PAD_MP12_0008,
 PAD_MP12_0009,
 PAD_MP12_000A,
 PAD_MP12_000B,
 PAD_MP12_000C,
 PAD_MP12_000D,
 PAD_MP12_000E,
 PAD_MP12_000F,
 PAD_MP12_0010,
 PAD_MP12_0011,
 PAD_MP12_0012,
 PAD_MP12_0013,
 PAD_MP12_0014,
 PAD_MP12_0015,
 PAD_MP12_0016,
 PAD_MP12_0017,
 PAD_MP12_0018,
 PAD_MP12_0019,
 PAD_MP12_001A,
 PAD_MP12_001B,
 PAD_MP12_001C,
 PAD_MP12_001D,
 PAD_MP12_001E,
 PAD_MP12_001F,
 PAD_MP12_0020,
 PAD_MP12_0021,
 PAD_MP12_0022,
 PAD_MP12_0023,
 PAD_MP12_0024,
 PAD_MP12_0025,
 PAD_MP12_0026,
 PAD_MP12_0027,
 PAD_MP12_0028,
 PAD_MP12_0029,
 PAD_MP12_002A,
 PAD_MP12_002B,
 PAD_MP12_002C,
 PAD_MP12_002D,
 PAD_MP12_002E,
 PAD_MP12_002F,
 PAD_MP12_0030,
 PAD_MP12_0031,
 PAD_MP12_0032,
 PAD_MP12_0033,
 PAD_MP12_0034,
 PAD_MP12_0035,
 PAD_MP12_0036,
 PAD_MP12_0037,
 PAD_MP12_0038,
 PAD_MP12_0039,
 PAD_MP12_003A,
 PAD_MP12_003B,
 PAD_MP12_003C,
 PAD_MP12_003D,
 PAD_MP12_003E,
 PAD_MP12_003F,
 PAD_MP12_0040,
 PAD_MP12_0041,
 PAD_MP12_0042,
 PAD_MP12_0043,
 PAD_MP12_0044,
 PAD_MP12_0045,
 PAD_MP12_0046,
 PAD_MP12_0047,
 PAD_MP12_0048,
 PAD_MP12_0049,
 PAD_MP12_004A,
 PAD_MP12_004B,
 PAD_MP12_004C,
 PAD_MP12_004D,
 PAD_MP12_004E,
 PAD_MP12_004F,
 PAD_MP12_0050,
 PAD_MP12_0051,
 PAD_MP12_0052,
 PAD_MP12_0053,
 PAD_MP12_0054,
 PAD_MP12_0055,
 PAD_MP12_0056,
 PAD_MP12_0057,
 PAD_MP12_0058,
 PAD_MP12_0059,
 PAD_MP12_005A,
 PAD_MP12_005B,
 PAD_MP12_005C,
 PAD_MP12_005D,
 PAD_MP12_005E,
 PAD_MP12_005F,
 PAD_MP12_0060,
 PAD_MP12_0061,
 PAD_MP12_0062,
 PAD_MP12_0063,
 PAD_MP12_0064,
 PAD_MP12_0065,
 PAD_MP12_0066,
 PAD_MP12_0067,
 PAD_MP12_0068,
 PAD_MP12_0069,
 PAD_MP12_006A,
 PAD_MP12_006B,
 PAD_MP12_006C,
 PAD_MP12_006D,
 PAD_MP12_006E,
 PAD_MP12_006F,
 PAD_MP12_0070,
 PAD_MP12_0071,
 PAD_MP12_0072,
 PAD_MP12_0073,
 PAD_MP12_0074,
 PAD_MP12_0075,
 PAD_MP12_0076,
 PAD_MP12_0077,
 PAD_MP12_0078,
 PAD_MP12_0079,
 PAD_MP12_007A,
 PAD_MP12_007B,
 PAD_MP12_007C,
 PAD_MP12_007D,
 PAD_MP12_007E,
 PAD_MP12_007F,
 PAD_MP12_0080,
 PAD_MP12_0081,
 PAD_MP12_0082,
 PAD_MP12_0083,
 PAD_MP12_0084,
 PAD_MP12_0085,
 PAD_MP12_0086,
 PAD_MP12_0087,
 PAD_MP12_0088,
 PAD_MP12_0089,
 PAD_MP12_008A,
 PAD_MP12_008B,
 PAD_MP12_008C,
 PAD_MP12_008D,
 PAD_MP12_008E,
 PAD_MP12_008F,
 PAD_MP12_0090,
 PAD_MP12_0091,
 PAD_MP12_0092,
 PAD_MP12_0093,
 PAD_MP12_0094,
 PAD_MP12_0095,
 PAD_MP12_0096,
 PAD_MP12_0097,
 PAD_MP12_0098,
 PAD_MP12_0099,
 PAD_MP12_009A,
 PAD_MP12_009B,
 PAD_MP12_009C,
 PAD_MP12_009D,
 PAD_MP12_009E,
 PAD_MP12_009F,
 PAD_MP12_00A0,
 PAD_MP12_00A1,
 PAD_MP12_00A2,
 PAD_MP12_00A3,
 PAD_MP12_00A4,
 PAD_MP12_00A5,
 PAD_MP12_00A6,
 PAD_MP12_00A7,
 PAD_MP12_00A8,
 PAD_MP12_00A9,
 PAD_MP12_00AA,
 PAD_MP12_00AB,
 PAD_MP12_00AC,
 PAD_MP12_00AD,
 PAD_MP12_00AE,
 PAD_MP12_00AF,
 PAD_MP12_00B0,
 PAD_MP12_00B1,
 PAD_MP12_00B2,
 PAD_MP12_00B3,
 PAD_MP12_00B4,
 PAD_MP12_00B5,
 PAD_MP12_00B6,
 PAD_MP12_00B7,
 PAD_MP12_00B8,
 PAD_MP12_00B9,
 PAD_MP12_00BA,
 PAD_MP12_00BB,
 PAD_MP12_00BC,
 PAD_MP12_00BD,
 PAD_MP12_00BE,
 PAD_MP12_00BF,
 PAD_MP12_00C0,
 PAD_MP12_00C1,
 PAD_MP12_00C2,
 PAD_MP12_00C3,
 PAD_MP12_00C4,
 PAD_MP12_00C5,
 PAD_MP12_00C6,
 PAD_MP12_00C7,
 PAD_MP12_00C8,
 PAD_MP12_00C9,
 PAD_MP12_00CA,
 PAD_MP12_00CB,
 PAD_MP12_00CC,
 PAD_MP12_00CD,
 PAD_MP12_00CE,
 PAD_MP12_00CF,
 PAD_MP12_00D0,
 PAD_MP12_00D1,
 PAD_MP12_00D2,
 PAD_MP12_00D3,
 PAD_MP12_00D4,
 PAD_MP12_00D5,
 PAD_MP12_00D6,
 PAD_MP12_00D7,
 PAD_MP12_00D8,
 PAD_MP12_00D9,
 PAD_MP12_00DA,
 PAD_MP12_00DB,
 PAD_MP12_00DC,
 PAD_MP12_00DD,
 PAD_MP12_00DE,
 PAD_MP12_00DF,
 PAD_MP12_00E0,
 PAD_MP12_00E1,
 PAD_MP12_00E2,
 PAD_MP12_00E3,
 PAD_MP12_00E4,
 PAD_MP12_00E5,
 PAD_MP12_00E6,
 PAD_MP12_00E7,
 PAD_MP12_00E8,
 PAD_MP12_00E9,
 PAD_MP12_00EA,
 PAD_MP12_00EB,
 PAD_MP12_00EC,
 PAD_MP12_00ED,
 PAD_MP12_00EE,
 PAD_MP12_00EF,
 PAD_MP12_00F0,
 PAD_MP12_00F1,
 PAD_MP12_00F2,
 PAD_MP12_00F3,
 PAD_MP12_00F4,
 PAD_MP12_00F5,
 PAD_MP12_00F6,
 PAD_MP12_00F7,
 PAD_MP12_00F8,
 PAD_MP12_00F9,
 PAD_MP12_00FA,
 PAD_MP12_00FB,
 PAD_MP12_00FC,
 PAD_MP12_00FD,
 PAD_MP12_00FE,
 PAD_MP12_00FF,
 PAD_MP12_0100,
 PAD_MP12_0101,
 PAD_MP12_0102,
 PAD_MP12_0103,
 PAD_MP12_0104,
 PAD_MP12_0105,
 PAD_MP12_0106,
 PAD_MP12_0107,
 PAD_MP12_0108,
 PAD_MP12_0109,
 PAD_MP12_010A,
 PAD_MP12_010B,
 PAD_MP12_010C,
 PAD_MP12_010D,
 PAD_MP12_010E,
 PAD_MP12_010F,
 PAD_MP12_0110,
 PAD_MP12_0111,
 PAD_MP12_0112,
 PAD_MP12_0113,
 PAD_MP12_0114,
 PAD_MP12_0115,
 PAD_MP12_0116,
 PAD_MP12_0117,
 PAD_MP12_0118,
 PAD_MP12_0119,
 PAD_MP12_011A,
 PAD_MP12_011B,
 PAD_MP12_011C,
 PAD_MP12_011D,
 PAD_MP12_011E,
 PAD_MP12_011F,
 PAD_MP12_0120,
 PAD_MP12_0121,
 PAD_MP12_0122,
 PAD_MP12_0123,
 PAD_MP12_0124,
 PAD_MP12_0125,
 PAD_MP12_0126,
 PAD_MP12_0127,
 PAD_MP12_0128,
 PAD_MP12_0129,
 PAD_MP12_012A,
 PAD_MP12_012B,
 PAD_MP12_012C,
 PAD_MP12_012D,
 PAD_MP12_012E,
 PAD_MP12_012F,
 PAD_MP12_0130,
 PAD_MP12_0131,
 PAD_MP12_0132,
 PAD_MP12_0133,
 PAD_MP12_0134,
 PAD_MP12_0135,
 PAD_MP12_0136,
 PAD_MP12_0137,
 PAD_MP12_0138,
 PAD_MP12_0139,
 PAD_MP12_013A,
 PAD_MP12_013B,
 PAD_MP12_013C,
 PAD_MP12_013D,
 PAD_MP12_013E,
 PAD_MP12_013F,
 PAD_MP12_0140,
 PAD_MP12_0141,
 PAD_MP12_0142,
 PAD_MP12_0143,
 PAD_MP12_0144,
 PAD_MP12_0145,
 PAD_MP12_0146,
 PAD_MP12_0147,
 PAD_MP12_0148,
 PAD_MP12_0149,
 PAD_MP12_014A,
 PAD_MP12_014B,
 PAD_MP12_014C,
 PAD_MP12_014D,
 PAD_MP12_014E,
 PAD_MP12_014F,
 PAD_MP12_0150,
 PAD_MP12_0151,
 PAD_MP12_0152,
 PAD_MP12_0153,
 PAD_MP12_0154,
 PAD_MP12_0155,
 PAD_MP12_0156,
 PAD_MP12_0157,
 PAD_MP12_0158,
 PAD_MP12_0159,
 PAD_MP12_015A,
 PAD_MP12_015B,
 PAD_MP12_015C,
 PAD_MP12_015D,
 PAD_MP12_015E,
 PAD_MP12_015F,
 PAD_MP12_0160,
 PAD_MP12_0161,
 PAD_MP12_0162,
 PAD_MP12_0163,
 PAD_MP12_0164,
 PAD_MP12_0165,
 PAD_MP12_0166,
 PAD_MP12_0167,
 PAD_MP12_0168,
 PAD_MP12_0169,
 PAD_MP12_016A,
 PAD_MP12_016B,
 PAD_MP12_016C,
 PAD_MP12_016D,
 PAD_MP12_016E,
 PAD_MP12_016F,
 PAD_MP12_0170,
 PAD_MP12_0171,
 PAD_MP12_0172,
 PAD_MP12_0173,
 PAD_MP12_0174,
 PAD_MP12_0175,
 PAD_MP12_0176,
 PAD_MP12_0177,
 PAD_MP12_0178,
 PAD_MP12_0179,
 PAD_MP12_017A,
 PAD_MP12_017B,
 PAD_MP12_017C,
 PAD_MP12_017D,
 PAD_MP12_017E,
 PAD_MP12_017F,
 PAD_MP12_0180,
 PAD_MP12_0181,
 PAD_MP12_0182,
 PAD_MP12_0183,
 PAD_MP12_0184,
 PAD_MP12_0185,
 PAD_MP12_0186,
 PAD_MP12_0187,
 PAD_MP12_0188,
 PAD_MP12_0189,
 PAD_MP12_018A,
 PAD_MP12_018B,
 PAD_MP12_018C,
 PAD_MP12_018D,
 PAD_MP12_018E,
 PAD_MP12_018F,
 PAD_MP12_0190,
 PAD_MP12_0191,
 PAD_MP12_0192,
 PAD_MP12_0193,
 PAD_MP12_0194,
 PAD_MP12_0195,
 PAD_MP12_0196,
 PAD_MP12_0197,
 PAD_MP12_0198,
 PAD_MP12_0199,
 PAD_MP12_019A,
 PAD_MP12_019B,
 PAD_MP12_019C,
 PAD_MP12_019D,
 PAD_MP12_019E,
 PAD_MP12_019F,
 PAD_MP12_01A0,
 PAD_MP12_01A1,
 PAD_MP12_01A2,
 PAD_MP12_01A3,
 PAD_MP12_01A4,
 PAD_MP12_01A5,
 PAD_MP12_01A6,
 PAD_MP12_01A7,
 PAD_MP12_01A8,
 PAD_MP12_01A9,
 PAD_MP12_01AA,
 PAD_MP12_01AB,
 PAD_MP12_01AC,
 PAD_MP12_01AD,
 PAD_MP12_01AE,
 PAD_MP12_01AF,
 PAD_MP12_01B0,
 PAD_MP12_01B1,
 PAD_MP12_01B2,
 PAD_MP12_01B3,
 PAD_MP12_01B4,
 PAD_MP12_01B5,
 PAD_MP12_01B6,
 PAD_MP12_01B7,
 PAD_MP12_01B8,
 PAD_MP12_01B9,
 PAD_MP12_01BA,
 PAD_MP12_01BB,
 PAD_MP12_01BC,
 PAD_MP12_01BD,
 PAD_MP12_01BE,
 PAD_MP12_01BF,
 PAD_MP12_01C0,
 PAD_MP12_01C1,
 PAD_MP12_01C2,
 PAD_MP12_01C3,
 PAD_MP12_01C4,
 PAD_MP12_01C5,
 PAD_MP12_01C6,
 PAD_MP12_01C7,
 PAD_MP12_01C8,
 PAD_MP12_01C9,
 PAD_MP12_01CA,
 PAD_MP12_01CB,
 PAD_MP12_01CC,
 PAD_MP12_01CD,
 PAD_MP12_01CE,
 PAD_MP12_01CF,
 PAD_MP12_01D0,
 PAD_MP12_01D1,
 PAD_MP12_01D2,
 PAD_MP12_01D3,
 PAD_MP12_01D4,
 PAD_MP12_01D5,
 PAD_MP12_01D6,
 PAD_MP12_01D7,
 PAD_MP12_01D8,
 PAD_MP12_01D9,
 PAD_MP12_01DA,
 PAD_MP12_01DB,
 PAD_MP12_01DC,
 PAD_MP12_01DD,
 PAD_MP12_01DE,
 PAD_MP12_01DF,
 PAD_MP12_01E0,
 PAD_MP12_01E1,
 PAD_MP12_01E2,
 PAD_MP12_01E3,
 PAD_MP12_01E4,
 PAD_MP12_01E5,
 PAD_MP12_01E6,
 PAD_MP12_01E7,
 PAD_MP12_01E8,
 PAD_MP12_01E9,
 PAD_MP12_01EA,
 PAD_MP12_01EB,
 PAD_MP12_01EC,
 PAD_MP12_01ED,
 PAD_MP12_01EE,
 PAD_MP12_01EF,
 PAD_MP12_01F0,
 PAD_MP12_01F1,
 PAD_MP12_01F2,
 PAD_MP12_01F3,
 PAD_MP12_01F4,
 PAD_MP12_01F5,
 PAD_MP12_01F6,
 PAD_MP12_01F7,
 PAD_MP12_01F8,
 PAD_MP12_01F9,
 PAD_MP12_01FA,
 PAD_MP12_01FB,
 PAD_MP12_01FC,
 PAD_MP12_01FD,
 PAD_MP12_01FE,
 PAD_MP12_01FF,
 PAD_MP12_0200,
 PAD_MP12_0201,
 PAD_MP12_END
};
enum pad_mp13 {
 PAD_MP13_0000,
 PAD_MP13_0001,
 PAD_MP13_0002,
 PAD_MP13_0003,
 PAD_MP13_0004,
 PAD_MP13_0005,
 PAD_MP13_0006,
 PAD_MP13_0007,
 PAD_MP13_0008,
 PAD_MP13_0009,
 PAD_MP13_000A,
 PAD_MP13_000B,
 PAD_MP13_000C,
 PAD_MP13_000D,
 PAD_MP13_000E,
 PAD_MP13_000F,
 PAD_MP13_0010,
 PAD_MP13_0011,
 PAD_MP13_0012,
 PAD_MP13_0013,
 PAD_MP13_0014,
 PAD_MP13_0015,
 PAD_MP13_0016,
 PAD_MP13_0017,
 PAD_MP13_0018,
 PAD_MP13_0019,
 PAD_MP13_001A,
 PAD_MP13_001B,
 PAD_MP13_001C,
 PAD_MP13_001D,
 PAD_MP13_001E,
 PAD_MP13_001F,
 PAD_MP13_0020,
 PAD_MP13_0021,
 PAD_MP13_0022,
 PAD_MP13_0023,
 PAD_MP13_0024,
 PAD_MP13_0025,
 PAD_MP13_0026,
 PAD_MP13_0027,
 PAD_MP13_0028,
 PAD_MP13_0029,
 PAD_MP13_002A,
 PAD_MP13_002B,
 PAD_MP13_002C,
 PAD_MP13_002D,
 PAD_MP13_002E,
 PAD_MP13_002F,
 PAD_MP13_0030,
 PAD_MP13_0031,
 PAD_MP13_0032,
 PAD_MP13_0033,
 PAD_MP13_0034,
 PAD_MP13_0035,
 PAD_MP13_0036,
 PAD_MP13_0037,
 PAD_MP13_0038,
 PAD_MP13_0039,
 PAD_MP13_003A,
 PAD_MP13_003B,
 PAD_MP13_003C,
 PAD_MP13_003D,
 PAD_MP13_003E,
 PAD_MP13_003F,
 PAD_MP13_0040,
 PAD_MP13_0041,
 PAD_MP13_0042,
 PAD_MP13_0043,
 PAD_MP13_0044,
 PAD_MP13_0045,
 PAD_MP13_0046,
 PAD_MP13_0047,
 PAD_MP13_0048,
 PAD_MP13_0049,
 PAD_MP13_004A,
 PAD_MP13_004B,
 PAD_MP13_004C,
 PAD_MP13_004D,
 PAD_MP13_004E,
 PAD_MP13_004F,
 PAD_MP13_0050,
 PAD_MP13_0051,
 PAD_MP13_0052,
 PAD_MP13_0053,
 PAD_MP13_0054,
 PAD_MP13_0055,
 PAD_MP13_0056,
 PAD_MP13_0057,
 PAD_MP13_0058,
 PAD_MP13_0059,
 PAD_MP13_005A,
 PAD_MP13_005B,
 PAD_MP13_005C,
 PAD_MP13_005D,
 PAD_MP13_005E,
 PAD_MP13_005F,
 PAD_MP13_0060,
 PAD_MP13_0061,
 PAD_MP13_0062,
 PAD_MP13_0063,
 PAD_MP13_0064,
 PAD_MP13_0065,
 PAD_MP13_0066,
 PAD_MP13_0067,
 PAD_MP13_0068,
 PAD_MP13_0069,
 PAD_MP13_006A,
 PAD_MP13_006B,
 PAD_MP13_006C,
 PAD_MP13_006D,
 PAD_MP13_006E,
 PAD_MP13_006F,
 PAD_MP13_0070,
 PAD_MP13_0071,
 PAD_MP13_0072,
 PAD_MP13_0073,
 PAD_MP13_0074,
 PAD_MP13_0075,
 PAD_MP13_0076,
 PAD_MP13_0077,
 PAD_MP13_0078,
 PAD_MP13_0079,
 PAD_MP13_007A,
 PAD_MP13_007B,
 PAD_MP13_007C,
 PAD_MP13_007D,
 PAD_MP13_007E,
 PAD_MP13_007F,
 PAD_MP13_0080,
 PAD_MP13_0081,
 PAD_MP13_0082,
 PAD_MP13_0083,
 PAD_MP13_0084,
 PAD_MP13_0085,
 PAD_MP13_0086,
 PAD_MP13_0087,
 PAD_MP13_0088,
 PAD_MP13_0089,
 PAD_MP13_008A,
 PAD_MP13_008B,
 PAD_MP13_008C,
 PAD_MP13_008D,
 PAD_MP13_008E,
 PAD_MP13_008F,
 PAD_MP13_0090,
 PAD_MP13_0091,
 PAD_MP13_0092,
 PAD_MP13_0093,
 PAD_MP13_0094,
 PAD_MP13_0095,
 PAD_MP13_0096,
 PAD_MP13_0097,
 PAD_MP13_0098,
 PAD_MP13_0099,
 PAD_MP13_009A,
 PAD_MP13_009B,
 PAD_MP13_009C,
 PAD_MP13_009D,
 PAD_MP13_009E,
 PAD_MP13_009F,
 PAD_MP13_00A0,
 PAD_MP13_00A1,
 PAD_MP13_00A2,
 PAD_MP13_00A3,
 PAD_MP13_00A4,
 PAD_MP13_00A5,
 PAD_MP13_00A6,
 PAD_MP13_00A7,
 PAD_MP13_00A8,
 PAD_MP13_00A9,
 PAD_MP13_00AA,
 PAD_MP13_00AB,
 PAD_MP13_00AC,
 PAD_MP13_00AD,
 PAD_MP13_00AE,
 PAD_MP13_00AF,
 PAD_MP13_00B0,
 PAD_MP13_00B1,
 PAD_MP13_00B2,
 PAD_MP13_00B3,
 PAD_MP13_00B4,
 PAD_MP13_00B5,
 PAD_MP13_00B6,
 PAD_MP13_00B7,
 PAD_MP13_00B8,
 PAD_MP13_00B9,
 PAD_MP13_00BA,
 PAD_MP13_00BB,
 PAD_MP13_00BC,
 PAD_MP13_00BD,
 PAD_MP13_00BE,
 PAD_MP13_00BF,
 PAD_MP13_00C0,
 PAD_MP13_00C1,
 PAD_MP13_00C2,
 PAD_MP13_00C3,
 PAD_MP13_00C4,
 PAD_MP13_00C5,
 PAD_MP13_00C6,
 PAD_MP13_00C7,
 PAD_MP13_00C8,
 PAD_MP13_00C9,
 PAD_MP13_00CA,
 PAD_MP13_00CB,
 PAD_MP13_00CC,
 PAD_MP13_00CD,
 PAD_MP13_00CE,
 PAD_MP13_00CF,
 PAD_MP13_00D0,
 PAD_MP13_00D1,
 PAD_MP13_00D2,
 PAD_MP13_00D3,
 PAD_MP13_00D4,
 PAD_MP13_00D5,
 PAD_MP13_00D6,
 PAD_MP13_00D7,
 PAD_MP13_00D8,
 PAD_MP13_00D9,
 PAD_MP13_00DA,
 PAD_MP13_END
};
enum pad_mp14 {
 PAD_MP14_END
};
enum pad_mp15 {
 PAD_MP15_0000,
 PAD_MP15_0001,
 PAD_MP15_0002,
 PAD_MP15_0003,
 PAD_MP15_0004,
 PAD_MP15_0005,
 PAD_MP15_0006,
 PAD_MP15_0007,
 PAD_MP15_0008,
 PAD_MP15_0009,
 PAD_MP15_000A,
 PAD_MP15_000B,
 PAD_MP15_000C,
 PAD_MP15_000D,
 PAD_MP15_000E,
 PAD_MP15_000F,
 PAD_MP15_0010,
 PAD_MP15_0011,
 PAD_MP15_0012,
 PAD_MP15_0013,
 PAD_MP15_0014,
 PAD_MP15_0015,
 PAD_MP15_0016,
 PAD_MP15_0017,
 PAD_MP15_0018,
 PAD_MP15_0019,
 PAD_MP15_001A,
 PAD_MP15_001B,
 PAD_MP15_001C,
 PAD_MP15_001D,
 PAD_MP15_001E,
 PAD_MP15_001F,
 PAD_MP15_0020,
 PAD_MP15_0021,
 PAD_MP15_0022,
 PAD_MP15_0023,
 PAD_MP15_0024,
 PAD_MP15_0025,
 PAD_MP15_0026,
 PAD_MP15_0027,
 PAD_MP15_0028,
 PAD_MP15_0029,
 PAD_MP15_002A,
 PAD_MP15_002B,
 PAD_MP15_002C,
 PAD_MP15_002D,
 PAD_MP15_002E,
 PAD_MP15_002F,
 PAD_MP15_0030,
 PAD_MP15_0031,
 PAD_MP15_0032,
 PAD_MP15_0033,
 PAD_MP15_0034,
 PAD_MP15_0035,
 PAD_MP15_0036,
 PAD_MP15_0037,
 PAD_MP15_0038,
 PAD_MP15_0039,
 PAD_MP15_003A,
 PAD_MP15_003B,
 PAD_MP15_003C,
 PAD_MP15_003D,
 PAD_MP15_003E,
 PAD_MP15_003F,
 PAD_MP15_0040,
 PAD_MP15_0041,
 PAD_MP15_0042,
 PAD_MP15_0043,
 PAD_MP15_0044,
 PAD_MP15_0045,
 PAD_MP15_0046,
 PAD_MP15_0047,
 PAD_MP15_0048,
 PAD_MP15_0049,
 PAD_MP15_004A,
 PAD_MP15_004B,
 PAD_MP15_004C,
 PAD_MP15_004D,
 PAD_MP15_004E,
 PAD_MP15_004F,
 PAD_MP15_0050,
 PAD_MP15_0051,
 PAD_MP15_0052,
 PAD_MP15_0053,
 PAD_MP15_0054,
 PAD_MP15_0055,
 PAD_MP15_0056,
 PAD_MP15_0057,
 PAD_MP15_0058,
 PAD_MP15_0059,
 PAD_MP15_005A,
 PAD_MP15_005B,
 PAD_MP15_005C,
 PAD_MP15_005D,
 PAD_MP15_005E,
 PAD_MP15_005F,
 PAD_MP15_0060,
 PAD_MP15_0061,
 PAD_MP15_0062,
 PAD_MP15_0063,
 PAD_MP15_0064,
 PAD_MP15_0065,
 PAD_MP15_0066,
 PAD_MP15_0067,
 PAD_MP15_0068,
 PAD_MP15_0069,
 PAD_MP15_006A,
 PAD_MP15_006B,
 PAD_MP15_006C,
 PAD_MP15_006D,
 PAD_MP15_006E,
 PAD_MP15_006F,
 PAD_MP15_0070,
 PAD_MP15_0071,
 PAD_MP15_0072,
 PAD_MP15_0073,
 PAD_MP15_0074,
 PAD_MP15_0075,
 PAD_MP15_0076,
 PAD_MP15_0077,
 PAD_MP15_0078,
 PAD_MP15_0079,
 PAD_MP15_007A,
 PAD_MP15_007B,
 PAD_MP15_007C,
 PAD_MP15_007D,
 PAD_MP15_007E,
 PAD_MP15_007F,
 PAD_MP15_0080,
 PAD_MP15_0081,
 PAD_MP15_0082,
 PAD_MP15_0083,
 PAD_MP15_0084,
 PAD_MP15_0085,
 PAD_MP15_0086,
 PAD_MP15_0087,
 PAD_MP15_0088,
 PAD_MP15_0089,
 PAD_MP15_008A,
 PAD_MP15_008B,
 PAD_MP15_008C,
 PAD_MP15_008D,
 PAD_MP15_008E,
 PAD_MP15_008F,
 PAD_MP15_0090,
 PAD_MP15_0091,
 PAD_MP15_0092,
 PAD_MP15_0093,
 PAD_MP15_0094,
 PAD_MP15_0095,
 PAD_MP15_0096,
 PAD_MP15_0097,
 PAD_MP15_0098,
 PAD_MP15_0099,
 PAD_MP15_009A,
 PAD_MP15_009B,
 PAD_MP15_009C,
 PAD_MP15_009D,
 PAD_MP15_009E,
 PAD_MP15_009F,
 PAD_MP15_00A0,
 PAD_MP15_00A1,
 PAD_MP15_00A2,
 PAD_MP15_00A3,
 PAD_MP15_00A4,
 PAD_MP15_00A5,
 PAD_MP15_00A6,
 PAD_MP15_00A7,
 PAD_MP15_00A8,
 PAD_MP15_00A9,
 PAD_MP15_00AA,
 PAD_MP15_00AB,
 PAD_MP15_00AC,
 PAD_MP15_00AD,
 PAD_MP15_00AE,
 PAD_MP15_00AF,
 PAD_MP15_00B0,
 PAD_MP15_00B1,
 PAD_MP15_00B2,
 PAD_MP15_00B3,
 PAD_MP15_00B4,
 PAD_MP15_00B5,
 PAD_MP15_00B6,
 PAD_MP15_00B7,
 PAD_MP15_00B8,
 PAD_MP15_00B9,
 PAD_MP15_00BA,
 PAD_MP15_00BB,
 PAD_MP15_00BC,
 PAD_MP15_00BD,
 PAD_MP15_00BE,
 PAD_MP15_00BF,
 PAD_MP15_00C0,
 PAD_MP15_00C1,
 PAD_MP15_00C2,
 PAD_MP15_00C3,
 PAD_MP15_00C4,
 PAD_MP15_00C5,
 PAD_MP15_00C6,
 PAD_MP15_00C7,
 PAD_MP15_00C8,
 PAD_MP15_00C9,
 PAD_MP15_00CA,
 PAD_MP15_00CB,
 PAD_MP15_00CC,
 PAD_MP15_00CD,
 PAD_MP15_00CE,
 PAD_MP15_00CF,
 PAD_MP15_00D0,
 PAD_MP15_00D1,
 PAD_MP15_00D2,
 PAD_MP15_00D3,
 PAD_MP15_00D4,
 PAD_MP15_00D5,
 PAD_MP15_00D6,
 PAD_MP15_00D7,
 PAD_MP15_00D8,
 PAD_MP15_00D9,
 PAD_MP15_END
};
enum pad_mp16 {
 PAD_MP16_END
};
enum pad_mp17 {
 PAD_MP17_END
};
enum pad_mp18 {
 PAD_MP18_END
};
enum pad_mp19 {
 PAD_MP19_END
};
enum pad_mp20 {
 PAD_MP20_END
};
typedef union {
 f32 m[4][4];
 s32 unused;
} Mtxf;
struct coord {
 union {
  struct {
   f32 x;
   f32 y;
   f32 z;
  };
  f32 f[3];
 };
};
struct bbox {
 f32 xmin;
 f32 xmax;
 f32 ymin;
 f32 ymax;
 f32 zmin;
 f32 zmax;
};
struct propstate {
          u16 propcount;
          u16 chrpropcount;
          u16 foregroundpropcount;
          u16 foregroundchrpropcount;
          s32 updatetime;
          s32 chrupdatetime;
          s32 slotupdate240;
          s32 slotupdate60error;
          u16 lastupdateframe;
};
struct playerstats {
          u32 shotcount[7];
          s32 killcount;
          u32 ggkillcount;
          u32 kills[4];
          u32 drawplayercount;
          f32 distance;
          u32 backshotcount;
          f32 armourcount;
          s32 fastest2kills;
          s32 slowest2kills;
          s32 longestlife;
          s32 shortestlife;
          u32 maxkills;
          s32 maxsimulkills;
          f32 damagescale;
          s32 tokenheldtime;
          u32 unk64;
          u32 cloaktime;
          u32 speedpillcount;
 union {
          s32 mpindex;
          u32 mpindexu32;
 };
          f32 scale_bg2gfx;
          f32 damreceived;
          f32 damtransmitted;
};
struct g_vars {
            s32 diffframe60;
            f32 diffframe60f;
            s32 lvframe60;
            s32 lvframenum;
            f32 diffframe60freal;
            s32 lastframetime;
            s32 thisframetime;
            s32 thisframeticks;
            s32 lostframetime;
            s32 lostframetime240;
            u32 mininc60;
            s32 roomportalrecursionlimit;
            s32 lvframe240;
            s32 lvupdate240;
            s32 lvupdate240_60;
            s32 lvupdate240_60error;
            s32 diffframe240;
            f32 lvupdate240f;
            f32 diffframe240f;
            f32 lvupdate240freal;
            f32 lvupdate240frealprev;
            s32 lastframe240;
            s32 thisframe240;
            f32 diffframe240freal;
            s16 *waypointnums;
            struct player *players[4];
            struct playerstats playerstats[4];
            u32 playerorder[4];
            struct player *currentplayer;
            struct playerstats *currentplayerstats;
            s32 currentplayernum;
            s32 currentplayerindex;
            s32 bondplayernum;
            s32 coopplayernum;
            s32 antiplayernum;
            struct player *bond;
            struct player *coop;
            struct player *anti;
            s32 tickmode;
            s32 killcount;
            u32 knockoutcount;
            struct textoverride *textoverrides;
            s32 roomcount;
            u32 hitboundscount;
            u32 unk0002c4;
            u32 unk0002c8;
            u32 unk0002cc;
            u32 unk0002d0;
            u32 unk0002d4;
            u32 unk0002d8;
            u32 unk0002dc;
            u32 unk0002e0;
            u32 unk0002e4;
            u32 unk0002e8;
            u32 unk0002ec;
            u32 unk0002f0;
            u32 unk0002f4;
            u32 unk0002f8;
            u32 unk0002fc;
            u32 unk000300;
            u32 unk000304;
            u32 unk000308;
            u32 unk00030c;
            u32 unk000310;
            u32 mplayerisrunning;
            u32 normmplayerisrunning;
            s32 lvmpbotlevel;
            s32 lockscreen;
            s32 bondvisible;
            s32 bondcollisions;
            s32 enableslopes;
            u32 padrandomroutes;
            s32 maxprops;
            struct prop *props;
            struct prop *activeprops;
            struct prop *activepropstail;
            struct prop *freeprops;
            struct prop **onscreenprops;
            struct prop **endonscreenprops;
            s32 numonscreenprops;
            struct prop *pausedprops;
            u8 numpropstates;
            u8 allocstateindex;
            u8 runstateindex;
            u8 alwaystick;
            u16 updateframe;
            u16 prevupdateframe;
            struct propstate propstates[7];
            struct chrdata *chrdata;
            struct truckobj *truck;
            struct heliobj *heli;
            struct chopperobj *hovercar;
            u8 *ailist;
            u32 aioffset;
            u32 unk00043c;
            s32 antiheadnum;
            s32 antibodynum;
            s32 coopradaron;
            s32 antiradaron;
            s32 pendingantiplayernum;
            s32 coopfriendlyfire;
            u32 modifiedfiles;
            s32 speedpilltime;
            s32 speedpillchange;
            u32 speedpillwant;
            s32 speedpillon;
            s32 restartlevel;
            s32 perfectbuddynum;
            s32 numaibuddies;
            s32 aibuddiesspawned;
            u32 unk00047c;
            u16 unk000480;
            u16 unk000482;
            s32 mphilltime;
            s32 totalkills;
            u32 unk00048c;
            s32 unk000490;
            s8 unk000494[4];
            s32 unk000498;
            s32 unk00049c;
            s32 unk0004a0;
            s32 mpquickteamnumsims;
            s32 mpsimdifficulty;
            s8 mpplayerteams[4];
            u32 mpquickteam;
            s32 stagenum;
            struct prop *aibuddies[4];
            u32 dontplaynrg;
            s32 in_cutscene;
            u8 paksconnected;
            u8 paksconnected2;
            s8 autocutnum;
            s8 autocutplaying;
            s8 autocutgroupcur;
            s8 autocutgroupleft;
            s8 autocutfinished;
            s8 autocutgroupskip;
            s32 joydisableframestogo;
            u8 playertojoymap[4];
            u8 fourmeg2player;
            u8 remakewallhitvtx;
            u8 cutsceneskip60ths;
            u8 langfilteron;
            u16 unk0004e4;
            u32 unk0004e8;
            u32 unk0004ec;
            u32 unk0004f0;
            u32 unk0004f4;
            u32 unk0004f8;
            u32 unk0004fc;
            u32 unk000500;
            u32 unk000504;
            u32 unk000508;
            u32 unk00050c;
            u32 unk000510;
            u32 unk000514;
            u32 unk000518;
            u32 unk00051c;
            u32 unk000520;
            u32 unk000524;
            u32 unk000528;
            u32 unk00052c;
            u32 unk000530;
            u32 unk000534;
            u32 unk000538;
            u32 unk00053c;
            u32 unk000540;
            u32 unk000544;
            u32 unk000548;
            u32 unk00054c;
            u32 unk000550;
            u32 unk000554;
            u32 unk000558;
            u32 unk00055c;
            u32 unk000560;
            u32 unk000564;
            u32 unk000568;
            u32 unk00056c;
            u32 unk000570;
            u32 unk000574;
            u32 unk000578;
            u32 unk00057c;
            u32 unk000580;
            u32 unk000584;
            u32 unk000588;
            u32 unk00058c;
            u32 unk000590;
            u32 unk000594;
            u32 unk000598;
            u32 unk00059c;
            u32 unk0005a0;
            u32 unk0005a4;
            u32 unk0005a8;
            u32 unk0005ac;
            u32 unk0005b0;
            u32 unk0005b4;
            u32 unk0005b8;
            u32 unk0005bc;
            u32 unk0005c0;
            u32 unk0005c4;
            u32 unk0005c8;
            u32 unk0005cc;
            u32 unk0005d0;
            u32 unk0005d4;
            u32 unk0005d8;
            u32 unk0005dc;
            u32 unk0005e0;
            u32 unk0005e4;
            u32 unk0005e8;
            u32 unk0005ec;
            u32 unk0005f0;
            u32 unk0005f4;
            u32 unk0005f8;
            u32 unk0005fc;
            u32 unk000600;
            u32 unk000604;
            u32 unk000608;
            u32 unk00060c;
            u32 unk000610;
            u32 unk000614;
            u32 unk000618;
            u32 unk00061c;
            u32 unk000620;
            u32 unk000624;
            u32 unk000628;
            u32 unk00062c;
            u32 unk000630;
            u32 unk000634;
            u32 unk000638;
            u32 unk00063c;
            u32 unk000640;
            u32 unk000644;
            u32 unk000648;
            u32 unk00064c;
            u32 unk000650;
            u32 unk000654;
            u32 unk000658;
            u32 unk00065c;
            u32 unk000660;
            u32 unk000664;
            u32 unk000668;
            u32 unk00066c;
            u32 unk000670;
            u32 unk000674;
            u32 unk000678;
            u32 unk00067c;
            u32 unk000680;
            u32 unk000684;
            u32 unk000688;
            u32 unk00068c;
            u32 unk000690;
            u32 unk000694;
            u32 unk000698;
            u32 unk00069c;
            u32 unk0006a0;
            u32 unk0006a4;
            u32 unk0006a8;
            u32 unk0006ac;
            u32 unk0006b0;
            u32 unk0006b4;
            u32 unk0006b8;
            u32 unk0006bc;
            u32 unk0006c0;
            u32 unk0006c4;
            u32 unk0006c8;
            u32 unk0006cc;
            u32 unk0006d0;
            u32 unk0006d4;
            u32 unk0006d8;
            u32 unk0006dc;
            u32 unk0006e0;
            u32 unk0006e4;
            u32 unk0006e8;
            u32 unk0006ec;
            u32 unk0006f0;
            u32 unk0006f4;
            u32 unk0006f8;
            u32 unk0006fc;
            u32 unk000700;
            u32 unk000704;
            u8 unk000708[0x1a8];
};
struct weaponobj;
struct prop;
struct explosion;
struct prop {
          u8 type;
          u8 flags;
          s16 timetoregen;
 union {
  struct chrdata *chr;
  struct defaultobj *obj;
  struct doorobj *door;
  struct weaponobj *weapon;
  struct explosion *explosion;
  struct smoke *smoke;
 };
          struct coord pos;
          f32 z;
          struct prop *parent;
          struct prop *child;
          struct prop *next;
          struct prop *prev;
          s16 rooms[8];
          u16 lastupdateframe;
          u16 propupdate240;
          u8 propupdate60err;
          u8 propstateindex;
          u8 backgroundedframes;
          u8 forceonetick : 1;
          u8 backgrounded : 1;
          u8 forcetick : 1;
          u8 active : 1;
          struct var800a41b0 *unk40;
          struct var800a41b0 *unk44;
};
struct pad {
          struct coord pos;
          struct coord look;
          struct coord up;
          struct coord normal;
          struct bbox bbox;
          s32 room;
          u32 flags;
          u8 liftnum;
          s16 unk52;
};
union filedataptr {
 u8 *u8;
 u32 *u32;
};
struct attackanimconfig {
          s16 animnum;
          f32 unk04;
          f32 unk08;
          f32 unk0c;
          f32 unk10;
          f32 unk14;
          f32 unk18;
          f32 unk1c;
          f32 unk20;
          f32 unk24;
          f32 unk28;
          f32 unk2c;
          f32 unk30;
          f32 unk34;
          f32 unk38;
          f32 unk3c;
          f32 unk40;
          f32 unk44;
};
struct model;
struct anim {
          s16 animnum;
          s16 animnum2;
          u8 unk04;
          u8 unk05;
          u8 unk06;
          u8 unk07;
          s8 flip;
          s8 flip2;
          s8 looping;
          s8 average;
          f32 frame;
          f32 frac;
          s16 framea;
          s16 frameb;
          f32 endframe;
          f32 speed;
          f32 newspeed;
          f32 oldspeed;
          f32 timespeed;
          f32 elapsespeed;
          f32 frame2;
          f32 frac2;
          s16 frame2a;
          s16 frame2b;
          f32 endframe2;
          f32 speed2;
          f32 newspeed2;
          f32 oldspeed2;
          f32 timespeed2;
          f32 elapsespeed2;
          f32 fracmerge;
          f32 timemerge;
          f32 elapsemerge;
          f32 loopframe;
          f32 loopmerge;
          void (*flipfunc)(void);
          u32 unk6c;
          s32 (*unk70)(struct model *model, struct coord *arg1, struct coord *arg2, f32 *ground);
          f32 playspeed;
          f32 newplay;
          f32 oldplay;
          f32 timeplay;
          f32 elapseplay;
          f32 animscale;
};
struct modeltype {
 s16 type;
 u16 numthings;
 u16 *things;
};
struct modelrodata_chrinfo {
 u16 unk00;
 s16 mtxindex;
 f32 unk04;
 u16 rwdataindex;
};
struct modelrodata_position {
 struct coord pos;
 u16 part;
 s16 pieces[3];
 f32 drawdist;
};
struct modelrodata_gundl {
 Gfx *primary;
 Gfx *secondary;
 void *baseaddr;
 void *unk0c;
 u16 unk10;
 s16 unk12;
};
struct modelrodata_distance {
 f32 near;
 f32 far;
 struct modelnode *target;
 u16 rwdataindex;
};
struct modelrodata_reorder {
 f32 unk00;
 f32 unk04;
 f32 unk08;
 f32 unk0c[3];
 struct modelnode *unk18;
 struct modelnode *unk1c;
 s16 unk20;
 u16 rwdataindex;
};
struct modelrodata_bbox {
 s32 hitpart;
 f32 xmin;
 f32 xmax;
 f32 ymin;
 f32 ymax;
 f32 zmin;
 f32 zmax;
};
struct modelrodata_type0b {
 u32 unk00;
 u32 unk04;
 u32 unk08;
 u32 unk0c;
 u32 unk10;
 u32 unk14;
 u32 unk18;
 u32 unk1c;
 u32 unk20;
 u32 unk24;
 u32 unk28;
 u32 unk2c;
 u32 unk30;
 u32 unk34;
 u32 unk38;
 void *unk3c;
 u32 unk40;
 u16 rwdataindex;
 void *baseaddr;
};
struct modelrodata_gunfire {
 struct coord pos;
 struct coord dim;
 struct modeltexture *texture;
 f32 unk1c;
 u16 rwdataindex;
 void *baseaddr;
};
struct modelrodata_type0d {
 u32 unk00;
 u32 unk04;
 u32 unk08;
 u32 unk0c;
 void *unk10;
 void *unk14;
 u32 unk18;
 void *baseaddr;
};
struct modelrodata_type11 {
 u32 unk00;
 u32 unk04;
 u32 unk08;
 u32 unk0c;
 u32 unk10;
 void *unk14;
};
struct modelrodata_toggle {
 struct modelnode *target;
 u16 rwdataindex;
};
struct modelrodata_positionheld {
 struct coord pos;
 s16 unk0c;
};
struct modelrodata_type16 {
 u32 unk00;
 void *unk04;
 Gfx *unk08;
 void *baseaddr;
};
struct modelrodata_headspot {
 u16 rwdataindex;
};
struct modelrodata_dl {
          Gfx *primary;
          Gfx *secondary;
          u32 *colourtable;
          struct gfxvtx *vertices;
          s16 numvertices;
          s16 mcount;
          u16 rwdataindex;
          u16 numcolours;
};
union modelrodata {
 struct modelrodata_chrinfo chrinfo;
 struct modelrodata_position position;
 struct modelrodata_gundl gundl;
 struct modelrodata_distance distance;
 struct modelrodata_reorder reorder;
 struct modelrodata_bbox bbox;
 struct modelrodata_type0b type0b;
 struct modelrodata_gunfire gunfire;
 struct modelrodata_type0d type0d;
 struct modelrodata_type11 type11;
 struct modelrodata_toggle toggle;
 struct modelrodata_positionheld positionheld;
 struct modelrodata_type16 type16;
 struct modelrodata_headspot headspot;
 struct modelrodata_dl dl;
};
struct modelnode {
          u16 type;
          union modelrodata *rodata;
          struct modelnode *parent;
          struct modelnode *next;
          struct modelnode *prev;
          struct modelnode *child;
};
struct modelfiledata {
 struct modelnode *rootnode;
 struct modeltype *type;
 struct modelnode **parts;
 s16 numparts;
 s16 nummatrices;
 f32 unk10;
 s16 rwdatalen;
 void *unk18;
};
struct model {
          u8 unk00;
          u8 unk01;
          s16 unk02;
 union {
  struct chrdata *chr;
  struct defaultobj *obj;
 };
          struct modelfiledata *filedata;
          Mtxf *matrices;
          union modelrwdata **rwdatas;
          f32 scale;
          struct model *attachedtomodel;
          struct modelnode *attachedtonode;
          struct anim *anim;
};
struct modelrwdata_chrinfo {
 s8 unk00;
 s8 unk01;
 s8 unk02;
 f32 ground;
 struct coord pos;
 f32 unk14;
 f32 unk18;
 f32 unk1c;
 f32 unk20;
 struct coord unk24;
 f32 unk30;
 struct coord unk34;
 struct coord unk40;
 struct coord unk4c;
 f32 unk58;
 f32 unk5c;
};
struct modelrwdata_05 {
 s32 unk00;
};
struct modelrwdata_distance {
 s32 visible;
};
struct modelrwdata_reorder {
 s32 visible;
};
struct modelrwdata_0b {
 u16 unk00;
 u16 unk04;
};
struct modelrwdata_gunfire {
 s16 visible;
 u16 unk02;
};
struct modelrwdata_toggle {
 s32 visible;
};
struct modelrwdata_headspot {
 struct modelfiledata *modelfiledata;
 void *rwdatas;
};
struct modelrwdata_dl {
 struct gfxvtx *vertices;
 Gfx *gdl;
 u32 *unk08;
};
union modelrwdata {
 struct modelrwdata_chrinfo chrinfo;
 struct modelrwdata_05 type05;
 struct modelrwdata_distance distance;
 struct modelrwdata_reorder reorder;
 struct modelrwdata_0b type0b;
 struct modelrwdata_gunfire gunfire;
 struct modelrwdata_toggle toggle;
 struct modelrwdata_headspot headspot;
 struct modelrwdata_dl dl;
};
struct waygroup {
 s32 *neighbours;
 s32 *waypoints;
 s32 unk08;
};
struct waypoint {
 s32 padnum;
 s32 *neighbours;
 s32 groupnum;
 s32 unk0c;
};
struct aibot {
           u8 unk000;
           s16 aibotnum;
           struct mpsim *simulant;
           s16 attackingplayernum;
           s16 followingplayernum;
           s16 dangerouspropnum;
           struct prop *fetchprop;
           struct invitem *items;
           s8 maxitems;
           s32 *ammoheld;
           s32 weaponnum;
           s32 loadedammo[2];
           s16 unk02c[2];
           u32 unk030;
           u32 unk034;
           u32 unk038;
           u16 unk03c;
           s16 unk03e;
           f32 unk040;
           struct prop *skrocket;
           s16 unk048;
           s16 unk04a;
           u8 unk04c_00 : 1;
           u8 iscloserangeweapon : 1;
           u8 gunfunc : 1;
           u8 unk04c_03 : 1;
           u8 unk04c_04 : 1;
           u8 unk04c_05 : 1;
           u8 cloakdeviceenabled : 1;
           u8 unk04c_07 : 1;
           u8 unk04d[2];
           u8 teamisonlyai : 1;
           u8 unk04f_01 : 1;
           u8 unk04f_02 : 1;
           u8 unk04f_03 : 1;
           u8 unk04f_04 : 1;
           u8 unk04f_05 : 1;
           u8 unk04f_06 : 1;
           u8 unk04f_07 : 1;
           struct prop *unk050;
           u32 unk054;
           u8 unk058;
           u8 unk059;
           u32 unk05c;
           u32 unk060;
           u16 unk064;
           struct attackanimconfig *unk068;
           f32 unk06c;
           f32 unk070;
           s8 unk074;
           s8 unk075;
           s16 lastkilledbyplayernum;
           u8 unk078;
           u8 command;
           s16 rooms[1];
           u32 unk07c;
           u32 unk080;
           u32 unk084;
           u32 unk088;
           struct coord defendholdpos;
           f32 unk098;
           u8 unk09c_00 : 1;
           u8 unk09c_01 : 1;
           u8 rcp120cloakenabled : 1;
           u8 unk09c_03 : 1;
           u8 unk09c_04 : 1;
           u8 unk09c_05 : 1;
           u8 unk09c_06 : 1;
           u8 cheap : 1;
           u8 unk09d;
           s32 unk0a0;
           f32 unk0a4;
           f32 angleoffset;
           f32 speedtheta;
           f32 unk0b0;
           f32 unk0b4;
           f32 unk0b8;
           s32 unk0bc;
           s32 attackpropnum;
           u32 unk0c4[2];
           u32 unk0cc;
           s32 unk0d0;
           s32 followprotectpropnum;
           s32 unk0d8;
           u32 unk0dc;
           s16 unk0e0[2];
           f32 unk0e4[2];
           u32 unk0ec;
           u32 unk0f0;
           u32 unk0f4;
           u32 unk0f8;
           u32 unk0fc;
           u32 unk100;
           u32 unk104;
           struct coord shotspeed;
           s32 unk114;
           u32 unk118;
           s32 unk11c;
           s32 unk120;
           s32 unk124;
           u32 unk128;
           u32 unk12c;
           s8 unk130[12];
           f32 playerdistances[12];
           u8 unk16c[12];
           s32 unk178[12];
           s16 unk1a8[12];
           f32 unk1c0;
           f32 unk1c4;
           f32 unk1c8;
           s32 unk1cc;
           u32 unk1d0;
           f32 unk1d4;
           u32 unk1d8;
           u32 unk1dc;
           u32 unk1e0;
           s32 unk1e4;
           u32 unk1e8;
           u32 unk1ec;
           u32 unk1f0;
           u32 unk1f4;
           u32 unk1f8;
           u32 unk1fc;
           u32 unk200;
           u32 unk204;
           s32 unk208;
           s32 randttl60;
           u32 rand;
           f32 killsbygunfunc[6][2];
           f32 suicidesbygunfunc[6][2];
           s32 equipdurations60[6][2];
           s32 unk2a4;
           s32 unk2a8[6];
           s32 dampensuicidesttl60;
           f32 unk2c4;
           s32 unk2c8;
           u32 unk2cc;
           u32 unk2d0;
           f32 unk2d4;
           u32 unk2d8;
           u32 unk2dc;
};
struct tile {
          u8 type;
          u8 numvertices;
          u16 flags;
};
struct tiletype0 {
 struct tile header;
          u16 floortype;
          u8 xmin;
          u8 ymin;
          u8 zmin;
          u8 xmax;
          u8 ymax;
          u8 zmax;
          u16 floorcol;
          s16 vertices[64][3];
};
struct tiletype1 {
 struct tile header;
          u16 floortype;
          u8 xmin;
          u8 ymin;
          u8 zmin;
          u8 xmax;
          u8 ymax;
          u8 zmax;
          u16 floorcol;
          struct coord vertices[64];
};
struct tiletype2 {
 struct tile header;
          f32 ymax;
          f32 ymin;
          f32 vertices[8][2];
};
struct tiletype3 {
 struct tile header;
          f32 ymax;
          f32 ymin;
          f32 x;
          f32 z;
          f32 width;
};
struct tilething {
 struct tile *tile;
 u32 unk04;
 u32 unk08;
 struct prop *lift;
 u32 floorroom;
};
struct act_stand {
          s32 prestand;
          s32 flags;
          s32 entityid;
          s32 reaim;
          s32 turning;
          s32 checkfacingwall;
          s32 wallcount;
          f32 mergetime;
          u8 playwalkanim;
};
struct act_anim {
          s32 movewheninvis;
          s32 pauseatend;
          s32 completed;
          s32 slowupdate;
          s32 lockpos;
          u8 ishitanim;
          u8 reverse;
          u16 hitframe;
          u16 hitdamage;
          u16 hitradius;
          s16 animnum;
          u8 flip;
          f32 startframe;
          f32 endframe;
          f32 speed;
          f32 blend;
};
struct act_die {
          s32 notifychrindex;
          f32 thudframe1;
          f32 thudframe2;
          f32 timeextra;
          f32 elapseextra;
          struct coord extraspeed;
          s16 drcarollimagedelay;
};
struct act_dead {
          s32 fadenow;
          s32 fadewheninvis;
          s32 invistimer60;
          s32 fadetimer60;
          s32 notifychrindex;
};
struct act_argh {
          s32 notifychrindex;
          s32 lvframe60;
};
struct gset {
 u8 weaponnum;
 u8 unk0639;
 u8 unk063a;
 u8 weaponfunc;
};
struct act_preargh {
          struct coord dir;
          f32 relshotdir;
          s32 hitpart;
          struct gset gset;
          s32 aplayernum;
};
struct act_attack {
          struct attackanimconfig *animcfg;
          s8 turning;
          s8 fired;
          s8 nextgun;
          s8 numshots;
          s8 maxshots;
          s8 onehanded;
          s8 dorecoil;
          s8 dooneburst;
          s8 firegun[2];
          s8 everytick[2];
          u8 singleshot[2];
          s8 flip;
          s32 pausecount;
          s32 lastfire60;
          s32 lastontarget60;
          u32 flags;
          s32 entityid;
          s32 standing;
          s32 reaim;
};
struct act_attackwalk {
          s32 unk02c;
          s32 frame60count;
          s32 frame60max;
          s32 facedtarget;
          struct attackanimconfig *animcfg;
          s32 nextshot60;
          s32 nextgun;
          s8 firegun[2];
          s8 everytick[2];
          s8 singleshot[2];
          u8 flip;
          s32 dorecoil;
          f32 turnspeed;
};
struct act_sidestep {
          s32 side;
};
struct act_jumpout {
          s32 side;
};
struct act_runpos {
          struct coord pos;
          f32 neardist;
          s32 eta60;
          f32 turnspeed;
};
struct waydata {
          s8 mode;
          u8 iter;
          u8 gotaimpos;
          u8 gotaimposobj;
          struct coord aimpos;
          u32 unk10;
          u32 unk14;
          u32 unk18;
          u32 unk1c;
          u32 unk20;
          u32 unk24;
          s32 age;
          u32 aimposobj;
          s8 unk30_test;
          u32 unk34;
          f32 magicdone;
          f32 magictotal;
          s32 lastvisible60;
};
struct act_patrol {
           struct path *path;
           s32 nextstep;
           s32 forward;
           struct waydata waydata;
           f32 turnspeed;
};
struct act_gopos {
           struct coord endpos;
           s16 endrooms[8];
           struct waypoint *target;
           struct waypoint *waypoints[6];
           u8 curindex;
           u8 flags;
           u16 restartttl;
           struct waydata waydata;
           f32 turnspeed;
};
struct act_surprised {
          u32 type;
};
struct act_throwgrenade {
          u32 flags;
          u32 entityid;
          u32 hand;
          s32 needsequip;
};
struct act_bondmulti {
          struct attackanimconfig *animcfg;
};
struct act_druggedcomingup {
          s16 timer60;
};
struct act_robotattack {
          struct coord pos[2];
          struct coord dir[2];
          u32 guntype[2];
          s32 numshots[2];
          u8 firing[2];
          u8 finished;
};
struct act_skjump {
          u8 state;
          u8 needsnewanim;
          u8 hit;
          f32 vel[2];
          f32 roty;
          s32 timer60;
          struct coord pos;
          s32 total60;
          f32 ground;
};
struct chrdata {
           s16 chrnum;
           s8 accuracyrating;
           s8 speedrating;
           u8 firecount[2];
           s8 headnum;
           s8 actiontype;
           s8 sleep;
           s8 invalidmove;
           s8 numclosearghs;
           s8 numarghs;
           u8 fadealpha;
           s8 arghrating;
           s8 aimendcount;
           u8 grenadeprob;
           s16 bodynum;
           s8 flinchcnt;
           s8 path;
           u32 hidden;
           u32 chrflags;
           struct prop *prop;
           struct model *model;
           f32 chrwidth;
           f32 chrheight;
 union {
  struct act_stand act_stand;
  struct act_anim act_anim;
  struct act_die act_die;
  struct act_dead act_dead;
  struct act_argh act_argh;
  struct act_preargh act_preargh;
  struct act_attack act_attack;
  struct act_attackwalk act_attackwalk;
  struct act_sidestep act_sidestep;
  struct act_jumpout act_jumpout;
  struct act_runpos act_runpos;
  struct act_patrol act_patrol;
  struct act_gopos act_gopos;
  struct act_surprised act_surprised;
  struct act_throwgrenade act_throwgrenade;
  struct act_bondmulti act_bondmulti;
  struct act_druggedcomingup act_druggedcomingup;
  struct act_robotattack act_robotattack;
  struct act_skjump act_skjump;
 };
           f32 sumground;
           f32 manground;
           f32 ground;
           struct coord fallspeed;
           struct coord prevpos;
           s32 lastwalk60;
           s32 lastmoveok60;
           f32 visionrange;
           s32 lastseetarget60;
           s32 lastvisibletarget60;
           struct prop *poisonprop;
           s16 lastshooter;
           s16 timeshooter;
           f32 hearingscale;
           s32 lastheartarget60;
           u8 shadecol[4];
           u8 nextcol[4];
           f32 damage;
           f32 maxdamage;
           u8 *ailist;
           u16 aioffset;
           s16 aireturnlist;
           s16 aishotlist;
           u8 morale;
           u8 alertness;
           u32 flags;
           u32 flags2;
           s32 timer60;
           s32 soundtimer;
           u8 random;
           u8 team;
           u8 soundgap;
           s16 padpreset1;
           s16 chrpreset1;
           s16 proppreset1;
           s16 chrseeshot;
           s16 chrseedie;
           s16 chrdup;
 struct tiletype3 geo;
           f32 shotbondsum;
           f32 aimuplshoulder;
           f32 aimuprshoulder;
           f32 aimupback;
           f32 aimsideback;
           f32 aimendlshoulder;
           f32 aimendrshoulder;
           f32 aimendback;
           f32 aimendsideback;
           struct prop *weapons_held[3];
           s8 fireslots[2];
           s16 target;
           f32 cshield;
           s8 cmnum;
           s8 cmnum2;
           s8 cmnum3;
           s8 cmnum4;
           u16 cmcount;
           u16 floorcol;
           f32 oldframe;
           s8 footstep;
           u8 floortype;
           u16 hidden2;
           f32 magicframe;
           f32 magicspeed;
           s32 magicanim;
           s32 bdlist[60];
           u8 bdstart;
           u8 goposhitcount;
           s16 cover;
           struct coord targetlastseenp;
           u8 myaction;
           u8 orders;
           u8 squadron;
           u8 listening;
           u32 convtalk;
           s32 talktimer;
           u8 question;
           u8 talkgap;
           u16 unk2ae;
           u8 tude;
           u8 voicebox;
           s16 floorroom;
           u32 unk2b4;
           s16 oldrooms[8];
           struct coord runfrompos;
           struct aibot *aibot;
           s16 blurdrugamount;
           s16 cloakpause;
           f32 drugheadsway;
           u8 drugheadcount;
           u8 cloakfadefrac : 7;
           u8 cloakfadefinished : 1;
           u8 teamscandist;
           u8 naturalanim;
           s32 myspecial;
           f32 timeextra;
           f32 elapseextra;
           struct coord extraspeed;
           u8 yvisang;
           u8 hitpart;
           u8 race;
           u8 blurnumtimesdied;
           struct prop *gunprop;
           f32 pushspeed[2];
           f32 gunroty[2];
           f32 gunrotx[2];
           u32 onladder;
           struct coord laddernormal;
 u8 liftaction : 8;
 u8 inlift : 1;
 u8 pouncebits : 3;
 u8 unk32c_12 : 2;
 u8 darkroomthing : 1;
 u8 playerdeadthing : 1;
 u8 p1p2 : 2;
 u8 unk32c_18 : 1;
 u8 noblood : 1;
 u8 rtracked : 1;
 u8 unk32c_21 : 1;
 u8 unk32c_22 : 2;
 u8 specialdie : 8;
           u16 roomtosearch;
           u8 propsoundcount;
           s8 patrolnextstep;
           u8 bulletstaken;
           u8 woundedsplatsadded;
           u16 tickssincesplat;
           u8 splatsdroppedhe;
           u8 stdsplatsadded;
           u8 deaddropsplatsadded;
           s8 aimtesttimer60;
           struct coord lastdroppos;
           struct fireslotthing *unk348[2];
           struct chrdata *lastattacker;
           s16 aipunchdodgelist;
           s16 aishootingatmelist;
           s16 poisoncounter;
           s16 aidarkroomlist;
           s16 aiplayerdeadlist;
           u8 dodgerating;
           u8 maxdodgerating;
           u8 unarmeddodgerating;
           u8 lastfootsample;
           u8 drcarollimage_left : 4;
           u8 drcarollimage_right : 4;
           struct prop *lift;
};
struct projectile {
           u32 flags;
           struct coord unk004;
           f32 unk010;
           f32 unk014;
           f32 unk018;
           f32 unk01c;
           f32 unk020;
           u32 unk024;
           u32 unk028;
           u32 unk02c;
           u32 unk030;
           u32 unk034;
           u32 unk038;
           u32 unk03c;
           u32 unk040;
           struct projectile *unk044;
           u32 unk048;
           u32 unk04c;
           u32 unk050;
           u32 unk054;
           u32 unk058;
           u32 unk05c;
           f32 unk060;
           u32 unk064;
           u32 unk068;
           u32 unk06c;
           u32 unk070;
           u32 unk074;
           u32 unk078;
           u32 unk07c;
           u32 unk080;
           u32 unk084;
           struct prop *ownerprop;
           f32 unk08c;
           u32 unk090;
           s32 unk094;
           f32 unk098;
           s32 unk09c;
           u32 unk0a0;
           s32 unk0a4;
           u32 unk0a8;
           u32 unk0ac;
           s16 dropreason;
           s16 unk0b2;
           s32 unk0b4;
           f32 unk0b8;
           f32 unk0bc;
           f32 unk0c0;
           struct coord pos;
           u32 unk0d0;
           struct defaultobj *obj;
           s32 unk0d8;
           f32 unk0dc;
           f32 unk0e0;
           f32 unk0e4;
           u32 unk0e8;
           f32 unk0ec;
           f32 unk0f0;
           u32 unk0f4;
           s16 waypads[6];
           u8 numwaypads;
           u8 unk105;
           struct prop *unk108;
};
struct monitorthing {
           u32 flags;
           Mtxf matrix;
           struct projectile *unk044;
};
struct tvscreen {
          u32 *cmdlist;
          u16 offset;
          s16 pause60;
          struct textureconfig *tconfig;
          f32 rot;
          f32 xscale;
          f32 xscalefrac;
          f32 xscaleinc;
          f32 xscaleold;
          f32 xscalenew;
          f32 yscale;
          f32 yscalefrac;
          f32 yscaleinc;
          f32 yscaleold;
          f32 yscalenew;
          f32 xmid;
          f32 xmidfrac;
          f32 xmidinc;
          f32 xmidold;
          f32 xmidnew;
          f32 ymid;
          f32 ymidfrac;
          f32 ymidinc;
          f32 ymidold;
          f32 ymidnew;
          u8 red;
          u8 redold;
          u8 rednew;
          u8 green;
          u8 greenold;
          u8 greennew;
          u8 blue;
          u8 blueold;
          u8 bluenew;
          u8 alpha;
          u8 alphaold;
          u8 alphanew;
          f32 colfrac;
          f32 colinc;
};
struct hov {
          u32 unk00;
          f32 unk04;
          f32 unk08;
          f32 unk0c;
          f32 unk10;
          f32 unk14;
          f32 unk18;
          f32 unk1c;
          f32 unk20;
          f32 unk24;
          f32 unk28;
          f32 unk2c;
          f32 unk30;
          f32 ground;
          s32 groundnext60;
          s32 groundprev60;
};
struct defaultobj {
          u16 extrascale;
          u8 hidden2;
          u8 type;
          s16 modelnum;
          s16 pad;
          u32 flags;
          u32 flags2;
          u32 flags3;
          struct prop *prop;
          struct model *model;
          f32 realrot[9];
          u32 hidden;
 union {
           struct tiletype1 *geo1;
           struct tiletype2 *geo2;
           struct tiletype3 *geo3;
           struct tiletype3 *unkgeo;
 };
 union {
           struct projectile *projectile;
           struct monitorthing *monitorthing;
 };
          s16 damage;
          s16 maxdamage;
          u8 shadecol[4];
          u8 nextcol[4];
          u16 floorcol;
          s8 numtiles;
};
struct doorobj {
 struct defaultobj base;
          f32 maxfrac;
          f32 perimfrac;
          f32 accel;
          f32 decel;
          f32 maxspeed;
          u16 doorflags;
          u16 doortype;
          u32 keyflags;
          s32 autoclosetime;
          f32 frac;
          f32 fracspeed;
          s8 mode;
          u8 glasshits;
          s16 fadealpha;
          s16 xludist;
          s16 opadist;
          struct coord startpos;
          u32 unk98;
          u32 unk9c;
          u32 unka0;
          struct gfxvtx *unka4;
          u32 unka8;
          u32 unkac;
          u32 unkb0;
          u32 unkb4;
          u32 unkb8;
          struct doorobj *sibling;
          s32 lastopen60;
          s16 portalnum;
          s8 soundtype;
          s8 fadetime60;
          s32 lastcalc60;
          u8 laserfade;
          u8 unusedmaybe[3];
          u8 shadeinfo1[4];
          u8 shadeinfo2[4];
};
struct doorscaleobj {
 u32 unk00;
 s32 scale;
};
struct keyobj {
 struct defaultobj base;
 u32 keyflags;
};
struct cctvobj {
 struct defaultobj base;
          s16 lookatpadnum;
          s16 toleft;
          Mtxf camrotm;
          f32 yzero;
          f32 yrot;
          f32 yleft;
          f32 yright;
          f32 yspeed;
          f32 ymaxspeed;
          s32 seebondtime60;
          f32 maxdist;
          f32 xzero;
};
struct ammocrateobj {
 struct defaultobj base;
          s32 ammotype;
};
struct weaponobj {
 struct defaultobj base;
          u8 weaponnum;
          s8 unk5d;
          s8 unk5e;
          u8 gunfunc;
          s8 unk60;
          s8 dualweaponnum;
          s16 timer240;
          struct weaponobj *dualweapon;
};
struct packedchr {
 s16 chrindex;
 s8 unk02;
 s8 typenum;
 u32 spawnflags;
 s16 chrnum;
 u16 padnum;
 u8 bodynum;
 s8 headnum;
 u16 ailistnum;
 u16 padpreset;
 u16 chrpreset;
 u16 hearscale;
 u16 viewdist;
 u32 flags;
 u32 flags2;
 u8 team;
 u8 squadron;
 s16 chair;
 u32 convtalk;
 u8 tude;
 u8 naturalanim;
 u8 yvisang;
 u8 teamscandist;
};
struct singlemonitorobj {
 struct defaultobj base;
          struct tvscreen screen;
          s16 owneroffset;
          s8 ownerpart;
          u8 imagenum;
};
struct multimonitorobj {
 struct defaultobj base;
 struct tvscreen screens[4];
 u8 imagenums[4];
};
struct hangingmonitorsobj {
 struct defaultobj base;
};
struct autogunobj {
 struct defaultobj base;
          s16 targetpad;
          u8 firing;
          u8 firecount;
          f32 yzero;
          f32 ymaxleft;
          f32 ymaxright;
          f32 yrot;
          f32 yspeed;
          f32 xzero;
          f32 xrot;
          f32 xspeed;
          f32 maxspeed;
          f32 aimdist;
          f32 barrelspeed;
          f32 barrelrot;
          s32 lastseebond60;
          s32 lastaimbond60;
          s32 allowsoundframe;
          struct beam *beam;
          f32 shotbondsum;
          u32 unka4;
          u8 targetteam;
          u8 ammoquantity;
          u16 nextchrtest;
};
struct linkgunsobj {
 u32 unk00;
 s16 offset1;
 s16 offset2;
};
struct hatobj {
 struct defaultobj base;
};
struct grenadeprobobj {
 u32 unk00;
 s16 chrnum;
 u16 probability;
};
struct linkliftdoorobj {
 u32 unk00;
 struct prop *door;
 struct prop *lift;
 struct linkliftdoorobj *next;
 s32 stopnum;
};
struct multiammocrateslot {
 u16 modelnum;
 u16 quantity;
};
struct multiammocrateobj {
 struct defaultobj base;
          struct multiammocrateslot slots[19];
};
struct shieldobj {
 struct defaultobj base;
          f32 initialamount;
          f32 amount;
};
struct tag {
          u32 identifier;
          u16 tagnum;
          s16 cmdoffset;
          struct tag *next;
          struct defaultobj *obj;
};
struct objective {
          u32 unk00;
          s32 index;
          u32 text;
          u16 unk0c;
          u8 flags;
          s8 difficulties;
};
struct briefingobj {
 u32 unk00;
 u32 type;
 u32 text;
 struct briefingobj *next;
};
struct padlockeddoorobj {
 u32 unk00;
 struct doorobj *door;
 struct defaultobj *lock;
 struct padlockeddoorobj *next;
};
struct truckobj {
 struct defaultobj base;
          u8 *ailist;
          u16 aioffset;
          s16 aireturnlist;
          f32 speed;
          f32 wheelxrot;
          f32 wheelyrot;
          f32 speedaim;
          f32 speedtime60;
          f32 turnrot60;
          f32 roty;
          struct path *path;
          s32 nextstep;
};
struct heliobj {
 struct defaultobj base;
          u8 *ailist;
          u16 aioffset;
          s16 aireturnlist;
          f32 rotoryrot;
          f32 rotoryspeed;
          f32 rotoryspeedaim;
          f32 rotoryspeedtime;
          f32 speed;
          f32 speedaim;
          f32 speedtime60;
          f32 yrot;
          struct path *path;
          s32 nextstep;
};
struct glassobj {
 struct defaultobj base;
          s16 portalnum;
};
struct safeobj {
 u32 unk00;
};
struct safeitemobj {
 u32 unk00;
 struct defaultobj *item;
 struct safeobj *safe;
 struct doorobj *door;
 struct safeitemobj *next;
};
struct cameraposobj {
 s32 type;
 f32 x;
 f32 y;
 f32 z;
 f32 theta;
 f32 verta;
 s32 pad;
};
struct tintedglassobj {
 struct defaultobj base;
          s16 xludist;
          s16 opadist;
          s16 opacity;
          s16 portalnum;
          f32 unk64;
};
struct liftobj {
 struct defaultobj base;
          s16 pads[4];
          struct doorobj *doors[4];
          f32 dist;
          f32 speed;
          f32 accel;
          f32 maxspeed;
          s8 soundtype;
          s8 levelcur;
          s8 levelaim;
          struct coord prevpos;
};
struct linksceneryobj {
 u32 unk00;
 struct defaultobj *trigger;
 struct defaultobj *unexp;
 struct defaultobj *exp;
 struct linksceneryobj *next;
};
struct blockedpathobj {
 u32 unk00;
 struct defaultobj *blocker;
 s16 waypoint1;
 s16 waypoint2;
 struct blockedpathobj *next;
};
struct hoverbikeobj {
 struct defaultobj base;
 struct hov hov;
           f32 speed[2];
           f32 prevpos[2];
           f32 w;
           f32 rels[2];
           f32 exreal;
           f32 ezreal;
           f32 ezreal2;
           f32 leanspeed;
           f32 leandiff;
           u32 maxspeedtime240;
           f32 speedabs[2];
           f32 speedrel[2];
};
struct hoverpropobj {
 struct defaultobj base;
 struct hov hov;
};
struct fanobj {
 struct defaultobj base;
          f32 yrot;
          f32 yrotprev;
          f32 ymaxspeed;
          f32 yspeed;
          f32 yaccel;
          s8 on;
};
struct hovercarobj {
 struct defaultobj base;
          u8 *ailist;
          u16 aioffset;
          s16 aireturnlist;
          f32 speed;
          f32 speedaim;
          f32 speedtime60;
          f32 turnyspeed60;
          f32 turnxspeed60;
          f32 turnrot60;
          f32 roty;
          f32 rotx;
          f32 rotz;
          struct path *path;
          s32 nextstep;
          s16 status;
          s16 dead;
          s16 deadtimer60;
          s16 sparkstimer60;
};
struct padeffectobj {
 u32 unk00;
 s32 effect;
 s32 pad;
};
struct chopperobj {
 struct defaultobj base;
          u8 *ailist;
          u16 aioffset;
          s16 aireturnlist;
 union {
  struct {
            f32 speed;
            f32 speedaim;
            f32 speedtime60;
  };
  struct coord fall;
 };
          u32 turnyspeed60;
          u32 turnxspeed60;
          f32 turnrot60;
          f32 roty;
          f32 rotx;
          f32 rotz;
          struct path *path;
          s32 nextstep;
          s16 weaponsarmed;
          s16 ontarget;
          s16 target;
          u8 attackmode;
          u8 cw;
          f32 vx;
          f32 vy;
          f32 vz;
          f32 power;
          f32 otx;
          f32 oty;
          f32 otz;
          f32 bob;
          f32 bobstrength;
          u32 targetvisible;
          s32 timer60;
          s32 patroltimer60;
          u32 gunturnyspeed60;
          u32 gunturnxspeed60;
          f32 gunroty;
          f32 gunrotx;
          f32 barrelrotspeed;
          f32 barrelrot;
          struct fireslotthing *fireslotthing;
          s32 dead;
};
struct mineobj {
 struct defaultobj base;
};
struct escalatorobj {
 struct defaultobj base;
          s32 frame;
          struct coord prevpos;
};
struct eyespy {
          struct prop *prop;
          struct coord look;
          struct coord up;
          f32 theta;
          f32 costheta;
          f32 sintheta;
          f32 verta;
          f32 cosverta;
          f32 sinverta;
          u8 init;
          u8 initialised;
          s8 startuptimer60;
          s8 active;
          s8 buttonheld;
          s8 camerabuttonheld;
          s16 bobdir;
          u8 bobtimer;
          u8 bobactive;
          struct coord vel;
          struct coord unk4c;
          f32 speed;
          f32 oldground;
          f32 height;
          f32 gravity;
          s8 camerashuttertime;
          u8 hit;
          u8 opendoor;
          u8 mode;
          f32 velf[2];
          f32 vels[2];
          f32 pitch;
};
struct audiohandle {
 u32 unk00;
};
struct gunheld {
 s32 weapon1;
 s32 weapon2;
 s32 totaltime240_60;
};
struct playerbond {
                   struct coord unk00;
                   f32 width;
                   struct coord unk10;
                   struct coord unk1c;
                   struct coord unk28;
};
struct threat {
 struct prop *prop;
 s16 x1;
 s16 y1;
 s16 x2;
 s16 y2;
};
struct beam {
          s8 age;
          s8 weaponnum;
          struct coord from;
          struct coord dir;
          f32 maxdist;
          f32 speed;
          f32 mindist;
          f32 dist;
};
struct abmag {
          s16 loadedammo;
          s16 timer60;
          s8 ref;
          s8 change;
          u16 alignment;
};
struct hand {
 struct gset gset;
            s8 firing;
            s8 flashon;
            u8 gunon;
            s8 visible;
            s8 inuse;
            s32 triggeron;
            s32 triggerprev;
            s32 triggerreleased;
            s32 count;
            s32 count60;
            u32 mode;
            u32 modenext;
            u32 numfires;
            u32 numshotguncarts;
            u32 refiretime;
            u32 typechange;
            s32 pausetime60;
            u32 pausechange;
            u32 nextprevchange;
            struct coord posstart;
            f32 rotxstart;
            struct coord posend;
            f32 rotxend;
            struct coord posoffset;
            f32 rotxoffset;
            Mtxf posrotmtx;
            s32 useposrot;
            struct coord damppos;
            struct coord damplook;
            struct coord dampup;
            struct coord damppossum;
            struct coord damplooksum;
            struct coord dampupsum;
            f32 blendpos[4][3];
            f32 blendlook[4][3];
            f32 blendup[4][3];
            s32 curblendpos;
            f32 dampt;
            f32 blendscale;
            f32 blendscale1;
            s32 sideflag;
            struct coord adjustdamp;
            struct coord adjustpos;
            f32 xshift;
            struct coord aimpos;
            struct audiohandle *audiohandle2;
            struct audiohandle *audiohandle3;
            s32 allowshootframe;
            s32 lastshootframe60;
            struct beam beam;
            f32 noiseradius;
            u32 fingerroty;
            u32 slidetrans;
            u32 slideinc;
            struct weaponobj *rocket;
            s32 firedrocket;
            s32 loadedammo[2];
            s32 clipsizes[2];
            f32 angledamper;
            u32 lastrotangx;
            u32 lastrotangy;
            f32 matmot1;
            f32 matmot2;
            f32 matmot3;
            u32 unk0880;
            u32 unk0884;
            f32 loadslide;
            f32 upgrademult[12];
            f32 finalmult[12];
            Mtxf cammtx;
            Mtxf posmtx;
            Mtxf prevmtx;
            struct coord muzzlepos;
            f32 muzzlez;
            struct model unk09bc;
            struct anim anim;
            u32 unk0a6c[32];
            u32 handsavedata[32];
            u32 unk0b6c;
            u32 unk0b70;
            u32 unk0b74;
            u32 unk0b78;
            u32 unk0b7c;
            u32 unk0b80;
            u32 unk0b84;
            u32 unk0b88;
            struct anim *unk0b8c;
            s32 burstbullets;
            struct coord hitpos;
            u32 unk0ba0;
            u32 unk0ba4;
            u32 unk0ba8;
            u32 unk0bac;
            u32 unk0bb0;
            u32 unk0bb4;
            u32 unk0bb8;
            u32 unk0bbc;
            u32 unk0bc0;
            u32 unk0bc4;
            u32 unk0bc8;
            u32 unk0bcc;
            u32 unk0bd0;
            u32 unk0bd4;
            u32 unk0bd8;
            u32 unk0bdc;
            u32 unk0be0;
            u32 unk0be4;
            u32 unk0be8;
            u32 unk0bec;
            u32 unk0bf0;
            u32 unk0bf4;
            u32 unk0bf8;
            u32 unk0bfc;
            u32 unk0c00;
            u32 unk0c04;
            u32 unk0c08;
            u32 unk0c0c;
            u32 unk0c10;
            u8 lastdirvalid;
            struct coord lastshootdir;
            struct coord lastshootpos;
            s32 shotstotake;
            f32 shotremainder;
            u32 hitspershot;
            s32 state;
            s32 stateminor;
            u32 stateflags;
            u32 statemode;
            s32 stateframes;
            s32 statecycles;
            s32 statelastframe;
            Mtxf muzzlemat;
            f32 gs_float1;
            f32 gs_float2;
            f32 gs_float3;
            f32 gs_float4;
            s32 gs_int1;
            s32 gs_int2;
            s32 gs_int3;
            s32 gs_int4;
            s32 animload;
            s32 animframeinc;
            u32 animframeincfreal;
            u32 animmode;
            u8 unk0cc8_01 : 1;
            u8 unk0cc8_02 : 1;
            u8 unk0cc8_03 : 1;
            u8 unk0cc8_04 : 1;
            u8 unk0cc8_05 : 1;
            u8 unk0cc8_06 : 1;
            u8 unk0cc8_07 : 1;
            u8 unk0cc8_08 : 1;
            u8 animloopcount;
            f32 crosspos[2];
            f32 guncrosspossum[2];
            u32 statejob;
            s32 statevar1;
            s32 attacktype;
            struct guncmd *unk0ce8;
            u32 unk0cec;
            struct coord unk0cf0;
            struct coord unk0cfc;
            f32 ganstarot;
            s16 primetimer;
            u8 unk0d0e_00 : 4;
            u8 unk0d0e_04 : 3;
            u8 unk0d0e_07 : 1;
            u8 unk0d0f_00 : 2;
            u8 unk0d0f_02 : 1;
            u8 unk0d0f_03 : 1;
            u8 unk0d0f_04 : 4;
            u32 unk0d10;
            u32 unk0d14;
            u32 unk0d18;
            u32 unk0d1c;
            u32 unk0d20;
            u32 unk0d24;
            u32 unk0d28;
            u32 unk0d2c;
            u32 unk0d30;
            u32 unk0d34;
            u32 unk0d38;
            u32 unk0d3c;
            u32 unk0d40;
            u32 unk0d44;
            u32 unk0d48;
            u32 unk0d4c;
            u32 unk0d50;
            u32 unk0d54;
            u32 unk0d58;
            u32 unk0d5c;
            u32 unk0d60;
            u32 unk0d64;
            u32 unk0d68;
            u32 unk0d6c;
            u32 unk0d70;
            u16 gunroundsspent[4];
            u32 ispare1;
            struct guncmd *unk0d80;
            struct audiohandle *audiohandle;
            u32 ispare4;
            u32 ispare5;
            u32 ispare6;
            u32 ispare7;
            u32 ispare8;
            u32 ispare9;
            u32 ispare10;
            f32 fspare1;
            f32 fspare2;
            f32 fspare3;
            f32 fspare4;
            f32 fspare5;
            f32 gunsmokepoint;
            u32 fspare7;
            u32 fspare8;
            struct abmag abmag;
            u32 unk0dcc;
            u32 unk0dd0;
            s32 unk0dd4;
            u32 unk0dd8;
};
struct player0610 {
          u16 unk00;
          u16 unk02;
          u16 unk04;
          u16 unk06;
          u16 unk08;
          u16 unk0a;
          u16 unk0c;
          u16 unk0e;
};
struct gunctrl {
            s8 weaponnum;
            s8 prevweaponnum;
            s8 switchtoweaponnum;
            u8 dualwielding : 1;
            u8 unk1583_01 : 1;
            u8 invertgunfunc : 1;
            u8 gangsta : 1;
            u8 unk1583_04 : 1;
            u8 wantammo : 1;
            u8 unk1583_06 : 1;
            u8 passivemode : 1;
            s32 gunmemnew;
            s32 gunmemtype;
            void *unk158c;
            u32 unk1590;
            struct modelfiledata *unk1594;
            s32 unk1598;
            u16 handfilenum;
            s32 unk15a0;
            s32 unk15a4;
            u32 unk15a8;
            u32 unk15ac;
            u8 unk15b0;
            u8 unk15b1;
            u32 unk15b4;
            u32 unk15b8;
            u32 unk15bc;
            u32 unk15c0;
            u32 unk15c4;
            u32 unk15c8;
            u32 unk15cc;
            u32 unk15d0;
            u32 unk15d4;
            u32 unk15d8;
            struct abmag abmag;
            s8 ammotypes[2];
            u8 action;
            u8 fnfader;
            u8 upgradewant;
            s8 lastmag;
            u8 gunmemowner;
            s8 gunlocktimer;
            u16 curfnstr;
            u8 fnstrtimer;
            u8 guntypetimer;
            u8 guntypefader;
            u16 curgunstr;
            u8 paddingashdown;
};
struct player {
            s32 cameramode;
            struct coord memcampos;
            u16 visionmode;
            s32 memcamroom;
            struct coord eraserpos;
            f32 eraserpropdist;
            u32 eraserbgdist;
            u32 eraserdepth;
            s32 isfalling;
            s32 fallstart;
            struct coord globaldrawworldoffset;
            struct coord globaldrawcameraoffset;
            struct coord globaldrawworldbgoffset;
            Mtx *matrix5c;
            Mtx *matrix60;
            Mtxf *matrix64;
            Mtxf *matrix68;
            Mtxf *matrix6c;
            f32 sumground;
            f32 vv_manground;
            f32 vv_ground;
            struct coord bdeltapos;
            f32 sumcrouch;
            f32 crouchheight;
            s32 crouchtime240;
            f32 crouchfall;
            s32 swaypos;
            f32 swayoffset;
            f32 swaytarget;
            f32 swayoffset0;
            f32 swayoffset2;
            s32 crouchpos;
            s32 autocrouchpos;
            f32 crouchoffset;
            f32 crouchspeed;
            struct prop *prop;
            s32 bondperimenabled;
            u32 devicesactive;
            s32 badrockettime;
            u32 gunspeed;
            s32 bondactivateorreload;
            struct model *model00d4;
            s32 isdead;
            f32 bondhealth;
            struct audiohandle *chokehandle;
            f32 oldhealth;
            f32 oldarmour;
            f32 apparenthealth;
            f32 apparentarmour;
            f32 damageshowtime;
            f32 healthshowtime;
            u32 healthshowmode;
            s32 docentreupdown;
            u32 lastupdown60;
            s32 prevupdown;
            s32 movecentrerelease;
            s32 lookaheadcentreenabled;
            s32 automovecentreenabled;
            s32 fastmovecentreenabled;
            s32 automovecentre;
            s32 insightaimmode;
            s32 autoyaimenabled;
            f32 autoaimy;
            struct prop *autoyaimprop;
            s32 autoyaimtime60;
            s32 autoxaimenabled;
            f32 autoaimx;
            struct prop *autoxaimprop;
            s32 autoxaimtime60;
            f32 vv_theta;
            f32 speedtheta;
            f32 vv_costheta;
            f32 vv_sintheta;
            f32 vv_verta;
            f32 vv_verta360;
            f32 speedverta;
            f32 vv_cosverta;
            f32 vv_sinverta;
            f32 speedsideways;
            f32 speedstrafe;
            f32 speedforwards;
            f32 speedboost;
            s32 speedmaxtime60;
            f32 bondshotspeed[3];
            f32 bondfadetime60;
            f32 bondfadetimemax60;
            f32 bondfadefracold;
            f32 bondfadefracnew;
            f32 bondbreathing;
            s32 activatetimelast;
            s32 activatetimethis;
            struct coord moveinitspeed;
            s32 bondmovemode;
            f32 gunextraaimx;
            f32 gunextraaimy;
            s32 playwatchup;
            struct anim unk01c0;
            s16 invdowntime;
            s16 usedowntime;
            u8 activemenumode;
            u8 ecol_1;
            u8 ecol_2;
            u8 ecol_3;
            s32 erasertime;
            f32 autoeraserdist;
            struct prop *autoerasertarget;
            s32 aimtaptime;
            struct weaponobj *slayerrocket;
            s32 eyesshut;
            f32 eyesshutfrac;
            u8 epcol_0;
            u8 epcol_1;
            u8 epcol_2;
            u8 flashbang;
            s32 waitforzrelease;
            f32 shieldshowrot;
            u32 shieldshowrnd;
            f32 shieldshowtime;
            s16 bondprevrooms[8];
            f32 liftground;
            struct prop *lift;
            f32 ladderupdown;
            struct coord laddernormal;
            s32 onladder;
            s32 inlift;
            struct coord posdie;
            struct coord bonddampeyesum;
            struct coord bonddampeye;
            s32 colourscreenred;
            s32 colourscreengreen;
            s32 colourscreenblue;
            f32 colourscreenfrac;
            f32 colourfadetime60;
            f32 colourfadetimemax60;
            s32 colourfaderedold;
            s32 colourfaderednew;
            s32 colourfadegreenold;
            s32 colourfadegreennew;
            s32 colourfadeblueold;
            s32 colourfadebluenew;
            f32 colourfadefracold;
            f32 colourfadefracnew;
            struct coord bondprevpos;
            f32 thetadie;
            f32 vertadie;
            u32 bondtype;
            s32 startnewbonddie;
            s32 redbloodfinished;
            s32 deathanimfinished;
            s32 controldef;
            struct playerbond bonddie;
            struct playerbond bond2;
            s32 resetheadpos;
            s32 resetheadrot;
            s32 resetheadtick;
            s32 headanim;
            f32 headdamp;
            s32 headwalkingtime60;
            f32 headamplitude;
            f32 sideamplitude;
            struct coord headpos;
            struct coord headlook;
            struct coord headup;
            struct coord headpossum;
            struct coord headlooksum;
            struct coord headupsum;
            struct coord headbodyoffset;
            f32 standheight;
            struct coord standbodyoffset;
            f32 standfrac;
            struct coord standlook[2];
            struct coord standup[2];
            s32 standcnt;
            struct model model;
            struct eyespy *eyespy;
            u8 *unk0484;
            u8 *unk0488;
            s32 aborted;
            s32 eyespydarts;
            union modelrwdata *bondheadsave[30];
            u32 unk050c;
            Mtxf bondheadmatrices[4];
            struct player0610 viewport[2];
            s16 viewwidth;
            s16 viewheight;
            s16 viewleft;
            s16 viewtop;
            struct hand hands[2];
 struct gunctrl gunctrl;
            f32 gunposamplitude;
            f32 gunxamplitude;
            s32 doautoselect;
            u32 playertriggeron;
            u32 playertriggerprev;
            s32 playertrigtime240;
            s32 curguntofire;
            u8 gunshadecol[4];
            s16 resetshadecol;
            u16 floorcol;
            u16 floorflags;
            u8 floortype;
            u32 aimtype;
            struct threat lookingatprop;
            struct threat cmpfollowprops[4];
            f32 crosspos[2];
            f32 crosspossum[2];
            f32 guncrossdamp;
            f32 crosspos2[2];
            f32 crosssum2[2];
            f32 gunaimdamp;
            struct coord aimangle;
            Mtxf aimanglemat;
            s32 copiedgoldeneye;
            u32 gunammooff;
            f32 gunsync;
            f32 syncchange;
            f32 synccount;
            s32 syncoffset;
            f32 cyclesum;
            f32 gunampsum;
            f32 gunzoomfovs[3];
            s32 lastroomforoffset;
            f32 c_screenwidth;
            f32 c_screenheight;
            f32 c_screenleft;
            f32 c_screentop;
            f32 c_perspnear;
            f32 c_perspfovy;
            f32 c_perspaspect;
            f32 c_halfwidth;
            f32 c_halfheight;
            f32 c_scalex;
            f32 c_scaley;
            f32 c_recipscalex;
            f32 c_recipscaley;
            void *unk1738;
            Mtx *unk173c;
            Mtxf *matrix1740;
            s32 c_viewfmdynticknum;
            u32 unk1748;
            Mtxf *unk174c;
            void *unk1750;
            Mtxf *unk1754;
            Mtx *unk1758;
            void *unk175c;
            Mtxf *prev1740;
            s32 c_prevviewfmdynticknum;
            Mtxf *unk1768;
            f32 c_scalelod60;
            f32 c_scalelod;
            f32 c_lodscalez;
            u32 c_lodscalezu32;
            struct coord c_cameratopnorm;
            struct coord c_cameraleftnorm;
            f32 screenxminf;
            f32 screenyminf;
            f32 screenxmaxf;
            f32 screenymaxf;
            u32 gunsightoff;
            s32 ammoheldarr[33];
            u32 unk182c;
            u32 unk1830;
            u32 unk1834;
            u32 unk1838;
            u32 unk183c;
            f32 zoomintime;
            f32 zoomintimemax;
            f32 zoominfovy;
            f32 zoominfovyold;
            f32 zoominfovynew;
            f32 fovy;
            f32 aspect;
            u32 hudmessoff;
            s32 bondmesscnt;
            struct invitem *weapons;
            struct invitem *equipment;
            s32 equipmaxitems;
            u32 equipallguns;
            u32 equipcuritem;
            struct gunheld gunheldarr[10];
            s32 magnetattracttime;
            f32 angleoffset;
            u32 buthist[10];
            u8 buthistindex;
            u8 buthistlen;
            u8 invincible;
            s32 healthdamagetype;
            f32 bondleandown;
            s32 mpmenuon;
            u32 mpmenumode;
            u32 mpquitconfirm;
            u32 mpjoywascentre;
            s32 damagetype;
            u32 deathcount;
            u32 oldcrosspos[2];
            s32 lastkilltime60;
            s32 lastkilltime60_2;
            s32 lastkilltime60_3;
            s32 lastkilltime60_4;
            s32 lifestarttime60;
            u32 killsthislife;
            u32 healthdisplaytime60;
            f32 guncloseroffset;
            f32 shootrotx;
            f32 shootroty;
            char *award1;
            char *award2;
            struct coord chrmuzzlelastpos[2];
            s32 chrmuzzlelast[2];
            f32 healthscale;
            f32 armourscale;
            f32 speedgo;
            s32 sighttimer240;
            s32 crouchoffsetreal;
            s16 floorroom;
            u8 unk19b2;
            u8 dostartnewlife;
            f32 crouchoffsetsmall;
            s32 crouchoffsetrealsmall;
            f32 vv_height;
            f32 vv_headheight;
            f32 vv_eyeheight;
            s32 haschrbody;
            struct tiletype3 periminfo;
            struct tiletype3 perimshoot;
            f32 bondprevtheta;
            struct coord grabbedprevpos;
            f32 grabbedrotoffset;
            struct coord grabbedposoffset;
            s32 grabbeddoextra;
            f32 grabbedrotextra;
            s32 pausemode;
            s32 pausetime60;
            struct coord grabbedposextra;
            f32 grabbedrotextrasum;
            struct coord grabbedposextrasum;
            f32 bondtankthetaspeedsum;
            f32 bondtankverta;
            f32 bondtankvertasum;
            f32 bondturrettheta;
            f32 bondturretthetasum;
            f32 bondturretspeedsum;
            f32 bondturretside;
            f32 bondturretchange;
            s32 bondtankslowtime;
            struct prop *hoverbike;
            struct coord bondvehicleoffset;
            s32 bondvehiclemode;
            f32 bondentert;
            f32 bondentert2;
            u32 bondentertheta;
            u32 bondenterverta;
            struct coord bondenterpos;
            Mtxf bondentermtx;
            struct coord bondenteraim;
            f32 bondonground;
            struct prop *tank;
            struct prop *unk1af0;
            u32 bondonturret;
            s32 walkinitmove;
            struct coord walkinitpos;
            Mtxf walkinitmtx;
            f32 walkinitt;
            f32 walkinitt2;
            struct coord walkinitstart;
            struct prop *grabbedprop;
            f32 bondgrabthetaspeedsum;
            s32 grabstarttime;
            f32 autoaimdamp;
            struct coord bondforcespeed;
            s32 bondtankexplode;
            s32 bondviewlevtime60;
            f32 bondwatchtime60;
            s32 tickdiefinished;
            s32 introanimnum;
            s32 lastsighton;
            u16 targetset[4];
            u8 target;
            f32 speedthetacontrol;
            s32 cam_room;
            s16 autocontrol_aimpad;
            s16 autocontrol_lookup;
            s16 autocontrol_dist;
            s16 autocontrol_walkspeed;
            s32 autocontrol_turnspeed;
            struct coord cam_pos;
            struct coord cam_look;
            struct coord cam_up;
            u32 unk1bd4;
            s32 autocontrol_x;
            s32 autocontrol_y;
            f32 cachedlookahead;
            u16 lookaheadframe;
            u8 numaibuddies;
            u8 aibuddynums[8];
            s32 bondexploding;
            s32 bondnextexplode;
            s32 bondcurexplode;
            u8 teleportstate;
            u8 teleporttime;
            s16 teleportpad;
            u16 teleportcamerapad;
            struct chrdata *commandingaibot;
            u32 training;
            s32 deadtimer;
            s32 coopcanrestart;
            s32 foot;
            f32 footstepdist;
            u32 unk1c1c;
            u32 unk1c20;
            u32 unk1c24;
            s32 unk1c28;
            s32 unk1c2c;
            s32 unk1c30;
            s32 unk1c34;
            s32 unk1c38;
            s32 unk1c3c;
            u32 joybutinhibit;
            struct coord bondextrapos;
            u8 unk1c50_01 : 1;
            u8 disguised : 1;
            u32 devicesinhibit;
            f32 grabbedforcez;
            f32 stealhealth;
            s32 unk1c60;
            s32 unk1c64;
            u32 unk1c68;
            u32 unk1c6c;
};
struct ailist {
 u8 *list;
 s32 id;
};
struct path {
          s32 *pads;
          u8 id;
          u8 flags;
          u16 len;
};
struct covercandidate {
 u64 sqdist;
 s32 covernum;
};
struct coverdefinition {
 struct coord pos;
 struct coord look;
 u16 flags;
};
struct cover {
          struct coord *pos;
          struct coord *look;
          s16 rooms[2];
          u16 flags;
};
struct stagesetup {
          struct waypoint *waypoints;
          struct waygroup *waygroups;
          void *cover;
          s32 *intro;
          u32 *props;
          struct path *paths;
          struct ailist *ailists;
          s8 *padfiledata;
};
struct inventory_menupos {
 f32 x;
 f32 y;
 f32 z;
 f32 rotation;
 f32 size;
};
struct inventory_class {
 f32 zoomfov;
 f32 unk04;
 f32 unk08;
 f32 unk0c;
 f32 unk10;
 f32 unk14;
 u32 unk18;
 u32 flags;
};
struct inventory_typef {
 u8 unk00;
 u16 unk02;
 u8 unk04;
 u16 partnum;
 u16 unk08;
};
struct weaponfunc {
          s32 type;
          u16 name;
          u8 unk06;
          s8 ammoindex;
          struct inventory_menupos *menupos;
          struct guncmd *fire_animation;
          u32 flags;
};
struct weaponfunc_shoot {
 struct weaponfunc base;
          struct inventory_menupos *unk14;
          s8 unk18;
          f32 damage;
          f32 spread;
          s8 unk24;
          s8 unk25;
          s8 unk26;
          s8 unk27;
          f32 recoil;
          f32 unk2c;
          f32 unk30;
          f32 strength;
          u8 unk38;
          u16 shootsound;
          u8 unk3c;
};
struct weaponfunc_shootsingle {
 struct weaponfunc_shoot base;
};
struct weaponfunc_shootauto {
 struct weaponfunc_shoot base;
          f32 initialfirerate;
          f32 maxfirerate;
          f32 *unk48;
          f32 *unk4c;
          s8 unk50;
          s8 unk51;
};
struct weaponfunc_shootprojectile {
 struct weaponfunc_shoot base;
          s32 projectilemodelnum;
          u32 unk44;
          u32 unk48;
          u32 unk4c;
          u32 unk50;
          u32 unk54;
          u32 unk58;
          u32 unk5c;
          u32 unk60;
};
struct weaponfunc_throw {
 struct weaponfunc base;
          s32 projectilemodelnum;
          s16 activatetime60;
          s32 recoverytime60;
          f32 damage;
};
struct weaponfunc_close {
 struct weaponfunc base;
          f32 damage;
          f32 range;
          u32 unk1c;
          u32 unk20;
          u32 unk24;
          u32 unk28;
          u32 unk2c;
          u32 unk30;
          u32 unk34;
          u32 unk38;
          u32 unk3c;
          u32 unk40;
          u32 unk44;
          u32 unk48;
};
struct weaponfunc_special {
 struct weaponfunc base;
          s32 specialfunc;
          s32 unk18;
          u32 unk1c;
};
struct weaponfunc_device {
 struct weaponfunc base;
          u32 device;
};
struct inventory_ammo {
 u32 type;
 u32 casingeject;
 s16 clipsize;
 struct guncmd *reload_animation;
 u8 flags;
};
struct modelpartvisibility {
 u8 part;
 u8 visible;
};
struct weapon {
          u16 hi_model;
          u16 lo_model;
          struct guncmd *equip_animation;
          struct guncmd *unequip_animation;
          struct guncmd *pritosec_animation;
          struct guncmd *sectopri_animation;
          void *functions[2];
          struct inventory_ammo *ammos[2];
          struct inventory_class *eptr;
          f32 sway;
          f32 leftright;
          f32 updown;
          f32 frontback;
          f32 unk38;
          struct inventory_typef *fptr;
          struct modelpartvisibility *partvisibility;
          u16 shortname;
          u16 name;
          u16 manufacturer;
          u16 description;
          u32 flags;
};
struct cutscene {
 s16 stage;
 s16 mission;
 u32 scene;
 u16 name;
};
struct cheat {
 u16 nametextid;
 u16 time;
 u8 stage_index;
 u8 difficulty;
 u8 flags;
};
struct headorbody {
          u16 ismale : 1;
          u16 unk00_01 : 1;
          u16 canvaryheight : 1;
          u16 type : 3;
          u16 height : 8;
          u16 filenum;
          f32 scale;
          f32 animscale;
          void *filedata;
          u16 handfilenum;
};
struct stagetableentry {
          s16 id;
          u8 light_type;
          u8 light_alpha;
          u8 light_width;
          u8 light_height;
          u16 unk06;
          u16 bgfileid;
          u16 tilefileid;
          u16 padsfileid;
          u16 setupfileid;
          u16 mpsetupfileid;
          f32 unk14;
          f32 unk18;
          f32 unk1c;
          u16 unk20;
          u8 unk22;
          s8 unk23;
          u32 unk24;
          u32 unk28;
          s16 unk2c;
          u16 unk2e;
          u16 unk30;
          f32 unk34;
};
struct mpweaponset {
          u16 name;
          u8 slots[6];
          u8 requirefeatures[4];
          u8 unk0c;
          u8 unk0d;
          u8 unk0e;
          u8 unk0f;
          u8 unk10;
          u8 unk11;
};
struct mphead {
 s16 headnum;
 u8 requirefeature;
};
struct mpsimulanttype {
          u8 type;
          u8 difficulty;
          u16 name;
          u16 body;
          u8 requirefeature;
};
struct mpbody {
 s16 bodynum;
 s16 name;
 s16 headnum;
 u8 requirefeature;
};
struct mptrack {
 u16 musicnum : 7;
 u16 duration : 9;
 s16 name;
 s16 unlockstage;
};
struct stageoverviewentry {
          u32 stagenum;
          u8 unk04;
          u16 name1;
          u16 name2;
          u16 name3;
};
struct stagemusic {
 s16 stagenum;
 s16 primarytrack;
 s16 ambienttrack;
 s16 xtrack;
};
struct mission {
 u32 first_stage_index;
 u16 name;
};
struct var800a6600 {
 u32 unk00;
 u32 unk04;
 u32 unk08;
 u32 unk0c;
 f32 unk10;
 f32 unk14;
};
struct var80081058 {
          s32 fogmin;
          s32 fogmax;
          u8 sky_r;
          u8 sky_g;
          u8 sky_b;
          u8 num_suns;
          struct sun *sun;
          u8 clouds_enabled;
          f32 clouds_scale;
          u16 unk18;
          f32 clouds_r;
          f32 clouds_g;
          f32 clouds_b;
          u8 water_enabled;
          f32 water_scale;
          u16 water_type;
          f32 water_r;
          f32 water_g;
          f32 water_b;
          f32 unk40;
          f32 skyredfrac;
          f32 skygreenfrac;
          f32 skybluefrac;
};
struct sun {
 u8 lens_flare;
 u8 red;
 u8 green;
 u8 blue;
 f32 x;
 f32 y;
 f32 z;
 u16 texture_size;
 u16 orb_size;
};
struct skytype1 {
          s16 stage;
          s16 near;
          s16 far;
          s16 unk06;
          s16 unk08;
          s16 unk0a;
          s16 fogmin;
          s16 fogmax;
          u8 sky_r;
          u8 sky_g;
          u8 sky_b;
          u8 num_suns;
          struct sun *sun;
          u8 clouds_enabled;
          s16 clouds_scale;
          u8 unk1c;
          u8 clouds_r;
          u8 clouds_g;
          u8 clouds_b;
          u8 water_enabled;
          u8 unk21;
          s16 water_scale;
          u8 water_type;
          u8 water_r;
          u8 water_g;
          u8 water_b;
          u8 unk28;
};
struct skytype2 {
          s32 stage;
          s16 near;
          s16 far;
          s16 unk08;
          s16 unk0a;
          s16 unk0c;
          u8 sky_r;
          u8 sky_g;
          u8 sky_b;
          u8 num_suns;
          struct sun *sun;
          u8 clouds_enabled;
          u8 clouds_r;
          u8 clouds_g;
          u8 clouds_b;
          f32 clouds_scale;
          s16 unk20;
          u8 water_enabled;
          u8 water_r;
          u8 water_g;
          u8 water_b;
          f32 water_scale;
          s16 water_type;
          f32 unk30;
          u32 unk34;
};
struct hoverprop {
 u32 fileid;
 s32 y_offset;
 u16 size;
};
struct menuitemdata_controller {
 u8 textfadetimer;
 u8 contfadetimer;
 u8 curmode;
 u8 controlgroup;
 s8 prevmode;
};
struct menuitemdata_dropdown {
 u16 unk00;
 u16 unk02;
 s16 unk04;
 u16 unk06;
 u16 unk08;
 u16 unk0a;
 s16 scrolloffset;
 u16 unk0e;
};
struct menuitemdata_keyboard {
 char string[11];
 s8 col;
 s8 row;
 u8 capslock : 1;
 u8 capseffective : 1;
};
struct menuitemdata_list {
 s16 unk00;
 s16 unk02;
 s16 unk04;
 s16 unk06;
};
struct menuitemdata_marquee {
 u16 totalmoved;
 u16 sum;
 u16 unk04;
 u16 unk06;
};
struct menuitemdata_ranking {
 s16 scrolloffset;
};
struct menuitemdata_scrollable {
 s16 unk00;
 s16 unk02;
 s16 unk04;
 s16 unk06;
 s16 unk08;
};
struct menuitemdata_slider {
 s16 unk00;
};
union menuitemdata {
 struct menuitemdata_controller controller;
 struct menuitemdata_dropdown dropdown;
 struct menuitemdata_keyboard keyboard;
 struct menuitemdata_list list;
 struct menuitemdata_marquee marquee;
 struct menuitemdata_ranking ranking;
 struct menuitemdata_scrollable scrollable;
 struct menuitemdata_slider slider;
};
struct handlerdata_carousel {
 s32 value;
 u32 unk04;
};
struct handlerdata_checkbox {
 u32 value;
};
struct handlerdata_dropdown {
 u32 value;
 u32 unk04;
};
struct handlerdata_keyboard {
 char *string;
};
struct handlerdata_label {
 u32 colour1;
 u32 colour2;
};
struct handlerdata_list {
 u32 value;
 u32 unk04;
 u32 groupstartindex;
 u32 unk0c;
};
struct handlerdata_list2 {
 s16 unk00;
 s16 unk02;
 u32 unk04;
 u32 unk08;
 u32 unk0c;
};
struct handlerdata_slider {
 u32 value;
 char *label;
};
struct menuitemrenderdata {
 s32 x;
 s32 y;
 s32 width;
 u32 colour;
 u8 unk10;
};
struct handlerdata_type19 {
 Gfx *gdl;
 struct menuitemrenderdata *renderdata1;
 struct menuitemrenderdata *renderdata2;
};
struct handlerdata_dialog1 {
 u32 preventclose;
};
struct handlerdata_dialog2 {
 struct menuinputs *inputs;
};
union handlerdata {
 struct handlerdata_carousel carousel;
 struct handlerdata_checkbox checkbox;
 struct handlerdata_list list;
 struct handlerdata_list2 list2;
 struct handlerdata_dropdown dropdown;
 struct handlerdata_keyboard keyboard;
 struct handlerdata_label label;
 struct handlerdata_slider slider;
 struct handlerdata_type19 type19;
 struct handlerdata_dialog1 dialog1;
 struct handlerdata_dialog2 dialog2;
};
struct menuitem {
 u8 type;
 u8 param;
 u32 param1;
 u32 param2;
 s32 param3;
 union {
  s32 (*handler)(s32 operation, struct menuitem *item, union handlerdata *data);
  void (*handlervoid)(s32 operation, struct menuitem *item, union handlerdata *data);
 };
};
struct menudialog {
 u8 type;
 u32 title;
 struct menuitem *items;
 s32 (*handler)(s32 operation, struct menudialog *dialog, union handlerdata *data);
 u32 unk10;
 struct menudialog *nextsibling;
};
struct twowords {
 u32 unk00;
 u32 unk04;
};
struct surfacetype {
 u16 *sounds;
 u8 *unk04;
 s16 numsounds;
 s16 num04;
};
union soundnumhack {
 u16 packed;
 struct {
  u16 isruss : 1;
  u16 id : 15;
 } bits;
 struct {
  u16 isruss : 1;
  u16 unk02 : 2;
  u16 id : 13;
 } bits2;
 struct {
  u16 isruss : 1;
  u16 unk02 : 2;
  u16 unk04 : 2;
  u16 id : 11;
 } bits3;
};
struct audiorussmapping {
 s16 soundnum;
 u16 audioconfig_index;
};
struct audioconfig {
 f32 unk00;
 f32 unk04;
 f32 unk08;
 f32 unk0c;
 s32 unk10;
 s32 unk14;
 u32 unk18;
 u32 flags;
};
struct bootbufferthingdeep {
 u16 unk00;
 u16 unk02;
 u16 unk04;
 u16 unk06;
 u16 *unk08;
 union {
  u16 *u16p;
  struct {
   u16 u16_1;
   u16 u16_2;
  };
 } unk0c;
 u32 unk10;
};
struct bootbufferthing {
 struct bootbufferthingdeep unk00[120];
};
struct credit {
 u8 more : 1;
 u8 notranin : 1;
 u8 notranout : 1;
 u8 durationindex : 2;
 u8 style;
 u16 text1;
 u16 text2;
};
struct sparktype {
 u16 unk00;
 s16 unk02;
 u16 unk04;
 u16 unk06;
 u16 unk08;
 u16 unk0a;
 f32 weight;
 u16 maxage;
 u16 unk12;
 u16 unk14;
 u32 unk18;
 u32 unk1c;
 u32 unk20;
 f32 decel;
};
struct sparkgroup {
 s32 type;
 s32 numsparks;
 s32 age;
 s32 startindex;
 u32 unk10;
 u32 unk14;
 u32 unk18;
 u32 unk1c;
 u32 unk20;
};
struct spark {
 struct coord pos;
 struct coord speed;
 s32 ttl;
};
struct roombitfield {
 u8 prevop : 4;
 u8 b : 4;
};
struct screenbox {
 s16 xmin;
 s16 ymin;
 s16 xmax;
 s16 ymax;
};
struct room14 {
          u32 unk00;
          u32 unk04;
          u32 unk08;
          u32 unk0c;
};
struct room {
          u16 flags;
          s16 unk02;
          u8 portalrecursioncount;
          s8 numportals;
          u8 unk06;
          u8 unk07;
          s8 numlights;
          u8 numwaypoints;
          u16 lightindex;
          u16 firstwaypoint;
          s16 roomportallistoffset;
          s16 unk10;
          struct room14 *unk14;
          f32 bbmin[3];
          f32 bbmax[3];
          struct coord centre;
          f32 radius;
          u32 unk40;
          void *unk44;
          u8 unk48;
          u8 unk49;
          u8 unk4a;
          u8 unk4b;
          u8 unk4c;
          u8 unk4d;
          struct roombitfield bitfield;
          s16 brightness;
          s16 unk52;
          u16 unk54;
          u32 unk58;
          f32 unk5c;
          f32 unk60;
          f32 unk64;
          f32 unk68;
          f32 unk6c;
          f32 unk70;
          u32 unk74;
          u32 unk78;
          u32 unk7c;
          s32 unk80;
          u32 unk84;
          u32 unk88;
};
struct fireslotthing {
          u8 unk00;
          s8 unk01;
          struct beam *beam;
          s32 unk08;
          f32 unk0c;
          f32 unk10;
          f32 unk14;
          u32 unk18;
          u32 unk1c;
};
struct fireslot {
          s32 endlvframe;
 struct audiohandle *unk04nb;
 struct audiohandle *unk08nb;
          struct beam beam;
};
struct menulayer {
 struct menuframe *siblings[5];
 s8 numsiblings;
 s8 cursibling;
};
struct menuframe {
 struct menudialog *dialog;
 u8 unk04;
 u8 unk05;
 u16 unk06;
 struct menuitem *focuseditem;
          s32 dimmed;
          u32 unk10;
          s32 unk14;
          s32 y;
          s32 unk1c;
          s32 height;
          u32 unk24;
          u32 unk28;
          s32 unk2c;
          u32 unk30;
          u32 unk34;
          u32 unk38;
          u8 type;
          u8 type2;
          f32 transitiontimer;
          u32 colourweight;
          f32 unk48;
          f32 unk4c;
          f32 unk50;
          u32 unk54;
          u32 unk58;
          u32 unk5c;
          u8 unk60;
          u32 unk64;
          u32 unk68;
          u8 unk6c;
          s8 unk6d;
          u8 unk6e;
};
struct menudfc {
 struct menuitem *item;
 f32 unk04;
};
struct menudata_endscreen {
 u32 unke1c;
 u32 cheatinfo;
 s32 isfirstcompletion;
 u32 unke28;
 u32 stageindex;
};
struct menudata_main {
 u32 unke1c;
 u32 controlmode;
 u32 mpindex;
 u32 unke28;
 u32 unke2c;
};
struct menudata_mpsetup {
 u32 slotindex;
 u32 slotcount;
 u32 unke24;
};
struct menudata_mppause {
 u32 unke1c;
 u32 unke20;
 u32 unke24;
 s32 weaponnum;
};
struct menudata_mpend {
 u32 unke1c;
};
struct menudata_filemgr {
 u32 filetypeplusone;
 u32 device;
 u32 unke24;
 u32 isdeletingforsave;
 u32 unke2c;
};
struct menudata_pak {
 u32 unke1c;
 u32 device;
 u32 unke24;
 u32 noteindex;
};
struct menudata_main4mb {
 u32 slotindex;
};
struct menudata_train {
 u32 unke1c;
 struct mpconfigfull *mpconfig;
 u32 unke24;
 u32 weaponnum;
};
struct menu {
 struct menuframe frames[10];
           s16 numframes;
           struct menulayer layers[6];
           s16 depth;
           struct menuframe *curframe;
           s16 unk4fc[1][2];
           u32 unk500;
           u32 unk504;
           u32 unk508;
           u32 unk50c;
           u32 unk510;
           u32 unk514;
           u32 unk518;
           u32 unk51c;
           u32 unk520;
           u32 unk524;
           u32 unk528;
           u32 unk52c;
           u32 unk530;
           u32 unk534;
           u32 unk538;
           u32 unk53c;
           u32 unk540;
           u32 unk544;
           u32 unk548;
           u32 unk54c;
           u32 unk550;
           u32 unk554;
           u32 unk558;
           u32 unk55c;
           u32 unk560;
           u32 unk564;
           u32 unk568;
           u32 unk56c;
           u32 unk570;
           u32 unk574;
           u32 unk578;
           u32 unk57c;
           u32 unk580;
           u32 unk584;
           u32 unk588;
           u32 unk58c;
           u32 unk590;
           u32 unk594;
           u32 unk598;
           u32 unk59c;
           u32 unk5a0;
           u32 unk5a4;
           u32 unk5a8;
           u32 unk5ac;
           u32 unk5b0;
           u32 unk5b4;
           u32 unk5b8;
           u32 unk5bc;
           u32 unk5c0;
           u32 unk5c4;
           u32 unk5c8;
           u32 unk5cc;
           u32 unk5d0;
           u32 unk5d4;
           u32 unk5d8;
           u32 unk5dc;
           u32 unk5e0;
           u32 unk5e4;
           u32 unk5e8;
           u32 unk5ec;
           u32 unk5f0;
           u32 unk5f4;
           u32 unk5f8;
           u32 unk5fc;
           u32 unk600;
           u32 unk604;
           u32 unk608;
           u32 unk60c;
           u32 unk610;
           u32 unk614;
           u32 unk618;
           u32 unk61c;
           u32 unk620;
           u32 unk624;
           u32 unk628;
           u32 unk62c;
           u32 unk630;
           u32 unk634;
           u32 unk638;
           u32 unk63c;
           u32 unk640;
           u32 unk644;
           u32 unk648;
           u32 unk64c;
           u32 unk650;
           u32 unk654;
           u32 unk658;
           s32 unk65c;
           u16 unk660[1][5];
           u32 unk66c;
           u32 unk670;
           u32 unk674;
           u32 unk678;
           u32 unk67c;
           u32 unk680;
           u32 unk684;
           u32 unk688;
           u32 unk68c;
           u32 unk690;
           u32 unk694;
           u32 unk698;
           u32 unk69c;
           u32 unk6a0;
           u32 unk6a4;
           u32 unk6a8;
           u32 unk6ac;
           u32 unk6b0;
           u32 unk6b4;
           u32 unk6b8;
           u32 unk6bc;
           u32 unk6c0;
           u32 unk6c4;
           u32 unk6c8;
           u32 unk6cc;
           u32 unk6d0;
           u32 unk6d4;
           s32 unk6d8;
           u32 unk6dc;
           u32 unk6e0;
           u32 unk6e4;
           u32 unk6e8;
           u32 unk6ec;
           u32 unk6f0;
           u32 unk6f4;
           u32 unk6f8;
           u32 unk6fc;
           u32 unk700;
           u32 unk704;
           u32 unk708;
           u32 unk70c;
           u32 unk710;
           u32 unk714;
           u32 unk718;
           u32 unk71c;
           u32 unk720;
           u32 unk724;
           u32 unk728;
           u32 unk72c;
           u32 unk730;
           u32 unk734;
           u32 unk738;
           u32 unk73c;
           u32 unk740;
           u32 unk744;
           u32 unk748;
           u32 unk74c;
           u32 unk750;
           u32 unk754;
           u32 unk758;
           u32 unk75c;
           u32 unk760;
           u32 unk764;
           u32 unk768;
           u32 unk76c;
           u32 unk770;
           u32 unk774;
           u32 unk778;
           u32 unk77c;
           u32 unk780;
           u32 unk784;
           u32 unk788;
           u32 unk78c;
           u32 unk790;
           u32 unk794;
           u32 unk798;
           u32 unk79c;
           u32 unk7a0;
           u32 unk7a4;
           u32 unk7a8;
           u32 unk7ac;
           u32 unk7b0;
           u32 unk7b4;
           u32 unk7b8;
           u32 unk7bc;
           u32 unk7c0;
           u32 unk7c4;
           u32 unk7c8;
           u32 unk7cc;
           u32 unk7d0;
           u32 unk7d4;
           u32 unk7d8;
           u32 unk7dc;
           u32 unk7e0;
           u32 unk7e4;
           u32 unk7e8;
           u32 unk7ec;
           u32 unk7f0;
           u32 unk7f4;
           u32 unk7f8;
           u32 unk7fc;
           u32 unk800;
           u32 unk804;
           u32 unk808;
           u32 unk80c;
           u32 unk810;
           u32 unk814;
           u32 unk818;
           s32 unk81c;
           u32 unk820;
           u32 unk824;
           u32 unk828;
           s16 unk82c;
           u32 unk830;
           u32 unk834;
           u16 unk838;
           u8 unk83a;
           u8 playernum;
           u8 unk83c;
           u8 unk83d;
           u8 unk83e;
           u8 unk83f;
           u8 unk840;
 union {
            u32 unk844;
            u8 *mpconfigbuffer;
 };
 union {
            u32 unk848;
            u32 mpconfigbufferlen;
 };
           u32 unk84c;
           u32 unk850;
           u8 unk854;
           u8 unk855;
           u32 unk858;
           u32 unk85c;
           u32 unk860;
           u32 unk864;
           u32 unk868;
           u32 unk86c;
           u32 unk870;
           u32 unk874;
           u32 unk878;
           u32 unk87c;
           u32 unk880;
           u32 unk884;
           u32 unk888;
           u32 unk88c;
           u32 unk890;
           u32 unk894;
           u32 unk898;
           u16 unk89c;
           u32 unk8a0;
           u32 unk8a4;
           u32 unk8a8;
           u32 unk8ac;
           u32 unk8b0;
           u32 unk8b4;
           u32 unk8b8;
           u32 unk8bc;
           u32 unk8c0;
           u32 unk8c4;
           u32 unk8c8;
           u32 unk8cc;
           u32 unk8d0;
           u32 unk8d4;
           u32 unk8d8;
           u32 unk8dc;
           u32 unk8e0;
           u32 unk8e4;
           u32 unk8e8;
           u32 unk8ec;
           u32 unk8f0;
           u32 unk8f4;
           u32 unk8f8;
           u32 unk8fc;
           u32 unk900;
           u32 unk904;
           u32 unk908;
           u32 unk90c;
           u32 unk910;
           u32 unk914;
           u32 unk918;
           u32 unk91c;
           u32 unk920;
           u32 unk924;
           u32 unk928;
           u32 unk92c;
           u32 unk930;
           u32 unk934;
           u32 unk938;
           u32 unk93c;
           u32 unk940;
           u32 unk944;
           u32 unk948;
           u32 unk94c;
           u32 unk950;
           u32 unk954;
           u32 unk958;
           u32 unk95c;
           u32 unk960;
           u32 unk964;
           u32 unk968;
           u32 unk96c;
           u32 unk970;
           u32 unk974;
           u32 unk978;
           u32 unk97c;
           u32 unk980;
           u32 unk984;
           u32 unk988;
           u32 unk98c;
           u32 unk990;
           u32 unk994;
           u32 unk998;
           u32 unk99c;
           u32 unk9a0;
           u32 unk9a4;
           u32 unk9a8;
           u32 unk9ac;
           u32 unk9b0;
           u32 unk9b4;
           u32 unk9b8;
           u32 unk9bc;
           u32 unk9c0;
           u32 unk9c4;
           u32 unk9c8;
           u32 unk9cc;
           u32 unk9d0;
           u32 unk9d4;
           u32 unk9d8;
           u32 unk9dc;
           u32 unk9e0;
           u32 unk9e4;
           u32 unk9e8;
           u32 unk9ec;
           u32 unk9f0;
           u32 unk9f4;
           u32 unk9f8;
           u32 unk9fc;
           u32 unka00;
           u32 unka04;
           u32 unka08;
           u32 unka0c;
           u32 unka10;
           u32 unka14;
           u32 unka18;
           u32 unka1c;
           u32 unka20;
           u32 unka24;
           u32 unka28;
           u32 unka2c;
           u32 unka30;
           u32 unka34;
           u32 unka38;
           u32 unka3c;
           u32 unka40;
           u32 unka44;
           u32 unka48;
           u32 unka4c;
           u32 unka50;
           u32 unka54;
           u32 unka58;
           u32 unka5c;
           u32 unka60;
           u32 unka64;
           u32 unka68;
           u32 unka6c;
           u32 unka70;
           u32 unka74;
           u32 unka78;
           u32 unka7c;
           u32 unka80;
           u32 unka84;
           u32 unka88;
           u32 unka8c;
           u32 unka90;
           u32 unka94;
           u32 unka98;
           u32 unka9c;
           u32 unkaa0;
           u32 unkaa4;
           u32 unkaa8;
           u32 unkaac;
           u32 unkab0;
           u32 unkab4;
           u32 unkab8;
           u32 unkabc;
           u32 unkac0;
           u32 unkac4;
           u32 unkac8;
           u32 unkacc;
           u32 unkad0;
           u32 unkad4;
           u32 unkad8;
           u32 unkadc;
           u32 unkae0;
           u32 unkae4;
           u32 unkae8;
           u32 unkaec;
           u32 unkaf0;
           u32 unkaf4;
           u32 unkaf8;
           u32 unkafc;
           u32 unkb00;
           u32 unkb04;
           u32 unkb08;
           u32 unkb0c;
           u32 unkb10;
           u32 unkb14;
           u32 unkb18;
           u32 unkb1c;
           u32 unkb20;
           u32 unkb24;
           u32 unkb28;
           u32 unkb2c;
           u32 unkb30;
           u32 unkb34;
           u32 unkb38;
           u32 unkb3c;
           u32 unkb40;
           u32 unkb44;
           u32 unkb48;
           u32 unkb4c;
           u32 unkb50;
           u32 unkb54;
           u32 unkb58;
           u32 unkb5c;
           u32 unkb60;
           u32 unkb64;
           u32 unkb68;
           u32 unkb6c;
           u32 unkb70;
           u32 unkb74;
           u32 unkb78;
           u32 unkb7c;
           u32 unkb80;
           u32 unkb84;
           u32 unkb88;
           u32 unkb8c;
           u32 unkb90;
           u32 unkb94;
           u32 unkb98;
           u32 unkb9c;
           u32 unkba0;
           u32 unkba4;
           u32 unkba8;
           u32 unkbac;
           u32 unkbb0;
           u32 unkbb4;
           u32 unkbb8;
           u32 unkbbc;
           u32 unkbc0;
           u32 unkbc4;
           u32 unkbc8;
           u32 unkbcc;
           u32 unkbd0;
           u32 unkbd4;
           u32 unkbd8;
           u32 unkbdc;
           u32 unkbe0;
           u32 unkbe4;
           u32 unkbe8;
           u32 unkbec;
           u32 unkbf0;
           u32 unkbf4;
           u32 unkbf8;
           u32 unkbfc;
           u32 unkc00;
           u32 unkc04;
           u32 unkc08;
           u32 unkc0c;
           u32 unkc10;
           u32 unkc14;
           u32 unkc18;
           u32 unkc1c;
           u32 unkc20;
           u32 unkc24;
           u32 unkc28;
           u32 unkc2c;
           u32 unkc30;
           u32 unkc34;
           u32 unkc38;
           u32 unkc3c;
           u32 unkc40;
           u32 unkc44;
           u32 unkc48;
           u32 unkc4c;
           u32 unkc50;
           u32 unkc54;
           u32 unkc58;
           u32 unkc5c;
           u32 unkc60;
           u32 unkc64;
           u32 unkc68;
           u32 unkc6c;
           u32 unkc70;
           u32 unkc74;
           u32 unkc78;
           u32 unkc7c;
           u32 unkc80;
           u32 unkc84;
           u32 unkc88;
           u32 unkc8c;
           u32 unkc90;
           u32 unkc94;
           u32 unkc98;
           u32 unkc9c;
           u32 unkca0;
           u32 unkca4;
           u32 unkca8;
           u32 unkcac;
           u32 unkcb0;
           u32 unkcb4;
           u32 unkcb8;
           u32 unkcbc;
           u32 unkcc0;
           u32 unkcc4;
           u32 unkcc8;
           u32 unkccc;
           u32 unkcd0;
           u32 unkcd4;
           u32 unkcd8;
           u32 unkcdc;
           u32 unkce0;
           u32 unkce4;
           u32 unkce8;
           u32 unkcec;
           u32 unkcf0;
           u32 unkcf4;
           u32 unkcf8;
           u32 unkcfc;
           u32 unkd00;
           u32 unkd04;
           u32 unkd08;
           u32 unkd0c;
           u32 unkd10;
           u32 unkd14;
           u32 unkd18;
           u32 unkd1c;
           u32 unkd20;
           u32 unkd24;
           u32 unkd28;
           u32 unkd2c;
           u32 unkd30;
           u32 unkd34;
           u32 unkd38;
           u32 unkd3c;
           u32 unkd40;
           u32 unkd44;
           u32 unkd48;
           u32 unkd4c;
           f32 unkd50;
           f32 unkd54;
           f32 unkd58;
           f32 unkd5c;
           f32 unkd60;
           f32 unkd64;
           f32 unkd68;
           f32 unkd6c;
           f32 unkd70;
           f32 unkd74;
           f32 unkd78;
           f32 unkd7c;
           f32 unkd80;
           f32 unkd84;
           f32 unkd88;
           f32 unkd8c;
           f32 unkd90;
           f32 unkd94;
           u32 unkd98;
           u32 unkd9c;
           u32 unkda0;
           u32 unkda4;
           u32 unkda8;
           u32 unkdac;
           u32 unkdb0;
           s32 unkdb4;
           s32 unkdb8;
           u32 unkdbc;
           u32 unkdc0;
           u32 unkdc4;
           u32 unkdc8;
           u32 unkdcc;
           u32 unkdd0;
           u32 unkdd4;
           u32 unkdd8;
           u32 unkddc;
           u32 unkde0;
           u32 unkde4;
           u32 unkde8;
           u32 unkdec;
           u32 unkdf0;
           struct modelpartvisibility *partvisibility;
           u8 unkdf8;
           struct menudfc unkdfc[4];
 union {
  struct menudata_endscreen endscreen;
  struct menudata_main main;
  struct menudata_mpsetup mpsetup;
  struct menudata_mppause mppause;
  struct menudata_mpend mpend;
  struct menudata_filemgr filemgr;
  struct menudata_pak pak;
  struct menudata_main4mb main4mb;
  struct menudata_train train;
 } data;
           u32 unke30;
           u16 errno;
           struct savelocation000 *unke38;
           u8 unke3c;
           u8 unke3d;
           u8 unke3e;
           u8 listnum;
           u8 unke40;
           u8 unke41;
           u8 unke42;
           void *unke44;
           u32 unke48;
           u32 unke4c;
           u16 isretryingsave;
           u8 device;
           char unke53[1];
           u32 unke54;
           u32 unke58;
           u32 unke5c;
           u32 unke60;
           u32 unke64;
           u32 unke68;
           s8 savedevice;
};
struct savefile_solo {
          char name[11];
          u8 thumbnail : 5;
          u8 autodifficulty : 3;
          u8 autostageindex;
          u32 totaltime;
          u8 flags[10];
          u16 unk1e;
          u16 besttimes[21][3];
          s32 coopcompletions[3];
          u8 firingrangescores[9];
          u8 weaponsfound[6];
};
struct mpchr {
          char name[1];
          u32 unk04;
          u32 unk08;
          u8 unk0c;
          u8 unk0d;
          u8 unk0e;
          u8 mpheadnum;
          u8 mpbodynum;
          u8 team;
          u32 displayoptions;
          u16 unk18;
          u16 unk1a;
          u16 unk1c;
          s8 placement;
          u32 unk20;
          s16 killcounts[12];
          s16 numdeaths;
          s16 unk3e;
          s16 unk40;
          u16 unk42;
          u8 controlmode;
          s8 contpad1;
          s8 contpad2;
          u8 simtype;
};
struct savelocation_2d8 {
 u32 unk00;
 u16 unk04;
};
struct mpplayer {
          struct mpchr base;
          u16 options;
          struct savelocation_2d8 unk4c;
          u32 kills;
          u32 deaths;
          u32 gamesplayed;
          u32 gameswon;
          u32 gameslost;
          u32 time;
          u32 distance;
          u32 accuracy;
          u32 damagedealt;
          u32 painreceived;
          u32 headshots;
          u32 ammoused;
          u32 accuracymedals;
          u32 headshotmedals;
          u32 killmastermedals;
          u32 survivormedals;
          u8 medals;
          u8 title;
          u8 newtitle;
          u8 gunfuncs[5];
          u8 unk9c;
          u8 handicap;
};
struct mpsim {
          struct mpchr base;
          u8 difficulty;
};
struct missionconfig {
 u8 difficulty : 7;
 u8 pdmode : 1;
          u8 stagenum;
          u8 stageindex;
 u8 iscoop : 1;
 u8 isanti : 1;
          u8 pdmodereaction;
          u8 pdmodehealth;
          u8 pdmodedamage;
          u8 pdmodeaccuracy;
          f32 pdmodereactionf;
          f32 pdmodehealthf;
          f32 pdmodedamagef;
          f32 pdmodeaccuracyf;
};
struct mpsetup {
                char name[12];
                u32 options;
                u8 scenario;
                u8 stagenum;
                u8 timelimit;
                u8 scorelimit;
                u16 teamscorelimit;
                u16 chrslots;
                u8 weapons[6];
                u8 paused;
                struct savelocation_2d8 unk20;
};
struct savefile_setup {
          char teamnames[8][12];
          u8 locktype;
          u8 unk89;
          u8 usingmultipletunes;
          u8 unk8b;
          s8 tracknum;
          u8 multipletracknums[5];
};
struct mpscenario {
 struct menudialog *optionsdialog;
 void (*initfunc)(void);
 s32 (*unk08)(void);
 void (*resetfunc)(void);
 void (*tickfunc)(void);
 void (*unk14)(struct chrdata *chr);
 void *unk18;
 void (*killfunc)(struct mpchr *mpchr, s32 arg1, s32 *score, s32 *arg3);
 Gfx *(*radarfunc)(Gfx *gdl);
 s32 (*radar2func)(Gfx **gdl, struct prop *prop);
 s32 (*highlightfunc)(struct prop *prop, u32 *colour);
 s32 (*spawnfunc)(f32 arg0, struct coord *pos, s16 *rooms, struct prop *prop, f32 *arg4);
 s32 (*maxteamsfunc)(void);
 s32 (*isroomhighlightedfunc)(s16 room);
 void (*unk38)(s16 arg0, s32 *arg1, s32 *arg2, s32 *arg3);
 void *unk3c;
 void (*unk40)(s32 *arg0);
 void (*unk44)(s32 *arg0);
};
struct mpscenariooverview {
 u16 name;
 u16 shortname;
 u8 unlockfeature;
 u8 unk05;
};
struct mparena {
 s16 stagenum;
 u8 requirefeature;
 u16 name;
};
struct savelocation000 {
 u32 unk00;
 u16 unk04;
 char unk06[6];
 u32 unk0c;
 u32 unk10;
 u32 unk14;
};
struct filelist {
           struct savelocation000 unk000[30];
           s16 numfiles;
           s8 spacesfree[4];
           struct savelocation_2d8 unk2d8[4];
           u32 unk2f8;
           u32 unk2fc;
           s8 devicestartindexes[8];
           u8 unk308;
           u8 unk309;
           u8 unk30a;
           u8 filetype;
           u8 outdated;
           u8 unk30d;
           u8 unk30e;
};
struct challenge {
          u16 name;
          s16 confignum;
          u8 availability;
          u8 completions[4];
          u8 unlockfeatures[16];
};
struct scenariodata_cbt {
 u32 unk00;
 u32 unk04;
 u32 unk08;
 u16 unk0c;
 s16 unk0e[1];
};
struct scenariodata_htb {
 u32 unk00;
 struct prop *token;
 struct coord pos;
 u32 unk14;
 s16 nextindex;
 s16 padnums[60];
};
struct htmthing {
 u32 unk00;
 struct prop *prop;
 s16 unk08;
 u8 unk0a;
 u8 unk0b;
};
struct scenariodata_htm {
                s16 nextindex;
                u16 unk002;
                s16 padnums[60];
                struct htmthing unk07c[7];
                s16 unk0d0;
                s16 unk0d2;
                s32 unk0d4;
                u32 unk0d8[12];
                u32 unk108[12];
                u32 unk138;
                struct prop *uplink;
                u32 unk140;
};
struct scenariodata_pac {
 s16 unk00;
 u16 age240;
 s32 victimindex;
 s16 victims[12];
 s16 unk20[12];
 s16 wincounts[12];
};
struct scenariodata_koh {
                u32 unk00;
                s16 occupiedteam;
                s16 unk06;
                s16 unk08;
                s16 hillindex;
                s16 hillcount;
                s16 hillrooms[2];
                s16 hillpads[9];
                struct coord hillpos;
                f32 unk30;
                f32 unk34;
                f32 unk38;
};
struct ctcspawnpadsperteam {
 s16 teamindex;
 s16 numspawnpads;
 s16 spawnpads[6];
};
struct scenariodata_ctc {
          s16 unk00[4];
          s16 teamindexes[4];
          s16 baserooms[4];
          struct ctcspawnpadsperteam spawnpadsperteam[4];
          struct prop *tokens[4];
};
struct scenariodata {
 union {
  struct scenariodata_cbt cbt;
  struct scenariodata_htb htb;
  struct scenariodata_htm htm;
  struct scenariodata_pac pac;
  struct scenariodata_koh koh;
  struct scenariodata_ctc ctc;
 };
};
struct bgportal {
 u16 unk00;
 s16 roomnum1;
 s16 roomnum2;
 u8 flags;
};
struct portalthing {
 u16 unk00;
 u16 unk02;
 u16 unk04;
 s16 unk06;
 u32 unk08;
 u32 unk0c;
};
struct var800a4ccc {
 struct coord coord;
 f32 unk0c;
 f32 unk10;
};
struct trainingdata {
 u8 intraining : 1;
 u8 failed : 1;
 u8 completed : 1;
 u8 finished : 1;
 u8 holographedpc : 1;
 s8 timeleft;
 s32 timetaken;
 struct defaultobj *obj;
 u32 unk0c;
};
struct activemenu {
          s8 screenindex;
          s16 xradius;
          s16 slotwidth;
          s16 selx;
          s16 sely;
          s16 dstx;
          s16 dsty;
          u8 slotnum;
          u8 fromslotnum;
          s32 cornertimer;
          s32 returntimer;
          f32 alphafrac;
          f32 selpulse;
          u8 invindexes[8];
          u8 favourites[8];
          u8 togglefunc;
          u8 numitems;
          u8 allbots;
          u8 prevallbots;
          s8 origscreennum;
};
struct briefing {
 u16 briefingtextnum;
 u16 objectivenames[6];
 u16 objectivedifficulties[6];
 u16 langbank;
};
struct criteria_roomentered {
 u32 unk00;
 u32 pad;
 u32 status;
 struct criteria_roomentered *next;
};
struct criteria_multiroomentered {
 u32 unk00;
 u32 unk04;
 u32 pad;
 u32 status;
 struct criteria_multiroomentered *next;
};
struct criteria_holograph {
 u32 unk00;
 u32 obj;
 u32 status;
 struct criteria_holograph *next;
};
struct mppreset {
 u16 name;
 u32 confignum;
 u8 requirefeatures[16];
};
struct explosiontype {
          f32 rangeh;
          f32 rangev;
          f32 changerateh;
          f32 changeratev;
          f32 innersize;
          f32 blastradius;
          f32 damageradius;
          s16 duration;
          u16 propagationrate;
          f32 flarespeed;
          u8 smoketype;
          u16 sound;
          f32 damage;
};
struct explosionpart {
 struct coord pos;
 u32 size;
 u32 rot;
 s16 frame;
 u8 bb;
};
struct explosionbb {
 struct coord unk00;
 struct coord unk0c;
 s16 room;
 s16 room2;
};
struct explosion {
 struct prop *prop;
 u32 unk04;
 struct explosionpart parts[40];
           s16 age;
           s16 frame60;
           s8 type;
           s8 makescorch;
           s8 owner;
           u8 numbb;
           u32 unk3d0;
           u32 unk3d4;
           u32 unk3d8;
           u32 unk3dc;
           u32 unk3e0;
           u32 unk3e4;
           struct explosionbb bbs[5];
           u32 unk474;
};
struct smoketype {
          s16 duration;
          u16 fadespeed;
          u16 spreadspeed;
          u16 size;
          f32 bgrotatespeed;
          u32 colour;
          f32 fgrotatespeed;
          u16 numclouds;
          f32 unk18;
          f32 unk1c;
          f32 unk20;
};
struct smokepart {
          struct coord pos;
          f32 size;
          u32 rot;
          u32 deltarot;
          u32 offset1;
          u32 offset2;
          u32 alpha;
          u16 count;
};
struct smoke {
           struct prop *prop;
           s16 age;
           u16 type : 7;
           u16 srcispadeffect : 1;
           u16 unk06_08 : 8;
           struct smokepart parts[10];
           void *source;
};
struct textoverride {
          u32 unk00;
          s32 objoffset;
          s32 weapon;
          u32 unk0c;
          u32 unk10;
          u32 unk14;
          u32 unk18;
          u32 activatetextid;
          struct textoverride *next;
          struct defaultobj *obj;
};
struct pakdata {
           OSPfsState notes[16];
           s32 notesinuse[16];
           u16 pagesused;
           u16 pagesfree;
};
struct var800a2380 {
           s32 unk000;
           u32 unk004;
           u32 unk008;
           u32 unk00c;
           u32 unk010;
           u8 unk014;
           struct pakdata pakdata;
           u32 unk25c;
           u32 unk260;
           u32 unk264;
           u32 unk268;
           u32 unk26c;
           u32 unk270;
           u32 unk274;
           u32 unk278;
           u32 unk27c;
           u32 unk280;
           s32 unk284;
           u32 unk288;
           u32 unk28c;
           u32 unk290;
           u32 unk294;
           u32 unk298;
           s32 unk29c;
           u32 unk2a0;
           u32 unk2a4;
           u32 unk2a8;
           u32 unk2ac;
           u32 unk2b0;
           f32 unk2b4;
           u8 unk2b8_01 : 1;
           u8 unk2b8_02 : 1;
           u8 unk2b8_03 : 1;
           u8 unk2b8_04 : 1;
           u8 unk2b8_05 : 1;
           u8 unk2b8_06 : 1;
           u8 unk2b8_07 : 1;
           u8 unk2b9;
           u8 unk2ba;
           u8 unk2bb;
           u8 unk2bc;
           u8 unk2bd;
           u8 unk2be;
           void *unk2c0;
           u8 *unk2c4;
};
struct gecreditsdata {
          u16 text1;
          u16 text2;
          u16 posoffset1;
          u16 alignoffset1;
          u16 posoffset2;
          u16 alignoffset2;
};
struct memorypool_len {
 u32 len;
 struct memorypool *pool;
};
struct memorypool {
          u32 start;
          u32 leftpos;
          u32 rightpos;
          u32 end;
          u32 prevallocation;
};
struct invitem_weap {
 s16 weapon1;
 s16 pickuppad;
};
struct invitem_prop {
 struct prop *prop;
};
struct invitem_dual {
 s32 weapon1;
 s32 weapon2;
};
struct invitem {
          s32 type;
 union {
  struct invitem_weap type_weap;
  struct invitem_prop type_prop;
  struct invitem_dual type_dual;
 };
          struct invitem *next;
          struct invitem *prev;
};
struct chrnumaction {
 s16 chrnum;
 u8 myaction;
};
struct modelstate {
 struct modelfiledata *filedata;
 u16 fileid;
 u16 scale;
};
struct simdifficulty {
 u8 unk00;
 f32 unk04;
 f32 unk08;
 u16 unk0c;
 f32 unk10;
 f32 unk14;
 f32 unk18;
 s32 blurdrugamount;
};
struct animtablerow {
 s16 animnum;
 s32 flip;
 f32 endframe;
 f32 speed;
 u32 unk10;
 f32 thudframe1;
 f32 thudframe2;
};
struct animtable {
 s32 hitpart;
 struct animtablerow *deathanims;
 struct animtablerow *injuryanims;
 s32 deathanimcount;
 s32 injuryanimcount;
};
struct var80075c00 {
 s16 animnum;
 f32 loopframe;
 f32 endframe;
 f32 unk0c;
 f32 unk10;
 f32 unk14;
};
struct vimode {
 s32 fbwidth;
 s32 fbheight;
 s32 width;
 f32 yscale;
 s32 xscale;
 s32 fullheight;
 s32 fulltop;
 s32 wideheight;
 s32 widetop;
 s32 cinemaheight;
 s32 cinematop;
};
struct miscbio {
 u32 name;
 u32 description;
};
struct hangarbio {
 u32 name;
 u32 description;
 u32 unk08;
 u32 unk0c;
};
struct chrbio {
 u32 name;
 u32 race;
 u32 age;
 u32 description;
};
struct mpteaminfo {
 struct mpchr *mpchr;
 u32 teamnum;
 u32 unk08;
 u32 unk0c;
 s32 score;
};
struct hudmessagething {
 s32 unk00;
};
struct hudmsgtype {
          u8 unk00;
          u8 unk01;
          u8 unk02;
          struct hudmessagething *unk04;
          struct hudmessagething *unk08;
          u32 colour;
          u32 unk10;
          u8 alignh;
          u8 alignv;
          s16 unk16;
          s16 unk18;
          s32 duration;
};
struct hudmessage {
           u8 state;
           u8 boxed;
           u8 allowfadein;
           u8 flash;
           u8 opacity;
           u16 timer;
           u32 font1;
           u32 font2;
           u32 textcolour;
           u32 glowcolour;
           u16 x;
           u16 y;
           u16 width;
           u16 height;
           char text[400];
           s32 channelnum;
           u32 type;
           s32 id;
           s32 showduration;
           s32 playernum;
           u32 flags;
           u8 alignh;
           u8 alignv;
           u32 xmarginextra;
           u32 xmargin;
           u32 ymargin;
           u32 hash;
};
struct frtarget {
          u8 inuse : 1;
          u8 active : 1;
          u8 destroyed : 1;
          u8 scriptenabled : 1;
          u8 rotating : 1;
          u8 rotateoncloak : 1;
          u8 frpadindex : 2;
          u8 maxdamage;
          u8 scriptindex;
          struct prop *prop;
          struct coord dstpos;
          s32 scriptsleep;
          s32 timeuntilrotate;
          f32 travelspeed;
          u8 damage;
          u8 scriptoffset;
          f32 rotatespeed;
          f32 angle;
          f32 rotatetoangle;
          u8 flags;
          u8 silent;
          u8 donestopsound;
          u8 travelling;
          s8 frpadnum;
          s32 invincibletimer;
};
struct frdata {
           u8 maxactivetargets;
           u16 goalscore;
           u8 goaltargets;
           u8 timelimit;
           u8 ammolimit;
           u8 sdgrenadelimit;
           u8 goalaccuracy;
           f32 speed;
           struct frtarget targets[18];
           u8 difficulty;
           s32 timetaken;
           s32 score;
           u8 numtargets;
           u8 targetsdestroyed;
           u16 slot;
           u16 numshots;
           u8 numshotssincetopup;
           u8 failreason;
           u16 numhitsbullseye;
           u16 numhitsring1;
           u16 numhitsring2;
           u16 numhitsring3;
           s8 menucountdown;
           u8 menutype : 3;
           u8 donelighting : 1;
           u8 donealarm : 1;
           u8 ammohasgrace : 1;
           u8 helpscriptindex;
           u8 helpscriptoffset;
           u8 helpscriptenabled;
           s32 helpscriptsleep;
           u8 padindexoffset;
           u8 feedbackzone;
           s8 feedbackttl;
           s16 proxyendtimer;
           s16 ammoextra;
           s16 sdgrenadeextra;
           u32 unk47c;
};
struct menudata {
           s32 count;
           s32 root;
           s32 unk008;
           struct menudialog *unk00c;
           f32 unk010;
           u8 unk014;
           u8 unk015;
           u8 unk016;
           u8 unk017[4];
           s8 unk01b;
           u32 unk01c;
           u32 unk020;
           u32 unk024;
           u32 unk028;
           u32 unk02c;
           u32 unk030;
           u32 unk034;
           u32 unk038;
           u32 unk03c;
           u32 unk040;
           u32 unk044;
           u32 unk048;
           u32 unk04c;
           u32 unk050;
           u32 unk054;
           u32 unk058;
           u32 unk05c;
           u32 unk060;
           u32 unk064;
           u32 unk068;
           u32 unk06c;
           u32 unk070;
           u32 unk074;
           u32 unk078;
           u32 unk07c;
           u32 unk080;
           u32 unk084;
           u32 unk088;
           u32 unk08c;
           u32 unk090;
           u32 unk094;
           u32 unk098;
           u32 unk09c;
           u32 unk0a0;
           u32 unk0a4;
           u32 unk0a8;
           u32 unk0ac;
           u32 unk0b0;
           u32 unk0b4;
           u32 unk0b8;
           u32 unk0bc;
           u32 unk0c0;
           u32 unk0c4;
           u32 unk0c8;
           u32 unk0cc;
           u32 unk0d0;
           u32 unk0d4;
           u32 unk0d8;
           u32 unk0dc;
           u32 unk0e0;
           u32 unk0e4;
           u32 unk0e8;
           u32 unk0ec;
           u32 unk0f0;
           u32 unk0f4;
           u32 unk0f8;
           u32 unk0fc;
           u32 unk100;
           u32 unk104;
           u32 unk108;
           u32 unk10c;
           u32 unk110;
           u32 unk114;
           u32 unk118;
           u32 unk11c;
           u32 unk120;
           u32 unk124;
           u32 unk128;
           u32 unk12c;
           u32 unk130;
           u32 unk134;
           u32 unk138;
           u32 unk13c;
           u32 unk140;
           u32 unk144;
           u32 unk148;
           u32 unk14c;
           u32 unk150;
           u32 unk154;
           u32 unk158;
           u32 unk15c;
           u32 unk160;
           u32 unk164;
           u32 unk168;
           u32 unk16c;
           u32 unk170;
           u32 unk174;
           u32 unk178;
           u32 unk17c;
           u32 unk180;
           u32 unk184;
           u32 unk188;
           u32 unk18c;
           u32 unk190;
           u32 unk194;
           u32 unk198;
           u32 unk19c;
           u32 unk1a0;
           u32 unk1a4;
           u32 unk1a8;
           u32 unk1ac;
           u32 unk1b0;
           u32 unk1b4;
           u32 unk1b8;
           u32 unk1bc;
           u32 unk1c0;
           u32 unk1c4;
           u32 unk1c8;
           u32 unk1cc;
           u32 unk1d0;
           u32 unk1d4;
           u32 unk1d8;
           u32 unk1dc;
           u32 unk1e0;
           u32 unk1e4;
           u32 unk1e8;
           u32 unk1ec;
           u32 unk1f0;
           u32 unk1f4;
           u32 unk1f8;
           u32 unk1fc;
           u32 unk200;
           u32 unk204;
           u32 unk208;
           u32 unk20c;
           u32 unk210;
           u32 unk214;
           u32 unk218;
           u32 unk21c;
           u32 unk220;
           u32 unk224;
           u32 unk228;
           u32 unk22c;
           u32 unk230;
           u32 unk234;
           u32 unk238;
           u32 unk23c;
           u32 unk240;
           u32 unk244;
           u32 unk248;
           u32 unk24c;
           u32 unk250;
           u32 unk254;
           u32 unk258;
           u32 unk25c;
           u32 unk260;
           u32 unk264;
           u32 unk268;
           u32 unk26c;
           u32 unk270;
           u32 unk274;
           u32 unk278;
           u32 unk27c;
           u32 unk280;
           u32 unk284;
           u32 unk288;
           u32 unk28c;
           u32 unk290;
           u32 unk294;
           u32 unk298;
           u32 unk29c;
           u32 unk2a0;
           u32 unk2a4;
           u32 unk2a8;
           u32 unk2ac;
           u32 unk2b0;
           u32 unk2b4;
           u32 unk2b8;
           u32 unk2bc;
           u32 unk2c0;
           u32 unk2c4;
           u32 unk2c8;
           u32 unk2cc;
           u32 unk2d0;
           u32 unk2d4;
           u32 unk2d8;
           u32 unk2dc;
           u32 unk2e0;
           u32 unk2e4;
           u32 unk2e8;
           u32 unk2ec;
           u32 unk2f0;
           u32 unk2f4;
           u32 unk2f8;
           u32 unk2fc;
           u32 unk300;
           u32 unk304;
           u32 unk308;
           u32 unk30c;
           u32 unk310;
           u32 unk314;
           u32 unk318;
           u32 unk31c;
           u32 unk320;
           u32 unk324;
           u32 unk328;
           u32 unk32c;
           u32 unk330;
           u32 unk334;
           u32 unk338;
           u32 unk33c;
           u32 unk340;
           u32 unk344;
           u32 unk348;
           u32 unk34c;
           u32 unk350;
           u32 unk354;
           u32 unk358;
           u32 unk35c;
           u32 unk360;
           u32 unk364;
           u32 unk368;
           u32 unk36c;
           u32 unk370;
           u32 unk374;
           u32 unk378;
           u32 unk37c;
           u32 unk380;
           u32 unk384;
           u32 unk388;
           u32 unk38c;
           u32 unk390;
           u32 unk394;
           u32 unk398;
           u32 unk39c;
           u32 unk3a0;
           u32 unk3a4;
           u32 unk3a8;
           u32 unk3ac;
           u32 unk3b0;
           u32 unk3b4;
           u32 unk3b8;
           u32 unk3bc;
           u32 unk3c0;
           u32 unk3c4;
           u32 unk3c8;
           u32 unk3cc;
           u32 unk3d0;
           u32 unk3d4;
           u32 unk3d8;
           u32 unk3dc;
           u32 unk3e0;
           u32 unk3e4;
           u32 unk3e8;
           u32 unk3ec;
           u32 unk3f0;
           u32 unk3f4;
           u32 unk3f8;
           u32 unk3fc;
           u32 unk400;
           u32 unk404;
           u32 unk408;
           u32 unk40c;
           u32 unk410;
           u32 unk414;
           u32 unk418;
           u32 unk41c;
           u32 unk420;
           u32 unk424;
           u32 unk428;
           u32 unk42c;
           u32 unk430;
           u32 unk434;
           u32 unk438;
           u32 unk43c;
           u32 unk440;
           u32 unk444;
           u32 unk448;
           u32 unk44c;
           u32 unk450;
           u32 unk454;
           u32 unk458;
           u32 unk45c;
           u32 unk460;
           u32 unk464;
           u32 unk468;
           u32 unk46c;
           u32 unk470;
           u32 unk474;
           u32 unk478;
           u32 unk47c;
           u32 unk480;
           u32 unk484;
           u32 unk488;
           u32 unk48c;
           u32 unk490;
           u32 unk494;
           u32 unk498;
           u32 unk49c;
           u32 unk4a0;
           u32 unk4a4;
           u32 unk4a8;
           u32 unk4ac;
           u32 unk4b0;
           u32 unk4b4;
           u32 unk4b8;
           u32 unk4bc;
           u32 unk4c0;
           u32 unk4c4;
           u32 unk4c8;
           u32 unk4cc;
           u32 unk4d0;
           u32 unk4d4;
           u32 unk4d8;
           u32 unk4dc;
           u32 unk4e0;
           u32 unk4e4;
           u32 unk4e8;
           u32 unk4ec;
           u32 unk4f0;
           u32 unk4f4;
           u32 unk4f8;
           u32 unk4fc;
           u32 unk500;
           u32 unk504;
           u32 unk508;
           u32 unk50c;
           u32 unk510;
           u32 unk514;
           u32 unk518;
           u32 unk51c;
           u32 unk520;
           u32 unk524;
           u32 unk528;
           u32 unk52c;
           u32 unk530;
           u32 unk534;
           u32 unk538;
           u32 unk53c;
           u32 unk540;
           u32 unk544;
           u32 unk548;
           u32 unk54c;
           u32 unk550;
           u32 unk554;
           u32 unk558;
           u32 unk55c;
           u32 unk560;
           u32 unk564;
           u32 unk568;
           u32 unk56c;
           u32 unk570;
           u32 unk574;
           u32 unk578;
           u32 unk57c;
           u32 unk580;
           u32 unk584;
           u32 unk588;
           u32 unk58c;
           u32 unk590;
           u32 unk594;
           u32 unk598;
           u32 unk59c;
           u32 unk5a0;
           u32 unk5a4;
           u32 unk5a8;
           u32 unk5ac;
           u32 unk5b0;
           u32 unk5b4;
           u32 unk5b8;
           u32 unk5bc;
           u32 unk5c0;
           u32 unk5c4;
           u32 unk5c8;
           u32 unk5cc;
           u32 unk5d0;
           u8 unk5d4;
           u8 unk5d5_01 : 1;
           u8 unk5d5_02 : 1;
           u8 unk5d5_03 : 1;
           u8 unk5d5_04 : 1;
           u8 unk5d5_05 : 1;
           u8 unk5d5_06 : 1;
           u8 unk5d5_07 : 1;
           u8 unk5d5_08 : 1;
           u32 unk5d8;
           u32 unk5dc;
           u32 unk5e0;
           u32 unk5e4;
           u32 unk5e8;
           u32 unk5ec;
           u32 unk5f0;
           u32 unk5f4;
           u32 unk5f8;
           u32 unk5fc;
           u32 unk600;
           u32 unk604;
           u32 unk608;
           u32 unk60c;
           u32 unk610;
           u32 unk614;
           u32 unk618;
           u32 unk61c;
           u32 unk620;
           u32 unk624;
           u32 unk628;
           u32 unk62c;
           u32 unk630;
           u32 unk634;
           u32 unk638;
           u32 unk63c;
           u32 unk640;
           u32 unk644;
           u32 unk648;
           u32 unk64c;
           u32 unk650;
           u32 unk654;
           u32 unk658;
           u32 unk65c;
           u32 unk660;
           u32 unk664;
           u8 unk668;
           u8 unk669[4];
           u8 unk66d;
           s8 unk66e;
           u8 unk66f;
           s32 unk670;
           s32 unk674;
};
struct ammotype {
 s32 capacity;
 u32 unk04;
 f32 unk08;
};
struct weather58 {
 f32 unk00;
 f32 unk04;
 s32 unk08;
};
struct weatherdata {
          f32 windspeedx;
          f32 windspeedz;
          f32 windanglerad;
          f32 unk0c;
          s32 unk10;
          f32 windspeed;
          u32 unk18;
          u32 unk1c;
          s32 type;
          struct weatherparticledata *particledata;
          u32 unk28;
          u32 unk2c;
          u32 unk30;
          struct audiohandle *audiohandles[4];
          s32 unk44;
          s32 unk48;
          s32 unk4c;
          s32 unk50;
          s32 unk54;
          struct weather58 unk58[4];
          f32 unk88;
          f32 unk8c;
          s32 unk90;
          s32 unk94;
          u32 unk98;
          u32 unk9c;
          u32 unka0;
          u32 unka4;
          u32 unka8;
          u32 unkac;
          u32 unkb0;
          u32 unkb4;
          f32 unkb8;
          f32 unkbc;
          s32 unkc0;
          f32 unkc4;
          f32 unkc8;
          s32 intensity;
          s32 unkd0;
          s32 unkd4;
          u32 unkd8;
          u32 unkdc;
          u32 unke0;
          u32 unke4;
          u32 unke8;
          u32 unkec;
          u32 unkf0;
          u32 unkf4;
          s16 unkf8;
          u32 unkfc;
};
struct weatherparticle {
 struct coord pos;
 s32 active;
 struct coord inc;
 f32 unk1c;
};
struct weatherparticledata {
 struct weatherparticle particles[500];
            f32 unk3e80;
            f32 unk3e84;
            f32 unk3e88;
            struct coord boundarymax;
            struct coord boundarymin;
            struct coord boundaryrange;
            u32 unk3eb0;
            u32 unk3eb4;
            u32 unk3eb8;
            u32 unk3ebc;
            u32 unk3ec0;
            u32 unk3ec4;
            f32 unk3ec8[8];
            u32 unk3ee8;
            u32 unk3eec;
            u32 unk3ef0;
            u32 unk3ef4;
            u32 unk3ef8;
            u32 unk3efc;
};
struct texture {
 u8 soundsurfacetype : 4;
 u8 surfacetype : 4;
 u8 unk01;
 u16 dataoffset;
 u32 unk04;
};
struct var800aabb8 {
 u16 unk00_00 : 2;
 u16 unk00_02 : 14;
};
struct fileinfo {
 u32 size;
 u32 unk04;
};
struct portalcmd {
 u8 type;
 u8 len;
 s32 param;
};
struct var800a4640_00 {
 s16 unk00;
 u8 unk02;
 u8 unk03;
 struct screenbox box;
};
struct var800a4640 {
           struct var800a4640_00 unk000[60];
           struct var800a4640_00 unk2d0;
};
struct zrange {
 f32 near;
 f32 far;
};
struct var800a4d00 {
          s16 roomnum;
          s16 unk02[5];
          s8 unk0c;
          s8 unk0d;
          s16 unk0e;
          struct screenbox screenbox;
};
struct var800a4cf0 {
 u16 unk00;
 s16 index;
 s16 unk04;
 struct zrange zrange;
 struct var800a4d00 unk10[250];
};
struct menuinputs {
          s8 leftright;
          s8 updown;
          u8 select;
          u8 back;
          s8 xaxis;
          s8 yaxis;
          u8 shoulder;
          u8 back2;
          s8 unk08;
          s8 unk09;
          s8 start;
};
struct mpconfigsim {
 u8 type;
 u8 mpheadnum;
 u8 mpbodynum;
 u8 team;
 u8 difficulties[4];
};
struct mpconfig {
 struct mpsetup setup;
 struct mpconfigsim simulants[8];
};
struct mpweapon {
          u8 weaponnum;
          s8 weapon1ammotypeminusone;
          u8 weapon1ammoqty;
          s8 weapon2ammotypeminusone;
          u8 weapon2ammoqty;
          u8 giveweapon : 1;
          u8 unlockfeature : 7;
          s16 model;
          s16 extrascale;
};
struct mpstrings {
 char description[200];
 char aibotnames[8][15];
};
struct mpconfigfull {
 struct mpconfig config;
 struct mpstrings strings;
};
struct movedata {
          s32 canswivelgun;
          s32 canmanualaim;
          s32 triggeron;
          s32 btapcount;
          s32 canlookahead;
          s32 unk14;
          s32 cannaturalturn;
          s32 cannaturalpitch;
          s32 digitalstepforward;
          s32 digitalstepback;
          s32 digitalstepleft;
          s32 digitalstepright;
          f32 unk30;
          f32 unk34;
          f32 speedvertadown;
          f32 speedvertaup;
          f32 aimturnleftspeed;
          f32 aimturnrightspeed;
          s32 weaponbackoffset;
          s32 weaponforwardoffset;
          u32 unk50;
          u32 unk54;
          u32 unk58;
          f32 zoomoutfovpersec;
          f32 zoominfovpersec;
          s32 crouchdown;
          s32 crouchup;
          s32 rleanleft;
          s32 rleanright;
          s32 detonating;
          s32 canautoaim;
          s32 farsighttempautoseek;
          s32 eyesshut;
          s32 invertpitch;
          s32 disablelookahead;
          s32 c1stickxsafe;
          s32 c1stickysafe;
          s32 c1stickxraw;
          s32 c1stickyraw;
          s32 analogturn;
          s32 analogpitch;
          s32 analogstrafe;
          s32 analogwalk;
};
struct attackanimgroup {
 struct attackanimconfig *animcfg;
 u32 len;
};
struct modelthing {
          u32 unk00;
          u32 unk04;
          u32 unk08;
          u32 unk0c;
          s16 unk10;
          u16 unk12;
          u16 unk14;
          u16 unk16;
};
struct animheader {
          u16 numframes;
          u16 framelen;
          u8 *data;
          u16 initialposbytes;
          u8 initiaposbitsperentry;
          u8 flags;
};
struct objticksp476 {
          Mtxf *matrix;
          u32 unk04;
          u32 unk08;
          u32 unk0c;
          Mtxf *unk10;
          u32 unk14;
          u32 unk18;
          u32 unk1c;
          u32 unk20;
          u32 unk24;
          u32 unk28;
          u32 unk2c;
          u32 unk30;
          u32 unk34;
          u32 unk38;
          u32 unk3c;
};
struct modelrenderdata {
          u32 unk00;
          s32 zbufferenabled;
          u32 flags;
          Gfx *gdl;
          u32 unk10;
          u32 unk14;
          u32 unk18;
          u32 unk1c;
          u32 unk20;
          u32 unk24;
          u32 unk28;
          u32 unk2c;
          s32 unk30;
          u32 envcolour;
          u32 fogcolour;
          u32 cullmode;
};
struct rend_vidat {
          void *unk00;
          u16 x;
          u16 y;
          f32 fovy;
          f32 aspect;
          f32 znear;
          f32 zfar;
          s16 bufx;
          s16 bufy;
          s16 viewx;
          s16 viewy;
          s16 viewleft;
          s16 viewtop;
          s32 usezbuf;
          void *unk28;
};
struct var80062a8c {
          struct prop *prop;
          struct modelnode *node;
          struct model *model;
          s32 lvframe60;
          s8 side;
          s8 unk011;
          s16 unk012;
          s16 unk014;
          s16 unk016;
          s8 unk018[32];
          s8 unk038[32];
          f32 shield;
};
struct bgroom {
 u32 unk00;
 struct coord pos;
 u8 unk10;
 u8 unk11;
};
struct damagetype {
 f32 flashstartframe;
 f32 flashfullframe;
 f32 flashendframe;
 f32 maxalpha;
 s32 red;
 s32 green;
 s32 blue;
};
struct healthdamagetype {
 s32 openendframe;
 s32 updatestartframe;
 s32 updateendframe;
 s32 closestartframe;
 s32 closeendframe;
};
struct optiongroup {
 s32 offset;
 u16 name;
};
struct var800aa5d8 {
 s32 tracktype;
 s32 tracknum;
 s32 unk08;
 f32 unk0c;
 s16 volume;
 s16 unk12;
 s16 unk14;
 s16 unk16;
 s16 unk18;
};
struct casing {
          u32 unk00;
          u32 unk04;
          u32 unk08;
          u32 unk0c;
          u32 unk10;
          u32 unk14;
          u32 unk18;
          u32 unk1c;
          u32 unk20;
          u32 unk24;
          u32 unk28;
          u32 unk2c;
          u32 unk30;
          u32 unk34;
          u32 unk38;
          u32 unk3c;
          u32 unk40;
};
struct mplockinfo {
 s8 lockedplayernum;
 s8 lastwinner;
 s8 lastloser;
 s8 unk03;
 s32 unk04;
};
struct var8009da60 {
 union {
  s32 unk00;
  struct prop *unk00_prop;
 };
 u8 unk04;
 u8 unk05;
 u32 unk08;
 u32 unk0c;
 u32 unk10;
 struct coord unk14;
 struct coord unk20;
 f32 unk2c;
};
struct var800a4ce8 {
 u16 unk00;
 s16 unk02;
};
struct var800aaa38 {
 u32 tracktype;
 u32 unk04;
 u32 unk08;
 u32 unk0c;
};
struct var80094ed8 {
           u32 unk000;
           u32 unk004;
           u32 unk008;
           u32 unk00c;
           u32 unk010;
           u32 unk014;
           u32 unk018;
           u32 unk01c;
           u32 unk020;
           u32 unk024;
           u32 unk028;
           u32 unk02c;
           u32 unk030;
           u32 unk034;
           u32 unk038;
           u32 unk03c;
           u32 unk040;
           u32 unk044;
           u32 unk048;
           u32 unk04c;
           u32 unk050;
           u32 unk054;
           u32 unk058;
           u32 unk05c;
           u32 unk060;
           u32 unk064;
           u32 unk068;
           u32 unk06c;
           u32 unk070;
           u32 unk074;
           u32 unk078;
           u32 unk07c;
           u32 unk080;
           u32 unk084;
           u32 unk088;
           u32 unk08c;
           u32 unk090;
           u32 unk094;
           u32 unk098;
           u32 unk09c;
           u32 unk0a0;
           u32 unk0a4;
           u32 unk0a8;
           u32 unk0ac;
           u32 unk0b0;
           u32 unk0b4;
           u32 unk0b8;
           u32 unk0bc;
           u32 unk0c0;
           u32 unk0c4;
           u32 unk0c8;
           u32 unk0cc;
           u32 unk0d0;
           u32 unk0d4;
           u32 unk0d8;
           u32 unk0dc;
           u32 unk0e0;
           u32 unk0e4;
           u32 unk0e8;
           u32 unk0ec;
           u32 unk0f0;
           u32 unk0f4;
           ALCSPlayer *seqp;
           void *unk0fc;
           u16 unk100;
           u32 unk104;
};
struct lasersight {
 s32 id;
 struct coord unk04;
 struct coord beamnear;
 struct coord beamfar;
 f32 unk28;
 struct coord dotpos;
 struct coord dotrot;
 u32 unk44;
};
struct vec3s16 {
 s16 x;
 s16 y;
 s16 z;
};
struct light {
          u16 roomnum;
          u16 unk02;
          u8 unk04;
          u8 unk05_00 : 1;
          u8 healthy : 1;
          u8 on : 1;
          u8 unk05_03 : 1;
          u8 vulnerable : 1;
          u8 unk06;
          s8 unk07;
          s8 unk08;
          s8 unk09;
          struct vec3s16 bbox[4];
};
struct var80061420 {
 s32 unk00;
 s32 unk04;
};
struct var800ab5b8 {
 u32 unk00;
 u32 unk04;
 u32 unk08;
 u32 unk0c;
 u32 unk10;
 u32 unk14;
 u32 unk18;
 u32 unk1c;
 u32 unk20;
 u32 unk24;
 u32 unk28;
};
struct var800ab718 {
 u32 unk00;
 u32 unk04;
 u32 unk08;
 u32 unk0c;
 u32 unk10;
};
struct splat {
           struct coord unk000;
           struct coord unk00c;
           u32 unk018;
           struct coord unk01c;
           struct coord unk028;
           u32 unk034;
           u32 unk038;
           u32 unk03c;
           u32 unk040;
           u32 unk044;
           u32 unk048;
           u32 unk04c;
           u32 unk050;
           u32 unk054;
           u32 unk058;
           u32 unk05c;
           u32 unk060;
           u32 unk064;
           u32 unk068;
           u32 unk06c;
           u32 unk070;
           u32 unk074;
           u32 unk078;
           u32 unk07c;
           u32 unk080;
           u32 unk084;
           u32 unk088;
           u32 unk08c;
           u32 unk090;
           u32 unk094;
           u32 unk098;
           u32 unk09c;
           u32 unk0a0;
           u32 unk0a4;
           u32 unk0a8;
           u32 unk0ac;
           u32 unk0b0;
           u32 unk0b4;
           u32 unk0b8;
           u32 unk0bc;
           u32 unk0c0;
           u32 unk0c4;
           u32 unk0c8;
           u32 unk0cc;
           u32 unk0d0;
           u32 unk0d4;
           u32 unk0d8;
           u32 unk0dc;
           u32 unk0e0;
           u32 unk0e4;
           u32 unk0e8;
           u32 unk0ec;
           u32 unk0f0;
           u32 unk0f4;
           u32 unk0f8;
           u32 unk0fc;
           u32 unk100;
           u32 unk104;
           u32 unk108;
           u32 unk10c;
           u32 unk110;
           u32 unk114;
           u32 unk118;
           u32 unk11c;
           u32 unk120;
           u32 unk124;
           u32 unk128;
           u32 unk12c;
           u32 unk130;
           u32 unk134;
           u32 unk138;
           u32 unk13c;
           u32 unk140;
           u32 unk144;
           u32 unk148;
           u32 unk14c;
           u32 unk150;
           u32 unk154;
           u32 unk158;
           u32 unk15c;
           u32 unk160;
           u32 unk164;
           u32 unk168;
           u32 unk16c;
           u32 unk170;
           u32 unk174;
           u32 unk178;
           u32 unk17c;
           u32 unk180;
           u32 unk184;
           u32 unk188;
           u32 unk18c;
           u32 unk190;
           u32 unk194;
           u32 unk198;
           u32 unk19c;
           u32 unk1a0;
           u32 unk1a4;
           u32 unk1a8;
           u32 unk1ac;
           u32 unk1b0;
           u32 unk1b4;
           u32 unk1b8;
           u32 unk1bc;
           u32 unk1c0;
           u32 unk1c4;
           u32 unk1c8;
           u32 unk1cc;
           u32 unk1d0;
           u32 unk1d4;
           u32 unk1d8;
           u32 unk1dc;
           u32 unk1e0;
           u32 unk1e4;
           u32 unk1e8;
           u32 unk1ec;
           u32 unk1f0;
           u32 unk1f4;
           u32 unk1f8;
           u32 unk1fc;
           u32 unk200;
           u32 unk204;
           u32 unk208;
           u32 unk20c;
           u32 unk210;
           u32 unk214;
           u32 unk218;
           u32 unk21c;
           u32 unk220;
           u32 unk224;
           u32 unk228;
           u32 unk22c;
           u32 unk230;
           u32 unk234;
           u32 unk238;
           u32 unk23c;
           u32 unk240;
           u32 unk244;
           u32 unk248;
           u32 unk24c;
           u32 unk250;
           u32 unk254;
           u32 unk258;
           u32 unk25c;
           u32 unk260;
           u32 unk264;
           u32 unk268;
           u32 unk26c;
           u32 unk270;
           u32 unk274;
           u32 unk278;
           u32 unk27c;
           u32 unk280;
           u32 unk284;
           u32 unk288;
           u32 unk28c;
           u32 unk290;
           u32 unk294;
           u32 unk298;
           u32 unk29c;
           u32 unk2a0;
           u32 unk2a4;
           u32 unk2a8;
           u32 unk2ac;
           u32 unk2b0;
           u32 unk2b4;
           u32 unk2b8;
           u32 unk2bc;
           u32 unk2c0;
           u32 unk2c4;
           u32 unk2c8;
           u32 unk2cc;
           u32 unk2d0;
           u32 unk2d4;
           u32 unk2d8;
           u32 unk2dc;
           u32 unk2e0;
           u32 unk2e4;
           u32 unk2e8;
           u32 unk2ec;
           u32 unk2f0;
           u32 unk2f4;
           u32 unk2f8;
           u32 unk2fc;
           u32 unk300;
           u32 unk304;
           u32 unk308;
           u32 unk30c;
           u32 unk310;
           u32 unk314;
           u32 unk318;
           u32 unk31c;
           u32 unk320;
           u32 unk324;
           u32 unk328;
           u32 unk32c;
           u32 unk330;
           u32 unk334;
           u32 unk338;
           u32 unk33c;
           u32 unk340;
           u32 unk344;
           u32 unk348;
           u32 unk34c;
           u32 unk350;
           u32 unk354;
           u32 unk358;
           u32 unk35c;
           u32 unk360;
           u32 unk364;
           u32 unk368;
           u32 unk36c;
           u32 unk370;
           u32 unk374;
           u32 unk378;
           u32 unk37c;
           u32 unk380;
           u32 unk384;
           u32 unk388;
           u32 unk38c;
           u32 unk390;
           u32 unk394;
           u32 unk398;
           u32 unk39c;
           u32 unk3a0;
           u32 unk3a4;
           u32 unk3a8;
           u32 unk3ac;
           u32 unk3b0;
           u32 unk3b4;
           u32 unk3b8;
           u32 unk3bc;
           u32 unk3c0;
           u32 unk3c4;
           u32 unk3c8;
           u32 unk3cc;
           u32 unk3d0;
           u32 unk3d4;
           u32 unk3d8;
           u32 unk3dc;
           u32 unk3e0;
           u32 unk3e4;
           u32 unk3e8;
           u32 unk3ec;
           u32 unk3f0;
           u32 unk3f4;
           u32 unk3f8;
           u32 unk3fc;
           u32 unk400;
           u32 unk404;
           u32 unk408;
           u32 unk40c;
           u32 unk410;
           u32 unk414;
           u32 unk418;
           u32 unk41c;
           u32 unk420;
           u32 unk424;
           u32 unk428;
           u32 unk42c;
           u32 unk430;
           u32 unk434;
           u32 unk438;
           u32 unk43c;
           u32 unk440;
};
struct menurendercontext {
 s16 x;
 s16 y;
 s16 width;
 s16 height;
 struct menuitem *item;
 s32 focused;
 struct menuframe *frame;
 union menuitemdata *data;
};
struct menucolourpalette {
          u32 unk00;
          u32 unk04;
          u32 unk08;
          u32 unk0c;
          u32 unk10;
          u32 unk14;
          u32 unfocused;
          u32 disabled;
          u32 focused;
          u32 checkedunfocused;
          u32 unk28;
          u32 unk2c;
          u32 unk30;
          u32 unk34;
          u32 unk38;
};
struct var800a45d0 {
          u8 unk00;
          u32 colour04;
          u32 colour08;
          s32 unk0c;
          s32 unk10;
          f32 unk14;
          u8 unk18;
          u32 unk1c;
          u32 unk20;
          f32 unk24;
          u8 unk28;
          u8 unk29;
          u8 unk2a;
          s32 unk2c;
          s32 unk30;
          u32 unk34;
          s32 x1;
          s32 x2;
          u32 unk40;
          u32 colour44;
          u32 colour48;
          u32 unk4c;
          u32 unk50;
          u32 unk54;
          u32 colour58;
          u32 colour5c;
          u32 unk60;
};
struct bytelist {
 u8 b0;
 u8 b1;
 u8 b2;
 u8 b3;
 u8 b4;
 u8 b5;
 u8 b6;
 u8 b7;
 u8 b8;
 u8 b9;
};
struct stageallocation {
 s32 stagenum;
 char *string;
};
struct contsample {
 OSContPad pads[4];
};
struct joydata {
 struct contsample samples[20];
 s32 curlast;
 s32 curstart;
 s32 nextlast;
 s32 nextsecondlast;
 u16 buttonspressed[4];
 u16 buttonsreleased[4];
 s32 unk200;
};
struct guncmd {
 u8 type;
 u8 unk01;
 u16 unk02;
 s32 unk04;
};
struct pakthing {
 char unk00[4];
 u32 unk04;
 u32 unk08;
 u32 unk0c;
 u16 unk10;
};
struct pakthing16 {
 u32 unk00;
 u32 unk04;
 u32 unk08_01 : 9;
 u32 unk08_10 : 3;
 u32 unk08_fill : 20;
 u32 unk0c_01 : 20;
 u32 unk0c_21 : 9;
};
struct var80067e6c {
 s16 animnum;
 f32 value;
};
struct textureconfig {
 u32 texturenum;
 u8 width;
 u8 height;
 u8 level;
 u8 format;
 u8 depth;
 u8 s;
 u8 t;
};
struct gfxvtx {
          s16 x;
          s16 y;
          s16 z;
          u8 flags;
          u8 s;
          s16 unk08;
          s16 unk0a;
};
struct shard {
          s16 room;
          s32 age60;
          struct coord pos;
          struct coord rot;
          struct coord vel;
          struct coord rotspeed;
          struct gfxvtx vertices[3];
          u8 colours[3][4];
          u8 type;
};
struct audiochannel {
          struct audiohandle *audiohandle;
          s16 soundnum04;
          s16 unk06;
          s16 unk08;
          s16 unk0a;
          s16 unk0c;
          s16 unk0e;
          s16 unk10;
          u16 unk12;
          u32 unk14;
          s16 unk18;
          u8 unk1a;
          s32 unk1c;
          s32 unk20;
          s16 padnum;
          s16 soundnum26;
          s16 unk28;
          u16 unk2a;
          s16 unk2c;
          s16 channelnum;
          u16 flags;
          u16 flags2;
          f32 unk34;
          f32 unk38;
          f32 unk3c;
          f32 unk40;
          f32 unk44;
          f32 unk48;
          f32 unk4c;
          struct prop *prop;
          struct coord *posptr;
          struct coord pos;
          s16 rooms[8];
};
struct var8007e3d0_data {
 void *unk00;
 u32 unk04;
 u32 unk08;
 s16 unk0c;
 s16 unk0e;
};
struct var8007e3d0 {
 s32 valifsp;
 s32 numifsp;
 s32 valifmp;
 s32 numifmp;
 s32 valifspecial;
 s32 numifspecial;
 s32 unk18;
 s32 unk1c;
 s32 unk20;
 struct var8007e3d0_data *unk24;
 s32 val1;
 s32 val2;
 s32 numallocated;
};
struct stageheadlimit {
 u8 stagenum;
 u8 maxheads;
};
struct var800a41b0 {
          u32 unk00;
          u32 unk04;
          u32 unk08;
          u32 unk0c;
          u32 unk10;
          u32 unk14;
          u32 unk18;
          u32 unk1c;
          u32 unk20;
          u32 unk24;
          u32 unk28;
          u32 unk2c;
          u32 unk30;
          u32 unk34;
          u32 unk38;
          u32 unk3c;
          u32 unk40;
          u32 unk44;
          u32 unk48;
          u32 unk4c;
          u32 unk50;
          u32 unk54;
          u32 unk58;
          struct prop *prop;
          u32 unk60;
          u32 unk64;
          s16 unk68;
          u8 unk6a;
          u8 unk6b;
          u8 unk6c;
          u8 unk6d;
          u8 unk6e;
          u8 unk6f_00 : 1;
          u8 unk6f_01 : 1;
          u8 unk6f_02 : 1;
          u8 unk6f_03 : 1;
          u32 unk70_00 : 28;
          u32 unk70_28 : 4;
          struct var800a41b0 *prev;
          u32 unk78;
};
struct roomproplistchunk {
 s16 propnums[8];
};
struct var8009ce60 {
 struct defaultobj base;
          u32 unk5c;
};
struct nbomb {
 struct coord pos;
 s32 age240;
 f32 radius;
 s16 unk14;
 f32 unk18;
 struct prop *prop;
 struct audiohandle *audiohandle20;
 struct audiohandle *audiohandle24;
};
struct roomacousticdata {
 f32 surfacearea;
 f32 unk04;
 f32 unk08;
 f32 roomvolume;
};
struct var8009dd78 {
 s16 unk00;
 f32 unk04;
};
struct colour {
 union {
  struct {
   u8 r;
   u8 g;
   u8 b;
   u8 a;
  } u8;
  u32 u32;
 };
};
struct var800ab570 {
 u32 unk00;
 u32 unk04;
};
struct collisionthing {
 struct tile *tile;
 u32 unk04;
 s32 unk08;
 struct prop *prop;
 u32 roomnum;
};
struct escastepkeyframe {
 s32 frame;
 struct coord pos;
};
struct font2a4 {
 u8 index;
 s8 baseline;
 u8 height;
 u8 width;
 u32 unk04;
 u8 *data;
};
struct font {
 u32 unk000[169];
 struct font2a4 unk2a4[94];
};
typedef union {
 struct {
  short type;
 } gen;
 struct {
  short type;
  struct AudioInfo_s *info;
 } done;
 OSScMsg app;
} AudioMsg;
typedef struct AudioInfo_s {
 short *data;
 short frameSamples;
 OSScTask task;
 AudioMsg msg;
} AudioInfo;
typedef struct {
 Acmd *ACMDList[2];
 AudioInfo *audioInfo[3];
 OSThread thread;
 OSMesgQueue audioFrameMsgQ;
 OSMesg audioFrameMsgBuf[8];
 OSMesgQueue audioReplyMsgQ;
 OSMesg audioReplyMsgBuf[8];
 ALGlobals g;
} AMAudioMgr;
typedef struct {
 ALLink node;
 u32 startAddr;
 u32 lastFrame;
 char *ptr;
} AMDMABuffer;
typedef struct {
 u8 initialized;
 AMDMABuffer *firstUsed;
 AMDMABuffer *firstFree;
} AMDMAState;
union audioparam {
 s32 s32;
 f32 f32;
};
struct animationdefinition {
 u16 numkeyframes;
 u16 numkeyframebytes;
 void *data;
 u16 unk08;
 u8 unk0a;
 u8 flags;
};
struct portalvertices {
 u8 count;
 struct coord vertices[1];
};
struct aibotweaponpreference {
 u8 unk00;
 u8 unk01;
 u8 unk02;
 u8 unk03;
 u16 unk04_00 : 1;
 u16 unk04_01 : 1;
 u16 unk04_02 : 4;
 u16 unk04_06 : 4;
 u16 unk06;
 u16 unk08;
 u16 unk0a;
 u16 unk0c;
 u16 unk0e_00 : 3;
 u16 unk0e_03 : 1;
};
struct handweaponinfo {
 s32 weaponnum;
 struct weapon *definition;
 struct gunctrl *gunctrl;
};
struct seqtableentry {
 u8 *data;
 u16 binlen;
 u16 ziplen;
};
struct seqtable {
 u16 count;
 struct seqtableentry entries[1];
};
struct mp3vars {
          s32 var8009c390;
          u32 var8009c394;
          u8 *var8009c398;
          s16 var8009c39c;
          s16 var8009c39e;
          s16 ivol1;
          s16 ivol2;
          u16 var8009c3a4;
          u16 var8009c3a6;
          u16 ratel1;
          s16 ratem1;
          s16 var8009c3ac;
          u16 ratel2;
          s16 ratem2;
          s16 var8009c3b2;
          u16 var8009c3b4;
          s32 samples;
          s32 var8009c3bc;
          s32 var8009c3c0;
          s32 var8009c3c4;
          u32 var8009c3c8;
          u32 var8009c3cc;
          u32 var8009c3d0;
          u8 *var8009c3d4;
          u32 var8009c3d8;
          void *var8009c3dc;
          u32 var8009c3e0;
          u32 var8009c3e4;
          u32 var8009c3e8;
          s16 var8009c3ec;
          s16 var8009c3ee;
          u8 var8009c3f0;
};
struct var8009c340 {
 u8 unk00;
 u8 unk01;
 u8 unk02;
 u8 unk03;
};
struct var80095210 {
 u16 *soundnums;
 u8 unk04[45];
 u16 unk32[45];
};
struct rdptask {
 OSScTask sctask;
 u16 *framebuffer;
 u32 unk5c;
};
struct var8009ddec {
          u32 unk00;
          struct coord pos;
          f32 look[2];
          s32 pad;
};
struct hitthing {
 struct coord unk00;
 struct coord unk0c;
 u32 unk18;
 u32 unk1c;
 u32 unk20;
 u32 unk24;
 s16 unk28;
 s16 texturenum;
 s16 unk2c;
 s16 unk2e;
};
struct hit {
          f32 distance;
          struct prop *prop;
          u32 unk08;
          struct modelnode *node;
          struct hitthing hitthing;
          u32 unk40;
          struct modelnode *unk44;
          struct model *model;
          s8 unk4c;
          s8 unk4d;
          struct coord pos;
          struct coord dir;
};
struct shotdata {
          struct coord unk00;
          struct coord unk0c;
          struct gset gset;
          struct coord gunpos;
          struct coord dir;
          f32 unk34;
          s32 unk38;
          struct hit hits[10];
};
extern u32 var8008ae20;
extern u32 var8008ae24;
extern u32 var8008ae28;
extern u32 var8008ae2c;
extern u32 var8008ae30;
extern OSThread g_MainThread;
extern OSMesgQueue var8008db30;
extern OSSched g_SchedThread;
extern OSViMode var8008dcc0[2];
extern OSViMode *var8008dd60[2];
extern u32 var8008de08;
extern u32 var8008de0c;
extern u32 var8008de10;
extern u8 g_BootBufferDirtyIndexes[3];
extern s32 g_BootBufferIndex0;
extern OSPiHandle CartRomHandle;
extern OSPiHandle LeoDiskHandle;
extern OSPifRam __osPfsPifRam;
extern u32 var80090ab0;
extern u32 var80090ad0;
extern u32 var80090ae8;
extern u8 g_Is4Mb;
extern u32 var80090af4;
extern u32 var80090af8;
extern u32 var80090afc;
extern u32 var80090b00;
extern u32 var80090b04;
extern u32 var80090b08;
extern AMAudioMgr g_AudioManager;
extern u32 var80092828;
extern u32 var80092870;
extern s32 var80094ea8;
extern struct var80094ed8 var80094ed8[3];
extern ALHeap g_SndHeap;
extern u32 var80095200;
extern struct var80095210 var80095210;
extern OSMesgQueue var80099e78;
extern struct g_vars g_Vars;
extern u32 var8009a874;
extern u32 var8009a888;
extern union filedataptr g_TileFileData;
extern s32 g_TileNumRooms;
extern u32 *g_TileRooms;
extern u32 var8009a918;
extern u8 g_RdpDramStack[0x400];
extern u32 var8009c2d0;
extern struct var8009c340 var8009c340;
extern u32 var8009c344;
extern u32 var8009c3e0;
extern u8 *var8009c3f4;
extern u8 *var8009c640;
extern u8 *var8009c644;
extern u8 var8009c650[];
extern u8 *var8009c6d8;
extern u8 *var8009c6dc;
extern __OSEventState __osEventStateTab[15];
extern OSTimer var8009c760;
extern OSTime __osCurrentTime;
extern u32 __osBaseCounter;
extern u32 __osViIntrCount;
extern OSMesgQueue __osPiAccessQueue;
extern OSPifRam __osContPifRam;
extern u8 __osContLastCmd;
extern u8 __osMaxControllers;
extern OSTimer __osEepromTimer;
extern OSMesgQueue __osEepromTimerQ;
extern OSMesg __osEepromTimerMsg;
extern OSPifRam __osEepPifRam;
extern u32 var8009ca84;
extern void *var8009cac0;
extern void *var8009cac4;
extern u8 var8009caec[4];
extern u8 var8009caf0;
extern struct prop *g_DangerousProps[12];
extern u16 *var8009cc40;
extern s32 var8009cc44;
extern u32 var8009cc48;
extern u32 var8009cc4c;
extern u32 var8009cc50;
extern u32 var8009cc54;
extern u32 var8009cc58;
extern u32 var8009cc5c;
extern u32 var8009cc60;
extern s32 var8009cc64;
extern u32 var8009cc68;
extern u32 var8009cc70;
extern u32 var8009cc74;
extern f32 var8009cc78;
extern Mtx *var8009cc80;
extern Mtx *var8009cc84;
extern Mtx *var8009cc88;
extern u8 *var8009cca0;
extern void *var8009ccc0[20];
extern s32 g_NumChrs;
extern s16 *g_Chrnums;
extern s16 *g_ChrIndexes;
extern s32 g_NumActiveHeadsPerGender;
extern u32 g_ActiveMaleHeads[8];
extern u32 g_ActiveFemaleHeads[8];
extern s16 *g_RoomPropListChunkIndexes;
extern struct roomproplistchunk *g_RoomPropListChunks;
extern struct prop *g_InteractProp;
extern u32 var8009cdac;
extern u32 var8009cdb0;
extern struct weaponobj *g_Proxies[30];
extern s32 var8009ce40;
extern s32 var8009ce44;
extern s32 var8009ce48;
extern s32 var8009ce4c;
extern s32 g_NumProjectiles;
extern s32 g_NumMonitorThings;
extern struct weaponobj *var8009ce58;
extern struct defaultobj *var8009ce5c;
extern struct var8009ce60 *var8009ce60;
extern struct defaultobj *var8009ce64;
extern struct projectile *g_Projectiles;
extern struct monitorthing *g_MonitorThings;
extern u32 var8009ce70;
extern u32 var8009ce74;
extern u32 var8009ce78[4];
extern u32 var8009ce88[4];
extern struct tvscreen var8009ce98;
extern struct tvscreen var8009cf10;
extern struct tvscreen var8009cf88;
extern struct stagesetup g_StageSetup;
extern u8 *g_GeCreditsData;
extern struct objective *g_Objectives[10];
extern u32 g_ObjectiveStatuses[10];
extern struct tag *g_TagsLinkedList;
extern struct briefingobj *g_BriefingObjs;
extern struct criteria_roomentered *g_RoomEnteredCriterias;
extern struct criteria_multiroomentered *g_MultiroomEnteredCriterias;
extern struct criteria_holograph *g_HolographCriterias;
extern u32 var8009d0d0[2];
extern s32 var8009d0d8;
extern struct audiohandle *g_BgunAudioHandles[4];
extern struct fireslot g_Fireslots[20];
extern struct casing g_Casings[20];
extern struct var8009da60 var8009da60[8];
extern struct lasersight g_LaserSights[4];
extern u32 var8009dd00;
extern u32 var8009dd04;
extern u32 var8009dd08;
extern u32 var8009dd0c;
extern u32 var8009dd10;
extern u32 var8009dd20;
extern u32 var8009dd24;
extern u32 var8009dd28;
extern u32 var8009dd2c;
extern u32 var8009dd30;
extern u32 var8009dd34;
extern u32 var8009dd38;
extern u32 var8009dd3c;
extern u32 var8009dd40;
extern u32 var8009dd44;
extern u32 var8009dd48;
extern u32 var8009dd4c;
extern u32 var8009dd50;
extern u32 var8009dd54;
extern u32 var8009dd58;
extern u32 var8009dd5c;
extern u32 var8009dd6c;
extern s32 g_DefaultWeapons[2];
extern s32 g_CutsceneCurAnimFrame60;
extern s16 g_CutsceneAnimNum;
extern s32 g_CutsceneFrameOverrun240;
extern s32 g_CutsceneSkipRequested;
extern f32 g_CutsceneCurTotalFrame60f;
extern s16 g_SpawnPoints[24];
extern s32 g_NumSpawnPoints;
extern u32 var8009de90;
extern u32 var8009de94;
extern u32 var8009de98;
extern u32 var8009de9c;
extern u8 g_MpSelectedPlayersForStats[4];
extern char g_CheatMarqueeString[252];
extern u32 var8009dfbc;
extern s32 var8009dfc0;
extern struct briefing g_Briefing;
extern struct missionconfig g_MissionConfig;
extern struct menu g_Menus[4];
extern struct menudata g_MenuData;
extern struct activemenu g_AmMenus[4];
extern s32 g_AmIndex;
extern u32 g_CheatsActiveBank0;
extern u32 g_CheatsActiveBank1;
extern u32 g_CheatsEnabledBank0;
extern u32 g_CheatsEnabledBank1;
extern struct savelocation_2d8 var800a21f8;
extern struct savefile_solo g_SoloSaveFile;
extern struct savelocation_2d8 g_FilemgrLoadedMainFile;
extern s8 g_SoloCompleted;
extern u8 g_AltTitle;
extern s32 *g_PadsFile;
extern u16 *g_PadOffsets;
extern u16 *g_CoverFlags;
extern s32 *g_CoverRooms;
extern struct covercandidate *g_CoverCandidates;
extern u16 g_NumSpecialCovers;
extern u16 *g_SpecialCoverNums;
extern struct var800a2380 var800a2380[5];
extern OSPfs g_Pfses[4];
extern u32 var800a33a0;
extern u32 var800a33a4;
extern struct explosion *g_Explosions;
extern s32 g_MaxExplosions;
extern struct smoke *g_Smokes;
extern s32 g_MaxSmokes;
extern Mtx var800a3448;
extern Mtx var800a3488;
extern struct spark g_Sparks[100];
extern u32 g_NextSparkIndex;
extern struct sparkgroup g_SparkGroups[10];
extern u32 var800a4130;
extern u8 *var800a41a0;
extern struct var800a41b0 *var800a41b0;
extern struct var800a41b0 *var800a41b4;
extern u32 var800a41b8;
extern s32 g_MaxShards;
extern struct shard *g_Shards;
extern Gfx *var800a4634;
extern struct room *g_Rooms;
extern u8 *g_MpRoomVisibility;
extern struct bgroom *g_BgRooms;
extern struct bgportal *g_BgPortals;
extern struct var800a4ccc *var800a4ccc;
extern u8 *g_BgLightsFileData;
extern s16 *g_RoomPortals;
extern struct var800a4cf0 var800a4cf0;
extern u32 var800a6470;
extern u32 var800a647c;
extern u32 var800a6538;
extern u32 var800a65b8;
extern struct screenbox var800a65c0;
extern struct screenbox var800a65c8;
extern u32 g_PortalDisableParentExec;
extern u32 g_FogDisabled;
extern u32 var800a65e4;
extern struct coord *var800a65e8;
extern u8 *var800a6660;
extern s16 *var800a6664;
extern s16 *var800a6668;
extern f32 *var800a666c;
extern Mtxf *var800a6670;
extern struct fileinfo g_FileInfo[2014];
extern u8 *g_VtxBuffers[3];
extern u8 *g_GfxMemPos;
extern u8 g_GfxActiveBufferIndex;
extern s32 g_MusicStageNum;
extern struct var800aa5d8 var800aa5d8[40];
extern struct var800aaa38 var800aaa38[3];
extern u32 g_AudioXReasonsActive[4];
extern u32 var800aaa78[4];
extern u32 g_AudioXReasonDurations[4];
extern u32 *g_LangBanks[69];
extern void *var800aabb4;
extern struct var800aabb8 *var800aabb8;
extern struct texture *g_Textures;
extern u32 var800aabc8[4];
extern u8 var800aabd8[2400];
extern u32 var800ab538;
extern u8 *var800ab540;
extern u32 var800ab544;
extern s32 var800ab548;
extern u32 var800ab558;
extern struct textureconfig *var800ab55c;
extern struct textureconfig *var800ab560;
extern Gfx *var800ab564;
extern Gfx *var800ab568;
extern struct var800ab570 *var800ab570;
extern struct textureconfig *var800ab574;
extern struct textureconfig *var800ab578;
extern struct textureconfig *var800ab57c;
extern struct textureconfig *var800ab580;
extern struct textureconfig *var800ab588;
extern struct textureconfig *var800ab58c;
extern struct textureconfig *var800ab590;
extern struct textureconfig *var800ab594;
extern struct textureconfig *var800ab598;
extern struct textureconfig *var800ab5a0;
extern struct textureconfig *var800ab5a4;
extern struct textureconfig *var800ab5a8;
extern struct textureconfig *var800ab5ac;
extern struct scenariodata g_ScenarioData;
extern u32 var800ac4cc;
extern struct chrdata *g_MpPlayerChrs[(4 + 8)];
extern struct mpchr *var800ac500[(4 + 8)];
extern s32 g_MpNumPlayers;
extern struct mpsim g_MpSimulants[8];
extern u8 g_MpSimulantDifficultiesPerNumPlayers[32];
extern struct mpplayer g_MpPlayers[6];
extern u8 g_AmBotCommands[16];
extern struct mpsetup g_MpSetup;
extern struct savefile_setup g_MpSetupSaveFile;
extern struct chrdata *g_MpSimulantChrs[8];
void amOpenPickTarget(void);
s32 menudialog000fcd48(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 amPickTargetMenuDialog(s32 operation, struct menudialog *dialog, union handlerdata *data);
void amSetAiBuddyTemperament(s32 aggressive);
void amSetAiBuddyStealth(void);
s32 amGetFirstBuddyIndex(void);
void amApply(s32 slot);
void amGetSlotDetails(s32 slot, u32 *flags, char *label);
void amInit(void);
s16 amCalculateSlotWidth(void);
void amChangeScreen(s32 step);
void amAssignWeaponSlots(void);
void amOpen(void);
void amClose(void);
s32 amIsCramped(void);
void amCalculateSlotPosition(s16 column, s16 row, s16 *x, s16 *y);
Gfx *amRenderText(Gfx *gdl, char *text, u32 colour, s16 left, s16 top);
Gfx *amRenderAibotInfo(Gfx *gdl, s32 buddynum);
Gfx *amRenderSlot(Gfx *gdl, char *text, s16 x, s16 y, s32 mode, s32 flags);
Gfx *amRender(Gfx *gdl);
void amTick(void);
f32 atan2f(f32 x, f32 z);
void roomUnpauseProps(u32 roomnum, s32 tintedglassonly);
void func0f157e94(s32 room, s32 arg1, struct screenbox *arg2);
void func0f158108(s32 roomnum, u8 *arg1, u8 *arg2);
struct var800a4640_00 *func0f158140(s32 roomnum);
u32 func0f158184(void);
u32 func0f158400(void);
u32 func0f158884(void);
u32 func0f158d9c(void);
u32 func0f1598b4(void);
u32 func0f159f1c(void);
u32 func0f15a0fc(void);
u32 func0f15a2c4(void);
u32 func0f15a6f4(void);
Gfx *func0f15b114(Gfx *gdl);
void bgLoadFile(void *memaddr, u32 offset, u32 len);
s32 stageGetIndex2(s32 stagenum);
f32 func0f15b274(s32 portal);
u8 func0f15b4c0(s32 portal);
u32 not(u32 arg);
u32 xorBabebabe(u32 value);
void bgInit(s32 stagenum);
void bgBuildTables(s32 stagenum);
void func0f15c850(void);
void func0f15c880(f32 arg0);
f32 func0f15c888(void);
f32 currentPlayerGetScaleBg2Gfx(void);
void currentPlayerSetScaleBg2Gfx(f32 arg0);
void func0f15c920(void);
void roomsTick(void);
Gfx *bgRender(Gfx *gdl);
Gfx *currentPlayerScissorToViewport(Gfx *gdl);
Gfx *currentPlayerScissorWithinViewportF(Gfx *gdl, f32 viewleft, f32 viewtop, f32 viewright, f32 viewbottom);
Gfx *currentPlayerScissorWithinViewport(Gfx *gdl, s32 viewleft, s32 viewtop, s32 viewright, s32 viewbottom);
void func0f15cd28(void);
s32 func0f15cd90(u32 room, struct screenbox *arg1);
s32 func0f15d08c(struct coord *a, struct coord *b);
s32 func0f15d10c(s32 portal, struct screenbox *arg1);
Gfx *boxRenderBorder(Gfx *gdl, s32 x1, s32 y1, s32 x2, s32 y2);
s32 boxGetIntersection(struct screenbox *a, struct screenbox *b);
void boxExpand(struct screenbox *a, struct screenbox *b);
void boxCopy(struct screenbox *dst, struct screenbox *src);
s32 roomIsVisibleByAnyPlayer(s32 room);
s32 roomIsVisibleByAnyAibot(s32 room);
s32 roomIsVisibleByPlayer(s32 room, u32 playernum);
s32 roomIsVisibleByAibot(s32 room, u32 aibotindex);
s32 func0f15d870(void *arg0);
u32 bgInflate(void *src, void *dst, u32 len);
u32 func0f15da00(void);
u32 func0f15dab4(void);
u32 func0f15dbb4(void);
void func0f15dc58(s32 roomnum);
void func0f15e474(s32 room);
void func0f15e538(void);
void func0f15e5b8(s32 size, u32 arg1);
void func0f15e728(void);
Gfx *func0f15e85c(Gfx *gdl, s32 roomnum, u32 arg2, s32 arg3);
Gfx *func0f15eb28(Gfx *gdl, s32 roomnum);
Gfx *func0f15ebd4(Gfx *gdl, s32 roomnum);
u32 func0f15ecd8(void);
u32 func0f15ef9c(void);
u32 func0f15f20c(void);
u32 func0f15f2b0(void);
u32 func0f15f560(void);
u32 func0f15ffdc(void);
u32 func0f160a38(void);
u32 func0f1612e4(void);
s32 func0f161520(struct coord *arg0, struct coord *arg1, s16 room, struct coord *arg3);
s32 func0f161ab4(s32 room);
s32 roomContainsCoord(struct coord *pos, s16 roomnum);
s32 func0f161c08(struct coord *arg0, s16 roomnum);
s32 func0f161d30(struct coord *arg0, s16 roomnum);
s32 func0f162128(struct coord *arg0, s16 roomnum);
void func0f162194(struct coord *pos, s16 *rooms1, s16 *rooms2, s32 len, s16 *arg4);
s32 portalPushValue(s32 value);
s32 portalPopValue(void);
s32 portalGetNthValueFromEnd(s32 offset);
struct portalcmd *portalCommandsExecute(struct portalcmd *cmd, s32 s2);
struct portalcmd *portalCommandsExecuteForCurrentPlayer(struct portalcmd *cmd);
u32 func0f162d9c(void);
void func0f1632d4(s16 roomnum1, s16 roomnum2, s16 arg2, struct screenbox *box);
void func0f163528(struct var800a4d00 *arg0);
s32 func0f163904(void);
u32 func0f16397c(void);
void func0f163e34(void);
Gfx *func0f164150(Gfx *gdl);
s32 roomsGetActive(s16 *rooms, s32 len);
s32 roomGetNeighbours(s32 room, s16 *rooms, s32 len);
s32 roomsAreNeighbours(s32 roomnum1, s32 roomnum2);
void currentPlayerCalculateScreenProperties(void);
void func0f1648cc(s32 roomnum);
void portalSwapRooms(u32 portal);
void func0f164ab8(s32 portalnum);
void func0f164c64(s32 roomnum);
void portalSetEnabled(s32 portal, s32 enable);
s32 func0f164e8c(struct coord *arg0, struct coord *arg1);
u32 func0f164f9c(void);
u32 func0f165004(void);
void func0f1650d0(struct coord *lower, struct coord *upper, s16 *rooms, s32 arg3, s32 arg4);
void bbikeInit(void);
void bbikeExit(void);
void bbikeUpdateVehicleOffset(void);
void bbikeTryDismountAngle(f32 relativeangle, f32 distance);
void bbikeHandleActivate(void);
void bbikeApplyMoveData(struct movedata *data);
void bbike0f0d2b40(struct defaultobj *bike, struct coord *arg1, f32 arg2, struct defaultobj *obstacle);
s32 bbikeCalculateNewPosition(struct coord *arg0, f32 arg1);
s32 bbikeCalculateNewPositionWithPush(struct coord *arg0, f32 arg1);
void bbikeUpdateVertical(struct coord *arg0);
s32 bbike0f0d3680(struct coord *arg0, struct coord *arg1, struct coord *arg2);
s32 bbike0f0d36d4(struct coord *arg0, struct coord *arg1, struct coord *arg2, struct coord *arg3, struct coord *arg4);
s32 bbike0f0d3840(struct coord *arg0, struct coord *arg1, struct coord *arg2);
s32 bbike0f0d3940(struct coord *arg0, struct coord *arg1, struct coord *arg2);
void bbike0f0d3c60(struct coord *arg0);
void bbikeTick(void);
void bcutsceneInit(void);
void bcutsceneTick(void);
f32 eyespyFindGround(s16 *floorroom);
s32 eyespyTryMoveUpwards(f32 yvel);
s32 eyespyCalculateNewPosition(struct coord *vel);
s32 eyespyCalculateNewPositionWithPush(struct coord *vel);
s32 eyespy0f0cf890(struct coord *arg0, struct coord *arg1, struct coord *arg2, struct coord *arg3, struct coord *arg4);
s32 eyespy0f0cf9f8(struct coord *arg0, struct coord *arg1, struct coord *arg2);
s32 eyespy0f0cfafc(struct coord *arg0, struct coord *arg1, struct coord *arg2);
s32 eyespy0f0cfdd0(struct coord *vel, struct coord *arg1, struct coord *arg2);
void eyespyUpdateVertical(void);
s32 eyespyTryLaunch(void);
void eyespyProcessInput(s32 allowbuttons);
void bgrabInit(void);
void bgrabExit(void);
void bgrab0f0ccbf0(struct coord *delta, f32 angle);
s32 bgrabTryMoveUpwards(f32 y);
s32 bgrabCalculateNewPosition(struct coord *delta, f32 angle, s32 arg2);
s32 bgrabCalculateNewPositiontWithPush(struct coord *delta, f32 angle, s32 arg2);
s32 bgrab0f0cdb04(f32 angle, s32 arg2);
void bgrab0f0cdb68(f32 angle);
void bgrab0f0cdef0(void);
s32 bgrab0f0cdf64(struct coord *delta, struct coord *arg1, struct coord *arg2);
s32 bgrab0f0cdfbc(struct coord *delta, struct coord *arg1, struct coord *arg2);
void bgrab0f0ce0bc(struct coord *arg0);
void bgrabUpdatePrevPos(void);
void bgrab0f0ce178(void);
void bgrabUpdateVertical(void);
void bgrabHandleActivate(void);
void bgrabUpdateSpeedSideways(f32 targetspeed, f32 accelspeed, s32 mult);
void bgrabUpdateSpeedForwards(f32 target, f32 speed);
void bgrabApplyMoveData(struct movedata *data);
void bgrabUpdateSpeedTheta(void);
void bgrab0f0ce924(void);
void bgrabTick(void);
void bgun0f097ba0(s32 handnum, s32 weaponnum);
s32 bgunGetUnequippedReloadIndex(s32 weaponnum);
void bgunTickUnequippedReload(void);
s32 bgun0f097df0(struct inventory_typef *arg0, struct hand *hand);
void bgun0f097e74(s16 partnum, s32 arg1, struct hand *hand, struct modelfiledata *arg3);
void bgun0f097f28(struct hand *hand, struct modelfiledata *arg1, struct inventory_typef *arg2);
void bgun0f098030(struct hand *hand, struct modelfiledata *arg1);
f32 bgun0f09815c(struct hand *hand);
u32 bgun0f0981e8(void);
s32 bgun0f098884(struct guncmd *cmd, struct gset *gset);
void bgun0f0988e0(struct guncmd *cmd, s32 handnum, struct hand *hand);
s32 bgun0f098a44(struct hand *hand, s32 time);
s32 bgunIsAnimBusy(struct hand *hand);
void bgunResetAnim(struct hand *hand);
void bgunGetWeaponInfo(struct handweaponinfo *info, s32 handnum);
s32 bgun0f098ca0(s32 arg0, struct handweaponinfo *info, struct hand *hand);
void bgun0f098df8(s32 weaponfunc, struct handweaponinfo *info, struct hand *hand, u8 onebullet, u8 checkunequipped);
void bgun0f098f8c(struct handweaponinfo *info, struct hand *hand);
s32 bgun0f099008(s32 handnum);
s32 bgun0f0990b0(struct weaponfunc *basefunc, struct weapon *weapon);
s32 bgun0f099188(struct hand *hand, s32 gunfunc);
s32 bgunTickIncIdle(struct handweaponinfo *info, s32 handnum, struct hand *hand, s32 lvupdate);
void bgun0f099780(struct hand *hand, f32 angle);
s32 bgunTickIncAutoSwitch(struct handweaponinfo *info, s32 handnum, struct hand *hand, s32 lvupdate);
s32 bgunIsReloading(struct hand *hand);
s32 bgunTickIncReload(struct handweaponinfo *info, s32 handnum, struct hand *hand, s32 lvupdate);
s32 bgunTickIncChangeFunc(struct handweaponinfo *info, s32 handnum, struct hand *hand, s32 lvupdate);
s32 bgun0f09a3f8(struct hand *hand, struct weaponfunc *func);
void bgun0f09a6f8(struct handweaponinfo *info, s32 handnum, struct hand *hand, struct weaponfunc *func);
s32 bgun0f09aba4(struct hand *hand, struct handweaponinfo *info, s32 handnum, struct weaponfunc_shoot *func);
s32 bgunTickIncAttackingShoot(struct handweaponinfo *info, s32 handnum, struct hand *hand);
s32 bgunTickIncAttackingThrow(s32 handnum, struct hand *hand);
s32 bgunGetMinClipQty(s32 weaponnum, s32 funcnum);
s32 bgunTickIncAttackingClose(s32 handnum, struct hand *hand);
s32 bgunTickIncAttackingSpecial(struct hand *hand);
s32 bgunTickIncAttackEmpty(struct handweaponinfo *info, s32 handnum, struct hand *hand, s32 lvupdate);
s32 bgunTickIncAttack(struct handweaponinfo *info, s32 handnum, struct hand *hand, s32 lvupdate);
s32 bgunIsReadyToSwitch(s32 handnum);
s32 bgun0f09bec8(s32 handnum);
s32 bgun0f09bf44(s32 handnum);
s32 bgunTickIncChangeGun(struct handweaponinfo *info, s32 handnum, struct hand *hand, s32 lvupdate);
s32 bgunTickIncState2(struct handweaponinfo *info, s32 handnum, struct hand *hand, s32 lvupdate);
s32 bgunTickInc(struct handweaponinfo *info, s32 handnum, s32 lvupdate);
s32 bgunSetState(s32 handnum, s32 state);
void bgunTick(s32 handnum);
void bgun0f09ce8c(void);
void bgun0f09ceac(void);
f32 bgunGetNoiseRadius(s32 handnum);
void bgunDecreaseNoiseRadius(void);
void bgunCalculateBlend(s32 hand);
u32 bgun0f09d550(void);
void bgun0f09d8dc(f32 breathing, f32 arg1, f32 arg2, f32 arg3, f32 arg4);
s32 bgun0f09dd7c(void);
u32 bgun0f09ddcc(void);
u8 *bgun0f09ddec(void);
u32 bgun0f09ddfc(void);
void bgun0f09df50(void);
void bgun0f09df64(s32 weaponnum);
void bgun0f09df9c(void);
s32 bgun0f09e004(s32 newowner);
u32 bgun0f09e144(void);
void bgun0f09e4e0(void);
void bgun0f09ea90(void);
s32 bgun0f09eae4(void);
s32 bgun0f09ebbc(void);
void bgun0f09ebcc(struct defaultobj *obj, struct coord *coord, s16 *rooms, Mtxf *matrix1, f32 *arg4, Mtxf *matrix2, struct prop *prop, struct coord *pos);
void bgun0f09ed2c(struct defaultobj *obj, struct coord *coord, Mtxf *arg2, f32 *arg3, Mtxf *arg4);
struct defaultobj *bgun0f09ee18(struct chrdata *chr, struct gset *gset, struct coord *pos, s16 *rooms, Mtxf *arg4, struct coord *arg5);
void bgunCreateThrownProjectile(s32 handnum, struct gset *gset);
void bgun0f09f848(s32 handnum);
void bgun0f09f974(s32 handnum, struct weaponfunc_shootprojectile *func);
void bgun0f09fa20(s32 handnum);
void bgunCreateFiredProjectile(s32 handnum);
void bgun0f0a0394(f32 autoaimx, f32 autoaimy, f32 damp, f32 arg3);
void bgunSwivelTowards(f32 screenx, f32 screeny, f32 damp);
void bgun0f0a0b98(f32 arg0, f32 arg1);
void bgunGetCrossPos(f32 *x, f32 *y);
void bgun0f0a0c08(struct coord *arg0, struct coord *arg1);
void bgun0f0a0c44(s32 handnum, struct coord *arg1, struct coord *arg2);
void bgunCalculateShotSpread(struct coord *arg0, struct coord *arg1, s32 handnum, s32 dorandom);
u32 bgun0f0a0fac(void);
void bgunSetLastShootInfo(struct coord *pos, struct coord *dir, s32 handnum);
u32 bgunGetUnk0c30(s32 handnum);
void bgun0f0a134c(s32 handnum);
void bgun0f0a1528(void);
void bgunEquipWeapon(s32 weaponnum);
s32 bgunGetWeaponNum(s32 handnum);
s32 bgun0f0a1a10(s32 weaponnum);
s32 bgun0f0a1a68(s32 arg0);
void bgun0f0a1ab0(void);
void bgunCycleForward(void);
void bgunCycleBack(void);
s32 bgunHasAmmoForWeapon(s32 weaponnum);
void bgunAutoSwitchWeapon(void);
void bgunEquipWeapon2(s32 arg0, s32 weaponnum);
s32 bgunIsFiring(s32 handnum);
s32 bgunGetAttackType(s32 handnum);
char *bgunGetName(s32 weaponnum);
u16 bgunGetNameId(s32 weaponnum);
char *bgunGetShortName(s32 arg0);
void bgunReloadIfPossible(s32 handnum);
void bgunSetAdjustPos(f32 angle);
void bgunResetSlideInc(s32 handnum);
u32 bgun0f0a233c(void);
f32 bgun0f0a2498(f32 arg0, f32 arg1, f32 arg2, f32 arg3);
void bgun0f0a24f0(struct coord *arg0, s32 handnum);
s32 bgun0f0a27c8(void);
void bgun0f0a29c8(void);
s32 bgunIsMissionCritical(s32 weaponnum);
void bgunLoseGun(struct prop *attacker);
u32 bgun0f0a2da8(void);
u32 bgun0f0a2e94(void);
void bgunDetonateRemoteMines(s32 playernum);
u32 bgun0f0a3160(void);
u32 bgun0f0a3490(void);
u32 bgun0f0a37b4(void);
u32 bgun0f0a3aa4(void);
u32 bgun0f0a3eac(void);
u32 bgun0f0a4094(void);
u32 bgun0f0a419c(void);
void bgun0f0a4334(struct hand *hand);
u32 bgun0f0a4438(void);
void bgun0f0a4570(struct hand *hand, s32 handnum, struct weaponfunc_shootprojectile *func);
void bgun0f0a45d0(struct hand *hand, struct modelfiledata *filedata, s32 arg2);
u32 bgun0f0a46a4(void);
u32 bgun0f0a4e44(void);
u32 bgun0f0a5300(void);
void bgun0f0a5550(s32 handnum);
void bgunTickMaulerCharge(void);
void bgun0f0a6c30(void);
s32 bgunAllocateFireslot(void);
void bgun0f0a7138(Gfx **gdl);
struct audiohandle **bgunAllocateAudioHandle(void);
void bgunPlayPropHitSound(struct gset *gset, struct prop *prop, s32 texturenum);
void bgun0f0a8404(struct coord *pos, s16 *rooms, s32 arg2);
void bgunPlayBgHitSound(struct gset *gset, struct coord *arg1, s32 texturenum, s16 *arg3);
void bgunSetTriggerOn(s32 handnum, s32 on);
s32 bgunConsiderToggleGunFunction(s32 usedowntime, s32 firing, s32 arg2);
void bgun0f0a8c50(void);
s32 bgunIsUsingSecondaryFunction(void);
void bgunsTick(s32 triggeron);
void bgunSetPassiveMode(s32 enable);
void bgunSetAimType(u32 aimtype);
void bgunSetAimPos(struct coord *coord);
void bgunSetHitPos(struct coord *coord);
void bgun0f0a9494(u32 operation);
void bgun0f0a94d0(u32 operation, struct coord *pos, struct coord *rot);
void bgunSetGunAmmoVisible(u32 reason, s32 enable);
void bgunSetAmmoQuantity(s32 ammotype, s32 quantity);
s32 bgunGetAmmoCountWithCheck(s32 type);
s32 bgunGetAmmoCount(s32 ammotype);
s32 bgunGetCapacityByAmmotype(u32 ammotype);
s32 bgunAmmotypeAllowsUnlimitedAmmo(u32 ammotype);
void bgunGiveMaxAmmo(s32 force);
u32 bgunGetAmmoTypeForWeapon(u32 weaponnum, u32 func);
s32 bgunGetAmmoQtyForWeapon(u32 weaponnum, u32 func);
void bgunSetAmmoQtyForWeapon(u32 weaponnum, u32 func, u32 quantity);
s32 bgunGetAmmoCapacityForWeapon(s32 weaponnum, s32 func);
Gfx *bgunRenderHudString(Gfx *gdl, char *text, s32 x, s32 halign, s32 y, s32 valign, u32 colour);
Gfx *bgunRenderHudInteger(Gfx *gdl, s32 value, s32 x, s32 halign, s32 y, s32 valign, u32 colour);
void bgunResetAbmag(struct abmag *abmag);
void bgun0f0a9da8(struct abmag *abmag, s32 remaining, s32 capacity, s32 height);
Gfx *bgunRenderHudGauge(Gfx *gdl, s32 x1, s32 y1, s32 x2, s32 y2, struct abmag *abmag, s32 remaining, s32 capacity, u32 vacantcolour, u32 occupiedcolour, s32 flip);
Gfx *bgunRenderHud(Gfx *gdl);
void bgunAddBoostc(s32 arg0);
void bgunSubtractBoost(s32 arg0);
void bgunApplyBoost(void);
void bgunRevertBoost(void);
void bgunTickBoost(void);
void bgunSetSightVisible(u32 bits, s32 visible);
Gfx *bgun0f0abcb0(Gfx *gdl);
void bgun0f0abd30(s32 handnum);
s32 bgunGetWeaponNum2(s32 handnum);
s8 bgunFreeFireslotWrapper(s32 fireslot);
s8 bgunFreeFireslot(s32 fireslot);
void bheadFlipAnimation(void);
void bheadUpdateIdleRoll(void);
void bheadUpdatePos(struct coord *vel);
void bheadUpdateRot(struct coord *lookvel, struct coord *upvel);
void bheadSetdamp(f32 headdamp);
void bheadUpdate(f32 arg0, f32 arg1);
void bheadAdjustAnimation(f32 speed);
void bheadStartDeathAnimation(s16 animnum, u32 flip, f32 fstarttime, f32 speed);
void bheadSetSpeed(f32 speed);
f32 bheadGetBreathingValue(void);
void bmoveSetControlDef(u32 controldef);
void bmoveSetAutoMoveCentreEnabled(s32 enabled);
void bmoveSetAutoAimY(s32 enabled);
s32 bmoveIsAutoAimYEnabled(void);
s32 bmoveIsAutoAimYEnabledForCurrentWeapon(void);
s32 bmoveIsInSightAimMode(void);
void bmoveUpdateAutoAimYProp(struct prop *prop, f32 autoaimy);
void bmoveSetAutoAimX(s32 enabled);
s32 bmoveIsAutoAimXEnabled(void);
s32 bmoveIsAutoAimXEnabledForCurrentWeapon(void);
void bmoveUpdateAutoAimXProp(struct prop *prop, f32 autoaimx);
struct prop *bmoveGetHoverbike(void);
struct prop *bmoveGetGrabbedProp(void);
void bmoveGrabProp(struct prop *prop);
void bmoveSetMode(u32 movemode);
void bmoveSetModeForAllPlayers(u32 movemode);
void bmoveHandleActivate(void);
void bmoveApplyMoveData(struct movedata *data);
void bmoveUpdateSpeedTheta(void);
f32 bmoveGetSpeedVertaLimit(f32 value);
void bmoveUpdateSpeedVerta(f32 value);
f32 bmoveGetSpeedThetaControlLimit(f32 value);
void bmoveUpdateSpeedThetaControl(f32 value);
f32 bmoveCalculateLookahead(void);
void bmoveResetMoveData(struct movedata *data);
void bmoveProcessInput(s32 allowc1x, s32 allowc1y, s32 allowc1buttons, s32 ignorec2);
void bmove0f0cb79c(struct player *player, struct coord *arg1, s16 *rooms);
void bmove0f0cb89c(struct player *player, s16 *rooms);
void bmove0f0cb8c4(struct player *player);
void bmove0f0cb904(struct coord *coord);
void bmove0f0cba88(f32 *a, f32 *b, struct coord *c, f32 mult1, f32 mult2);
void bmoveUpdateMoveInitSpeed(struct coord *newpos);
void bmoveTick(s32 allowc1x, s32 allowc1y, s32 allowc1buttons, s32 ignorec2);
void bmoveUpdateVerta(void);
void bmove0f0cc19c(struct coord *arg);
void bmoveUpdateHead(f32 arg0, f32 arg1, f32 arg2, Mtxf *arg3, f32 arg4);
void bmove0f0cc654(f32 arg0, f32 arg1, f32 arg2);
s32 bmoveGetCrouchPos(void);
s32 bmoveGetCrouchPosByPlayer(s32 playernum);
Gfx *bviewRenderIrRect(Gfx *gdl, s32 x1, s32 y1, s32 x2, s32 y2);
Gfx *bviewRenderLensRect(Gfx *gdl, void *arg1, s32 top, s32 arg3, s32 arg4, f32 arg5, s32 left, s32 width);
Gfx *bviewRenderFisheyeRect(Gfx *gdl, s32 arg1, f32 arg2, s32 arg3, s32 arg4);
Gfx *bviewPrepareStaticRgba16(Gfx *gdl, u32 colour, u32 alpha);
Gfx *bviewPrepareStaticI8(Gfx *gdl, u32 colour, u32 alpha);
Gfx *bviewRenderMotionBlur(Gfx *gdl, u32 colour, u32 alpha);
Gfx *bviewRenderStatic(Gfx *gdl, u32 arg1, s32 arg2);
Gfx *bviewRenderSlayerRocketLens(Gfx *gdl, u32 arg1, u32 arg2);
Gfx *bviewRenderFilmLens(Gfx *gdl, u32 colour, u32 alpha);
Gfx *bviewRenderZoomBlur(Gfx *gdl, u32 colour, s32 alpha, f32 arg3, f32 arg4);
f32 bview0f142d74(s32 arg0, f32 arg1, f32 arg2, f32 arg3);
Gfx *bviewRenderFisheye(Gfx *gdl, s32 arg1, u32 arg2, u32 arg3, u32 arg4, u32 arg5);
Gfx *bviewRenderEyespySideRect(Gfx *gdl, s32 *points, u8 r, u8 g, u8 b, u8 alpha);
Gfx *bviewRenderEyespyDecorations(Gfx *gdl);
Gfx *bviewRenderNvLens(Gfx *gdl);
Gfx *bviewRenderIrLens(Gfx *gdl);
Gfx *bviewRenderRarePresents(Gfx *gdl);
Gfx *bviewRenderHorizonScanner(Gfx *gdl);
Gfx *bviewRenderIrBinoculars(Gfx *gdl);
void bviewSetBlur(u32 bluramount);
void bviewClearBlur(void);
Gfx *bviewRenderNvBinoculars(Gfx *gdl);
void bwalkInit(void);
void bwalkSetSwayTarget(s32 value);
void bwalkAdjustCrouchPos(s32 value);
void bwalk0f0c3b38(struct coord *param_1, struct defaultobj *obj);
s32 bwalkTryMoveUpwards(f32 amount);
s32 bwalkCalculateNewPosition(struct coord *vel, f32 rotateamount, s32 apply, f32 extrawidth, s32 arg4);
s32 bwalkCalculateNewPositionWithPush(struct coord *delta, f32 rotateamount, s32 apply, f32 extrawidth, s32 types);
s32 bwalk0f0c4764(struct coord *delta, struct coord *arg1, struct coord *arg2, s32 types);
s32 bwalk0f0c47d0(struct coord *a, struct coord *b, struct coord *c, struct coord *d, struct coord *e, s32 types);
s32 bwalk0f0c494c(struct coord *a, struct coord *b, struct coord *c, s32 types);
s32 bwalk0f0c4a5c(struct coord *a, struct coord *b, struct coord *c, s32 types);
void bwalk0f0c4d98(void);
void bwalkUpdateSpeedSideways(f32 targetspeed, f32 accelspeed, s32 mult);
void bwalkUpdateSpeedForwards(f32 targetspeed, f32 accelspeed);
void bwalkUpdateVertical(void);
void bwalkApplyCrouchSpeed(void);
void bwalkUpdateCrouchOffsetReal(void);
void bwalkUpdateCrouchOffset(void);
void bwalkUpdateTheta(void);
void bwalk0f0c63bc(struct coord *arg0, u32 arg1, s32 types);
void bwalkUpdatePrevPos(void);
void bwalkHandleActivate(void);
void bwalkApplyMoveData(struct movedata *data);
void bwalkUpdateSpeedTheta(void);
u32 bwalk0f0c69b8(void);
void bwalkTick(void);
u32 func0f149c90(void);
u32 func0f149d58(void);
u32 func0f149e58(void);
u32 func0f14a00c(void);
u32 func0f14a06c(void);
u32 func0f14a16c(void);
u32 func0f14a1cc(void);
u32 func0f14a1ec(void);
u32 func0f14a20c(void);
u32 func0f14a240(void);
u32 func0f14a2fc(void);
void func0f14a328(void);
void func0f14a3c4(void);
u32 func0f14a52c(void);
u32 func0f14a560(void);
u32 func0f14a594(void);
u32 func0f14a5a4(void);
u32 func0f14a678(void);
u32 func0f14a8e8(void);
u32 func0f14a95c(void);
u32 func0f14a984(void);
u32 func0f14a9f8(void);
u32 func0f14aa48(void);
u32 func0f14aa70(void);
u32 func0f14aac4(void);
u32 func0f14ab3c(void);
u32 func0f14ad14(void);
u32 func0f14ad38(void);
u32 func0f14ad58(void);
u32 func0f14aea0(void);
u32 func0f14aed0(void);
u32 func0f14b178(void);
u32 func0f14b228(void);
u32 func0f14b394(void);
u32 func0f14b484(void);
u32 func0f14bc04(void);
u32 func0f14bd34(void);
u32 func0f14bdbc(void);
u32 func0f14bec8(void);
u32 func0f14c1cc(void);
u32 func0f14c50c(void);
u32 func0f14c75c(void);
u32 func0f14c7dc(void);
u32 func0f14c814(void);
u32 func0f14c870(void);
u32 func0f14cdb8(void);
u32 func0f14cf6c(void);
u32 func0f14d064(void);
u32 func0f14d2c8(void);
u32 func0f14d4f0(void);
u32 func0f14d714(void);
u32 func0f14d84c(void);
u32 func0f14d8d8(void);
u32 func0f14dac0(void);
u32 func0f14dc30(void);
u32 func0f14def0(void);
u32 func0f14dfc0(void);
u32 func0f14e1c4(void);
u32 func0f14e4ac(void);
u32 func0f14e790(void);
u32 func0f14e7e0(void);
u32 func0f14e884(void);
u32 func0f14ec2c(void);
u32 func0f14ecd8(void);
u32 func0f14eeb0(void);
u32 func0f14ef50(void);
u32 func0f14f008(void);
Gfx *func0f14f07c(Gfx *gdl, s32 headorbodynum, s32 x1, s32 y1, s32 x2, s32 y2);
u32 func0f14f4b8(void);
u32 func0f14f4e4(void);
u32 func0f14f510(void);
u32 func0f14f700(void);
u32 func0f14f76c(void);
u32 func0f14f7d4(void);
u32 func0f14f8cc(void);
u32 func0f14f974(void);
u32 func0f14faf8(void);
u32 func0f14fbfc(void);
u32 func0f14fdb0(void);
u32 func0f14ff94(void);
u32 func0f150068(void);
u32 func0f15015c(void);
u32 func0f1507b4(void);
extern const char var7f1b6050[];
extern const char var7f1b6058[];
extern const char var7f1b606c[];
extern const char var7f1b6080[];
extern const char var7f1b6088[];
extern const char var7f1b6090[];
extern const char var7f1b6098[];
void casingsReset(void);
void casingTick(struct casing *arg0);
void casingsTick(void);
f32 ceilf(f32 value);
s32 ceil(f32 value);
extern struct menudialog g_CheatsMenuDialog;
u32 cheatIsUnlocked(s32 cheat_id);
s32 cheatIsActive(s32 cheat_id);
void cheatActivate(s32 cheat_id);
void cheatDeactivate(s32 cheat_id);
void cheatsDisableAll(void);
void cheatsActivate(void);
char *cheatGetNameIfUnlocked(struct menuitem *item);
char *cheatGetMarquee(struct menuitem *item);
s32 cheatGetByTimedStageIndex(s32 stage_index, s32 difficulty);
s32 cheatGetByCompletedStageIndex(s32 stage_index);
s32 cheatGetTime(s32 cheat_id);
char *cheatGetName(s32 cheat_id);
s32 cheatMenuHandleDialog(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 cheatMenuHandleCheatCheckbox(s32 operation, struct menuitem *item, union handlerdata *data);
s32 cheatMenuHandleBuddyCheckbox(s32 operation, struct menuitem *item, union handlerdata *data);
s32 cheatMenuHandleTurnOffAllCheats(s32 operation, struct menuitem *item, union handlerdata *data);
void propsTick2(void);
void chrSetChrnum(struct chrdata *chr, s16 chrnum);
void chrDeregister(s32 chrnum);
void chrCalculatePushPos(struct chrdata *chr, struct coord *pos, s16 *rooms, s32 arg3);
s32 func0f01f264(struct chrdata *chr, struct coord *pos, s16 *room, f32 arg3, s32 arg4);
void func0f01f378(void);
s32 getNumFreeChrSlots(void);
s16 getNextUnusedChrnum(void);
struct prop *func0f020b14(struct prop *prop, struct model *model, struct coord *pos, s16 *rooms, f32 arg3, u8 *ailist);
void func0f020d44(struct prop *prop, s32 removechr);
void chrUpdateAimProperties(struct chrdata *chr);
void chrFlinchBody(struct chrdata *chr);
void chrFlinchHead(struct chrdata *chr, f32 arg1);
f32 func0f02143c(struct chrdata *chr);
void func0f021fa8(struct chrdata *chr, struct coord *pos, s16 *rooms);
void func0f0220ec(struct chrdata *chr, s32 arg1, s32 arg2);
u32 func0f022214(void);
void chrUpdateCloak(struct chrdata *chr);
s32 func0f022be4(struct chrdata *chr);
void chrSetPoisoned(struct chrdata *chr, struct prop *poisonprop);
void chrTickPoisoned(struct chrdata *chr);
s32 chrTickBeams(struct prop *prop);
s32 func0f023098(struct prop *prop);
void chrDropItems(struct chrdata *chr);
void chrSetHudpieceVisible(struct chrdata *chr, s32 visible);
void chrDropWeapons(struct chrdata *chr);
void func0f0246e4(u8 *arg0);
s32 func0f024738(struct chrdata *chr);
s32 func0f024b18(struct model *model, struct modelnode *node);
void chrRenderAttachedObject(struct prop *prop, struct modelrenderdata *renderdata, s32 withalpha, struct chrdata *chr);
void bodyGetBloodColour(s16 bodynum, u8 *colour1, u32 *colour2);
Gfx *chrRender(struct prop *prop, Gfx *gdl, s32 withalpha);
void chrEmitSparks(struct chrdata *chr, struct prop *prop, s32 hitpart, struct coord *coord, struct coord *coord2, struct chrdata *chr2);
u32 func0f0260c4(void);
u32 func0f0268bc(void);
u32 func0f0270f4(void);
f32 func0f0278a4(struct chrdata *chr);
void func0f027994(struct prop *prop, struct shotdata *shotdata, s32 arg2, s32 arg3);
void func0f027e1c(struct shotdata *shotdata, struct hit *hit);
void func0f028498(s32 value);
void chrsCheckForNoise(f32 noiseradius);
s32 func0f028a50(struct prop *prop, struct coord *arg1, f32 *arg2, f32 *arg3);
s32 func0f028e18(struct prop *arg0, struct modelnode *node, struct model *model, struct prop *arg3);
s32 func0f028e6c(s32 arg0, struct prop *prop, struct prop **propptr, struct modelnode **nodeptr, struct model **modelptr);
void func0f028f7c(struct prop *prop, f32 shield, struct prop *arg2, struct modelnode *node, struct model *model, s32 side, s16 *arg6);
void func0f0291d4(struct var80062a8c *thing);
void func0f0292bc(struct prop *prop);
s32 func0f02932c(struct prop *prop, s32 arg1);
s32 func0f0293ec(struct prop *prop, s32 arg1);
s32 func0f0294cc(struct prop *prop, s32 arg1);
void func0f0295f8(f32 arg0, s32 *arg1, s32 *arg2, s32 *arg3);
f32 propGetShieldThing(struct prop **propptr);
u32 func0f02983c(void);
Gfx *func0f02b7d4(Gfx *gdl, struct prop *arg1, struct prop *arg2, s32 alpha, s32 arg4, s32 arg5, s32 arg6, s32 arg7, s32 arg8);
Gfx *func0f02bdf8(Gfx *gdl, struct prop *chr1, struct prop *chr2);
Gfx *chrRenderShield(Gfx *gdl, struct chrdata *chr, u32 alpha);
u32 func0f02c9b0(void);
void chrSetDrCarollImages(struct chrdata *drcaroll, s32 imageleft, s32 imageright);
s32 getNumChrSlots(void);
void chrRegister(s32 chrnum, s32 chrindex);
struct gfxvtx *chrAllocateVertices(s32 numvertices);
void setVar8006297c(u32 arg0);
u32 getVar8006297c(void);
void setVar80062980(u32 arg0);
u32 getVar80062980(void);
void chrSetOrUnsetHiddenFlag00000100(struct chrdata *chr, s32 unset);
void chrSetMaxDamage(struct chrdata *chr, f32 maxdamage);
f32 chrGetMaxDamage(struct chrdata *chr);
void chrAddHealth(struct chrdata *chr, f32 health);
f32 chrGetArmor(struct chrdata *chr);
void chrInit(struct prop *prop, u8 *ailist);
struct prop *propAllocateChr(struct model *model, struct coord *pos, s16 *rooms, f32 faceangle, u8 *ailist);
void propClearReferences(s32 propnum);
void func0f022084(struct chrdata *chr, s16 *room);
void func0f0220ac(struct chrdata *chr);
void chrCloak(struct chrdata *chr, s32 arg1);
void chrUncloak(struct chrdata *chr, s32 value);
void chrUncloakTemporarily(struct chrdata *chr);
void func0f02472c(void);
void func0f028490(f32 arg1);
struct chrdata *chrFindByLiteralId(s32 chrnum);
struct prop *chrGetEquippedWeaponProp(struct chrdata *chr, s32 hand);
struct prop *chrGetEquippedWeaponPropWithCheck(struct chrdata *chr, s32 hand);
struct prop *chrGetTargetProp(struct chrdata *chr);
s32 chrUpdateGeometry(struct prop *prop, u8 **start, u8 **end);
void propChrGetBbox(struct prop *prop, f32 *width, f32 *ymax, f32 *ymin);
extern struct attackanimconfig var800656c0[];
extern struct attackanimconfig var80065be0[];
extern struct attackanimconfig var800663d8[];
extern struct attackanimconfig g_AttackAnimHeavyWalk;
extern struct attackanimconfig g_AttackAnimHeavyRun;
extern struct attackanimconfig g_AttackAnimLightWalk;
extern struct attackanimconfig g_AttackAnimLightRun;
extern struct attackanimconfig g_AttackAnimDualWalk;
extern struct attackanimconfig g_AttackAnimDualRun;
f32 func0f02dff0(s16 animnum);
s32 func0f02e064(struct chrdata *chr);
s32 weaponIsOneHanded(struct prop *prop);
f32 chrGetRangedSpeed(struct chrdata *chr, f32 min, f32 max);
s32 chrGetPercentageOfSlowness(struct chrdata *chr, s32 percentage);
f32 chrGetRangedArghSpeed(struct chrdata *chr, f32 min, f32 max);
f32 chrGetAttackEntityRelativeAngle(struct chrdata *chr, s32 attackflags, s32 entityid);
f32 chrGetAttackEntityDistance(struct chrdata *chr, u32 attackflags, s32 entityid);
void func0f02e3dc(struct coord *a, struct coord *b, struct coord *c, struct coord *d, struct coord *dst);
void func0f02e4f8(struct coord *arg0, struct coord *arg1, struct coord *dst);
f32 func0f02e550(struct prop *prop, f32 arg1, f32 arg2, u32 arg3, f32 ymax, f32 ymin);
f32 func0f02e684(struct prop *prop, f32 arg1, f32 arg2);
void chrStandChooseAnimation(struct chrdata *chr, f32 mergetime);
void chrStand(struct chrdata *chr);
s32 chrFaceCover(struct chrdata *chr);
void chrKneelChooseAnimation(struct chrdata *chr);
void chrStartAlarmChooseAnimation(struct chrdata *chr);
void chrThrowGrenadeChooseAnimation(struct chrdata *chr);
void chrSurprisedChooseAnimation(struct chrdata *chr);
void chrDoSurprisedOneHand(struct chrdata *chr);
void chrSurrenderChooseAnimation(struct chrdata *chr);
void chrSidestepChooseAnimation(struct chrdata *chr);
void chrJumpOutChooseAnimation(struct chrdata *chr);
void chrRunPosChooseAnimation(struct chrdata *chr);
void chrAttackStand(struct chrdata *chr, u32 attackflags, s32 entityid);
void chrAttackLie(struct chrdata *chr, u32 attackflags, s32 entityid);
void chrAttackKneel(struct chrdata *chr, u32 attackflags, s32 entityid);
void chrAttackWalkChooseAnimation(struct chrdata *chr);
void chrAttackWalk(struct chrdata *chr, s32 run);
void chrAttackRollChooseAnimation(struct chrdata *chr);
void chrAttackRoll(struct chrdata *chr, s32 toleft);
void chrStartAnim(struct chrdata *chr, s32 animnum, f32 startframe, f32 endframe, u8 chranimflags, s32 merge, f32 speed);
void func0f031254(struct chrdata *chr);
void chrAttack(struct chrdata *chr, struct attackanimgroup **animgroups, s32 flip, s32 *firing, u32 attackflags, s32 entityid, s32 standing);
void chrAttackAmount(struct chrdata *chr, u32 attackflags, u32 entityid, u32 maxshots);
void chrBeginDeath(struct chrdata *chr, struct coord *dir, f32 relangle, s32 hitpart, struct gset *gset, s32 knockout, s32 aplayernum);
void chrBeginArgh(struct chrdata *chr, f32 angle, s32 hitpart);
void chrReactToDamage(struct chrdata *chr, struct coord *dir, f32 angle, s32 hitpart, struct gset *gset, s32 playernum);
void chrYeetFromPos(struct chrdata *chr, struct coord *pos, f32 arg2);
s32 gsetGetBlurAmount(struct gset *gset);
void chrKnockOut(struct chrdata *chr, f32 angle, s32 hitpart, struct gset *gset);
s32 chrIsAnimPreventingArgh(struct chrdata *chr, f32 *arg1);
void chrChoke(struct chrdata *chr, s32 choketype);
void chrSetShield(struct chrdata *chr, f32 shield);
s32 func0f034080(struct chrdata *chr, struct modelnode *node, struct prop *prop, struct model *model, s32 side, s16 *arg5);
void chrDamageByMisc(struct chrdata *chr, f32 damage, struct coord *vector, struct gset *gset, struct prop *prop);
void chrDamageByLaser(struct chrdata *chr, f32 damage, struct coord *vector, struct gset *gset, struct prop *prop);
void func0f0341dc(struct chrdata *chr, f32 damage, struct coord *vector, struct gset *gset, struct prop *prop, u32 hitpart, struct prop *prop2, struct modelnode *node, struct model *model, s32 side, s16 *arg10);
void chrDamageByImpact(struct chrdata *chr, f32 damage, struct coord *vector, struct gset *gset, struct prop *prop, s32 arg5);
void chrDamageByExplosion(struct chrdata *chr, f32 damage, struct coord *vector, struct prop *prop, struct coord *explosionpos);
void playerUpdateDamageStats(struct prop *attacker, struct prop *victim, f32 damage);
void chrDamage(struct chrdata *chr, f32 damage, struct coord *vector, struct gset *gset, struct prop *aprop, s32 hitpart, s32 damageshield, struct prop *prop2, struct modelnode *node, struct model *model, s32 side, s16 *arg11, s32 explosion, struct coord *explosionpos);
u32 func0f03645c(void);
s32 func0f03654c(struct chrdata *chr, struct coord *pos, s16 *rooms, struct coord *pos2, s16 *rooms2, struct coord *vector, f32 arg6, u32 types);
s32 propchrHasClearLineToPos(struct prop *prop, struct coord *dstpos, struct coord *vector);
s32 propchrHasClearLineInVector(struct prop *prop, struct coord *coord, f32 arg2);
s32 func0f036974(struct prop *prop, struct coord *coord);
void chrGetSideVectorToTarget(struct chrdata *chr, s32 side, struct coord *vector);
s32 chrCanRollInDirection(struct chrdata *chr, s32 toleft, f32 distance);
void chrGetSideVector(struct chrdata *chr, s32 side, struct coord *vector);
s32 chrCanJumpInDirection(struct chrdata *chr, s32 side, f32 distance);
s32 func0f036c08(struct chrdata *chr, struct coord *arg1, s16 *rooms);
void chrGoPosInitMagic(struct chrdata *chr, struct waydata *waydata, struct coord *arg2, struct coord *prevpos);
void chrGoPosGetCurWaypointInfoWithFlags(struct chrdata *chr, struct coord *pos, s16 *rooms, u32 *flags);
f32 func0f0370a8(struct chrdata *chr);
s32 chrGoPosCalculateBaseTtl(struct chrdata *chr);
void chrGoPosConsiderRestart(struct chrdata *chr);
void chrGoPosInitExpensive(struct chrdata *chr);
void chrGoPosAdvanceWaypoint(struct chrdata *chr);
s32 chrPatrolCalculateStep(struct chrdata *chr, s32 *forward, s32 numsteps);
s16 chrPatrolCalculatePadNum(struct chrdata *chr, s32 numsteps);
void chrPatrolGetCurWaypointInfoWithFlags(struct chrdata *chr, struct coord *pos, s16 *rooms, u32 *flags);
void func0f037580(struct chrdata *chr);
void func0f0375b0(struct chrdata *chr);
void chrGoPosTickMagic(struct chrdata *chr, struct waydata *waydata, f32 speed, struct coord *arg3, s16 *rooms);
void chrCalculatePosition(struct chrdata *chr, struct coord *pos);
void chrGoPosChooseAnimation(struct chrdata *chr);
s32 chrGoToPos(struct chrdata *chr, struct coord *pos, s16 *rooms, u32 flags);
void chrPatrolChooseAnimation(struct chrdata *chr);
void chrStartPatrol(struct chrdata *chr, struct path *path);
s32 chrCanSeeEntity(struct chrdata *chr, struct coord *chrpos, s16 *chrrooms, s32 allowextraheight, u32 attackflags, u32 entityid);
s32 chrCanSeeAttackTarget(struct chrdata *chr, struct coord *pos, s16 *rooms, s32 allowextraheight);
s32 chrCanSeeChr(struct chrdata *chr, struct chrdata *target, s16 *room);
s32 chrHasLineOfSightToPos(struct chrdata *viewerchr, struct coord *pos, s16 *rooms);
s32 chrCanSeePos(struct chrdata *chr, struct coord *pos, s16 *room);
s32 chrCanSeeProp(struct chrdata *chr, struct prop *prop);
s32 chrIsStopped(struct chrdata *chr);
s32 chrCheckTargetInSight(struct chrdata *chr);
s32 chrIsReadyForOrders(struct chrdata *chr);
s32 chrTrySidestep(struct chrdata *chr);
s32 chrTryJumpOut(struct chrdata *chr);
s32 chrTryRunSideways(struct chrdata *chr);
s32 chrTryAttackWalk(struct chrdata *chr);
s32 chrTryAttackRun(struct chrdata *chr);
s32 chrTryAttackRoll(struct chrdata *chr);
s32 chrTryAttackAmount(struct chrdata *chr, u32 arg1, u32 arg2, u8 arg3, u8 arg4);
s32 chrGoToPad(struct chrdata *chr, s32 padnum, u32 speed);
s32 func0f03abd0(struct chrdata *chr, struct coord *pos, u32 flags);
s32 func0f03aca0(struct chrdata *chr, f32 arg1, u8 arg2);
s32 chrTryRunFromTarget(struct chrdata *chr);
s32 chrGoToCoverProp(struct chrdata *chr);
s32 chrDropItem(struct chrdata *chr, u32 modelnum, u32 weaponnum);
void chrPunchInflictDamage(struct chrdata *chr, s32 damage, s32 range, u8 reverse);
s32 chrTryPunch(struct chrdata *chr, u8 reverse);
struct eyespy *chrToEyespy(struct chrdata *chr);
void chrTickStand(struct chrdata *chr);
void chrTickAnim(struct chrdata *chr);
void chrTickSurrender(struct chrdata *chr);
void chrTickDead(struct chrdata *chr);
void chrAlertOthersOfInjury(struct chrdata *chr, s32 dying);
void chrTickDie(struct chrdata *chr);
void chrTickDruggedComingUp(struct chrdata *chr);
void chrTickDruggedDrop(struct chrdata *chr);
void chrTickDruggedKo(struct chrdata *chr);
void chrTickArgh(struct chrdata *chr);
void chrTickPreArgh(struct chrdata *chr);
void chrTickSidestep(struct chrdata *chr);
void chrTickJumpOut(struct chrdata *chr);
void chrTickStartAlarm(struct chrdata *chr);
void chrTickSurprised(struct chrdata *chr);
void chrCreateFireslot(struct chrdata *chr, s32 handnum, s32 withsound, s32 withbeam, struct coord *from, struct coord *to);
f32 chrGetInverseTheta(struct chrdata *chr);
f32 chrGetAimAngle(struct chrdata *chr);
f32 func0f03e754(struct chrdata *chr);
s32 chrTurn(struct chrdata *chr, s32 turning, f32 endanimframe, f32 speed, f32 toleranceangle);
s32 func0f03e9f4(struct chrdata *chr, struct attackanimconfig *animcfg, s32 arg2, s32 arg3, f32 arg4);
void chrCalculateAimEndProperties(struct chrdata *chr, struct attackanimconfig *animcfg, s32 hasleftgun, s32 hasrightgun, f32 shootrotx);
f32 chrGetAimLimitAngle(f32 sqdist);
void chrCalculateHit(struct chrdata *chr, s32 *angleokptr, s32 *hit, struct gset *gset);
s32 func0f03fde4(struct chrdata *chr, s32 handnum, struct coord *arg2);
void chrCalculateShieldHit(struct chrdata *chr, struct coord *pos, struct coord *vector, struct modelnode **node, s32 *hitpart, struct model **model, s32 *side);
void func0f04031c(struct coord *coord, f32 arg1, struct coord *coord2, f32 *arg3);
u32 func0f0404d4(void);
u32 func0f041a74(void);
s32 func0f041c44(struct chrdata *chr);
void chrAttackAmountUpdateAnimation(struct chrdata *chr);
void chrTickFire(struct chrdata *chr);
void chrTickAttackAmount(struct chrdata *chr);
void robotSetMuzzleFlash(struct chrdata *chr, s32 right, s32 enabled);
void robotAttack(struct chrdata *chr);
void chrTickRobotAttack(struct chrdata *chr);
void chrTickAttack(struct chrdata *chr);
void chrTickAttackRoll(struct chrdata *chr);
void propPrintDangerous(void);
void chrTickThrowGrenade(struct chrdata *chr);
s32 func0f043f2c(struct chrdata *chr, struct coord *runpos, u32 arg2, f32 *turnspeed);
void chrTickAttackWalk(struct chrdata *chr);
s32 posIsMovingTowardsPosOrStoppedInRange(struct coord *prevpos, struct coord *moveddelta, struct coord *targetpos, f32 range);
s32 posIsArrivingLaterallyAtPos(struct coord *prevpos, struct coord *curpos, struct coord *targetpos, f32 range);
s32 posIsArrivingAtPos(struct coord *prevpos, struct coord *curpos, struct coord *targetpos, f32 range);
void chrTickRunPos(struct chrdata *chr);
u32 func0f044b68(void);
u32 func0f044c38(void);
u32 func0f0451a8(void);
u32 func0f045760(void);
struct prop *chrOpenDoor(struct chrdata *chr, struct coord *coord);
void func0f045b9c(struct chrdata *chr, struct coord *pos, struct waydata *waydata, s32 arg3);
s32 goPosUpdateLiftAction(struct chrdata *chr, u32 curpadflags, s32 arg2, s32 arrivingatlift, s16 curpadnum, s32 nextpadnum);
s16 chrGoPosGetNextPadNum(struct chrdata *chr);
void chrTickGoPos(struct chrdata *chr);
void chrTickPatrol(struct chrdata *chr);
s32 chrStartSkJump(struct chrdata *chr, u8 arg1, u8 arg2, s32 arg3, u8 arg4);
void chrTickSkJump(struct chrdata *chr);
void chrTick(struct chrdata *chr);
void func0f0482cc(u32 ailistid);
void func0f048398(void);
f32 chrGetAngleToPos(struct chrdata *chr, struct coord *pos);
void chrGetAttackEntityPos(struct chrdata *chr, u32 attackflags, s32 entityid, struct coord *pos, s16 *rooms);
f32 chrGetAngleFromTargetsFov(struct chrdata *chr);
f32 chrGetVerticalAngleToTarget(struct chrdata *chr);
s32 chrIsInTargetsFovX(struct chrdata *chr, u8 fov360);
s32 chrIsVerticalAngleToTargetWithin(struct chrdata *chr, u8 arg1);
f32 func0f048fcc(struct chrdata *chr, u8 reverse);
s32 chrIsTargetInFov(struct chrdata *chr, u8 arg1, u8 reverse);
s32 func0f04911c(struct chrdata *chr, struct coord *pos, u8 arg2);
f32 chrGetSameFloorDistanceToPad(struct chrdata *chr, s32 pad_id);
void chrsClearRefsToPlayer(s32 playernum);
s32 chrResolveId(struct chrdata *ref, s32 id);
f32 chrGetTimer(struct chrdata *chr);
s32 chrCanSeeTargetWithExtraCheck(struct chrdata *chr);
s32 chrSawInjury(struct chrdata *chr, u8 arg1);
s32 chrSawDeath(struct chrdata *chr, u8 arg1);
s32 waypointIsWithin90DegreesOfPosAngle(struct waypoint *waypoint, struct coord *pos, f32 angle);
s32 chrFindWaypointWithinPosQuadrant(struct coord *pos, s16 *rooms, f32 angle, u8 quadrant);
s32 func0f04a4ec(struct chrdata *chr, u8 quadrant);
s32 chrSetPadPresetToWaypointWithinTargetQuadrant(struct chrdata *chr, u8 arg1);
s32 chrSetChrPresetToChrNearPad(u32 checktype, struct chrdata *chr, f32 distance, s32 padnum);
s32 chrSetChrPresetToChrNearPos(u8 checktype, struct chrdata *chr, f32 distance, struct coord *pos, s16 *room);
s32 chrSetPadPresetToPadOnRouteToTarget(struct chrdata *chr);
s32 func0f04aeb0(struct coord *pos, s16 *rooms);
s32 chrAdjustPosForSpawn(f32 chrwidth, struct coord *pos, s16 *rooms, f32 angle, s32 arg4, s32 arg5);
struct prop *chrSpawnAtCoord(s32 body, s32 head, struct coord *pos, s16 *room, f32 angle, u8 *ailist, u32 spawnflags);
s32 func0f04b658(struct chrdata *chr);
s32 chrMoveToPos(struct chrdata *chr, struct coord *pos, s16 *room, f32 angle, s32 allowonscreen);
s32 chrCheckCoverOutOfSight(struct chrdata *chr, s32 covernum, s32 arg2);
s32 chrAssignCoverByCriteria(struct chrdata *chr, u16 arg1, s32 arg2);
s32 chrAssignCoverAwayFromDanger(struct chrdata *chr, s32 mindist, s32 maxdist);
s32 chrRunFromPos(struct chrdata *chr, u32 speed, f32 distance, struct coord *frompos);
f32 func0f04c784(struct chrdata *chr);
s32 func0f04c874(struct chrdata *chr, u32 angle360, struct coord *pos, u8 arg3, u8 arg4);
u32 rebuildTeams(void);
u32 rebuildSquadrons(void);
void chrAvoid(struct chrdata *chr);
void func0f02e9a0(struct chrdata *chr, f32 mergetime);
void func0f02ed28(struct chrdata *chr, f32 mergetime);
void chrStop(struct chrdata *chr);
void chrKneel(struct chrdata *chr);
void chrStartAlarm(struct chrdata *chr);
void chrThrowGrenade(struct chrdata *chr, s32 arg1, s32 arg2);
void chrDoSurprisedSurrender(struct chrdata *chr);
void chrDoSurprisedLookAround(struct chrdata *chr);
void chrSurrender(struct chrdata *chr);
void chrSidestep(struct chrdata *chr, s32 side);
void chrJumpOut(struct chrdata *chr, s32 side);
void chrRunToPos(struct chrdata *chr, struct coord *pos);
void chrBeginDead(struct chrdata *chr);
f32 chrGetShield(struct chrdata *chr);
void chrDie(struct chrdata *chr, s32 aplayernum);
void chrGoPosGetCurWaypointInfo(struct chrdata *chr, struct coord *pos, s16 *rooms);
void chrGoPosClearRestartTtl(struct chrdata *chr);
void chrPatrolGetCurWaypointInfo(struct chrdata *chr, struct coord *pos, s16 *rooms);
struct path *pathFindById(u32 path_id);
void chrRecordLastVisibleTargetTime(struct chrdata *chr);
s32 chrCanSeeTarget(struct chrdata *chr);
void chrRecordLastSeeTargetTime(struct chrdata *chr);
void chrRecordLastHearTargetTime(struct chrdata *chr);
s32 chrIsDead(struct chrdata *chr);
s32 chrTryAttackStand(struct chrdata *chr, u32 attackflags, s32 entityid);
s32 chrTryAttackKneel(struct chrdata *chr, u32 attackflags, s32 entityid);
s32 chrTryAttackLie(struct chrdata *chr, u32 attackflags, s32 entityid);
s32 chrTryModifyAttack(struct chrdata *chr, u32 attackflags, s32 entityid);
s32 chrFaceEntity(struct chrdata *chr, u32 attackflags, s32 entityid);
s32 chrSetPath(struct chrdata *chr, u32 path_id);
s32 chrTryStartPatrol(struct chrdata *chr);
s32 chrTrySurrender(struct chrdata *chr);
s32 chrFadeOut(struct chrdata *chr);
s32 chrGoToTarget(struct chrdata *chr, u32 speed);
s32 chrGoToChr(struct chrdata *chr, u32 dst_chrnum, u32 speed);
s32 chrGoToProp(struct chrdata *chr, struct prop *prop, s32 speed);
s32 chrTryStop(struct chrdata *chr);
s32 chrTrySurprisedOneHand(struct chrdata *chr);
s32 chrTrySurprisedSurrender(struct chrdata *chr);
s32 chrTrySurprisedLookAround(struct chrdata *chr);
s32 chrTryKneel(struct chrdata *chr);
s32 chrTryStartAnim(struct chrdata *chr, s32 animnum, f32 startframe, f32 endframe, u8 chranimflags, s32 merge, f32 speed);
s32 chrTryStartAlarm(struct chrdata *chr, s32 pad_id);
s32 chrConsiderGrenadeThrow(struct chrdata *chr, u32 attackflags, u32 entityid);
void chrTickKneel(struct chrdata *chr);
void chrFadeCorpse(struct chrdata *chr);
void chrFadeCorpseWhenOffScreen(struct chrdata *chr);
void chrTickTest(struct chrdata *chr);
void chrSetLookAngle(struct chrdata *chr, f32 angle);
f32 func0f03e578(struct chrdata *chr);
void func0f03e5b0(struct chrdata *chr, f32 arg1);
void chrResetAimEndProperties(struct chrdata *chr);
void chrSetFiring(struct chrdata *chr, s32 hand, s32 firing);
void chrStopFiring(struct chrdata *chr);
void func0f03f988(struct chrdata *chr, s32 hand, s32 arg2);
void func0f0429d8(struct chrdata *chr, f32 arg1, f32 arg2);
void propUnsetDangerous(struct prop *prop);
void propSetDangerous(struct prop *prop);
s32 chrDetectDangerousObject(struct chrdata *chr, u8 flags);
void chrTickBondDie(struct chrdata *chr);
s32 chrIsUsingLift(struct chrdata *chr);
s32 chrTrySkJump(struct chrdata *chr, u8 arg1, u8 arg2, s32 arg3, u8 arg4);
s32 chrSawTargetRecently(struct chrdata *chr);
s32 chrHeardTargetRecently(struct chrdata *chr);
f32 chrGetAngleToTarget(struct chrdata *chr);
f32 chrGetDistanceToTarget(struct chrdata *chr);
f32 chrGetDistanceToTarget2(struct chrdata *chr);
f32 chrGetDistanceToCurrentPlayer(struct chrdata *chr);
f32 propGetDistanceToProp(struct prop *a, struct prop *b);
f32 propGetLateralDistanceToProp(struct prop *a, struct prop *b);
f32 chrGetDistanceToPad(struct chrdata *chr, s32 pad_id);
f32 chrGetDistanceToCoord(struct chrdata *chr, struct coord *pos);
f32 chrGetLateralDistanceToCoord(struct chrdata *chr, struct coord *pos);
f32 chrGetLateralDistanceToPad(struct chrdata *chr, s32 pad_id);
f32 chrGetSquaredDistanceToCoord(struct chrdata *chr, struct coord *pos);
f32 coordGetSquaredDistanceToCoord(struct coord *a, struct coord *b);
s32 chrGetPadRoom(struct chrdata *chr, s32 pad_id);
s32 chrResolvePadId(struct chrdata *chr, s32 pad_id);
struct chrdata *chrFindById(struct chrdata *data, s32 chrnum);
s32 propGetIndexByChrId(struct chrdata *chr, s32 chrnum);
f32 chrGetDistanceToChr(struct chrdata *chr1, s32 chr2num);
f32 chrGetDistanceFromTargetToPad(struct chrdata *chr, s32 pad_id);
void chrSetFlags(struct chrdata *chr, u32 flags, u8 bank);
void chrUnsetFlags(struct chrdata *chr, u32 flags, u8 bank);
s32 chrHasFlag(struct chrdata *chr, u32 flag, u8 bank);
void chrSetFlagsById(struct chrdata *ref, u32 chrnum, u32 flags, u32 bank);
void chrUnsetFlagsById(struct chrdata *ref, u32 chrnum, u32 flags, u32 bank);
s32 chrHasFlagById(struct chrdata *ref, u32 chrnum, u32 flag, u32 bank);
void chrSetStageFlag(struct chrdata *chr, u32 flag);
void chrUnsetStageFlag(struct chrdata *chr, u32 flag);
s32 chrHasStageFlag(struct chrdata *chr, u32 flag);
s32 chrIsHearingTarget(struct chrdata *chr);
void chrRestartTimer(struct chrdata *chr);
s32 chrResetNearMiss(struct chrdata *chr);
s32 chrGetNumArghs(struct chrdata *chr);
s32 chrGetNumCloseArghs(struct chrdata *chr);
void decrementByte(u8 *dst, u8 amount);
void incrementByte(u8 *dst, u8 amount);
s32 chrCanHearAlarm(struct chrdata *chr);
s32 chrSetChrPresetToAnyChrNearSelf(struct chrdata *chr, f32 distance);
s32 chrSetChrPresetToChrNearSelf(u8 checktype, struct chrdata *chr, f32 distance);
s32 chrCompareTeams(struct chrdata *chr1, struct chrdata *chr2, u8 checktype);
void chrSetChrPreset(struct chrdata *chr, s32 chrpreset);
void chrSetChrPresetByChrnum(struct chrdata *chr, s32 chrnum, s32 chrpreset);
void chrSetPadPreset(struct chrdata *chr, s32 pad_id);
void chrSetPadPresetByChrnum(struct chrdata *chr, s32 chrnum, s32 pad_id);
struct prop *chrSpawnAtPad(struct chrdata *chr, s32 body, s32 head, s32 pad, u8 *ailist, u32 spawnflags);
struct prop *chrSpawnAtChr(struct chrdata *basechr, s32 body, s32 head, u32 chrnum, u8 *ailist, u32 spawnflags);
s16 chrGoToCover(struct chrdata *chr, u8 speed);
void chrAddTargetToBdlist(struct chrdata *chr);
s32 chrGetDistanceLostToTargetInLastSecond(struct chrdata *chr);
s32 chrIsTargetNearlyInSight(struct chrdata *chr, u32 distance);
s32 chrIsNearlyInTargetsSight(struct chrdata *chr, u32 distance);
s16 *teamGetChrIds(s32 team_id);
s16 *squadronGetChrIds(s32 squadron_id);
void audioMarkAsRecentlyPlayed(s16 audioid);
s32 audioWasNotPlayedRecently(s16 audioid);
void chrToggleModelPart(struct chrdata *chr, s32 partnum);
s32 chrIsAvoiding(struct chrdata *chr);
void chrDrCarollEmitSparks(struct chrdata *chr);
s32 chraiGetListIdByList(u8 *ailist, s32 *is_global);
u32 chraiGoToLabel(u8 *ailist, u32 aioffset, u8 label);
void chraiExecute(void *entity, s32 proptype);
u32 chraiGetCommandLength(u8 *ailist, u32 aioffset);
           s32 aiGoToNext(void);
           s32 aiGoToFirst(void);
           s32 aiLabel(void);
           s32 aiYield(void);
           s32 aiEndList(void);
           s32 aiSetList(void);
           s32 aiSetReturnList(void);
           s32 aiSetShotList(void);
           s32 aiReturn(void);
           s32 aiStop(void);
           s32 aiKneel(void);
           s32 aiChrDoAnimation(void);
           s32 aiIfIdle(void);
           s32 aiBeSurprisedOneHand(void);
           s32 aiBeSurprisedLookAround(void);
           s32 aiTrySidestep(void);
           s32 aiTryJumpOut(void);
           s32 aiTryRunSideways(void);
           s32 aiTryAttackWalk(void);
           s32 aiTryAttackRun(void);
           s32 aiTryAttackRoll(void);
           s32 aiTryAttackStand(void);
           s32 aiTryAttackKneel(void);
           s32 aiTryModifyAttack(void);
           s32 aiFaceEntity(void);
           s32 ai0019(void);
           s32 aiChrDamageChr(void);
           s32 aiConsiderGrenadeThrow(void);
           s32 aiDropItem(void);
           s32 aiJogToPad(void);
           s32 aiGoToPadPreset(void);
           s32 aiWalkToPad(void);
           s32 aiRunToPad(void);
           s32 aiSetPath(void);
           s32 aiStartPatrol(void);
           s32 aiIfPatrolling(void);
           s32 aiSurrender(void);
           s32 aiFadeOut(void);
           s32 aiRemoveChr(void);
           s32 aiTryStartAlarm(void);
           s32 aiActivateAlarm(void);
           s32 aiDeactivateAlarm(void);
           s32 aiTryRunFromTarget(void);
           s32 aiTryJogToTargetProp(void);
           s32 aiTryWalkToTargetProp(void);
           s32 aiTryRunToTargetProp(void);
           s32 aiTryGoToCoverProp(void);
           s32 aiTryJogToChr(void);
           s32 aiTryWalkToChr(void);
           s32 aiTryRunToChr(void);
           s32 aiIfStopped(void);
           s32 aiIfChrDead(void);
           s32 aiIfChrDeathAnimationFinished(void);
           s32 aiIfTargetInSight(void);
           s32 aiRandom(void);
           s32 aiIfRandomLessThan(void);
           s32 aiIfRandomGreaterThan(void);
           s32 aiIfCanHearAlarm(void);
           s32 aiIfAlarmActive(void);
           s32 aiIfGasActive(void);
           s32 aiIfHearsTarget(void);
           s32 aiIfSawInjury(void);
           s32 aiIfSawDeath(void);
           s32 aiIfCanSeeTarget(void);
           s32 aiIfTargetNearlyInSight(void);
           s32 aiIfNearlyInTargetsSight(void);
           s32 aiSetPadPresetToPadOnRouteToTarget(void);
           s32 aiIfSawTargetRecently(void);
           s32 aiIfHeardTargetRecently(void);
           s32 ai0045(void);
           s32 aiIfNeverBeenOnScreen(void);
           s32 ai0047(void);
           s32 aiIfChrInActiveRoom(void);
           s32 aiIfRoomActive(void);
           s32 ai004a(void);
           s32 aiIfNearMiss(void);
           s32 aiIfSeesSuspiciousItem(void);
           s32 aiIfTargetInFovLeft(void);
           s32 aiIfCheckFovWithTarget(void);
           s32 aiIfTargetOutOfFovLeft(void);
           s32 aiIfTargetInFov(void);
           s32 aiIfTargetOutOfFov(void);
           s32 aiIfDistanceToTargetLessThan(void);
           s32 aiIfDistanceToTargetGreaterThan(void);
           s32 aiIfChrDistanceToPadLessThan(void);
           s32 aiIfChrDistanceToPadGreaterThan(void);
           s32 aiIfDistanceToChrLessThan(void);
           s32 aiIfDistanceToChrGreaterThan(void);
           s32 ai0058(void);
           s32 aiIfDistanceFromTargetToPadLessThan(void);
           s32 aiIfDistanceFromTargetToPadGreaterThan(void);
           s32 aiIfChrInRoom(void);
           s32 aiIfTargetInRoom(void);
           s32 aiIfChrHasObject(void);
           s32 aiIfWeaponThrown(void);
           s32 aiIfWeaponThrownOnObject(void);
           s32 aiIfChrHasWeaponEquipped(void);
           s32 aiIfGunUnclaimed(void);
           s32 aiIfObjectHealthy(void);
           s32 aiIfChrActivatedObject(void);
           s32 aiObjInteract(void);
           s32 aiDestroyObject(void);
           s32 ai0067(void);
           s32 aiChrDropItems(void);
           s32 aiChrDropWeapon(void);
           s32 aiGiveObjectToChr(void);
           s32 aiObjectMoveToPad(void);
           s32 aiOpenDoor(void);
           s32 aiCloseDoor(void);
           s32 aiIfDoorState(void);
           s32 aiIfObjectIsDoor(void);
           s32 aiLockDoor(void);
           s32 aiUnlockDoor(void);
           s32 aiIfDoorLocked(void);
           s32 aiIfObjectiveComplete(void);
           s32 aiIfObjectiveFailed(void);
           s32 ai0075(void);
           s32 aiSetPadPresetToTargetQuadrant(void);
           s32 aiIfDifficultyLessThan(void);
           s32 aiIfDifficultyGreaterThan(void);
           s32 aiIfStageTimerLessThan(void);
           s32 aiIfStageTimerGreaterThan(void);
           s32 aiIfStageIdLessThan(void);
           s32 aiIfStageIdGreaterThan(void);
           s32 aiIfNumArghsLessThan(void);
           s32 aiIfNumArghsGreaterThan(void);
           s32 aiIfNumCloseArghsLessThan(void);
           s32 aiIfNumCloseArghsGreaterThan(void);
           s32 aiIfChrHealthGreaterThan(void);
           s32 aiIfChrHealthLessThan(void);
           s32 aiIfInjured(void);
           s32 aiSetMorale(void);
           s32 aiAddMorale(void);
           s32 aiChrAddMorale(void);
           s32 aiSubtractMorale(void);
           s32 aiIfMoraleLessThan(void);
           s32 aiIfMoraleLessThanRandom(void);
           s32 aiSetAlertness(void);
           s32 aiAddAlertness(void);
           s32 aiChrAddAlertness(void);
           s32 aiSubtractAlertness(void);
           s32 aiIfAlertness(void);
           s32 aiIfChrAlertnessLessThan(void);
           s32 aiIfAlertnessLessThanRandom(void);
           s32 aiNoOp0091(void);
           s32 aiSetHearDistance(void);
           s32 aiSetViewDistance(void);
           s32 aiSetGrenadeProbability(void);
           s32 aiSetChrNum(void);
           s32 aiSetMaxDamage(void);
           s32 aiAddHealth(void);
           s32 aiSetReactionSpeed(void);
           s32 aiSetRecoverySpeed(void);
           s32 aiSetAccuracy(void);
           s32 aiSetFlag(void);
           s32 aiUnsetFlag(void);
           s32 aiIfHasFlag(void);
           s32 aiChrSetFlag(void);
           s32 aiChrUnsetFlag(void);
           s32 aiIfChrHasFlag(void);
           s32 aiSetStageFlag(void);
           s32 aiUnsetStageFlag(void);
           s32 aiIfStageFlagEq(void);
           s32 aiSetChrflag(void);
           s32 aiUnsetChrflag(void);
           s32 aiIfHasChrflag(void);
           s32 aiChrSetChrflag(void);
           s32 aiChrUnsetChrflag(void);
           s32 aiIfChrHasChrflag(void);
           s32 aiSetObjFlag(void);
           s32 aiUnsetObjFlag(void);
           s32 aiIfObjHasFlag(void);
           s32 aiSetObjFlag2(void);
           s32 aiUnsetObjFlag2(void);
           s32 aiIfObjHasFlag2(void);
           s32 aiSetChrPreset(void);
           s32 aiSetChrTarget(void);
           s32 aiSetPadPreset(void);
           s32 aiChrSetPadPreset(void);
           s32 aiChrCopyPadPreset(void);
           s32 aiPrint(void);
           s32 aiRestartTimer(void);
           s32 aiResetTimer(void);
           s32 aiPauseTimer(void);
           s32 aiResumeTimer(void);
           s32 aiIfTimerStopped(void);
           s32 aiIfTimerGreaterThanRandom(void);
           s32 aiIfTimerLessThan(void);
           s32 aiIfTimerGreaterThan(void);
           s32 aiShowCountdownTimer(void);
           s32 aiHideCountdownTimer(void);
           s32 aiSetCountdownTimerValue(void);
           s32 aiStopCountdownTimer(void);
           s32 aiStartCountdownTimer(void);
           s32 aiIfCountdownTimerStopped(void);
           s32 aiIfCountdownTimerLessThan(void);
           s32 aiIfCountdownTimerGreaterThan(void);
           s32 aiSpawnChrAtPad(void);
           s32 aiSpawnChrAtChr(void);
           s32 aiTryEquipWeapon(void);
           s32 aiTryEquipHat(void);
           s32 aiDuplicateChr(void);
           s32 aiShowHudmsg(void);
           s32 aiShowHudmsgTopMiddle(void);
           s32 aiSpeak(void);
           s32 aiPlaySound(void);
           s32 ai00cf(void);
           s32 ai00d0(void);
           s32 ai00d1(void);
           s32 ai00d2(void);
           s32 aiAudioMuteChannel(void);
           s32 ai00d4(void);
           s32 aiHovercarBeginPath(void);
           s32 aiSetVehicleSpeed(void);
           s32 aiSetRotorSpeed(void);
           s32 aiNoOp00d8(void);
           s32 aiNoOp00d9(void);
           s32 aiSetObjImage(void);
           s32 aiNoOp00db(void);
           s32 aiEndLevel(void);
           s32 ai00dd(void);
           s32 aiWarpJoToPad(void);
           s32 ai00df(void);
           s32 aiRevokeControl(void);
           s32 aiGrantControl(void);
           s32 aiChrMoveToPad(void);
           s32 ai00e3(void);
           s32 ai00e4(void);
           s32 aiIfColourFadeComplete(void);
           s32 aiSetDoorOpen(void);
           s32 ai00e9(void);
           s32 aiIfNumPlayersLessThan(void);
           s32 aiIfChrAmmoQuantityLessThan(void);
           s32 aiChrDrawWeapon(void);
           s32 aiChrDrawWeaponInCutscene(void);
           s32 ai00ee(void);
           s32 aiIfObjInRoom(void);
           s32 ai00f0(void);
           s32 aiIfAttacking(void);
           s32 aiSwitchToAltSky(void);
           s32 aiChrSetInvincible(void);
           s32 ai00f4(void);
           s32 ai00f5(void);
           s32 ai00f6(void);
           s32 aiIfAllObjectivesComplete(void);
           s32 aiIfPlayerIsInvincible(void);
           s32 aiPlayXTrack(void);
           s32 aiStopXTrack(void);
           s32 aiChrExplosions(void);
           s32 aiIfKillCountGreaterThan(void);
           s32 ai00fd(void);
           s32 aiKillBond(void);
           s32 aiBeSurprisedSurrender(void);
           s32 aiNoOp0100(void);
           s32 aiNoOp0101(void);
           s32 aiSetLights(void);
           s32 ai0103(void);
           s32 aiRemoveObjectAtPropPreset(void);
           s32 aiIfPropPresetHeightLessThan(void);
           s32 aiSetTarget(void);
           s32 aiIfPresetsTargetIsNotMyTarget(void);
           s32 aiIfChrTarget(void);
           s32 aiSetChrPresetToChrNearSelf(void);
           s32 aiSetChrPresetToChrNearPad(void);
           s32 aiChrSetTeam(void);
           s32 aiIfCompareChrPresetsTeam(void);
           s32 aiNoOp010d(void);
           s32 aiSetShield(void);
           s32 aiIfChrShieldLessThan(void);
           s32 aiIfChrShieldGreaterThan(void);
           s32 aiSetCameraAnimation(void);
           s32 aiObjectDoAnimation(void);
           s32 aiIfInCutscene(void);
           s32 aiEnableChr(void);
           s32 aiDisableChr(void);
           s32 aiEnableObj(void);
           s32 aiDisableObj(void);
           s32 aiSetObjFlag3(void);
           s32 aiUnsetObjFlag3(void);
           s32 aiIfObjHasFlag3(void);
           s32 aiChrSetHiddenFlag(void);
           s32 aiChrUnsetHiddenFlag(void);
           s32 aiIfChrHasHiddenFlag(void);
           s32 aiIfHuman(void);
           s32 aiIfSkedar(void);
           s32 ai0120(void);
           s32 aiFindCover(void);
           s32 aiFindCoverWithinDist(void);
           s32 aiFindCoverOutsideDist(void);
           s32 aiGoToCover(void);
           s32 aiCheckCoverOutOfSight(void);
           s32 aiIfPlayerUsingCmpOrAr34(void);
           s32 aiDetectEnemyOnSameFloor(void);
           s32 aiDetectEnemy(void);
           s32 aiIfSafetyLessThan(void);
           s32 aiIfTargetMovingSlowly(void);
           s32 aiIfTargetMovingCloser(void);
           s32 aiIfTargetMovingAway(void);
           s32 ai012f(void);
           s32 aiSayQuip(void);
           s32 aiIncreaseSquadronAlertness(void);
           s32 aiSetAction(void);
           s32 aiSetTeamOrders(void);
           s32 aiIfOrders(void);
           s32 aiIfHasOrders(void);
           s32 aiRetreat(void);
           s32 aiIfChrInSquadronDoingAction(void);
           s32 aiIfChannelIdle(void);
           s32 ai0139(void);
           s32 aiSetChrPresetToUnalertedTeammate(void);
           s32 aiSetSquadron(void);
           s32 aiFaceCover(void);
           s32 aiIfDangerousObjectNearby(void);
           s32 ai013e(void);
           s32 aiIfHeliWeaponsArmed(void);
           s32 aiIfHoverbotNextStep(void);
           s32 aiShuffleInvestigationTerminals(void);
           s32 aiSetPadPresetToInvestigationTerminal(void);
           s32 aiHeliArmWeapons(void);
           s32 aiHeliUnarmWeapons(void);
           s32 aiRebuildTeams(void);
           s32 aiRebuildSquadrons(void);
           s32 aiIfSquadronIsDead(void);
           s32 aiChrSetListening(void);
           s32 aiIfChrListening(void);
           s32 aiIfTrue(void);
           s32 aiIfNotListening(void);
           s32 aiIfNumChrsInSquadronGreaterThan(void);
           s32 aiSetTintedGlassEnabled(void);
           s32 aiPlayTrackIsolated(void);
           s32 aiPlayDefaultTracks(void);
           s32 aiIfChrInjured(void);
           s32 aiIfAction(void);
           s32 aiHovercopterFireRocket(void);
           s32 ai0168(void);
           s32 aiIfNaturalAnim(void);
           s32 aiIfY(void);
           s32 ai016b(void);
           s32 aiNoOp016c(void);
           s32 aiChrAdjustMotionBlur(void);
           s32 aiDamageChrByAmount(void);
           s32 aiIfChrHasGun(void);
           s32 aiDoGunCommand(void);
           s32 aiIfDistanceToGunLessThan(void);
           s32 ai0172(void);
           s32 aiChrCopyProperties(void);
           s32 aiIfCutsceneButtonPressed(void);
           s32 ai0175(void);
           s32 ai0176(void);
           s32 aiPlayerAutoWalk(void);
           s32 aiIfPlayerAutoWalkFinished(void);
           s32 ai0179(void);
           s32 aiIfCanSeeAttackTarget(void);
           s32 aiIfChrKnockedOut(void);
           s32 aiAssignSound(void);
           s32 aiPlayCutsceneTrack(void);
           s32 aiStopCutsceneTrack(void);
           s32 aiPlayTemporaryTrack(void);
           s32 aiStopAmbientTrack(void);
           s32 aiIfPlayerLookingAtObject(void);
           s32 aiPunchOrKick(void);
           s32 aiIfTargetIsPlayer(void);
           s32 ai0184(void);
           s32 aiMpInitSimulants(void);
           s32 aiIfSoundTimer(void);
           s32 aiSetTargetToEyespyIfInSight(void);
           s32 aiIfLiftStationary(void);
           s32 aiLiftGoToStop(void);
           s32 aiIfLiftAtStop(void);
           s32 aiConfigureRain(void);
           s32 aiChrToggleModelPart(void);
           s32 aiActivateLift(void);
           s32 aiMiniSkedarTryPounce(void);
           s32 aiIfObjectDistanceToPadLessThan(void);
           s32 aiSetSavefileFlag(void);
           s32 aiUnsetSavefileFlag(void);
           s32 aiIfSavefileFlagIsSet(void);
           s32 aiIfSavefileFlagIsUnset(void);
           s32 aiIfObjHealthLessThan(void);
           s32 aiSetObjHealth(void);
           s32 aiSetChrSpecialDeathAnimation(void);
           s32 aiSetRoomToSearch(void);
           s32 aiSayCiStaffQuip(void);
           s32 aiDoPresetAnimation(void);
           s32 aiShowHudmsgMiddle(void);
           s32 aiIfUsingLift(void);
           s32 aiIfTargetYDifferenceLessThan(void);
           s32 aiIfChrPropsoundcountZero(void);
           s32 ai01aa(void);
           s32 aiIfNumKnockedOutChrs(void);
           s32 aiReleaseObject(void);
           s32 aiClearInventory(void);
           s32 aiChrGrabObject(void);
           s32 aiShuffleRuinsPillars(void);
           s32 ai01b2(void);
           s32 aiToggleP1P2(void);
           s32 ai01b4(void);
           s32 aiChrSetP1P2(void);
           s32 aiConfigureSnow(void);
           s32 aiChrSetCloaked(void);
           s32 aiSetAutogunType(void);
           s32 aiShufflePelagicSwitches(void);
           s32 aiTryAttackLie(void);
           s32 aiNoOp01bb(void);
           s32 ai01bc(void);
           s32 aiIfTrainingPcHolographed(void);
           s32 aiIfPlayerUsingDevice(void);
           s32 aiChrBeginOrEndTeleport(void);
           s32 aiIfChrTeleportFullWhite(void);
           s32 aiSetPunchDodgeList(void);
           s32 aiSetShootingAtMeList(void);
           s32 aiSetDarkRoomList(void);
           s32 aiSetPlayerDeadList(void);
           s32 aiAvoid(void);
           s32 aiSetDodgeRating(void);
           s32 aiSetUnarmedDodgeRating(void);
           s32 aiTitleInitMode(void);
           s32 aiTryExitTitle(void);
           s32 aiChrSetCutsceneWeapon(void);
           s32 aiFadeScreen(void);
           s32 aiIfFadeComplete(void);
           s32 aiSetChrHudpieceVisible(void);
           s32 aiSetPassiveMode(void);
           s32 aiChrSetFiringInCutscene(void);
           s32 aiSetPortalFlag(void);
           s32 aiObjSetModelPartVisible(void);
           s32 aiChrEmitSparks(void);
           s32 aiSetDrCarollImages(void);
           s32 ai01d4(void);
           s32 aiShowCutsceneChrs(void);
           s32 aiConfigureEnvironment(void);
           s32 aiIfDistanceToTarget2LessThan(void);
           s32 aiIfDistanceToTarget2GreaterThan(void);
           s32 aiPlaySoundFromProp(void);
           s32 aiPlayTemporaryPrimaryTrack(void);
           s32 aiChrKill(void);
           s32 aiRemoveWeaponFromInventory(void);
           s32 ai01dd(void);
           s32 aiIfCoopMode(void);
           s32 aiIfChrSameFloorDistanceToPadLessThan(void);
           s32 aiRemoveReferencesToChr(void);
void func0f05abdc(struct prop *prop);
void func0f137430(struct gfxvtx *vertices, f32 arg1, f32 arg2, f32 arg3, f32 arg4, f32 arg5, f32 arg6);
void creditsInitVertices(struct gfxvtx *vertices, s32 z);
void func0f137874(struct gfxvtx *vertices, u32 *colours, s32 arg2, s32 alpha, s32 arg4);
Gfx *creditsRenderBackgroundLayer(Gfx *gdl, u8 type, u8 layernum, f32 arg3, u32 alpha, s32 arg5);
void creditsCopyBackgroundLayer(s32 srcindex, s32 dstindex, s32 move);
Gfx *creditsRenderBackground(Gfx *gdl);
f32 func0f1382e0(f32 range);
void creditsCreatePendingBgLayers(u32 arg0);
Gfx *creditsClearFramebuffer(Gfx *gdl, u32 colour);
void func0f13870c(void);
void func0f13899c(void);
u32 func0f138e6c(void);
Gfx *creditsRenderSprites(Gfx *gdl);
u32 func0f1399d0(void);
u32 func0f139d9c(void);
void creditsInitSlides(void);
struct credit *creditGetByRow(s32 row);
void creditsTickSlide(void);
Gfx *func0f13a3ec(Gfx *gdl);
u32 func0f13ae04(void);
void creditsTick(void);
Gfx *creditsRender(Gfx *gdl);
void creditsInit(void);
void creditsRequestAltTitle(void);
struct credits000c {
 f32 unk00;
 f32 unk04;
 f32 unk08;
 f32 unk0c;
 u8 unk10;
 u8 unk11;
 u8 unk12;
 u8 unk13;
 u8 unk14;
};
struct creditsbgtype {
 s16 unk00;
 s16 unk02;
 s16 unk04;
 s32 texturenum;
 f32 unk0c;
 f32 unk10;
};
struct creditsbglayer {
 s32 type;
 f32 rotatespeed;
 f32 unk08;
 u32 unk0c;
};
struct creditsdata {
            u32 unk0000;
            u32 unk0004;
            u32 unk0008;
            struct credits000c unk000c[500];
            u8 unk2eec;
            u8 unk2eed;
            u8 unk2eee;
            u8 unk2eef;
            u8 unk2ef0;
            u32 unk2ef4;
            u32 unk2ef8;
            u32 unk2efc;
            u32 unk2f00;
            u32 unk2f04;
            u32 unk2f08;
            u32 unk2f0c;
            u32 unk2f10;
            u32 unk2f14;
            u32 unk2f18;
            u32 unk2f1c;
            u32 unk2f20;
            u32 unk2f24;
            u32 unk2f28;
            u32 unk2f2c;
            u32 unk2f30;
            u32 unk2f34;
            u32 unk2f38;
            u32 unk2f3c;
            u32 unk2f40;
            u32 unk2f44;
            u32 unk2f48;
            u32 unk2f4c;
            u32 unk2f50;
            u32 unk2f54;
            u32 unk2f58;
            u32 unk2f5c;
            u32 unk2f60;
            u32 unk2f64;
            u32 unk2f68;
            u32 unk2f6c;
            u32 unk2f70;
            u32 unk2f74;
            u32 unk2f78;
            u32 unk2f7c;
            u32 unk2f80;
            u32 unk2f84;
            u32 unk2f88;
            u32 unk2f8c;
            u32 unk2f90;
            u32 unk2f94;
            u32 unk2f98;
            u32 unk2f9c;
            u32 unk2fa0;
            u32 unk2fa4;
            u32 unk2fa8;
            u32 unk2fac;
            u32 unk2fb0;
            u32 unk2fb4;
            u32 unk2fb8;
            u32 unk2fbc;
            u32 unk2fc0;
            u32 unk2fc4;
            u32 unk2fc8;
            u32 unk2fcc;
            u32 unk2fd0;
            u32 unk2fd4;
            u32 unk2fd8;
            u32 unk2fdc;
            u32 unk2fe0;
            u32 unk2fe4;
            u32 unk2fe8;
            u32 unk2fec;
            u32 unk2ff0;
            u32 unk2ff4;
            u32 unk2ff8;
            u32 unk2ffc;
            u32 unk3000;
            u32 unk3004;
            u32 unk3008;
            u32 unk300c;
            u32 unk3010;
            u32 unk3014;
            u32 unk3018;
            u32 unk301c;
            u32 unk3020;
            u32 unk3024;
            u32 unk3028;
            u32 unk302c;
            u32 unk3030;
            u32 unk3034;
            u32 unk3038;
            u32 unk303c;
            u32 unk3040;
            u32 unk3044;
            u32 unk3048;
            u32 unk304c;
            u32 unk3050;
            u32 unk3054;
            u32 unk3058;
            u32 unk305c;
            u32 unk3060;
            u32 unk3064;
            u32 unk3068;
            u32 unk306c;
            u32 unk3070;
            u32 unk3074;
            u32 unk3078;
            u32 unk307c;
            u32 unk3080;
            u32 unk3084;
            u32 unk3088;
            u32 unk308c;
            u32 unk3090;
            u32 unk3094;
            u32 unk3098;
            u32 unk309c;
            u32 unk30a0;
            u32 unk30a4;
            u32 unk30a8;
            u32 unk30ac;
            u32 unk30b0;
            u32 unk30b4;
            u32 unk30b8;
            u32 unk30bc;
            u32 unk30c0;
            u32 unk30c4;
            u32 unk30c8;
            u32 unk30cc;
            u32 unk30d0;
            u32 unk30d4;
            u32 unk30d8;
            u32 unk30dc;
            u32 unk30e0;
            u32 unk30e4;
            u32 unk30e8;
            u32 unk30ec;
            u32 unk30f0;
            u32 unk30f4;
            u32 unk30f8;
            u32 unk30fc;
            u32 unk3100;
            u32 unk3104;
            u32 unk3108;
            u32 unk310c;
            u32 unk3110;
            u32 unk3114;
            u32 unk3118;
            u32 unk311c;
            u32 unk3120;
            u32 unk3124;
            u32 unk3128;
            u32 unk312c;
            u32 unk3130;
            u32 unk3134;
            u32 unk3138;
            u32 unk313c;
            u32 unk3140;
            u32 unk3144;
            u32 unk3148;
            u32 unk314c;
            u32 unk3150;
            u32 unk3154;
            u32 unk3158;
            u32 unk315c;
            u32 unk3160;
            u32 unk3164;
            u32 unk3168;
            u32 unk316c;
            u32 unk3170;
            u32 unk3174;
            u32 unk3178;
            u32 unk317c;
            u32 unk3180;
            u32 unk3184;
            u32 unk3188;
            u32 unk318c;
            u32 unk3190;
            u32 unk3194;
            u32 unk3198;
            u32 unk319c;
            u32 unk31a0;
            u32 unk31a4;
            u32 unk31a8;
            u32 unk31ac;
            u32 unk31b0;
            u32 unk31b4;
            u32 unk31b8;
            u32 unk31bc;
            u32 unk31c0;
            u32 unk31c4;
            u32 unk31c8;
            u32 unk31cc;
            u32 unk31d0;
            u32 unk31d4;
            u32 unk31d8;
            u32 unk31dc;
            u32 unk31e0;
            u32 unk31e4;
            u32 unk31e8;
            u32 unk31ec;
            u32 unk31f0;
            u32 unk31f4;
            u32 unk31f8;
            u32 unk31fc;
            u32 unk3200;
            u32 unk3204;
            u32 unk3208;
            u32 unk320c;
            u32 unk3210;
            u32 unk3214;
            u32 unk3218;
            u32 unk321c;
            u32 unk3220;
            u32 unk3224;
            u32 unk3228;
            u32 unk322c;
            u32 unk3230;
            u32 unk3234;
            u32 unk3238;
            u32 unk323c;
            u32 unk3240;
            u32 unk3244;
            u32 unk3248;
            u32 unk324c;
            u32 unk3250;
            u32 unk3254;
            u32 unk3258;
            u32 unk325c;
            u32 unk3260;
            u32 unk3264;
            u32 unk3268;
            u32 unk326c;
            u32 unk3270;
            u32 unk3274;
            u32 unk3278;
            u32 unk327c;
            u32 unk3280;
            u32 unk3284;
            u32 unk3288;
            u32 unk328c;
            u32 unk3290;
            u32 unk3294;
            u32 unk3298;
            u32 unk329c;
            u32 unk32a0;
            u32 unk32a4;
            u32 unk32a8;
            u32 unk32ac;
            u32 unk32b0;
            u32 unk32b4;
            u32 unk32b8;
            u32 unk32bc;
            u32 unk32c0;
            u32 unk32c4;
            u32 unk32c8;
            u32 unk32cc;
            u32 unk32d0;
            u32 unk32d4;
            u32 unk32d8;
            u32 unk32dc;
            u32 unk32e0;
            u32 unk32e4;
            u32 unk32e8;
            u32 unk32ec;
            u32 unk32f0;
            u32 unk32f4;
            u32 unk32f8;
            u32 unk32fc;
            u32 unk3300;
            u32 unk3304;
            u32 unk3308;
            u32 unk330c;
            u32 unk3310;
            u32 unk3314;
            u32 unk3318;
            u32 unk331c;
            u32 unk3320;
            u32 unk3324;
            u32 unk3328;
            u32 unk332c;
            u32 unk3330;
            u32 unk3334;
            u32 unk3338;
            u32 unk333c;
            u32 unk3340;
            u32 unk3344;
            u32 unk3348;
            u32 unk334c;
            u32 unk3350;
            u32 unk3354;
            u32 unk3358;
            u32 unk335c;
            u32 unk3360;
            u32 unk3364;
            u32 unk3368;
            u32 unk336c;
            u32 unk3370;
            u32 unk3374;
            u32 unk3378;
            u32 unk337c;
            u32 unk3380;
            u32 unk3384;
            u32 unk3388;
            u32 unk338c;
            u32 unk3390;
            u32 unk3394;
            u32 unk3398;
            u32 unk339c;
            u32 unk33a0;
            u32 unk33a4;
            u32 unk33a8;
            u32 unk33ac;
            u32 unk33b0;
            u32 unk33b4;
            u32 unk33b8;
            u32 unk33bc;
            u32 unk33c0;
            u32 unk33c4;
            u32 unk33c8;
            u32 unk33cc;
            u32 unk33d0;
            u32 unk33d4;
            u32 unk33d8;
            u32 unk33dc;
            u32 unk33e0;
            u32 unk33e4;
            u32 unk33e8;
            u32 unk33ec;
            u32 unk33f0;
            u32 unk33f4;
            u32 unk33f8;
            u32 unk33fc;
            f32 unk3400;
            f32 unk3404;
            u32 unk3408;
            f32 unk340c;
            f32 unk3410;
            f32 unk3414;
            f32 unk3418;
            u32 unk341c;
            u32 unk3420;
            u32 unk3424;
            f32 unk3428;
            f32 unk342c;
            f32 unk3430;
            f32 unk3434;
            f32 unk3438;
            f32 unk343c;
            f32 unk3440;
            u32 unk3444;
            u32 unk3448;
            u32 unk344c;
            u32 unk3450;
            u32 unk3454;
            u32 unk3458;
            u32 unk345c;
            u32 unk3460;
            u32 unk3464;
            u32 unk3468;
            u32 unk346c;
            u32 unk3470;
            u32 unk3474;
            u32 unk3478;
            u32 unk347c;
            u32 unk3480;
            u32 unk3484;
            u32 unk3488;
            u32 unk348c;
            u32 unk3490;
            u32 unk3494;
            u32 unk3498;
            u32 unk349c;
            u8 unk34a0;
            u8 unk34a1;
            u32 unk34a4;
            u32 unk34a8;
            u32 unk34ac;
            u32 unk34b0;
            u32 unk34b4;
            u32 unk34b8;
            u32 unk34bc;
            u32 unk34c0;
            u32 unk34c4;
            u32 unk34c8;
            u32 unk34cc;
            u32 unk34d0;
            u32 unk34d4;
            u32 unk34d8;
            u32 unk34dc;
            u32 unk34e0;
            u32 unk34e4;
            u32 unk34e8;
            u32 unk34ec;
            u32 unk34f0;
            u32 unk34f4;
            u32 unk34f8;
            u32 unk34fc;
            u32 unk3500;
            u32 unk3504;
            u32 unk3508;
            u32 unk350c;
            u32 unk3510;
            u32 unk3514;
            u32 unk3518;
            u32 unk351c;
            u32 unk3520;
            u32 unk3524;
            u32 unk3528;
            u32 unk352c;
            u32 unk3530;
            u32 unk3534;
            u32 unk3538;
            u32 unk353c;
            u32 unk3540;
            u32 unk3544;
            u32 unk3548;
            u32 unk354c;
            u32 unk3550;
            u32 unk3554;
            u32 unk3558;
            u32 unk355c;
            u32 unk3560;
            u32 unk3564;
            u32 unk3568;
            u32 unk356c;
            u32 unk3570;
            u32 unk3574;
            u32 unk3578;
            u32 unk357c;
            u32 unk3580;
            u32 unk3584;
            u32 unk3588;
            u32 unk358c;
            u32 unk3590;
            u32 unk3594;
            u32 unk3598;
            u32 unk359c;
            u32 unk35a0;
            u32 unk35a4;
            u32 unk35a8;
            u32 unk35ac;
            u32 unk35b0;
            u32 unk35b4;
            u32 unk35b8;
            u32 unk35bc;
            u32 unk35c0;
            u32 unk35c4;
            u32 unk35c8;
            u32 unk35cc;
            u32 unk35d0;
            u32 unk35d4;
            u32 unk35d8;
            u32 unk35dc;
            u32 unk35e0;
            u32 unk35e4;
            u32 unk35e8;
            u32 unk35ec;
            u32 unk35f0;
            u32 unk35f4;
            u32 unk35f8;
            u32 unk35fc;
            u32 unk3600;
            u32 unk3604;
            u32 unk3608;
            u32 unk360c;
            u32 unk3610;
            u32 unk3614;
            u32 unk3618;
            u32 unk361c;
            u32 unk3620;
            u32 unk3624;
            u32 unk3628;
            u32 unk362c;
            u32 unk3630;
            u32 unk3634;
            u32 unk3638;
            u32 unk363c;
            u32 unk3640;
            u32 unk3644;
            u32 unk3648;
            u32 unk364c;
            u32 unk3650;
            u32 unk3654;
            u32 unk3658;
            u32 unk365c;
            u32 unk3660;
            u32 unk3664;
            u32 unk3668;
            u32 unk366c;
            u32 unk3670;
            u32 unk3674;
            u32 unk3678;
            u32 unk367c;
            u32 unk3680;
            u32 unk3684;
            u32 unk3688;
            u32 unk368c;
            u32 unk3690;
            u32 unk3694;
            u32 unk3698;
            u32 unk369c;
            u32 unk36a0;
            u32 unk36a4;
            u32 unk36a8;
            u32 unk36ac;
            u32 unk36b0;
            u32 unk36b4;
            u32 unk36b8;
            u32 unk36bc;
            u32 unk36c0;
            u32 unk36c4;
            u32 unk36c8;
            u32 unk36cc;
            u32 unk36d0;
            u32 unk36d4;
            u32 unk36d8;
            u32 unk36dc;
            u32 unk36e0;
            u32 unk36e4;
            u32 unk36e8;
            u32 unk36ec;
            u32 unk36f0;
            u32 unk36f4;
            u32 unk36f8;
            u32 unk36fc;
            u32 unk3700;
            u32 unk3704;
            u32 unk3708;
            u32 unk370c;
            u32 unk3710;
            u32 unk3714;
            u32 unk3718;
            u32 unk371c;
            u32 unk3720;
            u32 unk3724;
            u32 unk3728;
            u32 unk372c;
            u32 unk3730;
            u32 unk3734;
            u32 unk3738;
            u32 unk373c;
            u32 unk3740;
            u32 unk3744;
            u32 unk3748;
            u32 unk374c;
            u32 unk3750;
            u32 unk3754;
            u32 unk3758;
            u32 unk375c;
            u32 unk3760;
            u32 unk3764;
            u32 unk3768;
            u32 unk376c;
            u32 unk3770;
            u32 unk3774;
            u32 unk3778;
            u32 unk377c;
            u32 unk3780;
            u32 unk3784;
            u32 unk3788;
            u32 unk378c;
            u32 unk3790;
            u32 unk3794;
            u32 unk3798;
            u32 unk379c;
            u32 unk37a0;
            u32 unk37a4;
            u32 unk37a8;
            u32 unk37ac;
            u32 unk37b0;
            u32 unk37b4;
            u32 unk37b8;
            u32 unk37bc;
            u32 unk37c0;
            u32 unk37c4;
            u32 unk37c8;
            u32 unk37cc;
            u32 unk37d0;
            u32 unk37d4;
            u32 unk37d8;
            u32 unk37dc;
            u32 unk37e0;
            u32 unk37e4;
            u32 unk37e8;
            u32 unk37ec;
            u32 unk37f0;
            u32 unk37f4;
            u32 unk37f8;
            u32 unk37fc;
            u32 unk3800;
            u32 unk3804;
            u32 unk3808;
            u32 unk380c;
            u32 unk3810;
            u32 unk3814;
            u32 unk3818;
            u32 unk381c;
            u32 unk3820;
            u32 unk3824;
            u32 unk3828;
            u32 unk382c;
            u32 unk3830;
            u32 unk3834;
            u32 unk3838;
            u32 unk383c;
            u32 unk3840;
            u32 unk3844;
            u32 unk3848;
            u32 unk384c;
            u32 unk3850;
            u32 unk3854;
            u32 unk3858;
            u32 unk385c;
            u32 unk3860;
            u32 unk3864;
            u32 unk3868;
            u32 unk386c;
            u32 unk3870;
            u32 unk3874;
            u32 unk3878;
            u32 unk387c;
            u32 unk3880;
            u32 unk3884;
            u32 unk3888;
            u32 unk388c;
            u32 unk3890;
            u32 unk3894;
            u32 unk3898;
            u32 unk389c;
            u32 unk38a0;
            u32 unk38a4;
            u32 unk38a8;
            u32 unk38ac;
            u32 unk38b0;
            u32 unk38b4;
            u32 unk38b8;
            u32 unk38bc;
            u32 unk38c0;
            u32 unk38c4;
            u32 unk38c8;
            u32 unk38cc;
            u32 unk38d0;
            u32 unk38d4;
            u32 unk38d8;
            u32 unk38dc;
            u32 unk38e0;
            u32 unk38e4;
            u32 unk38e8;
            u32 unk38ec;
            u32 unk38f0;
            u32 unk38f4;
            u32 unk38f8;
            u32 unk38fc;
            u32 unk3900;
            u32 unk3904;
            u32 unk3908;
            u32 unk390c;
            u32 unk3910;
            u32 unk3914;
            u32 unk3918;
            u32 unk391c;
            u32 unk3920;
            u32 unk3924;
            u32 unk3928;
            u32 unk392c;
            u32 unk3930;
            u32 unk3934;
            u32 unk3938;
            u32 unk393c;
            u32 unk3940;
            u32 unk3944;
            u32 unk3948;
            u32 unk394c;
            u32 unk3950;
            u32 unk3954;
            u32 unk3958;
            u32 unk395c;
            u32 unk3960;
            u32 unk3964;
            u32 unk3968;
            u32 unk396c;
            u32 unk3970;
            u32 unk3974;
            u32 unk3978;
            u32 unk397c;
            u32 unk3980;
            u32 unk3984;
            u32 unk3988;
            u32 unk398c;
            u32 unk3990;
            u32 unk3994;
            u32 unk3998;
            u32 unk399c;
            u32 unk39a0;
            u32 unk39a4;
            u32 unk39a8;
            u32 unk39ac;
            u32 unk39b0;
            u32 unk39b4;
            u32 unk39b8;
            u32 unk39bc;
            u32 unk39c0;
            u32 unk39c4;
            u32 unk39c8;
            u32 unk39cc;
            u32 unk39d0;
            u32 unk39d4;
            u32 unk39d8;
            u32 unk39dc;
            u32 unk39e0;
            u32 unk39e4;
            u32 unk39e8;
            u32 unk39ec;
            u32 unk39f0;
            u32 unk39f4;
            u32 unk39f8;
            u32 unk39fc;
            u32 unk3a00;
            u32 unk3a04;
            u32 unk3a08;
            u32 unk3a0c;
            u32 unk3a10;
            u32 unk3a14;
            u32 unk3a18;
            u32 unk3a1c;
            u32 unk3a20;
            u32 unk3a24;
            u32 unk3a28;
            u32 unk3a2c;
            u32 unk3a30;
            u32 unk3a34;
            u32 unk3a38;
            u32 unk3a3c;
            u32 unk3a40;
            u32 unk3a44;
            u32 unk3a48;
            u32 unk3a4c;
            u32 unk3a50;
            u32 unk3a54;
            u32 unk3a58;
            u32 unk3a5c;
            u32 unk3a60;
            u32 unk3a64;
            u32 unk3a68;
            u32 unk3a6c;
            u32 unk3a70;
            u32 unk3a74;
            u32 unk3a78;
            u32 unk3a7c;
            u32 unk3a80;
            u32 unk3a84;
            u32 unk3a88;
            u32 unk3a8c;
            u32 unk3a90;
            u32 unk3a94;
            u32 unk3a98;
            u32 unk3a9c;
            u32 unk3aa0;
            u32 unk3aa4;
            u32 unk3aa8;
            u32 unk3aac;
            u32 unk3ab0;
            u32 unk3ab4;
            u32 unk3ab8;
            u32 unk3abc;
            u32 unk3ac0;
            u32 unk3ac4;
            u32 unk3ac8;
            u32 unk3acc;
            u32 unk3ad0;
            u32 unk3ad4;
            u32 unk3ad8;
            u32 unk3adc;
            u32 unk3ae0;
            u32 unk3ae4;
            u32 unk3ae8;
            u32 unk3aec;
            u32 unk3af0;
            u32 unk3af4;
            u32 unk3af8;
            u32 unk3afc;
            u32 unk3b00;
            u32 unk3b04;
            u32 unk3b08;
            u32 unk3b0c;
            u32 unk3b10;
            u32 unk3b14;
            u32 unk3b18;
            u32 unk3b1c;
            u32 unk3b20;
            u32 unk3b24;
            u32 unk3b28;
            u32 unk3b2c;
            u32 unk3b30;
            u32 unk3b34;
            u32 unk3b38;
            u32 unk3b3c;
            u32 unk3b40;
            u32 unk3b44;
            u32 unk3b48;
            u32 unk3b4c;
            u32 unk3b50;
            u32 unk3b54;
            u32 unk3b58;
            u32 unk3b5c;
            u32 unk3b60;
            u32 unk3b64;
            u32 unk3b68;
            u32 unk3b6c;
            u32 unk3b70;
            u32 unk3b74;
            u32 unk3b78;
            u32 unk3b7c;
            u32 unk3b80;
            u32 unk3b84;
            u32 unk3b88;
            u32 unk3b8c;
            u32 unk3b90;
            u32 unk3b94;
            u32 unk3b98;
            u32 unk3b9c;
            u32 unk3ba0;
            u32 unk3ba4;
            u32 unk3ba8;
            u32 unk3bac;
            u32 unk3bb0;
            u32 unk3bb4;
            u32 unk3bb8;
            u32 unk3bbc;
            u32 unk3bc0;
            u32 unk3bc4;
            u32 unk3bc8;
            u32 unk3bcc;
            u32 unk3bd0;
            u32 unk3bd4;
            u32 unk3bd8;
            u32 unk3bdc;
            u32 unk3be0;
            u32 unk3be4;
            u32 unk3be8;
            u32 unk3bec;
            u32 unk3bf0;
            u32 unk3bf4;
            u32 unk3bf8;
            u32 unk3bfc;
            u32 unk3c00;
            u32 unk3c04;
            u32 unk3c08;
            u32 unk3c0c;
            u32 unk3c10;
            u32 unk3c14;
            u32 unk3c18;
            u32 unk3c1c;
            u32 unk3c20;
            u32 unk3c24;
            u32 unk3c28;
            u32 unk3c2c;
            u32 unk3c30;
            u32 unk3c34;
            u32 unk3c38;
            u32 unk3c3c;
            u32 unk3c40;
            u32 unk3c44;
            u32 unk3c48;
            u32 unk3c4c;
            u32 unk3c50;
            u32 unk3c54;
            u32 unk3c58;
            u32 unk3c5c;
            u32 unk3c60;
            u32 unk3c64;
            u32 unk3c68;
            u32 unk3c6c;
            u32 unk3c70;
            u32 unk3c74;
            u32 unk3c78;
            u32 unk3c7c;
            u32 unk3c80;
            u32 unk3c84;
            u32 unk3c88;
            u32 unk3c8c;
            u32 unk3c90;
            u32 unk3c94;
            u32 unk3c98;
            u32 unk3c9c;
            u32 unk3ca0;
            u32 unk3ca4;
            u32 unk3ca8;
            u32 unk3cac;
            u32 unk3cb0;
            u32 unk3cb4;
            u32 unk3cb8;
            u32 unk3cbc;
            u32 unk3cc0;
            u32 unk3cc4;
            u32 unk3cc8;
            u32 unk3ccc;
            u32 unk3cd0;
            u32 unk3cd4;
            u32 unk3cd8;
            u32 unk3cdc;
            u32 unk3ce0;
            u32 unk3ce4;
            u32 unk3ce8;
            u32 unk3cec;
            u32 unk3cf0;
            u32 unk3cf4;
            u32 unk3cf8;
            u32 unk3cfc;
            u32 unk3d00;
            u32 unk3d04;
            u32 unk3d08;
            u32 unk3d0c;
            u32 unk3d10;
            u32 unk3d14;
            u32 unk3d18;
            u32 unk3d1c;
            u32 unk3d20;
            u32 unk3d24;
            u32 unk3d28;
            u32 unk3d2c;
            u32 unk3d30;
            u32 unk3d34;
            u32 unk3d38;
            u32 unk3d3c;
            u32 unk3d40;
            u32 unk3d44;
            u32 unk3d48;
            u32 unk3d4c;
            u32 unk3d50;
            u32 unk3d54;
            u32 unk3d58;
            u32 unk3d5c;
            u32 unk3d60;
            u32 unk3d64;
            u32 unk3d68;
            u32 unk3d6c;
            u32 unk3d70;
            u32 unk3d74;
            u32 unk3d78;
            u32 unk3d7c;
            u32 unk3d80;
            u32 unk3d84;
            u32 unk3d88;
            u32 unk3d8c;
            u32 unk3d90;
            u32 unk3d94;
            u32 unk3d98;
            u32 unk3d9c;
            u32 unk3da0;
            u32 unk3da4;
            u32 unk3da8;
            u32 unk3dac;
            u32 unk3db0;
            u32 unk3db4;
            u32 unk3db8;
            u32 unk3dbc;
            u32 unk3dc0;
            u32 unk3dc4;
            u32 unk3dc8;
            u32 unk3dcc;
            u32 unk3dd0;
            u32 unk3dd4;
            u32 unk3dd8;
            u32 unk3ddc;
            u32 unk3de0;
            u32 unk3de4;
            u32 unk3de8;
            u32 unk3dec;
            u32 unk3df0;
            u32 unk3df4;
            u32 unk3df8;
            u32 unk3dfc;
            u32 unk3e00;
            u32 unk3e04;
            u32 unk3e08;
            u32 unk3e0c;
            u32 unk3e10;
            u32 unk3e14;
            u32 unk3e18;
            u32 unk3e1c;
            u32 unk3e20;
            u32 unk3e24;
            u32 unk3e28;
            u32 unk3e2c;
            u32 unk3e30;
            u32 unk3e34;
            u32 unk3e38;
            u32 unk3e3c;
            u32 unk3e40;
            u32 unk3e44;
            u32 unk3e48;
            u32 unk3e4c;
            u32 unk3e50;
            u32 unk3e54;
            u32 unk3e58;
            u32 unk3e5c;
            u32 unk3e60;
            u32 unk3e64;
            u32 unk3e68;
            u32 unk3e6c;
            u32 unk3e70;
            u32 unk3e74;
            u32 unk3e78;
            u32 unk3e7c;
            u32 unk3e80;
            u32 unk3e84;
            u32 unk3e88;
            u32 unk3e8c;
            u32 unk3e90;
            u32 unk3e94;
            u32 unk3e98;
            u32 unk3e9c;
            u32 unk3ea0;
            u32 unk3ea4;
            u32 unk3ea8;
            u32 unk3eac;
            u32 unk3eb0;
            u32 unk3eb4;
            u32 unk3eb8;
            u32 unk3ebc;
            u32 unk3ec0;
            u32 unk3ec4;
            u32 unk3ec8;
            u32 unk3ecc;
            u32 unk3ed0;
            u32 unk3ed4;
            u32 unk3ed8;
            u32 unk3edc;
            u32 unk3ee0;
            u32 unk3ee4;
            u32 unk3ee8;
            u32 unk3eec;
            u32 unk3ef0;
            u32 unk3ef4;
            u32 unk3ef8;
            u32 unk3efc;
            u32 unk3f00;
            u32 unk3f04;
            u32 unk3f08;
            u32 unk3f0c;
            u32 unk3f10;
            u32 unk3f14;
            u32 unk3f18;
            u32 unk3f1c;
            u32 unk3f20;
            u32 unk3f24;
            u32 unk3f28;
            u32 unk3f2c;
            u32 unk3f30;
            u32 unk3f34;
            u32 unk3f38;
            u32 unk3f3c;
            u32 unk3f40;
            u32 unk3f44;
            u32 unk3f48;
            u32 unk3f4c;
            u32 unk3f50;
            u32 unk3f54;
            u32 unk3f58;
            u32 unk3f5c;
            u32 unk3f60;
            u32 unk3f64;
            u32 unk3f68;
            u32 unk3f6c;
            u32 unk3f70;
            u32 unk3f74;
            u32 unk3f78;
            u32 unk3f7c;
            u32 unk3f80;
            u32 unk3f84;
            u32 unk3f88;
            u32 unk3f8c;
            u32 unk3f90;
            u32 unk3f94;
            u32 unk3f98;
            u32 unk3f9c;
            u32 unk3fa0;
            u32 unk3fa4;
            u32 unk3fa8;
            u32 unk3fac;
            u32 unk3fb0;
            u32 unk3fb4;
            u32 unk3fb8;
            u32 unk3fbc;
            u32 unk3fc0;
            u32 unk3fc4;
            u32 unk3fc8;
            u32 unk3fcc;
            u32 unk3fd0;
            u32 unk3fd4;
            u32 unk3fd8;
            u32 unk3fdc;
            u32 unk3fe0;
            u32 unk3fe4;
            u32 unk3fe8;
            u32 unk3fec;
            u32 unk3ff0;
            u32 unk3ff4;
            u32 unk3ff8;
            u32 unk3ffc;
            u32 unk4000;
            u32 unk4004;
            u32 unk4008;
            u32 unk400c;
            u32 unk4010;
            u32 unk4014;
            u32 unk4018;
            u32 unk401c;
            u32 unk4020;
            u32 unk4024;
            u32 unk4028;
            u32 unk402c;
            u32 unk4030;
            u32 unk4034;
            u32 unk4038;
            u32 unk403c;
            u32 unk4040;
            u32 unk4044;
            u32 unk4048;
            u32 unk404c;
            u32 unk4050;
            u32 unk4054;
            u32 unk4058;
            u32 unk405c;
            u32 unk4060;
            u32 unk4064;
            u32 unk4068;
            u32 unk406c;
            u32 unk4070;
            u32 unk4074;
            u32 unk4078;
            u32 unk407c;
            u32 unk4080;
            u32 unk4084;
            u32 unk4088;
            u32 unk408c;
            u32 unk4090;
            u32 unk4094;
            u32 unk4098;
            u32 unk409c;
            u32 unk40a0;
            u32 unk40a4;
            u32 unk40a8;
            u32 unk40ac;
            u32 unk40b0;
            u32 unk40b4;
            u32 unk40b8;
            u32 unk40bc;
            u32 unk40c0;
            u32 unk40c4;
            u32 unk40c8;
            u32 unk40cc;
            u32 unk40d0;
            u32 unk40d4;
            u32 unk40d8;
            u32 unk40dc;
            u32 unk40e0;
            u32 unk40e4;
            u32 unk40e8;
            u32 unk40ec;
            u32 unk40f0;
            u32 unk40f4;
            u32 unk40f8;
            u32 unk40fc;
            u32 unk4100;
            u32 unk4104;
            u32 unk4108;
            u32 unk410c;
            u32 unk4110;
            u32 unk4114;
            u32 unk4118;
            u32 unk411c;
            u32 unk4120;
            u32 unk4124;
            u32 unk4128;
            u32 unk412c;
            u32 unk4130;
            u32 unk4134;
            u32 unk4138;
            u32 unk413c;
            u32 unk4140;
            u32 unk4144;
            u32 unk4148;
            u32 unk414c;
            u32 unk4150;
            u32 coreteammap[17];
            s32 creditnum;
            u8 numthisslide;
            f32 slideage;
            f32 slidelifetime;
            u8 unk41a8[8];
            s8 unk41b0[2];
            struct creditsbglayer bglayers[4];
            u8 slidesenabled;
            s8 unk41f5;
            u8 unk41f6;
            f32 unk41f8;
            u8 unk41fc;
            u8 unk41fd;
            u8 unk41fe;
            u8 unk41ff;
            u8 unk4200;
            u32 unk4204;
            u8 unk4208;
            u32 unk420c;
            u32 unk4210;
            u32 unk4214;
            u32 unk4218;
            u32 unk421c;
            u32 unk4220;
            u32 unk4224;
            u32 unk4228;
            u32 unk422c;
            u32 unk4230;
            u32 unk4234;
            u32 unk4238;
            u32 unk423c;
};
s32 debug0f11ed70(void);
s32 debugEnableBgRendering(void);
s32 debugEnablePropRendering(void);
s32 debug0f11edb0(void);
s32 debug0f11edb8(void);
s32 debugIsRoomStateDebugEnabled(void);
s32 debugIsTurboModeEnabled(void);
s32 debugForceAllObjectivesComplete(void);
s32 debug0f11ee28(void);
s32 debug0f11ee40(void);
u32 debugGetSlowMotion(void);
s32 debug0f11ee88(void);
s32 debug0f11ee90(void);
s32 debug0f11eea8(void);
s32 debugDangerousProps(void);
s32 debugGetMotionBlur(void);
s32 debugEnableFootsteps(void);
s32 debugAllowEndLevel(void);
s32 debug0f11ef78(void);
s32 debug0f11ef80(void);
s32 func0f000920(s32 num1, s32 num2);
struct light *roomGetLight(s32 roomnum, s32 lightnum);
u8 func0f0009c0(s32 roomnum);
u8 func0f000a10(s32 roomnum);
u8 func0f000b24(s32 roomnum);
u8 roomGetBrightness(s32 room);
s32 func0f000c54(s32 roomnum);
f32 roomGetUnk5c(s32 roomnum);
f32 func0f000dbc(s32 roomnum);
s32 lightGetBboxCentre(s32 roomnum, u32 lightnum, struct coord *pos);
s32 lightIsHealthy(s32 roomnum, s32 lightnum);
s32 lightIsVulnerable(s32 roomnum, s32 lightnum);
s32 lightIsOn(s32 roomnum, s32 lightnum);
void roomSetUnk52(s32 roomnum, s32 value);
void roomSetDefaults(struct room *room);
Gfx *func0f001138(Gfx *gdl, s16 roomnum);
Gfx *func0f001300(Gfx *gdl);
void roomInitLights(s32 roomnum);
s32 func0f001734(struct coord *arg0, struct coord *arg1, s32 roomnum);
void roomSetLightsFaulty(s32 roomnum, s32 chance);
void roomSetLightBroken(s32 roomnum, s32 lightnum);
void func0f001bdc(void);
void func0f001c0c(void);
u32 func0f00215c(void);
u32 func0f0023b8(void);
u32 func0f00259c(void);
u32 func0f002844(void);
void func0f002a98(void);
void roomSetLightsOn(s32 roomnum, s32 enable);
void roomSetLighting(s32 roomnum, s32 operation, u8 arg2, u8 arg3, u8 arg4);
u32 func0f002ef8(void);
void lightingTick(void);
void func0f003444(void);
void func0f0035c0(void);
void func0f00372c(void);
void func0f0037ac(void);
void func0f004314(void);
void roomAdjustLighting(s32 roomnum, s32 start, s32 limit);
void func0f004558(s32 roomnum, s32 increment, s32 limit);
void func0f004604(s32 roomnum);
void func0f004c6c(void);
u32 func0f00505c(void);
u32 func0f0053d0(void);
void func0f0056f4(s32 room1, struct coord *coord1, s32 room2, struct coord *coord2, s32 arg4, f32 *arg5, s32 arg6);
void func0f005bb0(void);
s32 menudialogRetryMission(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 menudialogSolo2PEndscreenCompleted(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 menudialogSolo2PEndscreenFailed(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 menuhandlerDeclineMission(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerEndscreenCheats(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerContinueMission(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerReplayLastLevel(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerReplayPreviousMission(s32 operation, struct menuitem *item, union handlerdata *data);
char *menuDialogTitleRetryStageName(struct menudialog *dialog);
char *menuDialogTitleNextMissionStageName(struct menudialog *dialog);
char *soloMenuTextNumKills(struct menuitem *item);
char *soloMenuTextNumShots(struct menuitem *item);
char *soloMenuTextNumHeadShots(struct menuitem *item);
char *soloMenuTextNumBodyShots(struct menuitem *item);
char *soloMenuTextNumLimbShots(struct menuitem *item);
char *soloMenuTextNumOtherShots(struct menuitem *item);
char *soloMenuTextAccuracy(struct menuitem *item);
char *soloMenuTextMissionStatus(struct menuitem *item);
char *soloMenuTextAgentStatus(struct menuitem *item);
char *menuTitleStageCompleted(struct menuitem *item);
char *menuTextCurrentStageName3(struct menuitem *item);
char *menuTitleStageFailed(struct menuitem *item);
char *soloMenuTextMissionTime(struct menuitem *item);
struct menudialog *func0f10d730(void);
void func0f10d770(void);
void endscreenHandleContinue(s32 context);
char *soloMenuTextTimedCheatName(struct menuitem *item);
char *soloMenuTextCompletionCheatName(struct menuitem *item);
char *soloMenuTextTargetTime(struct menuitem *item);
void endscreenSetCoopCompleted(void);
void endscreenPrepare(void);
void soloPushCoopModeEndscreen(void);
void soloPushSoloModeEndscreen(void);
void soloPushAntiModeEndscreen(void);
s32 explosionCreateSimple(struct prop *prop, struct coord *pos, s16 *rooms, s16 type, s32 playernum);
s32 explosionCreateComplex(struct prop *prop, struct coord *pos, s16 *rooms, s16 type, s32 playernum);
f32 explosionGetHorizontalRangeAtFrame(struct explosion *exp, s32 frame);
f32 explosionGetVerticalRangeAtFrame(struct explosion *exp, s32 frame);
void explosionGetBboxAtFrame(struct coord *lower, struct coord *upper, s32 frame, struct prop *prop);
void explosionAlertChrs(f32 *radius, struct coord *noisepos);
s32 explosionCreate(struct prop *prop, struct coord *pos, s16 *rooms, s16 type, s32 playernum, s32 arg5, struct coord *arg6, s16 room, struct coord *arg8);
void func0f12acec(struct coord *arg0, struct coord *arg1, struct coord *arg2);
s32 func0f12af5c(struct explosion *exp, struct prop *prop, struct coord *pos1, struct coord *pos2);
u32 func0f12b0e0(void);
u32 func0f12bbdc(struct prop *prop);
u32 explosionTick(struct prop *prop);
Gfx *explosionRender(struct prop *prop, Gfx *gdl, s32 withalpha);
Gfx *explosionRenderPart(struct explosion *exp, struct explosionpart *part, Gfx *gdl, struct coord *coord, s32 arg4);
void explosionsInit(void);
void explosionsReset(void);
void *fileGetRomAddress(s32 filenum);
u32 fileGetRomSizeByTableAddress(u32 *filetableaddr);
s32 fileGetRomSizeByFileNum(s32 filenum);
u32 func0f166ea8(u32 *filetableaddr);
void func0f166eb4(void *dst, u32 scratchlen, u32 *romaddrptr, struct fileinfo *info);
void func0f166f74(void);
void func0f166ff0(u16 filenum, void *memaddr, s32 offset, u32 len);
u32 fileGetInflatedLength(u32 filenum);
void *func0f1670fc(s32 filenum, u32 arg1);
void *func0f167200(s32 filenum, s32 arg1, u8 *ptr, u32 size);
u32 fileGetSize(s32 filenum);
u32 fileGetUnk04(s32 filenum);
void func0f1672a8(s32 filenum, void *ptr, u32 size, s32 resizing);
void func0f1672f0(u8 arg0);
void func0f167330(void);
f32 floorf(f32 value);
s32 floor(f32 value);
void func0f000000(void);
void func0f000090(void);
void func0f000100(void);
void func0f000130(void);
u32 func0f0002a0(void);
u32 func0f000358(void);
u32 func0f00040c(void);
u32 func0f00052c(void);
u32 func0f000590(void);
u32 func0f0005c0(void);
u32 func0f000648(void);
void func0f000880(void);
s32 func0f005fd0(s32 animnum);
s32 chrChooseFootstepSound(struct chrdata *chr, s32 index);
void chrCheckFootstep(struct chrdata *chr);
void chrCheckFootstepMagic(struct chrdata *chr);
u32 colourBlend(u32 a, u32 b, u32 weight);
void func0f0069dc(void);
f32 func0f006b08(f32 arg0);
f32 func0f006b54(f32 arg0);
u32 func0f006ba0(void);
f32 func0f006bd0(f32 arg0);
u32 func0f006c80(void);
Gfx *func0f008558(Gfx *gdl, s32 arg1);
void nbombReset(struct nbomb *nbomb);
s32 nbombCalculateAlpha(struct nbomb *nbomb);
Gfx *nbombCreateGdl(void);
Gfx *nbombRender(Gfx *gdl, struct nbomb *nbomb, Gfx *subgdl);
void func0f0099a4(void);
void nbombInflictDamage(struct nbomb *nbomb);
void nbombTick(struct nbomb *nbomb);
void nbombsTick(void);
Gfx *nbombsRender(Gfx *gdl);
void nbombCreate(struct coord *pos, struct prop *prop);
f32 gasGetDoorFrac(s32 tagnum);
Gfx *func0f00a490(Gfx *gdl);
Gfx *gasRender(Gfx *gdl);
void loadTextureList(void);
void mpInitPresetFeatures(void);
void stageLoadCommonLang(s32 stagenum);
void func0f00b420(void);
void func0f00b480(void);
void func0f00b510(void);
void func0f00b62c(s32 numchrs);
void stageChooseActiveHeads(s32 stagenum);
void resetSomeStageThings(void);
void stageAllocateBgChrs(void);
void gvarsInitProps(void);
void gvarsInitRoomProps(void);
void setupInit(void);
void addLiftDoor(struct linkliftdoorobj *link);
void addPadlockedDoor(struct padlockeddoorobj *link);
void addSafeItem(struct safeitemobj *link);
void addConditionalScenery(struct linksceneryobj *link);
void addBlockedPath(struct blockedpathobj *link);
void func0f00cc8c(void);
void setupClearProxyMines(void);
s32 setupCountCommandType(u32 type);
void setupGenericObject(struct defaultobj *obj, s32 cmdindex);
void weaponAssignToHome(struct weaponobj *weapon, s32 cmdindex);
void setupHat(struct hatobj *hat, s32 cmdindex);
void setupKey(struct keyobj *key, s32 cmdindex);
void setupMine(struct mineobj *mine, s32 cmdindex);
void setupCctv(struct cctvobj *camera, s32 cmdindex);
void setupAutogun(struct autogunobj *autogun, s32 cmdindex);
void setupHangingMonitors(struct hangingmonitorsobj *monitors, s32 cmdindex);
void setupSingleMonitor(struct singlemonitorobj *monitor, s32 cmdindex);
void setupMultiMonitor(struct multimonitorobj *monitor, s32 cmdindex);
s32 func0f00e1f8(s32 padnum);
s32 func0f00e2b0(s32 padnum);
void setupDoor(struct doorobj *door, s32 cmdindex);
void setupHov(struct defaultobj *obj, struct hov *hov);
void func0f00e980(s32 stagenum, s32 arg0, s32 arg1, struct briefing *briefing);
void setupLoadFiles(s32 stagenum);
void setupParseObjects(s32 stagenum);
void func0f0108d0(void);
void func0f010bb0(void);
void func0f011110(void);
void func0f011124(s32 value);
void func0f011130(s32 arg0, s32 numchrs);
s32 weaponLoadProjectileModels(s32 weaponnum);
void currentPlayerInitEyespy(void);
void currentPlayerInit(void);
void func0f0125a0(s32 animnum, s32 loopframe, s32 endframe, s32 *arg3);
void currentPlayerInitAnimation(void);
void func0f012aa0(void);
void waypointsLoad(void);
void func0f013550(void);
void func0f013b80(void);
void func0f013ba0(void);
void stageLoadTiles(void);
void stageParseTiles(void);
void func0f013ee0(void);
void coverAllocateSpecial(u16 *specialcovernums);
void coverLoad(void);
void func0f0147a0(u32 stagenum);
void func0f0147d0(void);
void lasersightsReset(void);
void mpRemoveAllSimulants(void);
void aibotAllocate(s32 chrnum, s32 aibotnum);
void aibotAllocateInventory(struct chrdata *chr, s32 maxitems);
void chrsReset(void);
void alarmReset(void);
void objsReset(void);
u32 func0f0150a0(void);
void func0f015470(void);
void getitleInit(void *addr, u32 arg1);
Gfx *getitleRender(Gfx *gdl);
Gfx *func0f01afc0(Gfx *gdl);
f32 pdmodeGetEnemyReactionSpeed(void);
f32 pdmodeGetEnemyHealth(void);
f32 pdmodeGetEnemyDamage(void);
f32 pdmodeGetEnemyAccuracy(void);
void func0f01b148(u32 arg0);
void titleSetNextStage(s32 stagenum);
void func0f01bea0(void);
void menuCountDialogs(void);
void menuTickAll(void);
void func0f01d860(void);
void func0f01d8c0(void);
void func0f01d990(void);
u32 bodyGetRace(s32 bodynum);
s32 bodyLoad(s32 bodynum);
struct model *func0f02ce8c(s32 bodynum, s32 headnum, struct modelfiledata *bodyfiledata, struct modelfiledata *headfiledata, s32 sunglasses, struct model *model, s32 isplayer, u8 varyheight);
struct model *func0f02d338(s32 bodynum, s32 headnum, struct modelfiledata *bodyfiledata, struct modelfiledata *headfiledata, s32 sunglasses, u8 varyheight);
struct model *modelAllocateChr(s32 bodynum, s32 headnum, u32 spawnflags);
s32 func0f02d3f8(void);
s32 bodyChooseHead(s32 bodynum);
void chrUnpack(s32 stagenum, struct packedchr *packed, s32 cmdindex);
struct prop *propAllocateEyespy(struct pad *pad, s16 room);
void bodyCalculateHeadOffset(struct modelfiledata *headfiledata, s32 headnum, s32 bodynum);
u32 setupGetCommandLength(u32 *cmd);
u32 *setupGetPtrToCommandByIndex(u32 cmdindex);
s32 tagGetCommandIndex(struct tag *tag);
u32 setupGetCommandOffset(struct prop *prop);
s32 modelLoad(s32 propnum);
s32 func0f09220c(struct defaultobj *obj, struct coord *pos, f32 *realrot, struct coord *arg3, struct coord *arg4);
s32 func0f092304(struct defaultobj *obj, struct coord *arg1, struct coord *arg2);
void func0f09233c(struct defaultobj *obj, struct coord *pos, f32 *realrot, s16 *rooms);
void func0f0923d4(struct defaultobj *obj);
struct defaultobj *setupCommandGetObject(u32 cmdindex);
u32 func0f092484(void);
s32 func0f092610(struct prop *prop, s32 arg1);
void func0f0926bc(struct prop *prop, s32 arg1, u16 arg2);
s32 func0f0927d4(f32 arg0, f32 arg1, f32 arg2, f32 arg3, s32 arg4);
s32 channelGetUnk06(s32 channelnum);
u32 func0f09294c(void);
void func0f092a98(s32 channelnum);
void propsndPrintChannel(struct audiochannel *channel);
s32 propsndGetSubtitleOpacity(s32 channelnum);
void func0f092c04(s32 channelnum);
void func0f093508(void);
void func0f093630(struct prop *prop, f32 arg1, s32 arg2);
void func0f093790(struct prop *prop, s32 arg1);
void func0f0938ec(struct prop *prop);
s16 func0f0939f8(struct audiochannel *channel, struct prop *prop, s16 soundnum, s16 padnum, s32 arg4, u16 flags, u16 flags2, s32 arg7, struct coord *pos, f32 arg9, s16 *rooms, s32 room, f32 arg12, f32 arg13, f32 arg14);
void audioMuteChannel(s32 channelnum);
s32 audioIsChannelIdle(s32 channelnum);
void audioPlayFromProp2(s32 channelnum, s32 soundnum, s16 padnum, struct prop *prop, s32 arg4, s32 arg5, s32 arg6, u16 arg7);
u32 func0f0946b0(void);
u32 func0f094940(void);
u32 func0f094b1c(void);
u32 func0f094d78(void);
void func0f094ef4(struct coord *pos, s16 *rooms, s32 soundnum, s32 *arg3, s32 *arg4);
void func0f09505c(struct audiohandle *handle, struct coord *pos, f32 arg2, f32 arg3, f32 arg4, s16 *rooms, s16 soundnum, u16 arg7, u32 arg8);
u32 func0f095200(void);
s32 func0f095278(s32 channelnum);
u32 xorBaffbeff(u32 value);
u32 xorBabeffff(u32 value);
u32 xorBoobless(u32 value);
void tagsAllocatePtrs(void);
s32 objGetTagNum(struct defaultobj *obj);
s32 objectiveGetCount(void);
u32 objectiveGetDifficultyBits(s32 index);
s32 objectiveCheck(s32 index);
s32 objectiveIsAllComplete(void);
void objectivesDisableChecking(void);
void objectivesShowHudmsg(char *buffer, s32 hudmsgtype);
void objectivesCheckAll(void);
void objectiveCheckRoomEntered(s32 currentroom);
void objectiveCheckMultiroomEntered(s32 arg0, s16 *requiredrooms);
void objectiveCheckHolograph(f32 sqdist);
struct prop *chopperGetTargetProp(struct chopperobj *heli);
struct defaultobj *objFindByTagId(s32 tag_id);
struct tag *tagFindById(s32 tag_id);
void func0f096360(f32 mf[4][4]);
u32 func0f0964b4(void);
u32 func0f0965e4(void);
u32 func0f096698(void);
f32 func0f096700(f32 value);
s32 func0f096890(s32 arg0);
u16 func0f096910(s16 arg0);
s16 func0f096964(s16 arg0);
f32 func0f0969d0(f32 value);
f32 func0f096a7c(f32 value);
void func0f096b20(struct coord *arg0, struct coord *arg1, f32 standfrac, struct coord *vel);
void func0f096b70(struct coord *arg0, struct coord *arg1, struct coord *arg2, struct coord *arg3, f32 bondfadefracnew, struct coord *vel);
void func0f096ca0(struct coord *arg0, struct coord *arg1);
void func0f096ed4(f32 *arg0, Mtxf *arg1);
void func0f097044(Mtxf *matrix, struct coord *arg1);
u32 func0f097264(void);
void func0f0972b8(struct coord *arg0, struct coord *arg1, f32 arg2, f32 *arg3);
u32 func0f097518(void);
void func0f0976c0(struct coord *arg0, struct coord *arg1);
void func0f097738(f32 *arg0, f32 *arg1, f32 *arg2);
f32 func0f097a50(f32 value);
f32 func0f097aa0(f32 arg0);
void func0f097b40(f32 *a, f32 *b, f32 *dst);
void func0f097b64(f32 *param_1, f32 *param_2, f32 scale, f32 *dst);
void beamCreate(struct beam *beam, s32 weaponnum, struct coord *from, struct coord *to);
u32 func0f0ac138(void);
Gfx *func0f0ac4b8(Gfx *gdl, struct textureconfig *arg1, f32 arg2, struct coord *arg3, u32 colour1, f32 arg5, struct coord *arg6, u32 colour2);
Gfx *func0f0acb90(Gfx *gdl, struct beam *beam, s32 arg2, u32 arg3);
void beamTick(struct beam *beam);
u32 func0f0adcc8(void);
u32 func0f0ade00(void);
u32 func0f0ae964(void);
u32 func0f0aebe0(void);
s32 func0f0aec54(struct prop *prop);
s32 func0f0aeca8(struct prop *prop);
void func0f0aed3c(s32 index, struct coord *pos);
void func0f0aed70(s32 index, struct coord *pos);
void func0f0aeda4(s32 arg0, f32 arg1, u32 arg2);
void func0f0aeea8(s32 index, f32 arg1);
Gfx *func0f0aeed8(Gfx *gdl);
void func0f0aefb8(void);
s32 lasersightExists(s32 id, s32 *index);
Gfx *lasersightRenderDot(Gfx *gdl);
Gfx *lasersightRenderBeam(Gfx *gdl);
void func0f0b0268(s32 id, s32 arg1, struct coord *near, struct coord *far);
void lasersightSetDot(s32 arg0, struct coord *pos, struct coord *rot);
void lasersightFree(s32 arg0);
struct weapon *weaponFindById(s32 itemid);
struct weaponfunc *weaponGetFunctionById(u32 weaponnum, u32 which);
struct weaponfunc *gsetGetWeaponFunction2(struct gset *gset);
struct weaponfunc *gsetGetWeaponFunction(struct gset *gset);
struct weaponfunc *weaponGetFunction(struct gset *gset, s32 which);
struct weaponfunc *currentPlayerGetWeaponFunction(u32 hand);
struct inventory_class *func0f0b11bc(struct gset *gset);
struct inventory_ammo *weaponGetAmmoByFunction(u32 weaponnum, u32 funcnum);
f32 handGetXShift(s32 handnum);
f32 func0f0b131c(s32 handnum);
f32 currentPlayerGetGunZoomFov(void);
void currentPlayerZoomOut(f32 fovpersec);
void currentPlayerZoomIn(f32 fovpersec);
s32 weaponHasFlag(s32 itemid, u32 flag);
s32 weaponHasClassFlag(s32 weaponnum, u32 flag);
s32 func0f0b184c(s32 weaponnum, s32 funcnum, s32 arg2);
s32 currentPlayerGetDeviceState(s32 weaponnum);
void currentPlayerSetDeviceActive(s32 weaponum, s32 active);
u16 weaponGetModelNum(s32 weaponnum);
u16 weaponGetModelNum2(s32 weaponnum);
void handPopulateFromCurrentPlayer(s32 handnum, struct gset *gset);
struct inventory_ammo *gsetGetAmmoDefinition(struct gset *gset);
u8 gsetGetSingleUnk3c(struct gset *gset);
f32 gsetGetStrength(struct gset *gset);
f32 gsetGetDamage(struct gset *gset);
u8 gsetGetFireslotDuration(struct gset *gset);
u16 gsetGetSingleShootSound(struct gset *gset);
s32 gsetHasFunctionFlags(struct gset *gset, u32 flags);
s8 weaponGetNumTicksPerShot(u32 weaponnum, u32 funcindex);
u32 currentPlayerGetSight(void);
void func0f0b201c(struct gset *gset, f32 *arg1);
struct guncmd *gsetGetPriToSecAnim(struct gset *gset);
struct guncmd *gsetGetSecToPriAnim(struct gset *gset);
u32 func0f0b2150(void);
void func0f0b2740(Gfx **gdl, f32 *arg1, f32 *arg2, s32 width, s32 height, s32 arg5, s32 arg6, s32 arg7, u32 arg8);
void func0f0b278c(Gfx **gdl, f32 *arg1, f32 *arg2, s32 twidth, u32 theight, u32 arg5, u32 arg6, u32 arg7, u32 arg8, u32 arg9, u32 arg10, u32 arg11, u32 arg12, u32 arg13);
struct arg_test {
s16 unk0;
s16 unk2;
s32 unk4;
s32 unk8;
s32 unkc;
s32 unk10;
s16 unk14;
};
bool func0f0b28d0(struct arg_test *arg0, struct arg_test *arg1);
void func0f0b2904(void);
u32 func0f0b2b64(void);
struct model *modelInstantiate(struct modelfiledata *modelfiledata);
void modelFree(struct model *model);
struct model *func0f0b3280(struct modelfiledata *modelfiledata);
void func0f0b32a0(struct model *model, struct modelnode *node, struct modelfiledata *headfiledata);
struct anim *func0f0b32e4(void);
void animTurnOff(struct anim *anim);
s32 func0f0b3350(s32 value);
u32 func0f0b33f8(void);
u32 func0f0b3468(void);
u32 func0f0b34d8(void);
u32 func0f0b3548(void);
u32 func0f0b35b8(void);
void func0f0b3988(struct textureconfig *config);
void func0f0b39c0(Gfx **gdl, struct textureconfig *arg1, u32 arg2, u32 arg3, u32 arg4, u32 arg5, u32 arg6);
void currentPlayerSetScreenSize(f32 width, f32 height);
void currentPlayerSetScreenPosition(f32 left, f32 top);
void currentPlayerSetPerspective(f32 near, f32 fovy, f32 aspect);
f32 func0f0b49b8(f32 arg0);
void currentPlayerSetCameraScale(void);
void func0f0b4c3c(f32 *crosspos, struct coord *arg1, f32 arg2);
void func0f0b4d04(struct coord *in, f32 *out);
void func0f0b4d68(struct coord *in, struct coord *out);
void func0f0b4dec(struct coord *in, struct coord *out);
void func0f0b4e68(struct coord *in, f32 divisor, struct coord *out);
void func0f0b4eb8(struct coord *arg0, f32 arg1[2], f32 zoom, f32 aspect);
void currentPlayerSetUnk1738(void *matrix);
void currentPlayerSetUnk173c(Mtx *matrix);
Mtx *currentPlayerGetUnk173c(void);
void currentPlayerSetUnk006c(Mtxf *matrix);
Mtxf *currentPlayerGetUnk006c(void);
void currentPlayerSetUnk1750(void *value);
void *currentPlayerGetUnk1750(void);
void currentPlayerSetUnk1758(Mtx *matrix);
Mtx *currentPlayerGetUnk1758(void);
void currentPlayerSetMatrix1740(Mtxf *value);
Mtxf *func0f0b5050(Mtxf *matrix);
u32 func0f0b53a4(void);
Mtxf *currentPlayerGetMatrix1740(void);
void currentPlayerSetUnk1754(Mtxf *matrix);
Mtxf *currentPlayerGetUnk1754(void);
void currentPlayerSetUnk174c(Mtxf *matrix);
Mtxf *currentPlayerGetUnk174c(void);
void currentPlayerSetUnk175c(void *value);
void *currentPlayerGetUnk175c(void);
f32 currentPlayerGetLodScaleZ(void);
f32 currentPlayerGetScreenWidth(void);
f32 currentPlayerGetScreenHeight(void);
f32 currentPlayerGetScreenLeft(void);
f32 currentPlayerGetScreenTop(void);
f32 currentPlayerGetPerspAspect(void);
void func0f0b5838(void);
u32 func0f0b5b9c(void);
s32 func0f0b5d38(struct coord *pos, f32 arg1, struct var800a4640_00 *arg2);
s32 func0f0b6260(s16 *rooms, struct coord *coord, f32 arg2);
void func0f0b63b0(s32 portalnum, f32 frac);
void func0f0b6470(s32 portalnum, f32 frac);
f32 func0f0b6534(s32 arg0);
f32 func0f0b656c(s32 arg0);
void func0f0b65a8(s32 arg0);
void func0f0b65f8(void);
void acousticReset(void);
f32 playerChooseSpawnLocation(f32 chrwidth, struct coord *dstpos, s16 *dstrooms, struct prop *prop, s16 *spawnpads, s32 numspawnpads);
f32 playerChooseGeneralSpawnLocation(f32 chrwidth, struct coord *pos, s16 *rooms, struct prop *prop);
void currentPlayerStartNewLife(void);
void currentPlayerResetToDefault(void);
s32 currentPlayerAssumeChrForAnti(struct chrdata *chr, s32 param_2);
void currentPlayerSpawn(void);
void func0f0b85a0(struct playerbond *pb, struct coord *pos);
void func0f0b85f8(void);
void currentPlayerChooseBodyAndHead(s32 *bodynum, s32 *headnum, s32 *arg2);
void func0f0b8ba0(void);
void func0f0b9538(void);
void currentPlayerTickMpSwirl(void);
void currentPlayerExecutePreparedWarp(void);
void cameraDoAnimation(s16 anim_id);
void func0f0ba190(u32 arg0);
void func0f0ba29c(s32 arg0);
f32 cutsceneGetBlurFrac(void);
void currentPlayerSetZoomFovY(f32 fovy, f32 timemax);
f32 currentPlayerGetZoomFovY(void);
void func0f0ba8b0(f32 value);
f32 currentPlayerGetTeleportFovY(void);
void currentPlayerUpdateZoom(void);
void func0f0bace0(void);
void currentPlayerTickPauseMenu(void);
void currentPlayerPause(s32 root);
void func0f0baf38(void);
Gfx *func0f0baf84(Gfx *gdl);
Gfx *fadeDraw(Gfx *gdl, u32 r, u32 g, u32 b, f32 frac);
Gfx *currentPlayerDrawFade(Gfx *gdl);
void currentPlayerUpdateColourScreenProperties(void);
void currentPlayerTickChrFade(void);
void currentPlayerDisplayHealth(void);
void currentPlayerTickDamageAndHealth(void);
void currentPlayerDisplayDamage(void);
Gfx *hudRenderHealthBar(Gfx *gdl);
void currentPlayerSurroundWithExplosions(s32 arg0);
void currentPlayerTickExplode(void);
void viResetDefaultModeIf4Mb(void);
s16 viGetFbWidth(void);
s16 viGetFbHeight(void);
s32 func0f0bc4c0(void);
s16 currentPlayerGetViewportWidth(void);
s16 currentPlayerGetViewportLeft(void);
s16 currentPlayerGetViewportHeight(void);
s16 currentPlayerGetViewportTop(void);
f32 func0f0bd358(void);
void func0f0bd3c4(void);
void currentPlayerTickTeleport(f32 *arg0);
void currentPlayerConfigureVi(void);
void func0f0bd904(s32 arg0);
void func0f0bfc7c(struct coord *cam_pos, struct coord *cam_look, struct coord *cam_up);
Gfx *currentPlayerUpdateShootRot(Gfx *gdl);
void currentPlayerDisplayShield(void);
Gfx *currentPlayerRenderShield(Gfx *gdl);
Gfx *currentPlayerRenderHud(Gfx *gdl);
void currentPlayerDie(s32 force);
void currentPlayerDieByShooter(u32 shooter, s32 force);
void currentPlayerCheckIfShotInBack(s32 attackerplayernum, f32 x, f32 z);
f32 currentPlayerGetHealthBarHeightFrac(void);
void func0f0c1840(struct coord *pos, struct coord *up, struct coord *look, struct coord *pos2, s16 *rooms);
void func0f0c1ba4(struct coord *pos, struct coord *up, struct coord *look, struct coord *memcampos, s32 memcamroom);
void func0f0c1bd8(struct coord *pos, struct coord *up, struct coord *look);
void allPlayersClearMemCamRoom(void);
void currentPlayerSetPerimEnabled(struct prop *prop, s32 enable);
s32 playerUpdateGeometry(struct prop *prop, u8 **start, u8 **end);
void currentPlayerUpdatePerimInfo(void);
void propPlayerGetBbox(struct prop *prop, f32 *width, f32 *ymax, f32 *ymin);
f32 currentPlayerGetHealthFrac(void);
f32 currentPlayerGetShieldFrac(void);
void currentPlayerSetShieldFrac(f32 frac);
s32 getMissionTime(void);
s32 playerTickBeams(struct prop *prop);
s32 playerTick(struct prop *prop);
void func0f0c2a58(struct chrdata *chr, s32 crouchpos, f32 speedsideways, f32 speedforwards, f32 speedtheta, f32 *angleoffset, struct attackanimconfig **animcfg);
Gfx *playerRender(struct prop *prop, Gfx *gdl, s32 withalpha);
Gfx *currentPlayerLoadMatrix(Gfx *gdl);
void func0f0c3320(Mtxf *matrices, s32 count);
void setTickMode(s32 tickmode);
void func0f0b9650(void);
void playersBeginMpSwirl(void);
void func0f0b9a20(void);
void currentPlayerEndCutscene(void);
void currentPlayerPrepareWarpToPad(s16 pad_id);
void currentPlayerPrepareWarpType2(struct var8009ddec *cmd, s32 arg1, s32 arg2);
void currentPlayerPrepareWarpType3(f32 arg0, f32 arg1, f32 arg2, f32 arg3, f32 arg4, s32 arg5);
void func0f0ba010(void);
void currentPlayerSetFadeColour(s32 r, s32 g, s32 b, f32 a);
void currentPlayerAdjustFade(f32 maxfadetime, s32 r, s32 g, s32 b, f32 frac);
void currentPlayerSetFadeFrac(f32 maxfadetime, f32 frac);
s32 currentPlayerIsFadeComplete(void);
void currentPlayerStartChrFade(f32 duration60, f32 targetfrac);
void optionsSetHiRes(s32 enable);
void currentPlayerAutoWalk(s16 aimpad, u8 walkspeed, u8 turnspeed, u8 lookup, u8 dist);
void currentPlayerLaunchSlayerRocket(struct weaponobj *rocket);
void currentPlayerSetGlobalDrawWorldOffset(s32 room);
void currentPlayerSetGlobalDrawCameraOffset(void);
s32 currentPlayerIsHealthVisible(void);
void currentPlayerSetCameraMode(s32 mode);
void currentPlayerSetCamPropertiesWithRoom(struct coord *pos, struct coord *up, struct coord *look, s32 room);
void currentPlayerSetCamPropertiesWithoutRoom(struct coord *pos, struct coord *up, struct coord *look, s32 room);
void currentPlayerSetCamProperties(struct coord *pos, struct coord *up, struct coord *look, s32 room);
void currentPlayerClearMemCamRoom(void);
void func0f0c33f0(Mtxf *matrices, s32 count);
u32 func0f0d4690(void);
u32 func0f0d475c(void);
Gfx *func0f0d479c(Gfx *gdl);
Gfx *func0f0d49c8(Gfx *gdl);
Gfx func0f0d4a3c(Gfx *gdl, s32 arg1);
u32 func0f0d4c80(void);
u32 func0f0d4d0c(void);
void savefileGetSomething(s32 *arg0, s32 arg1, s32 arg2);
u32 func0f0d5360(void);
s32 savefileGetInteger(s32 *arg0, s32 arg1);
u32 func0f0d545c(void);
void func0f0d5484(s32 *arg0, char *arg1, s32 arg2);
u32 func0f0d54c4(void);
void func0f0d54e4(s32 *arg0, char *arg1, s32 arg2);
u32 func0f0d55a4(void);
void func0f0d564c(char *arg0, char *buffer, s32 arg2);
void func0f0d5690(void *arg0, char *buffer);
u32 func0f0d575c(void);
u32 func0f0d579c(void);
void formatTime(char *dst, s32 time, u32 arg2);
void func0f0d5a7c(void);
u32 func0f0e0770(void);
u32 func0f0e0998(void);
u32 func0f0e0dac(void);
u32 func0f0e1668(void);
u32 func0f0e194c(void);
u32 func0f0e1ce8(void);
u32 func0f0e1fac(void);
Gfx *func0f0e2348(Gfx *gdl);
Gfx *func0f0e2498(Gfx *gdl);
Gfx *func0f0e258c(Gfx *gdl, s32 x1, s32 y1, s32 x2, s32 y2, s32 arg5, s32 arg6, s32 arg7);
Gfx *renderLine(Gfx *gdl, s32 x1, s32 y1, s32 x2, s32 y2, s32 arg5, s32 arg6);
Gfx *func0f0e2744(Gfx *gdl, s32 x1, s32 y1, s32 x2, s32 y2, u32 colourleft, u32 colourright);
Gfx *func0f0e2aa4(Gfx *gdl, s32 x1, s32 y1, s32 x2, s32 y2, u32 colourleft, u32 arg6, u32 arg7, u32 arg8);
u32 func0f0e2ee8(void);
Gfx *renderFilledRect(Gfx *gdl, s32 x1, s32 y1, s32 x2, s32 y2, u32 colourleft, u32 colourright);
Gfx *menuRenderCarouselArrow(Gfx *gdl, s32 x, s32 y, u32 arg3, u32 arg4, s32 arg5, u32 colour);
u32 func0f0e3324(void);
Gfx *renderCheckbox(Gfx *gdl, s32 x, s32 y, s32 size, s32 checked, s32 bordercolour, s32 fillcolour);
u32 func0f0e39d0(void);
Gfx *func0f0e4190(Gfx *gdl);
void func0f0e4fd4(void);
u32 func0f0e4fe0(void);
u32 func0f0e5000(void);
extern const struct menucolourpalette g_MenuColourPalettes[];
extern const struct menucolourpalette g_MenuColourPalettes2[];
extern const struct menucolourpalette g_MenuColourPalettes3[];
void menuPlaySound(s32 menusound);
s32 menuIsSoloMissionOrMp(void);
s32 currentPlayerIsMenuOpenInSoloOrMp(void);
s32 func0f0f0c68(void);
void func0f0f0ca0(s32 value, u32 allplayers);
u32 func0f0f0ce8(void);
struct menudfc *func0f0f1338(struct menuitem *item);
void func0f0f139c(struct menuitem *item, f32 arg1);
void func0f0f13ec(struct menuitem *item);
void func0f0f1418(void);
void func0f0f1494(void);
char *menuResolveText(u32 thing, struct menuitem *item);
char *menuResolveParam2Text(struct menuitem *item);
char *menuResolveParam1Text(struct menuitem *item);
void func0f0f15a4(u8 *arg0, u32 *arg1);
void func0f0f1618(struct menuitem *item, void *arg1, void *arg2, struct menuframe *frame);
void func0f0f1d6c(struct menudialog *dialog, struct menuframe *frame, struct menu *menu);
u32 func0f0f1ef4(void);
void func0f0f2134(struct menudialog *dialog, struct menuframe *frame, struct menu *menu);
u32 func0f0f2354(struct menuframe *frame, struct menuitem *item, u32 *arg2, u32 *arg3);
s32 menuIsScrollableUnscrollable(struct menuitem *item);
s32 menuIsItemDisabled(struct menuitem *item, struct menuframe *frame);
s32 func0f0f2674(struct menuitem *item, struct menuframe *frame, u32 arg2);
u32 func0f0f26fc(void);
struct menuitem *func0f0f288c(struct menuframe *frame);
u32 func0f0f2928(void);
u32 func0f0f29cc(void);
u32 func0f0f2b2c(void);
u32 func0f0f2c44(void);
void menuOpenDialog(struct menudialog *dialog, struct menuframe *arg1, struct menu *menu);
void menuPushDialog(struct menudialog *dialog);
void func0f0f3220(s32 arg0);
void menuCloseDialog(void);
void menuUpdateCurFrame(void);
void menuPopDialog(void);
void func0f0f3704(struct menudialog *dialog);
void func0f0f372c(u8 *arg0, f32 x, f32 y, f32 arg3, f32 arg4, f32 arg5, f32 arg6, f32 arg7, s32 arg8);
void func0f0f37a4(u8 *arg0);
Gfx *func0f0f38b0(Gfx *gdl, u8 *arg1, u32 arg2);
u32 func0f0f5004(void);
Gfx *func0f0f50fc(Gfx *gdl);
u32 func0f0f5360(void);
u32 func0f0f74a8(void);
u32 func0f0f7594(void);
u32 func0f0f7728(void);
void func0f0f7e98(struct menuframe *frame);
void func0f0f8040(void);
void func0f0f8120(void);
void func0f0f820c(struct menudialog *dialog, s32 root);
s32 func0f0f82a8(s32 arg0);
void func0f0f8300(void);
void menuPushRootDialog(struct menudialog *dialog, s32 arg1);
void func0f0f85e0(struct menudialog *dialog, s32 root);
u32 func0f0f8634(void);
u32 func0f0f86a8(void);
void func0f0f8bb4(u8 *arg0, u32 arg1, u32 arg2);
void menuInit(void);
u32 func0f0f9030(void);
u32 func0f0f935c(void);
void func0f0fa574(struct menuframe *frame);
void func0f0fa6ac(void);
void func0f0fa704(void);
u32 func0f0fb488(void);
Gfx *func0f0fbba0(Gfx *gdl, u8 param_2, s32 arg2);
Gfx *func0f0fbc30(Gfx *gdl);
u32 menuChooseMusic(void);
u32 menuGetRoot(void);
u32 func0f0fce8c(void);
char *menuTextSaveDeviceName(struct menuitem *item);
u32 func0f0fd118(u32 arg0);
s32 func0f0fd1f4(u32 arg0, u32 arg1);
u32 func0f0fd320(void);
void func0f0fd494(struct coord *pos);
void func0f0fd548(s32 arg0);
s32 menuIsDialogOpen(struct menudialog *dialog);
struct chrdata *currentPlayerGetCommandingAibot(void);
s32 menuhandler000fcc34(s32 operation, struct menuitem *item, union handlerdata *data);
s32 amPickTargetMenuList(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerRepairPak(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerRetrySavePak(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerWarnRepairPak(s32 operation, struct menuitem *item, union handlerdata *data);
extern const char var7f1b2658[];
extern const char var7f1b265c[];
extern const char var7f1b2660[];
extern const char var7f1b2664[];
extern u16 g_ControlStyleOptions[];
char *menuTextCurrentStageName(struct menuitem *item);
char *soloMenuTextDifficulty(struct menuitem *item);
s32 menuhandlerControlStyleImpl(s32 operation, struct menuitem *item, union handlerdata *data, s32 mpindex);
char *soloMenuTitleStageOverview(struct menudialog *dialog);
f32 func0f1036ac(u8 value, s32 prop);
s32 isStageDifficultyUnlocked(s32 stageindex, s32 difficulty);
char *soloMenuTextBestTime(struct menuitem *item);
s32 getMaxAiBuddies(void);
s32 getNumUnlockedSpecialStages(void);
s32 func0f104720(s32 value);
char *func0f105664(struct menuitem *item);
char *func0f1056a0(struct menuitem *item);
char *invMenuTextPrimaryFunction(struct menuitem *item);
char *invMenuTextSecondaryFunction(struct menuitem *item);
void func0f105948(s32 weaponnum);
char *invMenuTextWeaponName(struct menuitem *item);
char *invMenuTextWeaponManufacturer(struct menuitem *item);
char *invMenuTextWeaponDescription(struct menuitem *item);
s32 soloChoosePauseDialog(void);
s32 menudialogBriefing(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 menudialog00103608(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 menudialogCoopAntiOptions(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 menudialog0010559c(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 inventoryMenuDialog(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 menudialogAbortMission(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 soloMenuDialogPauseStatus(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 menuhandler001024dc(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandler001024fc(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerAcceptMission(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerAcceptPdModeSettings(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerBuddyOptionsContinue(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerCoopDifficulty(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerAntiDifficulty(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandler0010476c(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerControlStyle(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandler00106028(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandler00106178(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerAbortMission(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerCinema(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerAimControl(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerAlternativeTitle(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerAlwaysShowTarget(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerAmmoOnScreen(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerAntiPlayer(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerAntiRadar(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerAutoAim(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerChangeAgent(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerCoopBuddy(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerCoopFriendlyFire(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerCoopRadar(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerCutsceneSubtitles(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerHeadRoll(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerHiRes(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerInGameSubtitles(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerLangFilter(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerLookAhead(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMusicVolume(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerPaintball(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerPdMode(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerPdModeSetting(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerReversePitch(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerScreenRatio(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerScreenSize(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerScreenSplit(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerSfxVolume(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerShowGunFunction(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerShowMissionTime(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerShowZoomRange(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerSightOnScreen(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerSoloDifficulty(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerSoundMode(s32 operation, struct menuitem *item, union handlerdata *data);
char *filemgrGetDeviceName(s32 index);
char *filemgrMenuTextDeviceName(struct menuitem *item);
void filemgrGetFileName(char *buffer, struct savelocation000 *arg1, u32 filetype);
char *filemgrMenuTextDeleteFileName(struct menuitem *item);
void func0f108324(s32 arg0);
void func0f1083b0(struct savelocation000 *arg0);
void func0f1083d0(struct savelocation000 *arg0, s32 filetype);
char *filemgrMenuTextFailReason(struct menuitem *item);
char *filemgrMenuTextDeviceNameForError(struct menuitem *item);
void filemgrPushErrorDialog(u16 errno);
s32 filemgrGetDeviceNameOrStartIndex(s32 listnum, s32 operation, s32 optionindex);
char *filemgrMenuTextErrorTitle(struct menuitem *item);
char *filemgrMenuTextFileType(struct menuitem *item);
void func0f10898c(void);
void func0f108a80(void);
void filemgrEraseCorruptFile(void);
char *filemgrMenuTextInsertOriginalPak(struct menuitem *item);
void filemgrRetrySave(s32 arg0);
s32 fileSave(s32 arg0, s32 arg1);
s32 func0f1094e4(struct savelocation_2d8 *arg0, s32 arg1, void *arg2);
void filemgrDeleteCurrentFile(void);
void func0f1097d0(s32 device);
void func0f109954(s32 arg0);
void func0f1099a8(char *buffer, struct savelocation000 *arg1);
void filemgrGetRenameName(char *buffer);
void filemgrSetRenameName(char *name);
s32 filemgrIsNameAvailable(s32 arg0);
void func0f109ec4(void);
char *filemgrMenuTextDeviceNameContainingDuplicateFile(struct menuitem *item);
char *filemgrMenuTextDuplicateFileName(struct menuitem *item);
char *filemgrMenuTextLocationName2(struct menuitem *item);
char *filemgrMenuTextSaveLocationSpaces(struct menuitem *item);
void filemgrPushSelectLocationDialog(s32 arg0, u32 arg1);
char *filemgrMenuTextFileInUseDescription(struct menuitem *item);
Gfx *filemgrRenderPerfectHeadThumbnail(Gfx *gdl, struct menuitemrenderdata *renderdata, u32 arg2, u32 arg3);
s32 filemgrIsFileInUse(struct savelocation000 *arg0);
s32 filemgrFileToCopyOrDeleteListMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data, s32 isdelete);
void filemgrPushDeleteFileDialog(s32 listnum);
char *pakMenuTextPagesFree(struct menuitem *item);
char *pakMenuTextPagesUsed(struct menuitem *item);
char *pakMenuTextStatusMessage(struct menuitem *item);
char *pakMenuTextEditingPakName(struct menuitem *item);
s32 filemgrConsiderPushingFileSelectDialog(void);
void pakPushPakMenuDialog(void);
s32 filemgrChooseAgentListMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 pakGameNoteListMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 filemgrFileToCopyListMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 filemgrFileToDeleteListMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 filemgrInsertOriginalPakMenuDialog(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 filemgrCopyOrDeleteListMenuDialog(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 pakGameNotesMenuDialog(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 pakChoosePakMenuDialog(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 filemgrMainMenuDialog(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 filemgrDeviceNameMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 filemgrFileNameMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 filemgrDeviceNameForErrorMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 filemgrRetrySaveMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 filemgrCancelSave2MenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 filemgrAcknowledgeFileLostMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 filemgrReinsertedOkMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 filemgrReinsertedCancelMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 filemgrDuplicateRenameMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 filemgrDuplicateCancelMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 filemgrCancelSaveMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 filemgrDeleteFilesForSaveMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 filemgrConfirmDeleteMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 pakDeleteGameNoteMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 pakSelectionMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 filemgrAgentNameKeyboardMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 filemgrOpenCopyFileMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 filemgrOpenDeleteFileMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 filemgrConfirmRenameMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 filemgrSaveElsewhereYesMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 filemgrSelectLocationMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
extern const char var7f1b3214[];
extern const char var7f1b3218[];
extern const char var7f1b321c[];
extern const char var7f1b3220[];
void func0f10cb2c(void);
s32 menuhandler4MbDropOut(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandler0010ca1c(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandler4MbAdvancedSetup(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandler0010cabc(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menudialog4MbMainMenu(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 func0f1106c8(void);
u32 func0f110720(void);
u32 func0f11080c(void);
void func0f1109c0(void);
void func0f110b68(void);
void func0f110bf0(void);
void func0f110bf8(void);
void func0f110c5c(s32 listnum, u8 filetype);
u8 func0f110cf8(u8 arg0);
u32 func0f110d90(void);
void func0f110da8(void);
u32 func0f110f4c(void);
struct textureconfig *func0f111460(s32 playernum, s32 arg1, s32 arg2);
u32 func0f11f000(void);
u32 func0f11f07c(void);
u32 func0f11f1fc(void);
void func0f11f384(struct coord *arg0, struct coord *arg1, struct coord *out);
f32 func0f11f3d0(f32 value, f32 min, f32 max);
f32 func0f11f410(f32 value);
u32 func0f11f438(void);
u32 func0f11f6ec(void);
Gfx *func0f11f984(Gfx *gdl);
u32 func0f1228d0(void);
u32 func0f122ce8(void);
u32 func0f122d4c(void);
u32 func0f123fd4(void);
u32 func0f125948(void);
u32 func0f125a1c(void);
u32 func0f125a6c(void);
u32 func0f126384(void);
u32 func0f126c3c(void);
u32 func0f126de8(void);
Gfx *func0f12715c(Gfx *gdl);
void func0f127334(s32 arg0, s32 arg1, s32 arg2);
s32 func0f127490(s32 arg0, s32 arg1);
Gfx *func0f1274d8(Gfx *gdl);
void func0f127910(void);
void playersUnrefAll(void);
void playersAllocate(s32 count);
void playerAllocate(s32 index);
void currentPlayerCalculateAiBuddyNums(void);
s32 propGetPlayerNum(struct prop *prop);
void currentPlayerSetViewSize(s32 viewx, s32 viewy);
void currentPlayerSetViewPosition(s32 viewleft, s32 viewtop);
void currentPlayerSetFovY(f32 fovy);
void currentPlayerSetAspectRatio(f32 aspect);
s32 weaponGetModel(s32 weapon);
void currentPlayerSetWeaponReapable(s32 hand);
void func0f128d20(s32 hand);
void randomisePlayerOrder(void);
s32 getPlayerByOrderNum(s32 arg0);
void setCurrentPlayerNum(s32 playernum);
u32 calculatePlayerIndex(u32 playernum);
u32 func0f128f30(void);
u32 func0f12906c(void);
u32 func0f1291b0(void);
u32 func0f1291f8(void);
void func0f129210(union modelrwdata *find, union modelrwdata *replacement);
void func0f12939c(void);
void *func0f12955c(s32 count, s32 index, s32 arg2, s32 arg3);
void func0f129818(s32 arg0, void *arg1);
u32 func0f13b670(void);
u32 func0f13b754(void);
u32 func0f13b7bc(void);
u32 func0f13b8a0(void);
u32 func0f13bb5c(void);
u32 func0f13bc48(void);
u32 func0f13c07c(void);
u32 func0f13c2e8(void);
u32 func0f13c370(void);
void func0f13c3f4(void);
u32 func0f13c4f0(void);
void func0f13c510(void);
void func0f13c54c(void);
u16 func0f13c574(f32 arg0);
u32 func0f13c710(void);
u32 func0f13c780(void);
u8 func0f13d3c4(u8 arg0, u8 arg1);
Gfx *func0f13d40c(Gfx *gdl);
Gfx *func0f13d54c(Gfx *gdl);
Gfx *func0f13d568(Gfx *gdl, u32 arg1);
u32 func0f152fa0(void);
Gfx *func0f153134(Gfx *gdl);
void func0f1531a0(void);
void func0f1531a8(u32 arg0);
void func0f1531b8(u32 arg0);
void func0f1531c4(u32 arg0);
void func0f1531d0(u32 arg0);
void func0f1531dc(s32 arg0);
void fontLoad(u8 *romstart, u8 *romend, struct font **font1, struct font2a4 **font2, s32 monospace);
void fontsLoadForCurrentStage(void);
Gfx *func0f153628(Gfx *gdl);
Gfx *func0f153780(Gfx *gdl);
Gfx *gfxSetPrimColour(Gfx *gdl, u32 colour);
Gfx *func0f153838(Gfx *gdl);
Gfx *func0f153858(Gfx *gdl, s32 *x1, s32 *y1, s32 *x2, s32 *y2);
Gfx *func0f1538e4(Gfx *gdl, s32 *x1, s32 *y1, s32 *x2, s32 *y2);
Gfx *func0f153990(Gfx *gdl, s32 left, s32 top, s32 width, s32 height);
Gfx *func0f153a34(Gfx *gdl, s32 x1, s32 y1, s32 x2, s32 y2, u32 colour);
Gfx *func0f153ab0(Gfx *arg0);
void func0f153b40(void);
void func0f153b6c(s32 arg0);
void func0f153c20(s32 x, s32 y, f32 arg2, u8 arg3);
void func0f153c50(void);
void func0f153c88(void);
void func0f153ce8(s32 x1, s32 x2, u32 arg2);
void func0f153d24(void);
void func0f153d3c(void);
void func0f153d50(s32 arg0, s32 arg1, s32 arg2);
void func0f153d88(f32 arg0);
void func0f153e38(u32 colour1, u32 colour2);
void func0f153e4c(void);
s32 func0f153e58(void);
u32 func0f153e94(s32 x, s32 y, s32 arg2);
u32 func0f1543ac(u32 arg0, u32 arg1, u32 colour);
Gfx *func0f154ecc(Gfx *gdl, u32 arg1, u32 arg2);
u32 func0f154f38(void);
u32 func0f1552d4(void);
u32 func0f15568c(void);
u32 func0f156024(void);
u32 func0f156030(void);
Gfx *textRenderProjected(Gfx *gdl, s32 *x, s32 *y, char *text, struct font2a4 *font1, struct font *font2, s32 colour, s16 width, s16 height, s32 arg9, u32 arg10);
Gfx *func0f1566cc(Gfx *gdl, u32 arg1, u32 arg2);
u32 func0f156790(void);
u32 func0f156a24(void);
Gfx *textRender(Gfx *gdl, s32 *x, s32 *y, char *text, struct font2a4 *font1, struct font *font2, u32 arg6, u32 colour, s16 arg8, s16 arg9, u32 arg10, u32 arg11);
void textMeasure(s32 *textheight, s32 *textwidth, char *text, struct font2a4 *font1, struct font *font2, s32 lineheight);
void textWrap(s32 width, char *in, char *out, struct font2a4 *font1, struct font *font2);
u32 func0f1577f0(void);
u32 func0f1578c8(void);
f32 func0f1579cc(f32 *arg0, f32 *arg1, f32 *arg2, f32 *arg3);
u32 func0f165360(void);
struct stagetableentry *stageGetCurrent(void);
s32 stageGetIndex(s32 stagenum);
u32 func0f165670(void);
u32 func0f165728(void);
struct var80081058 *func0f1657cc(void);
f32 func0f1657e4(void);
void func0f1657f8(void);
void skyApplyType1(struct skytype1 *sky);
void skyApplyType2(struct skytype2 *sky);
void skySetStageNum(s32 stagenum);
void skyChooseAndApply(s32 stagenum, s32 arg1);
void skySetTransitionFrac(f32 arg0);
Gfx *func0f1664a0(Gfx *gdl, s32 arg1);
Gfx *gfxConsiderDisableFog(Gfx *gdl);
s32 func0f1666f8(struct coord *pos, f32 arg1);
struct coord *func0f1667e8(void);
s32 func0f1667f4(struct prop *prop, f32 *arg1);
void currentPlayerSetLastRoomForOffset(s32 room);
void func0f1668f0(s32 index, s32 roomnum);
void func0f16692c(s32 index, s32 roomnum);
void func0f16696c(s32 index);
s32 func0f1669fc(void);
void func0f166a6c(Mtxf *matrix, s32 roomnum);
s32 func0f166c20(s32 roomnum);
Gfx *func0f166d7c(Gfx *gdl, s32 roomnum);
struct coord *func0f166dd0(s32 room);
void func0f166df0(s32 room, struct coord *globaldrawworldoffset);
void func0f173a08(void);
s32 func0f173a44(u32 value);
u32 func0f173a70(void);
u32 func0f173b8c(void);
u32 func0f173c10(void);
u32 func0f173cb8(void);
u32 func0f173d60(void);
u32 func0f173e10(void);
u32 func0f173e50(void);
s32 func0f173f18(s32 arg0);
u32 func0f173f48(u32 arg0);
u32 func0f173f78(void);
u32 func0f1742e4(void);
u32 func0f1743a0(void);
u32 func0f1747a4(void);
u32 func0f174b54(void);
u32 func0f174f30(void);
u32 func0f1751e4(void);
u32 func0f175308(void);
u32 func0f175490(void);
u32 func0f1755dc(void);
u32 func0f17563c(void);
u32 func0f1756c0(void);
u32 func0f175ef4(void);
extern const char var7f1b7ba0[];
extern const char var7f1b7ba8[];
extern const char var7f1b7bb0[];
extern const char var7f1b7bb8[];
extern const char var7f1b7bc0[];
extern const char var7f1b7bc8[];
extern const char var7f1b7bd8[];
extern const char var7f1b7be0[];
extern const char var7f1b7be8[];
extern const char var7f1b7bec[];
extern const char var7f1b7bf4[];
extern const char var7f1b7c00[];
extern const char var7f1b7c04[];
extern const char var7f1b7c10[];
extern const char var7f1b7c18[];
void func0f175f90(void);
void func0f175f98(void);
void func0f175fc8(void);
void *func0f176080(void);
void func0f17608c(s32 stagenum);
void func0f1760c4(void);
void func0f176298(void);
Gfx *func0f1762ac(Gfx *gdl);
Gfx *func0f1763f4(Gfx *gdl);
void *func0f176668(s32 arg0);
Gfx *func0f1766b4(Gfx *gdl);
s32 stageGetPrimaryTrack(s32 stagenum);
s32 stageGetAmbientTrack(s32 stagenum);
s32 stageGetXTrack(s32 stagenum);
s32 func0f190260(struct chrdata *chr);
void mpChrReset(struct chrdata *chr, u8 full);
void mpInitSimulant(struct chrdata *chr, u8 full);
void mpInitSimulants(void);
u32 add87654321(u32 value);
u32 propobjHandlePickupByAibot(struct prop *prop, struct chrdata *chr);
s32 func0f190be4(struct prop *prop, struct chrdata *chr);
void func0f19124c(struct chrdata *chr);
s32 chrGuessCrouchPos(struct chrdata *chr);
s32 func0f191448(struct chrdata *chr);
s32 func0f191638(struct chrdata *chr, s32 arg1);
s32 aibotTick(struct prop *prop);
f32 aibotCalculateMaxSpeed(struct chrdata *chr);
void func0f1921f8(struct chrdata *chr, f32 *arg1, s32 arg2, f32 arg3);
void aibotLoseGun(struct chrdata *chr, struct prop *attacker);
void func0f19277c(struct chrdata *chr, s32 propnum);
s32 func0f19294c(struct chrdata *botchr, struct chrdata *otherchr);
s32 chrHasGround(struct chrdata *chr);
void func0f192a74(struct chrdata *chr);
s32 func0f192d64(struct chrdata *botchr, struct chrdata *otherchr);
s32 func0f192dc0(struct chrdata *botchr, struct chrdata *chr);
u32 func0f192e90(void);
void func0f19369c(struct chrdata *chr, s32 arg1);
s32 func0f1937a4(struct chrdata *chr, s32 arg1);
s32 mpGetNumOpponentsInHill(struct chrdata *chr);
void func0f194b40(struct chrdata *chr);
s32 mpObjIsSafe(struct defaultobj *obj);
s32 mpchrGetWeaponNum(struct chrdata *chr);
u8 mpchrGetTargetsWeaponNum(struct chrdata *chr);
char *mpGetBotCommandName(s32 command);
void mpAibotApplyAttack(struct chrdata *chr, struct prop *prop);
void mpAibotApplyFollow(struct chrdata *chr, struct prop *prop);
void mpAibotApplyProtect(struct chrdata *chr, struct prop *prop);
void mpAibotApplyDefend(struct chrdata *chr, struct coord *pos, s16 *room, f32 arg3);
void mpAibotApplyHold(struct chrdata *chr, struct coord *pos, s16 *room, f32 arg3);
void mpAibotApplyScenarioCommand(struct chrdata *chr, u32 arg1);
s32 mpIsChrFollowedByChr(struct chrdata *leader, struct chrdata *follower);
s32 func0f193530(struct chrdata *chr, f32 range);
s32 func0f194670(struct chrdata *chr);
s32 func0f194694(struct chrdata *chr);
s32 func0f1946b4(struct chrdata *chr);
s32 mpGetNumPlayerTeammates(struct chrdata *chr);
s32 mpCountAibotsWithCommand(struct chrdata *self, u32 command, s32 includeself);
s32 scenarioCtcIsChrsTokenHeld(struct chrdata *chr);
s32 func0f19489c(struct chrdata *chr);
s32 mpGetNumTeammatesDefendingHill(struct chrdata *bot);
void func0f197544(struct chrdata *chr);
void func0f197600(struct chrdata *chr);
void mpAibotApplyCommand(struct chrdata *chr, u32 command);
void aibotClearInventory(struct chrdata *chr);
struct invitem *aibotGetFreeInvSlot(struct chrdata *chr);
struct invitem *aibotGetInvItem(struct chrdata *chr, u32 weaponnum);
void aibotRemoveInvItem(struct chrdata *chr, s32 weaponnum);
u32 aibotGetInvItemType(struct chrdata *chr, u32 weaponnum);
s32 aibotGiveSingleWeapon(struct chrdata *chr, u32 weaponnum);
void aibotGiveDualWeapon(struct chrdata *chr, u32 weaponnum);
s16 aibotGetWeaponPad(struct chrdata *chr, u32 weaponnum);
s32 aibotGiveProp(struct chrdata *chr, struct prop *prop);
void func0f198068(struct chrdata *chr, s32 *weaponnums, s32 *scores1, s32 *scores2);
s32 mpHasShield(void);
s32 mpGetWeaponIndexByWeaponNum(s32 weaponnum);
void aibotScoreWeapon(struct chrdata *chr, s32 weaponnum, s32 funcnum, s32 arg3, s32 arg4, s32 *dst1, s32 *dst2, s32 arg7, s32 arg8);
void func0f198db4(struct chrdata *chr, s32 weaponnum, s32 funcnum, s32 arg3, s32 arg4, s32 *dst1, s32 *dst2);
void func0f198df8(struct chrdata *chr, s32 weaponnum, s32 funcnum, s32 arg3, s32 arg4, s32 *dst1, s32 *dst2);
s32 func0f198e38(s32 weaponnum, s32 funcnum);
s32 func0f198e78(struct chrdata *chr, s32 weaponnum, s32 funcnum);
void aibotTickInventory(struct chrdata *chr);
s32 aibotSwitchToWeapon(struct chrdata *chr, s32 weaponnum, s32 funcnum);
void func0f19978c(struct chrdata *chr, s32 weaponnum, u8 arg2);
void func0f199964(struct chrdata *chr, u32 weaponnum);
void func0f199984(struct chrdata *chr, u32 weaponnum);
s32 weaponGetAmmoTypeByFunction(s32 weaponnum, u32 funcnum);
s32 weaponGetClipCapacityByFunction(s32 weaponnum, u32 funcnum);
void aibotReloadWeapon(struct chrdata *chr, s32 handnum, s32 withsound);
s32 aibotGetAmmoQuantityByWeapon(struct aibot *aibot, s32 weaponnum, s32 funcnum, s32 include_equipped);
s32 aibotGetAmmoQuantityByType(struct aibot *aibot, s32 ammotype, s32 include_equipped);
s32 aibotTryRemoveAmmoFromReserve(struct aibot *aibot, s32 weaponnum, s32 funcnum, s32 qty);
void aibotGiveAmmoByWeapon(struct aibot *aibot, s32 weaponnum, s32 funcnum, s32 qty);
void aibotGiveAmmoByType(struct aibot *aibot, u32 ammotype, s32 quantity);
s32 aibotDoFarsightThing(struct chrdata *chr, u32 arg1, struct coord *arg2, struct coord *arg3);
s32 func0f19a29c(u32 weaponnum, s32 is_secondary);
u32 aibotGetProjectileThrowInterval(u32 weapon);
u32 ammotypeGetWeapon(u32 ammotype);
void func0f19a37c(struct chrdata *chr);
s32 func0f19a60c(s32 weaponnum, s32 funcnum);
s32 func0f19a6d0(struct chrdata *chr, struct coord *frompos, struct coord *topos, s16 *fromrooms, s16 *torooms, struct projectile *projectile);
void func0f19a7d0(u16 padnum, struct coord *pos);
void aibotCreateSlayerRocket(struct chrdata *chr);
extern u32 g_MpChallengeIndex;
extern struct mpconfigfull *g_MpCurrentChallengeConfig;
extern struct challenge g_MpChallenges[30];
s32 ai0176(void);
u32 func0f19ab70(void);
void mpDetermineUnlockedFeatures(void);
void mpPerformSanityChecks(void);
char *mpGetChallengeNameBySlot(s32 slot);
s32 mpIsChallengeCompletedByAnyChrWithNumPlayersBySlot(s32 slot, s32 numplayers);
struct mpconfigfull *mpLoadConfig(s32 confignum, u8 *buffer, s32 len);
s32 mpForceUnlockFeature(s32 featurenum, u8 *array, s32 tail, s32 len);
s32 mpForceUnlockSetupFeatures(struct mpsetup *mpsetup, u8 *array, s32 len);
void mpForceUnlockConfigFeatures(struct mpconfig *config, u8 *array, s32 len, s32 challengeindex);
void mpForceUnlockSimulantFeatures(void);
void func0f19c1cc(void);
char *mpGetCurrentChallengeDescription(void);
char *mpconfigGetDescription(struct mpconfigfull *mpconfig);
s32 mpIsChallengeCompleteForEndscreen(void);
s32 aiMpInitSimulants(void);
void func0f19ab40(void);
s32 mpIsChallengeAvailableToPlayer(s32 chrnum, s32 challengeindex);
s32 mpIsChallengeAvailableToAnyPlayer(s32 challengeindex);
s32 mpGetNumAvailableChallenges(void);
char *mpChallengeGetName(s32 challengeindex);
void mpSetCurrentChallenge(s32 slotnum);
s32 mpGetCurrentChallengeIndex(void);
struct mpconfigfull *mpLoadChallenge(s32 challengeindex, u8 *buffer, s32 len);
struct mpconfigfull *mpGetNthAvailableChallengeSomething(s32 n, u8 *buffer, s32 len);
struct mpconfigfull *mpLoadCurrentChallenge(u8 *buffer, s32 len);
void func0f19c190(void);
s32 mpRemoveLock(void);
void mpLoadAndStoreCurrentChallenge(u8 *buffer, s32 len);
void mpClearCurrentChallenge(void);
s32 mpIsChallengeLoaded(void);
s32 mpGetAutoFocusedChallengeIndex(s32 mpchrnum);
char *mpChallengeGetNameWithArg(s32 playernum, s32 challengeindex);
s32 mpIsChallengeCompletedByPlayerWithNumPlayers2(s32 mpchrnum, s32 index, s32 numplayers);
s32 mpIsChallengeCompletedByAnyPlayerWithNumPlayers(s32 index, s32 numplayers);
void mpSetChallengeCompletedByAnyPlayerWithNumPlayers(s32 index, s32 numplayers, s32 completed);
s32 mpIsChallengeCompletedByPlayerWithNumPlayers(s32 mpchrnum, s32 index, s32 numplayers);
void mpSetChallengeCompletedByPlayerWithNumPlayers(u32 mpchrnum, s32 index, s32 numplayers, s32 completed);
void mpConsiderMarkingCurrentChallengeComplete(void);
s32 mpIsFeatureUnlocked(s32 feature);
void func0f1a7560(void *ptr, u16 fileid, u32 arg2, void *arg3, s32 arg4, s32 arg5);
void modelPromoteTypeToPointer(struct modelfiledata *filedata);
void *func0f1a7794(u16 fileid, u8 *arg1, s32 arg2, s32 arg3);
void *fileLoad(u16 fileid);
void func0f1a78b0(void);
u32 savefileHasFlag(u32 value);
void savefileSetFlag(u32 value);
void savefileUnsetFlag(u32 value);
void savefilePrintFlags(void);
void savefileApplyOptions(struct savefile_solo *file);
void savefileLoadDefaults(struct savefile_solo *file);
s32 func0f10fac8(s32 arg0);
s32 func0f10feac(s32 arg0, s32 arg1, u16 arg2);
void savefileGetOverview(char *arg0, char *name, u8 *stage, u8 *difficulty, u32 *time);
s32 func0f1106c8(void);
u32 func0f110720(void);
u32 func0f11080c(void);
void func0f1109c0(void);
void func0f110b68(void);
void func0f110bf0(void);
void func0f110bf8(void);
void func0f110c5c(s32 listnum, u8 filetype);
u8 func0f110cf8(u8 arg0);
u32 func0f110d90(void);
void func0f110da8(void);
u32 func0f110f4c(void);
struct textureconfig *func0f111460(s32 playernum, s32 arg1, s32 arg2);
void gfxInitMemory(void);
Gfx *gfxGetMasterDisplayList(void);
struct gfxvtx *gfxAllocateVertices(s32 count);
void *gfxAllocateMatrix(void);
void *gfxAllocate4Words(s32 count);
void *gfxAllocateColours(s32 count);
void *gfxAllocate(u32 size);
void gfxSwapBuffers(void);
u32 func0f0d5a90(void);
u32 func0f0d5c60(void);
Gfx *healthbarRender(Gfx *gdl, u32 arg1, u32 arg2, f32 arg3);
u8 hudmsgsAreActive(void);
s32 hudmsgIsZoomRangeVisible(void);
Gfx *hudmsgRenderMissionTimer(Gfx *gdl, u32 arg1);
Gfx *hudmsgRenderZoomRange(Gfx *gdl, s32 arg1);
Gfx *hudmsgRenderBox(Gfx *gdl, s32 x1, s32 y1, s32 x2, s32 y2, f32 bgopacity, u32 bordercolour, f32 textopacity);
s32 func0f0ddb1c(s32 *arg0, s32 arg1);
void hudmsgsHideByChannel(s32 value);
void hudmsgsInit(void);
void hudmsgRemoveAll(void);
s32 hudmsgGetNext(s32 refid);
void hudmsgCreate(char *text, s32 type);
void hudmsgCreateWithFlags(char *text, s32 type, u32 flags);
void hudmsgCreateWithColour(char *text, s32 type, u8 colour);
void hudmsgCreateWithDuration(char *text, s32 type, struct hudmsgtype *config, s32 duration60);
u32 func0f0de160(void);
void hudmsgCalculatePosition(struct hudmessage *msg);
void hudmsgCreateFromArgs(char *text, s32 type, s32 conf00, s32 conf01, s32 conf02,
  struct hudmessagething *conf04, struct hudmessagething *conf08,
  u32 textcolour, u32 shadowcolour,
  u32 alignh, s32 conf16, u32 alignv, s32 conf18, s32 arg14, u32 flags);
void hudmsgsTick(void);
void currentPlayerSetHudmsgsOn(u32 reason);
void currentPlayerSetHudmsgsOff(u32 reason);
void hudmsgRemoveForDeadPlayer(s32 playernum);
Gfx *hudmsgsRender(Gfx *gdl);
void hudmsgsReset(void);
void invInitGunsHeld(void);
void invInit(s32 numdoubles);
void invClear(void);
void invSortItem(struct invitem *item);
void invInsertItem(struct invitem *item);
void invRemoveItem(struct invitem *item);
struct invitem *invFindUnusedSlot(void);
void invSetAllGuns(s32 enable);
struct invitem *invFindSingleWeapon(s32 weaponnum);
s32 invHasSingleWeaponExcAllGuns(s32 weaponnum);
struct invitem *invFindDoubleWeapon(s32 weapon1, s32 weapon2);
s32 invHasDoubleWeaponExcAllGuns(s32 weapon1, s32 weapon2);
s32 invHasSingleWeaponOrProp(s32 weaponnum);
s32 invAddOneIfCantHaveSlayer(s32 arg0);
s32 currentStageForbidsSlayer(void);
s32 invCanHaveAllGunsWeapon(s32 weaponnum);
s32 invHasSingleWeaponIncAllGuns(s32 weaponnum);
s32 invHasDoubleWeaponIncAllGuns(s32 weapon1, s32 weapon2);
s32 invGiveSingleWeapon(s32 weaponnum);
s32 invGiveDoubleWeapon(s32 weapon1, s32 weapon2);
void invRemoveItemByNum(s32 weaponnum);
s32 invGiveProp(struct prop *prop);
void invRemoveProp(struct prop *prop);
s32 invGiveWeaponsByProp(struct prop *prop);
void invChooseCycleForwardWeapon(s32 *weaponnum1, s32 *weaponnum2, s32 arg2);
void invChooseCycleBackWeapon(s32 *weaponnum1, s32 *weaponnum2, s32 arg2);
s32 invHasKeyFlags(u32 wantkeyflags);
s32 invHasBriefcase(void);
s32 invHasDataUplink(void);
s32 invHasProp(struct prop *prop);
s32 invGetCount(void);
struct invitem *invGetItemByIndex(s32 index);
struct textoverride *invGetTextOverrideForObj(struct defaultobj *obj);
struct textoverride *invGetTextOverrideForWeapon(s32 weaponnum);
s32 invGetWeaponNumByIndex(s32 index);
u16 invGetNameIdByIndex(s32 index);
char *invGetNameByIndex(s32 index);
char *invGetShortNameByIndex(s32 index);
void invInsertTextOverride(struct textoverride *override);
u32 invGetCurrentIndex(void);
void invSetCurrentIndex(u32 item);
void invCalculateCurrentIndex(void);
char *invGetActivatedTextByObj(struct defaultobj *obj);
char *invGetActivatedTextByWeaponNum(s32 weaponnum);
void invIncrementHeldTime(s32 param_1, s32 param_2);
void invGetWeaponOfChoice(s32 *weapon1, s32 *weapon2);
extern struct inventory_menupos invmenupos_00010fd0;
extern struct inventory_class invclass_default;
extern struct weapon *g_Weapons[WEAPON_SUICIDEPILL + 1];
u32 langGetLangBankIndexFromStagenum(s32 stagenum);
u32 func0f16e3fc(void);
s32 langGetFileNumOffset(void);
s32 langGetFileId(s32 bank);
void langSetBankSimple(s32 bank);
void langSetBank(s32 bank, u8 *arg1, s32 arg2);
void langClearBank(s32 bank);
char *langGet(s32 textid);
u32 getVar80084040(void);
void lv0f167af8(void);
void lvStopAllMiscSfx(void);
s32 lvGetMiscSfxIndex(u32 arg0);
void lvSetMiscSfxState(u32 type, s32 play);
void lvUpdateMiscSfx(void);
void lvInit(s32 stagenum);
Gfx *lvRenderFade(Gfx *gdl);
void lvCancelFade(void);
s32 lvCheckCmpFollowThreat(struct threat *threat, s32 index);
void lvFindThreatsForProp(struct prop *prop, s32 inchild, struct coord *playerpos, s32 *activeslots, f32 *param_5);
void func0f168f24(struct prop *prop, s32 inchild, struct coord *playerpos, s32 *activeslots, f32 *distances);
void lvFindThreats(void);
Gfx *lvRender(Gfx *gdl);
void lvUpdateSoloHandicaps(void);
s32 sub54321(s32 value);
void lvUpdateCutsceneTime(void);
s32 lvGetSlowMotionType(void);
void lvTick(void);
void lvRecordDistanceMoved(void);
void lvCheckPauseStateChanged(void);
void lvSetPaused(s32 paused);
void lvConfigureFade(u32 color, s16 num_frames);
s32 lvIsFadeActive(void);
void lvReset(void);
s32 lvIsPaused(void);
s32 lvGetDifficulty(void);
void lvSetDifficulty(s32 difficulty);
void lvSetMpTimeLimit60(u32 limit);
void lvSetMpScoreLimit(u32 limit);
void lvSetMpTeamScoreLimit(u32 limit);
f32 lvGetStageTimeInSeconds(void);
s32 lvGetStageTime60(void);
u32 func0f0e5d2c(void);
s32 func0f0e5ef8(s16 arg0, struct menuitem *item);
u32 func0f0e6038(void);
Gfx *menuRenderOverlayList(Gfx *gdl, s16 x, s16 y, s16 x2, s16 y2);
Gfx *menuRenderItemList(Gfx *gdl, struct menurendercontext *context);
s32 menuTickItemList(struct menuitem *item, struct menuinputs *inputs, u32 arg2, union menuitemdata *data);
void menuInitItemDropdown(struct menuitem *item, union menuitemdata *data);
Gfx *menuRenderItemDropdown(Gfx *gdl);
s32 menuTickItemDropdown(struct menuitem *item, struct menuframe *frame, struct menuinputs *inputs, u32 arg3, union menuitemdata *data);
Gfx *menuRenderOverlayDropdown(Gfx *gdl, s16 x, s16 y, s16 x2, s16 y2, struct menuitem *item, struct menuframe *frame, union menuitemdata *data);
s32 menuIsStringEmptyOrSpaces(char *text);
Gfx *menuRenderItemKeyboard(Gfx *gdl, struct menurendercontext *thing);
s32 menuTickItemKeyboard(struct menuitem *item, struct menuinputs *inputs, u32 arg2, union menuitemdata *data);
void menuInitItemKeyboard(struct menuitem *item, union menuitemdata *data);
Gfx *menuRenderItemSeparator(Gfx *gdl, struct menurendercontext *context);
Gfx *menuRenderObjective(Gfx *gfx, struct menuframe *frame, s32 index, s32 position, s32 x, s32 y, s32 width, s32 height, s32 arg8, s32 arg9);
Gfx *menuRenderItemObjectives(Gfx *gdl, struct menurendercontext *context);
Gfx *menuRenderItemModel(Gfx *gdl, struct menurendercontext *context);
Gfx *menuRenderItemLabel(Gfx *gdl, struct menurendercontext *context);
Gfx *menuRenderItemMeter(Gfx *gdl, struct menurendercontext *context);
Gfx *menuRenderItemSelectable(Gfx *gdl, struct menurendercontext *context);
s32 menuTickItemSelectable(struct menuitem *item, struct menuinputs *inputs, u32 arg2);
Gfx *menuRenderItemSlider(Gfx *gdl);
s32 menuTickItemSlider(struct menuitem *item, struct menuframe *frame, struct menuinputs *inputs, u32 arg3, union menuitemdata *data);
void menuInitItemSlider(union menuitemdata *data);
Gfx *menuRenderItemCarousel(Gfx *gdl, struct menurendercontext *context);
s32 menuTickItemCarousel(struct menuitem *item, struct menuinputs *inputs, u32 arg2);
Gfx *menuRenderItemCheckbox(Gfx *gdl, struct menurendercontext *context);
s32 menuTickItemCheckbox(struct menuitem *item, struct menuinputs *inputs, u32 arg2);
char *menuItemScrollableGetText(u32 type);
Gfx *menuRenderItemScrollable(Gfx *gdl, struct menurendercontext *context);
s32 menuTickItemScrollable(struct menuitem *item, struct menuframe *frame, struct menuinputs *inputs, u32 arg3, union menuitemdata *data);
void menuInitItemScrollable(union menuitemdata *data);
Gfx *menuRenderItemMarquee(Gfx *gdl, struct menurendercontext *context);
s32 menuTickItemMarquee(struct menuitem *item, union menuitemdata *data);
void menuInitItemMarquee(union menuitemdata *data);
Gfx *menuRenderItem07(Gfx *gdl);
Gfx *menuRenderItemRanking(Gfx *gdl, struct menurendercontext *context);
s32 menuTickItemRanking(struct menuinputs *inputs, u32 arg1, union menuitemdata *data);
void menuInitItemRanking(union menuitemdata *data);
Gfx *menuRenderItemPlayerStats(Gfx *gdl, struct menurendercontext *context);
s32 menuTickItemPlayerStats(struct menuitem *item, struct menuframe *frame, struct menuinputs *inputs, u32 arg3, union menuitemdata *data);
Gfx *menuRenderOverlayPlayerStats(Gfx *gdl, s16 x, s16 y, s16 x2, s16 y2, struct menuitem *item, struct menuframe *frame, union menuitemdata *data);
void menuInitItemPlayerStats(struct menuitem *item, union menuitemdata *data);
u32 func0f0ef394(void);
Gfx *menuRenderControllerTexture(Gfx *gdl, s32 x, s32 y, s32 texturenum, u32 alpha);
Gfx *menuRenderControllerLines(Gfx *gdl, struct menurendercontext *context, s32 arg2, s32 arg3, s32 x, s32 y, u32 alpha);
u16 menuControllerGetButtonAction(s32 mode, s32 buttonnum);
Gfx *menuRenderControllerText(Gfx *gdl, s32 curmode, struct menurendercontext *context, s32 x, s32 y, u32 valuecolour, u32 labelcolour, s8 prevmode);
Gfx *menuRenderControllerInfo(Gfx *gdl, struct menurendercontext *context, s32 x, s32 y, s32 curmode, u32 alpha, u32 colour1, u32 colour2, s8 prevmode);
Gfx *menuRenderItemController(Gfx *gdl, struct menurendercontext *context);
void menuInitItemController(union menuitemdata *data);
Gfx *menuRenderItem(Gfx *gdl, struct menurendercontext *context);
s32 menuTickItem(struct menuitem *item, struct menuframe *frame, struct menuinputs *inputs, u32 arg3, union menuitemdata *data);
void menuInitItem(struct menuitem *item, union menuitemdata *data);
Gfx *menuRenderOverlay(Gfx *gdl, s16 x, s16 y, s16 x2, s16 y2, struct menuitem *item, struct menuframe *frame, union menuitemdata *data);
s32 mpStatsForPlayerDropdownHandler(s32 operation, struct menuitem *item, union handlerdata *data);
char *mpMenuTextInGameLimit(struct menuitem *item);
char *menutextPauseOrUnpause(s32 arg0);
char *menutextMatchTime(s32 arg0);
char *mpMenuTextWeaponDescription(struct menuitem *item);
char *mpMenuTitleStatsFor(struct menudialog *dialog);
char *mpMenuTextWeaponOfChoiceName(struct menuitem *item);
char *mpMenuTextAward1(struct menuitem *item);
char *mpMenuTextAward2(struct menuitem *item);
char *mpMenuTextPlacementWithSuffix(struct menuitem *item);
s32 mpPlacementMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 mpAwardsMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 mpPlayerTitleMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
char *mpMenuTextPlayerTitle(s32 arg0);
s32 menuhandler00178bf4(s32 operation, struct menuitem *item, union handlerdata *data);
void mpPushPauseDialog(void);
void mpPushEndscreenDialog(u32 arg0, u32 playernum);
s32 menuhandlerMpEndGame(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandler00178018(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpInGameLimitLabel(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpPause(s32 operation, struct menuitem *item, union handlerdata *data);
f32 mpHandicapToDamageScale(u8 value);
void func0f187838(struct mpchr *mpchr);
void mpStartMatch(void);
void mpInit(void);
void mpCalculateTeamIsOnlyAi(void);
void func0f187fbc(s32 playernum);
void func0f187fec(void);
void mpPlayerSetDefaults(s32 playernum, s32 autonames);
void func0f1881d4(s32 index);
void mpSetDefaultSetup(void);
void mpSetDefaultNamesIfEmpty(void);
s32 mpCalculateTeamScoreLimit(void);
void mpApplyLimits(void);
s32 mpGetPlayerRankings(struct mpteaminfo *info);
u32 func0f188930(void);
s32 mpGetTeamRankings(struct mpteaminfo *info);
s32 func0f188bcc(void);
s32 mpGetNumWeaponOptions(void);
char *mpGetWeaponLabel(s32 weaponnum);
void mpSetWeaponSlot(s32 slot, s32 mpweaponnum);
s32 mpGetWeaponSlot(s32 slot);
struct mpweapon *func0f188e24(s32 arg0);
s32 mpCountWeaponSetThing(s32 weaponsetindex);
s32 func0f188f9c(s32 arg0);
s32 func0f189058(s32 arg0);
s32 func0f189088(void);
char *mpGetWeaponSetName(s32 arg0);
u32 func0f18913c(void);
u32 func0f1892dc(void);
void mpSetWeaponSet(s32 weaponsetnum);
void func0f1895e8(void);
s32 mpGetWeaponSet(void);
s32 mpIsPaused(void);
void mpSetPaused(u8 mode);
Gfx *mpRenderModalText(Gfx *gdl);
u32 func0f189cc0(void);
u32 func0f189dc8(void);
u32 func0f189ed0(void);
u32 func0f18a030(void);
void mpCalculatePlayerTitle(struct mpplayer *mpplayer);
void func0f18a56c(void);
void mpEndMatch(void);
s32 func0f18bb1c(void);
s32 mpGetNumHeads(void);
s32 mpGetHeadId(u8 headnum);
s32 mpGetHeadRequiredFeature(u8 headnum);
s32 mpGetBeauHeadId(u8 headnum);
u32 mpGetNumBodies(void);
s32 mpGetBodyId(u8 bodynum);
s32 mpGetMpbodynumByBodynum(u16 bodynum);
char *mpGetBodyName(u8 mpbodynum);
u8 mpGetBodyRequiredFeature(u8 bodynum);
s32 mpGetMpheadnumByMpbodynum(s32 bodynum);
u32 mpChooseRandomLockPlayer(void);
s32 mpSetLock(s32 locktype, s32 playernum);
s32 mpGetLockType(void);
u32 mpGetLockPlayerNum(void);
s32 mpIsPlayerLockedOut(u32 playernum);
void mpCalculateLockIfLastWinnerOrLoser(void);
s32 mpIsTrackUnlocked(s32 tracknum);
s32 mpGetTrackSlotIndex(s32 tracknum);
s32 mpGetTrackNumAtSlotIndex(s32 slotindex);
s32 mpGetNumUnlockedTracks(void);
s32 mpGetTrackMusicNum(s32 slotindex);
char *mpGetTrackName(s32 slotindex);
void mpSetUsingMultipleTunes(s32 enable);
s32 mpGetUsingMultipleTunes(void);
s32 mpIsMultiTrackSlotEnabled(s32 slot);
void mpSetMultiTrackSlotEnabled(s32 slot, s32 enable);
void mpSetTrackSlotEnabled(s32 slot);
void mpEnableAllMultiTracks(void);
void mpDisableAllMultiTracks(void);
void mpRandomiseMultiTracks(void);
void mpSetTrackToRandom(void);
s32 mpGetCurrentTrackSlotNum(void);
u32 func0f18c4c0(void);
struct mpchr *func0f18c794(s32 index);
u32 func0f18c828(void);
s32 mpGetNumChrs(void);
u32 func0f18c8b8(void);
void func0f18c984(s32 numsims, u8 difficulty);
u32 func0f18cb60(void);
s32 mpGetNumSimulants(void);
void mpRemoveSimulant(s32 index);
s32 mpHasSimulants(void);
u32 func0f18cc8c(void);
s32 mpIsSimSlotEnabled(s32 slot);
s32 mpGetSimTypeIndex(s32 type, s32 difficulty);
void func0f18cddc(void);
s32 mpPlayerGetIndex(struct chrdata *chr);
struct chrdata *mpGetChrFromPlayerIndex(s32 index);
s32 func0f18d074(s32 index);
s32 func0f18d0e8(s32 arg0);
u32 func0f18d1b8(void);
u32 func0f18d238(void);
u32 func0f18d2b8(void);
u32 func0f18d5c4(void);
void func0f18d9a4(char *arg0, char *buffer, u32 *arg2);
s32 func0f18d9fc(void *arg0, s32 arg1, s32 arg2, u16 arg3);
s32 func0f18dac0(void *arg0, s32 arg1, s32 arg2, u16 arg3);
s32 mpGetNumPresets(void);
s32 mpIsPresetUnlocked(s32 presetnum);
s32 mpGetNumUnlockedPresets(void);
char *mpGetPresetNameBySlot(s32 slot);
void func0f18dcec(struct mpconfigfull *mpconfig);
u32 func0f18dec4(void);
u32 func0f18df5c(void);
u32 func0f18e16c(void);
void func0f18e39c(char *arg0, char *filename, u16 *numsims, u16 *stagenum, u16 *scenarionum);
s32 func0f18e420(s32 arg0, s32 arg1, u16 arg2);
s32 func0f18e4c8(s32 arg0, s32 arg1, u16 arg2);
void func0f18e558(void);
struct modelfiledata *func0f18e57c(s32 index, s32 *headnum);
extern struct menudialog g_MpScenarioMenuDialog;
extern struct menudialog g_MpQuickTeamScenarioMenuDialog;
void scenarioHtbInit(void);
s32 scenarioHtbCallback08(void);
void scenarioHtbReset(void);
void scenarioHtbTick(void);
void scenarioHtbCallback14(struct chrdata *chr);
void scenarioHtbKill(struct mpchr *mpchr, s32 mpchrnum, s32 *score, s32 *arg3);
Gfx *scenarioHtbRadar(Gfx *gdl);
s32 scenarioHtbRadar2(Gfx **gdl, struct prop *prop);
s32 scenarioHtbHighlight(struct prop *prop, u32 *colour);
void scenarioCtcInit(void);
s32 scenarioCtcCallback08(void);
void scenarioCtcTick(void);
void scenarioCtcCallback14(struct chrdata *chr);
void scenarioCtcKill(struct mpchr *mpchr, s32 mpchrnum, s32 *score, s32 *arg3);
Gfx *scenarioCtcRadar(Gfx *gdl);
s32 scenarioCtcRadar2(Gfx **gdl, struct prop *prop);
s32 scenarioCtcChooseSpawnLocation(f32 arg0, struct coord *pos, s16 *rooms, struct prop *prop, f32 *arg4);
s32 scenarioCtcGetMaxTeams(void);
s32 scenarioCtcIsRoomHighlighted(s16 room);
s32 menuhandlerMpHillTime(s32 operation, struct menuitem *item, union handlerdata *data);
void scenarioKohCallback40(s32 *arg0);
void scenarioKohCallback44(s32 *arg0);
void scenarioKohInit(void);
void scenarioKohReset(void);
void scenarioKohKill(struct mpchr *mpchr, s32 mpchrnum, s32 *score, s32 *arg3);
Gfx *scenarioKohRadar(Gfx *gdl);
s32 scenarioKohIsRoomHighlighted(s16 arg0);
void scenarioHtmInit(void);
s32 scenarioHtmCallback08(void);
void scenarioHtmTick(void);
void scenarioHtmKill(struct mpchr *mpchr, s32 mpchrnum, s32 *score, s32 *arg3);
s32 scenarioHtmRadar2(Gfx **gdl, struct prop *prop);
s32 scenarioHtmHighlight(struct prop *prop, u32 *colour);
void scenarioPacInit(void);
void scenarioPacReset(void);
void scenarioPacKill(struct mpchr *mpchr, s32 mpchrnum, s32 *score, s32 *arg3);
Gfx *scenarioPacRadar(Gfx *gdl);
s32 menuhandlerMpOpenOptions(s32 operation, struct menuitem *item, union handlerdata *data);
void scenarioCallback40(s32 *arg0);
void scenarioCallback44(s32 *arg0);
void scenarioInit(void);
s32 scenarioCallback08(void);
void scenarioReset(void);
void scenarioTick(void);
void scenarioCallback14(struct chrdata *chr);
Gfx *scenarioRadar(Gfx *gdl);
s32 scenarioRadar2(Gfx **gdl, struct prop *prop);
f32 scenarioChooseSpawnLocation(f32 arg0, struct coord *pos, s16 *rooms, struct prop *prop);
s32 scenarioGetMaxTeams(void);
void scenarioCallback38(s16 arg0, s32 *arg1, s32 *arg2, s32 *arg3);
s32 menuhandlerMpOpenOptions(s32 operation, struct menuitem *item, union handlerdata *data);
void scenarioCallback40(s32 *arg0);
void scenarioCallback44(s32 *arg0);
void scenarioInit(void);
s32 scenarioCallback08(void);
void scenarioReset(void);
void scenarioTick(void);
void scenarioCallback14(struct chrdata *chr);
Gfx *scenarioRadar(Gfx *gdl);
s32 scenarioRadar2(Gfx **gdl, struct prop *prop);
f32 scenarioChooseSpawnLocation(f32 arg0, struct coord *pos, s16 *rooms, struct prop *prop);
s32 scenarioGetMaxTeams(void);
void scenarioCallback38(s16 arg0, s32 *arg1, s32 *arg2, s32 *arg3);
char *mpGetCurrentPlayerName(struct menuitem *item);
s16 mpChooseRandomStage(void);
s32 menuhandler001791c8(s32 operation, struct menuitem *item, union handlerdata *data);
char *mpMenuTextWeaponNameForSlot(struct menuitem *item);
char *mpMenuTextSetupName(struct menuitem *item);
s32 func0f179da4(s32 operation, struct menuitem *item, union handlerdata *data, s32 mpheadnum, s32 mpbodynum, s32 arg5);
s32 menudialog0017a174(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 mpChallengesListHandler(s32 operation, struct menuitem *item, union handlerdata *data);
char *mpMenuTextKills(struct menuitem *item);
char *mpMenuTextDeaths(struct menuitem *item);
char *mpMenuTextGamesPlayed(struct menuitem *item);
char *mpMenuTextGamesWon(struct menuitem *item);
char *mpMenuTextGamesLost(struct menuitem *item);
char *mpMenuTextHeadShots(struct menuitem *item);
char *mpMenuTextMedalAccuracy(struct menuitem *item);
char *mpMenuTextMedalHeadShot(struct menuitem *item);
char *mpMenuTextMedalKillMaster(struct menuitem *item);
char *mpMenuTextMedalSurvivor(struct menuitem *item);
char *mpMenuTextAmmoUsed(struct menuitem *item);
char *mpMenuTextDistance(struct menuitem *item);
u32 mpMenuTextTime(void);
char *mpMenuTextAccuracy(struct menuitem *item);
void mpFormatDamageValue(char *dst, f32 damage);
char *mpMenuTextPainReceived(struct menuitem *item);
char *mpMenuTextDamageDealt(struct menuitem *item);
s32 menuhandlerMpMedal(s32 operation, struct menuitem *item, union handlerdata *data);
char *mpMenuTitleStatsForPlayerName(struct menudialog *dialog);
char *mpMenuTextUsernamePassword(struct menuitem *item);
s32 func0f17b4f8(s32 operation, struct menuitem *item, union handlerdata *data, s32 mpheadnum, s32 arg4);
char *mpMenuTextBodyName(struct menuitem *item);
void func0f17b8f0(void);
s32 menuhandler0017b91c(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandler0017bab4(s32 operation, struct menuitem *item, union handlerdata *data);
char *mpMenuTextMpconfigMarquee(struct menuitem *item);
s32 menuhandler0017bfc0(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpHandicapPlayer(s32 operation, struct menuitem *item, union handlerdata *data);
char *mpMenuTextHandicapPlayerName(struct menuitem *item);
s32 menuhandler0017c6a4(s32 operation, struct menuitem *item, union handlerdata *data);
char *mpMenuTextSimulantDescription(struct menuitem *item);
s32 menudialog0017ccfc(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 menuhandlerMpSimulantDifficulty(s32 operation, struct menuitem *item, union handlerdata *data);
char *mpMenuTitleEditSimulant(struct menudialog *dialog);
char *mpMenuTextSimulantName(struct menuitem *item);
s32 menuhandlerMpNTeams(s32 operation, struct menuitem *item, union handlerdata *data, s32 numteams);
char *mpMenuTextChrNameForTeamSetup(struct menuitem *item);
s32 func0f17dac4(s32 operation, struct menuitem *item, union handlerdata *data);
char *mpMenuTextSelectTuneOrTunes(struct menuitem *item);
s32 mpSelectTuneListHandler(s32 operation, struct menuitem *item, union handlerdata *data);
char *mpMenuTextCurrentTrack(struct menuitem *item);
s32 menuhandler0017e06c(s32 operation, struct menuitem *item, union handlerdata *data);
char *mpMenuTextTeamName(struct menuitem *item);
char *func0f17e318(struct menudialog *dialog);
s32 menudialog0017e3fc(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 menuhandler0017e4d4(s32 operation, struct menuitem *item, union handlerdata *data);
char *mpMenuTextChallengeName(struct menuitem *item);
s32 mpCombatChallengesMenuDialog(s32 operation, struct menudialog *dialog, union handlerdata *data);
char *mpMenuTextSavePlayerOrCopy(struct menuitem *item);
char *mpMenuTextArenaName(struct menuitem *item);
char *mpMenuTextWeaponSetName(struct menuitem *item);
void mpConfigureQuickTeamPlayers(void);
void mpConfigureQuickTeamSimulants(void);
void func0f17f428(void);
s32 menuhandlerPlayerTeam(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpNumberOfSimulants(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpSimulantsPerTeam(s32 operation, struct menuitem *item, union handlerdata *data);
s32 mpQuickTeamSimulantDifficultyHandler(s32 operation, struct menuitem *item, union handlerdata *data);
u32 func0f17fa28(void);
void func0f17fcb0(s32 silent);
s32 menuhandlerMpSlowMotion(s32 operation, struct menuitem *item, union handlerdata *data);
void mpHtbAddPad(s16 padnum);
u32 func0f17ffe4(void);
void func0f180078(void);
void func0f1800a8(void);
u32 scenarioHtbCallback18(void);
void scenarioCtcReset(void);
s32 scenarioCtcHighlight(struct prop *prop, u32 *colour);
void mpCtcAddPad(s32 *cmd);
void scenarioCtcCallback38(s16 arg0, s32 *arg1, s32 *arg2, s32 *arg3);
void scenarioKohTick(void);
u32 scenarioKohCallback18(void);
void mpKohAddHill(s32 *cmd);
void scenarioKohCallback38(s16 arg0, s32 *arg1, s32 *arg2, s32 *arg3);
void mpHtmAddPad(s16 padnum);
void func0f182bf4(void);
void func0f182c98(void);
void scenarioHtmReset(void);
void scenarioHtmCallback14(struct chrdata *chr);
u32 scenarioHtmCallback18(void);
Gfx *scenarioHtmRadar(Gfx *gdl);
void scenarioPacChooseVictims(void);
s32 scenarioPacHighlight(struct prop *prop, u32 *colour);
void func0f1845bc(void);
u32 func0f1847b8(void);
void scenarioPacTick(void);
u32 scenarioPacCallback18(void);
s32 scenarioPacRadar2(Gfx **gdl, struct prop *prop);
s32 mpOptionsMenuDialog(s32 operation, struct menudialog *dialog, union handlerdata *data);
char *mpMenuTextScenarioShortName(struct menuitem *item);
char *mpMenuTextScenarioName(struct menuitem *item);
s32 menuhandler00185068(s32 operation, struct menuitem *item, union handlerdata *data);
void mpCreateMatchStartHudmsgs(void);
Gfx *func0f185774(Gfx *gdl);
u32 func0f185c14(void);
s32 func0f185e20(struct prop *prop, s32 *arg1);
void mpPrepareScenario(void);
u32 func0f186508(void);
void mpCreateScenarioHudmsg(s32 playernum, char *message);
s32 mpChrsAreSameTeam(s32 arg0, s32 arg1);
u32 chrGiveBriefcase(struct chrdata *chr, struct prop *prop);
void func0f187288(struct chrdata *chr, struct prop *prop);
s32 chrGiveUplink(struct chrdata *chr, struct prop *prop);
void scenarioHtmActivateUplink(struct chrdata *chr, struct prop *prop);
s32 menuhandlerMpDropOut(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpTeamsLabel(s32 operation, struct menuitem *item, union handlerdata *data);
s32 mpGetNumStages(void);
s32 menuhandlerMpControlStyle(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpWeaponSlot(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpWeaponSetDropdown(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpControlCheckbox(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpAimControl(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpCheckboxOption(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpTeamsEnabled(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpDisplayOptionCheckbox(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpConfirmSaveChr(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpPlayerName(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpSaveSetupOverwrite(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpSaveSetupCopy(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpCharacterBody(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpUsernamePassword(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpCharacterHead(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpTimeLimitSlider(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpScoreLimitSlider(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpTeamScoreLimitSlider(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpRestoreScoreDefaults(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpRestoreHandicapDefaults(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menudialogMpReady(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 menudialogMpSimulant(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 menuhandlerMpSimulantHead(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpSimulantBody(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpDeleteSimulant(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpChangeSimulantType(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpClearAllSimulants(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpAddSimulant(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpSimulantSlot(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menudialogMpSimulants(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 menuhandlerMpTwoTeams(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpThreeTeams(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpFourTeams(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpMaximumTeams(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpHumansVsSimulants(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpHumanSimulantPairs(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpTeamSlot(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menudialogMpSelectTune(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 menuhandlerMpMultipleTunes(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpTeamNameSlot(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandler0017e38c(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandler0017e9d8(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpAbortChallenge(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpStartChallenge(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandler0017ec64(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpLock(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpSavePlayer(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandler0017ef30(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpSaveSettings(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menudialogMpGameSetup(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 menudialogMpQuickGo(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 menuhandlerMpFinishedSetup(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerQuickTeamSeparator(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpQuickTeamOption(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menudialogCombatSimulator(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 menuhandlerMpAdvancedSetup(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpDisplayTeam(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerMpOneHitKills(s32 operation, struct menuitem *item, union handlerdata *data);
void mpstatsIncrementPlayerShotCount(struct gset *gset, s32 region);
void mpstatsIncrementPlayerShotCount2(struct gset *gset, s32 region);
void mpstats0f0b0520(void);
s32 mpstatsGetPlayerShotCountByRegion(u32 type);
void mpstatsIncrementTotalKillCount(void);
void mpstatsIncrementTotalKnockoutCount(void);
void mpstatsDecrementTotalKnockoutCount(void);
u8 mpstatsGetTotalKnockoutCount(void);
u32 mpstatsGetTotalKillCount(void);
void mpstatsRecordPlayerKill(void);
s32 mpstatsGetPlayerKillCount(void);
void mpstatsRecordPlayerDeath(void);
void mpstatsRecordPlayerSuicide(void);
void mpstatsRecordDeath(s32 aplayernum, s32 vplayernum);
u16 musicGetVolume(void);
void musicSetVolume(u16 volume);
s32 func0f16d0a8(s32 tracktype, s32 arg1);
s32 func0f16d124(s32 tracktype);
void musicStart(u32 tracktype, u32 tracknum, f32 arg2, u16 volume);
void musicEnd(s32 tracktype);
void func0f16d2ac(s32 tracktype, f32 arg1, s32 arg2);
void func0f16d324(void);
void func0f16d3d0(void);
void func0f16d430(void);
void func0f16d44c(void);
void musicStartPrimary(f32 arg0);
void musicStartAmbient(f32 arg0);
s32 musicIsAnyPlayerInAmbientRoom(void);
void musicStartX(f32 arg0);
void musicStartMenu2(s32 tracknum);
void musicSetStageAndStartMusic(s32 stagenum);
void musicSetStage(s32 stagenum);
void musicReset(void);
void func0f16da2c(void);
void func0f16daa4(void);
void musicStartForMenu(void);
void musicResumeAfterUnpause(void);
void musicStartSoloDeath(void);
void musicStartMpDeath(f32 arg0);
void func0f16dd14(void);
void func0f16ddb0(void);
void musicPlayTrackIsolated(s32 tracknum);
void musicPlayDefaultTracks(void);
void musicStartTemporaryPrimary(s32 tracknum);
void musicStartCutscene(s32 tracknum);
void musicEndCutscene(void);
void musicStartTemporary(s32 tracknum);
void musicEndAmbient(void);
void musicSetXReason(s32 index, u32 arg1, u32 duration);
void musicUnsetXReason(s32 reason);
void func0f16e138(void);
u32 func0f006c80(void);
Gfx *func0f008558(Gfx *gdl, s32 arg1);
void nbombReset(struct nbomb *nbomb);
s32 nbombCalculateAlpha(struct nbomb *nbomb);
Gfx *nbombCreateGdl(void);
Gfx *nbombRender(Gfx *gdl, struct nbomb *nbomb, Gfx *subgdl);
void func0f0099a4(void);
void nbombInflictDamage(struct nbomb *nbomb);
void nbombTick(struct nbomb *nbomb);
void nbombsTick(void);
Gfx *nbombsRender(Gfx *gdl);
void nbombCreate(struct coord *pos, struct prop *prop);
f32 gasGetDoorFrac(s32 tagnum);
Gfx *func0f00a490(Gfx *gdl);
Gfx *gasRender(Gfx *gdl);
void objectivesAutocomplete(void);
void objectivesReset(void);
void tagInsert(struct tag *tag);
void briefingInsert(struct briefingobj *obj);
void objectiveInsert(struct objective *objective);
void objectiveAddRoomEnteredCriteria(struct criteria_roomentered *criteria);
void objectiveAddMultiroomEnteredCriteria(struct criteria_multiroomentered *criteria);
void objectiveAddHolographCriteria(struct criteria_holograph *criteria);
u32 xorBaffbeff(u32 value);
u32 xorBabeffff(u32 value);
u32 xorBoobless(u32 value);
void tagsAllocatePtrs(void);
s32 objGetTagNum(struct defaultobj *obj);
s32 objectiveGetCount(void);
u32 objectiveGetDifficultyBits(s32 index);
s32 objectiveCheck(s32 index);
s32 objectiveIsAllComplete(void);
void objectivesDisableChecking(void);
void objectivesShowHudmsg(char *buffer, s32 hudmsgtype);
void objectivesCheckAll(void);
void objectiveCheckRoomEntered(s32 currentroom);
void objectiveCheckMultiroomEntered(s32 arg0, s16 *requiredrooms);
void objectiveCheckHolograph(f32 sqdist);
struct prop *chopperGetTargetProp(struct chopperobj *heli);
struct defaultobj *objFindByTagId(s32 tag_id);
struct tag *tagFindById(s32 tag_id);
s32 optionsGetControlMode(s32 mpchrnum);
void optionsSetControlMode(s32 mpchrnum, s32 mode);
s32 optionsGetContpadNum1(s32 mpchrnum);
s32 optionsGetContpadNum2(s32 mpchrnum);
s32 optionsGetForwardPitch(s32 mpchrnum);
s32 optionsGetAutoAim(s32 mpchrnum);
s32 optionsGetLookAhead(s32 mpchrnum);
s32 optionsGetAimControl(s32 mpchrnum);
s32 optionsGetSightOnScreen(s32 mpchrnum);
s32 optionsGetAmmoOnScreen(s32 mpchrnum);
s32 optionsGetShowGunFunction(s32 mpchrnum);
s32 optionsGetAlwaysShowTarget(s32 mpchrnum);
s32 optionsGetShowZoomRange(s32 mpchrnum);
s32 optionsGetPaintball(s32 mpchrnum);
s32 optionsGetShowMissionTime(s32 mpchrnum);
u8 optionsGetInGameSubtitles(void);
u8 optionsGetCutsceneSubtitles(void);
s32 optionsGetHeadRoll(s32 mpchrnum);
void optionsSetForwardPitch(s32 mpchrnum, s32 enable);
void optionsSetAutoAim(s32 mpchrnum, s32 enable);
void optionsSetLookAhead(s32 mpchrnum, s32 enable);
void optionsSetAimControl(s32 mpchrnum, s32 index);
void optionsSetSightOnScreen(s32 mpchrnum, s32 enable);
void optionsSetAmmoOnScreen(s32 mpchrnum, s32 enable);
void optionsSetShowGunFunction(s32 mpchrnum, s32 enable);
void optionsSetAlwaysShowTarget(s32 mpchrnum, s32 enable);
void optionsSetShowZoomRange(s32 mpchrnum, s32 enable);
void optionsSetPaintball(s32 mpchrnum, s32 enable);
void optionsSetShowMissionTime(s32 mpchrnum, s32 enable);
void optionsSetInGameSubtitles(s32 enable);
void optionsSetCutsceneSubtitles(s32 enable);
void optionsSetHeadRoll(s32 mpchrnum, s32 enable);
s32 optionsGetEffectiveScreenSize(void);
s32 optionsGetScreenSize(void);
void optionsSetScreenSize(s32 size);
s32 optionsGetScreenRatio(void);
void optionsSetScreenRatio(s32 ratio);
u8 optionsGetScreenSplit(void);
void optionsSetScreenSplit(u8 split);
u16 optionsGetMusicVolume(void);
void optionsSetMusicVolume(u16 volume);
void padUnpack(s32 padnum, u32 fields, struct pad *pad);
s32 padHasBboxData(s32 padnum);
void padGetCentre(s32 padnum, struct coord *coord);
void func0f116068(s32 padnum);
void padCopyBboxFromPad(s32 padnum, struct pad *src);
void padSetFlag(s32 padnum, u32 flag);
void padUnsetFlag(s32 padnum, u32 flag);
s32 coverGetCount(void);
s32 coverUnpack(s32 covernum, struct cover *cover);
s32 coverIsInUse(s32 covernum);
void coverSetInUse(s32 covernum, s32 enable);
void coverSetFlag(s32 covernum, u32 flag);
void coverUnsetFlag(s32 covernum, u32 flag);
void coverSetFlag0001(s32 covernum, s32 enable);
s32 coverIsSpecial(struct cover *cover);
void waypointSetHashThing(s32 hash1, s32 hash2);
struct waypoint *waypointFindClosestToPos(struct coord *pos, s16 *rooms);
struct waygroup *func0f114810(s32 *groupnums, s32 value, u32 mask);
void func0f114958(s32 *groupnums, s32 value, u32 mask);
s32 func0f1149b0(struct waygroup *group, s32 arg1, u32 mask);
s32 func0f114a2c(struct waygroup *from, struct waygroup *to, struct waygroup *groups, s32 arg3, u32 mask);
s32 func0f114aec(struct waygroup *from, struct waygroup *to, struct waygroup *groups);
struct waypoint *func0f114b7c(s32 *pointnums, s32 arg1, s32 groupnum, u32 mask);
void func0f114ccc(s32 *pointnums, s32 value, s32 groupnum, u32 mask);
s32 func0f114d34(s32 *pointnums, s32 arg1, s32 groupnum, u32 mask);
void func0f114de0(struct waypoint *from, struct waypoint *to, s32 arg2, u32 mask);
void func0f114ee4(struct waypoint *from, struct waypoint *to);
s32 func0f114f70(struct waypoint *from, struct waypoint *to, struct waypoint **arr, s32 arrlen);
void func0f11505c(struct waygroup *arg0, struct waygroup *arg1, struct waypoint **arg2, struct waypoint **arg3);
s32 waypointFindRoute(struct waypoint *from, struct waypoint *to, struct waypoint **arr, s32 arrlen);
void func0f115390(void);
struct waypoint *func0f1153c4(s32 *pointnums, s32 arg1);
struct waygroup *func0f1154cc(s32 *groupnums, s32 arg1);
struct waypoint *func0f1155e0(struct waypoint *pointa, struct waypoint *pointb);
void waypointDisableSegmentInDirection(struct waypoint *a, struct waypoint *b);
void waypointEnableSegmentInDirection(struct waypoint *a, struct waypoint *b);
void waypointDisableSegment(struct waypoint *a, struct waypoint *b);
void waypointEnableSegment(struct waypoint *a, struct waypoint *b);
u32 func0f1165d0(s8 device);
u32 func0f1165f8(s8 device, u32 arg1);
u32 func0f116658(s8 arg0, u32 arg1);
u32 func0f116684(u32 arg0);
u32 func0f11668c(s8 device);
s32 pakIsConnected(s8 device);
s32 func0f1167b0(s8 device, u32 arg1, u32 *buffer1024);
u32 func0f1167d8(s8 arg0);
s32 func0f116800(s8 arg0, s32 arg1, void *arg2, s32 arg3);
s32 func0f116828(s8 arg0, s32 arg1, s32 arg2, void *arg3, s32 *arg4, s32 arg5);
s32 filemgrDeleteFile(s8 arg0, s32 arg1);
s32 pakDeleteGameNote(s8 device, u16 company_code, u32 game_code, char *game_name, char *ext_name);
s32 func0f1168c4(s8 device, struct pakdata **arg1);
u32 func0f116914(s8 device);
s32 func0f11693c(s8 device);
void func0f116994(void);
void func0f1169c8(s8 device, s32 arg1);
s32 func0f116aec(s8 device);
s32 func0f116b5c(s8 device);
void func0f116bdc(s8 device, u8 *arg1, u8 *arg2);
u16 func0f116c54(s8 device);
u32 func0f116c94(s8 device);
void func0f116db0(s8 device, s32 value);
s32 func0f116df0(s8 device, struct pakdata **arg1);
s32 func0f116e84(s8 device, u16 company_code, u32 game_code, char *game_name, char *ext_name);
s32 func0f116fa0(s8 device, s32 arg1);
s32 func0f11702c(s8 device);
u32 func0f11706c(s8 device);
s32 func0f117094(s8 arg0, u32 arg1);
void func0f117150(s8 device, u8 *ptr);
void func0f1171b4(s8 device, s32 arg1, s32 arg2);
s32 func0f117210(s8 device);
u32 func0f117430(void);
u32 func0f117520(s8 arg0, u32 arg1, u32 *arg2);
void pakDumpBuffer(u8 *buffer, u32 len, char *name);
void pakDumpEeprom(void);
s32 func0f11789c(s8 arg0, s32 arg1, s32 arg2, void *arg3, s32 *arg4, s32 arg5);
s32 pakInitPak(OSMesgQueue *mq, OSPfs *pfs, s32 channel);
s32 pakReadWriteFile(OSPfs *pfs, s32 file_no, u8 flag, u32 address, u32 len, u8 *buffer);
s32 func0f117c0c(s32 arg0, s32 *arg1, s32 *arg2);
s32 pakFreeBlocks(OSPfs *pfs, s32 *bytes_not_used);
s32 pakFileState(OSPfs *pfs, s32 file_no, OSPfsState *note);
s32 pakAllocateFile(OSPfs *pfs, u16 company_code, u32 game_code, char *game_name, char *ext_name, s32 size, s32 *file_no);
u32 pakDeleteFile(OSPfs *pfs, u16 company_code, u32 game_code, char *game_name, char *ext_name);
s32 pakFindFile(OSPfs *pfs, u16 company_code, u32 game_code, char *game_name, char *ext_name, s32 *file_no);
s32 func0f117ec0(OSPfs *pfs, u16 company_code, u32 game_code, u8 *game_name, u8 *ext_name, u32 numbytes);
s32 func0f117f80(s8 device);
s32 func0f117fc0(s8 device);
s32 func0f118000(s8 device);
s32 func0f11807c(s8 device);
s32 func0f118148(s8 device);
s32 func0f118230(s8 device, s32 arg1);
s32 func0f118334(s8 device, s32 numpages);
void func0f1185e0(s8 device, s32 arg1, s32 param_3);
u32 func0f118674(s8 arg0, u32 arg1, u32 arg2);
void func0f1189d8(void);
void func0f118ae4(u8 *arg0, u8 *arg1, u16 *arg2);
s32 func0f118bc8(s8 arg0, s32 arg1, void *arg2, s32 arg3);
s32 func0f118d18(s8 device, u32 arg1, u32 *buffer1024);
s32 func0f1190bc(s8 device, s32 arg1, s32 *arg2);
u32 func0f119298(s8 index);
void func0f119340(u32 arg0);
u32 func0f119368(s32 device, u32 arg1, struct pakthing16 *pakthing16);
u32 func0f1194e0(s8 arg0, u32 *arg1, u32 arg2);
u32 func0f11970c(void);
void pakCorrupt(void);
u32 func0f119e8c(void);
u32 func0f11a0e8(void);
u32 func0f11a1d0(void);
void func0f11a2e4(void);
void func0f11a32c(s8 device, u8 arg1, u32 line, char *file);
void func0f11a434(s8 arg0);
s32 func0f11a504(s8 device, OSPfs *pfs, s32 file_no, u8 flag, u32 address, u32 len, u8 *buffer);
s32 func0f11a574(s8 device);
u32 func0f11a7dc(void);
u32 func0f11a8f4(void);
u32 func0f11ac7c(void);
void pakWipeEeprom(s32 arg0, s32 arg1, s32 arg2);
u32 func0f11b178(void);
u32 func0f11b488(void);
void pakForceScrub(s32 arg0);
u32 func0f11b75c(void);
u32 func0f11b86c(void);
u32 func0f11bbd8(void);
u32 func0f11bc54(s8 device, u32 arg1, u32 arg2, u32 arg3, u32 arg4, u32 arg5, u32 arg6, u32 arg7, u32 arg8);
s32 pakRepair(s32 arg0);
u32 func0f11c39c(u32 arg0, s8 device, u32 ar2, u32 arg3);
void func0f11c54c(void);
void func0f11c6d0(void);
void pakExecuteDebugOperations(void);
void func0f11ca30(void);
void func0f11cb9c(u32 arg0);
s32 func0f11cbd8(s8 device, s32 arg1, char *arg2, u16 arg3);
s32 func0f11cc6c(s8 device, u16 arg1, char *arg2, u16 arg3);
s32 func0f11cd00(s8 arg0, s32 arg1, char *arg2, s32 arg3, s32 arg4);
s32 func0f11ce00(s8 arg0, s32 arg1, char *arg2, s32 arg3, s32 arg4);
u32 func0f11d118(void);
u32 func0f11d174(void);
u32 func0f11d214(void);
u32 func0f11d3f8(void);
u32 func0f11d478(void);
u32 func0f11d4dc(void);
s32 func0f11d540(s8 index, s32 arg1);
s32 func0f11d5b0(s8 index);
void func0f11d620(s8 device);
u32 func0f11d8b4(void);
u32 func0f11d9c4(void);
void func0f11dc04(s32 arg0, f32 arg1, s32 arg2, s32 arg3);
void func0f11dcb0(s32 arg0);
void func0f11dd58(s8 playernum);
void func0f11de20(s8 device);
void func0f11deb8(void);
void func0f11df38(void);
void pakDumpPak(void);
void func0f11df94(s8 device);
void func0f11e3bc(s8 device);
void pakProbeEeprom(void);
s32 pakReadEeprom(u8 address, u8 *buffer, u32 len);
s32 pakWriteEeprom(u8 address, u8 *buffer, u32 len);
void pakSetBitflag(u32 flagnum, u8 *bitstream, s32 set);
s32 pakHasBitflag(u32 flagnum, u8 *stream);
void pakClearAllBitflags(u8 *flags);
void func0f11e618(char *src, char *dst, u32 len);
s8 pakSearch(s32 arg0);
s32 func0f11e750(s8 arg0);
s32 func0f11e78c(void);
s32 func0f11e7f0(char *a, char *b);
s32 func0f11e844(s8 device);
s32 func0f11ea34(s8 arg0);
extern const char var7f1b423c[];
extern const char var7f1b4244[];
extern const char var7f1b424c[];
extern const char var7f1b4254[];
extern const char var7f1b425c[];
extern f32 var80069880;
void propsSort(void);
void propEnable(struct prop *prop);
void propDisable(struct prop *prop);
struct prop *propAllocate(void);
void propFree(struct prop *prop);
void propActivate(struct prop *prop);
void propActivateThisFrame(struct prop *prop);
void propDelist(struct prop *prop);
void propReparent(struct prop *mover, struct prop *adopter);
void propDetach(struct prop *prop);
Gfx *propRender(Gfx *gdl, struct prop *prop, s32 withalpha);
Gfx *propsRender(Gfx *gdl, s16 arg1, s32 arg2, s16 *arg3);
void weaponPlayWhooshSound(s32 weaponnum, struct prop *prop);
void func0f060bac(s32 weaponnum, struct prop *prop);
struct prop *shotCalculateHits(s32 handnum, s32 arg1, struct coord *arg2, struct coord *arg3, struct coord *gunpos, struct coord *dir, u32 arg6, f32 arg7, s32 arg8);
struct prop *func0f061d54(s32 handnum, u32 arg1, u32 arg2);
void handCreateBulletRaycast(s32 handnum, s32 arg1, s32 dorandom, s32 arg3, s32 arg4);
void func0f061fa8(struct shotdata *shotdata, struct prop *prop, f32 arg2, s32 arg3, struct modelnode *node, struct hitthing *hitthing, s32 arg6, struct modelnode *arg7, struct model *model, s32 arg9, s32 arg10, struct coord *arg11, struct coord *arg12);
void handInflictCloseRangeDamage(s32 handnum, struct gset *gset, s32 arg2);
void handTickAttack(s32 handnum);
void handsTickAttack(void);
void propExecuteTickOperation(struct prop *prop, s32 op);
struct prop *propFindForInteract(s32 eyespy);
void propFindForUplink(void);
s32 currentPlayerInteract(s32 eyespy);
void propPause(struct prop *prop);
void propUnpause(struct prop *prop);
void propsTick(u32 islastplayer);
void propsTickPadEffects(void);
void propSetCollisionsEnabled(struct prop *prop, s32 enable);
void func0f0641f4(void);
f32 func0f06438c(struct prop *prop, struct coord *arg1, f32 *arg2, f32 *arg3, f32 *arg4, s32 arg5, s32 cangangsta, s32 arg7);
void farsightChooseTarget(void);
void autoaimTick(void);
u32 propDoorGetCdTypes(struct prop *prop);
s32 propIsOfCdType(struct prop *prop, u32 types);
void roomsCopy(s16 *srcrooms, s16 *dstrooms);
void roomsAppend(s16 *newrooms, s16 *dstrooms, s32 maxlen);
s32 arrayIntersects(s16 *a, s16 *b);
s32 propTryAddToChunk(s16 propnum, s32 chunkindex);
s32 roomAllocatePropListChunk(s32 room, s32 arg1);
void propRegisterRoom(struct prop *prop, s16 room);
void propDeregisterRoom(struct prop *prop, s16 room);
void propDeregisterRooms(struct prop *prop);
void propRegisterRooms(struct prop *prop);
void func0f065d1c(struct coord *pos, s16 *rooms, struct coord *newpos, s16 *newrooms, s16 *morerooms, u32 arg5);
void func0f065dd8(struct coord *pos, s16 *rooms, struct coord *newpos, s16 *newrooms);
void func0f065dfc(struct coord *pos, s16 *rooms, struct coord *newpos, s16 *newrooms, s16 *morerooms, u32 arg5);
void func0f065e74(struct coord *pos, s16 *rooms, struct coord *newpos, s16 *newrooms);
void func0f065e98(struct coord *pos, s16 *rooms, struct coord *pos2, s16 *rooms2);
void roomGetProps(s16 *room, s16 *propnums, s32 len);
void propsDefragRoomProps(void);
void propGetBbox(struct prop *prop, f32 *width, f32 *ymax, f32 *ymin);
s32 propUpdateGeometry(struct prop *prop, u8 **start, u8 **end);
extern u32 var800698f0;
extern u32 var800698f4;
extern u32 var80069914;
extern u32 var80069930;
extern s32 var80069934;
extern f32 g_CameraWaitMultiplier;
extern f32 var8006994c;
extern f32 var80069950;
extern f32 var80069954;
extern f32 var80069958;
extern struct beam *g_ThrownLaptopBeams;
void alarmActivate(void);
void alarmDeactivate(void);
s32 alarmIsActive(void);
s32 audioPlayFromProp(s32 channelnum, s16 soundnum, s32 arg2, struct prop *prop, s16 arg4, u16 arg5);
f32 countdownTimerGetValue60(void);
s32 countdownTimerIsVisible(void);
s32 countdownTimerIsRunning(void);
void countdownTimerSetRunning(s32 running);
void countdownTimerSetValue60(f32 value);
void countdownTimerSetVisible(u32 reason, s32 visible);
void countdownTimerTick(void);
s32 doorCallLift(struct prop *doorprop, s32 allowclose);
s32 doorIsPadlockFree(struct doorobj *door);
s32 objPassesSafePickupChecks(struct defaultobj *obj);
void objUpdateLinkedScenery(struct defaultobj *obj);
u32 func0f0667ac(void);
u32 func0f0667b4(void);
f32 func0f0667bc(struct modelrodata_bbox *bbox);
u32 func0f0667c4(void);
u32 func0f0667cc(void);
u32 func0f0667d4(void);
u32 func0f0667dc(void);
u32 func0f06680c(void);
u32 func0f06683c(void);
u32 func0f06686c(void);
u32 func0f06689c(void);
f32 func0f0668cc(struct modelrodata_bbox *bbox, Mtxf *matrix);
u32 func0f0668fc(void);
u32 func0f06692c(void);
f32 func0f06695c(struct modelrodata_bbox *bbox, f32 *realrot);
u32 func0f06698c(void);
u32 func0f0669bc(void);
u32 func0f0669ec(void);
f32 func0f066a1c(struct modelrodata_bbox *bbox, f32 arg1, f32 arg2, f32 arg3);
f32 func0f066abc(struct modelrodata_bbox *bbox, f32 arg1, f32 arg2, f32 arg3);
u32 func0f066b5c(void);
u32 func0f067424(void);
u32 func0f0674bc(void);
s32 func0f0675c8(struct coord *coord, f32 arg1, struct modelrodata_bbox *arg2, Mtxf *arg3);
s32 func0f0677ac(struct coord *coord, f32 *arg1, struct coord *pos, struct coord *normal, struct coord *up, struct coord *look, f32 xmin, f32 xmax, f32 ymin, f32 ymax, f32 zmin, f32 zmax);
s32 func0f0678f8(struct coord *coord, f32 *arg1, s32 padnum);
s32 func0f06797c(struct coord *coord, f32 arg1, s32 padnum);
s32 func0f0679ac(struct model *model, f32 *distance, f32 *arg2, f32 *arg3, f32 *arg4);
void func0f067bc4(struct model *model, f32 *arg1, f32 *arg2, s32 arg5);
void func0f067d88(struct model *model, f32 *arg1, f32 *arg2, f32 *arg3, f32 *arg4);
s32 func0f067dc4(struct model *model, f32 *arg1, f32 *arg2, f32 *arg3, f32 *arg4);
s32 modelSetRedBox(struct model *model, f32 *arg1, f32 *arg2, f32 *arg3, f32 *arg4);
void func0f06803c(struct coord *arg0, f32 *arg1, f32 *arg2, f32 *arg3, f32 *arg4);
struct defaultobj *objFindByPadNum(s32 padnum);
u32 func0f068218(void);
void projectileFree(struct projectile *projectile);
void projectilesUnrefOwner(struct prop *owner);
void projectileReset(struct projectile *projectile);
struct projectile *projectileGetNew(void);
void func0f0685e4(struct prop *prop);
void objSetProjectileFlag4(struct prop *prop);
void projectileSetFlag1(struct projectile *projectile);
struct monitorthing *monitorthingGetNew(void);
s32 objGetDestroyedThird(struct defaultobj *obj);
s32 objGetDestroyedValue(struct defaultobj *obj);
struct modelnode *func0f0687e4(struct model *model);
struct modelnode *modelFileDataFindBboxNode(struct modelfiledata *filedata);
struct modelrodata_bbox *modelFileDataFindBboxRodata(struct modelfiledata *filedata);
struct modelnode *modelFindBboxNode(struct model *model);
struct modelrodata_bbox *modelFindBboxRodata(struct model *model);
u32 func0f068ad4(void);
struct modelrodata_bbox *objFindBboxRodata(struct defaultobj *obj);
u32 func0f068b14(void);
u32 func0f068c04(void);
s32 func0f068fc8(struct prop *prop, s32 arg1);
void func0f069144(struct prop *prop, u8 *nextcol, u16 floorcol);
void func0f069630(struct prop *prop, u8 *nextcol, u16 floorcol);
void colourTween(u8 *col, u8 *nextcol);
void func0f069750(s32 *arg0, s32 arg1, f32 *arg2);
void func0f069850(struct defaultobj *obj, struct coord *pos, f32 *realrot, struct tiletype3 *geo);
void func0f069b4c(struct defaultobj *obj);
void func0f069c1c(struct defaultobj *obj);
void func0f069c70(struct defaultobj *obj, s32 arg1, s32 arg2);
void objInitToggleNodes(struct defaultobj *obj);
u32 func0f069d38(void);
u32 func0f06a170(void);
struct prop *objInitialise(struct defaultobj *obj, struct modelfiledata *filedata, struct prop *prop, struct model *model);
struct prop *func0f06a52c(struct defaultobj *obj, struct modelfiledata *modelfiledata);
struct prop *func0f06a550(struct defaultobj *obj);
void func0f06a580(struct defaultobj *obj, struct coord *pos, Mtxf *matrix, s16 *rooms);
f32 func0f06a620(struct defaultobj *obj);
void func0f06a730(struct defaultobj *obj, struct pad *newpad, Mtxf *matrix, s16 *arg3, struct pad *arg4);
u32 func0f06ab60(void);
void objEndFlight(struct defaultobj *obj);
void func0f06ac90(struct prop *prop);
void objRemove2(struct defaultobj *obj, s32 freeprop, s32 regen);
void objRemove(struct defaultobj *obj, s32 freeprop);
u32 func0f06b36c(void);
s32 func0f06b39c(struct coord *arg0, struct coord *arg1, struct coord *arg2, f32 arg3);
u32 func0f06b488(void);
u32 func0f06b610(void);
u32 func0f06be44(void);
s32 func0f06bea0(struct model *model, struct modelnode *arg1, struct modelnode *arg2, struct coord *arg3, struct coord *arg4, void *arg5, void *arg6, void *arg7, void *arg8, void *arg9, void *arg10);
u32 func0f06c28c(void);
u32 func0f06c8ac(void);
u32 func0f06cd00(void);
u32 func0f06d37c(void);
void applySpeed(f32 *distdone, f32 maxdist, f32 *speed, f32 accel, f32 decel, f32 maxspeed);
void applyRotation(f32 *angle, f32 maxrot, f32 *speed, f32 accel, f32 decel, f32 maxspeed);
u32 func0f06dbd8(void);
u32 func0f06e87c(void);
u32 func0f06e9cc(void);
u32 func0f06eb4c(void);
u32 func0f06ec20(void);
u32 func0f06ed64(void);
u32 func0f06ef44(void);
u32 func0f06f0a0(void);
void func0f06f314(struct prop *prop, u32 explosiontype);
void func0f06f504(struct prop *ammocrate);
void func0f06f54c(struct prop *weapon);
void func0f07063c(struct prop *prop, s32 arg1);
void func0f070698(struct prop *prop, s32 arg1);
void func0f0706f8(struct prop *prop, s32 arg1);
void func0f07079c(struct prop *prop, s32 arg1);
s32 glassCalculateOpacity(struct coord *pos, f32 xludist, f32 opadist, f32 arg3);
u32 func0f070a1c(void);
u32 func0f070bd0(void);
void func0f070ca0(struct defaultobj *obj, void *ptr, s32 arg2, s32 arg3, union modelrodata *rodata);
void liftActivate(struct prop *prop, u8 liftnum);
struct prop *liftFindByPad(s16 padnum);
f32 liftGetY(struct liftobj *lift);
void func0f070f08(struct liftobj *lift, s32 arg0);
void liftGoToStop(struct liftobj *lift, s32 stopnum);
f32 objGetHov04(struct defaultobj *obj);
void hovUpdateGround(struct defaultobj *obj, struct hov *hov, struct coord *pos, s16 *rooms, f32 *matrix);
void func0f0714b8(struct defaultobj *obj, struct hov *hov);
s32 objIsHoverpropOrBike(struct defaultobj *obj);
f32 hoverpropGetTurnAngle(struct defaultobj *obj);
void hoverpropSetTurnAngle(struct defaultobj *obj, f32 angle);
u32 func0f072144(void);
void hovercarFindNextPath(struct hovercarobj *hovercar);
void hovercarStartNextPath(struct hovercarobj *hovercar);
void hovercarIncrementStep(struct hovercarobj *hovercar);
u32 func0f0727d4(void);
void func0f072adc(struct hoverbikeobj *hoverbike, f32 arg1, f32 arg2, f32 arg3);
u32 func0f0732d4(void);
u32 func0f073478(void);
u32 func0f073ae8(void);
s32 func0f073c6c(struct defaultobj *obj, s32 *arg1);
void doorTick(struct prop *doorprop);
void doorUpdatePortalIfWindowed(struct prop *door, s32 playercount);
u32 func0f077448(void);
void platformDisplaceProps(struct prop *platform, s16 *propnums, struct coord *prevpos, struct coord *newpos);
void liftTick(struct prop *lift);
void escastepTick(struct prop *escalator);
void cctvTick(struct prop *camera);
u32 func0f078930(void);
void fanTick(struct prop *fan);
void fanUpdateModel(struct prop *fan);
void autogunTick(struct prop *autogun);
u32 func0f079ca4(void);
void func0f079f1c(struct prop *autogun);
u32 func0f07accc(void);
struct chopperobj *chopperFromHovercar(struct chopperobj *obj);
s32 chopperCheckTargetInFov(struct chopperobj *hovercar, u8 param_2);
s32 chopperCheckTargetInSight(struct chopperobj *obj);
void chopperSetTarget(struct chopperobj *obj, u32 chrnum);
s32 chopperAttack(struct chopperobj *obj);
s32 chopperStop(struct chopperobj *obj);
s32 chopperSetArmed(struct chopperobj *obj, s32 armed);
void chopperRestartTimer(struct chopperobj *obj);
f32 chopperGetTimer(struct chopperobj *heli);
void chopperSetMaxDamage(struct chopperobj *obj, u16 health);
u32 func0f07b164(void);
void chopperFireRocket(struct chopperobj *chopper, s32 side);
u32 func0f07b3f0(void);
void func0f07ba38(struct prop *prop, f32 roty, f32 rotx, struct coord *vector, u32 arg4);
void chopperTickFall(struct prop *chopper);
void chopperTickIdle(struct prop *chopper);
void chopperTickPatrol(struct prop *chopper);
void chopperTickCombat(struct prop *chopper);
void hovercarTick(struct prop *hovercar);
void hoverpropTick(struct prop *hoverprop, s32 arg1);
void hoverbikeTick(struct prop *hoverbike, s32 arg1);
void dropshipUpdateInterior(struct prop *prop);
void glassUpdatePortal(struct prop *glass, s32 playercount, s32 *arg2);
u32 func0f07e184(void);
u32 func0f07e1fc(void);
void func0f07e2cc(struct prop *prop);
s32 propCanRegen(struct prop *prop);
u32 func0f07e474(struct prop *prop);
s32 objTick(struct prop *prop);
Gfx *propsRenderBeams(Gfx *gdl);
void tvscreenSetCmdlist(struct tvscreen *screen, u32 *cmdlist);
void tvscreenSetImageByNum(struct tvscreen *screen, s32 imagenum);
void tvscreenSetTexture(struct tvscreen *screen, s32 texturenum);
Gfx *tvscreenRender(struct model *model, struct modelnode *node, struct tvscreen *screen, Gfx *gdl, s32 arg4, s32 arg5);
void objRenderProp(struct prop *prop, struct modelrenderdata *renderdata, s32 withalpha);
Gfx *gfxRenderRadialShadow(Gfx *gdl, f32 x, f32 y, f32 z, f32 angle, f32 size, u32 alpha);
Gfx *objRenderShadow(struct defaultobj *obj, Gfx *gdl);
Gfx *objRender(struct prop *prop, Gfx *gdl, s32 withalpha);
s32 modelIsNodeNotTvscreen(struct modelfiledata *filedata, struct modelnode *node);
void func0f081ccc(struct chopperobj *chopper, s32 arg1);
u32 func0f0826cc(void);
void propobjSetDropped(struct prop *prop, u32 reason);
void func0f082a1c(struct defaultobj *obj, struct coord *coord, f32 rotate, s32 arg3, s32 arg4);
void piracyRestore(void);
void func0f082e84(struct defaultobj *obj, struct coord *arg1, struct coord *arg2, struct coord *arg3, s32 arg4);
void objDetach(struct prop *prop);
void func0f08307c(struct prop *prop, s32 arg1);
u32 func0f083db0(void);
u32 func0f0840ac(void);
void func0f0841dc(struct defaultobj *obj, struct coord *pos, s32 playernum);
s32 func0f084594(struct model *model, struct modelnode *node, struct coord *arg2, struct coord *arg3, struct hitthing *arg4, s32 *arg5, struct modelnode **arg6);
s32 func0f0849dc(struct model *model, struct modelnode *node, struct coord *arg2, struct coord *arg3, struct hitthing *arg4, s32 *arg5, struct modelnode **arg6);
u32 add43214321(u32 value);
void glassDestroy(struct defaultobj *obj);
void doorDestroyGlass(struct doorobj *door);
void func0f084f64(struct defaultobj *obj);
void func0f085050(struct prop *prop, f32 damage, struct coord *pos, s32 arg3, s32 playernum);
s32 func0f085194(struct defaultobj *obj);
s32 func0f0851ec(struct defaultobj *obj);
void objTakeGunfire(struct defaultobj *obj, f32 damage, struct coord *pos, s32 weaponnum, s32 playernum);
void objDamage(struct defaultobj *obj, f32 damage, struct coord *pos, s32 weaponnum, s32 playernum);
void func0f0859a0(struct prop *prop, struct shotdata *shotdata);
void func0f085e00(struct prop *prop, struct shotdata *shotdata);
void func0f085eac(struct shotdata *shotdata, struct hit *hit);
u32 propobjGetCiTagId(struct prop *prop);
s32 objIsHealthy(struct defaultobj *obj);
s32 objTestForInteract(struct prop *prop);
s32 currentPlayerTryMountHoverbike(struct prop *prop);
s32 propobjInteract(struct prop *prop);
void propObjSetOrUnsetHiddenFlag00400000(struct prop *prop, s32 enable);
s32 objUpdateGeometry(struct prop *prop, u8 **start, u8 **end);
void propObjGetBbox(struct prop *prop, f32 *width, f32 *ymax, f32 *ymin);
void ammotypeGetPickedUpText(char *dst);
void ammotypeGetDeterminer(char *dst, s32 ammotype, s32 qty);
void ammotypeGetPickupName(char *dst, s32 ammotype, s32 qty);
void ammotypePlayPickupSound(u32 ammotype);
s32 propPlayPickupSound(struct prop *prop, s32 weapon);
void weaponPlayPickupSound(s32 weaponnum);
void ammotypeGetPickupMessage(char *dst, s32 ammotype, s32 qty);
void currentPlayerQueuePickupAmmoHudmsg(s32 ammotype, s32 pickupqty);
void func0f088028(s32 ammotype, s32 quantity, s32 arg2, s32 showhudmsg);
s32 ammocrateGetPickupAmmoQty(struct ammocrateobj *crate);
s32 weaponGetPickupAmmoQty(struct weaponobj *weapon);
void weaponGetPickupText(char *buffer, s32 weaponnum, s32 dual);
void currentPlayerQueuePickupWeaponHudmsg(u32 weaponnum, s32 dual);
s32 propPickupByPlayer(struct prop *prop, s32 showhudmsg);
s32 objTestForPickup(struct prop *prop);
s32 func0f0899dc(struct prop *prop, struct coord *arg1, f32 *arg2, f32 *arg3);
void func0f089a94(s32 arg0, struct model *model);
struct prop *hatApplyToChr(struct hatobj *hat, struct chrdata *chr, struct modelfiledata *filedata, struct prop *prop, struct model *model);
void hatLoadAndApplyToChr(struct hatobj *hat, struct chrdata *chr);
void hatAssignToChr(struct hatobj *hat, struct chrdata *chr);
struct prop *hatCreateForChr(struct chrdata *chr, s32 modelnum, u32 flags);
struct weaponobj *func0f089f8c(s32 arg0, s32 arg1, struct modelfiledata *filedata);
struct hatobj *func0f08a38c(s32 arg0, s32 arg1, struct modelfiledata *filedata);
struct ammocrateobj *func0f08a724(void);
u32 func0f08a88c(void);
void playerActivateRemoteMineDetonator(s32 playernum);
struct weaponobj *func0f08aa70(s32 weaponnum, struct prop *prop);
struct weaponobj *weaponFindThrown(s32 weaponnum);
void weaponRegisterProxy(struct weaponobj *weapon);
void weaponUnregisterProxy(struct weaponobj *weapon);
void coordTriggerProxies(struct coord *pos, s32 arg1);
void chrsTriggerProxies(void);
void propweaponSetDual(struct weaponobj *weapon1, struct weaponobj *weapon2);
struct prop *func0f08adc8(struct weaponobj *weapon, struct modelfiledata *filedata, struct prop *prop, struct model *model);
struct prop *func0f08ae0c(struct weaponobj *weapon, struct modelfiledata *filedata);
s32 chrEquipWeapon(struct weaponobj *weapon, struct chrdata *chr);
struct prop *func0f08b108(struct weaponobj *weapon, struct chrdata *chr, struct modelfiledata *modelfiledata, struct prop *prop, struct model *model);
void func0f08b208(struct weaponobj *weapon, struct chrdata *chr);
void func0f08b25c(struct weaponobj *weapon, struct chrdata *chr);
struct autogunobj *laptopDeploy(s32 modelnum, struct gset *gset, struct chrdata *chr);
struct weaponobj *func0f08b658(s32 modelnum, struct gset *gset, struct chrdata *chr);
struct weaponobj *func0f08b880(s32 modelnum, s32 weaponnum, struct chrdata *chr);
void chrSetWeaponReapable(struct chrdata *chr, s32 hand);
struct prop *weaponCreateForChr(struct chrdata *chr, s32 modelnum, s32 weaponnum, u32 flags, struct weaponobj *obj, struct modelfiledata *filedata);
struct prop *chrGiveWeapon(struct chrdata *chr, s32 model, s32 weaponnum, u32 flags);
s32 weaponTestForPickup(struct prop *prop);
void weaponSetGunfireVisible(struct prop *prop, s32 visible, s16 room);
s32 weaponIsGunfireVisible(struct prop *prop);
s32 hatGetType(struct prop *prop);
s32 doorIsUnlocked(struct prop *playerprop, struct prop *doorprop);
s32 doorIsPosInRange(struct doorobj *door, struct coord *pos, f32 arg2, s32 isbike);
s32 doorIsObjInRange(struct doorobj *door, struct defaultobj *obj, s32 isbike);
s32 vectorIsInFrontOfDoor(struct doorobj *door, struct coord *vector);
s32 doorIsRangeEmpty(struct doorobj *door);
void doorsCheckAutomatic(void);
void func0f08c424(struct doorobj *door, Mtxf *matrix);
void doorGetBbox(struct doorobj *door, struct modelrodata_bbox *dst);
u32 func0f08c54c(struct doorobj *door);
void func0f08cb20(struct doorobj *door, struct gfxvtx *vertices1, struct gfxvtx *vertices2, s32 numvertices);
void func0f08d3dc(struct doorobj *door);
void func0f08d460(struct doorobj *door);
void doorActivatePortal(struct doorobj *door);
void doorDeactivatePortal(struct doorobj *door);
struct prop *func0f08d540(struct doorobj *door, struct coord *pos, f32 *matrix, s16 *rooms, struct coord *coord, struct coord *centre);
void func0f08d784(s32 soundtype, struct prop *prop);
void func0f08daa8(s32 soundtype, struct prop *prop);
void func0f08dd44(s32 soundtype, struct prop *prop);
void func0f08df10(s32 soundtype, struct prop *prop);
void doorPrepareForOpen(struct doorobj *door);
void doorPrepareForClose(struct doorobj *door);
u32 decodeXorAaaaaaaa(u32 value);
void func0f08e224(struct doorobj *door);
void func0f08e2ac(struct doorobj *door);
void doorSetMode(struct doorobj *door, s32 newmode);
void doorsRequestMode(struct doorobj *door, s32 newmode);
s32 doorIsClosed(struct doorobj *door);
s32 doorIsOpen(struct doorobj *door);
s32 func0f08e5a8(s16 *rooms, struct screenbox *box);
f32 func0f08e6bc(struct prop *prop, f32 arg1);
s32 func0f08e794(struct coord *coord, f32 arg1);
s32 func0f08e8ac(struct prop *prop, struct coord *coord, f32 arg2, s32 arg3);
s32 posIsInDrawDistance(struct coord *arg);
void func0f08ea50(struct doorobj *door);
s32 doorCalcIntendedFrac(struct doorobj *door);
void doorsCalcFrac(struct doorobj *door);
f32 func0f08f538(f32 x, f32 y);
u32 func0f08f604(void);
s32 func0f08f968(struct doorobj *door, s32 arg1);
s32 doorTestForInteract(struct prop *prop);
void doorsActivate(struct prop *prop, s32 allowliftclose);
s32 posIsInFrontOfDoor(struct coord *pos, struct doorobj *door);
void doorsChooseSwingDirection(struct prop *chrprop, struct doorobj *door);
s32 propdoorInteract(struct prop *doorprop);
void alarmStopAudio(void);
void gasReleaseFromPos(struct coord *pos);
void gasStopAudio(void);
s32 gasIsActive(void);
void gasTick(void);
Gfx *countdownTimerRender(Gfx *gdl);
void projectilesDebug(void);
void alarmTick(void);
void func0f091030(void);
void func0f0910ac(void);
void func0f091250(s32 weaponnum);
void projectileCreate(struct prop *fromprop, u32 arg1, struct coord *pos, struct coord *direction, s32 weaponnum, struct prop *targetprop);
void objSetModelPartVisible(struct defaultobj *obj, s32 partnum, s32 visible);
Gfx *radarRenderBackground(Gfx *gdl, struct textureconfig *tconfig, s32 arg2, s32 arg3, s32 arg4);
s32 radarGetTeamIndex(s32 team);
Gfx *radarDrawDot(Gfx *gdl, struct prop *prop, struct coord *dist, u32 colour1, u32 colour2, s32 swapcolours);
Gfx *radarRender(Gfx *gdl);
Gfx *radarRenderRTrackedProps(Gfx *gdl);
void shardsCreate(struct coord *pos, f32 *rotx, f32 *roty, f32 *rotz, f32 xmin, f32 xmax, f32 ymin, f32 ymax, s32 type, struct prop *prop);
void shardsInit(void);
Gfx *shardsRender(Gfx *gdl);
void shardsReset(void);
void shardsTick(void);
s32 sightIsPropFriendly(struct prop *prop);
s32 sightCanTargetProp(struct prop *prop, s32 max);
s32 sightIsReactiveToProp(struct prop *prop);
s32 sightFindFreeTargetIndex(s32 max);
void func0f0d7364(void);
void sightTick(s32 sighton);
s32 func0f0d789c(s32 arg0, s32 arg1, s32 arg2, s32 arg3);
Gfx *sightRenderTargetBox(Gfx *gdl, struct threat *threat, u32 textid, s32 time);
Gfx *func0f0d7f54(Gfx *gdl, s32 arg1, s32 arg2, s32 arg3, s32 arg4, s32 arg5);
Gfx *func0f0d87a8(Gfx *gdl, s32 arg1, s32 arg2, s32 arg3, s32 arg4, s32 arg5);
Gfx *sightRenderDefault(Gfx *gdl, s32 sighton);
Gfx *sightRenderClassic(Gfx *gdl, s32 sighton);
Gfx *sightRenderType2(Gfx *gdl, s32 sighton);
u32 func0f0d9948(void);
Gfx *sightRenderSkedar(Gfx *gdl, s32 sighton);
Gfx *sightRenderZoom(Gfx *gdl, s32 sighton);
Gfx *sightRenderMaian(Gfx *gdl, s32 sighton);
Gfx *sightRenderTarget(Gfx *gdl);
s32 sightHasTargetWhileAiming(s32 sight);
Gfx *sightRender(Gfx *gdl, s32 sighton, s32 sight);
void func0f135c70(void);
u32 func0f135f08(void);
void skyInit(void);
u32 func0f13687c(void);
void smokeInit(void);
void smokeReset(void);
Gfx *smokeRenderPart(struct smoke *smoke, struct smokepart *part, Gfx *gdl, struct coord *coord, f32 size);
struct smoke *smokeCreate(struct coord *pos, s16 *rooms, s16 type);
s32 func0f12e454(struct coord *pos, s16 *rooms, s16 type, u32 arg4);
s32 smokeCreateWithSource(void *source, struct coord *pos, s16 *rooms, s16 type, s32 srcispadeffect);
void smokeCreateAtProp(struct prop *prop, s16 type);
void smokeCreateAtPadEffect(struct padeffectobj *effect, struct coord *pos, s16 *rooms, s16 type);
void smokeClearForProp(struct prop *prop);
struct smoke *smokeCreateSimple(struct coord *pos, s16 *rooms, s16 type);
u32 func0f12e848(struct prop *prop);
u32 smokeTick(struct prop *prop);
Gfx *smokeRender(struct prop *prop, Gfx *gdl, s32 withalpha);
void smokeClearSomeTypes(void);
void sparksInit(void);
u32 func0f12f6c0(void);
void sparkgroupEnsureFreeSparkSlot(struct sparkgroup *group);
void sparksCreate(s32 room, struct prop *prop, struct coord *pos, struct coord *arg3, struct coord *dir, s32 type);
Gfx *sparksRender(Gfx *gdl);
void sparksTick(void);
void splatTick(struct prop *prop);
void func0f148e54(struct prop *prop, struct splat *arg1, struct coord *arg2, struct coord *arg3, s32 arg4, s32 arg5, struct chrdata *arg6);
s32 func0f148f18(s32 qty, f32 arg1, struct prop *prop, struct splat *arg3, struct coord *arg4, struct coord *arg5, s32 arg6, s32 arg7, s32 arg8, struct chrdata *arg9, s32 arg10);
s32 func0f149274(f32 arg0, struct prop *prop, struct splat *arg2, f32 arg3, s32 arg4, s32 arg5, s32 arg6, struct chrdata *arg7, s32 arg8);
void func0f149864(void);
u32 func0f14986c(void);
void chrInitSplats(struct chrdata *chr);
void func0f14a3bc(void);
void stub0f000840(void);
void stub0f000850(void);
void stub0f000860(void);
void stub0f000870(void);
void stub0f0008e0(void);
void stub0f0008f0(void);
void stub0f000900(void);
void stub0f000910(void);
void stub0f00b180(void);
void stub0f00b200(void);
void stub0f013540(void);
void stub0f015260(void);
void stub0f015270(void);
void stub0f0153f0(void);
void stub0f015400(void);
void stub0f015410(void);
void stub0f102230(void);
void stub0f175f50(void);
void stub0f175f58(s32 arg0, u32 addr, u32 numwords);
s32 stub0f175f68(s32 arg0, s32 arg1);
void stub0f175f78(s32 arg0, s32 arg1, s32 arg2);
void func0f16e810(u32 arg0);
u32 func0f16edac(void);
u32 func0f16ee58(void);
u32 func0f16eefc(void);
u32 func0f16efa0(void);
u32 func0f16f044(void);
u32 func0f16f0f4(void);
u32 func0f16fc8c(void);
u32 func0f16fd50(void);
u32 func0f16fde4(void);
u32 func0f1706ec(void);
u32 func0f1711b4(void);
u32 func0f171558(void);
u32 func0f171724(void);
s32 func0f171828(s32 arg0);
u32 func0f17184c(void);
u32 func0f1718a0(void);
u32 func0f171ba8(void);
u32 func0f17218c(void);
u32 func0f172554(void);
u32 func0f1729f8(void);
u32 func0f172b5c(void);
u32 func0f172e70(void);
u32 func0f172e8c(void);
u32 func0f172f44(void);
u32 func0f172f54(void);
void func0f172f5c(Gfx *gdl, s32 arg1, s32 arg2);
void func0f173010(u32 *texturenum, u32 arg1, u32 arg2);
u32 func0f173434(void);
void func0f1734e8(u16 arg0, void *arg1);
u32 func0f173520(void);
s32 func0f17353c(s32 arg0);
void texturesLoadConfigs(void);
void frametimeInit(void);
void frametimeApply(s32 diffframe60, s32 diffframe240, s32 frametime);
void frametimeCalculate(void);
void func0f16cf94(void);
extern const char var7f1b78c0[];
extern const char var7f1b78c8[];
extern const char var7f1b78d4[];
extern const char var7f1b78f4[];
extern const char var7f1b7900[];
extern const char var7f1b7908[];
extern const char var7f1b7910[];
extern u32 var800624d4;
extern u32 var800624d8;
extern u32 var800624dc;
char *mpPlayerGetWeaponOfChoiceName(u32 playernum, u32 slot);
u32 func0f01616c(void);
void titleInitLegal(void);
void titleTickLegal(void);
void titleInitCheckControllers(void);
void titleExitCheckControllers(void);
void titleTickCheckControllers(void);
Gfx *titleRenderCheckControllers(Gfx *gdl);
Gfx *titleRenderLegal(Gfx *gdl);
void titleInitPdLogo(void);
void titleExitPdLogo(void);
void titleTickPdLogo(void);
u32 func0f017248(void);
void func0f017980(void);
Gfx *titleRenderPdLogo(Gfx *gdl);
void titleInitRarePresents(void);
void titleExitRarePresents(void);
void titleTickRarePresents(void);
Gfx *func0f018ebc(Gfx *gdl, s32 *x, s32 *y, u32 textnum, s32 timer, s32 *arg5);
Gfx *titleRenderRarePresents(Gfx *gdl);
void titleInitNintendoLogo(void);
void titleExitNintendoLogo(void);
void titleTickNintendoLogo(void);
Gfx *titleRenderNintendoLogo(Gfx *gdl);
void titleInitRareLogo(void);
void titleExitRareLogo(void);
void titleTickRareLogo(void);
f32 func0f019d0c(f32 arg0);
Gfx *titleRenderRareLogo(Gfx *gdl);
void titleInitSkip(void);
void titleInitNoController(void);
void titleTickNoController(void);
Gfx *titleRenderNoController(Gfx *gdl);
void titleSetNextMode(s32 mode);
void titleTick(void);
s32 titleIsChangingMode(void);
s32 titleIsKeepingMode(void);
void titleExit(void);
void titleInitFromAiCmd(u32 arg0);
s32 func0f01ad5c(void);
void func0f01adb8(void);
void titleTickOld(void);
Gfx *titleRender(Gfx *gdl);
void titleExitLegal(void);
s32 getNumPlayers(void);
void setNumPlayers(s32 numplayers);
void titleExitNoController(void);
char *frMenuTextFailReason(struct menuitem *item);
char *frMenuTextDifficultyName(struct menuitem *item);
char *frMenuTextTimeTakenValue(struct menuitem *item);
char *frMenuTextScoreValue(struct menuitem *item);
char *frMenuTextWeaponName(struct menuitem *item);
char *frMenuTextTargetsDestroyedValue(struct menuitem *item);
char *frMenuTextAccuracyValue(struct menuitem *item);
char *frMenuTextGoalScoreLabel(struct menuitem *item);
char *frMenuTextGoalScoreValue(struct menuitem *item);
char *frMenuTextMinAccuracyOrTargetsLabel(struct menuitem *item);
char *frMenuTextMinAccuracyOrTargetsValue(struct menuitem *item);
char *frMenuTextTimeLimitLabel(struct menuitem *item);
char *frMenuTextTimeLimitValue(struct menuitem *item);
char *frMenuTextAmmoLimitLabel(struct menuitem *item);
char *frMenuTextAmmoLimitValue(struct menuitem *item);
char *ciMenuTextChrBioName(struct menuitem *item);
char *ciMenuTextChrBioAge(struct menuitem *item);
char *ciMenuTextChrBioRace(struct menuitem *item);
char *ciMenuTextMiscBioName(struct menuitem *item);
char *dtMenuTextName(struct menuitem *item);
char *dtMenuTextOkOrResume(struct menuitem *item);
char *dtMenuTextCancelOrAbort(struct menuitem *item);
char *dtMenuTextTimeTakenValue(struct menuitem *item);
char *htMenuTextName(struct menuitem *item);
char *htMenuTextOkOrResume(struct menuitem *item);
char *htMenuTextCancelOrAbort(struct menuitem *item);
char *htMenuTextTimeTakenValue(struct menuitem *item);
char *bioMenuTextName(struct menuitem *item);
char *ciMenuTextHangarBioSubheading(struct menuitem *item);
struct menudialog *ciGetFrWeaponListMenuDialog(void);
s32 frTrainingInfoMenuDialog(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 frTrainingStatsMenuDialog(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 ciCharacterProfileMenuDialog(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 dtTrainingDetailsMenuDialog(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 menudialogDeviceTrainingResults(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 menudialog001a6aa4(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 menudialogFiringRangeResults(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 ciHangarHolographMenuDialog(s32 operation, struct menudialog *dialog, union handlerdata *data);
s32 frDetailsOkMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 frAbortMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandler001a348c(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandler001a44c0(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerFrFailedContinue(s32 operation, struct menuitem *item, union handlerdata *data);
s32 ciOfficeInformationMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 dtDeviceListMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandlerDtOkOrResume(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandler001a6514(s32 operation, struct menuitem *item, union handlerdata *data);
s32 htHoloListMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandler001a6a34(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandler001a6a70(s32 operation, struct menuitem *item, union handlerdata *data);
s32 ciHangarInformationMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
s32 menuhandler001a6ea4(s32 operation, struct menuitem *item, union handlerdata *data);
s32 frDifficultyMenuHandler(s32 operation, struct menuitem *item, union handlerdata *data);
extern u8 g_FrIsValidWeapon;
extern s32 g_FrWeaponNum;
extern u8 g_ChrBioSlot;
extern u8 var80088bb4;
extern u8 g_HangarBioSlot;
extern u8 g_DtSlot;
s32 ciIsTourDone(void);
u8 ciGetFiringRangeScore(s32 weaponindex);
void frSaveScoreIfBest(s32 weaponindex, s32 difficulty);
u8 frIsWeaponFound(s32 weapon);
void frSetWeaponFound(s32 weaponnum);
s32 ciIsStageComplete(s32 stageindex);
s32 func0f19cbcc(s32 weapon);
s32 frIsWeaponAvailable(s32 weapon);
u32 frGetWeaponIndexByWeapon(u32 weaponnum);
u32 frGetWeaponScriptIndex(u32 weaponnum);
s32 frIsClassicWeaponUnlocked(u32 weapon);
s32 frGetSlot(void);
void frSetSlot(s32 slot);
u32 frGetWeaponBySlot(s32 slot);
s32 frGetNumWeaponsAvailable(void);
void frInitLighting(void);
void frRestoreLighting(void);
void frUnloadData(void);
void *frLoadRomData(u32 len);
void frSetDifficulty(s32 difficulty);
u32 frGetDifficulty(void);
void frInitDefaults(void);
struct frdata *frGetData(void);
u32 frResolveFrPad(u32 arg0);
s32 frIsDifficulty(u32 difficulties);
void frExecuteWeaponScript(s32 scriptindex);
void frSetTargetProps(void);
s32 frTargetIsAtScriptStart(s32 targetnum);
char *frGetInstructionalText(u32 index);
void frExecuteHelpScript(void);
s32 frExecuteTargetScript(s32 targetnum);
void frHideAllTargets(void);
void frInitTargets(void);
void frCloseAndLockDoor(void);
void frUnlockDoor(void);
void frLoadData(void);
u32 frInitAmmo(s32 weapon);
void frBeginSession(s32 weapon);
char *frGetWeaponDescription(void);
void frEndSession(s32 hidetargets);
s32 frWasTooInaccurate(void);
void frSetFailReason(s32 failreason);
void frSetCompleted(void);
s32 frIsTargetOneHitExplodable(struct prop *prop);
f32 frGetTargetAngleToPos(struct coord *a, f32 angle, struct coord *b);
s32 frIsTargetFacingPos(struct prop *prop, struct coord *pos);
struct prop *frChooseAutogunTarget(struct coord *arg0);
s32 frIsAmmoWasted(void);
void frTick(void);
void func0f1a0924(struct prop *prop);
s32 frChooseFarsightTarget(void);
s32 frIsInTraining(void);
void frCalculateHit(struct defaultobj *obj, struct coord *hitpos, f32 maulercharge);
void frIncrementNumShots(void);
s32 ciIsChrBioUnlocked(u32 bodynum);
struct chrbio *ciGetChrBioByBodynum(u32 bodynum);
char *ciGetChrBioDescription(void);
s32 ciGetNumUnlockedChrBios(void);
s32 ciGetChrBioBodynumBySlot(s32 slot);
struct miscbio *ciGetMiscBio(s32 index);
s32 ciIsMiscBioUnlocked(s32 index);
s32 ciGetNumUnlockedMiscBios(void);
s32 ciGetMiscBioIndexBySlot(s32 slot);
char *ciGetMiscBioDescription(void);
s32 ciIsHangarBioAVehicle(s32 index);
struct hangarbio *ciGetHangarBio(s32 index);
s32 ciIsHangarBioUnlocked(u32 bioindex);
s32 ciGetNumUnlockedLocationBios(void);
s32 ciGetNumUnlockedHangarBios(void);
s32 ciGetHangarBioIndexBySlot(s32 slot);
char *ciGetHangarBioDescription(void);
struct trainingdata *dtGetData(void);
void dtRestorePlayer(void);
void dtPushEndscreen(void);
void dtTick(void);
void func0f1a1ac0(void);
void dtBegin(void);
void dtEnd(void);
s32 dtIsAvailable(s32 deviceindex);
s32 dtGetNumAvailable(void);
s32 dtGetIndexBySlot(s32 wantindex);
u32 dtGetWeaponByDeviceIndex(s32 deviceindex);
u32 ciGetStageFlagByDeviceIndex(u32 deviceindex);
char *dtGetDescription(void);
char *dtGetTip1(void);
char *dtGetTip2(void);
struct trainingdata *getHoloTrainingData(void);
void htPushEndscreen(void);
void htTick(void);
void func0f1a2198(void);
void htBegin(void);
void htEnd(void);
s32 htIsUnlocked(u32 value);
s32 htGetNumUnlocked(void);
s32 htGetIndexBySlot(s32 slot);
char *htGetName(s32 index);
u32 func0f1a25c0(s32 index);
char *htGetDescription(void);
char *htGetTip1(void);
char *htGetTip2(void);
void frGetGoalTargetsText(char *buffer);
void frGetTargetsDestroyedValue(char *buffer);
void frGetScoreValue(char *buffer);
void frGetGoalScoreText(char *buffer);
f32 frGetAccuracy(char *buffer);
s32 frGetMinAccuracy(char *buffer, f32 accuracy);
s32 frFormatTime(char *buffer);
s32 frGetHudMiddleSubtext(char *buffer);
s32 frGetFeedback(char *score, char *zone);
Gfx *frRenderHudElement(Gfx *gdl, s32 x, s32 y, char *string1, char *string2, u32 colour, u8 alpha);
Gfx *frRenderHud(Gfx *gdl);
void func0f176d70(s32 arg0);
u32 align4(u32 arg0);
u32 align16(u32 arg0);
u32 align32(u32 arg0);
void func0f176ddc(void);
void func0f1770ac(struct coord *a, struct coord *b, struct coord *out);
s32 func0f177164(struct coord *arg0, struct coord *arg1, u32 line, char *file);
s32 func0f1773c8(struct coord *a, struct coord *b);
f32 coordsGetDistance(struct coord *a, struct coord *b);
f32 func0f1776cc(struct coord *a, struct coord *b, struct coord *c);
s32 func0f17776c(struct coord *a, struct coord *b, f32 mult, struct coord *out);
void func0f1777f8(void);
u32 func0f177a54(void);
u32 func0f177bb4(void);
s32 func0f177c8c(s32 arg0, s32 *arg1, s32 *arg2);
s16 func0f13e0e0(f32 arg0);
void func0f13e1b0(struct var800a41b0 *arg0);
void func0f13e40c(struct prop *prop, s8 arg1);
s32 chrIsUsingPaintball(struct chrdata *chr);
void func0f13e5c8(struct prop *prop);
void func0f13e640(struct var800a41b0 *thing, u32 arg1);
u32 func0f13e744(void);
u32 func0f13e994(void);
void func0f13eb44(void);
void func0f13f3f4(struct coord *arg0, struct coord *arg1, struct coord *arg2, u32 arg3, u32 arg4, u32 arg5, s32 room, u32 arg7, s32 arg8, u32 arg9, struct chrdata *chr, s32 arg11);
u32 func0f13f504(void);
s32 func0f140750(struct coord *coord);
u32 func0f1408a8(void);
u32 func0f140b7c(void);
Gfx *func0f140e20(Gfx *gdl, struct prop *prop, s32 withalpha);
u32 func0f1411b0(void);
void func0f141234(void);
void func0f14159c(struct prop *prop);
void func0f141704(struct prop *prop);
void weatherInit(void);
void weatherTick(void);
Gfx *weatherRender(Gfx *gdl);
void weatherSetBoundaries(struct weatherparticledata *data, s32 index, f32 min, f32 max);
struct weatherparticledata *weatherAllocateParticles(void);
void func0f131610(struct weatherdata *weather);
void func0f131678(s32 arg0);
void weatherSetIntensity(s32 intensity);
void weatherTickRain(struct weatherdata *weather);
void weatherTickSnow(struct weatherdata *weather);
void weatherConfigureRain(u32 intensity);
void weatherConfigureSnow(u32 intensity);
s32 weatherIsRoomWeatherProof(s32 room);
Gfx *weatherRenderRain(Gfx *gdl, struct weatherdata *weather, s32 arg2);
Gfx *weatherRenderSnow(Gfx *gdl, struct weatherdata *weather, s32 arg2);
void weatherReset(void);
struct huft {
 u8 e;
 u8 b;
 union {
  u16 n;
  struct huft *t;
 } v;
};
u32 inflate1173(void *src, void *dst, struct huft *hlist);
char *argParseString(char *str);
void argSetString(char *string);
s32 argsParseDebugArgs(void);
char *argFindByPrefix(s32 occurrence, char *string);
void argGetLevel(s32 *stagenum);
void amgrAllocateStack(void);
void amgrCreate(ALSynConfig *config);
void amgrStartThread(void);
OSMesgQueue *amgrGetFrameMesgQueue(void);
void amgrStopThread(void);
void boot(void);
s32 boot000010a4(void);
s32 boot0000113c(void);
s32 boot00001180(void);
void bootUnmapTLBRange(s32 first, s32 last);
void dmaInit(void);
void dmaStart(void *memaddr, u32 romaddr, u32 len, s32 priority);
void dmaCheckPiracy(void *memaddr, u32 len);
void dmaWait(void);
void dmaExec(void *memaddr, u32 romaddr, u32 len);
void dmaExecHighPriority(void *memaddr, u32 romaddr, u32 len);
void *dmaExecWithAutoAlign(void *memaddr, u32 romaddr, u32 len);
void gvarsInit(void);
void init(void);
s32 initGetMemSize(void);
void *allocateStack(s32 threadid, s32 size);
void mainproc(void *arg);
void joy00013900(void);
void joy00013938(void);
void joy00013974(u32 value);
u32 joy00013980(void);
void joy0001398c(s32 value);
void joy000139c8(void);
s32 joyShiftPfsStates(void);
void joyRecordPfsState(u8 pfsstate);
void joyCheckPfs(s32 arg0);
void joySetPfsTemporarilyPlugged(s8 index);
void joySystemInit(void);
void joyDisableTemporarily(void);
void joy00013dfc(void);
void joy00013e84(void);
u32 joyGetConnectedControllers(void);
void joyConsumeSamples(struct joydata *joydata);
void joy00014238(void);
void joyDebugJoy(void);
s32 joyStartReadData(OSMesgQueue *mq);
void joyReadData(void);
void joyPoll(void);
void joy00014810(s32 value);
s32 joyGetNumSamples(void);
s32 joyGetStickXOnSample(s32 samplenum, s8 contpadnum);
s32 joyGetStickYOnSample(s32 samplenum, s8 contpadnum);
s32 joyGetStickYOnSampleIndex(s32 samplenum, s8 contpadnum);
u16 joyGetButtonsOnSample(s32 samplenum, s8 contpadnum, u16 mask);
u16 joyGetButtonsPressedOnSample(s32 samplenum, s8 contpadnum, u16 mask);
s32 joyCountButtonsOnSpecificSamples(u32 *arg0, s8 contpadnum, u16 mask);
s32 joyGetStickX(s8 contpadnum);
s32 joyGetStickY(s8 contpadnum);
u16 joyGetButtons(s8 contpadnum, u16 mask);
u16 joyGetButtonsPressedThisFrame(s8 contpadnum, u16 mask);
s32 joy000150c4(void);
void joy000150e8(s32 line, char *file);
void joy00015144(s32 line, char *file);
void joyReset(void);
void joyGetContpadNumsForPlayer(s8 playernum, s32 *pad1, s32 *pad2);
void joy000153c4(s8 arg0, s32 arg1);
s32 joy000155b4(s8 index);
s32 joy000155f4(s8 index);
void joy0001561c(void);
u32 func00006100(void);
s32 func00006330(OSPfs *pfs, u32 arg1);
s32 osEepromLongWrite(OSMesgQueue *mq, u8 address, u8 *buffer, int nbytes);
s32 func00006550(OSPfs *pfs, u16 company_code, u32 game_code, u8 *game_name, u8 *ext_name, u32 numbytes);
s32 osPfsInitPak(OSMesgQueue *mq, OSPfs *pfs, s32 channel);
s32 func00007084(OSPfs *pfs);
u32 func000070d0(void);
u32 rzipInflate(void *src, void *dst, void *tmpbuffer);
u32 func00007908(void);
u32 func0000796c(void);
u32 func00007a10(void);
u32 func00007f20(void);
u32 func00008024(void);
u32 func00008064(void);
u32 func00008610(void);
u32 func000088b0(void);
s32 rzipIs1173(void *buffer);
u32 func00008a08(void);
u32 func00009660(void);
void amgrClearDmaBuffers(void);
u32 func00009a08(void);
void func00009a80(void);
void func00009a88(void);
void func00009a90(void);
void func00009a98(void);
void func00009aa0(u32 arg0);
Gfx *debugRenderSomething(Gfx *gdl);
void func00009ab0(void);
void func00009b50(void *fb);
void func00009bf8(void);
void func00009c3c(s32 stagenum);
void func00009ec4(s32 arg0);
u32 func00009ed4(void);
void func0000a044(void);
void func0000aa50(f32 arg0);
void func0000aab0(u32 arg0);
void *viGetUnk28(void);
void *vi2GetUnk28(void);
u32 func0000ab4c(void);
u32 func0000ab6c(void);
u32 func0000ab78(void);
u32 func0000aca4(void);
u32 func0000ad5c(void);
u32 func0000af00(void);
u32 func0000b0e8(void);
Gfx *func0000b1a8(Gfx *gdl);
Gfx *func0000b1d0(Gfx *gdl);
Gfx *func0000b280(Gfx *gdl);
Gfx *func0000b2c4(Gfx *gdl);
Gfx *func0000b330(Gfx *gdl);
void viSetBuf(s16 x, s16 y);
s16 viGetBufX(void);
s16 viGetBufY(void);
void viSetXY(s16 x, s16 y);
s16 viGetWidth(void);
s16 viGetHeight(void);
void viSetViewSize(s16 x, s16 y);
s16 viGetViewWidth(void);
s16 viGetViewHeight(void);
void viSetViewPosition(s16 left, s16 top);
s16 viGetViewLeft(void);
s16 viGetViewTop(void);
void viSetUseZBuf(s32 use);
void viSetFovY(f32 fovy);
void viSetAspect(f32 aspect);
f32 viGetAspect(void);
void viSetFovAspectAndSize(f32 fovy, f32 aspect, s16 width, s16 height);
f32 viGetFovY(void);
void viSetZRange(f32 arg0, f32 arg1);
void viGetZRange(struct zrange *zrange);
u32 func0000bf04(void);
void faultCreateThread(void);
void func0000bfd0(char *msg, char *file, s32 line);
extern const char var70052700[];
extern const char var70052704[];
extern const char var70052708[];
extern const char var7005270c[];
extern const char var70052710[];
extern const char var70052714[];
extern const char var70052718[];
extern const char var7005271c[];
extern const char var70052720[];
extern const char var70052724[];
extern const char var70052728[];
extern const char var70052730[];
extern const char var70052738[];
extern const char var70052744[];
extern const char var70052770[];
extern const char var70052788[];
extern const char var700527b4[];
extern const char var700527dc[];
extern const char var700527e4[];
extern const char var700527e8[];
extern const char var70052800[];
extern const char var70052810[];
extern const char var7005281c[];
extern const char var70052824[];
extern const char var70052854[];
extern const char var70052864[];
extern const char var70052870[];
extern const char var700528a0[];
extern const char var700528a4[];
extern const char var700528a8[];
extern const char var700528ac[];
extern const char var700528b0[];
extern const char var700528b4[];
extern const char var700528b8[];
extern const char var700528bc[];
extern const char var700528c0[];
extern const char var700528c4[];
extern const char var700528c8[];
extern const char var700528cc[];
extern const char var700528d0[];
extern const char var700528d4[];
extern const char var700528d8[];
extern const char var700528dc[];
extern const char var700528e0[];
extern const char var700528e4[];
extern const char var700528e8[];
extern const char var700528ec[];
extern const char var700528f0[];
extern const char var700528f4[];
extern const char var700528f8[];
extern const char var700528fc[];
extern const char var70052900[];
extern const char var70052904[];
extern const char var70052908[];
extern const char var7005290c[];
extern const char var70052910[];
extern const char var70052914[];
extern const char var70052918[];
extern const char var7005291c[];
extern const char var70052920[];
extern const char var70052924[];
extern const char var70052928[];
extern const char var70052938[];
extern const char var70052944[];
extern const char var70052950[];
extern const char var7005295c[];
extern const char var70052968[];
extern const char var70052974[];
extern const char var70052978[];
extern const char var7005297c[];
extern const char var70052980[];
extern const char var70052984[];
extern const char var70052988[];
extern const char var7005298c[];
extern const char var70052990[];
extern const char var70052994[];
extern const char var70052998[];
extern const char var7005299c[];
extern const char var700529a0[];
extern const char var700529a4[];
extern const char var700529a8[];
extern const char var700529ac[];
void faultCreateThread2(void);
u32 crashGetParentStackFrame(u32 *ptr, u32 *start, u32 sp, u32 *regs);
s32 crashIsReturnAddress(u32 *instruction);
u32 crashGetStackEnd(u32 sp, s32 tid);
u32 crashGetStackStart(u32 arg0, s32 tid);
s32 crashIsDouble(f32 value);
void rmonPrintFloat(s32 index, f32 arg1);
void rmonPrint2Floats(s32 index, f32 value1, f32 value2);
void rmonPrint3Floats(s32 index, f32 value1, f32 value2, f32 value3);
void crashPutChar(s32 x, s32 y, char c);
void crashAppendChar(char c);
void crashScroll(s32 numlines);
void func0000cdc8(s32 x, s32 y, char c);
void crashReset(void);
void func0000cf54(u16 *fb);
u32 func00011420(void);
u32 func00011700(void);
u32 func00011780(void);
u32 func00011884(void);
u32 func000118f4(void);
void func0001190c(void);
void func00011d84(void);
u32 func000126b0(void);
u32 func000126d4(void);
u32 func000126f0(void);
void func000127b8(void);
u32 func00012800(void);
u32 func00012914(void);
void func00012a0c(void);
void func00012a14(void *ptr, u32 arg1);
void func00012a8c(void);
void *func00012ab0(u32 size);
u32 func00012c3c(void);
void func00012cb4(void *arg0, s32 arg1);
s32 func00012cdc(void);
u32 func00012d48(void);
void func00013710(void);
void func00013750(void);
void func00013758(void);
void func00013790(void);
void func00013798(void);
void func000159b0(Mtxf *matrix);
void func000159fc(Mtxf *matrix, Mtxf *arg1);
void func00015a00(Mtxf *matrix, Mtxf *arg1, Mtxf *arg2);
void func00015b10(Mtxf *matrix, struct coord *coord);
void func00015b14(Mtxf *matrix, struct coord *coord, struct coord *arg2);
void func00015b64(Mtxf *arg0, struct coord *coord);
void func00015b68(Mtxf *matrix, struct coord *arg1, struct coord *arg2);
void func00015be0(Mtxf *matrix1, Mtxf *matrix2);
void func00015be4(Mtxf *arg0, Mtxf *arg1, Mtxf *arg2);
void func00015cd8(f32 *arg0, f32 *realrot);
void func00015d18(Mtxf *matrix, Mtxf *arg1);
void func00015d54(f32 *realrot, Mtxf *matrix);
void func00015da0(Mtxf *arg0, f32 *arg1);
void func00015dd4(struct coord *pos, Mtxf *matrix);
u32 func00015df0(void);
void func00015e24(f32 mult, f32 *matrix);
u32 func00015e4c(void);
void func00015e80(f32 mult, f32 *matrix);
u32 func00015ea8(void);
void func00015edc(f32 mult, Mtxf *matrix);
void func00015f04(f32 scale, Mtxf *arg1);
void func00015f4c(f32 scale, Mtxf *arg1);
void func00015f88(f32 arg0, Mtxf *arg1);
u32 func00015fd0(void);
void func00016054(Mtxf *matrix, Mtxf *arg1);
void func00016110(f32 *matrix1, f32 *matrix2);
void func00016140(f32 *realrot, f32 *arg1, f32 *arg2);
void func000161b0(f32 *matrix, f32 src[3], f32 dest[3]);
void func00016208(f32 *matrix, struct coord *coord);
void func00016248(struct coord *coord, f32 angle, Mtxf *matrix);
void func000162e8(f32 angle, Mtxf *matrix);
void func00016374(f32 angle, Mtxf *matrix);
void func00016400(f32 angle, Mtxf *matrix);
void func0001648c(struct coord *coord, Mtxf *dest);
u32 func000165d8(void);
void func000166a4(struct coord *pos, struct coord *rot, Mtxf *matrix);
void func000166dc(struct coord *pos, Mtxf *matrix);
u32 func00016710(void);
void func00016748(f32 arg0);
u32 func00016760(void);
u32 func00016784(void);
void func00016798(Mtxf *arg0, Mtxf *arg1);
void func00016820(Mtx *arg0, Mtx *arg1);
void func00016874(Mtxf *matrix, f32 posx, f32 posy, f32 posz, f32 lookx, f32 looky, f32 lookz, f32 upx, f32 upy, f32 upz);
void func00016ae4(Mtxf *matrix, f32 arg1, f32 arg2, f32 arg3, f32 arg4, f32 arg5, f32 arg6, f32 arg7, f32 arg8, f32 arg9);
void func00016b58(Mtxf *matrix, f32 posx, f32 posy, f32 posz, f32 lookx, f32 looky, f32 lookz, f32 upx, f32 upy, f32 upz);
void func00016d58(Mtxf *matrix, f32 posx, f32 posy, f32 posz, f32 lookx, f32 looky, f32 lookz, f32 upx, f32 upy, f32 upz);
u32 func00016dcc(void);
u32 func00016e98(void);
u32 func00017028(void);
u32 func00017070(void);
u32 func000170e4(void);
u32 func0001719c(void);
u32 func000172f0(void);
u32 func00017588(void);
u32 func00017614(void);
u32 func00017a78(void);
u32 func00017c2c(void);
u32 func00017cbc(void);
u32 func00017ce0(void);
u32 func00017dc4(void);
s32 func00017e30(s32 portalnum, struct coord *arg1, struct coord *arg2);
void func00018148(struct coord *pos, struct coord *pos2, s16 *rooms, s16 *arg3, s16 *arg4, s32 arg5);
u8 *ailistFindById(s32 ailistid);
void objSetBlockedPathUnblocked(struct defaultobj *blocker, s32 unblocked);
s32 func00018680(void);
u32 func00019634(void);
u32 func000198dc(void);
u32 func00019be0(void);
u32 func00019d1c(void);
u32 func00019ddc(void);
u32 func00019f2c(void);
u32 func00019f98(void);
u32 func0001a024(void);
u32 func0001a0ac(void);
u32 func0001a110(void);
u32 func0001a1c0(void);
u32 func0001a25c(void);
u32 func0001a2dc(void);
void func000233c0(void);
u32 func00023640(void);
void func0002373c(void);
s32 animGetNumFrames(s16 anim_id);
s32 func00023794(s16 animnum);
s32 getNumAnimations(void);
u32 func000237e8(void);
s32 func0002384c(s32 animnum, s32 frame);
u32 func00023908(void);
s32 func000239e0(s16 animnum, s32 frame);
s32 func00023ab0(s16 animnum, s32 framenum);
void func00023d0c(void);
void func00023d38(s16 animnum);
u32 func00023f50(void);
u32 func00023fe0(void);
void func00024050(s32 arg0, s32 arg1, struct modeltype *arg2, s32 animnum, u32 arg4, struct coord *arg5, struct coord *arg6, struct coord *arg7);
u32 func0002485c(void);
f32 func00024b64(u32 arg0, u32 arg1, struct modeltype *arg2, s32 animnum, u32 arg4, struct coord *coord, u32 arg6);
f32 func00024c14(u32 arg0, s32 animnum, u8 arg2);
f32 func00024e40(void);
void func00024e4c(struct coord *arg0, struct coord *arg1, u32 line, char *file);
f32 func00024e98(void);
s32 func00024ea4(void);
struct prop *cdGetObstacle(void);
void cdGetPos(struct coord *pos, u32 line, char *file);
void func00024ee8(struct coord *arg0);
u32 cdGetTileFlags(void);
void func00024f6c(void);
void func00024fb0(struct coord *arg0, struct coord *arg1, struct prop *prop);
u32 func00025038(void);
void func000250cc(struct coord *arg0, struct coord *arg1, f32 width);
void func00025168(struct prop *prop);
u32 func000251ac(void);
void func00025254(struct coord *arg0, struct coord *arg1, struct coord *pos, struct prop *prop, f32 arg4, struct tile *tile);
u32 func00025314(void);
u32 func00025364(void);
u32 func000253c4(void);
u32 func00025410(void);
u32 func000254d8(void);
f32 func00025654(f32 x1, f32 z1, f32 x2, f32 z2, f32 x3, f32 z3);
f32 func00025724(f32 x1, f32 z1, f32 x2, f32 z2);
s32 func00025774(f32 x1, f32 z1, f32 x2, f32 z2, f32 x3, f32 z3);
void func00025848(f32 tilex, f32 tilez, f32 tilewidth, f32 posx, f32 posz, f32 *x1, f32 *z1, f32 *x2, f32 *z2);
void func00025928(struct tile *tile, struct coord *arg1);
void tileGetFloorCol(struct tile *tile, u16 *floorcol);
void tileGetFloorType(struct tile *tile, u8 *floortype);
f32 cdFindGroundInTileType0AtVertex(struct tiletype0 *tile, f32 x, f32 z, s32 vertexindex);
f32 cdFindGroundInTileType0(struct tiletype0 *tile, f32 x, f32 z);
f32 cdFindGroundInTileType1(struct tiletype1 *tile, f32 x, f32 z);
s32 cdIs2dPointInTileType0(struct tiletype0 *tile, f32 x, f32 z);
s32 cdIs2dPointInTileType1(struct tiletype1 *tile, f32 x, f32 z);
s32 cdIs2dPointInTileType2(struct tiletype2 *tile, f32 x, f32 z);
s32 cdIs2dPointInTileType3(struct tiletype3 *tile, f32 x, f32 z);
s32 func000266a4(f32 x, f32 z, struct tile *tile);
void platformGetRidingProps(struct prop *platform, s16 *propnums, s32 len);
s32 func00026a04(struct coord *pos, u8 *start, u8 *end, u16 flags, s32 room, struct tile **tileptr, s32 *roomptr, f32 *groundptr, s32 arg8);
void func00026e7c(struct coord *pos, s16 *rooms, u16 arg2, struct tile **tileptr, s16 *arg4, f32 *arg5, struct prop **arg6, s32 arg7);
s32 func0002709c(struct tiletype0 *tile, f32 x, f32 z, f32 width, struct prop *prop, struct collisionthing *thing);
s32 func000272f8(struct tiletype1 *tile, f32 x, f32 z, f32 width, struct prop *prop, struct collisionthing *thing);
s32 func000274e0(struct tiletype2 *tile, f32 x, f32 z, f32 width, struct prop *prop, struct collisionthing *thing);
s32 func000276c8(struct tiletype3 *tile, f32 x, f32 z, f32 width, struct prop *prop, struct collisionthing *thing);
void func00027738(struct coord *pos, f32 width, u8 *start, u8 *end, u16 flags, s32 checkvertical, f32 arg6, f32 arg7, struct prop *prop, struct collisionthing *things, s32 maxthings, s32 *thingnum, s32 roomnum);
void func00027d1c(struct coord *pos, f32 width, s16 *rooms, u32 types, u16 arg4, u32 arg5, f32 arg6, f32 arg7, struct collisionthing *arg8, s32 arg9);
u32 func00027f78(void);
s32 func00028200(struct tiletype0 *tile, struct coord *pos, f32 width, f32 y1, f32 y2);
u32 func0002840c(void);
u32 func00028638(void);
u32 func0002885c(void);
s32 func00028914(u8 *start, u8 *end, struct coord *pos, f32 width, u16 flags, s32 checkvertical, f32 arg6, f32 arg7, struct prop *prop, struct collisionthing *things, s32 maxthings, s32 *thingnum);
void func00028df0(struct coord *pos, f32 width, s16 *rooms, u32 types, u16 arg4, u32 arg5, f32 ymax, f32 ymin, struct collisionthing *arg8, s32 arg9);
void func0002901c(struct coord *pos, struct coord *dist, f32 width, struct collisionthing *arg3);
f32 func000296a0(struct collisionthing *arg0, struct coord *pos, struct tilething **arg2, f32 width);
s32 func00029ffc(struct coord *pos, f32 width, f32 foreheadheight, f32 inversefeettoeyesheight, s16 *rooms, u16 arg5, struct coord *laddernormal);
s32 func0002a13c(struct coord *pos, f32 width, f32 arg2, f32 arg3, s16 *rooms, u16 arg5);
f32 cdFindGroundY(struct coord *pos, f32 width, s16 *rooms, u16 *floorcol, u8 *floortype, u16 *floorflags, s16 *floorroom, s32 *inlift, struct prop **lift);
f32 func0002a324(void);
f32 cdFindGroundYSimple(struct coord *pos, f32 width, s16 *rooms, u16 *floorcol, u8 *floortype);
f32 func0002a36c(struct coord *coord, s16 *rooms, u16 *floorcol, u8 *floortype);
s32 func0002a400(struct coord *pos, s16 *rooms);
s16 func0002a440(struct coord *pos, s16 *rooms, f32 *arg2, u16 *floorcolptr);
s16 func0002a4d0(struct coord *pos, s16 *rooms, f32 *arg2, u16 *floorcolptr, u16 *flagsptr);
s32 func0002a564(struct coord *pos, s16 *rooms, f32 *arg2, u16 *floorcol, struct coord *arg4, struct prop **propptr);
s32 func0002a5e4(struct coord *pos, s16 *rooms, f32 *arg2, u16 *floorcol, u16 *arg4, struct coord *arg5);
s32 cdTestVolume(struct coord *pos, f32 width, s16 *rooms, s32 types, s32 arg4, f32 ymax, f32 ymin);
s32 func0002a6fc(struct coord *pos, struct coord *pos2, f32 width, s16 *rooms, s32 types, s32 arg5, f32 arg6, f32 arg7);
s32 cdTestAToB1(struct coord *origpos, struct coord *dstpos, f32 width, s16 *dstrooms, s32 types, s32 arg5, f32 ymax, f32 ymin);
u32 func0002aac0(void);
u32 func0002ab98(void);
u32 func0002ac70(void);
u32 func0002b128(void);
u32 func0002b560(void);
u32 func0002b954(void);
s32 func0002bd04(u8 *start, u8 *end, struct coord *arg2, struct coord *arg3, struct coord *arg4, s32 arg5, s32 arg6, s32 arg7, f32 arg8, f32 arg9);
u32 func0002c328(void);
u32 func0002c528(void);
s32 func0002c714(u8 *start, u8 *end, struct coord *arg2, struct coord *arg3, struct coord *arg4, u16 arg5, s32 arg6, s32 arg7, f32 ymax, f32 ymin, f32 *arg10, struct coord *arg11, struct coord *arg12, struct coord *arg13, struct tile **tile, s32 roomnum);
s32 func0002d15c(struct coord *pos, struct coord *coord2, s16 *rooms, u32 types, u16 arg4, s32 arg5, s32 arg6, f32 arg7, f32 arg8);
s32 func0002d3b0(struct coord *arg0, struct coord *arg1, s16 *arg2, s32 types, u16 arg4, s32 arg5, s32 arg6, f32 ymax, f32 ymin);
s32 func0002d6ac(struct coord *pos, s16 *rooms, struct coord *targetpos, u32 types, u32 arg4, f32 arg5, f32 arg6);
s32 cdTestAToB2(struct coord *pos, s16 *rooms, struct coord *coord2, s16 *rooms2, u32 types, s32 arg5, f32 arg6, f32 arg7);
s32 func0002d7c0(struct coord *pos, s16 *rooms, struct coord *arg2, u32 arg3, u32 arg4, f32 ymax, f32 ymin);
s32 func0002d840(struct coord *arg0, s16 *arg1, struct coord *arg2, s16 *arg3, u32 types, s32 arg5, f32 ymax, f32 ymin);
s32 func0002d8b8(struct coord *pos, s16 *rooms, struct coord *pos2, s16 *rooms2, s32 types, s32 arg5, f32 ymax, f32 ymin);
s32 cdTestAToB3(struct coord *arg0, s16 *arg1, struct coord *arg2, s16 *arg3, f32 arg4, s32 types, s32 arg6, f32 ymax, f32 ymin);
void func0002da50(struct coord *arg0, s16 *arg1, struct coord *arg2, s16 *arg3, u32 types, s32 arg5, f32 ymax, f32 ymin);
s32 func0002dac8(struct coord *arg0, s16 *arg1, struct coord *arg2, s16 *arg3, f32 width, u32 types, s32 arg6, f32 ymax, f32 ymin);
s32 func0002db98(struct coord *viewpos, s16 *rooms, struct coord *targetpos, u32 types, u16 arg4);
s32 func0002dc18(struct coord *coord, s16 *rooms, struct coord *coord2, s32 arg3);
s32 hasLineOfSight(struct coord *coord, s16 *rooms, struct coord *coord2, s16 *rooms2, s32 arg4, u16 arg5);
s32 func0002dcd0(struct coord *arg0, s16 *rooms1, struct coord *arg2, s16 *rooms2, u32 arg4);
s32 func0002dcfc(struct coord *pos, s16 *rooms, struct coord *pos2, s16 *rooms2, s16 *rooms3, u32 types, u16 arg6);
s32 cdTestAToB4(struct coord *pos, s16 *rooms, struct coord *pos2, u32 types, u16 arg4);
s32 func0002de10(struct coord *pos, s16 *rooms, struct coord *pos2, u32 types);
s32 func0002de34(struct coord *arg0, s16 *arg1, struct coord *arg2, s16 *arg3, u32 types, u16 arg5);
s32 func0002deac(struct coord *arg0, s16 *arg1, struct coord *arg2, s16 *arg3, u32 types);
s32 func0002ded8(struct coord *arg0, struct coord *arg1, struct prop *prop);
s32 func0002dffc(struct tiletype2 *arg0, struct tiletype2 *arg1);
s32 func0002e278(u8 *start, u8 *end, struct tiletype2 *ref, u16 flags);
s32 func0002e4c4(struct tiletype2 *geo, s16 *rooms, u32 types);
u32 func0002e680(void);
u32 func0002e82c(void);
u32 func0002e9d8(void);
u32 func0002eb84(void);
u32 func0002ed30(void);
u32 func0002f02c(void);
s32 func0002f308(struct coord *viewpos, s16 *rooms, struct coord *targetpos, f32 distance, s32 arg4, u16 arg5);
s32 func0002f450(struct coord *viewpos, s16 *rooms, struct coord *targetpos, f32 distance, s32 arg4);
u32 func0002f490(void);
u32 func0002f560(void);
void rmonproc();
s32 rmonIsDisabled(void);
void crashPrint(const char *format, ...);
void n_alInit(ALGlobals *g, ALSynConfig *c);
u32 func0002fc60(void);
u32 func000301a4(void);
u32 func000301e4(void);
u32 func00030690(void);
u32 func00030bd8(void);
u32 func00030bfc(void);
void func00030c98(ALSeqpConfig *config);
void n_alSynNew(ALSynConfig *c);
void func000317f0(void *arg0);
u32 func00031b34(void);
u32 func00033090(void);
u32 func00033100(void);
u32 func00033180(void);
u32 func00033274(void);
void func00033378(void *fn);
u32 func00033390(void);
void func00033634(void *fn);
u32 func0003364c(void);
u32 audioIsPlaying(struct audiohandle *handle);
struct audiohandle *func00033820(s32 arg0, s16 soundnum, s32 arg2, s32 arg3, f32 arg4, s32 arg5, s32 arg6, struct audiohandle **handle);
void audioStop(struct audiohandle *handle);
u32 func00033bc0(void);
u32 func00033c30(void);
u32 func00033cf0(void);
u32 func00033db0(void);
u32 func00033dd8(void);
void func00033e50(struct audiohandle *handle, s32 arg1, s32 arg2);
u16 func00033ec4(s32 arg0);
u32 func00033f08(void);
u32 func00033f24(void);
void func00033f44(u8 index, u16 volume);
void func00034030(s32 arg0);
void func00034104(s32 index, s32 arg1);
u32 alCSPNew(void);
u32 func00034df8(void);
u32 func00034f0c(void);
u32 func00034fb8(void);
u32 func00035110(void);
u32 func0003759c(void);
void func000034d0(void);
void func000034d8(void);
void func000034e0(Gfx **gdl);
void func00037650(ALCSPlayer *seqp, ALBank *bank);
void mp3Init(ALHeap *heap);
void func00037d88(s32 arg0, s32 arg1);
void func00037e1c(void);
void func00037e38(void);
void func00037e68(void);
s32 func00037ea4(void);
void func00037f08(s32 arg0, s32 arg1);
void func00037f5c(s32 arg0, s32 arg1);
u32 func00037fc0(void);
void func00038924(struct mp3vars *vars);
void func00038b90(void *fn);
void func00038cac(void);
u32 func00038d10(void);
u32 func00039be0(void);
u32 func00039c30(void);
void func00039c80(ALCSPlayer *seqp, s16 volume);
void func00039cd0(ALCSPlayer *seqp);
u32 func00039e5c(void);
u32 func00039f70(void);
void n_alSynDelete(void);
u32 func00039fe0(void);
u32 func0003a070(void);
u32 func0003a100(void);
u32 n_alFxParamHdl(void);
u32 func0003ae60(void);
u32 func0003b178(void);
u32 func0003b370(void);
u32 func0003b54c(void);
u32 func0003b710(void);
u32 func0003b884(void);
u32 func0003b9d4(void);
u32 func0003ba64(void);
u32 func0003bc50(void);
u32 func0003c214(void);
Acmd *func0003c430(s32 arg0, void *arg1);
u32 func0003c900(void);
u32 func0003c9e0(void);
u32 func0003cbb4(void);
u32 func0003c900(void);
u32 func0003c9e0(void);
u32 func0003cbb4(void);
u32 func0003cdc0(void);
u32 func0003cef0(void);
u32 func0003cfa0(void);
u32 func0003d050(void);
u32 func0003d100(void);
u32 func0003d1a0(void);
u32 func0003d280(void);
u32 func0003d5d8(void);
u32 func0003d69c(void);
u32 func0003d72c(void);
u32 func0003d8a4(void);
u32 func0003d9cc(void);
void __n_resetPerfChanState(ALSeqPlayer *seqp, s32 chan);
u32 func0003e3e0(void);
u32 func0003e490(void);
u32 func0003e540(void);
u32 func0003e5b8(void);
u32 n_alSynSetFXParam(void);
u32 func0003e674(void);
u32 func0003e730(void);
extern const u32 var70054b40[];
extern const u32 var70054b68[];
extern const u32 var70054bb8[];
extern const u32 var70054c08[];
extern const u32 var70054c08[];
extern const u32 var70054c90[];
extern const u32 var70054d18[];
extern const u32 var70054e40[];
extern const u32 var70054f68[];
extern const u32 var70055090[];
extern const u32 var70055298[];
extern const u32 var700554a0[];
extern const u32 var700556a8[];
extern const u32 var70055eb0[];
extern const u32 var70055eb0[];
extern const u32 var700566b8[];
extern const u32 var700566b8[];
extern const u32 var700566b8[];
extern const u32 var700566b8[];
extern const u32 var700566b8[];
extern const u32 var700566b8[];
extern const u32 var700566b8[];
extern const u32 var700566b8[];
extern const u32 var70056ec0[];
extern const u32 var70056ec0[];
extern const u32 var70056ec0[];
extern const u32 var70056ec0[];
extern const u32 var70056ec0[];
extern const u32 var70056ec0[];
extern const u32 var70056ec0[];
extern const u32 var70056ec0[];
extern const u32 var700576c8[];
extern const u32 var70057750[];
u32 func0003e8c0(void);
s32 n_alEnvmixerParam(void *filter, s32 paramID, void *param);
u32 func0003f328(void);
u32 func0003f60c(void);
s16 _getVol(s16 ivol, s32 samples, s16 ratem, u16 ratel);
u32 func0003f8a0(void);
u32 func00040164(void);
u32 func00040dac(void);
u32 func00041600(void);
u32 func00042238(void);
u32 func000427d8(void);
u32 func00042990(void);
u32 func00043dd0(void);
u32 func00043ef8(void);
u32 func00044404(void);
u32 func00044460(void);
u32 func0004453c(void);
u32 func00044650(void);
u32 func000446d0(void);
u32 func00044bc0(void);
u32 func00044f60(void);
u32 func00045608(void);
u32 func000458f8(void);
u32 func00045ab0(void);
void n_alLoadParam(void *arg0, s32 arg1, void *arg2);
u32 func00045ed0(void);
u32 func000461c0(void);
u32 func00046290(void);
u32 func000462c4(void);
u32 func000462f8(void);
u32 func000464a8(void);
u32 func00046650(void);
u32 func00047550(void);
u32 func00047a90(void);
u32 func00047d20(void);
u32 func00047ef0(void);
void osInvalICache(void *vaddr, s32 nbytes);
u32 func00050be0(void);
s32 func00050d60(OSPfs *pfs, char *arg1, u8 *arg2);
u32 func50f20(void);
s32 func000513b0(OSPfs *pfs, s32 arg1, u16 arg2, char *arg3, u16 arg4);
u32 func00051778(void);
u32 func00052360(void);
typedef struct lldiv_t
{
 long long quot;
 long long rem;
} lldiv_t;
typedef struct ldiv_t
{
 long quot;
 long rem;
} ldiv_t;
lldiv_t lldiv(long long num, long long denom);
ldiv_t ldiv(long num, long denom);
unsigned long long __ull_rshift(unsigned long long a0, unsigned long long a1);
unsigned long long __ull_rem(unsigned long long a0, unsigned long long a1);
unsigned long long __ull_div(unsigned long long a0, unsigned long long a1);
unsigned long long __ll_lshift(unsigned long long a0, unsigned long long a1);
long long __ll_rem(unsigned long long a0, long long a1);
long long __ll_div(long long a0, long long a1);
unsigned long long __ll_mul(unsigned long long a0, unsigned long long a1);
void __ull_divremi(unsigned long long *div, unsigned long long *rem, unsigned long long a2, unsigned short a3);
long long __ll_mod(long long a0, long long a1);
long long __ll_rshift(long long a0, long long a1);
long long __f_to_ll(float f);
unsigned long long __f_to_ull(float f);
double __ll_to_d(long long s);
float __ull_to_f(unsigned long long u);
typedef char *outfun(char*, const char*, size_t);
int _Printf(outfun prout, char *arg, const char *fmt, va_list args);
extern s32 var8005dd18;
void mainInit(void);
void mainEntry(void);
void func0000db30(char *string, void *ptr);
void mainLoop(void);
void mainTick(void);
void mainEndStage(void);
void mainSetStageNum(s32 stagenum);
void func0000e990(void);
void func0000e9c0(void);
s32 mainGetStageNum(void);
void func000121e0(void);
void memInit(u32 heapstart, u32 heaplen);
u32 memGetPool4Available(void);
u32 memGetNextPool4Allocation(void);
u32 memAllocFromBank(struct memorypool *pool, u32 size, u8 poolnum);
void *malloc(u32 len, u8 pool);
s32 memReallocate(u32 allocation, s32 size, u8 poolnum);
u32 memGetFree(u8 poolnum, u32 bank);
void memResetPool(u8 pool);
void memDisablePool(u8 pool);
u32 memAllocFromBankRight(struct memorypool *pool, u32 size, u8 poolnum);
void *mallocFromRight(u32 size, u8 poolnum);
void modelSetDistanceScale(f32 value);
void modelSetVtxAllocatorFunc(struct gfxvtx *(*fn)(s32 numvertices));
s32 func0001a524(struct modelnode *node, s32 arg1);
Mtxf *func0001a5cc(struct model *model, struct modelnode *node, s32 arg2);
Mtxf *func0001a60c(struct model *model);
struct modelnode *func0001a634(struct model *model, s32 mtxindex);
struct modelnode *func0001a740(struct modelnode *node);
struct modelnode *func0001a784(struct modelnode *node);
struct modelnode *func0001a7cc(struct modelnode *node);
struct modelnode *func0001a85c(struct modelnode *node);
struct modelnode *modelGetPart(struct modelfiledata *arg0, s32 partnum);
union modelrodata *modelGetPartRodata(struct modelfiledata *modelfiledata, s32 partnum);
f32 func0001a9e8(struct model *model);
void *modelGetNodeRwData(struct model *model, struct modelnode *node);
void modelNodeGetPosition(struct model *model, struct modelnode *node, struct coord *pos);
void modelNodeSetPosition(struct model *model, struct modelnode *node, struct coord *pos);
void modelGetRootPosition(struct model *model, struct coord *pos);
void modelSetRootPosition(struct model *model, struct coord *pos);
void modelNodeGetModelRelativePosition(struct model *model, struct modelnode *node, struct coord *pos);
f32 func0001ae44(struct model *model);
void func0001ae90(struct model *model, f32 angle);
void modelSetScale(struct model *model, f32 scale);
void modelSetAnimScale(struct model *model, f32 scale);
f32 func0001af80(struct model *model);
void func0001af98(struct coord *arg0, struct coord *arg1, f32 frac);
f32 func0001afe8(f32 arg0, f32 angle, f32 frac);
void func0001b07c(struct coord *arg0, struct coord *arg1, f32 mult);
void func0001b0e8(struct model *model, struct modelnode *node);
void func0001b3bc(struct model *model);
void func0001b400(struct objticksp476 *arg0, struct model *model, struct modelnode *node);
u32 func0001b80c(void);
u32 func0001bc14(void);
void func0001bfa8(struct objticksp476 *arg0, struct model *model, struct modelnode *node);
void func0001c5b4(struct objticksp476 *arg0, struct model *model, struct modelnode *node);
void func0001c664(struct model *model, struct modelnode *node);
void func0001c784(struct model *model, struct modelnode *node);
void func0001c7d0(struct model *model, struct modelnode *node);
void modelAttachHead(struct model *model, struct modelnode *node);
void func0001c868(struct modelnode *node, s32 visible);
void modelRenderNodeReorder(struct model *model, struct modelnode *node);
void func0001c950(struct model *model, struct modelnode *node);
void func0001cb0c(struct model *model, struct modelnode *node);
void func0001cc20(struct model *model);
void func0001cd18(struct objticksp476 *arg0, struct model *model);
void func0001ce64(struct objticksp476 *arg0, struct model *model);
void func0001cebc(struct objticksp476 *arg0, struct model *model);
s16 modelGetAnimNum(struct model *model);
s32 modelIsFlipped(struct model *model);
f32 modelGetCurAnimFrame(struct model *model);
f32 modelGetAnimEndFrame(struct model *model);
s32 modelGetNumAnimFrames(struct model *model);
f32 modelGetAnimSpeed(struct model *model);
f32 modelGetAbsAnimSpeed(struct model *model);
s32 modelConstrainOrWrapAnimFrame(s32 frame, s16 animnum, f32 endframe);
void modelCopyAnimForMerge(struct model *model, f32 merge);
void func0001d62c(struct model *model, s16 animnum, s32 flip, f32 fstartframe, f32 speed, f32 merge);
s32 modelIsAnimMerging(struct model *model);
void modelSetAnimationWithMerge(struct model *model, s16 animnum, u32 flip, f32 startframe, f32 speed, f32 timemerge, s32 domerge);
void modelSetAnimation(struct model *model, s16 animnum, s32 flip, f32 fstartframe, f32 speed, f32 merge);
void modelCopyAnimData(struct model *src, struct model *dst);
void modelSetAnimLooping(struct model *model, f32 loopframe, f32 loopmerge);
void modelSetAnimEndFrame(struct model *model, f32 endframe);
void modelSetAnimFlipFunction(struct model *model, void *callback);
void modelSetAnimSpeed(struct model *model, f32 speed, f32 startframe);
void modelSetAnimSpeedAuto(struct model *model, f32 arg1, f32 startframe);
void modelSetAnimPlaySpeed(struct model *model, f32 speed, f32 frame);
void modelSetAnim70(struct model *model, void *callback);
void func0001e018(struct model *model, f32 startframe);
void func0001e14c(struct model *model, f32 arg1, f32 arg2);
void func0001e29c(s32 value);
s32 func0001e2a8(void);
void func0001e2b4(struct model *model, f32 frame, f32 arg2, f32 frame2, f32 arg4);
void func0001ee18(struct model *model, s32 lvupdate240, s32 arg2);
void func0001f314(struct model *model, s32 lvupdate240, s32 arg2);
void func0001f7e0(struct modelrenderdata *renderdata);
void func0001f890(struct modelrenderdata *renderdata, s32 arg1);
void func00020248(struct modelrenderdata *renderdata, s32 arg1);
void func00020bdc(struct modelrenderdata *renderdata);
void modelApplyCullMode(struct modelrenderdata *renderdata);
void modelRenderNodeGundl(struct modelrenderdata *renderdata, struct model *model, struct modelnode *node);
void modelRenderNodeDl(struct modelrenderdata *renderdata, struct model *model, struct modelnode *node);
void modelRenderNodeType16(struct modelrenderdata *renderdata, struct modelnode *node);
void func000216cc(struct modelrenderdata *renderdata, struct textureconfig *tconfig, s32 arg2);
void modelRenderNodeGunfire(struct modelrenderdata *renderdata, struct model *model, struct modelnode *node);
void modelRender(struct modelrenderdata *renderdata, struct model *model);
s32 func000220fc(union modelrodata *rodata, Mtxf *mtx, struct coord *arg2, struct coord *arg3);
s32 func000225d4(struct model *model, struct coord *arg1, struct coord *arg2, struct modelnode **startnode);
void modelPromoteNodeOffsetsToPointers(struct modelnode *node, u32 vma, u32 fileramaddr);
void modelPromoteOffsetsToPointers(struct modelfiledata *filedata, u32 arg1, u32 arg2);
s32 modelCalculateRwDataIndexes(struct modelnode *node);
void modelCalculateRwDataLen(struct modelfiledata *filedata);
void modelInitRwData(struct model *model, struct modelnode *node);
void modelInit(struct model *model, struct modelfiledata *filedata, union modelrwdata **rwdatas, s32 resetanim);
void animInit(struct anim *anim);
u32 func00023108(void);
void modelIterateDisplayLists(struct modelfiledata *filedata, struct modelnode **nodeptr, Gfx **gdlptr);
void modelNodeReplaceGdl(u32 arg0, struct modelnode *node, Gfx *find, Gfx *replacement);
void rdpInit(void);
void rdpCreateTask(Gfx *gdlstart, Gfx *gdlend, u32 arg2, void *msg);
void resetproc(void *data);
u32 random(void);
void rngSetSeed(u32 seed);
u32 func00012e1c(u64 *value);
void func00001b28(u32 arg0);
void __scHandleRetraceViaPri(OSSched *sc, OSScTask *t);
void func00002078(OSSched *sc);
void __scHandleRSP(OSSched *sc);
void __scHandleRDP(OSSched *sc);
void __scHandleRetrace(OSSched *sc);
void __scAppendList(OSSched *sc, OSScTask *t);
void __scExec(OSSched *sc, OSScTask *sp, OSScTask *dp);
void __scMain(void *arg);
void __scYield(OSSched *sc);
struct bootbufferthing *bbufGetIndex0Buffer(void);
void bbufResetBuffers(void);
void bbufIncIndex0(void);
void bbufIncIndex1(void);
void bbufUpdateIndex2Buffer(void);
void func00002d90(void);
void *segGetDataStart(void);
void *segGetDatazipRomStart(void);
void *segGetInflateRomStart(void);
void *segGetInflateRomStart2(void);
void *segGetGamezipsRomStart(void);
u32 bootInflate();
s32 sndIsFiltered(s32 audioid);
u32 snd0000e9d0(void);
u16 snd0000e9dc(void);
void sndSetSfxVolume(u16 volume);
u32 snd0000ea80(void);
void snd0000eadc(void);
void sndLoadSfxCtl(void);
u32 snd0000ed14(void);
u32 snd0000ed68(void);
u32 snd0000ee88(void);
u32 snd0000efa8(void);
u32 snd0000f0d4(void);
u32 snd0000f228(void);
void sndSetSoundMode(s32 mode);
u32 snd0000f49c(void);
void snd0000f67c(struct var80094ed8 *arg0);
void sndInit(void);
s32 sndIsMp3(u16 soundnum);
s32 snd0000fbc4(s16 arg0);
u32 snd0000fc48(void);
u32 snd0000fd74(void);
void snd0000fd9c(struct var80094ed8 *arg0, u16 volume);
void snd0000fe18(void);
void snd0000fe20(void);
void snd0000fe50(void);
void sndTick(void);
s32 sndIsDisabled(void);
u32 snd0001036c(void);
void sndAdjust(struct audiohandle **handle, s32 arg1, s32 arg2, s32 speakerweight, s16 soundnum, f32 arg5, s32 arg6, s32 arg7, s32 arg8);
struct audiohandle *snd00010718(struct audiohandle **handle, s32 arg1, s32 arg2, s32 arg3, s32 arg4, f32 arg5, s32 arg6, s32 arg7, s32 arg8);
struct audiohandle *sndStart(s32 arg0, s16 sound, struct audiohandle **handle, s32 arg3, s32 arg4, f32 arg5, s32 arg6, s32 arg7);
void snd00010ae4(s32 soundnum, s32 arg1, s32 arg2, s32 arg3);
void sndPlayNosedive(s32 seconds);
void sndStopNosedive(void);
void sndTickNosedive(void);
void sndPlayUfo(s32 seconds);
void sndStopUfo(void);
void sndTickUfo(void);
char *strcpy(char *dst, char *src);
char *strncpy(char *dst, char *s2, s32 len);
char *strcat(char *dst, char *src);
s32 strcmp(char *s1, char *s2);
s32 strncmp(char *s1, char *s2, s32 len);
char toupper(char c);
s32 isdigit(char c);
s32 isalpha(char c);
s32 isspace(char c);
s32 strtol(char *arg0, char **arg1, s32 arg2);
typedef struct
{
 u32 initialized;
 OSThread *mgrThread;
 OSMesgQueue *unk08;
 OSMesgQueue *unk0c;
 OSMesgQueue *unk10;
 s32 (*dma_func)(s32,u32,void*,size_t);
 u64 force_align;
} OSMgrArgs;
typedef struct
{
           __OSInode inode;
             u8 bank;
             u8 map[256];
} __OSInodeCache;
s32 __osDisableInt();
void __osRestoreInt(s32);
s32 __osContRamRead(OSMesgQueue *mq, int channel, u16 address, u8 *buffer);
s32 __osContRamWrite(OSMesgQueue *mq, int channel, u16 address, u8 *buffer, int force);
void __osEnqueueAndYield(OSThread**);
void __osDequeueThread(OSThread**, OSThread*);
void __osEnqueueThread(OSThread**, OSThread*);
OSThread* __osPopThread(OSThread**);
s32 __osSiRawStartDma(s32, void*);
void __osSiCreateAccessQueue();
void __osSiGetAccess();
void __osSiRelAccess();
u32 __osProbeTLB(void*);
void __osPiCreateAccessQueue();
void __osPiGetAccess();
void __osSetSR(u32);
u32 __osGetSR();
void __osSetFpcCsr(u32);
s32 __osSiRawReadIo(u32, u32*);
s32 __osSiRawWriteIo(u32, u32);
s32 osPiRawReadIo(u32 a0, u32 *a1);
void __osSpSetStatus(u32);
u32 __osSpGetStatus();
s32 __osSpSetPc(u32);
s32 __osSpDeviceBusy();
s32 __osSiDeviceBusy();
s32 __osSpRawStartDma(s32 direction, u32 devAddr, void *dramAddr, u32 size);
void __osViInit();
__OSViContext * __osViGetCurrentContext();
void __osViSwapContext();
void __osSetTimerIntr(u64);
u64 __osInsertTimer(OSTimer *);
void __osSetCompare(u32);
s32 __osAiDeviceBusy();
void __osDispatchThread();
u32 __osGetCause();
s32 __osAtomicDec(u32*);
s32 __osPfsSelectBank(OSPfs *pfs, u8 bank);
u16 __osSumCalc(u8 *ptr, int length);
s32 __osIdCheckSum(u16 *ptr, u16 *csum, u16 *icsum);
s32 __osRepairPackId(OSPfs *pfs, __OSPackId *badid, __OSPackId *newid);
s32 __osCheckPackId(OSPfs *pfs, __OSPackId *temp);
s32 __osGetId(OSPfs *pfs);
s32 __osCheckId(OSPfs *pfs);
s32 __osPfsRWInode(OSPfs *pfs, __OSInode *inode, u8 flag, u8 bank);
u8 __osContAddressCrc(u16 inaddr);
u8 __osContDataCrc(u8 *data);
s32 __osPfsReleasePages(OSPfs *pfs, __OSInode *inode, u8 initial_page, u8 bank, __OSInodeUnit *final_page);
enum sfx {
 SFX_0000,
 SFX_LAUNCH_ROCKET,
 SFX_EQUIP_HORIZONSCANNER,
 SFX_0003,
 SFX_0004,
 SFX_0005,
 SFX_0006,
 SFX_0007,
 SFX_0008,
 SFX_0009,
 SFX_000A,
 SFX_000B,
 SFX_000C,
 SFX_ARGH_FEMALE_000D,
 SFX_ARGH_FEMALE_000E,
 SFX_ARGH_FEMALE_000F,
 SFX_BOTTLE_BREAK,
 SFX_0011,
 SFX_0012,
 SFX_0013,
 SFX_0014,
 SFX_0015,
 SFX_0016,
 SFX_0017,
 SFX_0018,
 SFX_0019,
 SFX_001A,
 SFX_001B,
 SFX_001C,
 SFX_001D,
 SFX_001E,
 SFX_001F,
 SFX_0020,
 SFX_0021,
 SFX_0022,
 SFX_0023,
 SFX_0024,
 SFX_0025,
 SFX_0026,
 SFX_0027,
 SFX_0028,
 SFX_0029,
 SFX_002A,
 SFX_MENU_CANCEL,
 SFX_002C,
 SFX_002D,
 SFX_002E,
 SFX_002F,
 SFX_0030,
 SFX_0031,
 SFX_0032,
 SFX_0033,
 SFX_0034,
 SFX_0035,
 SFX_0036,
 SFX_0037,
 SFX_0038,
 SFX_0039,
 SFX_003A,
 SFX_003B,
 SFX_003C,
 SFX_003D,
 SFX_HUDMSG,
 SFX_003F,
 SFX_0040,
 SFX_0041,
 SFX_0042,
 SFX_0043,
 SFX_0044,
 SFX_0045,
 SFX_0046,
 SFX_0047,
 SFX_0048,
 SFX_0049,
 SFX_004A,
 SFX_004B,
 SFX_004C,
 SFX_004D,
 SFX_004E,
 SFX_004F,
 SFX_0050,
 SFX_0051,
 SFX_REGEN,
 SFX_0053,
 SFX_0054,
 SFX_0055,
 SFX_0056,
 SFX_0057,
 SFX_0058,
 SFX_0059,
 SFX_005A,
 SFX_CLOAK_ON,
 SFX_CLOAK_OFF,
 SFX_005D,
 SFX_005E,
 SFX_005F,
 SFX_0060,
 SFX_0061,
 SFX_0062,
 SFX_0063,
 SFX_SHIELD_DAMAGE,
 SFX_LASER_STREAM,
 SFX_0066,
 SFX_0067,
 SFX_0068,
 SFX_0069,
 SFX_006A,
 SFX_006B,
 SFX_006C,
 SFX_006D,
 SFX_006E,
 SFX_006F,
 SFX_0070,
 SFX_0071,
 SFX_0072,
 SFX_0073,
 SFX_0074,
 SFX_0075,
 SFX_0076,
 SFX_0077,
 SFX_0078,
 SFX_0079,
 SFX_007A,
 SFX_007B,
 SFX_007C,
 SFX_007D,
 SFX_007E,
 SFX_007F,
 SFX_0080,
 SFX_0081,
 SFX_0082,
 SFX_0083,
 SFX_0084,
 SFX_0085,
 SFX_ARGH_MALE_0086,
 SFX_ARGH_MALE_0087,
 SFX_ARGH_MALE_0088,
 SFX_ARGH_MALE_0089,
 SFX_ARGH_MALE_008A,
 SFX_ARGH_MALE_008B,
 SFX_ARGH_MALE_008C,
 SFX_ARGH_MALE_008D,
 SFX_ARGH_MALE_008E,
 SFX_ARGH_MALE_008F,
 SFX_ARGH_MALE_0090,
 SFX_ARGH_MALE_0091,
 SFX_ARGH_MALE_0092,
 SFX_ARGH_MALE_0093,
 SFX_ARGH_MALE_0094,
 SFX_ARGH_MALE_0095,
 SFX_ARGH_MALE_0096,
 SFX_ARGH_MALE_0097,
 SFX_ARGH_MALE_0098,
 SFX_ARGH_MALE_0099,
 SFX_ARGH_MALE_009A,
 SFX_ARGH_MALE_009B,
 SFX_ARGH_MALE_009C,
 SFX_ARGH_MALE_009D,
 SFX_ARGH_MALE_009E,
 SFX_009F,
 SFX_00A0,
 SFX_ALARM_AIRBASE,
 SFX_ALARM_2,
 SFX_ALARM_DEFAULT,
 SFX_00A4,
 SFX_00A5,
 SFX_00A6,
 SFX_00A7,
 SFX_00A8,
 SFX_00A9,
 SFX_00AA,
 SFX_00AB,
 SFX_00AC,
 SFX_00AD,
 SFX_00AE,
 SFX_00AF,
 SFX_00B0,
 SFX_00B1,
 SFX_00B2,
 SFX_00B3,
 SFX_00B4,
 SFX_00B5,
 SFX_00B6,
 SFX_00B7,
 SFX_00B8,
 SFX_00B9,
 SFX_PRESS_SWITCH,
 SFX_00BB,
 SFX_00BC,
 SFX_00BD,
 SFX_00BE,
 SFX_00BF,
 SFX_00C0,
 SFX_00C1,
 SFX_00C2,
 SFX_00C3,
 SFX_00C4,
 SFX_00C5,
 SFX_00C6,
 SFX_00C7,
 SFX_00C8,
 SFX_00C9,
 SFX_00CA,
 SFX_00CB,
 SFX_00CC,
 SFX_00CD,
 SFX_00CE,
 SFX_00CF,
 SFX_00D0,
 SFX_00D1,
 SFX_00D2,
 SFX_00D3,
 SFX_00D4,
 SFX_00D5,
 SFX_00D6,
 SFX_00D7,
 SFX_00D8,
 SFX_00D9,
 SFX_00DA,
 SFX_00DB,
 SFX_00DC,
 SFX_00DD,
 SFX_00DE,
 SFX_00DF,
 SFX_00E0,
 SFX_00E1,
 SFX_00E2,
 SFX_00E3,
 SFX_00E4,
 SFX_PICKUP_KEYCARD,
 SFX_00E6,
 SFX_00E7,
 SFX_PICKUP_GUN,
 SFX_PICKUP_KNIFE,
 SFX_PICKUP_AMMO,
 SFX_PICKUP_MINE,
 SFX_00EC,
 SFX_00ED,
 SFX_00EE,
 SFX_00EF,
 SFX_00F0,
 SFX_00F1,
 SFX_PICKUP_LASER,
 SFX_00F3,
 SFX_00F4,
 SFX_BIKE_TAKEOFF,
 SFX_00F6,
 SFX_00F7,
 SFX_00F8,
 SFX_00F9,
 SFX_00FA,
 SFX_00FB,
 SFX_00FC,
 SFX_00FD,
 SFX_00FE,
 SFX_00FF,
 SFX_0100,
 SFX_0101,
 SFX_0102,
 SFX_0103,
 SFX_0104,
 SFX_0105,
 SFX_0106,
 SFX_0107,
 SFX_0108,
 SFX_0109,
 SFX_010A,
 SFX_010B,
 SFX_010C,
 SFX_010D,
 SFX_010E,
 SFX_010F,
 SFX_0110,
 SFX_0111,
 SFX_0112,
 SFX_0113,
 SFX_0114,
 SFX_0115,
 SFX_0116,
 SFX_0117,
 SFX_0118,
 SFX_0119,
 SFX_011A,
 SFX_011B,
 SFX_011C,
 SFX_011D,
 SFX_011E,
 SFX_011F,
 SFX_0120,
 SFX_0121,
 SFX_0122,
 SFX_0123,
 SFX_0124,
 SFX_0125,
 SFX_0126,
 SFX_0127,
 SFX_0128,
 SFX_0129,
 SFX_012A,
 SFX_012B,
 SFX_012C,
 SFX_012D,
 SFX_012E,
 SFX_012F,
 SFX_0130,
 SFX_0131,
 SFX_0132,
 SFX_0133,
 SFX_0134,
 SFX_0135,
 SFX_0136,
 SFX_0137,
 SFX_0138,
 SFX_0139,
 SFX_013A,
 SFX_013B,
 SFX_013C,
 SFX_013D,
 SFX_013E,
 SFX_013F,
 SFX_0140,
 SFX_0141,
 SFX_0142,
 SFX_0143,
 SFX_0144,
 SFX_0145,
 SFX_0146,
 SFX_0147,
 SFX_0148,
 SFX_0149,
 SFX_014A,
 SFX_014B,
 SFX_014C,
 SFX_014D,
 SFX_014E,
 SFX_014F,
 SFX_0150,
 SFX_0151,
 SFX_0152,
 SFX_0153,
 SFX_0154,
 SFX_0155,
 SFX_0156,
 SFX_0157,
 SFX_0158,
 SFX_0159,
 SFX_015A,
 SFX_015B,
 SFX_015C,
 SFX_015D,
 SFX_015E,
 SFX_015F,
 SFX_0160,
 SFX_0161,
 SFX_0162,
 SFX_0163,
 SFX_0164,
 SFX_0165,
 SFX_0166,
 SFX_0167,
 SFX_0168,
 SFX_0169,
 SFX_016A,
 SFX_016B,
 SFX_016C,
 SFX_016D,
 SFX_016E,
 SFX_016F,
 SFX_0170,
 SFX_0171,
 SFX_0172,
 SFX_0173,
 SFX_0174,
 SFX_0175,
 SFX_0176,
 SFX_0177,
 SFX_0178,
 SFX_0179,
 SFX_017A,
 SFX_017B,
 SFX_017C,
 SFX_017D,
 SFX_017E,
 SFX_017F,
 SFX_0180,
 SFX_0181,
 SFX_0182,
 SFX_0183,
 SFX_0184,
 SFX_0185,
 SFX_0186,
 SFX_0187,
 SFX_0188,
 SFX_0189,
 SFX_018A,
 SFX_018B,
 SFX_018C,
 SFX_018D,
 SFX_018E,
 SFX_018F,
 SFX_0190,
 SFX_0191,
 SFX_0192,
 SFX_0193,
 SFX_0194,
 SFX_0195,
 SFX_0196,
 SFX_0197,
 SFX_0198,
 SFX_0199,
 SFX_019A,
 SFX_019B,
 SFX_019C,
 SFX_019D,
 SFX_019E,
 SFX_019F,
 SFX_01A0,
 SFX_01A1,
 SFX_01A2,
 SFX_01A3,
 SFX_01A4,
 SFX_01A5,
 SFX_01A6,
 SFX_01A7,
 SFX_01A8,
 SFX_01A9,
 SFX_01AA,
 SFX_01AB,
 SFX_01AC,
 SFX_01AD,
 SFX_01AE,
 SFX_01AF,
 SFX_01B0,
 SFX_01B1,
 SFX_01B2,
 SFX_01B3,
 SFX_BIKE_ENGINE,
 SFX_01B5,
 SFX_01B6,
 SFX_01B7,
 SFX_01B8,
 SFX_01B9,
 SFX_01BA,
 SFX_01BB,
 SFX_01BC,
 SFX_EYESPY_RUNNING,
 SFX_01BE,
 SFX_01BF,
 SFX_01C0,
 SFX_01C1,
 SFX_01C2,
 SFX_01C3,
 SFX_01C4,
 SFX_01C5,
 SFX_01C6,
 SFX_01C7,
 SFX_SLAYER_BEEP,
 SFX_01C9,
 SFX_01CA,
 SFX_01CB,
 SFX_01CC,
 SFX_PICKUP_SHIELD,
 SFX_01CE,
 SFX_01CF,
 SFX_01D0,
 SFX_01D1,
 SFX_01D2,
 SFX_01D3,
 SFX_01D4,
 SFX_01D5,
 SFX_01D6,
 SFX_01D7,
 SFX_01D8,
 SFX_01D9,
 SFX_01DA,
 SFX_01DB,
 SFX_01DC,
 SFX_01DD,
 SFX_01DE,
 SFX_01DF,
 SFX_01E0,
 SFX_01E1,
 SFX_01E2,
 SFX_01E3,
 SFX_01E4,
 SFX_01E5,
 SFX_01E6,
 SFX_01E7,
 SFX_01E8,
 SFX_01E9,
 SFX_01EA,
 SFX_01EB,
 SFX_01EC,
 SFX_01ED,
 SFX_01EE,
 SFX_01EF,
 SFX_01F0,
 SFX_01F1,
 SFX_01F2,
 SFX_01F3,
 SFX_01F4,
 SFX_01F5,
 SFX_01F6,
 SFX_01F7,
 SFX_01F8,
 SFX_01F9,
 SFX_01FA,
 SFX_01FB,
 SFX_01FC,
 SFX_01FD,
 SFX_01FE,
 SFX_01FF,
 SFX_0200,
 SFX_0201,
 SFX_0202,
 SFX_0203,
 SFX_0204,
 SFX_0205,
 SFX_0206,
 SFX_0207,
 SFX_0208,
 SFX_0209,
 SFX_020A,
 SFX_020B,
 SFX_020C,
 SFX_020D,
 SFX_020E,
 SFX_020F,
 SFX_0210,
 SFX_0211,
 SFX_0212,
 SFX_0213,
 SFX_0214,
 SFX_0215,
 SFX_0216,
 SFX_0217,
 SFX_0218,
 SFX_0219,
 SFX_021A,
 SFX_021B,
 SFX_021C,
 SFX_021D,
 SFX_021E,
 SFX_021F,
 SFX_0220,
 SFX_0221,
 SFX_0222,
 SFX_0223,
 SFX_0224,
 SFX_0225,
 SFX_0226,
 SFX_0227,
 SFX_0228,
 SFX_0229,
 SFX_022A,
 SFX_022B,
 SFX_022C,
 SFX_022D,
 SFX_022E,
 SFX_022F,
 SFX_0230,
 SFX_0231,
 SFX_0232,
 SFX_0233,
 SFX_0234,
 SFX_0235,
 SFX_0236,
 SFX_0237,
 SFX_0238,
 SFX_0239,
 SFX_023A,
 SFX_023B,
 SFX_023C,
 SFX_DRCAROLL_COME_ON,
 SFX_DRCAROLL_TAKING_YOUR_TIME,
 SFX_DRCAROLL_WHAT,
 SFX_ARGH_DRCAROLL_0240,
 SFX_0241,
 SFX_0242,
 SFX_DRCAROLL_OH_CRIKEY,
 SFX_DRCAROLL_GOODNESS_GRACIOUS,
 SFX_DRCAROLL_DONT_THEY_KNOW,
 SFX_DRCAROLL_STOP_THAT,
 SFX_DRCAROLL_GET_OUT_OF_HERE,
 SFX_DRCAROLL_KNOW_WHAT_YOURE_DOING,
 SFX_DRCAROLL_0249,
 SFX_024A,
 SFX_DRCAROLL_GOING_TO_THE_HELIPAD,
 SFX_ARGH_DRCAROLL_024C,
 SFX_DRCAROLL_SYSTEMS_FAILURE,
 SFX_DRCAROLL_YOU_GO_ON,
 SFX_DRCAROLL_I_CANT_MAKE_IT,
 SFX_ARGH_DRCAROLL_0250,
 SFX_ARGH_DRCAROLL_0251,
 SFX_DRCAROLL_QUITE_ENOUGH,
 SFX_0253,
 SFX_0254,
 SFX_0255,
 SFX_DRCAROLL_IM_DYING,
 SFX_DRCAROLL_GOODBYE,
 SFX_DRCAROLL_YOU_WERE_SUPPOSED,
 SFX_ARGH_DRCAROLL_0259,
 SFX_ARGH_DRCAROLL_025A,
 SFX_M0_WHAT_THE,
 SFX_M0_WHO_THE,
 SFX_025D,
 SFX_M0_MEDIC,
 SFX_M0_OW,
 SFX_M0_YOU_SHOT_ME,
 SFX_M0_IM_HIT,
 SFX_M0_IM_TAKING_FIRE,
 SFX_M0_TAKING_DAMAGE,
 SFX_M0_HEY_YOU,
 SFX_M0_INTRUDER_ALERT,
 SFX_M0_GOT_A_CONTACT,
 SFX_0267,
 SFX_0268,
 SFX_0269,
 SFX_026A,
 SFX_M0_HEAR_THAT,
 SFX_M0_WHATS_THAT_NOISE,
 SFX_M0_HEARD_A_NOISE,
 SFX_M0_ARE_YOU_OKAY,
 SFX_M0_GOT_A_MAN_DOWN,
 SFX_M0_HES_BOUGHT_IT,
 SFX_0271,
 SFX_M0_INTRUDER_ALERT2,
 SFX_M0_WEVE_GOT_TROUBLE,
 SFX_M0_WEVE_GOT_PROBLEMS,
 SFX_M0_I_SEE_HER,
 SFX_0276,
 SFX_M0_THERE_SHE_IS,
 SFX_M0_THERE_MOVEMENT,
 SFX_0279,
 SFX_027A,
 SFX_M0_CLEAR_SHOT,
 SFX_M0_SHES_MINE,
 SFX_M0_OPEN_FIRE,
 SFX_M0_WIPE_HER_OUT,
 SFX_M0_WASTE_HER,
 SFX_M0_GIVE_IT_UP,
 SFX_M0_SURRENDER_NOW,
 SFX_M0_TAKE_THAT,
 SFX_0283,
 SFX_0284,
 SFX_0285,
 SFX_0286,
 SFX_M0_HOW_DID_I_MISS,
 SFX_0288,
 SFX_M0_SHES_A_TRICKY_ONE,
 SFX_M0_COVER_ME,
 SFX_M0_WATCH_MY_BACK,
 SFX_M0_TAKE_COVER,
 SFX_M0_TAKE_COVER_028D,
 SFX_M0_ILL_COVER_YOU,
 SFX_M0_GET_DOWN,
 SFX_M0_GO_TO_PLAN_B,
 SFX_0291,
 SFX_M0_RETREAT,
 SFX_M0_CATCH,
 SFX_M0_EVERYBODY_DOWN,
 SFX_0295,
 SFX_0296,
 SFX_M0_FALL_BACK,
 SFX_M0_EVERYONE_BACK_OFF,
 SFX_M0_WITHDRAW,
 SFX_M0_FLANK_THE_TARGET,
 SFX_M0_LETS_SPLIT_UP,
 SFX_M0_SURROUND_HER,
 SFX_M0_GRENADE,
 SFX_029E,
 SFX_029F,
 SFX_02A0,
 SFX_02A1,
 SFX_M0_SHE_GOT_ME,
 SFX_M0_GRAB_A_BODY_BAG,
 SFX_M0_ONE_FOR_THE_MORGUE,
 SFX_M0_REST_IN_PEACE,
 SFX_02A6,
 SFX_M0_DONT_SHOOT_ME,
 SFX_M0_I_GIVE_UP,
 SFX_M0_YOU_WIN_I_SURRENDER,
 SFX_ARGH_JO_02AA,
 SFX_ARGH_JO_02AB,
 SFX_ARGH_JO_02AC,
 SFX_ARGH_JO_02AD,
 SFX_ARGH_JO_02AE,
 SFX_ARGH_JO_02AF,
 SFX_ARGH_JO_02B0,
 SFX_ARGH_JO_02B1,
 SFX_ARGH_JO_02B2,
 SFX_ARGH_JO_02B3,
 SFX_02B4,
 SFX_02B5,
 SFX_02B6,
 SFX_02B7,
 SFX_02B8,
 SFX_02B9,
 SFX_02BA,
 SFX_02BB,
 SFX_02BC,
 SFX_02BD,
 SFX_02BE,
 SFX_02BF,
 SFX_02C0,
 SFX_02C1,
 SFX_02C2,
 SFX_02C3,
 SFX_02C4,
 SFX_02C5,
 SFX_02C6,
 SFX_02C7,
 SFX_02C8,
 SFX_02C9,
 SFX_02CA,
 SFX_02CB,
 SFX_02CC,
 SFX_02CD,
 SFX_02CE,
 SFX_02CF,
 SFX_02D0,
 SFX_02D1,
 SFX_02D2,
 SFX_02D3,
 SFX_02D4,
 SFX_02D5,
 SFX_02D6,
 SFX_02D7,
 SFX_02D8,
 SFX_02D9,
 SFX_02DA,
 SFX_02DB,
 SFX_02DC,
 SFX_02DD,
 SFX_02DE,
 SFX_02DF,
 SFX_02E0,
 SFX_02E1,
 SFX_02E2,
 SFX_02E3,
 SFX_02E4,
 SFX_02E5,
 SFX_02E6,
 SFX_02E7,
 SFX_02E8,
 SFX_02E9,
 SFX_02EA,
 SFX_02EB,
 SFX_02EC,
 SFX_02ED,
 SFX_02EE,
 SFX_02EF,
 SFX_02F0,
 SFX_02F1,
 SFX_02F2,
 SFX_02F3,
 SFX_02F4,
 SFX_02F5,
 SFX_02F6,
 SFX_02F7,
 SFX_02F8,
 SFX_02F9,
 SFX_02FA,
 SFX_02FB,
 SFX_02FC,
 SFX_02FD,
 SFX_02FE,
 SFX_M1_WHA,
 SFX_0300,
 SFX_M1_IM_HIT_IM_HIT,
 SFX_M1_IM_BLEEDING,
 SFX_0303,
 SFX_M1_HELP_ME_OUT,
 SFX_M1_IM_IN_TROUBLE,
 SFX_M1_COME_HERE,
 SFX_M1_THERES_SOMEONE_HERE,
 SFX_M1_GET_HER,
 SFX_M1_WHOA,
 SFX_030A,
 SFX_M1_IS_THAT_A_BULLET,
 SFX_030C,
 SFX_030D,
 SFX_M1_THAT_SOUNDED_LIKE,
 SFX_M1_GUNFIRE,
 SFX_M1_SOMEONES_SHOOTING,
 SFX_0311,
 SFX_0312,
 SFX_0313,
 SFX_0314,
 SFX_0315,
 SFX_M1_HES_GONE,
 SFX_0317,
 SFX_0318,
 SFX_0319,
 SFX_031A,
 SFX_031B,
 SFX_M1_M2_LOOK_OUT_SHES_COMING,
 SFX_M1_M2_TAKE_COVER,
 SFX_M1_M2_LOOK_OUT_LOOK_OUT,
 SFX_M1_OVER_THERE,
 SFX_M1_HALT,
 SFX_M1_FREEZE,
 SFX_M1_LAST_MISTAKE,
 SFX_M1_WHAT_ARE_YOU_WAITING_FOR,
 SFX_M1_BRING_IT_ON,
 SFX_M1_TAKE_HER_DOWN,
 SFX_M1_EVERYBODY_GET_HER,
 SFX_M1_ATTACK,
 SFX_0328,
 SFX_M1_YEAH_BABY,
 SFX_032A,
 SFX_032B,
 SFX_032C,
 SFX_032D,
 SFX_032E,
 SFX_M1_MY_GUN_ITS_USELESS,
 SFX_0330,
 SFX_M1_STOP_DODGING,
 SFX_M1_SOMEONE_HIT_HER,
 SFX_0333,
 SFX_0334,
 SFX_M1_COVER_ME_NOW,
 SFX_M1_IM_GOING_FOR_COVER,
 SFX_M1_GO_FOR_IT,
 SFX_M1_GO_GO_GO,
 SFX_M1_RUN,
 SFX_M1_SHES_TOO_GOOD_RUN,
 SFX_M1_GET_SOME_BACKUP,
 SFX_M1_EVACUATE_THE_AREA,
 SFX_M1_CATCH_THIS,
 SFX_033E,
 SFX_M1_HERE_KEEP_IT,
 SFX_0340,
 SFX_0341,
 SFX_M1_GRENADE,
 SFX_M1_WITHDRAW,
 SFX_M1_FALL_BACK,
 SFX_M1_EVERYONE_GET_BACK,
 SFX_M1_SURROUND_HER,
 SFX_M1_SPREAD_OUT,
 SFX_M1_SPLIT_UP,
 SFX_M1_PLEASE_DONT,
 SFX_M1_DONT_SHOOT,
 SFX_M1_IM_ONLY_DOING_MY_JOB,
 SFX_034C,
 SFX_M1_WHY_ME,
 SFX_M1_CHOKING,
 SFX_034F,
 SFX_0350,
 SFX_0351,
 SFX_0352,
 SFX_0353,
 SFX_0354,
 SFX_M1_OUTSTANDING,
 SFX_M1_IM_JUST_TOO_GOOD,
 SFX_M1_YEEHAH_GOT_ONE,
 SFX_0358,
 SFX_0359,
 SFX_M1_ANOTHER_ONE_BITES_THE_DUST,
 SFX_M0_M1_LOOK_OUT_LOOK_OUT,
 SFX_M0_M1_ITS_A_GRENADE,
 SFX_M0_M1_CLEAR_THE_AREA,
 SFX_035E,
 SFX_035F,
 SFX_0360,
 SFX_0361,
 SFX_0362,
 SFX_0363,
 SFX_0364,
 SFX_0365,
 SFX_0366,
 SFX_0367,
 SFX_0368,
 SFX_0369,
 SFX_036A,
 SFX_036B,
 SFX_036C,
 SFX_036D,
 SFX_036E,
 SFX_036F,
 SFX_0370,
 SFX_0371,
 SFX_0372,
 SFX_0373,
 SFX_0374,
 SFX_0375,
 SFX_0376,
 SFX_0377,
 SFX_0378,
 SFX_0379,
 SFX_037A,
 SFX_F_HEY,
 SFX_F_HUH,
 SFX_037D,
 SFX_F_IM_WOUNDED,
 SFX_F_HELP_ME_OUT,
 SFX_F_IM_IN_TROUBLE,
 SFX_F_GET_HER,
 SFX_F_HEY_YOU_COME_HERE,
 SFX_0383,
 SFX_F_TARGET_ATTACKING,
 SFX_F_UNDER_FIRE,
 SFX_0386,
 SFX_F_WERE_UNDER_FIRE,
 SFX_0388,
 SFX_F_0389,
 SFX_F_SOMEONES_SHOOTING,
 SFX_038B,
 SFX_038C,
 SFX_F_UNIT_DOWN,
 SFX_038E,
 SFX_038F,
 SFX_0390,
 SFX_0391,
 SFX_0392,
 SFX_0393,
 SFX_0394,
 SFX_0395,
 SFX_F_COME_ON,
 SFX_0397,
 SFX_F_EVERYONE_GET_HER,
 SFX_F_ATTACK,
 SFX_F_DID_THAT_HURT,
 SFX_F_YOU_WANT_SOME_MORE,
 SFX_039C,
 SFX_039D,
 SFX_039E,
 SFX_F_THIS_GUNS_USELESS,
 SFX_03A0,
 SFX_F_STAND_STILL,
 SFX_F_SOMEONE_HIT_HER,
 SFX_03A3,
 SFX_F_COVER_ME,
 SFX_03A5,
 SFX_F_TAKE_COVER,
 SFX_F_GO_FOR_IT,
 SFX_03A8,
 SFX_F_RUN,
 SFX_F_GET_REINFORCEMENTS,
 SFX_F_EVACUATE_THE_AREA,
 SFX_F_RETREAT,
 SFX_F_CATCH_THIS,
 SFX_F_TIME_TO_DIE,
 SFX_03AF,
 SFX_F_WITHDRAW,
 SFX_F_FALL_BACK,
 SFX_03B2,
 SFX_F_SPREAD_OUT,
 SFX_F_SPLIT_UP,
 SFX_F_PLEASE_DONT,
 SFX_F_DONT_SHOOT,
 SFX_F_WHY_ME,
 SFX_F_NOO,
 SFX_03B9,
 SFX_03BA,
 SFX_03BB,
 SFX_F_GET_A_CLEANER,
 SFX_03BD,
 SFX_F_IM_JUST_TOO_GOOD,
 SFX_03BF,
 SFX_F_SUCH_A_WASTE,
 SFX_F_LOOK_OUT,
 SFX_F_ITS_A_GRENADE,
 SFX_03C3,
 SFX_M2_HOW_THE,
 SFX_M2_HEY,
 SFX_M2_STOP,
 SFX_03C7,
 SFX_M2_WHY_YOU,
 SFX_03C9,
 SFX_03CA,
 SFX_03CB,
 SFX_03CC,
 SFX_M2_IM_INJURED,
 SFX_M2_IM_HIT_IM_HIT,
 SFX_03CF,
 SFX_M2_TARGET_SIGHTED,
 SFX_M2_COME_ON_MAN,
 SFX_03D2,
 SFX_M2_THAT_WAS_CLOSE,
 SFX_03D4,
 SFX_M2_AY_CARAMBA,
 SFX_M2_LISTEN_GUNSHOTS,
 SFX_M2_SOMEONES_NEARBY,
 SFX_03D8,
 SFX_M2_BODY_COUNTS_TOO_HIGH,
 SFX_M2_I_NEVER_LIKED_HIM_ANYWAY,
 SFX_M2_THAT_WAS_MY_BEST_FRIEND,
 SFX_03DC,
 SFX_03DD,
 SFX_03DE,
 SFX_03DF,
 SFX_03E0,
 SFX_M2_WATCH_OUT,
 SFX_M2_HELP_ME_OUT,
 SFX_M2_WEVE_GOT_AN_INTRUDER,
 SFX_M2_GET_HER,
 SFX_M2_THERE_ATTACK,
 SFX_M2_HEY_YOU_STOP,
 SFX_M2_COME_ON_MAN2,
 SFX_M2_DIE,
 SFX_M2_TAKE_THIS,
 SFX_M2_MOVE_IN,
 SFX_M2_YOURE_OUT_OF_YOUR_LEAGUE,
 SFX_M2_LET_HER_HAVE_IT,
 SFX_M2_SURRENDER_OR_DIE,
 SFX_M2_I_HAVE_YOU_NOW,
 SFX_M2_YOU_WANT_BEAT_ME,
 SFX_03F0,
 SFX_03F1,
 SFX_03F2,
 SFX_03F3,
 SFX_M2_I_DONT_BELIEVE_IT,
 SFX_03F5,
 SFX_03F6,
 SFX_M2_STOP_MOVING,
 SFX_M2_NO_ESCAPE_FOR_YOU,
 SFX_M2_HELP_ME_OUT_HERE,
 SFX_M2_HEY_DISTRACT_HER,
 SFX_M2_KEEP_HER_OCCUPIED,
 SFX_M2_MOVE_IT_MOVE_IT,
 SFX_M2_GET_TO_COVER_NOW,
 SFX_M2_RUN_FOR_IT,
 SFX_M2_RETREAT,
 SFX_0400,
 SFX_M2_GET_BACK_GET_BACK,
 SFX_0402,
 SFX_M2_FIRE_IN_THE_HOLE,
 SFX_0404,
 SFX_M2_HERES_A_LITTLE_PRESENT_FOR_YA,
 SFX_0406,
 SFX_M2_TRY_THIS_FOR_SIZE,
 SFX_M2_GET_OUT_OF_THE_WAY,
 SFX_M2_FALL_BACK,
 SFX_M2_MOVE_OUT,
 SFX_M2_TEAM_UP_GUYS,
 SFX_M2_COME_ON_AROUND_THE_SIDE,
 SFX_M2_SCATTER,
 SFX_M2_I_DONT_LIKE_THIS_ANY_MORE,
 SFX_M2_DONT_HURT_ME,
 SFX_M2_YOU_WIN_I_GIVE_UP,
 SFX_0411,
 SFX_0412,
 SFX_0413,
 SFX_0414,
 SFX_M2_I_DONT_WANT_TO_DIE,
 SFX_0416,
 SFX_M2_ITS_ALL_OVER_FOR_THIS_ONE,
 SFX_0418,
 SFX_0419,
 SFX_041A,
 SFX_M2_IM_THE_MAN,
 SFX_M2_BOY_THAT_WAS_CLOSE,
 SFX_M2_DID_YOU_SEE_THAT,
 SFX_041E,
 SFX_041F,
 SFX_M2_GET_BACK_QUICK,
 SFX_M2_WERE_GONNA_DIE,
 SFX_0422,
 SFX_0423,
 SFX_0424,
 SFX_0425,
 SFX_0426,
 SFX_0427,
 SFX_0428,
 SFX_0429,
 SFX_042A,
 SFX_DOOR_042B,
 SFX_DOOR_042C,
 SFX_042D,
 SFX_DISGUISE_ON,
 SFX_042F,
 SFX_0430,
 SFX_0431,
 SFX_0432,
 SFX_RELOAD_FARSIGHT,
 SFX_0434,
 SFX_0435,
 SFX_0436,
 SFX_0437,
 SFX_0438,
 SFX_0439,
 SFX_043A,
 SFX_043B,
 SFX_043C,
 SFX_043D,
 SFX_MENU_SUBFOCUS,
 SFX_043F,
 SFX_0440,
 SFX_MENU_FOCUS,
 SFX_0442,
 SFX_0443,
 SFX_CIV_THERES_A_MANIAC,
 SFX_0445,
 SFX_CIV_GREETINGS_CITIZEN,
 SFX_CIV_HOWS_IT_GOING,
 SFX_CIV_GUNS_DONT_SCARE_ME,
 SFX_CIV_KEEP_AWAY_FROM_THIS_CAR,
 SFX_044A,
 SFX_FBI_WE_HAVE_AN_INTRUDER,
 SFX_044C,
 SFX_044D,
 SFX_044E,
 SFX_044F,
 SFX_0450,
 SFX_0451,
 SFX_0452,
 SFX_0453,
 SFX_0454,
 SFX_0455,
 SFX_0456,
 SFX_0457,
 SFX_0458,
 SFX_FBI_CODE_2_SITUATION,
 SFX_FBI_REQUEST_BACKUP_IMMEDIATELY,
 SFX_045B,
 SFX_045C,
 SFX_CIV_TAKE_IT_EASY,
 SFX_CIV_I_DONT_WANT_ANY_TROUBLE,
 SFX_CIV_QUICK_DOWN_THERE,
 SFX_0460,
 SFX_CIV_HEY_SUGAR_WANNA_PARTY,
 SFX_0462,
 SFX_CIV_TAKE_THE_WALLET,
 SFX_0464,
 SFX_CIV_HEY_BABY,
 SFX_CIV_WHISTLE,
 SFX_0467,
 SFX_CIV_GET_ME_OUT_OF_HERE,
 SFX_0469,
 SFX_046A,
 SFX_046B,
 SFX_046C,
 SFX_046D,
 SFX_046E,
 SFX_JO_LANDING_046F,
 SFX_0470,
 SFX_0471,
 SFX_0472,
 SFX_0473,
 SFX_0474,
 SFX_0475,
 SFX_0476,
 SFX_0477,
 SFX_0478,
 SFX_0479,
 SFX_047A,
 SFX_047B,
 SFX_047C,
 SFX_047D,
 SFX_047E,
 SFX_047F,
 SFX_0480,
 SFX_0481,
 SFX_0482,
 SFX_0483,
 SFX_0484,
 SFX_0485,
 SFX_0486,
 SFX_0487,
 SFX_0488,
 SFX_0489,
 SFX_048A,
 SFX_048B,
 SFX_048C,
 SFX_048D,
 SFX_048E,
 SFX_048F,
 SFX_0490,
 SFX_0491,
 SFX_0492,
 SFX_0493,
 SFX_0494,
 SFX_0495,
 SFX_0496,
 SFX_0497,
 SFX_0498,
 SFX_0499,
 SFX_049A,
 SFX_049B,
 SFX_049C,
 SFX_049D,
 SFX_049E,
 SFX_049F,
 SFX_04A0,
 SFX_04A1,
 SFX_04A2,
 SFX_04A3,
 SFX_04A4,
 SFX_04A5,
 SFX_04A6,
 SFX_04A7,
 SFX_04A8,
 SFX_04A9,
 SFX_04AA,
 SFX_04AB,
 SFX_ALARM_INFILTRATION,
 SFX_04AD,
 SFX_04AE,
 SFX_04AF,
 SFX_04B0,
 SFX_04B1,
 SFX_04B2,
 SFX_04B3,
 SFX_04B4,
 SFX_04B5,
 SFX_04B6,
 SFX_04B7,
 SFX_04B8,
 SFX_04B9,
 SFX_04BA,
 SFX_04BB,
 SFX_04BC,
 SFX_04BD,
 SFX_04BE,
 SFX_04BF,
 SFX_04C0,
 SFX_04C1,
 SFX_04C2,
 SFX_04C3,
 SFX_04C4,
 SFX_04C5,
 SFX_M0_MY_GUN,
 SFX_M0_TRIGGER_THE_ALARM,
 SFX_04C8,
 SFX_M0_HELLO_THERE,
 SFX_M0_WHATS_THIS,
 SFX_M0_IM_SURE_I_HEARD_A_NOISE,
 SFX_M0_HEARING_THINGS,
 SFX_04CD,
 SFX_M1_WARN_THE_OTHERS,
 SFX_M1_WHAT_IS_IT,
 SFX_M1_HOW_DID_THAT_GET_HERE,
 SFX_M1_DONT_TOUCH_IT,
 SFX_M1_I_CANT_SEE_ANYBODY,
 SFX_M1_THERES_NO_ONE_HERE,
 SFX_M2_ACTIVATE_THE_ALARM,
 SFX_M2_IS_IT_DANGEROUS,
 SFX_M2_DONT_MOVE,
 SFX_M2_STAY_BACK,
 SFX_M2_I_BET_THIS_IS_ANOTHER_DRILL,
 SFX_M2_ANOTHER_FALSE_ALARM,
 SFX_04DA,
 SFX_04DB,
 SFX_04DC,
 SFX_04DD,
 SFX_04DE,
 SFX_04DF,
 SFX_04E0,
 SFX_04E1,
 SFX_04E2,
 SFX_04E3,
 SFX_04E4,
 SFX_04E5,
 SFX_04E6,
 SFX_04E7,
 SFX_04E8,
 SFX_04E9,
 SFX_04EA,
 SFX_04EB,
 SFX_04EC,
 SFX_04ED,
 SFX_04EE,
 SFX_04EF,
 SFX_04F0,
 SFX_04F1,
 SFX_04F2,
 SFX_04F3,
 SFX_04F4,
 SFX_04F5,
 SFX_04F6,
 SFX_04F7,
 SFX_04F8,
 SFX_04F9,
 SFX_04FA,
 SFX_RELOAD_04FB,
 SFX_04FC,
 SFX_04FD,
 SFX_04FE,
 SFX_CAMSPY_SHUTTER,
 SFX_0500,
 SFX_0501,
 SFX_0502,
 SFX_0503,
 SFX_0504,
 SFX_0505,
 SFX_0506,
 SFX_0507,
 SFX_0508,
 SFX_0509,
 SFX_050A,
 SFX_050B,
 SFX_050C,
 SFX_050D,
 SFX_050E,
 SFX_050F,
 SFX_0510,
 SFX_0511,
 SFX_0512,
 SFX_0513,
 SFX_0514,
 SFX_SECURE_THE_PERIMETER,
 SFX_0516,
 SFX_0517,
 SFX_0518,
 SFX_0519,
 SFX_051A,
 SFX_ELVIS_INTERGALACTIC_PEACE,
 SFX_ELVIS_EAT_HOT_LEAD_WEIRDOS,
 SFX_ELVIS_KISS_MY_ALIEN_BUTT,
 SFX_ELVIS_ILL_KICK_YOUR_ASS,
 SFX_ELVIS_FOR_YOUR_OWN_GOOD,
 SFX_ELVIS_YOU_DARE_SHOOT_AT_ME,
 SFX_ELVIS_DONT_MESS_WITH_THE_MAIAN,
 SFX_ELVIS_IM_BAD,
 SFX_ELVIS_ALL_GOING_WRONG,
 SFX_ELVIS_WATCH_THE_SUIT,
 SFX_ELVIS_HEHE,
 SFX_0526,
 SFX_0527,
 SFX_0528,
 SFX_SKEDAR_ROAR_0529,
 SFX_SKEDAR_ROAR_052A,
 SFX_SKEDAR_ROAR_052B,
 SFX_052C,
 SFX_SKEDAR_ROAR_052D,
 SFX_SKEDAR_ROAR_052E,
 SFX_SKEDAR_ROAR_052F,
 SFX_SKEDAR_ROAR_0530,
 SFX_SKEDAR_ROAR_0531,
 SFX_SKEDAR_ROAR_0532,
 SFX_SKEDAR_ROAR_0533,
 SFX_SKEDAR_ROAR_0534,
 SFX_0535,
 SFX_SKEDAR_ROAR_0536,
 SFX_SKEDAR_ROAR_0537,
 SFX_SKEDAR_ROAR_0538,
 SFX_SKEDAR_ROAR_0539,
 SFX_SKEDAR_ROAR_053A,
 SFX_053B,
 SFX_053C,
 SFX_053D,
 SFX_053E,
 SFX_053F,
 SFX_0540,
 SFX_0541,
 SFX_0542,
 SFX_0543,
 SFX_0544,
 SFX_0545,
 SFX_0546,
 SFX_0547,
 SFX_0548,
 SFX_0549,
 SFX_054A,
 SFX_054B,
 SFX_054C,
 SFX_054D,
 SFX_054E,
 SFX_054F,
 SFX_0550,
 SFX_0551,
 SFX_0552,
 SFX_0553,
 SFX_0554,
 SFX_0555,
 SFX_0556,
 SFX_0557,
 SFX_0558,
 SFX_0559,
 SFX_055A,
 SFX_055B,
 SFX_055C,
 SFX_055D,
 SFX_055E,
 SFX_055F,
 SFX_0560,
 SFX_0561,
 SFX_0562,
 SFX_0563,
 SFX_0564,
 SFX_0565,
 SFX_0566,
 SFX_0567,
 SFX_0568,
 SFX_0569,
 SFX_056A,
 SFX_056B,
 SFX_056C,
 SFX_056D,
 SFX_056E,
 SFX_056F,
 SFX_0570,
 SFX_0571,
 SFX_0572,
 SFX_0573,
 SFX_0574,
 SFX_0575,
 SFX_0576,
 SFX_0577,
 SFX_0578,
 SFX_0579,
 SFX_057A,
 SFX_057B,
 SFX_057C,
 SFX_057D,
 SFX_057E,
 SFX_057F,
 SFX_0580,
 SFX_0581,
 SFX_0582,
 SFX_0583,
 SFX_0584,
 SFX_0585,
 SFX_0586,
 SFX_0587,
 SFX_0588,
 SFX_0589,
 SFX_058A,
 SFX_058B,
 SFX_058C,
 SFX_058D,
 SFX_058E,
 SFX_058F,
 SFX_0590,
 SFX_0591,
 SFX_0592,
 SFX_0593,
 SFX_0594,
 SFX_0595,
 SFX_0596,
 SFX_0597,
 SFX_0598,
 SFX_0599,
 SFX_059A,
 SFX_059B,
 SFX_059C,
 SFX_INFIL_STATIC_SHORT,
 SFX_INFIL_STATIC_MEDIUM,
 SFX_INFIL_STATIC_LONG,
 SFX_05A0,
 SFX_05A1,
 SFX_05A2,
 SFX_05A3,
 SFX_05A4,
 SFX_05A5,
 SFX_05A6,
 SFX_05A7,
 SFX_05A8,
 SFX_05A9,
 SFX_05AA,
 SFX_COUGH_05AB,
 SFX_COUGH_05AC,
 SFX_COUGH_05AD,
 SFX_COUGH_05AE,
 SFX_COUGH_04AF,
 SFX_COUGH_04B0,
 SFX_GURGLE_05B1,
 SFX_GURGLE_05B2,
 SFX_05B3,
 SFX_05B4,
 SFX_05B5,
 SFX_JO_LANDING_05B6,
 SFX_JO_LANDING_05B7,
 SFX_MP_SCOREPOINT,
 SFX_05B9,
 SFX_05BA,
 SFX_MENU_SWIPE,
 SFX_MENU_OPENDIALOG,
 SFX_05BD,
 SFX_SHOULD_HAVE_COME_HERE_GIRL,
 SFX_WERE_TAKING_OVER,
 SFX_05C0,
 SFX_05C1,
 SFX_ALARM_ATTACKSHIP,
 SFX_05C3,
 SFX_05C4,
 SFX_05C5,
 SFX_05C6,
 SFX_05C7,
 SFX_HEARTBEAT,
 SFX_JO_BOOST_ACTIVATE,
 SFX_05CA,
 SFX_05CB,
 SFX_05CC,
 SFX_05CD,
 SFX_05CE,
 SFX_05CF,
 SFX_05D0,
 SFX_05D1,
 SFX_05D2,
 SFX_05D3,
 SFX_FR_ALARM,
 SFX_FR_LIGHTSON,
 SFX_FR_LIGHTSOFF,
 SFX_05D7,
 SFX_WOOD_BREAK,
 SFX_FR_CONVEYER,
 SFX_FR_CONVEYER_STOP,
 SFX_TRAINING_FAIL,
 SFX_TRAINING_COMPLETE,
 SFX_MENU_SELECT,
 SFX_05DE,
 SFX_ARGH_MAIAN_05DF,
 SFX_ARGH_MAIAN_05E0,
 SFX_ARGH_MAIAN_05E1,
 SFX_MAIAN_05E2,
 SFX_MAIAN_05E3,
 SFX_MAIAN_05E4,
 SFX_MAIAN_05E5,
 SFX_MAIAN_05E6,
 SFX_MAIAN_05E7,
 SFX_05E8,
 SFX_05E9,
 SFX_05EA,
 SFX_05EB,
 SFX_05EC,
 SFX_05ED,
 SFX_05EE,
 SFX_05EF,
 SFX_05F0,
 SFX_05F1,
 SFX_05F2,
 SFX_05F3,
 SFX_05F4,
 SFX_05F5,
 SFX_05F6,
 SFX_05F7,
 SFX_05F8,
 SFX_05F9,
 SFX_05FA,
 SFX_05FB,
 SFX_05FC,
 SFX_05FD,
 SFX_05FE,
 SFX_05FF,
 SFX_0600,
 SFX_0601,
 SFX_0602,
 SFX_0603,
 SFX_0604,
 SFX_0605,
 SFX_0606,
 SFX_0607,
 SFX_NOSEDIVE,
 SFX_UFOHUM,
 SFX_ALARM_CHICAGO = 0x6455,
 SFX_8000 = 0x8000,
 SFX_DOOR_8001,
 SFX_DOOR_8002,
 SFX_DOOR_8003,
 SFX_DOOR_8004,
 SFX_DOOR_8005,
 SFX_DOOR_8006,
 SFX_DOOR_8007,
 SFX_DOOR_8008,
 SFX_8009,
 SFX_DOOR_800A,
 SFX_DOOR_800B,
 SFX_DOOR_800C,
 SFX_DOOR_800D,
 SFX_DOOR_800E,
 SFX_DOOR_800F,
 SFX_DOOR_8010,
 SFX_DOOR_8011,
 SFX_DOOR_8012,
 SFX_DOOR_8013,
 SFX_DOOR_8014,
 SFX_DOOR_8015,
 SFX_DOOR_8016,
 SFX_DOOR_8017,
 SFX_DOOR_8018,
 SFX_DOOR_8019,
 SFX_DOOR_801A,
 SFX_DOOR_801B,
 SFX_DOOR_801C,
 SFX_DOOR_801D,
 SFX_DOOR_801E,
 SFX_DOOR_801F,
 SFX_DOOR_8020,
 SFX_DOOR_8021,
 SFX_DOOR_8022,
 SFX_8023,
 SFX_8024,
 SFX_8025,
 SFX_DOOR_8026,
 SFX_DOOR_8027,
 SFX_CARR_HELLO_JOANNA,
 SFX_8029,
 SFX_802A,
 SFX_CIFEM_HI_THERE,
 SFX_GRIMSHAW_WELCOME,
 SFX_GRIMSHAW_HI_THERE,
 SFX_GRIMSHAW_UMM_ERR_HI,
 SFX_HOLO_HI,
 SFX_HANGAR_WHAT_DO_YOU_WANT,
 SFX_FOSTER_STAR_AGENT,
 SFX_CIM_HEY_THERE,
 SFX_CIM_HI,
 SFX_CIM_HOWS_IT_GOING,
 SFX_CIFEM_HELLO,
 SFX_CIFEM_HI_JO,
 SFX_CIFEM_HOWS_IT_GOING,
 SFX_8038,
 SFX_8039,
 SFX_803A,
 SFX_803B,
 SFX_803C,
 SFX_803D,
 SFX_803E,
 SFX_803F,
 SFX_MENU_ERROR,
 SFX_8041,
 SFX_8042,
 SFX_8043,
 SFX_8044,
 SFX_8045,
 SFX_8046,
 SFX_8047,
 SFX_8048,
 SFX_8049,
 SFX_804A,
 SFX_804B,
 SFX_804C,
 SFX_804D,
 SFX_804E,
 SFX_RELOAD_DEFAULT,
 SFX_8050,
 SFX_8051,
 SFX_FIREEMPTY,
 SFX_LAUNCH_ROCKET_8053,
 SFX_8054,
 SFX_FIRE_SHOTGUN,
 SFX_8056,
 SFX_DRUGSPY_FIREDART,
 SFX_8058,
 SFX_8059,
 SFX_805A,
 SFX_805B,
 SFX_805C,
 SFX_805D,
 SFX_805E,
 SFX_805F,
 SFX_8060,
 SFX_8061,
 SFX_8062,
 SFX_8063,
 SFX_8064,
 SFX_MAULER_CHARGE,
 SFX_8066,
 SFX_8067,
 SFX_SLAYER_WHIR,
 SFX_8069,
 SFX_806A,
 SFX_806B,
 SFX_806C,
 SFX_806D,
 SFX_806E,
 SFX_806F,
 SFX_8070,
 SFX_8071,
 SFX_8072,
 SFX_8073,
 SFX_8074,
 SFX_8075,
 SFX_HIT_CHR,
 SFX_HIT_GLASS,
 SFX_GLASS_SHATTER,
 SFX_HIT_METAL_8079,
 SFX_807A,
 SFX_HIT_METAL_807B,
 SFX_HATHIT_807C,
 SFX_HIT_SNOW,
 SFX_HIT_WOOD_807E,
 SFX_HIT_WOOD_807F,
 SFX_HIT_WATER,
 SFX_HIT_MUD_8081,
 SFX_HIT_MUD_8082,
 SFX_HIT_MUD_8083,
 SFX_HIT_DIRT_8084,
 SFX_HIT_DIRT_8085,
 SFX_HIT_TILE,
 SFX_HIT_STONE_8087,
 SFX_HIT_STONE_8088,
 SFX_HIT_METALOBJ_8089,
 SFX_HIT_METALOBJ_808A,
 SFX_808B,
 SFX_EYESPYHIT,
 SFX_THUD_808D,
 SFX_THUD_808E,
 SFX_THUD_808F,
 SFX_THUD_8090,
 SFX_THUD_8091,
 SFX_THUD_8092,
 SFX_THUD_8093,
 SFX_THUD_8094,
 SFX_THUD_8095,
 SFX_THUD_8096,
 SFX_THUD_8097,
 SFX_EXPLOSION_8098,
 SFX_8099,
 SFX_EXPLOSION_809A,
 SFX_809B,
 SFX_809C,
 SFX_809D,
 SFX_809E,
 SFX_809F,
 SFX_80A0,
 SFX_80A1,
 SFX_80A2,
 SFX_80A3,
 SFX_80A4,
 SFX_80A5,
 SFX_80A6,
 SFX_80A7,
 SFX_80A8,
 SFX_THROW,
 SFX_80AA,
 SFX_DETONATE,
 SFX_DOOR_80AC,
 SFX_DOOR_80AD,
 SFX_DOOR_80AE,
 SFX_BIKE_PULSE,
 SFX_80B0,
 SFX_80B1,
 SFX_80B2,
 SFX_80B3,
 SFX_80B4,
 SFX_80B5,
 SFX_80B6,
 SFX_80B7,
 SFX_80B8,
 SFX_80B9,
 SFX_80BA,
 SFX_80BB,
 SFX_80BC,
 SFX_80BD,
 SFX_80BE,
 SFX_80BF,
 SFX_80C0,
 SFX_80C1,
 SFX_80C2,
 SFX_80C3,
 SFX_FOOTSTEP_80C4,
 SFX_FOOTSTEP_80C5,
 SFX_FOOTSTEP_80C6,
 SFX_FOOTSTEP_80C7,
 SFX_FOOTSTEP_80C8,
 SFX_FOOTSTEP_80C9,
 SFX_FOOTSTEP_80CA,
 SFX_FOOTSTEP_80CB,
 SFX_FOOTSTEP_80CC,
 SFX_FOOTSTEP_80CD,
 SFX_FOOTSTEP_80CE,
 SFX_FOOTSTEP_80CF,
 SFX_FOOTSTEP_80D0,
 SFX_FOOTSTEP_80D1,
 SFX_FOOTSTEP_80D2,
 SFX_FOOTSTEP_80D3,
 SFX_FOOTSTEP_80D4,
 SFX_FOOTSTEP_80D5,
 SFX_FOOTSTEP_80D6,
 SFX_FOOTSTEP_80D7,
 SFX_FOOTSTEP_80D8,
 SFX_FOOTSTEP_80D9,
 SFX_FOOTSTEP_80DA,
 SFX_FOOTSTEP_80DB,
 SFX_FOOTSTEP_80DC,
 SFX_FOOTSTEP_80DD,
 SFX_FOOTSTEP_80DE,
 SFX_FOOTSTEP_80DF,
 SFX_FOOTSTEP_80E0,
 SFX_FOOTSTEP_80E1,
 SFX_FOOTSTEP_80E2,
 SFX_FOOTSTEP_80E3,
 SFX_FOOTSTEP_80E4,
 SFX_FOOTSTEP_80E5,
 SFX_FOOTSTEP_80E6,
 SFX_FOOTSTEP_80E7,
 SFX_FOOTSTEP_80E8,
 SFX_FOOTSTEP_80E9,
 SFX_FOOTSTEP_80EA,
 SFX_FOOTSTEP_80EB,
 SFX_FOOTSTEP_80EC,
 SFX_FOOTSTEP_80ED,
 SFX_FOOTSTEP_80EE,
 SFX_FOOTSTEP_80EF,
 SFX_FOOTSTEP_80F0,
 SFX_FOOTSTEP_80F1,
 SFX_FOOTSTEP_80F2,
 SFX_FOOTSTEP_80F3,
 SFX_FOOTSTEP_80F4,
 SFX_FOOTSTEP_80F5,
 SFX_80F6,
 SFX_M0_WHAT_THE_HELL,
 SFX_M0_DAMN_IT_MISSED,
 SFX_M0_GODS_SAKE_SOMEONE_HIT_HER,
 SFX_M0_GET_THE_HELL_OUT_OF_HERE,
 SFX_M0_YOU_BITCH,
 SFX_M0_OH_MY_GOD,
 SFX_80FE,
 SFX_80FF,
 SFX_8100,
 SFX_8101,
 SFX_8102,
 SFX_SCI_WHO_THE_HELL_ARE_YOU,
 SFX_8104,
 SFX_8105,
 SFX_8106,
 SFX_8107,
 SFX_8108,
 SFX_8109,
 SFX_810A,
 SFX_810B,
 SFX_SHIP_HUM,
 SFX_810D,
 SFX_810E,
 SFX_810F,
 SFX_8110,
 SFX_8111,
 SFX_8112,
 SFX_8113,
 SFX_8114,
 SFX_8115,
 SFX_8116,
 SFX_8117,
 SFX_TYPING_8118,
 SFX_8119,
 SFX_811A,
 SFX_811B,
 SFX_811C,
 SFX_811D,
 SFX_811E,
 SFX_811F,
 SFX_8120,
 SFX_8121,
 SFX_8122,
 SFX_8123,
 SFX_M1_HOLY,
 SFX_M1_WHAT_THE_HELL,
 SFX_M1_OH_MY_GOD,
 SFX_M1_OH_GOD_IM_HIT,
 SFX_M1_MY_GOD,
 SFX_M1_NOOO,
 SFX_M1_BLOODY_STUPID_GUN,
 SFX_M1_DAMN_IT,
 SFX_M1_DAMN_SHES_GOOD,
 SFX_M1_COVER_MY_ASS,
 SFX_M1_SCREAM,
 SFX_F_MY_GOD,
 SFX_M2_GEEZ_THAT_HURT,
 SFX_M2_DAMN_IT_IM_TAKING_FIRE,
 SFX_M2_GOD_DAMN_IT,
 SFX_M2_HOLY_MOLY,
 SFX_M2_DAMN_MISSED_AGAIN,
 SFX_M2_DAMN_YOU,
 SFX_M2_HELL_SHES_GOOD,
 SFX_M2_LETS_GET_THE_HELL_OUT_OF_HERE,
 SFX_M2_NOOO,
 SFX_M2_OH_GOD_IM_DYING,
 SFX_M2_GOD_RUN,
 SFX_813E,
 SFX_813F,
 SFX_8140,
 SFX_8141,
 SFX_8142,
 SFX_8143,
 SFX_8144,
 SFX_8145,
 SFX_8146,
 SFX_8147,
 SFX_8148,
 SFX_8149,
 SFX_CIV_OH_MY_GOD,
 SFX_814B,
 SFX_814C,
 SFX_8150,
 SFX_8151,
 SFX_8152,
 SFX_8153,
 SFX_8154,
 SFX_8155,
 SFX_8156,
 SFX_8157,
 SFX_8158,
 SFX_8159,
 SFX_815A,
 SFX_815B,
 SFX_815C,
 SFX_M0_HOWS_THINGS,
 SFX_M0_HEY_THERE,
 SFX_M0_HI_HOW_ARE_YOU,
 SFX_8160,
 SFX_8161,
 SFX_8162,
 SFX_M1_HI_THERE,
 SFX_M1_HOWS_THINGS,
 SFX_M2_HELLO,
 SFX_M2_HEY_WHATS_UP,
 SFX_M0_WHAT_THE_HELL_8167,
 SFX_M1_M2_GEEZ,
 SFX_8169,
 SFX_816A,
 SFX_DOOR_816B,
 SFX_DOOR_816C,
 SFX_DOOR_816D,
 SFX_816E,
 SFX_816F,
 SFX_8170,
 SFX_8171,
 SFX_8172,
 SFX_8173,
 SFX_8174,
 SFX_8175,
 SFX_8176,
 SFX_8177,
 SFX_8178,
 SFX_8179,
 SFX_817A,
 SFX_817B,
 SFX_817C,
 SFX_817D,
 SFX_817E,
 SFX_817F,
 SFX_8180,
 SFX_8181,
 SFX_8182,
 SFX_8183,
 SFX_8184,
 SFX_8185,
 SFX_8186,
 SFX_FOOTSTEP_8187,
 SFX_FOOTSTEP_8188,
 SFX_FOOTSTEP_8189,
 SFX_FOOTSTEP_818A,
 SFX_FOOTSTEP_818B,
 SFX_FOOTSTEP_818C,
 SFX_FOOTSTEP_818D,
 SFX_FOOTSTEP_818E,
 SFX_FOOTSTEP_818F,
 SFX_FOOTSTEP_8190,
 SFX_FOOTSTEP_8191,
 SFX_FOOTSTEP_8192,
 SFX_8193,
 SFX_8194,
 SFX_8195,
 SFX_8196,
 SFX_8197,
 SFX_8198,
 SFX_8199,
 SFX_819A,
 SFX_819B,
 SFX_819C,
 SFX_819D,
 SFX_819E,
 SFX_819F,
 SFX_81A0,
 SFX_81A1,
 SFX_81A2,
 SFX_81A3,
 SFX_81A4,
 SFX_81A5,
 SFX_81A6,
 SFX_81A7,
 SFX_DOOR_81A8,
 SFX_81A9,
 SFX_DOOR_81AA,
 SFX_DOOR_81AB,
 SFX_81AC,
 SFX_DOOR_81AD,
 SFX_DOOR_81AE,
 SFX_DOOR_81AF,
 SFX_DOOR_81B0,
 SFX_DOOR_81B1,
 SFX_DOOR_81B2,
 SFX_DOOR_81B3,
 SFX_DOOR_81B4,
 SFX_DOOR_81B5,
 SFX_DOOR_81B6,
 SFX_DOOR_81B7,
 SFX_DOOR_81B8,
 SFX_81B9,
 SFX_81BA,
 SFX_81BB
};
