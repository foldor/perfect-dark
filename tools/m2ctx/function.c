s32 func0f0b28d0(void *arg0, void *arg1) {
    s16 temp_v1;
    s32 temp_t6;
    s32 temp_v0;
    s32 phi_v0;

    temp_v1 = arg1->unk14;
    temp_v0 = (s32) temp_v1 < 1;
    phi_v0 = temp_v0;
    if (temp_v0 == 0) {
        temp_t6 = arg0->unk10 != 0;
        phi_v0 = temp_t6;
        if (temp_t6 != 0) {
            phi_v0 = ((s32) arg0->unk2 < (s32) temp_v1) ^ 1;
        }
    }
    return phi_v0;
}
